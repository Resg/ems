FROM sdc.grfc.ru:5050/node:alpine as build
COPY . .
RUN yarn --ignore-engines --network-timeout 10000000
RUN yarn build:stage
FROM sdc.grfc.ru:5050/nginx:alpine
COPY nginx.conf /etc/nginx/nginx.conf
RUN rm -rf /usr/share/nginx/html/*
COPY --from=build /build /usr/share/nginx/html
EXPOSE 80
ENTRYPOINT ["nginx", "-g", "daemon off;"]

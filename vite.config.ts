import { defineConfig } from 'vite';
import { ViteAliases } from 'vite-aliases';
import svgr from 'vite-plugin-svgr';

import react from '@vitejs/plugin-react';

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    outDir: 'build',
  },
  server: {
    port: 3000,
  },
  plugins: [
    ViteAliases({ dir: 'src', prefix: '@', depth: 0 }),
    svgr(),
    react(),
  ],
});

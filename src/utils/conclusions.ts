import { Column } from 'react-data-grid';

import { TreeRow } from '../components/CustomDataGrid/types';
import { additionalsCols, appendicesCols } from '../constants/conclusions';
import {
  ConclusionAdditional,
  ConclusionAppendice,
  ConclusionFileType,
} from '../types/conclusions';

export const appendicesToTableAdapter = (data?: ConclusionAppendice[]) => {
  if (!data) {
    return {
      cols: appendicesCols,
      rows: [],
    };
  }

  return {
    cols: appendicesCols,
    rows: data.map(({ id, name, text, createDate }) => ({
      id,
      name,
      text,
      createDate,
    })),
  };
};

export const additionalsToTableAdapter = (data?: ConclusionAdditional[]) => {
  if (!data) {
    return {
      cols: additionalsCols,
      rows: [],
    };
  }

  return {
    cols: additionalsCols,
    rows: data.map(
      ({ id, name, ordinalNumber, conclusionText, permissionText }) => ({
        id,
        ordinalNumber,
        name,
        conclusionText,
        permissionText,
      })
    ),
  };
};

export const filesToTableAdapter = (
  files?: Array<ConclusionFileType>
): { cols: Column<TreeRow<any>>[]; rows: any } => {
  if (!files) {
    return {
      cols: [
        {
          key: 'name',
          name: 'Имя файла',
        },
      ],
      rows: [],
    };
  }
  return {
    cols: [
      {
        key: 'name',
        name: 'Имя файла',
      },
    ],
    rows: [
      ...files.map((file) => ({
        id: file.id,
        name: file.name,
        link: true,
        selected: false,
      })),
    ],
  };
};

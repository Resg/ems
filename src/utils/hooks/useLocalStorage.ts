import { useEffect, useState } from 'react';

const getStorageValue = (key: string) => {
  const storageValue = localStorage.getItem(key) ?? '';
  const initial = storageValue ? JSON.parse(storageValue) : [{ clear: true }];

  return initial;
};

export const useLocalStorage = (key: string) => {
  const [value, setValue] = useState(() => getStorageValue(key));

  useEffect(() => {
    localStorage.setItem(key, JSON.stringify(value));
  }, [value, setValue]);

  return [value, setValue];
};

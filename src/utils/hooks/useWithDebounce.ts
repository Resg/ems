import { useMemo } from 'react';

export const useWithDebounce = (
  fn: (...props: any) => any,
  delay: number = 300
) => {
  let timeOut: ReturnType<typeof setTimeout>;
  const resultFunction = useMemo(
    () =>
      (...props: any) => {
        if (timeOut) {
          clearTimeout(timeOut);
        }
        timeOut = setTimeout(() => fn(...props), delay);
      },
    [fn, delay]
  );

  return resultFunction;
};

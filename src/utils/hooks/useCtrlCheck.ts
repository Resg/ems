import { useEffect, useState } from 'react';

export const useCtrlCheck = () => {
  const [ctrlPress, setCtrlPress] = useState(false);
  useEffect(() => {
    const keyDownHandler = ({ key }: KeyboardEvent) => {
      if (key?.toLowerCase() === 'control') {
        setCtrlPress(true);
      }
    };

    const keyUpHandler = ({ key }: KeyboardEvent) => {
      if (key?.toLowerCase() === 'control') {
        setCtrlPress(false);
      }
    };
    window.addEventListener('keydown', keyDownHandler);
    window.addEventListener('keyup', keyUpHandler);

    return () => {
      window.removeEventListener('keydown', keyDownHandler);
      window.removeEventListener('keyup', keyUpHandler);
    };
  }, []);

  return ctrlPress;
};

const unsecuredCopyToClipboard = (
  text: string,
  successCallback?: () => void
) => {
  const textArea = document.createElement('textarea');
  textArea.value = text;
  textArea.setAttribute('style', 'position: fixed');
  document.body.prepend(textArea);
  textArea.focus();
  textArea.select();

  try {
    document.execCommand('copy');
    successCallback && successCallback();
  } catch (err) {
    console.error('Ошибка копирования', err);
  }

  document.body.removeChild(textArea);
};

export const copyToClipboard = (text: string, successCallback?: () => void) => {
  if (window.isSecureContext && navigator.clipboard) {
    navigator.clipboard.writeText(text);
    successCallback && successCallback();
  } else {
    unsecuredCopyToClipboard(text, successCallback);
  }
};

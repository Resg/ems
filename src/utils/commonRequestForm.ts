import { format } from 'date-fns';

import { RequestType } from '../types/commonRequestForm';

export const prepareData = (data: RequestType, documentId: string) => {
  return {
    ...data,
    date: format(new Date(data.date), 'yyyy-MM-dd'),
    permission:
      data.documentType === 'PERMISSION' ? Number(documentId) : undefined,
    conclusion:
      data.documentType === 'CONCLUSION' ? Number(documentId) : undefined,
    documentType: undefined,
    document: undefined,
    frequency: data?.frequency?.length ? [Number(data.frequency)] : [],
  };
};

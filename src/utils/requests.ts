import { RequestStageCols } from '../constants/requests';
import {
  AnnulledConclusionData,
  AnnulledPermissions,
  ConclusionData,
  ConclusionDocumentsData,
  PCTRData,
  RequestDocument,
  RequestStage,
  StageCreateData,
} from '../types/requests';

export const stagesToRowAdapter = (stages?: RequestStage[]) => {
  if (!stages) {
    return { cols: [], rows: [] };
  }
  return {
    cols: RequestStageCols,
    rows: stages.map(
      ({
        operationId,
        stageName,
        performerClerkName,
        startDate,
        planDate,
        endDate,
      }) => ({
        id: operationId,
        stageName,
        performerClerkName,
        startDate: new Date(startDate).toLocaleDateString(),
        planDate: new Date(planDate).toLocaleDateString(),
        endDate: endDate && new Date(endDate).toLocaleDateString(),
      })
    ),
  };
};

export const getStageNames = (data?: StageCreateData[]) => {
  if (data) {
    return data.map((i) => ({
      label: i.stageName,
      value: i.stageId,
    }));
  }

  return [];
};

export const ConclusionsToRowAdapter = (conclusions?: ConclusionData[]) => {
  return (
    conclusions?.map(
      ({
        id,
        number,
        date,
        dateValidity,
        dateAnnulment,
        state,
        status,
        type,
        sort,
        createdDateTimeName,
      }) => ({
        id,
        number,
        state,
        status,
        type,
        sort,
        createdDateTimeName,
        date: date && new Date(date).toLocaleDateString(),
        dateAnnulment:
          dateAnnulment && new Date(dateAnnulment).toLocaleDateString(),
        dateValidity:
          dateValidity && new Date(dateValidity).toLocaleDateString(),
      })
    ) || []
  );
};

export const AnnulledPermissionsToRowAdapter = (
  data?: AnnulledPermissions[]
) => {
  return (
    data?.map(
      ({
        id,
        requestId,
        permissionId,
        contractorName,
        number,
        date,
        dateValidity,
        dateAnnulment,
        state,
        stateCode,
        baseDocument,
        type,
        serviceMRFC,
        technology,
        regionList,
        federalDistrictList,
        stationCount,
      }) => ({
        id,
        requestId,
        permissionId,
        contractorName,
        number,
        date: date && new Date(date).toLocaleDateString(),
        dateAnnulment:
          dateAnnulment && new Date(dateAnnulment).toLocaleDateString(),
        dateValidity:
          dateValidity && new Date(dateValidity).toLocaleDateString(),
        type,
        state,
        baseDocument,
        serviceMRFC,
        technology,
        regionList,
        federalDistrictList,
        stationCount,
        stateCode,
      })
    ) || []
  );
};

export const AnnulledConclusionsToRowAdapter = (
  conclusions?: AnnulledConclusionData[]
) => {
  return (
    conclusions?.map(
      ({
        id,
        requestId,
        number,
        date,
        dateValidity,
        dateAnnulment,
        stateCode,
        state,
        statusCode,
        status,
        typeCode,
        type,
        sortId,
        sort,
        createdDateTimeName,
        conclusionRequestId,
      }) => ({
        id,
        requestId,
        number,
        stateCode,
        state,
        statusCode,
        status,
        typeCode,
        type,
        sortId,
        sort,
        createdDateTimeName,
        date: date && new Date(date).toLocaleDateString(),
        dateAnnulment:
          dateAnnulment && new Date(dateAnnulment).toLocaleDateString(),
        dateValidity:
          dateValidity && new Date(dateValidity).toLocaleDateString(),
        conclusionRequestId,
      })
    ) || []
  );
};

export const ConclusionDocumentsToRowAdapter = (
  conclutionDocuments?: ConclusionDocumentsData[]
) => {
  return (
    conclutionDocuments?.map(
      ({
        id,
        title,
        type,
        number,
        rubricList,
        performer,
        description,
        date,
        dateSign,
        recipientList,
        contractorDate,
        contractorNumber,
      }) => ({
        id,
        title,
        type,
        number,
        rubricList,
        performer,
        description,
        date: date && new Date(date).toLocaleDateString(),
        dateSign: dateSign && new Date(dateSign).toLocaleDateString(),
        recipientList,
        contractorDate:
          contractorDate && new Date(contractorDate).toLocaleDateString(),
        contractorNumber,
      })
    ) || []
  );
};
export const RequestInitialDocumentsToRowAdapter = (
  requestDocuments?: RequestDocument[]
) => {
  return (
    requestDocuments?.map(
      ({
        id,
        title,
        type,
        number,
        date,
        description,
        rubricList,
        contractorList,
        contractorDate,
        contractorNumber,
      }) => ({
        id,
        title,
        type,
        number,
        date: date && new Date(date).toLocaleDateString(),
        description,
        rubricList,
        contractorList,
        contractorDate:
          contractorDate && new Date(contractorDate).toLocaleDateString(),
        contractorNumber,
      })
    ) || []
  );
};

export const RequestDocumentsToRowAdapter = (
  requestDocuments?: RequestDocument[]
) => {
  return (
    requestDocuments?.map(
      ({
        id,
        title,
        type,
        number,
        date,
        description,
        dateSign,
        performer,
        rubricList,
        recipientList,
        contractorDate,
      }) => ({
        id,
        title,
        type,
        number,
        date: date && new Date(date).toLocaleDateString(),
        description,
        dateSign: dateSign && new Date(dateSign).toLocaleDateString(),
        performer,
        rubricList,
        recipientList,
        contractorDate,
      })
    ) || []
  );
};

export const PCTRToRowAdapter = (PCTRDocuments?: PCTRData[]) => {
  return (
    PCTRDocuments?.map(
      ({
        reqId,
        spsNumber,
        stationNumber,
        address,
        additionAddress,
        afsHeight,
        angleAsimuth,
        antennaGain,
        antennaLoss,
        powerTransm,
        channels,
        stationState,
        etsVidId,
        afsHeightSea,
        angleElevation,
        apertureAngleH,
        apertureAngleV,
        classEmi,
        polarization,
        cellRadius,
        stationPoint,
        freqReceive,
        freqTransm,
      }) => ({
        id: reqId,
        spsNumber,
        stationNumber,
        address,
        additionAddress,
        afsHeight,
        angleAsimuth,
        antennaGain,
        antennaLoss,
        powerTransm,
        channels,
        stationState,
        etsVidId,
        afsHeightSea,
        angleElevation,
        apertureAngleH,
        apertureAngleV,
        classEmi,
        polarization,
        cellRadius,
        stationPoint: `${stationPoint.coordDegrees.latitude} | ${stationPoint.coordDegrees.longitude} `,
        freqTransm: `${freqTransm} | ${freqReceive}`,
      })
    ) || []
  );
};

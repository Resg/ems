import { FormValues } from '../components/FilterBarNew';

export const prepareDataForCounterpartySearch = (values: FormValues) => {
  const preparedData = {
    isOutgoingDocumentsSendingForbidden:
      values.filters.isOutgoingDocumentsSendingForbidden === undefined
        ? undefined
        : values.filters.isOutgoingDocumentsSendingForbidden,
  };

  const receivingLegalData = () => {
    if (values.filters.legalEntity) {
      const legalData = {
        legalEntity: {
          organizationalLegalFormId: values.filters.organizationalLegalFormId
            ? values.filters.organizationalLegalFormId
            : undefined,
          isBudgetEntity:
            values.filters.isBudgetEntity === undefined ||
            values.filters.isBudgetEntity === false
              ? undefined
              : values.filters.isBudgetEntity,
          name:
            values.filters.name && values.filters.name.length
              ? values.filters.name
              : undefined,
          fullName:
            values.filters.fullName && values.filters.fullName.length
              ? values.filters.fullName
              : undefined,
          inn:
            values.filters.innLegalEntity &&
            values.filters.innLegalEntity.length
              ? values.filters.innLegalEntity.replace(/[^0-9]/g, '')
              : undefined,
          ogrn:
            values.filters.ogrn && values.filters.ogrn.length
              ? values.filters.ogrn.replace(/[^0-9]/g, '')
              : undefined,
          countryId: values.filters.countryId
            ? values.filters.countryId
            : undefined,
          parentName: values.filters.parentName
            ? values.filters.parentName
            : undefined,
          contactPersonName:
            values.filters.contactPersonName &&
            values.filters.contactPersonName.length
              ? values.filters.contactPersonName
              : undefined,
          comments:
            values.filters.commentsLegalEntity &&
            values.filters.commentsLegalEntity.length
              ? values.filters.commentsLegalEntity
              : undefined,
          createDateFrom:
            values.filters.createDateFrom &&
            values.filters.createDateFrom.length
              ? values.filters.createDateFrom
              : undefined,
          createDateTo:
            values.filters.createDateTo && values.filters.createDateTo.length
              ? values.filters.createDateTo
              : undefined,
          isActual:
            values.filters.isActual === undefined ||
            values.filters.isActual === false
              ? undefined
              : values.filters.isActual,
          isWorkViaWebAllowed:
            values.filters.isWorkViaWebAllowed === undefined ||
            values.filters.isWorkViaWebAllowed === false
              ? undefined
              : values.filters.isWorkViaWebAllowed,
          isOnHandIssuance:
            values.filters.isOnHandIssuance === undefined ||
            values.filters.isOnHandIssuance === false
              ? undefined
              : values.filters.isOnHandIssuance,
          isReorganized:
            values.filters.isReorganized === undefined ||
            values.filters.isReorganized === false
              ? undefined
              : values.filters.isReorganized,
        },
      };
      return legalData;
    }
    return;
  };

  const receivingPhisicalData = () => {
    if (values.filters.physicalEntity) {
      const phisicalData = {
        physicalEntity: {
          lastName:
            values.filters.lastName && values.filters.lastName.length
              ? values.filters.lastName
              : undefined,
          firstName:
            values.filters.firstName && values.filters.firstName.length
              ? values.filters.firstName
              : undefined,
          middleName:
            values.filters.middleName && values.filters.middleName.length
              ? values.filters.middleName
              : undefined,

          passportSeries:
            values.filters.passportSeries &&
            values.filters.passportSeries.length
              ? values.filters.passportSeries
              : undefined,
          passportNumber:
            values.filters.passportNumber &&
            values.filters.passportNumber.length
              ? values.filters.passportNumber
              : undefined,
          inn:
            values.filters.innPhysicalEntity &&
            values.filters.innPhysicalEntity.length
              ? values.filters.innPhysicalEntity.replace(/[^0-9]/g, '')
              : undefined,
          comments:
            values.filters.commentsPhysicalEntity &&
            values.filters.commentsPhysicalEntity.length
              ? values.filters.commentsPhysicalEntity
              : undefined,
          createDateFrom:
            values.filters.createDateFromPhysicalEntity &&
            values.filters.createDateFromPhysicalEntity.length
              ? values.filters.createDateFromPhysicalEntity
              : undefined,
          createDateTo:
            values.filters.createDateToPhysicalEntity &&
            values.filters.createDateToPhysicalEntity.length
              ? values.filters.createDateToPhysicalEntity
              : undefined,
        },
      };

      return phisicalData;
    }
    return;
  };

  const phisicalData = receivingPhisicalData();
  const legalData = receivingLegalData();

  return {
    chipName: values.chipName || '',
    filters: {
      ...phisicalData,
      ...legalData,
      ...preparedData,
    },
  };
};

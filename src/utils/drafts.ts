import { IncomingDraftsData } from '../types/drafts';

export const DraftsToRowAdapter = (drafts?: IncomingDraftsData[]) => {
  return (
    drafts?.map((draft) => ({
      ...draft,
      documentDate:
        draft.documentDate && new Date(draft.documentDate).toLocaleDateString(),
      planDate: draft.planDate && new Date(draft.planDate).toLocaleDateString(),
    })) || []
  );
};

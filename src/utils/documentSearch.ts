import { FormValues } from '../components/FilterBarNew';
import { DocumentSearch } from '../types/documentSearch';

export const documentSearchToRowAdapter = (
  documentSearch?: DocumentSearch[]
) => {
  return (
    documentSearch?.map(
      ({
        id,
        type,
        number,
        date,
        dateSign,
        localNumber,
        dateCreate,
        rubricList,
        description,
        comments,
        status,
        contractorList,
        contractorNumber,
        contractorDate,
        contractorCountry,
        recipientList,
        incomingNumber,
        recieptDate,
        performer,
        divisionFullName,
        requestList,
        returnDate,
        contractorDateSign,
        recipientListDRKK,
        channelList,
        regionList,
        resAddressList,
        managerList,
        сoordinationСountryCodeList,
      }) => ({
        id,
        type,
        number,
        date,
        dateSign,
        localNumber,
        dateCreate: new Date(dateCreate).toLocaleDateString(),
        rubricList,
        description,
        comments,
        status,
        contractorList,
        contractorNumber,
        contractorDate,
        contractorCountry,
        recipientList,
        incomingNumber,
        recieptDate,
        performer,
        divisionFullName,
        requestList,
        returnDate,
        contractorDateSign,
        recipientListDRKK,
        channelList,
        regionList,
        resAddressList,
        managerList,
        сoordinationСountryCodeList,
      })
    ) || []
  );
};

export const prepareData = (values: FormValues) => {
  const cleanValue = {
    isResolutionPOExternalDate: undefined,
    isResolutionPODate: undefined,
    isResolutionPOPlanDate: undefined,
    isDate: undefined,
    isDateCreation: undefined,
    isDateSign: undefined,
    isDateContractor: undefined,
    isRequestMaterialOuterDate: undefined,
    requestContractor: values.filters.requestContractor
      ? values.filters.requestContractor
      : undefined,

    resolutionPOAuthorContactPerson: values.filters
      .resolutionPOAuthorContactPerson
      ? values.filters.resolutionPOAuthorContactPerson
      : undefined,

    resolutionPOPerformerContactPerson: values.filters
      .resolutionPOPerformerContactPerson
      ? values.filters.resolutionPOPerformerContactPerson
      : undefined,

    requestTechnologyIds:
      values.filters.requestTechnologyIds &&
      values.filters.requestTechnologyIds.length
        ? values.filters.requestTechnologyIds
        : undefined,

    requestRegionAoguids:
      values.filters.requestRegionAoguids &&
      values.filters.requestRegionAoguids.length
        ? values.filters.requestRegionAoguids
        : undefined,

    isHaveBoundReguest:
      values.filters.isHaveBoundReguest === false
        ? undefined
        : values.filters.isHaveBoundReguest,

    isMyDocuments:
      values.filters.isMyDocuments === false
        ? undefined
        : values.filters.isMyDocuments,
    contractorIds:
      values.filters.contractorIds && values.filters.contractorIds.length
        ? values.filters.contractorIds
        : undefined,
    creatorClerkIds:
      values.filters.creatorClerkIds && values.filters.creatorClerkIds.length
        ? values.filters.creatorClerkIds
        : undefined,
    externalSigners:
      values.filters.externalSigners && values.filters.externalSigners.length
        ? values.filters.externalSigners
        : undefined,
    innerAddresseeIds:
      values.filters.innerAddresseeIds &&
      values.filters.innerAddresseeIds.length
        ? values.filters.innerAddresseeIds
        : undefined,
    isEDSAttached:
      values.filters.isEDSAttached === false
        ? undefined
        : values.filters.isEDSAttached,
    isLegacyClient:
      values.filters.isLegacyClient === false
        ? undefined
        : values.filters.isLegacyClient,
    isManagerSet:
      values.filters.isManagerSet === false
        ? undefined
        : values.filters.isManagerSet,
    isPhysicalClient:
      values.filters.isPhysicalClient === false
        ? undefined
        : values.filters.isPhysicalClient,
    locationClerkIds:
      values.filters.locationClerkIds && values.filters.locationClerkIds.length
        ? values.filters.locationClerkIds
        : undefined,
    locationDivisionIds:
      values.filters.locationDivisionIds &&
      values.filters.locationDivisionIds.length
        ? values.filters.locationDivisionIds
        : undefined,
    performerClerkIds:
      values.filters.performerClerkIds &&
      values.filters.performerClerkIds.length
        ? values.filters.performerClerkIds
        : undefined,
    performerDivisionIds:
      values.filters.performerDivisionIds &&
      values.filters.performerDivisionIds.length
        ? values.filters.performerDivisionIds
        : undefined,
    registrantIds:
      values.filters.registrantIds && values.filters.registrantIds.length
        ? values.filters.registrantIds
        : undefined,
    recipientIds:
      values.filters.recipientIds && values.filters.recipientIds.length
        ? values.filters.recipientIds
        : undefined,
    rubricIds:
      values.filters.rubricIds && values.filters.rubricIds.length
        ? values.filters.rubricIds
        : undefined,
    signerClerkIds:
      values.filters.signerClerkIds && values.filters.signerClerkIds.length
        ? values.filters.signerClerkIds
        : undefined,
    statusCodes:
      values.filters.statusCodes && values.filters.statusCodes.length
        ? values.filters.statusCodes
        : undefined,
    classifierName:
      values.filters.classifierName && values.filters.classifierName.length
        ? values.filters.classifierName
        : undefined,
    isHaveNoFiles:
      values.filters.isHaveNoFiles !== null
        ? values.filters.isHaveNoFiles
        : undefined,
    haveDateResolution: undefined,
    haveDatePlanResolution: undefined,
    haveDateInterimResolution: undefined,
    haveDateRemovedResolution: undefined,
    dateExpires: undefined,
    haveNoResolution:
      values.filters.haveNoResolution === false
        ? undefined
        : values.filters.haveNoResolution,
    haveResolution:
      values.filters.haveResolution === false
        ? undefined
        : values.filters.haveResolution,
    resolutionControlType:
      values.filters.resolutionControlType !== null
        ? values.filters.resolutionControlType
        : undefined,
    isChildrenIncluded:
      values.filters.isChildrenIncluded === false
        ? undefined
        : values.filters.isChildrenIncluded,
    isResponsible:
      values.filters.isResponsible === false
        ? undefined
        : values.filters.isResponsible,
    isRemovedControl:
      values.filters.isRemovedControl === false
        ? undefined
        : values.filters.isRemovedControl,
    resolutionAuthorIds:
      values.filters.resolutionAuthorIds &&
      values.filters.resolutionAuthorIds.length
        ? values.filters.resolutionAuthorIds
        : undefined,
    resolutionPerformerClerkIds:
      values.filters.resolutionPerformerClerkIds &&
      values.filters.resolutionPerformerClerkIds
        .filter((id: string) => id.includes('_C'))
        .map((id: string) => Number(id.split('_')[0])).length
        ? values.filters.resolutionPerformerClerkIds
            .filter((id: string) => id.includes('_C'))
            .map((id: string) => Number(id.split('_')[0]))
        : undefined,
    resolutionPerformerDivisionIds:
      values.filters.resolutionPerformerClerkIds &&
      values.filters.resolutionPerformerClerkIds
        .filter((id: string) => id.includes('_D'))
        .map((id: string) => Number(id.split('_')[0])).length
        ? values.filters.resolutionPerformerClerkIds
            .filter((id: string) => id.includes('_D'))
            .map((id: string) => Number(id.split('_')[0]))
        : undefined,

    haveResolutionPO: values.filters.haveResolutionPO || undefined,
  };

  return {
    ...values,
    filters: {
      ...values.filters,
      ...cleanValue,
    },
  };
};

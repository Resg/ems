import { FormValues } from '../components/FilterBarNew';
import { ConclusionReviewFormData } from '../types/conclusions';

export const ConclusionToRowAdapter = (rows?: ConclusionReviewFormData[]) => {
  return (
    rows?.map((row) => ({
      ...row,
      date: row.date && new Date(row.date).toLocaleDateString(),
      validUntil:
        row.validUntil && new Date(row.validUntil).toLocaleDateString(),
    })) || []
  );
};

export const prepareData = (values: FormValues) => {
  const cleanValue = {
    isDate: undefined,
  };

  return {
    ...values,
    filters: {
      ...values.filters,
      ...cleanValue,
    },
  };
};

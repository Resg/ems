import { FormValues } from '../components/FilterBarNew';

export const prepareData = (values: FormValues) => {
  const cleanValue = {
    contractorIds:
      values.filters.contractorIds && values.filters.contractorIds.length
        ? values.filters.contractorIds
        : undefined,
    typeCodes:
      values.filters.typeCodes && values.filters.typeCodes.length
        ? values.filters.typeCodes
        : undefined,
    baseDocumentCodes:
      values.filters.baseDocumentCodes &&
      values.filters.baseDocumentCodes.length
        ? values.filters.baseDocumentCodes
        : undefined,
    aoguids:
      values.filters.aoguids && values.filters.aoguids.length
        ? values.filters.aoguids
        : undefined,
    stateCodes:
      values.filters.stateCodes && values.filters.stateCodes.length
        ? values.filters.stateCodes
        : undefined,
    serviceMRFCIds:
      values.filters.serviceMRFCIds && values.filters.serviceMRFCIds.length
        ? values.filters.serviceMRFCIds
        : undefined,
    technologyIds:
      values.filters.technologyIds && values.filters.technologyIds.length
        ? values.filters.technologyIds
        : undefined,
  };

  return {
    ...values,
    filters: {
      ...values.filters,
      ...cleanValue,
    },
  };
};

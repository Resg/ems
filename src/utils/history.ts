import { HistoryData } from '../types/documents';

export const historyToRowAdapter = (history?: HistoryData[]) => {
  return (
    history?.map(
      ({
        name,
        orderNumber,
        numberName,
        registryNum,
        registryName,
        date,
        time,
        clerkName,
        registryTargetClerkName,
        registryTargetDivision,
        requestManagerName,
        documentHolder,
        outgoingPackageNumber,
        postPackageLocalNumber,
        registryId,
      }) => ({
        id: registryId,
        orderNumber,
        name,
        numberName,
        registryNum,
        registryName,
        date: new Date(date).toLocaleDateString(),
        time,
        clerkName,
        registryTargetClerkName,
        registryTargetDivision,
        requestManagerName,
        documentHolder,
        outgoingPackageNumber,
        postPackageLocalNumber,
      })
    ) || []
  );
};

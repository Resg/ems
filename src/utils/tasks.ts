import { FormValues } from '../components/FilterBarNew';
import { DetailedStageDataFields } from '../constants/detailedStageData';
import type {
  ObjectTask,
  TaskDetails,
  TaskSubtaskValuesData,
} from '../types/tasks';

export const tasksToRowAdapter = (tasks: ObjectTask[]) => {
  return {
    cols: [
      {
        key: 'title',
        name: 'Задача',
      },
      {
        key: 'author',
        name: 'Автор',
      },
      {
        key: 'performer',
        name: 'Исполнитель',
      },
      {
        key: 'comments',
        name: 'Комментарий',
      },
      {
        key: 'performerComment',
        name: 'Комментарий по результату',
      },
      {
        key: 'status',
        name: 'Статус',
      },
      {
        key: 'termLabel',
        name: 'Дата исполнения/Срок исполнения (р/д)',
      },
      {
        key: 'completeDate',
        name: 'Дата и время исполнения фактическая',
      },
      {
        key: 'result',
        name: 'Результат',
      },
      {
        key: 'createDate',
        name: 'Дата и время создания',
      },
      {
        key: 'editor',
        name: 'Редактор',
      },
      {
        key: 'id',
        name: 'Номер задачи',
      },

      {
        key: 'externalTaskId',
        name: 'Номер задачи внешний',
      },
      {
        key: 'objectLabel',
        name: 'Объект',
      },
    ],
    rows: tasks.map((task) => {
      return {
        title: task.title,
        author: task.author,
        performer: task.performer,
        comments: task.comments,
        performerComment: task.performerComment,
        status: task.status,
        termLabel: task.termLabel,
        completeDate: task.completeDate,
        editor: task.editor,
        id: task.id,
        externalTaskId: task.externalTaskId,
        objectLabel: task.objectLabel,
        parentId: task.parentId,
      };
    }),
  };
};

export const taskSubtaskUpdateAdapter = (
  data: TaskDetails,
  values: TaskSubtaskValuesData
) => ({
  id: data.id,
  title: values[DetailedStageDataFields.NAME] || data.title,
  comment: values[DetailedStageDataFields.COMMENTS] || data.comments,
  previousId: values[DetailedStageDataFields.EXECUTE_AFTER] || data.previousId,
  term: data.term,
  performerId: data.performerId,
  isControl: data.isControl,
  authorId: data.authorId,
  NULLS: true,
  dateTarget: data.planDate,
});

export const prepareTaskCompletedData = (values: FormValues) => {
  const cleanValue = {
    isOnlyMain:
      values.filters.isOnlyMain !== null
        ? values.filters.isOnlyMain
        : undefined,
    authorIds:
      values.filters.authorIds && values.filters.authorIds.length
        ? values.filters.authorIds
        : undefined,
  };

  return {
    ...values,
    filters: {
      ...values.filters,
      ...cleanValue,
    },
  };
};

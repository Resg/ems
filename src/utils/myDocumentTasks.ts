import { MyDocTasksRowType, MyTasksType } from '../types/myDocumentTasks';

const TaskForUIExample = {
  title: 'Задача',
  rootObjectId: 'Штрих-код',
  author: 'Автор',
  performer: 'Исполнитель',
  comments: 'Комментарий',
  performerComment: 'Комментарий по результату',
  status: 'Статус',
  termLabel: 'Дата исполнения/Срок исполнения (р/д)',
  completeDate: 'Дата и время исполнения фактическая',
  result: 'Результат',
  createDate: 'Дата и время создания',
  editor: 'Редактор',
  externalTaskId: 'Номер задачи внешний',
  objectLabel: 'Объект',
};

export const prepareMyTasksForGrid = (
  tasks: Array<MyTasksType>
): Array<MyDocTasksRowType> =>
  tasks.map((task) => ({
    title: task.title,
    author: task.author,
    performer: task.performer,
    comments: task.comments,
    performerComment: task.performerComment,
    status: task.status,
    termLabel: task.termLabel,
    completeDate:
      task.completeDate &&
      new Date(task.completeDate)
        .toLocaleString()
        .slice(0, -3)
        .replace(/,/g, ''),
    result: task.result,
    createDate:
      task.createDate &&
      new Date(task.createDate).toLocaleString().slice(0, -3).replace(/,/g, ''),
    editor: task.editor,
    id: task.id,
    rootObjectId: task.rootObjectId,
    externalTaskId: task.externalTaskId,
    objectLabel: task.objectLabel,
    selected: false,
  }));

import { RegistryDocuments, RegistryPackets } from '../types/registries';

export const RegistryDocumentsToRowAdapter = (
  documents?: RegistryDocuments[]
) => {
  return (
    documents?.map(
      ({
        id,
        copyId,
        orderNumberInRegistry,
        type,
        number,
        date,
        compound,
        rubricList,
        contractorList,
        orderNumberName,
        status,
      }) => ({
        id,
        copyId,
        orderNumberInRegistry,
        type,
        number,
        date: date && new Date(date).toLocaleDateString(),
        compound,
        rubricList,
        contractorList,
        orderNumberName,
        status,
      })
    ) || []
  );
};

export const RegistryPacketsToRowAdapter = (packets?: RegistryPackets[]) => {
  return (
    packets?.map(
      ({
        id,
        number,
        dateSign,
        dateRegistration,
        contractorName,
        typeDelivery,
        rubricList,
        state,
        registryId,
      }) => ({
        id,
        number,
        dateSign: dateSign && new Date(dateSign).toLocaleDateString(),
        dateRegistration:
          dateRegistration && new Date(dateRegistration).toLocaleDateString(),
        contractorName,
        typeDelivery,
        rubricList,
        state,
        registryId,
      })
    ) || []
  );
};

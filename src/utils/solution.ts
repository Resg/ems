import { RequestModalSolution } from '../types/dictionaries';
import { RequestSolution } from '../types/solution';

export const solutionsToRowAdapter = (solutions?: RequestSolution[]) => {
  return (
    solutions?.map(
      (
        {
          parentSolutionNumber,
          id,
          number,
          date,
          dateEnd,
          logId,
          logDescription,
        },
        index
      ) => ({
        index: index + 1,
        parentSolutionNumber,
        id,
        number,
        date: new Date(date).toLocaleDateString(),
        dateEnd: dateEnd && new Date(dateEnd).toLocaleDateString(),
        logId,
        logDescription,
      })
    ) || []
  );
};

export const solutionModalToRowAdapter = (
  modalSolutions?: RequestModalSolution[]
) => {
  return (
    modalSolutions?.map(({ id, number, date, dateEnd }) => ({
      id,
      number,
      date: new Date(date).toLocaleDateString(),
      dateEnd: dateEnd && new Date(dateEnd).toLocaleDateString(),
    })) || []
  );
};

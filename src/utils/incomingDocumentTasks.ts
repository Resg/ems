import { FilterChip } from '../types/filterStack';
import {
  IncomingDocRowType,
  IncomingTaskDocColumnType,
  IncomingTaskDocRowType,
  Task,
} from '../types/incomingDocumentTasks';

const documentForUIExample: Partial<
  Record<keyof IncomingTaskDocRowType, string>
> = {
  id: 'ИД задачи',
  type: 'Тип задачи',
  status: 'Статус',
  performer: 'Исполнитель',
  planDate: 'Срок исполнения',
  performerComment: 'Комментарий исполнителя',
  author: 'Автор',
  documentType: 'Тип документа',
  rubricList: 'Рубрика',
  documentDescription: 'Содержание',
  contractorList: 'Корреспондент',
  documentNumber: 'Номер документа',
  documentDate: 'Дата документа',
  documentCorrespondentNumber: 'Номер корреспондента',
  documentCorrespondentDate: 'Дата корреспондента',
  documentStatus: 'Статус документа',
  documentInternalNumber: 'Внутренний номер документа',
  documentId: 'Штрих-код документа',
  mainDocumentIdList: 'Штрих-код ЭРК',
  includedDocumentIdList: 'Штрих-код ДЭРК',
  documentCreatedNameDateTime: 'Документ создан',
  resolutionId: 'ИД резолюции',
  resolutionContent: 'Содержание резолюции',
  resolutionControlParagrafString: 'Пункт резолюции',
  requestList: 'Номер заявки',
  requestServiceGrhcList: 'Служба ГРЧЦ',
};

export const makeIncomingDocumentTasksColumns =
  (): IncomingTaskDocColumnType[] =>
    Object.getOwnPropertyNames(documentForUIExample).map((key) => ({
      key: key as keyof IncomingTaskDocRowType,
      name: documentForUIExample[key as keyof IncomingTaskDocRowType] as string,
      width: '250px',
      hidden: false,
    }));

export async function massServerQuery<T, R = any>(
  data: T[],
  query: (body: T) => Promise<R>,
  size: number = 6
) {
  const parentArr: T[][] = [];
  while (data.length > 0) {
    parentArr.push(data.splice(0, size));
  }

  return await Promise.all(
    parentArr.map(async (childArr) => {
      await Promise.all(
        childArr.map(async (queryData) => await query(queryData))
      );
    })
  );
}

export const makeExpandedGroups = (
  rows: Array<IncomingDocRowType>
): Array<string> => {
  const nonUnique = rows?.reduce((acc, nextRow) => {
    acc.push(nextRow.performer, nextRow.type);
    return acc;
  }, [] as Array<string>);

  return [...new Set(nonUnique)];
};

export const sortFilterChipsByName = (
  chips: FilterChip[],
  order: 'asc' | 'desc'
) =>
  chips.sort((obj1, obj2) => {
    if (order === 'asc') {
      return obj1.name > obj2.name ? 1 : -1;
    }
    return obj1.name > obj2.name ? -1 : 1;
  });

export const prepareTasksForGrid = (
  tasks: Array<Task>
): Array<IncomingTaskDocRowType> =>
  tasks.map((task) => ({
    id: task.id,
    type: task.type,
    status: task.status,
    performer: task.performer,
    planDate: task.planDate && new Date(task.planDate).toLocaleDateString(),
    performerComment: task.performerComment,
    author: task.author,
    documentType: task.documentType,
    rubricList: task.rubricList,
    documentDescription: task.documentDescription,
    contractorList: task.contractorList,
    documentNumber: task.documentNumber,
    documentDate:
      task.documentDate && new Date(task.documentDate).toLocaleDateString(),
    documentCorrespondentNumber: task.documentCorrespondentNumber,
    documentCorrespondentDate:
      task.documentCorrespondentDate &&
      new Date(task.documentCorrespondentDate).toLocaleDateString(),
    documentStatus: task.documentStatus,
    documentInternalNumber: task.documentInternalNumber,
    documentId: task.documentId,
    mainDocumentIdList: task.mainDocumentIdList,
    includedDocumentIdList: task.includedDocumentIdList,
    documentCreatedNameDateTime: task.documentCreatedNameDateTime,
    resolutionId: task.resolutionId,
    resolutionContent: task.resolutionContent,
    resolutionControlParagrafString: task.resolutionControlParagrafString,
    requestList: task.requestList,
    requestServiceGrhcList: task.requestServiceGrhcList,
    selected: false,
  }));

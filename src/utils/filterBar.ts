const isNotFalsy = (val: any) => {
  return !(val === undefined || val === null || val === '');
};

export const clearFalsyValues = (values: Record<string, any>) => {
  const clearFilters = {} as Record<string, any>;

  for (const key in values) {
    const value = values[key];

    if (typeof value === 'object' && value !== null) {
      if (Array.isArray(value)) {
        if (value.length) {
          clearFilters[key] = value
            .map((val: any) => {
              if (typeof val === 'object') {
                return clearFalsyValues(val);
              } else {
                return val;
              }
            })
            .filter(isNotFalsy);
        }
      } else {
        clearFilters[key] = clearFalsyValues(value);
      }
    } else if (isNotFalsy(value)) {
      clearFilters[key] = value;
    }
  }

  return clearFilters;
};

import format from 'date-fns/format';
import { FormikValues, useFormik } from 'formik';
import { Column } from 'react-data-grid';

import { TreeRow } from '../components/CustomDataGrid/types';
import { FileLink } from '../components/FileLink';
import { documentReceiversCols } from '../constants/documents';
import { BoundsFiles } from '../containers/documents/bookmarks/Bonds/BoundsList';
import {
  ClerkForFormType,
  DictionariesClerkType,
  Option,
} from '../types/dictionaries';
import {
  AccessCodeType,
  ClerkReceiverType,
  CommonDocumentType,
  ControlType,
  CreateOutputDataType,
  DocumentDeliveryType,
  DocumentFileType,
  DocumentFollowedType,
  DocumentIncludedFileType,
  DocumentOwnerType,
  DocumentProtocolType,
  DocumentRequestsType,
  DocumentRubricType,
  DocumentSightingsType,
  UpdateOutputDataType,
} from '../types/documents';
import { UserInfo } from '../types/user';

export const ownersToFormAdapter = (
  owners?: Array<DocumentOwnerType>
): Record<string, any> => {
  if (!owners) {
    return {
      contractorContactPerson: [],
      clerkNameAddressee: [],
      clerkNameExe: null,
      clerkNameReceiver: [],
      clerkNameRegister: [],
      clerkNameSigner: [],
      commentsSender: [],
      contractorDocumentDate: [],
      contractorDocumentNumber: [],
    };
  }
  return {
    contractorContactPerson: [
      ...new Set(
        owners
          .filter(
            (owner) =>
              owner.usageCode === 'SENDER' && !!owner.contractorContactPerson
          )
          .map((owner) => owner.contractorContactPerson)
      ),
    ].join(', '),
    contractorDocumentNumber: [
      ...new Set(
        owners
          .filter(
            (owner) =>
              owner.usageCode === 'SENDER' && !!owner.contractorDocumentNumber
          )
          .map((owner) => owner.contractorDocumentNumber)
      ),
    ].join(', '),
    clerkNameReceiver: [
      ...new Map(
        owners
          .filter((owner) => owner.usageCode === 'RECEIVER')
          .map((owner) => ({
            id: owner.id,
            clerkId: owner.clerkId,
            clerkName: owner.clerkName ?? owner.divisionShortName,
            divisionId: owner.divisionId,
            signOrder: owner.signOrder,
            copyId: owner.copyId,
            contractorId: owner.contractorId,
            contractorContactPersonId: owner.contractorContactPersonId,
            comments: owner.comments,
            contractorDeliveryId: owner.contractorDeliveryId,
            contractorAddressDeliveryTypeCode:
              owner.contractorAddressDeliveryType,
            contractorContactId: owner.contractorContactId,
            packetOutgoingPagesCount: owner.packetOutgoingPagesCount,
            contractorName: owner.contractorName,
            contractorContactPerson: owner.contractorContactPerson,
            contractorAddressDeliveryType: owner.contractorAddressDeliveryType,
            contractorContactInfo: owner.contractorContactInfo,
            contractorAddressPostIndex: owner.contractorAddressPostIndex,
            contractorAddress: owner.contractorAddress,
            packetOutgoingSendDate: owner.packetOutgoingSendDate,
            packetPostNotificationNumber: owner.packetPostNotificationNumber,
            packetOutgoingDeliveryDate: owner.packetOutgoingDeliveryDate,
            packetPostReturnDate: owner.packetPostReturnDate,
            packetOutgoingStatus: owner.packetOutgoingStatus,
          }))
          .map((owner) => [owner.clerkId, owner])
      ).values(),
    ],
    clerkNameAddressee: owners
      .filter((owner) => owner.usageCode === 'ADDRESSEE' && !!owner.clerkName)
      .map((owner) => ({
        id: owner.id,
        clerkId: owner.clerkId,
        clerkName: owner.clerkName,
        divisionId: owner.divisionId,
      }))
      .find((owner) => owner),
    contractorDocumentDate: [
      ...new Set(
        owners
          .filter(
            (owner) =>
              owner.usageCode === 'SENDER' && !!owner.contractorDocumentDate
          )
          .map((owner) => owner.contractorDocumentDate)
      ),
    ].join(', '),
    clerkNameRegister: owners
      .filter((owner) => owner.usageCode === 'REGISTER' && !!owner.clerkName)
      .map((owner) => ({
        id: owner.id,
        clerkId: owner.clerkId,
        clerkName: owner.clerkName,
        divisionId: owner.divisionId,
      }))
      .find((owner) => owner),
    commentsSender: [
      ...new Set(
        owners
          .filter((owner) => owner.usageCode === 'SENDER' && !!owner.comments)
          .map((owner) => owner.comments)
      ),
    ].join(', '),
    clerkNameSigner: [
      ...new Set(
        owners
          .filter((owner) => owner.usageCode === 'SIGNER' && !!owner.clerkName)
          .map((owner) => owner.clerkId)
      ),
    ],
    clerkNameExe: owners
      .filter((owner) => owner.usageCode === 'EXE' && !!owner.clerkName)
      .map((owner) => ({
        id: owner.id,
        clerkId: owner.clerkId,
        clerkName: owner.clerkName,
        divisionId: owner.divisionId,
      }))
      .find((owner) => owner),
  };
};

export const userToClerkAdapter = (user: UserInfo): ClerkForFormType => ({
  clerkId: user?.clerkId ?? 0,
  clerkName: user?.personFio ?? '',
  divisionId: user?.divisionId ?? 0,
  id: user?.personId ?? 0,
});

export const includedFilesToRowAdapter = (
  files: DocumentIncludedFileType[]
) => {
  const id = Date.now() + Math.random();

  return {
    id,
    name: 'Файлы сопровождаемых документов',
    edit: '',
    date: '',
    link: false,
    selected: false,
    children: [
      ...files.map((file) => ({
        id: file.id,
        name: file.name,
        edit: file.lastChangeBy,
        date: file.lastChangeDate,
        link: true,
        selected: false,
        parentId: id,
      })),
    ],
  };
};

export const filesToTableAdapter = (
  files?: Array<DocumentFileType>
): { cols: Column<TreeRow<any>>[]; rows: any } => {
  if (!files) {
    return {
      cols: [
        {
          key: 'name',
          name: 'Имя файла',
        },
        {
          key: 'edit',
          name: 'Кем изменен',
        },
        {
          key: 'date',
          name: 'Дата последнего изменения',
        },
      ],
      rows: [],
    };
  }
  return {
    cols: [
      {
        key: 'name',
        name: 'Имя файла',
        formatter: ({ row }: any) => {
          if (row.link) {
            return <FileLink name={row.name} rowId={row.id} />;
          }

          return row.name;
        },
      },
      {
        key: 'edit',
        name: 'Кем изменен',
      },
      {
        key: 'date',
        name: 'Дата последнего изменения',
      },
    ],
    rows: [
      ...files.map((file) => ({
        id: file.id,
        name: file.name,
        edit: file.lastChangeBy,
        date:
          file.lastChangeDate &&
          new Date(file.lastChangeDate).toLocaleDateString(),
        link: true,
        selected: false,
      })),
    ],
  };
};

export const receiversToTableAdapter = (receivers: Array<any>) => {
  return {
    cols: documentReceiversCols,
    rows: [
      ...receivers.map((receiver) => ({
        id: receiver.id,
        contractorName: receiver.contractorName,
        contractorContactPerson: receiver.contractorContactPerson,
        contractorAddressDeliveryType: receiver.contractorAddressDeliveryType,
        contractorContactInfo: receiver.contractorContactInfo,
        contractorAddressPostIndex: receiver.contractorAddressPostIndex,
        contractorAddress: receiver.contractorAddress,
        packetOutgoingSendDate: receiver.packetOutgoingSendDate,
        packetPostNotificationNumber: receiver.packetPostNotificationNumber,
        packetOutgoingDeliveryDate: receiver.packetOutgoingDeliveryDate,
        packetPostReturnDate: receiver.packetPostReturnDate,
        comments: receiver.comments,
        packetOutgoingStatus: receiver.packetOutgoingStatus,
        packetOutgoingPagesCount: receiver.packetOutgoingPagesCount,
        link: true,
        selected: false,
      })),
    ],
  };
};

export const formDataForSaveAdapter = (
  formData: any,
  formik: ReturnType<typeof useFormik> | FormikValues
): UpdateOutputDataType => {
  const isOutcoming = formData.objectName.includes('OUTCOMING');

  return {
    id: formik.values.id,
    title: formData.title,
    typeId: formData.typeId,
    classCode: formData.classCode,
    comments: formik.values.comments,
    number: formik.values.number,
    date: formik.values.date,
    rubricId: formData.rubricId ?? formik.values.rubricList[0].id,
    outerNumber: formData.outerNumber,
    signerClerkId: formData.signerClerkId,
    recipientInternalId: formData.recipientInternalId,
    extendedParameters: null,
    objectTypeCode: formData.objectTypeCode,
    objectId: formData.objectId,
    documentAccessCode: formik.values.documentAccess.code,
    isControl: formik.values.isControl,
    isUrgent: formData.isUrgent,
    deliveryTypeCode: formData.deliveryTypeCode,
    description: formik.values.description,
    compound: formik.values.compound,
    nulls: null,
    divisionId: formData.divisionId,
    referencePoint: formData.referencePoint,
    templateId: formik.values.maket.id,
    isManualSign: formData.isManualSign,
    isActual: formik.values.isActual,
    toBeControlled: formData.toBeControlled,
    isConfidential: formik.values.isConfidential,
    moKzId: formData.moKzId,
    externalDate: formData.externalDate,
    isEds: formik.values.isEds,
    signerIds: formik.values.clerkNameSigner.map(
      (signer: any) => signer.clerkId
    ),
    [`${isOutcoming ? 'outer' : 'inner'}Addressees`]: isOutcoming
      ? formik.values.clerkNameReceiver.map((receiver: any) => ({
          id: receiver.id,
          contractorId: receiver.contractorId,
          contractorContactPersonId: receiver.contractorContactPersonId,
          comments: receiver.comments,
          contractorDeliveryId: receiver.contractorDeliveryId,
          contractorAddressDeliveryTypeCode:
            receiver.contractorAddressDeliveryTypeCode,
          contractorContactId: receiver.contractorContactId,
          packetOutgoingPagesCount: receiver.packetOutgoingPagesCount,
        }))
      : formik.values.clerkNameReceiver.map((receiver: any) => ({
          id: receiver.id,
          divisionId: receiver.divisionId,
          clerkId: receiver.clerkId,
          signOrder: receiver.signOrder,
          copyId: receiver.copyId,
        })),
    performer: {
      id: formik.values.clerkNameExe?.id,
      divisionId: formik.values.clerkNameExe?.divisionId,
      clerkId: formik.values.clerkNameExe?.clerkId,
    },
    registrar: {
      id: formik.values.clerkNameRegister?.id,
      divisionId: formik.values.clerkNameRegister?.divisionId,
      clerkId: formik.values.clerkNameRegister?.clerkId,
    },
    rubricIds: formik.values.rubricList.map((rubric: any) => rubric.id),
  };
};

const createPredpr = (
  divisionId: number,
  formik: ReturnType<typeof useFormik> | FormikValues
) => ({
  id: null,
  title: '',
  typeId: 23,
  classCode: null,
  comments: formik.values.comments,
  number: formik.values.number,
  date: null,
  outerNumber: null,
  recipientInternalId: null,
  extendedParameters: null,
  objectTypeCode: 'DOCS',
  objectId: null,
  documentAccessCode: formik.values.documentAccess.code,
  isControl: formik.values.isControl,
  isUrgent: false,
  deliveryTypeCode: '',
  description: formik.values.description,
  compound: formik.values.compound,
  nulls: null,
  divisionId,
  referencePoint: null,
  templateId: formik.values.maket.id,
  isManualSign: false,
  isActual: formik.values.isActual,
  toBeControlled: false,
  isConfidential: formik.values.isConfidential,
  moKzId: null,
  externalDate: null,
  isEds: formik.values.isEds,
  signerIds: formik.values.clerkNameSigner.map((signer: any) => signer.clerkId),
  outerAddresses: [],
  innerAddresses: formik.values.clerkNameReceiver.map((receiver: any) => ({
    clerkId: receiver.clerkId,
  })),
  performer: {
    clerkId: formik.values.clerkNameExe?.clerkId,
  },
  registrar: {
    clerkId: formik.values.clerkNameRegister?.clerkId,
  },
  rubricIds: formik.values.rubricList.map((rubric: any) => rubric.id),
  externalProcedure: false,
  recipientExternal: null,
  parameters: null,
  requestIds: [],
  formName: '',
  followedDocuments: [],
  methodOfCreation: 'BY_TEMPLATE',
});

const createDRKK = (
  divisionId: number,
  formik: ReturnType<typeof useFormik> | FormikValues
) => ({
  id: null,
  title: '',
  typeId: 100,
  classCode: null,
  comments: formik.values.comments,
  number: '',
  date: null,
  outerNumber: null,
  recipientInternalId: null,
  extendedParameters: null,
  objectTypeCode: 'DOCS',
  objectId: null,
  documentAccessCode: 'COM' as AccessCodeType,
  isControl: 0 as ControlType,
  isUrgent: false,
  deliveryTypeCode: '',
  description: formik.values.description,
  compound: formik.values.compound,
  nulls: null,
  divisionId,
  referencePoint: null,
  templateId: formik.values.maket.id ?? null,
  isManualSign: false,
  isActual: false,
  toBeControlled: false,
  isConfidential: formik.values.isConfidential,
  moKzId: null,
  externalDate: null,
  isEds: formik.values.isEds,
  signerIds: formik.values.clerkNameSigner.map((signer: any) => signer.clerkId),
  outerAddresses: [],
  innerAddresses: [],
  performer: {
    clerkId: formik.values.clerkNameExe?.clerkId,
  },
  registrar: {
    clerkId: 24491,
  },
  rubricIds: formik.values.rubricList.map((rubric: any) => rubric.id),
  externalProcedure: false,
  recipientExternal: null,
  parameters: null,
  requestIds: [],
  formName: '',
  followedDocuments: [],
  methodOfCreation: 'BY_TEMPLATE',
});

export const formDataForCreateAdapter = (
  data: { divisionId: number; documentType: string },
  formik: ReturnType<typeof useFormik> | FormikValues
): CreateOutputDataType => {
  switch (data.documentType) {
    case 'DOCS_PREDPR':
      return createPredpr(data.divisionId, formik);
    case 'DOCS_OUTCOMING_DRKK':
      return createDRKK(data.divisionId, formik);
    default:
      return {
        id: null,
        title: '',
        typeId: 0,
        classCode: null,
        comments: formik.values.comments,
        number: formik.values.number,
        date: null,
        outerNumber: null,
        recipientInternalId: null,
        extendedParameters: null,
        objectTypeCode: 'DOCS',
        objectId: null,
        documentAccessCode: formik.values.documentAccess.code,
        isControl: formik.values.isControl,
        isUrgent: false,
        deliveryTypeCode: '',
        description: formik.values.description,
        compound: formik.values.compound,
        nulls: null,
        divisionId: data.divisionId,
        referencePoint: null,
        templateId: formik.values.maket.id,
        isManualSign: false,
        isActual: formik.values.isActual,
        toBeControlled: false,
        isConfidential: formik.values.isConfidential,
        moKzId: null,
        externalDate: null,
        isEds: formik.values.isEds,
        signerIds: formik.values.clerkNameSigner.map(
          (signer: any) => signer.clerkId
        ),
        outerAddressees: formik.values.clerkNameReceiver.map(
          (receiver: any) => ({
            contractorId: receiver.contractorId,
            contractorContactPersonId: receiver.contractorContactPersonId,
            comments: receiver.comments,
            contractorDeliveryId: receiver.contractorDeliveryId,
            contractorAddressDeliveryTypeCode:
              receiver.contractorAddressDeliveryTypeCode,
            contractorContactId: receiver.contractorContactId,
            packetOutgoingPagesCount: receiver.packetOutgoingPagesCount,
          })
        ),
        innerAddressees: formik.values.clerkNameReceiver.map(
          (receiver: any) => ({ clerkId: receiver.clerkId })
        ),
        performer: {
          clerkId: formik.values.clerkNameExe?.clerkId,
        },
        registrar: {
          clerkId: formik.values.clerkNameRegister?.clerkId,
        },
        rubricIds: formik.values.rubricList.map((rubric: any) => rubric.id),
        externalProcedure: false,
        recipientExternal: null,
        parameters: null,
        requestIds: [],
        formName: '',
        followedDocuments: [],
        methodOfCreation: 'BY_TEMPLATE',
      };
  }
};

//@todo возможно подумать над универсальным адаптером
export const outcomingDRKKUpdateData = (values: any) => ({
  id: values.id,
  title: values.title,
  typeId: values.typeId,
  classCode: values.classCode,
  comments: values.comments,
  number: values.number,
  date: values.date,
  rubricId: values.rubricId,
  outerNumber: values.outerNumber,
  signerClerkId: values.signerClerkId,
  recipientInternalId: values.recipientInternalId,
  extendedParameters: null,
  objectTypeCode: values.objectTypeCode,
  objectId: values.objectId,
  isUrgent: values.isUrgent,
  deliveryTypeCode: values.deliveryTypeCode,
  description: values.description,
  compound: values.compound,
  nulls: null,
  divisionId: values.divisionId,
  referencePoint: values.referencePoint,
  templateId: values.templateId,
  isManualSign: values.isManualSign,
  isConfidential: values.isConfidential,
  moKzId: values.moKzId,
  externalDate: values.externalDate,
  isEds: values.isEds,
  signerIds: values.signerIds,
  performer: {
    clerkId: values.performer,
  },
  rubricIds: values.rubricList,
});

export const outcomingDRKKCreateData = (divisionId: number, values: any) => ({
  id: null,
  title: '',
  typeId: 100,
  classCode: null,
  comments: values.comments,
  number: '',
  date: null,
  outerNumber: null,
  recipientInternalId: null,
  extendedParameters: null,
  objectTypeCode: 'DOCS',
  objectId: null,
  documentAccessCode: 'COM',
  isControl: 0,
  isUrgent: false,
  deliveryTypeCode: '',
  description: values.description,
  compound: values.compound,
  nulls: null,
  divisionId,
  referencePoint: null,
  templateId: values.templateId ?? null,
  isManualSign: false,
  isActual: false,
  toBeControlled: false,
  isConfidential: values.isConfidential,
  moKzId: null,
  externalDate: null,
  isEds: values.isEds,
  signerIds: values.signerIds,
  outerAddresses: [],
  innerAddresses: [],
  performer: {
    clerkId: values.performer,
  },
  rubricIds: values.rubricList,
  externalProcedure: false,
  recipientExternal: null,
  parameters: null,
  requestIds: [],
  formName: '',
  followedDocuments: [],
  methodOfCreation: null,
});

export const outcomingCreateAdapter = (data: any) => ({
  typeId: 21,
  comments: data.comments,
  documentAccessCode: data.documentAccessCode,
  description: data.description,
  compound: data.compound,
  templateId: data.templateId,
  isConfidential: data.isConfidential,
  isEds: data.isEds,
  signerIds: data.signerIds,
  outerAddressees: data.outerAddressees?.map((el: any) => ({
    // ...el,
    id: typeof el.id === 'number' ? el.id : null,
    contractorId: el.contractorId,
    contractorContactPersonId: el.contractorContactPersonId,
    comments: el.comments,
    contractorDeliveryId: el.contractorDeliveryId,
    contractorAddressDeliveryTypeCode: el.contractorAddressDeliveryTypeCode,
    contractorContactId: el.contractorContactId,
    packetOutgoingPagesCount: el.packetOutgoingPagesCount,
  })),
  // outerAddressees: [
  //   {
  //     "contractorId" : 108738,
  //     "contractorContactPersonId" : 380880,
  //     "comments" : "яяя",
  //     "contractorDeliveryId" : 14974,
  //     "contractorAddressDeliveryTypeCode" : "MAN",
  //     "contractorContactId" : 123600,
  //     "packetOutgoingPagesCount" : 2
  //   }
  // ],
  performer: {
    clerkId: data.performer,
  },
  rubricIds: data.rubricList,
});

export const outcomingUpdateAdapter = (data: any) => ({
  typeId: 21,
  comments: data.comments,
  documentAccessCode: data.documentAccessCode,
  description: data.description,
  compound: data.compound,
  templateId: data.templateId,
  isConfidential: data.isConfidential,
  isEds: data.isEds,
  signerIds: data.signerIds,
  outerAddressees: data.outerAddressees?.map((el: any) => ({
    // ...el,
    id: !el.initialId
      ? typeof el.id === 'number'
        ? el.id
        : null
      : el.initialId,

    contractorId: el.contractorId,
    contractorContactPersonId: el.contractorContactPersonId,
    comments: el.comments || null,
    contractorDeliveryId: el.contractorDeliveryId,
    contractorAddressDeliveryTypeCode: el.contractorAddressDeliveryTypeCode,
    contractorContactId: el.contractorContactId,
    packetOutgoingPagesCount: el.packetOutgoingPagesCount || null,
  })),
  // outerAddressees: [
  //   {
  //     "contractorId" : 108738,
  //     "contractorContactPersonId" : 380880,
  //     "comments" : "яяя",
  //     "contractorDeliveryId" : 14974,
  //     "contractorAddressDeliveryTypeCode" : "MAN",
  //     "contractorContactId" : 123600,
  //     "packetOutgoingPagesCount" : 2
  //   }
  // ],
  performer: {
    clerkId: data.performer,
  },
  rubricIds: data.rubricList,
});

export const boundsToRowAdapter = (
  bounds: BoundsFiles[]
): { cols: Column<TreeRow<any>>[]; rows: any } => {
  const rows = bounds.reduce((acc, bound, i) => {
    let files: any = [];
    const row: TreeRow = {
      id: bound.id,
      name: bound.name,
      docId: bound.docId,
    };

    const children: TreeRow['children'] =
      bound.files && bound.files.length > 0
        ? bound.files.map((file: DocumentFileType) => ({
            id: file.id,
            name: file.name,
            parentId: bound.id,
            link: true,
          }))
        : [];

    if (bound.includedFiles && bound.includedFiles.length > 0) {
      const tempId = `f${bound.id}`;

      children.push({
        id: tempId,
        parentId: bound.id,
        name: 'Файлы сопровождаемых документов',
      });

      files = bound.includedFiles?.map((file: DocumentIncludedFileType) => ({
        id: file.id,
        parentId: tempId,
        name: file.name,
        link: true,
      }));
    }

    return [...acc, row, ...children, ...files];
  }, [] as any);

  return {
    cols: [
      {
        key: 'name',
        name: 'Связки с документами',
        formatter: ({ row }) => {
          if (row.link) {
            return (
              <div style={{ marginLeft: (row.groupLevel - 1) * 32 }}>
                <FileLink name={row.name} rowId={row.id} />
              </div>
            );
          }

          return row.name;
        },
      },
    ],
    rows: rows,
  };
};

export const requestsToRowAdapter = (requests?: DocumentRequestsType[]) => {
  if (!requests) {
    return {
      cols: [
        {
          key: 'number',
          name: 'Номер',
        },
        {
          key: 'clientName',
          name: 'Заявитель',
        },
        {
          key: 'service',
          name: 'Радиослужба',
        },
        {
          key: 'tecnology',
          name: 'Технология',
        },
        {
          key: 'manager',
          name: 'Менеджер',
        },
        {
          key: 'channelList',
          name: 'Частота/Канал',
        },
        {
          key: 'status',
          name: 'Статус',
        },
        {
          key: 'regionList',
          name: 'Регион',
        },
        {
          key: 'resAddress',
          name: 'Пункт установки',
        },
        {
          key: 'country',
          name: 'Страна',
        },
      ],
      rows: [],
    };
  }
  return {
    cols: [
      {
        key: 'number',
        name: 'Номер',
      },
      {
        key: 'clientName',
        name: 'Заявитель',
      },
      {
        key: 'service',
        name: 'Радиослужба',
      },
      {
        key: 'tecnology',
        name: 'Технология',
      },
      {
        key: 'manager',
        name: 'Менеджер',
      },
      {
        key: 'channelList',
        name: 'Частота/Канал',
      },
      {
        key: 'status',
        name: 'Статус',
      },
      {
        key: 'regionList',
        name: 'Регион',
      },
      {
        key: 'resAddress',
        name: 'Пункт установки',
      },
      {
        key: 'country',
        name: 'Страна',
      },
    ],
    rows: requests.map((req) => {
      return {
        id: req.id,
        number: req.number,
        clientName: req.clientName,
        service: req.service,
        tecnology: req.technology,
        manager: req.manager,
        channelList: req.channelList,
        status: req.status,
        regionList: req.regionList,
        resAddress: req.resAddress,
        country: req.country,
      };
    }),
  };
};

export const FollowedDocumentCols = [
  {
    key: 'followedDocumentId',
    name: 'Штрих-код',
    hidden: false,
  },
  {
    key: 'сreateDate',
    name: 'Дата создания',
    hidden: false,
  },
  {
    key: 'signDate',
    name: 'Дата подписи',
    hidden: false,
  },
  {
    key: 'signerNameList',
    name: 'Подписал',
    hidden: false,
  },
  {
    key: 'performerNameList',
    name: 'Исполнитель',
    hidden: false,
  },
  {
    key: 'typeName',
    name: 'Тип',
    hidden: false,
  },
  {
    key: 'number',
    name: 'Номер',
    hidden: false,
  },
  {
    key: 'numberLocal',
    name: 'Внутренний номер',
    hidden: false,
  },
  {
    key: 'rubricList',
    name: 'Рубрики',
    hidden: false,
  },
  {
    key: 'composition',
    name: 'Состав',
    hidden: false,
  },
  {
    key: 'description',
    name: 'Содержание',
    hidden: false,
  },
  {
    key: 'comment',
    name: 'Примечание',
    hidden: false,
  },
];

export const followedToRowAdapter = (followed?: DocumentFollowedType[]) => {
  if (!followed) {
    return { rows: [] };
  }
  return {
    rows: followed.map((row) => ({
      id: row.id,
      followedDocumentId: row.followedDocumentId,
      сreateDate: row.сreateDate,
      signDate: row.signDate,
      signerNameList: row.signerNameList,
      performerNameList: row.performerNameList,
      typeName: row.typeName,
      number: row.number,
      numberLocal: row.numberLocal,
      rubricList: row.rubricList,
      composition: row.composition,
      description: row.description,
      comment: row.comment,
    })),
  };
};

export const sortByLabel = (arr: Option[]) => {
  if (!arr.length) return [];

  const sorted = [...arr];

  sorted.sort((a, b) => {
    if (a.label > b.label) return 1;
    if (a.label < b.label) return -1;
    return 0;
  });

  return sorted;
};

// @todo проверить на актуальность
export const idClerkFields = (data: Array<ClerkReceiverType>) => {
  return data?.map((elem) => elem.clerkId);
};

export const idRubricFields = (data: Array<DocumentRubricType> | undefined) => {
  return data?.map((elem) => elem.id) || [];
};

export const optionsClerkFields = (data: Array<ClerkReceiverType>) => {
  return data?.map((elem) => {
    return { value: elem.clerkId, label: elem.clerkName };
  });
};

export const optionsRubricFields = (
  data: Array<DocumentRubricType> | undefined
) => {
  return (
    data?.map((elem) => {
      return { value: elem.id, label: elem.name };
    }) || []
  );
};

export const prepareFieldsData = (
  newDoc: boolean,
  values: CommonDocumentType,
  dictionariesClerks: DictionariesClerkType[],
  documentOwners?: DocumentOwnerType[]
) => {
  const getClerkData = (id: number) => {
    const res = dictionariesClerks.find((clerk) => clerk.personId === id);
    if (res) {
      return newDoc
        ? { clerkId: res.id }
        : {
            id: res.personId,
            divisionId: res.divisionId,
            clerkId: res.id,
          };
    }
  };

  const getInnerAddresses = () => {
    const ownersData = documentOwners
      ? documentOwners
          .filter(
            (clerk) =>
              values.clerkNameReceiver.includes(clerk.personId) &&
              clerk.usageCode === 'RECEIVER'
          )
          .map(({ clerkId, divisionId, id, signOrder, copyId }) => {
            return {
              id: id,
              divisionId: divisionId,
              clerkId: clerkId,
              signOrder: signOrder,
              copyId: copyId,
            };
          })
      : [];
    const clerksData = dictionariesClerks
      ?.filter(
        (clerk) =>
          values.clerkNameReceiver.includes(clerk.personId) &&
          !ownersData.some((obj) => obj.clerkId === clerk.id)
      )
      .map(({ divisionId, id }) => {
        return id
          ? {
              clerkId: id,
            }
          : {
              divisionId,
            };
      });

    return [...ownersData, ...clerksData];
  };

  return {
    id: values.id,
    title: values.title,
    typeId: values.typeId,
    classCode: values.classCode,
    comments: values.comments || '',
    number: values.number,
    date: values.date || '',
    outerNumber: values.outerNumber,
    recipientInternalId: values.recipientInternalId,
    extendedParameters: null,
    objectTypeCode: values.objectTypeCode,
    objectId: values.objectId,
    documentAccessCode: values.documentAccessCode,
    isControl: values.isControl,
    isUrgent: values.isUrgent,
    deliveryTypeCode: values.deliveryTypeCode,
    description: values.description,
    compound: values.compound,
    nulls: null,
    divisionId: values.divisionId,
    referencePoint: values.referencePoint,
    templateId: values.templateId,
    isManualSign: values.isManualSign,
    isActual: values.isActual,
    toBeControlled: values.toBeControlled,
    isConfidential: values.isConfidential,
    moKzId: values.moKzId,
    externalDate: values.externalDate,
    isEds: values.isEds,
    signerIds: dictionariesClerks
      .filter((clerk) => values.clerkNameSigner.includes(clerk.personId))
      .map((clerk) => clerk.id),
    rubricIds: values.rubricList,

    rubricId: values.rubricId ?? +values.rubricList[0],
    signerClerkId: values.signerClerkId,
    innerAddressees: getInnerAddresses(),
    performer: getClerkData(values.clerkNameExe),
    registrar: getClerkData(values.clerkNameRegister),
  };
};

export const protocolToRowsAdapter = (data?: DocumentProtocolType[]) => {
  if (!data) {
    return [];
  }

  return data.map((row) => ({
    id: row.id,
    type: row.type,
    date: row.date
      ? format(new Date(row.date), 'dd.MM.yyyy \xa0\xa0\xa0 hh:mm')
      : '',
    shortReport: row.shortReport,
    information: row.information,
    clerkShortName: row.clerkShortName,
  }));
};

export const sightingsToTableAdapter = (
  sightings?: Array<DocumentSightingsType>
): { cols: Column<TreeRow<any>>[]; rows: any } => {
  if (!sightings) {
    return {
      cols: [
        {
          key: 'author',
          name: 'Исполнитель',
        },
        {
          key: 'state',
          name: 'Состояние',
        },
        {
          key: 'date',
          name: 'Дата выполнения задачи',
        },
        {
          key: 'comment',
          name: 'Комментарий исполнителя',
        },
      ],
      rows: [],
    };
  }
  return {
    cols: [
      {
        key: 'author',
        name: 'Исполнитель',
      },
      {
        key: 'state',
        name: 'Состояние',
      },
      {
        key: 'date',
        name: 'Дата выполнения задачи',
      },
      {
        key: 'comment',
        name: 'Комментарий исполнителя',
      },
    ],
    rows: [
      ...sightings.map((sighting) => ({
        author: sighting.performerFio,
        state: sighting.taskStatusName,
        date: sighting.taskDateComplete,
        comment: sighting.taskPerformerComment,
      })),
    ],
  };
};

export const deliveryToRowsAdapter = (data?: DocumentDeliveryType[]) => {
  if (!data) {
    return [];
  }

  return data.map((row) => ({
    id: row.id,
    localNumber: row.localNumber,
    clientShortName: row.clientShortName,
    clientFullName: row.clientFullName,
    ownType: row.ownType,
    index: row.index,
    address: row.address,
    email: row.email,
    fax: row.fax,
    recipientPersonName: row.recipientPersonName,
    deliveryTypeName: row.deliveryTypeName,
    status: row.status,
    sendingDate:
      row.sendingDate && new Date(row.sendingDate).toLocaleDateString(),
    deliveryDate:
      row.deliveryDate && new Date(row.deliveryDate).toLocaleDateString(),
    comment: row.comment,
    returnPostPacketeDate:
      row.returnPostPacketeDate &&
      new Date(row.returnPostPacketeDate).toLocaleDateString(),
    trustedPersonName: row.trustedPersonName,
    notificationNumber: row.notificationNumber,
    documentDate:
      row.documentDate && new Date(row.documentDate).toLocaleDateString(),
    signDate: row.signDate && new Date(row.signDate).toLocaleDateString(),
    countSendingOutgoingPacket: row.countSendingOutgoingPacket,
  }));
};

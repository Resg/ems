import { ObjectTask } from '../types/tasks';

export const CreateTemplateTree = (rows: ObjectTask[], selectedId: number) => {
  const selectedParent = rows.find((row) => row.id === selectedId);

  const findChildren = (parentId: number) => {
    return rows.filter((document) => document.parentId === parentId);
  };

  if (selectedParent) {
    const firstChildren = findChildren(selectedParent.id);
    const unPreparedArray = [selectedParent, ...firstChildren];
    firstChildren.forEach((child) =>
      unPreparedArray.push(...findChildren(child.id))
    );

    for (let index = 0; index < unPreparedArray.length; index++) {
      const id = unPreparedArray[index].id;

      for (
        let innerIndex = 0;
        innerIndex < unPreparedArray.length;
        innerIndex++
      ) {
        if (unPreparedArray[innerIndex].parentId === id) {
          unPreparedArray[innerIndex] = {
            ...unPreparedArray[innerIndex],
            temporaryParentId: index + 1,
          };
        }
        if (unPreparedArray[innerIndex].previousId === id) {
          unPreparedArray[innerIndex] = {
            ...unPreparedArray[innerIndex],
            temporaryPreviousId: index + 1,
          };
        }
      }
      unPreparedArray[index] = {
        ...unPreparedArray[index],
        temporaryId: index + 1,
      };
    }

    const result = unPreparedArray.map((document) => ({
      ...document,
      temporaryObjectId: null,
      id: undefined,
      previousId: undefined,
      parentId: undefined,
      objectId: undefined,
    }));

    return JSON.stringify(result);
  }
};

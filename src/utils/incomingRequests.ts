import {
  IIncomingRequestColumn,
  IIncomingRequestRow,
  IRequest,
} from '../types/incomingRequests';

const documentForUIExample = {
  type: 'Тип задачи',
  author: 'Автор',
  status: 'Статус',
  planDate: 'Срок исполнения',
  parentType: 'Задача',
  requestNumber: 'Номер заявки',
  requestDate: 'Дата',
  requestContractor: 'Заявитель',
  requestRegionList: 'Регион',
  requestTechnology: 'Технология',
  requestManager: 'Менеджер',
  requestStatus: 'Последний этап',
  requestParallelStages: 'Параллельные этапы',
  comments: 'Комментарий',
  requestCommentsForRfg: 'Комментарий для РФГ',
  id: 'ИД задачи',
};

export const makeIncomingDocumentTasksColumns =
  (): Array<IIncomingRequestColumn> =>
    Object.getOwnPropertyNames(documentForUIExample).map((key) => ({
      key: key as Exclude<keyof IIncomingRequestRow, 'id' | 'selected'>,
      name: documentForUIExample[
        key as Exclude<keyof IIncomingRequestRow, 'id' | 'selected'>
      ],
      hidden: false,
    }));

export const makeExpandedGroups = (
  rows: Array<IIncomingRequestRow>
): Array<string> => {
  const nonUnique: Array<string> = rows?.map((row) => row.type);
  return [...new Set(nonUnique)];
};

export const tasksForUIAdapter = (
  tasks: Array<IRequest>
): Array<IIncomingRequestRow> =>
  tasks.map((task) => ({
    id: task.id,
    type: task.type,
    author: task.author,
    status: task.status,
    planDate: task.planDate && new Date(task.planDate).toLocaleDateString(),
    parentType: task.parentType,
    requestNumber: task.requestNumber,
    requestDate:
      task.requestDate && new Date(task.requestDate).toLocaleDateString(),
    requestId: task.requestId,
    requestContractor: task.requestContractor,
    requestRegionList: task.requestRegionList,
    requestTechnology: task.requestTechnology,
    requestManager: task.requestManager,
    requestStatus: task.requestStatus,
    requestParallelStages: task.requestParallelStages,
    comments: task.comments,
    requestCommentsForRfg: task.requestCommentsForRfg,
    selected: false,
  }));

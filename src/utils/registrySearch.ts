import { FormValues } from '../components/FilterBarNew';

export const prepareData = (values: FormValues) => {
  const cleanValue = {
    authorDivisions:
      values.filters.authorDivisions && values.filters.authorDivisions.length
        ? values.filters.authorDivisions
        : undefined,
    authorClerks:
      values.filters.authorClerks && values.filters.authorClerks.length
        ? values.filters.authorClerks
        : undefined,
    targetDivisions:
      values.filters.targetDivisions && values.filters.targetDivisions.length
        ? values.filters.targetDivisions
        : undefined,
    targetClerks:
      values.filters.targetClerks && values.filters.targetClerks.length
        ? values.filters.targetClerks
        : undefined,
    isArchive: undefined,
    states: undefined,
    isMyRegistry: values.filters.isMyRegistry || undefined,
    isDocumentRegistry: values.filters.isDocumentRegistry
      ? values.filters.isDocumentRegistry
      : undefined,
    isOutgoingPacketRegistry: values.filters.isOutgoingPacketRegistry
      ? values.filters.isOutgoingPacketRegistry
      : undefined,
    stateCode: values.filters.isArchive
      ? ['R', 'V', 'S']
      : values.filters.stateCode,
  };

  return {
    ...values,
    filters: {
      ...values.filters,
      ...cleanValue,
    },
  };
};

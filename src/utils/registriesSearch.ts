import { RegistrySearchType } from '../types/registries';

export const registriesSearchToRowAdapter = (
  registriesSearch?: RegistrySearchType[]
) => {
  return (
    registriesSearch?.map(
      ({
        id,
        number,
        date,
        sentDateTimeName,
        sentTime,
        receivedDateTimeName,
        receivedTime,
        senderDivision,
        senderName,
        name,
        recipientDivision,
        recipientName,
        type,
        state,
        template,
      }) => ({
        id,
        number,
        date: date && new Date(date).toLocaleDateString(),
        sentDateTimeName,
        sentTime,
        receivedDateTimeName,
        receivedTime,
        senderDivision,
        senderName,
        name,
        recipientDivision,
        recipientName,
        type,
        state,
        template,
      })
    ) || []
  );
};

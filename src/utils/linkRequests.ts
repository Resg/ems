import { LinkRequests } from '../types/linkRequests';

export const linkRequestsToRowAdapter = (linkRequests?: LinkRequests[]) => {
  return (
    linkRequests?.map(
      ({
        id,
        number,
        date,
        clientName,
        manager,
        lastStage,
        lastStageDate,
      }) => ({
        id,
        number,
        date: date && new Date(date).toLocaleDateString(),
        clientName,
        manager,
        lastStage,
        lastStageDate:
          lastStageDate && new Date(lastStageDate).toLocaleDateString(),
      })
    ) || []
  );
};

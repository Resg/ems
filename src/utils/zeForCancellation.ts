import { RequestModalZeForCancellation } from '../types/dictionaries';

export const zeForCancellationModalToRowAdapter = (
  conclusionsModel?: RequestModalZeForCancellation[]
) => {
  return (
    conclusionsModel?.map(
      ({
        id,
        number,
        state,
        status,
        type,
        author,
        date,
        dateValidity,
        dateAnnulment,
      }) => ({
        id,
        number,
        state,
        status,
        type,
        author,
        date: new Date(date).toLocaleDateString(),
        dateValidity: new Date(dateValidity).toLocaleDateString(),
        dateAnnulment: new Date(dateAnnulment).toLocaleDateString(),
      })
    ) || []
  );
};

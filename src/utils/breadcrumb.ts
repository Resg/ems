import { Location } from 'history';

import { Dispatch } from '@reduxjs/toolkit';

import {
  addBreadCrumb,
  BreadCrumb,
  chooseBreadCrumbs,
  resetBreadCrumbs,
} from '../store/utils';

export const getTitle = (titleArr: BreadCrumb[]) => {
  let firstElement, newTitleArr;
  firstElement = titleArr[titleArr.length - 1]?.title;
  newTitleArr = firstElement
    ? [firstElement, 'Главная', 'ИС Экспертиза ЭМС']
    : ['Главная', 'ИС Экспертиза ЭМС'];
  return newTitleArr.join(' - ');
};

export const addBreadCrumbs = (
  location: Location,
  breadCrumbs: any,
  data: any,
  dispatch: Dispatch
) => {
  const key = location.pathname + location.search;

  if (data.data || data.name) {
    if (breadCrumbs.length) {
      if (breadCrumbs.map((crumb: BreadCrumb) => crumb.key).includes(key)) {
        const lengthCut =
          breadCrumbs.map((crumb: BreadCrumb) => crumb.key).indexOf(key) -
          breadCrumbs.length;
        dispatch(chooseBreadCrumbs(lengthCut));
      } else if (
        (breadCrumbs[breadCrumbs.length - 1]?.type &&
          breadCrumbs[breadCrumbs.length - 1].type === `object` &&
          ((location.search &&
            breadCrumbs[breadCrumbs.length - 1].search === location.search) ||
            breadCrumbs[breadCrumbs.length - 1].pathname ===
              location.pathname)) ||
        breadCrumbs[breadCrumbs.length - 1].type === `newObject` ||
        breadCrumbs[breadCrumbs.length - 1].title === data.name
      ) {
        dispatch(chooseBreadCrumbs(-1));
      }
    }
    dispatch(
      addBreadCrumb([
        {
          pathname: location.pathname,
          search: location.search,
          key: key,
          title: data.name,
          type: data.data ? `object` : 'newObject',
        },
      ])
    );
  } else if (data.menuName) {
    dispatch(resetBreadCrumbs());
    dispatch(
      addBreadCrumb(
        data.menuName.map((title: string, index: number) =>
          index !== data.menuName.length - 1
            ? { pathname: '', title }
            : {
                pathname: location.pathname,
                search: location.search,
                title,
                key: location.key,
              }
        )
      )
    );
  } else if (data.singleName) {
    if (breadCrumbs.length) {
      if (breadCrumbs.map((crumb: BreadCrumb) => crumb.key).includes(key)) {
        const lengthCut =
          breadCrumbs.map((crumb: BreadCrumb) => crumb.key).indexOf(key) -
          breadCrumbs.length;
        dispatch(chooseBreadCrumbs(lengthCut));
      } else if (
        breadCrumbs[breadCrumbs.length - 1]?.type &&
        breadCrumbs[breadCrumbs.length - 1].type === `object` &&
        breadCrumbs[breadCrumbs.length - 1].pathname === location.pathname
      ) {
        dispatch(chooseBreadCrumbs(-1));
      }
    }
    dispatch(
      addBreadCrumb([
        {
          pathname: location.pathname,
          search: location.search,
          title: data.singleName,
          key: key,
        },
      ])
    );
  } else if (data.mainPage) {
    dispatch(resetBreadCrumbs());
  }
};

import { FormValues } from '../components/FilterBarNew';

export const prepareData = (values: FormValues) => {
  const frequencyBands = values.filters.frequencyBands?.map((band: any) => ({
    frequencyTransmitFrom: Number(band.frequencyTransmitFrom) || undefined,
    frequencyTransmitTo: Number(band.frequencyTransmitTo) || undefined,
    frequencyReceiveFrom: Number(band.frequencyReceiveFrom) || undefined,
    frequencyReceiveTo: Number(band.frequencyReceiveTo) || undefined,
    koefficientToStandartTransmitFrom: band.frequencyTransmitFrom
      ? band.koefficientToStandartTransmitFrom
      : undefined,
    koefficientToStandartTransmitTo: band.frequencyTransmitTo
      ? band.koefficientToStandartTransmitTo
      : undefined,
    koefficientToStandartReceiveFrom: band.frequencyReceiveFrom
      ? band.koefficientToStandartReceiveFrom
      : undefined,
    koefficientToStandartReceiveTo: band.frequencyReceiveTo
      ? band.koefficientToStandartReceiveTo
      : undefined,
  }));

  const cleanValue = {
    contractorIds:
      values.filters.contractorIds && values.filters.contractorIds.length
        ? values.filters.contractorIds
        : undefined,
    countryIds:
      values.filters.countryIds && values.filters.countryIds.length
        ? values.filters.countryIds
        : undefined,
    regionAoguids:
      values.filters.regionAoguids && values.filters.regionAoguids.length
        ? values.filters.regionAoguids
        : undefined,
    serviceIds:
      values.filters.serviceIds && values.filters.serviceIds.length
        ? values.filters.serviceIds
        : undefined,
    serviceMRFCIds:
      values.filters.serviceMRFCIds && values.filters.serviceMRFCIds.length
        ? values.filters.serviceMRFCIds
        : undefined,
    stageIds:
      values.filters.stageIds && values.filters.stageIds.length
        ? values.filters.stageIds
        : undefined,
    technologyIds:
      values.filters.technologyIds && values.filters.technologyIds.length
        ? values.filters.technologyIds
        : undefined,
    typeIds:
      values.filters.typeIds && values.filters.typeIds.length
        ? values.filters.typeIds
        : undefined,
    isJointUse: values.filters.isJointUse || undefined,
  };

  return {
    ...values,
    filters: {
      ...values.filters,
      frequencyBands,
      ...cleanValue,
    },
  };
};

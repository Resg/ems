import {
  ClerkForFormType,
  DictionariesAccessType,
  DictionariesClerkType,
  DictionariesRubricType,
  DictionariesTemplateType,
} from '../types/dictionaries';

export const rubricsToFormAdapter = (
  rubrics?: { id: number; name: string }[]
): { value: number; label: string }[] =>
  rubrics?.length
    ? rubrics
        .filter((rubric) => !!rubric.name)
        .map((rubric) => ({ value: rubric.id, label: rubric.name }))
    : [];

export const accessesToOptionsAdapter = (
  dictionaries: Array<DictionariesAccessType>
): Array<{ code: string; name: string }> =>
  dictionaries
    .filter((dictionary) => !!dictionary.name)
    .map((dictionary) => ({
      code: dictionary.code,
      name: dictionary.name,
    }));

export const rubricsToOptionsAdapter = (
  rubrics: Array<DictionariesRubricType>
): Array<{ id: number; name: string }> =>
  rubrics
    .filter((rubric) => !!rubric.name)
    .map((rubric) => ({ id: rubric.id, name: rubric.name }));

export const templatesToOptionsAdapter = (
  templates: Array<DictionariesTemplateType>
): Array<{ value: number; label: string }> =>
  templates.map((template) => ({
    value: template.id,
    label: template.description,
  }));

export const clerksToOptionsAdapter = (
  clerks: Array<DictionariesClerkType>
): Array<ClerkForFormType> =>
  clerks
    .filter((clerk) => !!clerk.personFio)
    .map((clerk) => ({
      id: clerk.personId,
      clerkId: clerk.id,
      clerkName: clerk.personFio,
      divisionId: clerk.divisionId,
    }));

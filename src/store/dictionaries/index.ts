import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';

import {
  DictionariesAccessType,
  DictionariesClerkType,
  DictionariesRubricType,
  DictionariesTemplateType,
} from '../../types/dictionaries';

export interface DictionariesState {
  // areDictionariesFetching: boolean;
  dictionariesAccesses: DictionariesAccessType[];
  dictionariesRubrics: DictionariesRubricType[];
  dictionariesTemplates: DictionariesTemplateType[];
  dictionariesClerks: DictionariesClerkType[];
}

const initialState: DictionariesState = {
  // areDictionariesFetching: false,
  dictionariesAccesses: [],
  dictionariesRubrics: [],
  dictionariesTemplates: [],
  dictionariesClerks: [],
};

const dictionariesSlice = createSlice({
  name: 'dictionaries',
  initialState,
  reducers: {
    setDictionariesAccesses: (
      state,
      action: PayloadAction<DictionariesAccessType[]>
    ) => {
      state.dictionariesAccesses = action.payload;
    },
    setDictionariesRubrics: (
      state,
      action: PayloadAction<DictionariesRubricType[]>
    ) => {
      state.dictionariesRubrics = action.payload;
    },
    setDictionariesTemplates: (
      state,
      action: PayloadAction<DictionariesTemplateType[]>
    ) => {
      state.dictionariesTemplates = action.payload;
    },
    setDictionariesClerks: (
      state,
      action: PayloadAction<DictionariesClerkType[]>
    ) => {
      state.dictionariesClerks = action.payload;
    },
  },
});

export const {
  setDictionariesAccesses,
  setDictionariesClerks,
  setDictionariesRubrics,
  setDictionariesTemplates,
} = dictionariesSlice.actions;

export default dictionariesSlice.reducer;

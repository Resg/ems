import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { DefaultColumn } from '../../components/CustomDataGrid/types';
import {
  ColumnSetting,
  Sort,
  UserTableSettings,
} from '../../types/userTableSettings';
import { RootState } from '../index';

export type DataType = { [key: string]: any };
export interface Tool {
  name: string;
  checked: boolean;
  disabled?: boolean;
}

export interface UserTableSettingsState {
  userTableSettingsInfo: { [formKey: string]: UserTableSettings };
  colls: { [formKey: string]: DataType[] };
  groups: { [formKey: string]: string[] };
  tools: { [formKey: string]: Tool[] };
  sorts: { [formKey: string]: any[] };
}

const initialState: UserTableSettingsState = {
  userTableSettingsInfo: {},
  colls: {},
  groups: {},
  tools: {},
  sorts: {},
};
const userTableSettingsSlice = createSlice({
  name: 'userTableSettings',
  initialState,
  reducers: {
    setUserTableSettings: (
      state,
      action: PayloadAction<{ formKey?: string; data: UserTableSettings }>
    ) => {
      const { formKey, data } = action.payload;
      if (formKey) {
        state.userTableSettingsInfo[formKey] = data;
      }
    },
    setDataColls: (
      state,
      action: PayloadAction<{
        formKey?: string;
        data: ColumnSetting[] | DefaultColumn[];
      }>
    ) => {
      const { formKey, data } = action.payload;
      if (formKey) {
        state.colls[formKey] = data;
      }
    },
    setDataGroups: (
      state,
      action: PayloadAction<{ formKey?: string; data: string[] }>
    ) => {
      const { formKey, data } = action.payload;
      if (formKey) {
        state.groups[formKey] = data;
      }
    },
    setDataTools: (
      state,
      action: PayloadAction<{ formKey?: string; data: Tool[] }>
    ) => {
      const { formKey, data } = action.payload;
      if (formKey) {
        state.tools[formKey] = data;
      }
    },
    setDataSorts: (
      state,
      action: PayloadAction<{ formKey?: string; data: Sort[] }>
    ) => {
      const { formKey, data } = action.payload;
      if (formKey) {
        state.sorts[formKey] = data;
      }
    },
  },
});

export const {
  setUserTableSettings,
  setDataColls,
  setDataGroups,
  setDataTools,
  setDataSorts,
} = userTableSettingsSlice.actions;

export const getUserTableSettings = (state: RootState, formKey?: string) => {
  if (formKey) {
    return state.userTableSettings.userTableSettingsInfo[formKey];
  }
};

export const getDataColls = (state: RootState, formKey?: string) => {
  if (formKey) {
    return state.userTableSettings.colls[formKey];
  }
};

export const getDataGroups = (state: RootState, formKey?: string) => {
  if (formKey) {
    return state.userTableSettings.groups[formKey];
  }
};

export const getDataTools = (state: RootState, formKey?: string) => {
  if (formKey) {
    return state.userTableSettings.tools[formKey];
  }
};

export const getDataSorts = (state: RootState, formKey?: string) => {
  if (formKey) {
    return state.userTableSettings.sorts[formKey];
  }
};

export default userTableSettingsSlice.reducer;

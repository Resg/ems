import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';

import { EmployeeData } from '../../types/employee';

export interface BreadCrumb {
  pathname: string;
  search?: string;
  title?: string;
  type?: string;
  key?: string;
}

export interface ErrorData {
  login?: string;
  location?: string;
  url?: string;
  status?: string | number;
  body?: string;
  response?: string;
  message: string;
  showError?: boolean;
}

export interface UserMessageData extends ErrorData {
  type: 'default' | 'success' | 'warning' | 'info';
}

export interface UtilsState {
  sideBarIsOpen: boolean;
  breadCrumbs: BreadCrumb[];
  crumbsFileName: string;
  error?: { id: number; data: ErrorData };
  userMessage?: { id: number; data: UserMessageData };
  employeeModalIsOpen: boolean;
  selectedEmployees: Record<number, boolean>;
  onEmployeeModalSubmit?: (ids: number[]) => void;
  isEmployeeModalMultiple: boolean;
  loading: number;
  keyProperty: keyof EmployeeData;
  isResolutionFormOpen: boolean;
  isResolutionCreateFormOpen: boolean;
  isDocumentCreateFormOpen: Record<string, boolean>;
  isDocumentPreviewFormOpen: Record<string, boolean>;
  isDeleteDialogOpen: Record<string | number, boolean>;
  isConclusionAdditionsFormOpen: boolean;
  locationData: any;
  openFormInNewTab: boolean;
  isToolDialogOpen: boolean;
}

const initialState: UtilsState = {
  sideBarIsOpen: true,
  breadCrumbs: JSON.parse(sessionStorage.getItem('bredcrumb') || '[]'),
  crumbsFileName: '',
  employeeModalIsOpen: false,
  selectedEmployees: {},
  isEmployeeModalMultiple: false,
  loading: 0,
  keyProperty: 'id',
  isResolutionFormOpen: false,
  isResolutionCreateFormOpen: false,
  isDocumentCreateFormOpen: {},
  isDocumentPreviewFormOpen: {},
  isDeleteDialogOpen: {},
  isConclusionAdditionsFormOpen: false,
  locationData: {},
  openFormInNewTab: false,
  isToolDialogOpen: false,
};

const utilsSlice = createSlice({
  name: 'utils',
  initialState,
  reducers: {
    setConclusionAdditionsFormOpen: (state, action: PayloadAction<boolean>) => {
      state.isConclusionAdditionsFormOpen = action.payload;
    },
    setDeleteDialogOpen: (
      state,
      action: PayloadAction<Record<string | number, boolean>>
    ) => {
      state.isDeleteDialogOpen = {
        ...state.isDeleteDialogOpen,
        ...action.payload,
      };
    },
    setDocumentCreateFormOpen: (
      state,
      action: PayloadAction<Record<string, boolean>>
    ) => {
      state.isDocumentCreateFormOpen = {
        ...state.isDocumentCreateFormOpen,
        ...action.payload,
      };
    },
    setDocumentPreviewFormOpen: (
      state,
      action: PayloadAction<Record<string, boolean>>
    ) => {
      state.isDocumentPreviewFormOpen = {
        ...state.isDocumentPreviewFormOpen,
        ...action.payload,
      };
    },
    isResolutionFormOpen: (state, action: PayloadAction<boolean>) => {
      state.isResolutionFormOpen = action.payload;
    },
    isResolutionCreateFormOpen: (state, action: PayloadAction<boolean>) => {
      state.isResolutionCreateFormOpen = action.payload;
    },
    setKeyProperty: (state, action: PayloadAction<keyof EmployeeData>) => {
      state.keyProperty = action.payload;
    },
    setLocationData: (state, action: PayloadAction<any>) => {
      state.locationData = action.payload;
    },
    setOpenFormInNewTab: (state, action: PayloadAction<boolean>) => {
      state.openFormInNewTab = action.payload;
    },
    setSideBarIsOpen: (state, action: PayloadAction<boolean>) => {
      state.sideBarIsOpen = action.payload;
    },
    startLoading: (state) => {
      state.loading += 1;
    },
    finishLoading: (state) => {
      state.loading -= 1;
    },
    addBreadCrumb: (state, action: PayloadAction<BreadCrumb[]>) => {
      const newBreadCrumbs = [...state.breadCrumbs, ...action.payload];
      state.breadCrumbs = newBreadCrumbs;
      sessionStorage.setItem('bredcrumb', JSON.stringify(newBreadCrumbs));
    },
    resetBreadCrumbs: (state) => {
      state.breadCrumbs = [];
      sessionStorage.setItem('bredcrumb', JSON.stringify([]));
    },
    chooseBreadCrumbs: (state, action: PayloadAction<number>) => {
      const newBreadCrumbs = state.breadCrumbs.slice(0, action.payload);
      state.breadCrumbs = newBreadCrumbs;
      sessionStorage.setItem('bredcrumb', JSON.stringify([]));
      sessionStorage.setItem('bredcrumb', JSON.stringify([...newBreadCrumbs]));
    },
    setError: (state, action: PayloadAction<ErrorData>) => {
      state.error = { data: action.payload, id: (state.error?.id || 0) + 1 };
    },
    setUserMessage: (state, action: PayloadAction<UserMessageData>) => {
      state.userMessage = {
        data: action.payload,
        id: (state.userMessage?.id || 0) + 1,
      };
    },
    setEmployeeModalIsOpen: (state, action: PayloadAction<boolean>) => {
      state.employeeModalIsOpen = action.payload;
    },
    selectEmployee: (state, action: PayloadAction<number>) => {
      if (state.isEmployeeModalMultiple) {
        state.selectedEmployees = {
          ...state.selectedEmployees,
          [action.payload]: !state.selectedEmployees[action.payload],
        };
      } else {
        state.selectedEmployees = {
          [action.payload]: !state.selectedEmployees[action.payload],
        };
      }
    },
    setOnEmployeeModalSubmit: (
      state,
      action: PayloadAction<(ids: number[]) => void>
    ) => {
      state.onEmployeeModalSubmit = action.payload;
    },
    resetEmployeeModal: (state) => {
      state.selectedEmployees = {};
      state.onEmployeeModalSubmit = undefined;
    },
    setIsModalMultiple: (state, action: PayloadAction<boolean>) => {
      state.isEmployeeModalMultiple = action.payload;
    },
    setSelectedEmployee: (
      state,
      action: PayloadAction<Record<number, boolean>>
    ) => {
      state.selectedEmployees = { ...action.payload };
    },
    setCrumbsFileName: (state, action: PayloadAction<string>) => {
      state.crumbsFileName = action.payload;
    },
    setToolDialogOpen: (state, action: PayloadAction<boolean>) => {
      state.isToolDialogOpen = action.payload;
    },
  },
});

export const {
  setKeyProperty,
  setSideBarIsOpen,
  addBreadCrumb,
  resetBreadCrumbs,
  chooseBreadCrumbs,
  setError,
  setUserMessage,
  setEmployeeModalIsOpen,
  selectEmployee,
  setOnEmployeeModalSubmit,
  resetEmployeeModal,
  setIsModalMultiple,
  setSelectedEmployee,
  startLoading,
  finishLoading,
  isResolutionFormOpen,
  isResolutionCreateFormOpen,
  setDocumentCreateFormOpen,
  setDeleteDialogOpen,
  setConclusionAdditionsFormOpen,
  setCrumbsFileName,
  setDocumentPreviewFormOpen,
  setLocationData,
  setOpenFormInNewTab,
  setToolDialogOpen,
} = utilsSlice.actions;

export default utilsSlice.reducer;

import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';

export interface AdvancedCounterpartySearchState {
  docType: number | null;
}

const initialState: AdvancedCounterpartySearchState = {
  docType: null,
};

const AdvancedCounterpartySearchSlice = createSlice({
  name: 'request',
  initialState,
  reducers: {
    setAdvancedCounterpartySearch: (state, action: PayloadAction<number>) => {
      state.docType = action.payload;
    },
  },
});

export const { setAdvancedCounterpartySearch } =
  AdvancedCounterpartySearchSlice.actions;
export default AdvancedCounterpartySearchSlice.reducer;

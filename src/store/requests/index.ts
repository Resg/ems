import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';

import { RequestStage } from '../../types/requests';
import { RequestSolution } from '../../types/solution';

export interface RequestState {
  stages: RequestStage[];
  solutions: RequestSolution[];
}

const initialState: RequestState = {
  stages: [],
  solutions: [],
};

const requestSlice = createSlice({
  name: 'request',
  initialState,
  reducers: {
    setStages: (state, action: PayloadAction<RequestStage[]>) => {
      state.stages = action.payload;
    },
    setSolutions: (state, action: PayloadAction<RequestSolution[]>) => {
      state.solutions = action.payload;
    },
  },
});

export const { setStages, setSolutions } = requestSlice.actions;
export default requestSlice.reducer;

import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';

import {
  CommonDocumentType,
  DocumentBoundType,
  DocumentFileType,
  DocumentFollowedType,
  DocumentHolderType,
  DocumentIncludedFileType,
  DocumentOwnerType,
  DocumentRequestsType,
  DocumentRubricType,
  DocumentSightingType,
  DocumentsSearchType,
} from '../../types/documents';

export interface DocumentsState {
  currentDocumentData: CommonDocumentType | null;
  isCurrentDocumentFetching: boolean;
  documentOwners: DocumentOwnerType[];
  documentFiles: DocumentFileType[];
  includedFiles: DocumentIncludedFileType[];
  sightings: DocumentSightingType[];
  documentRubrics: Array<DocumentRubricType>;
  holders: DocumentHolderType[];
  bounds: DocumentBoundType[];
  searchDocs: DocumentsSearchType[];
  requests: DocumentRequestsType[];
  followed: DocumentFollowedType[];
}

const initialState: DocumentsState = {
  currentDocumentData: null,
  isCurrentDocumentFetching: false,
  documentOwners: [],
  documentFiles: [],
  includedFiles: [],
  documentRubrics: [],
  sightings: [],
  holders: [],
  bounds: [],
  searchDocs: [],
  requests: [],
  followed: [],
};

const documentSlice = createSlice({
  name: 'documents',
  initialState,
  reducers: {
    setCurrentDocument: (state, action: PayloadAction<CommonDocumentType>) => {
      state.currentDocumentData = action.payload;
    },
    setDocumentOwners: (state, action: PayloadAction<DocumentOwnerType[]>) => {
      state.documentOwners = action.payload;
    },
    setDocumentFiles: (state, action: PayloadAction<DocumentFileType[]>) => {
      state.documentFiles = action.payload;
    },
    setIncludedDocumentFiles: (
      state,
      action: PayloadAction<DocumentIncludedFileType[]>
    ) => {
      state.includedFiles = action.payload;
    },
    setDocumentSighting: (
      state,
      action: PayloadAction<DocumentSightingType[]>
    ) => {
      state.sightings = action.payload;
    },
    setDocumentRubrics: (
      state,
      action: PayloadAction<DocumentRubricType[]>
    ) => {
      state.documentRubrics = action.payload;
    },
    setDocumentHolders: (
      state,
      action: PayloadAction<DocumentHolderType[]>
    ) => {
      state.holders = action.payload;
    },
    setBoundDocs: (state, action: PayloadAction<DocumentBoundType[]>) => {
      state.bounds = action.payload;
    },
    setSearchDocs: (state, action: PayloadAction<DocumentsSearchType[]>) => {
      state.searchDocs = action.payload;
    },
    setDocsRequests: (state, action: PayloadAction<DocumentRequestsType[]>) => {
      state.requests = action.payload;
    },
    setDocsFollowed: (state, action: PayloadAction<DocumentFollowedType[]>) => {
      state.followed = action.payload;
    },
  },
});

export const {
  setCurrentDocument,
  setDocumentOwners,
  setDocumentFiles,
  setDocsFollowed,
  setDocsRequests,
  setSearchDocs,
  setDocumentHolders,
  setDocumentRubrics,
  setDocumentSighting,
  setIncludedDocumentFiles,
  setBoundDocs,
} = documentSlice.actions;

export default documentSlice.reducer;

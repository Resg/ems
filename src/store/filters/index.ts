import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';

import { clearFalsyValues } from '../../utils/filterBar';

export interface FiltersState {
  data: Record<string, any>;
}

const initialState: FiltersState = {
  data: {},
};

const filterSlices = createSlice({
  name: 'filters',
  initialState,
  reducers: {
    setFilters: (state, action: PayloadAction<Record<string, any>>) => {
      state.data = { ...state.data, ...clearFalsyValues(action.payload) };
    },
  },
});

export const { setFilters } = filterSlices.actions;

export default filterSlices.reducer;

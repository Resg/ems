import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';

interface ExecutionProps {
  isEds: boolean;
  statusId: number;
  ids: number[];
  action?: string;
  type?: number;
  setSelected?: (value: any) => void;
}

export interface TasksState {
  isExecutionFormOpen: boolean;
  executionProps: ExecutionProps;
}

const initialState: TasksState = {
  isExecutionFormOpen: false,
  executionProps: {
    isEds: false,
    ids: [],
    statusId: 6,
  },
};

const taskSlice = createSlice({
  name: 'tasks',
  initialState,
  reducers: {
    setOpenExecutionForm: (state, action: PayloadAction<boolean>) => {
      state.isExecutionFormOpen = action.payload;
    },
    setExecutionFormProps: (state, action: PayloadAction<ExecutionProps>) => {
      state.executionProps = action.payload;
    },
  },
});

export const { setOpenExecutionForm, setExecutionFormProps } =
  taskSlice.actions;

export default taskSlice.reducer;

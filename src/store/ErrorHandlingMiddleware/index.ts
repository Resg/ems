import type { Middleware, MiddlewareAPI } from '@reduxjs/toolkit';

import { UserService } from '../../services/auth';
import { setError } from '../utils';

export const ErrorHandlingMiddleware: Middleware =
  (api: MiddlewareAPI) => (next) => (action) => {
    if (action.error && action.error.name !== 'ConditionError') {
      api.dispatch(
        setError({
          login: UserService.getUsername(),
          location: window.location.href,
          url: action.payload?.request.url,
          status: action.payload?.status,
          body: JSON.stringify(
            action.payload?.request.data || action.payload?.request.params
          ),
          response:
            (action.payload?.data?.type || '') +
            ' ' +
            (action.payload?.data?.message || ''),
          message: action.payload?.request.url + ' ' + action.payload?.status,
          showError: action.payload?.showError,
        })
      );
    }

    return next(action);
  };

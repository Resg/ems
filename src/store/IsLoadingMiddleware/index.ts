import type { Middleware, MiddlewareAPI } from '@reduxjs/toolkit';

import { finishLoading, startLoading } from '../utils';

export const IsLoadingMiddleware: Middleware =
  (api: MiddlewareAPI) => (next) => (action) => {
    if (action.meta?.requestStatus === 'pending') {
      api.dispatch(startLoading());
    }

    if (['fulfilled', 'rejected'].includes(action.meta?.requestStatus)) {
      if (action.error?.name !== 'ConditionError') {
        api.dispatch(finishLoading());
      }
    }

    return next(action);
  };

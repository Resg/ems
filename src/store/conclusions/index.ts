import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';

import { ConclusionData } from '../../types/conclusions';

export interface ConclusionState {
  data: ConclusionData | null;
}

const initialState: ConclusionState = {
  data: null,
};

const conclusionSlice = createSlice({
  name: 'request',
  initialState,
  reducers: {
    setConclusion: (state, action: PayloadAction<ConclusionData>) => {
      state.data = action.payload;
    },
  },
});

export const { setConclusion } = conclusionSlice.actions;
export default conclusionSlice.reducer;

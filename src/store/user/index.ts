import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';

import { UserInfo } from '../../types/user';
import { RootState } from '../index';

export interface UserState {
  // isUserFetching: boolean
  userInfo?: UserInfo;
}

const initialState: UserState = {};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser: (state, action: PayloadAction<UserInfo>) => {
      state.userInfo = action.payload;
    },
  },
});

export const { setUser } = userSlice.actions;

export const getUser = (state: RootState) => state.user.userInfo;

export default userSlice.reducer;

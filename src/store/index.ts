import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';

import { configureStore } from '@reduxjs/toolkit';

import { completedTasksApi } from '../services/api/completedTasks';
import { conclusionsApi } from '../services/api/conclusions';
import { dictionariesApi } from '../services/api/dictionaries';
import { documentApi } from '../services/api/document';
import { documentSearchApi } from '../services/api/documentSearch';
import { draftsApi } from '../services/api/drafts';
import { employeeAPI } from '../services/api/employee';
import { fastSearchApi } from '../services/api/fastSearch';
import { filesApi } from '../services/api/files';
import { filtersApi } from '../services/api/filters';
import { incomingDocumentTasksApi } from '../services/api/incomingDocumentTasks';
import { myDocumentTasksApi } from '../services/api/myDocumentTasks';
import { permissionApi } from '../services/api/permissions';
import { registryApi } from '../services/api/registries';
import { requestAPI } from '../services/api/request';
import { resolutionApi } from '../services/api/resolutions';
import { taskAPI } from '../services/api/task';
import { userApi } from '../services/api/user';
import { userTableSettingsApi } from '../services/api/userTableSettings';

import advancedCounterpartySearch from './advancedCounterpartySearch';
import conclusions from './conclusions';
import dictionaries from './dictionaries';
import documents from './documents';
import { ErrorHandlingMiddleware } from './ErrorHandlingMiddleware';
import fastSearch from './fastSearchSlice';
import filters from './filters';
import { IsLoadingMiddleware } from './IsLoadingMiddleware';
import requests from './requests';
import tasks from './tasks';
import user from './user';
import userTableSettingsReducer from './userTableSettings';
import utils from './utils';

export const store = configureStore({
  reducer: {
    dictionaries,
    documents,
    requests,
    tasks,
    user,
    userTableSettings: userTableSettingsReducer,
    filters,
    utils,
    conclusions,
    advancedCounterpartySearch,
    fastSearch,
    [dictionariesApi.reducerPath]: dictionariesApi.reducer,
    [userApi.reducerPath]: userApi.reducer,
    [userTableSettingsApi.reducerPath]: userTableSettingsApi.reducer,
    [incomingDocumentTasksApi.reducerPath]: incomingDocumentTasksApi.reducer,
    [myDocumentTasksApi.reducerPath]: myDocumentTasksApi.reducer,
    [completedTasksApi.reducerPath]: completedTasksApi.reducer,
    [employeeAPI.reducerPath]: employeeAPI.reducer,
    [filtersApi.reducerPath]: filtersApi.reducer,
    [documentApi.reducerPath]: documentApi.reducer,
    [documentSearchApi.reducerPath]: documentSearchApi.reducer,
    [taskAPI.reducerPath]: taskAPI.reducer,
    [filesApi.reducerPath]: filesApi.reducer,
    [resolutionApi.reducerPath]: resolutionApi.reducer,
    [requestAPI.reducerPath]: requestAPI.reducer,
    [permissionApi.reducerPath]: permissionApi.reducer,
    [conclusionsApi.reducerPath]: conclusionsApi.reducer,
    [registryApi.reducerPath]: registryApi.reducer,
    [draftsApi.reducerPath]: draftsApi.reducer,
    [fastSearchApi.reducerPath]: fastSearchApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ serializableCheck: false })
      .concat(IsLoadingMiddleware)
      .concat(ErrorHandlingMiddleware)
      .concat(filesApi.middleware)
      .concat(incomingDocumentTasksApi.middleware)
      .concat(requestAPI.middleware)
      .concat(conclusionsApi.middleware)
      .concat(userApi.middleware)
      .concat(taskAPI.middleware)
      .concat(userTableSettingsApi.middleware)
      .concat(fastSearchApi.middleware)
      .concat(documentApi.middleware),
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = (): AppDispatch => useDispatch();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { RootState } from '../../store';
import { SearchResult } from '../../types/fastSearch';

interface FastSearchState {
  searchValueData: string;
  searchResults: SearchResult | {};
  searchError: string;
}

const initialState: FastSearchState = {
  searchValueData: '',
  searchResults: {},
  searchError: '',
};

const fastSearchSlice = createSlice({
  name: 'fastSearch',
  initialState,
  reducers: {
    setSearchValueData: (state, action: PayloadAction<string>) => {
      state.searchValueData = action.payload;
    },
    setSearchResults: (state, action: PayloadAction<SearchResult>) => {
      state.searchResults = action.payload;
      state.searchError = '';
    },
    setSearchError: (state, action: PayloadAction<string>) => {
      state.searchError = action.payload;
      state.searchResults = {};
    },
  },
});

export const { setSearchValueData, setSearchResults, setSearchError } =
  fastSearchSlice.actions;

export default fastSearchSlice.reducer;

export const selectSearchValue = (state: RootState) =>
  state.fastSearch.searchValueData;
export const selectSearchResults = (state: RootState) =>
  state.fastSearch.searchResults;
export const selectSearchError = (state: RootState) =>
  state.fastSearch.searchError;

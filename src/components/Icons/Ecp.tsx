import styles from './style.module.scss';

const EcpIcon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      fill="none"
      focusable="false"
      className={styles.icon}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M18.02 15.033V11.5l2.49-2.49-2.49-2.488V2.989h-3.532L11.999.5 9.51 2.989H5.977v3.533L3.488 9.01l2.49 2.489v3.533H9.51l2.489 2.489 2.489-2.49h3.533ZM7.985 9.01A4.013 4.013 0 0 0 12 13.025a4.013 4.013 0 0 0 4.014-4.014A4.013 4.013 0 0 0 12 4.996a4.013 4.013 0 0 0-4.015 4.015Z"
        fill="#4782DA"
      />
      <path
        d="m13.1 18.142 1.848-1.834 2.604-.01 2.22 5.358-2.78-.413L15.32 23.5l-2.22-5.358ZM10.992 18.142l-1.849-1.834-2.603-.01-2.22 5.358 2.78-.413L8.771 23.5l2.22-5.358Z"
        fill="#000"
        fillOpacity=".26"
      />
    </svg>
  );
};

export default EcpIcon;

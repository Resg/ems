export const EmployeeAvatar = () => {
  return (
    <svg
      width="36"
      height="36"
      viewBox="0 0 36 36"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect
        x="36"
        y="36"
        width="36"
        height="36"
        rx="18"
        transform="rotate(-180 36 36)"
        fill="black"
        fillOpacity="0.08"
      />
      <path
        d="M18 8C13.03 8 9 12.03 9 17C9 21.17 11.84 24.67 15.69 25.69L18 28L20.31 25.69C24.16 24.67 27 21.17 27 17C27 12.03 22.97 8 18 8ZM18 10C19.66 10 21 11.34 21 13C21 14.66 19.66 16 18 16C16.34 16 15 14.66 15 13C15 11.34 16.34 10 18 10ZM18 24.3C15.5 24.3 13.29 23.02 12 21.08C12.03 19.09 16 18 18 18C19.99 18 23.97 19.09 24 21.08C22.71 23.02 20.5 24.3 18 24.3Z"
        fill="#4782DA"
      />
    </svg>
  );
};

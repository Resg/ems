import styles from './style.module.scss';

const BarCodeIcon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      fill="none"
      focusable="false"
      className={styles.icon}
    >
      <path
        d="M21 21V3h-2v18h2Zm-4 0V3h-2v18h2Zm-4 0V3h-2v18h2Zm-4 0V3H7v18h2Zm-6 0h2V3H3v18Z"
        fill="currentColor"
      />
    </svg>
  );
};

export default BarCodeIcon;

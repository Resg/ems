import BarCode from './BarCode';
import DocIcon from './Doc';
import EcpIcon from './Ecp';
import ExcelIcon from './Excel';
import ExcelColor from './ExcelColor';
import Image from './Image';
import PdfIcon from './Pdf';
import Rtf from './Rtf';
import Sig from './Sig';
import Zip from './Zip';

export {
  BarCode,
  DocIcon,
  EcpIcon,
  ExcelColor,
  ExcelIcon,
  Image,
  PdfIcon,
  Rtf,
  Sig,
  Zip,
};

import styles from './style.module.scss';

const Image = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      focusable="false"
      className={styles.icon}
    >
      <path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M5 3.7417H19C20.1 3.7417 21 4.6417 21 5.7417V19.7417C21 20.8417 20.1 21.7417 19 21.7417H5C3.9 21.7417 3 20.8417 3 19.7417V5.7417C3 4.6417 3.9 3.7417 5 3.7417ZM19 19.7417V5.7417H5V19.7417H19Z"
        fill="black"
        fillOpacity="0.26"
      />
      <path
        d="M14.14 12.6016L11.14 16.4716L9 13.8816L6 17.7416H18L14.14 12.6016Z"
        fill="#84BD5A"
      />
    </svg>
  );
};

export default Image;

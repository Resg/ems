import React, { useCallback, useMemo, useState } from 'react';

import { DeleteOutline, Refresh } from '@mui/icons-material';
import { Button, Stack } from '@mui/material';

import { RoundButton } from '../../../components/RoundButton';
import {
  useDeleteAdditionalConditionListMutation,
  useGetAdditionalConditionListQuery,
} from '../../../services/api/user';
import { DataTable } from '../../CustomDataGrid';

export const AdditionalConditionTablePage: React.FC = ({}) => {
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const [filters, setFilters] = useState(false);

  const { data, refetch } = useGetAdditionalConditionListQuery({});

  const [deleteTemplate] = useDeleteAdditionalConditionListMutation();

  const selectedTemplates = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const rows = useMemo(() => {
    return data || [];
  }, [data]);

  const cols = useMemo(() => {
    return [
      {
        key: 'name',
        name: 'Наименование',
      },
    ];
  }, []);

  const handleDelete = useCallback(async () => {
    await deleteTemplate(selectedTemplates[0]);
    setSelected({});
    refetch();
  }, [selectedTemplates]);

  return (
    <>
      <Stack
        direction="row"
        spacing={1.5}
        marginY={2}
        justifyContent="space-between"
      >
        <RoundButton icon={<Refresh />} onClick={() => refetch()} />
        <Button
          variant="outlined"
          startIcon={<DeleteOutline />}
          onClick={handleDelete}
          disabled={selectedTemplates.length !== 1}
        >
          Удалить
        </Button>
      </Stack>
      <DataTable
        formKey={'table_page'}
        rows={rows}
        cols={cols}
        onRowSelect={setSelected}
        showFilters={filters}
      />
    </>
  );
};

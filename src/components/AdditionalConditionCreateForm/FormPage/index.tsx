import React from 'react';
import { useFormikContext } from 'formik';

import { ArrowForward } from '@mui/icons-material';
import { Button, Grid } from '@mui/material';

import Input from '../../Input';

import styles from './styles.module.scss';

interface FormPageProps {
  setPage: (page: 1 | 2) => void;
}
export const AdditionalConditionFormPage: React.FC<FormPageProps> = React.memo(
  ({ setPage }) => {
    const { values, handleChange, errors, touched, handleBlur } =
      useFormikContext<{ name: string }>();

    return (
      <Grid container spacing={2}>
        <Grid item xs={12} sx={{ mt: 3 }}>
          <Input
            name="name"
            label="Наименование набора"
            value={values.name}
            onChange={handleChange}
            required
            error={touched.name && Boolean(errors.name)}
            helperText={touched.name && errors.name}
            onBlur={handleBlur}
          />
        </Grid>
        <Grid item xs={12} sx={{ mt: 3 }} className={styles.buttonContainer}>
          <Button
            endIcon={<ArrowForward />}
            onClick={() => setPage(2)}
            className={styles.forwardButton}
          >
            Доступные наборы
          </Button>
        </Grid>
      </Grid>
    );
  }
);

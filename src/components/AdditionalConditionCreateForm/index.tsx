import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Form, Formik } from 'formik';

import { Close, Save } from '@mui/icons-material';
import { Button, Stack } from '@mui/material';

import {
  useCreateAdditionalConditionListMutation,
  useGetAdditionalConditionListQuery,
} from '../../services/api/user';
import { Popup } from '../Popup';

import { AdditionalConditionFormPage } from './FormPage';
import { AdditionalConditionTablePage } from './TablePage';

import styles from './styles.module.scss';

interface AdditionalModalProps {
  open: boolean;
  onClose: () => void;
  selectedArrNum: number[];
}

export const AdditionalConditionCreateForm: React.FC<AdditionalModalProps> = ({
  open,
  onClose,
  selectedArrNum,
}) => {
  const [page, setPage] = useState<1 | 2>(1);
  const [createAdditionalTemplate] = useCreateAdditionalConditionListMutation();
  const { data } = useGetAdditionalConditionListQuery({});

  const usedNames = useMemo(() => {
    return data?.map((el) => el.name);
  }, [data]);

  const handleSubmit = useCallback(
    async (values: { name: string }) => {
      const name = values.name;
      await createAdditionalTemplate({ name: name, ids: selectedArrNum });
      onClose();
    },
    [selectedArrNum, createAdditionalTemplate]
  );

  useEffect(() => {
    setPage(1);
  }, [open]);

  const title = useMemo(() => {
    if (page === 1) {
      return 'Сохранить в набор';
    } else {
      return 'Доступные наборы';
    }
  }, [page, open]);

  const validation = useCallback(
    (values: { name: string }) => {
      const errors: { name?: string } = {};
      if (!values.name) {
        errors.name = 'Обязательное поле';
      } else if (usedNames?.includes(values.name)) {
        errors.name = 'Данное имя набора уже используется';
      }
      return errors;
    },
    [usedNames]
  );

  return (
    <Popup
      open={Boolean(open)}
      onClose={onClose}
      backButton={page === 2}
      onBack={() => setPage(1)}
      title={title}
      width={750}
    >
      <Formik
        initialValues={{
          name: '',
        }}
        enableReinitialize
        onSubmit={handleSubmit}
        validate={validation}
      >
        {({ values, setFieldValue, handleSubmit }) => {
          return (
            <Form>
              {page === 1 && <AdditionalConditionFormPage setPage={setPage} />}
              {page === 2 && <AdditionalConditionTablePage />}

              <Stack
                className={styles.bar}
                direction="row"
                justifyContent="flex-end"
                spacing={1.5}
                style={{ width: '677px' }}
              >
                {page === 1 && (
                  <Button
                    variant="contained"
                    startIcon={<Save />}
                    onClick={() => handleSubmit()}
                  >
                    Сохранить
                  </Button>
                )}
                <Button
                  variant="outlined"
                  startIcon={<Close />}
                  onClick={onClose}
                >
                  Отмена
                </Button>
              </Stack>
            </Form>
          );
        }}
      </Formik>
    </Popup>
  );
};

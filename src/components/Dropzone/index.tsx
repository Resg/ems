import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useDropzone } from 'react-dropzone';

import { Download } from '@mui/icons-material';

import styles from './styles.module.scss';

export interface AddFileType {
  documentId: number;
  file: FormData;
}

export interface DropzoneProps {
  children: React.ReactNode;
  documentId: number;
  refetchTable: () => void;
  addFile: (value: AddFileType) => void;
}

export const Dropzone: React.FC<DropzoneProps> = ({
  refetchTable,
  addFile,
  documentId,
  children,
}) => {
  const ref = useRef<HTMLDivElement | null>(null);
  const [height, setHeight] = useState<number>(0);

  const onDrop = useCallback(
    async (files: File[]) => {
      await Promise.all(
        files.map(async (item: File) => {
          const file = new FormData();
          file.append('file', item);
          file.append('documentId', String(documentId));
          await addFile({ documentId, file });
        })
      ).then(() => {
        refetchTable();
      });
    },
    [addFile, refetchTable]
  );

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    noClick: true,
  });

  useEffect(() => {
    setTimeout(() => {
      if (ref.current) {
        setHeight(ref?.current?.clientHeight);
      }
    }, 3000);
  }, [ref.current?.clientHeight]);

  return (
    <div className={styles.dropzone} {...getRootProps()}>
      <input {...getInputProps()} />
      {isDragActive ? (
        <div className={styles.container} style={{ height: height }}>
          <div className={styles.prompt}>
            <Download className={styles.icon} />
            Перетащите файлы для загрузки
          </div>
        </div>
      ) : (
        <div ref={ref}>{children}</div>
      )}
    </div>
  );
};

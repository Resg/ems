import React from 'react';

import { Autocomplete } from '@mui/material';

import { Option } from '../../../types/dictionaries';
import Input from '../../Input';

import styles from './styles.module.scss';

export interface ForColumnProps {
  options: Option[];
  value: any;
  onChange: (value: any) => void;
}

export const ForColumn: React.FC<ForColumnProps> = ({
  options,
  onChange,
  value,
}) => {
  const handleChange = (_: any, option: Option | null) => {
    onChange(option?.value);
  };

  return (
    <Autocomplete
      onChange={handleChange}
      fullWidth
      value={options?.find((option) => option.value === value)}
      renderInput={(params) => {
        return (
          <Input
            {...params}
            fullWidth
            variant={'standard'}
            label={'Кому'}
            inputProps={{ ...params.inputProps, className: styles.input }}
          />
        );
      }}
      options={options}
    />
  );
};

import React, { useCallback, useEffect, useMemo, useState } from 'react';
import cn from 'classnames';
import { FormikProps, useFormikContext } from 'formik';
import { Column } from 'react-data-grid';
import { useSelector } from 'react-redux';

import { DeleteSweep, Search } from '@mui/icons-material';
import { Button, Checkbox } from '@mui/material';

import { DeliveryCols, ReceiverCols } from '../../constants/receiver';
import {
  useGetCounterpartiesQuery,
  useGetCounterpartyPersonsMutation,
  useGetDefaultAddresseeDeliveryMutation,
  useGetDefaultCounterpartiesQuery,
  useGetDeliveryTypesMutation,
} from '../../services/api/dictionaries';
import { useGetKZContractQuery } from '../../services/api/request';
import { RootState } from '../../store';
import { Counterparty, DeliveryType, Option } from '../../types/dictionaries';
import { prepareDataForCounterpartySearch } from '../../utils/counterpartySearch';
import { AdvancedCounterpartySearch } from '../AdvancedCounterpartySearch';
import { DataTable, TreeDataGrid } from '../CustomDataGrid';
import { FilterBar, FormValues } from '../FilterBarNew';
import { SettingsTypes, ToolsPanel } from '../ToolsPanel';

import { ForColumn } from './ForColumn';

import styles from './styles.module.scss';

export interface ReceiverFormProps {
  onChange: (value: any) => void;
  initialChosen?: number[];
  initialReceivers?: Partial<DeliveryType>[];
}
const groups = ['contractorName'];

export const ReceiverForm: React.FC<ReceiverFormProps> = ({
  onChange,
  initialChosen = [],
  initialReceivers = [],
}) => {
  const [getDeliveryType] = useGetDeliveryTypesMutation();
  const [getPersons] = useGetCounterpartyPersonsMutation();
  const [getDefaultAddresseeDelivery] =
    useGetDefaultAddresseeDeliveryMutation();
  const { setFieldValue, values } = useFormikContext<any>();
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [tableFilters, setTableFilters] = useState(false);

  const filters = useSelector(
    (state: RootState) => state.filters.data['receiver-form'] || {}
  );
  const {
    data,
    isLoading: isReceiverLoading,
    refetch,
  } = useGetCounterpartiesQuery({
    searchParameters: filters,
    page: page,
    size: perPage,
    useCount: true,
  });

  const { data: contractKZ } = useGetKZContractQuery(
    {
      id: values.objectId?.slice(values.objectId.lastIndexOf(',') + 1),
    },
    { skip: values.parametersCode !== 'COVER_LETTER' || !values.objectId }
  );

  const { data: defaultCounterpartiesIds = [] } =
    useGetDefaultCounterpartiesQuery(
      {
        templateId: values.templateId,
        objectId: values.objectId?.slice(values.objectId.lastIndexOf(',') + 1),
        objectTypeCode:
          values.precreationObjectTypeCode || values.objectTypeCode,
        requestIds:
          values.parametersCode === 'COVER_LETTER_TO_POWER_AGENCIES'
            ? values.objectId
            : undefined,
      },
      { skip: !values.templateId || !values.parametersCode }
    );

  const initialSelectedIds = useMemo(
    () => [
      ...defaultCounterpartiesIds,
      ...initialChosen,
      ...initialReceivers.map((receiver) => receiver.contractorId as number),
    ],
    [defaultCounterpartiesIds.length]
  );

  const { data: defaultCounterparties } = useGetCounterpartiesQuery(
    {
      searchParameters: {
        ids: initialSelectedIds as number[],
      },
    },
    { skip: !initialSelectedIds?.length }
  );

  const [selectedCounterparties, setSelectedCounterparties] = useState<
    Record<string, boolean>
  >({
    ...initialSelectedIds.reduce((acc, cur) => ({ ...acc, [cur]: true }), {}),
  });

  const [selectedDeliveries, setSelectedDeliveries] = useState<
    Record<number, boolean>
  >(
    initialReceivers.reduce(
      (acc, row) => ({
        ...acc,
        [row?.id as number]: true,
      }),
      {}
    )
  );

  useEffect(() => {
    setSelectedCounterparties({
      ...initialSelectedIds.reduce((acc, cur) => ({ ...acc, [cur]: true }), {}),
    });
  }, [initialSelectedIds]);

  const rows = useMemo(
    () =>
      Object.values(
        [...(defaultCounterparties?.data || []), ...(data?.data || [])].reduce(
          (acc, cur) => {
            return { ...acc, [cur.id]: cur };
          },
          {} as Record<number, Counterparty>
        )
      ) as Counterparty[],
    [data?.data, defaultCounterparties?.data]
  );

  const mappedCounterparties = useMemo(
    () =>
      rows.reduce((acc, el) => {
        acc[el.id] = el;
        return acc;
      }, {} as Record<string, Counterparty>),
    [rows]
  );

  const [chosenCounterparties, setChosenCounterparties] = useState<
    Counterparty[]
  >([]);

  const selected = useMemo(
    () => [
      ...Object.keys(selectedCounterparties)
        .filter(
          (key) => selectedCounterparties[key] && mappedCounterparties[key]
        )
        .map((id) => mappedCounterparties[id]),
    ],
    [mappedCounterparties, selectedCounterparties]
  );

  const [deliveryRows, setDeliveryRows] = useState<Partial<DeliveryType>[]>([
    ...initialReceivers,
  ]);

  const [counterpartiesPersons, setCounterpartiesPersons] = useState<
    Record<number, Option[]>
  >({});

  useEffect(() => {
    Promise.all(
      selected.map((el) =>
        getDeliveryType({ id: el.id }).then((response) => {
          if ('data' in response) return response.data;
          return [] as DeliveryType[];
        })
      )
    ).then((result) => {
      setDeliveryRows(
        Object.values(
          [
            ...result
              .reduce((acc, data) => [...acc, ...data], [] as DeliveryType[])
              .map((el, index) => ({
                ...el,
                id: (el.deliveryId?.toString() +
                  el.contractorId?.toString() +
                  el.contactId?.toString()) as unknown as number,
                initialId: initialReceivers.find(
                  (i) =>
                    i.id ===
                    ((el.deliveryId?.toString() +
                      el.contractorId?.toString() +
                      el.contactId?.toString()) as unknown as number)
                )?.initialId,
              })),
          ].reduce((acc, cur) => ({ ...acc, [cur.id as number]: cur }), {})
        ) as any
      );
    });
  }, [getDeliveryType, getPersons, selected]);

  useEffect(() => {
    Promise.all(
      initialSelectedIds.map((el) =>
        getDefaultAddresseeDelivery({
          contractorId: el,
          deliveryTypeCode: contractKZ?.isKzContract ? 'CABINET' : undefined,
        }).then((response) => {
          if ('data' in response) return response.data?.data;
          return [];
        })
      )
    ).then((results) => {
      const selectedDeliveryIds: string[] = results.flatMap((result) =>
        result.map(
          (el: any) =>
            el.defaultDeliveryId?.toString() +
            el.contractorId?.toString() +
            el.contactId?.toString()
        )
      );

      setSelectedDeliveries({
        ...selectedDeliveryIds.reduce(
          (acc, cur) => ({ ...acc, [cur]: true }),
          {}
        ),
      });
    });
  }, [initialSelectedIds, getDefaultAddresseeDelivery]);

  useEffect(() => {
    Promise.all(
      deliveryRows.map((el) =>
        getPersons({ id: Number(el.contractorId) }).then((response) => {
          if ('data' in response) {
            return { [Number(el.contractorId)]: response.data };
          } else {
            return { [Number(el.contractorId)]: [] as Option[] };
          }
        })
      )
    ).then((results) =>
      setCounterpartiesPersons(
        results.reduce(
          (acc, cur) => ({ ...acc, ...cur }),
          counterpartiesPersons
        )
      )
    );
  }, [deliveryRows, getPersons]);
  const [rowsPersons, setRowsPersons] = useState<Record<number, string>>(
    initialReceivers.reduce(
      (acc, row) => ({
        ...acc,
        [row?.id as number]: row.contactPersonId,
      }),
      {}
    )
  );

  const additionalColumnProps: Record<string, Partial<Column<any>>> = useMemo(
    () => ({
      fullName: {
        cellClass: styles['for-cell'],
        formatter: (props: any) => {
          return (
            <ForColumn
              onChange={(value) =>
                setRowsPersons({ ...rowsPersons, [props.row.id]: value })
              }
              value={rowsPersons[props.row.id]}
              options={props.row.contactPersonsOptions || []}
            />
          );
        },
      },
      selectDelivery: {
        width: 100,
        formatter: (props: any) => {
          return (
            <div>
              <Checkbox
                checked={selectedDeliveries[props.row.id] || false}
                onChange={() =>
                  setSelectedDeliveries({
                    ...selectedDeliveries,
                    [props.row.id]: !selectedDeliveries[props.row.id],
                  })
                }
              />
            </div>
          );
        },
      },
    }),
    [rowsPersons, selectedDeliveries]
  );

  const rowsWithOptions = useMemo(
    () =>
      deliveryRows.map((row) => {
        return {
          id: row.id as number,
          ...row,
          contactPersonsOptions:
            counterpartiesPersons[row.contractorId as number],
        };
      }),
    [counterpartiesPersons, deliveryRows]
  );

  useEffect(() => {
    onChange(
      Object.keys(selectedDeliveries)
        .filter((key) => selectedDeliveries[key as unknown as number])
        .map((key) => {
          const row = rowsWithOptions.find((row) => String(row.id) === key);
          return {
            ...(row || {}),
            fullName: counterpartiesPersons[row?.contractorId as number]?.find(
              (person: Option) =>
                person.value === rowsPersons[key as unknown as number]
            )?.label,
            contractorName: row?.contractorName,
            contractorContactPersonId: rowsPersons[key as unknown as number],
            deliveryType: row?.deliveryType,
            addressAndContactInfoView: row?.addressAndContactInfoView,
            contractorId: row?.contractorId,
            deliveryId: row?.deliveryId,
            deliveryTypeCode: row?.deliveryTypeCode,
            contactId: row?.contactId,
          };
        })
        .filter((delivery) =>
          selected.some((contractor) => contractor.id === delivery.contractorId)
        )
    );
  }, [
    counterpartiesPersons,
    onChange,
    rowsPersons,
    rowsWithOptions,
    selectedDeliveries,
    selected,
  ]);

  const handleSetSelectedDeliveries = useCallback(
    (selected: Record<string | number, boolean>) => {
      setDeliveryRows(deliveryRows.filter((row) => selected[row.id as number]));
      setSelectedCounterparties({});
    },
    [deliveryRows]
  );

  const [tableGroups, setTableGroups] = useState(groups);

  const handleChoose = useCallback(
    () =>
      setChosenCounterparties([
        ...chosenCounterparties,
        ...selected.filter((counterparty) =>
          chosenCounterparties.every(
            (counter) => counter.id !== counterparty.id
          )
        ),
      ]),
    [selected]
  );

  const createFilterButtons = useCallback(
    (
      onSearch: ({ values }: { values: FormValues }) => () => Promise<void>,
      props: FormikProps<FormValues>
    ) => {
      const { resetForm, values } = props;

      return (
        <>
          <Button
            color="primary"
            variant="contained"
            startIcon={<Search />}
            onClick={onSearch({
              values: prepareDataForCounterpartySearch(values as FormValues),
            })}
          >
            Поиск
          </Button>
          <Button
            color="primary"
            variant="outlined"
            startIcon={<DeleteSweep />}
            onClick={() => resetForm()}
          >
            Очистить
          </Button>
        </>
      );
    },
    []
  );

  return (
    <div className={styles.wrapper}>
      <FilterBar
        formKey={'receiver-form'}
        createFilterButtons={createFilterButtons}
        refetchSearch={() => refetch()}
      >
        <AdvancedCounterpartySearch />
      </FilterBar>
      <DataTable
        formKey={'receiver-form'}
        cols={ReceiverCols}
        rows={rows}
        className={styles.table}
        initialSelected={selectedCounterparties}
        onRowSelect={setSelectedCounterparties}
        loading={isReceiverLoading}
        pagerProps={{
          page,
          setPage,
          perPage,
          setPerPage,
          total: data?.totalCount || 0,
        }}
      />

      <TreeDataGrid
        formKey={'receiver_form'}
        groups={groups}
        showFilters={tableFilters}
        expanded
        cols={DeliveryCols}
        rows={rowsWithOptions}
        withSelect={false}
        additionalColumnProps={additionalColumnProps}
        className={cn(styles.table, styles['delivery-table'])}
        onRowSelect={handleSetSelectedDeliveries}
        loading={isReceiverLoading}
      >
        <ToolsPanel
          className={styles.tools}
          settings={[SettingsTypes.GROUPS]}
          setFilters={setTableFilters}
        />
      </TreeDataGrid>
    </div>
  );
};

import React from 'react';

import styles from './Container.module.scss';

export interface ContainerProps {
  children?: React.ReactNode;
}

export const Container = (props: ContainerProps) => {
  const { children } = props;

  return <div className={styles.box}>{children}</div>;
};

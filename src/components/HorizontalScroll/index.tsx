import React, { useEffect, useRef } from 'react';

import { Stack, StackProps } from '@mui/material';

export interface HorizontalScrollProps extends StackProps {}

export const HorizontalScroll: React.FC<HorizontalScrollProps> = ({
  children,
  sx,
  ...props
}) => {
  const ref = useRef<HTMLDivElement>();

  useEffect(() => {
    const current = ref.current;
    if (!current) {
      return;
    }

    const listener = (event: WheelEvent) => {
      event.preventDefault();
      current.scrollLeft += event.deltaY;
    };
    current.addEventListener('wheel', listener);

    return () => {
      current?.removeEventListener('wheel', listener);
    };
  }, []);

  return (
    <Stack
      ref={ref}
      direction="row"
      spacing={1.5}
      overflow="scroll"
      sx={{
        '&::-webkit-scrollbar': {
          display: 'none',
        },
        ...sx,
      }}
      {...props}
    >
      {children}
    </Stack>
  );
};

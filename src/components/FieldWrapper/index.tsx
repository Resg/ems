import React, { useEffect } from 'react';
import { useFormikContext } from 'formik';

export interface FieldWrapperProps {
  children?: React.ReactNode;
  disabled?: boolean;
  name: string;
  defaultValue?: any;
  className?: string;
}

export const FieldWrapper: React.FC<FieldWrapperProps> = ({
  children,
  disabled,
  name,
  defaultValue,
  className,
}) => {
  const { values, setFieldValue } = useFormikContext<any>();

  useEffect(() => {
    if (disabled) {
      setFieldValue(name, defaultValue);
    }
  }, [disabled]);

  return <div className={className}>{children}</div>;
};

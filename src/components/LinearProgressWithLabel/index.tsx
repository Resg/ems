import React from 'react';

import { Box, LinearProgress, Typography } from '@mui/material/';

import styles from './styles.module.scss';

type LinearProgressWithLabelProps = {
  value: number;
  label: string;
};

export const LinearProgressWithLabel: React.FC<
  LinearProgressWithLabelProps
> = ({ value, label }) => {
  return (
    <Box className={styles.progressBarLabel}>
      <Box className={styles.progressLine}>
        <LinearProgress
          variant="determinate"
          value={value}
          className={styles.progressBar}
        />
        <Typography variant="caption" className={styles.label}>
          {label}
        </Typography>
      </Box>
    </Box>
  );
};

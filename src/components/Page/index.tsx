import React from 'react';

import Card from '../Card';

import styles from './styles.module.scss';

export interface PageProps {
  title?: string;
  children: React.ReactNode;
}

export const Page: React.FC<PageProps> = ({ title, children }) => {
  return (
    <>
      <h1 className={styles.title}>{title}</h1>
      <Card className={styles.card}>{children}</Card>
    </>
  );
};

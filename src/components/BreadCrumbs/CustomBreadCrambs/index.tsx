import { useEffect } from 'react';
import { useSelector } from 'react-redux';

import HomeIcon from '@mui/icons-material/Home';
import Crumbs from '@mui/material/Breadcrumbs';

import { LocalStorageKeys } from '../../../constants/localStorage';
import { RootState } from '../../../store';
import { useLocalStorage } from '../../../utils/hooks/useLocalStorage';
import { Link } from '../../Link';

import styles from '../BreadCrumbs.module.scss';

export interface BreadCrumb {
  label: string;
  href?: string;
}

export const CustomBreadCrumbs = () => {
  const [value, setValue] = useLocalStorage(LocalStorageKeys.BREAD_CRUMBS);
  const fileName = useSelector(
    (state: RootState) => state.utils.crumbsFileName
  );

  let fileCrumbs;

  if (value[0].clear) {
    fileCrumbs = <div className={styles.current}>{fileName}</div>;
  } else {
    fileCrumbs = [...value, { label: fileName }].map((crumb) => {
      if (crumb.href) {
        return (
          <Link to={crumb.href} className={styles.link}>
            {crumb.label}
          </Link>
        );
      } else {
        return <div className={styles.current}>{crumb.label}</div>;
      }
    });
  }

  useEffect(() => {
    localStorage.setItem(
      LocalStorageKeys.BREAD_CRUMBS,
      JSON.stringify([{ clear: true }])
    );
  }, [value, fileName]);

  return (
    <Crumbs sx={{ pl: 2 }}>
      <Link to="/" className={styles.link}>
        <HomeIcon />
      </Link>
      {fileCrumbs}
    </Crumbs>
  );
};

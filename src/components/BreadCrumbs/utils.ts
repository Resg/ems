import { DocumentTypes, documentTypesLabels } from '../../constants/documents';
import { routeLabels, Routes } from '../../constants/routes';
import { BreadCrumb } from '../../store/utils';

export const getLinkLabel = (
  breadcrumb: BreadCrumb,
  id: string | null,
  type: string | null
) => {
  const [mainPath, addPath] = breadcrumb.pathname
    .split('/')
    .filter((path) => path);
  if (breadcrumb.pathname === Routes.DOCUMENTS && id) {
    return `Документ ШК: ${id}`;
  }
  if (breadcrumb.pathname === Routes.DOCUMENTS && type) {
    return `Новый документ: ${documentTypesLabels[type as DocumentTypes]}`;
  }
  if (Routes.REGISTRIES.includes(mainPath)) {
    return `Реестр ${addPath}`;
  }
  if (Routes.CONCLUSION_REVIEW.includes(mainPath)) {
    return `Просмотр заключений`;
  }
  return routeLabels[breadcrumb.pathname];
};

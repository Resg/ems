import React, { useCallback } from 'react';
import { useSelector } from 'react-redux';

import HomeIcon from '@mui/icons-material/Home';
import Crumbs from '@mui/material/Breadcrumbs';

import { RootState, useAppDispatch } from '../../store';
import { chooseBreadCrumbs, resetBreadCrumbs } from '../../store/utils';
import { Link } from '../Link';

import styles from './BreadCrumbs.module.scss';

export const BreadCrumbs = () => {
  const breadCrumbs = useSelector(
    (state: RootState) => state.utils.breadCrumbs
  );
  const dispatch = useAppDispatch();

  const handleChoose = useCallback(
    (href: string, index: number) => {
      dispatch(chooseBreadCrumbs(index));
      window.open(href, '_self');
    },
    [dispatch]
  );

  const handleChooseHome = useCallback(
    (_?: React.MouseEvent) => {
      dispatch(resetBreadCrumbs());
    },
    [dispatch]
  );

  return (
    <Crumbs className={styles.box}>
      <Link to="/" className={styles.link} onClick={handleChooseHome}>
        <HomeIcon />
      </Link>
      {breadCrumbs.map((breadcrumb, index) => {
        const href = breadcrumb.pathname
          ? breadcrumb.pathname + breadcrumb.search
          : '';
        const searchParams = new URLSearchParams(breadcrumb.search);
        const id = searchParams.get('id');
        const type = searchParams.get('documentType');
        const isLast = index === breadCrumbs.length - 1;
        const label = breadcrumb.title;

        if (isLast || !href) {
          return (
            <div className={styles.current} key={href}>
              {label}
            </div>
          );
        }

        return (
          <div
            key={href}
            className={styles.link}
            onClick={() => handleChoose(href, index)}
          >
            {label}
          </div>
        );
      })}
    </Crumbs>
  );
};

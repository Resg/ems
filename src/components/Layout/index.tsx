import React from 'react';
import cn from 'classnames';
import { useSelector } from 'react-redux';

import { RootState } from '../../store';

import styles from './Layout.module.scss';

export interface MainProps {
  children?: React.ReactNode;
}

export const Layout: React.FC<MainProps> = ({ children }) => {
  const isSideBarOpen = useSelector(
    (state: RootState) => state.utils.sideBarIsOpen
  );

  return (
    <div
      className={cn({
        [styles.content]: true,
        [styles.content_sidebar]: isSideBarOpen,
      })}
    >
      {children}
    </div>
  );
};

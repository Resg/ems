import React, { useCallback, useEffect, useState } from 'react';
import { useNavigate, useSearchParams } from 'react-router-dom';

import { Search } from '@mui/icons-material';
import { IconButton, OutlinedInput } from '@mui/material';

import { useGetFastSearchQuery } from '../../services/api/fastSearch';
import { useAppDispatch, useAppSelector } from '../../store';
import {
  setSearchResults,
  setSearchValueData,
} from '../../store/fastSearchSlice';
import { setError } from '../../store/utils';
import { useCtrlCheck } from '../../utils/hooks/useCtrlCheck';

import styles from './FastSearch.module.scss';
export interface FastSearchProps {
  placeholder?: string;
}

export const FastSearch: React.FC<FastSearchProps> = ({
  placeholder = 'Поиск документов и заявок',
}) => {
  const [value, setValue] = useState('');
  const [searchValue, setSearchValue] = useState('');
  const dispatch = useAppDispatch();
  const ctrlPress = useCtrlCheck();
  const navigate = useNavigate();

  const [searchParams, setSearchParams] = useSearchParams();

  const { data, refetch } = useGetFastSearchQuery({ searchValue });

  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
  };

  const containsNonPrintable = (inputString: string): boolean => {
    const regex = /^[%\s]*$/;
    return regex.test(inputString);
  };

  useEffect(() => {
    const result = searchParams.get('q');
    if (result) {
      setSearchValue(result);
      refetch();
      setValue('');
    }
  }, [searchParams]);

  const openFormInNewTab = useAppSelector(
    (state) => state.utils.openFormInNewTab
  );

  const openPage = useCallback(
    (path: string = '') => {
      window.open(`${path}`, openFormInNewTab ? '_blank' : '_self')?.focus();
    },
    [openFormInNewTab]
  );

  const handleSearchSubmit = () => {
    const inputValue = value.trim();
    if (!inputValue || containsNonPrintable(inputValue)) {
      dispatch(
        setError({
          message: 'Введите значение для осуществления поиска',
        })
      );
      return;
    }

    setSearchValue(inputValue);
    refetch();
    setValue('');
  };

  const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      handleSearchSubmit();
    }
  };

  useEffect(() => {
    if (data && data.data.length > 1) {
      dispatch(setSearchResults(data));
      dispatch(setSearchValueData(searchValue));

      ctrlPress
        ? window.open(`/search-result?q=${searchValue}`, '_blank')?.focus()
        : navigate('/search-result');
    }

    if (data && data.data.length === 1) {
      const { objectType, objectId } = data.data[0];
      const path =
        objectType === 'DOCUMENT'
          ? `/documents/${objectId}/common`
          : `/requests/${objectId}/common`;
      openPage(path);
    }
  }, [data]);

  return (
    <div className={styles.container}>
      <OutlinedInput
        className={styles.input}
        size="small"
        placeholder={placeholder}
        value={value}
        onChange={handleSearchChange}
        onKeyPress={handleKeyPress}
        endAdornment={
          <IconButton onClick={handleSearchSubmit}>
            <Search />
          </IconButton>
        }
      />
    </div>
  );
};

import React, { useCallback, useState } from 'react';
import cn from 'classnames';
import { useSnackbar } from 'notistack';

import {
  ContentCopy,
  KeyboardArrowRight,
  KeyboardArrowUp,
} from '@mui/icons-material';
import { IconButton, InputAdornment } from '@mui/material';

import { copyToClipboard } from '../../../utils/copyToClipboard';
import Input, { InputProps } from '../../Input';

import styles from './styles.module.scss';

type CopyValue = {
  label: string;
  value: any;
};

export type CopybleInputProps = {
  copyValue: CopyValue | CopyValue[] | null;
  showCollapse?: boolean;
} & InputProps;

export const CopybleInput: React.FC<CopybleInputProps> = ({
  disabled,
  copyValue,
  showCollapse,
  ...params
}) => {
  const [collapsed, setCollapsed] = useState(false);
  const { enqueueSnackbar } = useSnackbar();

  const getCopyValue = useCallback(() => {
    if (Array.isArray(copyValue)) {
      return copyValue
        .filter((i) => !!i.label)
        .map((i) => i.label)
        .join(', ');
    }

    return copyValue?.label || '';
  }, [copyValue]);

  const onCopy = useCallback(() => {
    copyToClipboard(getCopyValue(), () =>
      enqueueSnackbar('Скопировано в буфер обмена', {
        autoHideDuration: 3000,
        variant: 'info',
        anchorOrigin: { horizontal: 'right', vertical: 'top' },
      })
    );
  }, [getCopyValue]);

  return (
    <Input
      {...params}
      disabled={disabled}
      InputProps={{
        ...params.InputProps,
        classes: {
          root: showCollapse
            ? collapsed
              ? styles.collapsed
              : styles.uncollapsed
            : '',
        },
        endAdornment: (
          <InputAdornment position={'end'}>
            {showCollapse && (
              <IconButton
                size="small"
                onClick={() => {
                  setCollapsed((prev) => !prev);
                }}
                className={cn(styles.arrowIcon, {
                  [styles.arrowIcon_disabled]: disabled,
                })}
              >
                {collapsed ? (
                  <KeyboardArrowUp fontSize="inherit" />
                ) : (
                  <KeyboardArrowRight fontSize="inherit" />
                )}
              </IconButton>
            )}

            <IconButton
              size="small"
              onClick={onCopy}
              className={cn(styles.copyIcon, {
                [styles.copyIcon_disabled]: disabled,
              })}
            >
              <ContentCopy fontSize="inherit" />
            </IconButton>
            {React.cloneElement(
              params.InputProps?.endAdornment as React.ReactElement,
              {}
            )}
          </InputAdornment>
        ),
      }}
    />
  );
};

import React, { useCallback, useEffect, useMemo, useState } from 'react';
import cn from 'classnames';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { Autocomplete, createFilterOptions, SxProps } from '@mui/material';

import { CopybleInput } from './CopybleInput';
import { InputChip } from './InputChip';

import styles from './styles.module.scss';

export interface AutoCompleteInputProps {
  multiple?: boolean;
  sx?: SxProps;
  disabled?: boolean;
  required?: boolean;
  name: string;
  label: string;
  options: Array<{ label: string; value: any }>;
  className?: string;
  initialValue?: Array<{ label: string; value: any }>;
  onInputChange?: (value: any) => void;
  onFocus?: () => void;
  open?: boolean;
  onOpen?: () => void;
  forcePopupIcon?: boolean;
  clearOnBlur?: boolean;
  helperText?: string;
  matchFrom?: 'start' | 'any';
  onKeyDown?: (e: React.KeyboardEvent<HTMLDivElement>) => void;
  showCollapse?: boolean;
}

export const AutoCompleteInput: React.FC<AutoCompleteInputProps> = ({
  sx = {},
  multiple,
  disabled,
  required,
  label,
  name,
  options,
  className,
  initialValue,
  helperText,
  onInputChange,
  matchFrom = 'any',
  onFocus,
  showCollapse,
  ...restProps
}) => {
  const { setFieldValue, values, errors, setFieldTouched, touched } =
    useFormikContext<any>();

  const isTouched = get(touched, name);

  const parsedValue = useMemo(() => {
    const value = get(values, name);
    if (!value && value !== null) {
      return multiple ? [] : null;
    }

    if (multiple) {
      if (!Array.isArray(value)) {
        return [];
      }

      return value.map(
        (val: any) =>
          options.find((option) => option.value === val) || {
            value: 0,
            label: '',
          }
      );
    }
    return options.find((option) => option.value === value) || null;
  }, [multiple, name, options, values]);

  const error: string = useMemo(() => {
    const error = get(errors, name);
    if (!error) {
      return '';
    }
    if (Array.isArray(error)) {
      return error[0] as string;
    } else {
      return error as string;
    }
  }, [errors, name]);

  const [inputValue, setInputValue] = useState(
    initialValue?.length ? initialValue[0].label : ''
  );

  const handleBlur = useCallback(() => {
    setFieldTouched(name, true, true);
  }, [name, setFieldTouched]);

  const initValue = useMemo(
    () => (initialValue?.length ? initialValue[0].label : ''),
    [initialValue]
  );

  useEffect(() => {
    if (initValue) {
      setFieldValue(
        name,
        multiple
          ? initialValue?.map((el: any) => el.value)
          : initialValue?.length
          ? initialValue[0].value
          : ''
      );
    }
  }, [initValue]);

  return (
    <Autocomplete
      size={'small'}
      value={parsedValue}
      inputValue={inputValue}
      className={cn(styles.autocomplete, className)}
      filterOptions={createFilterOptions({
        matchFrom,
        stringify: (option) => option.label,
      })}
      renderOption={(params, option) => {
        return (
          <li {...params} key={option.value}>
            {option.label}
          </li>
        );
      }}
      onInputChange={(_, rowValue) => {
        setInputValue(rowValue);
      }}
      renderInput={(params) => {
        return (
          <CopybleInput
            {...params}
            copyValue={parsedValue}
            label={label}
            error={isTouched && Boolean(error)}
            helperText={(isTouched && error) || helperText}
            required={required}
            showCollapse={showCollapse}
          />
        );
      }}
      renderTags={(value, getTagProps) => {
        return (
          <div>
            {value.map((option, index) => {
              return (
                <InputChip {...getTagProps({ index })} label={option.label} />
              );
            })}
          </div>
        );
      }}
      onBlur={handleBlur}
      multiple={multiple}
      onChange={(event, value: any, reason, details) => {
        setFieldValue(
          name,
          multiple ? value?.map((el: any) => el.value) : value?.value
        );
        if (onInputChange) {
          onInputChange(value);
        }
      }}
      options={options}
      disabled={disabled}
      sx={
        disabled
          ? sx
          : {
              ...sx,
              'div > div > div.Mui-disabled': { opacity: 1 },
              'div > div > div.Mui-disabled > svg': { opacity: 0.38 },
            }
      }
      onFocus={onFocus}
      {...restProps}
    />
  );
};

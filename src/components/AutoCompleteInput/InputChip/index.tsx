import React from 'react';
import { useSnackbar } from 'notistack';

import { Chip, ChipProps } from '@mui/material';

import { copyToClipboard } from '../../../utils/copyToClipboard';
import { Copy } from '../../Icons/Copy';

import styles from './styles.module.scss';

const ChipContent: React.FC<{ label?: React.ReactNode }> = ({ label }) => {
  const { enqueueSnackbar } = useSnackbar();

  return (
    <div className={styles.labelBox}>
      <div className={styles.labelText}>{label}</div>
      <div
        className={styles.copyBtn}
        onClick={() =>
          copyToClipboard(label as string, () =>
            enqueueSnackbar('Скопировано в буфер обмена', {
              autoHideDuration: 3000,
              variant: 'info',
              anchorOrigin: { horizontal: 'right', vertical: 'top' },
            })
          )
        }
      >
        <Copy />
      </div>
    </div>
  );
};

export const InputChip: React.FC<ChipProps> = ({
  label,
  onDelete,
  disabled,
  ...otherProps
}) => {
  if (!label) return <></>;

  return (
    <Chip
      {...otherProps}
      size="small"
      label={<ChipContent label={label} />}
      className={styles.chip}
      onDelete={!disabled ? onDelete : undefined}
      classes={{ disabled: styles.chip_disabled }}
    />
  );
};

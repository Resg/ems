import React from 'react';

import { TextField, TextFieldProps } from '@mui/material';

import styles from './styles.module.scss';

export type InputProps = TextFieldProps & {
  notEditableField?: boolean;
};

const EmsInput: React.FC<InputProps> = React.memo(
  ({
    required,
    notEditableField,
    disabled,
    InputProps = {},
    InputLabelProps = {},
    rows = 3,
    ...restProps
  }) => {
    const inputProps = {
      ...InputProps,
      classes: {
        ...InputProps.classes,
        notchedOutline: required ? styles.requiredField : '',
        disabled: notEditableField
          ? styles.notEditableTextField
          : styles.editableTextField,
      },
    };

    const inputLabelProps = {
      ...InputLabelProps,
      classes: {
        root: required ? styles.requiredFieldLabel : '',
        focused: required ? styles.requiredFieldLabel : '',
      },
    };

    return (
      <TextField
        disabled={notEditableField || disabled}
        variant="outlined"
        size="small"
        required={required}
        {...restProps}
        InputProps={inputProps}
        InputLabelProps={inputLabelProps}
        sx={restProps.sx || { width: '100%' }}
        rows={rows}
        fullWidth={restProps.multiline}
      />
    );
  }
);

export default EmsInput;

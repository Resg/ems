import React from 'react';
import { ListChildComponentProps, VariableSizeList } from 'react-window';

import { Popper, PopperProps, Typography } from '@mui/material/';

import { useGetDictionaries } from '../../services/api/dictionaries/useGetDictionaries';
import { clerksToOptionsAdapter } from '../../utils/dictionaries';
import { ComboBox, ComboBoxPropsType } from '../ComboBox';

export type ClerksListProps = Omit<ComboBoxPropsType, 'options'>;

const LISTBOX_PADDING = 8;

function renderRow(props: ListChildComponentProps) {
  const { data, index, style } = props;
  const dataSet = data[index];
  const inlineStyle = {
    ...style,
    top: (style.top as number) + LISTBOX_PADDING,
  };

  return (
    <Typography
      key={dataSet[1].clerkId}
      component="li"
      {...dataSet[0]}
      noWrap
      style={inlineStyle}
    >
      {dataSet[1].clerkName}
    </Typography>
  );
}

const OuterElementContext = React.createContext({});

const OuterElementType = React.forwardRef<HTMLDivElement>((props, ref) => {
  const outerProps = React.useContext(OuterElementContext);
  return <div ref={ref} {...props} {...outerProps}></div>;
});

function useResetCache(data: any) {
  const ref = React.useRef<VariableSizeList>(null);

  React.useEffect(() => {
    if (ref.current != null) {
      ref.current.resetAfterIndex(0, true);
    }
  }, [data]);

  return ref;
}

const Listbox = React.forwardRef<
  HTMLDivElement,
  React.HTMLAttributes<HTMLElement>
>(function Listbox(props, ref) {
  const { children, ...other } = props;
  const itemData = children as React.ReactNode[];
  const itemCount = itemData.length;
  const itemSize = 36;
  const gridRef = useResetCache(itemCount);

  const getHeight = () => {
    if (itemCount > 8) {
      return 8 * itemSize;
    }

    return itemSize * itemData.length;
  };

  return (
    <div ref={ref}>
      <OuterElementContext.Provider value={other}>
        <VariableSizeList
          itemData={itemData}
          height={getHeight() + 2 * LISTBOX_PADDING}
          width="100%"
          ref={gridRef}
          outerElementType={OuterElementType}
          innerElementType="ul"
          itemSize={() => itemSize}
          overscanCount={5}
          itemCount={itemCount}
        >
          {renderRow}
        </VariableSizeList>
      </OuterElementContext.Provider>
    </div>
  );
});

const StyledPopper: React.FC<PopperProps> = (props) => (
  <Popper
    sx={{
      '& .MuiAutocomplete-listbox': {
        boxSizing: 'border-box',

        '& ul': {
          padding: 0,
          margin: 0,
        },
      },
    }}
    {...props}
  />
);

export const ClerksList: React.FC<ClerksListProps> = (props) => {
  const { dictionariesClerks } = useGetDictionaries({});

  return (
    <ComboBox
      PopperComponent={StyledPopper}
      ListboxComponent={Listbox}
      options={clerksToOptionsAdapter(dictionariesClerks)}
      renderOption={(props, option) => [props, option]}
      {...props}
    />
  );
};

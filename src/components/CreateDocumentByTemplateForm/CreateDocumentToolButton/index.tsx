import React, { useCallback, useMemo, useState } from 'react';
import cn from 'classnames';
import { useSelector } from 'react-redux';

import { Close, Minimize, WebAsset } from '@mui/icons-material';
import { Dialog, IconButton, Stack } from '@mui/material';

import { RootState, useAppDispatch } from '../../../store';
import {
  setDocumentCreateFormOpen,
  setDocumentPreviewFormOpen,
} from '../../../store/utils';
import Card from '../../Card';
import { ToolButton, ToolButtonProps } from '../../ToolsPanel';
import { CreateDocumentProgressModal } from '../CreateDocumentProgressModal';
import { CreateDocumentByTemplateForm } from '../index';

import styles from './styles.module.scss';

export interface CreateDocumentToolButtonProps extends ToolButtonProps {
  objectTypeCode?: string | null;
  precreationObjectTypeCode?: string | null;
  objectIds?: string | null;
  onDocumentCreated?: (ids: number[]) => void;
  isPreview?: boolean;
  id?: string;
}

export const CreateDocumentToolButton: React.FC<
  CreateDocumentToolButtonProps
> = ({
  objectIds,
  objectTypeCode,
  precreationObjectTypeCode,
  onDocumentCreated,
  isPreview,
  id = 'create-document-btn',
  ...toolButtonProps
}) => {
  const open = useSelector((state: RootState) =>
    isPreview
      ? state.utils.isDocumentPreviewFormOpen[id]
      : state.utils.isDocumentCreateFormOpen[id]
  );
  const dispatch = useAppDispatch();
  const [modalProgress, setModalProgress] = useState(false);
  const [fullScreen, setFullScreen] = useState(false);
  const [progress, setProgress] = useState(0);
  const [error, setError] = useState(0);
  const maxCounter = useMemo(
    () => objectIds?.split(',').length || 0,
    [objectIds]
  );
  const progressCounter = useMemo(() => {
    if (objectIds) {
      return (progress / maxCounter) * 100;
    }
    return 0;
  }, [progress, maxCounter]);

  const handleOpen = useCallback(() => {
    isPreview
      ? dispatch(setDocumentPreviewFormOpen({ [id]: true }))
      : dispatch(setDocumentCreateFormOpen({ [id]: true }));
  }, [isPreview]);

  const handleClose = useCallback(
    (e: React.MouseEvent) => {
      e.stopPropagation();
      isPreview
        ? dispatch(setDocumentPreviewFormOpen({ [id]: false }))
        : dispatch(setDocumentCreateFormOpen({ [id]: false }));
    },
    [isPreview]
  );

  const handleModalProgress = useCallback((value: boolean) => {
    setModalProgress(value);
  }, []);

  const handleError = useCallback((value: number) => {
    setError(value);
  }, []);

  const handleProgress = useCallback((value: number) => {
    setProgress(value);
  }, []);

  return (
    <>
      <ToolButton {...toolButtonProps} onClick={handleOpen} />
      {toolButtonProps.variant !== 'menu' && (
        <Dialog
          open={open}
          maxWidth={fullScreen ? false : 'lg'}
          PaperProps={{ className: styles.dialog }}
        >
          <Card
            className={cn({
              [styles.card]: true,
              [styles.card_fullscreen]: fullScreen,
            })}
          >
            <Stack
              direction="row"
              justifyContent="flex-end"
              alignItems="center"
              sx={{ mb: -7, pr: 2 }}
            >
              <IconButton sx={{ mt: 2 }}>
                {fullScreen ? (
                  <Minimize onClick={() => setFullScreen(!fullScreen)} />
                ) : (
                  <WebAsset onClick={() => setFullScreen(!fullScreen)} />
                )}
              </IconButton>
              <IconButton sx={{ mt: 2 }}>
                <Close onClick={handleClose} />
              </IconButton>
            </Stack>
            <CreateDocumentByTemplateForm
              objectIds={objectIds}
              objectTypeCode={objectTypeCode}
              precreationObjectTypeCode={precreationObjectTypeCode}
              onCancel={() =>
                isPreview
                  ? dispatch(setDocumentPreviewFormOpen({ [id]: false }))
                  : dispatch(setDocumentCreateFormOpen({ [id]: false }))
              }
              onDocumentCreated={onDocumentCreated}
              isPreview={isPreview}
              setModalProgress={handleModalProgress}
              setProgress={handleProgress}
              setError={handleError}
            />
          </Card>
        </Dialog>
      )}
      <CreateDocumentProgressModal
        openModal={modalProgress}
        onClose={() => handleModalProgress(false)}
        progressCounter={progressCounter}
        maxCounter={maxCounter}
        error={error}
      />
    </>
  );
};

import React from 'react';

import { Check } from '@mui/icons-material';
import { Button } from '@mui/material';

import { LinearProgressWithLabel } from '../../LinearProgressWithLabel';
import { Popup } from '../../Popup';

import styles from './styles.module.scss';

type CreateDocumentProgressModalProps = {
  openModal: boolean;
  onClose: () => void;
  progressCounter: number;
  maxCounter: number;
  error: number;
  onCancel?: () => void;
};

export const CreateDocumentProgressModal: React.FC<
  CreateDocumentProgressModalProps
> = ({ openModal, onClose, progressCounter, maxCounter, error, onCancel }) => {
  return (
    <Popup
      open={Boolean(openModal)}
      onClose={onClose}
      title={'Прогресс обработки'}
      height={'auto'}
      closeIcon={false}
    >
      <div>
        <LinearProgressWithLabel
          label={`${Math.ceil(
            (progressCounter / 100) * maxCounter
          )} / ${maxCounter}`}
          value={progressCounter}
        />
        {progressCounter === 100 && error ? (
          <p className={styles.message}>При формировании произошла ошибка</p>
        ) : null}
        {progressCounter === 100 ? (
          <p className={styles.message}>
            Создано {Math.ceil((progressCounter / 100) * maxCounter) - error} из{' '}
            {maxCounter}
          </p>
        ) : null}
        {progressCounter === 100 ? (
          <div className={styles.btn}>
            <Button variant="contained" startIcon={<Check />} onClick={onClose}>
              ОК
            </Button>
          </div>
        ) : null}
      </div>
    </Popup>
  );
};

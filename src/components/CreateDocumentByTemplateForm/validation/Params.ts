import { number, object, string } from 'yup';

import { CreateDocumentFormFields } from '../../../constants/createDocumentByTemplate';

export const ParamsValidation = object({
  [CreateDocumentFormFields.STATUS]: string().required(),
  [CreateDocumentFormFields.VIEW]: number().required(),
  [CreateDocumentFormFields.TYPE]: string().required(),
  [CreateDocumentFormFields.SIGN]: string().required(),
});

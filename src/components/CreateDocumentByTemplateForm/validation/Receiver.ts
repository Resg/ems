import { array, object } from 'yup';

import { CreateDocumentFormFields } from '../../../constants/createDocumentByTemplate';

export const ReceiverValidation = object({
  [CreateDocumentFormFields.CONTRACTORS]: array().required(),
});

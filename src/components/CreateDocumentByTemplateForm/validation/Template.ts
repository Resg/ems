import { number, object } from 'yup';

import { CreateDocumentFormFields } from '../../../constants/createDocumentByTemplate';

export const TemplateValidation = object({
  [CreateDocumentFormFields.RUBRIC]: number().required('Обязательное поле'),
  [CreateDocumentFormFields.TEMPLATE]: number().required('Обязательное поле'),
  [CreateDocumentFormFields.RECEIVER]: number().nullable(),
  [CreateDocumentFormFields.SIGNER]: number()
    .when(CreateDocumentFormFields.PARAMETERS_CODE, {
      is: (value: string | null) => value !== null,
      then: number().required('Обязательное поле'),
    })
    .nullable(),
});

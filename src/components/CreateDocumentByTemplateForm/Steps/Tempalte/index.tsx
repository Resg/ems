import React, { useCallback, useEffect, useMemo } from 'react';
import { useFormikContext } from 'formik';
import { get } from 'lodash';
import { useSearchParams } from 'react-router-dom';

import { Stack } from '@mui/material';

import {
  CreateDocumentFormFields,
  CreateDocumentFormLabels,
  CreateDocumentSteps,
} from '../../../../constants/createDocumentByTemplate';
import {
  useGetClerksQuery,
  useGetDocumentRubricsOptionsQuery,
  useGetDocumentTemplatesTreeQuery,
} from '../../../../services/api/dictionaries';
import { AutoCompleteInput } from '../../../AutoCompleteInput';
import { ClerkInput } from '../../../ClerkInput';

import { TemplateInput } from './TemplateInput';

import styles from './styles.module.scss';

export interface TemplateProps {
  step: CreateDocumentSteps;
  objectTypeCode?: string | null;
  precreationObjectTypeCode?: string | null;
}

export const Template: React.FC<TemplateProps> = ({
  step,
  objectTypeCode: typeCode,
  precreationObjectTypeCode: precreationCode,
}) => {
  const [searchParams] = useSearchParams();
  const { setFieldValue, values } = useFormikContext<any>();

  const parametersCode = values[CreateDocumentFormFields.PARAMETERS_CODE];
  const objectTypeCode = typeCode || searchParams.get('objectTypeCode') || '';

  const precreationObjectTypeCode =
    precreationCode || searchParams.get('precreationObjectTypeCode');

  const objStorage = objectTypeCode
    ? JSON.parse(
        localStorage?.getItem(precreationObjectTypeCode || objectTypeCode) ||
          '{}'
      )
    : null;

  const rubric = useMemo(
    () => get(values, CreateDocumentFormFields.RUBRIC),
    [values]
  );

  const template = useMemo(
    () => get(values, CreateDocumentFormFields.TEMPLATE),
    [values]
  );

  const stopPropagation = useCallback(
    (e: React.KeyboardEvent<HTMLDivElement>) => e.stopPropagation(),
    []
  );

  const { data: RubricsOptions = [] } = useGetDocumentRubricsOptionsQuery({
    objectTypeCode: precreationObjectTypeCode || objectTypeCode,
  });
  const rubricIds = useMemo(() => get(values, 'rubricIds'), [values]);
  const { data: templates } = useGetDocumentTemplatesTreeQuery(
    { rubricId: rubricIds, sort: 'parentId, description' },
    { skip: !rubricIds }
  );

  const rubricSelected = useMemo(() => {
    return RubricsOptions.find((opt) => opt.value === rubric);
  }, [RubricsOptions, rubric]);

  const templateSelected = useMemo(() => {
    return templates?.data.find((temp) => temp.templateId === template);
  }, [templates, template]);

  const { data: clerks = [] } = useGetClerksQuery({});
  const templateObjectTypes = useMemo(() => {
    return {
      RUBRIC: rubric || values[CreateDocumentFormFields.RUBRIC],
      TEMPLATE:
        templateSelected?.templateId ||
        values[CreateDocumentFormFields.TEMPLATE],
      TITLE:
        templateSelected?.description || values[CreateDocumentFormFields.TITLE],
      TYPE_ID:
        templateSelected?.documentTypeId ||
        values[CreateDocumentFormFields.TYPE_ID],
      REGISTRATOR:
        templateSelected?.documentTypeId === 23
          ? clerks.find((clerk) => clerk.isDefaultRegistrar)?.value
          : values[CreateDocumentFormFields.REGISTRATOR],
      PARAMETERS_CODE:
        templateSelected?.parametersCode ||
        values[CreateDocumentFormFields.PARAMETERS_CODE],
    };
  }, [values, objectTypeCode, rubric, templateSelected]);

  useEffect(() => {
    setFieldValue(CreateDocumentFormFields.RUBRIC, objStorage?.RUBRIC);
    setFieldValue(CreateDocumentFormFields.TEMPLATE, objStorage?.TEMPLATE);
    setFieldValue(CreateDocumentFormFields.TITLE, objStorage?.TITLE);
    setFieldValue(CreateDocumentFormFields.TYPE_ID, objStorage?.TYPE_ID);
    setFieldValue(
      CreateDocumentFormFields.REGISTRATOR,
      objStorage?.REGISTRATOR
    );
    setFieldValue(
      CreateDocumentFormFields.PARAMETERS_CODE,
      objStorage?.PARAMETERS_CODE
    );
  }, []);

  useEffect(() => {
    localStorage.setItem(
      precreationObjectTypeCode || objectTypeCode,
      JSON.stringify(templateObjectTypes)
    );
  }, [templateObjectTypes]);

  useEffect(() => {
    if (parametersCode === 'COVER_LETTER') {
      setFieldValue(
        CreateDocumentFormFields.SIGNER,
        clerks.find((clerk) => clerk.isDefaultOutgoingDocumentsSigner)?.value
      );
    }
  }, [parametersCode, rubric, template]);

  const handleChange = useCallback(() => {
    setFieldValue(CreateDocumentFormFields.TEMPLATE, null);
    setFieldValue(CreateDocumentFormFields.PARAMETERS_CODE, null);
    setFieldValue(CreateDocumentFormFields.TITLE, null);
    setFieldValue(CreateDocumentFormFields.TYPE_ID, null);
  }, [setFieldValue]);

  if (step !== CreateDocumentSteps.TEMPLATE) {
    return null;
  }

  return (
    <div className={styles.container}>
      <AutoCompleteInput
        className={styles.input}
        name={CreateDocumentFormFields.RUBRIC}
        label={CreateDocumentFormLabels[CreateDocumentFormFields.RUBRIC]}
        options={RubricsOptions}
        onInputChange={handleChange}
        onKeyDown={stopPropagation}
        required
      />
      <TemplateInput />
      <Stack spacing={3} sx={parametersCode ? { my: 3 } : { mt: 3 }}>
        {parametersCode === 'INTERNAL' && (
          <ClerkInput
            name={CreateDocumentFormFields.RECEIVER}
            label={CreateDocumentFormLabels[CreateDocumentFormFields.RECEIVER]}
          />
        )}
        {parametersCode && (
          <ClerkInput
            name={CreateDocumentFormFields.SIGNER}
            label={CreateDocumentFormLabels[CreateDocumentFormFields.SIGNER]}
            required
          />
        )}
      </Stack>
    </div>
  );
};

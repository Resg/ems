import React, { useCallback, useMemo, useState } from 'react';
import cn from 'classnames';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { ChevronRight } from '@mui/icons-material';

import { CreateDocumentFormFields } from '../../../../../constants/createDocumentByTemplate';
import { useGetClerksQuery } from '../../../../../services/api/dictionaries';
import { DocumentTemplate } from '../../../../../types/dictionaries';
import { DocIcon } from '../../../../Icons';

import styles from './styles.module.scss';

export interface RowProps {
  row: DocumentTemplate;
  rowChildren: DocumentTemplate[];
}

export const Row: React.FC<RowProps> = ({ row, rowChildren }) => {
  const [open, setOpen] = useState(true);
  const { values, setFieldValue } = useFormikContext<any>();
  const selected = useMemo(
    () => get(values, CreateDocumentFormFields.TEMPLATE) || '',
    [values]
  );
  const { data: clerks = [] } = useGetClerksQuery({});

  const chevronStyles = useMemo(
    () =>
      ({
        '--rotate': open ? '90deg' : '0',
      } as React.CSSProperties),
    [open]
  );

  const handleClickRow = useCallback(() => setOpen(!open), [open]);
  const handleClickChild = useCallback(
    (id: number) => () => {
      const template = rowChildren?.find((template) => template.id === id);
      setFieldValue(CreateDocumentFormFields.TEMPLATE, template?.templateId);
      setFieldValue(CreateDocumentFormFields.TITLE, template?.description);
      setFieldValue(CreateDocumentFormFields.TYPE_ID, template?.documentTypeId);
      if (template?.documentTypeId === 23) {
        setFieldValue(
          CreateDocumentFormFields.REGISTRATOR,
          clerks.find((clerk) => clerk.isDefaultRegistrar)?.value
        );
      }
      setFieldValue(
        CreateDocumentFormFields.PARAMETERS_CODE,
        template?.parametersCode
      );
    },
    [rowChildren, setFieldValue]
  );

  return (
    <>
      <div className={styles.row} onClick={handleClickRow}>
        <ChevronRight className={styles.chevron} style={chevronStyles} />
        {row?.description}
      </div>
      {open &&
        rowChildren?.map((child) => {
          return (
            <div
              key={child.id}
              className={cn(styles.row, styles.child)}
              onClick={handleClickChild(child.id)}
              aria-selected={child.templateId === selected}
            >
              <DocIcon className={styles.doc} />
              {child.description}
            </div>
          );
        })}
    </>
  );
};

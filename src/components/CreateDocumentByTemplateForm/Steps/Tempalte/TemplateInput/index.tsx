import React, { useMemo } from 'react';
import cn from 'classnames';
import { useFormikContext } from 'formik';
import { groupBy } from 'lodash';

import { useGetDocumentTemplatesTreeQuery } from '../../../../../services/api/dictionaries';

import { Row } from './Row';

import styles from './styles.module.scss';

export interface TemplateInputProps {
  className?: string;
}

export const TemplateInput: React.FC<TemplateInputProps> = ({ className }) => {
  const { values } = useFormikContext<any>();
  const { rubricIds } = values;
  const { data: templates } = useGetDocumentTemplatesTreeQuery(
    { rubricId: rubricIds, sort: 'parentId, description' },
    { skip: !rubricIds }
  );
  const grouped = useMemo(
    () => groupBy(templates?.data, 'parentId'),
    [templates?.data]
  );

  return (
    <div className={cn(styles.container, className)}>
      {templates &&
        rubricIds &&
        grouped[-1].map((parent) => {
          return (
            <Row
              row={parent}
              key={parent.itemId}
              rowChildren={grouped[parent.itemId]}
            />
          );
        })}
    </div>
  );
};

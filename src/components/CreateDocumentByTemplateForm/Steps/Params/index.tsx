import React from 'react';

import { Grid } from '@mui/material';

import {
  CreateDocumentFormFields,
  CreateDocumentFormLabels,
  CreateDocumentSteps,
} from '../../../../constants/createDocumentByTemplate';
import {
  useGetConclusionSortsQuery,
  useGetConclusionStatusesQuery,
  useGetConclusionTypesQuery,
} from '../../../../services/api/dictionaries';
import { AutoCompleteInput } from '../../../AutoCompleteInput';
import { ClerkInput } from '../../../ClerkInput';

export interface ParamsProps {
  step: CreateDocumentSteps;
}

export const Params: React.FC<ParamsProps> = ({ step }) => {
  const { data: StatusOptions = [] } = useGetConclusionStatusesQuery({});
  const { data: TypeOptions = [] } = useGetConclusionTypesQuery({});
  const { data: ViewOptions = [] } = useGetConclusionSortsQuery({});

  if (step !== CreateDocumentSteps.PARAMS) {
    return null;
  }
  return (
    <Grid container columnSpacing={9} rowSpacing={3}>
      <Grid item xs={6}>
        <AutoCompleteInput
          name={CreateDocumentFormFields.STATUS}
          label={CreateDocumentFormLabels[CreateDocumentFormFields.STATUS]}
          options={StatusOptions}
          required
        />
      </Grid>
      <Grid item xs={6}>
        <AutoCompleteInput
          name={CreateDocumentFormFields.TYPE}
          label={CreateDocumentFormLabels[CreateDocumentFormFields.TYPE]}
          options={TypeOptions}
          required
        />
      </Grid>
      <Grid item xs={6}>
        <AutoCompleteInput
          name={CreateDocumentFormFields.VIEW}
          label={CreateDocumentFormLabels[CreateDocumentFormFields.VIEW]}
          options={ViewOptions}
          required
        />
      </Grid>
      <Grid item xs={6}>
        <ClerkInput
          name={CreateDocumentFormFields.SIGN}
          label={CreateDocumentFormLabels[CreateDocumentFormFields.SIGN]}
          required
        />
      </Grid>
    </Grid>
  );
};

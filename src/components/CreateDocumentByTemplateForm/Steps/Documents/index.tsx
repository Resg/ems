import React, { useEffect, useMemo, useState } from 'react';
import { useFormikContext } from 'formik';
import { useSearchParams } from 'react-router-dom';

import { Refresh } from '@mui/icons-material/';

import {
  CreateDocumentFormFields,
  CreateDocumentSteps,
  DocumentsCols,
} from '../../../../constants/createDocumentByTemplate';
import { useGetIncludedDocumentsListQuery } from '../../../../services/api/document';
import { DataTable } from '../../../CustomDataGrid';
import Input from '../../../Input';
import { RoundButton } from '../../../RoundButton';
import { SettingsTypes, ToolsPanel } from '../../../ToolsPanel';

import styles from './styles.module.scss';

export interface DocumentsProps {
  step: CreateDocumentSteps;
  objectIds?: string | null;
  objectTypeCode?: string | null;
  precreationObjectTypeCode?: string | null;
}

export const Documents: React.FC<DocumentsProps> = ({
  step,
  objectTypeCode: typeCode,
  objectIds: ids,
  precreationObjectTypeCode: precreationCode,
}) => {
  const { values, setFieldValue } = useFormikContext<any>();
  const [searchParams] = useSearchParams();
  const objectIds = ids || searchParams.get('objectIds') || '';
  const objectTypeCode = typeCode || searchParams.get('objectTypeCode') || '';
  const precreationObjectTypeCode =
    precreationCode || searchParams.get('precreationObjectTypeCode');
  const { data, refetch, isLoading } = useGetIncludedDocumentsListQuery({
    templateId: values[CreateDocumentFormFields.TEMPLATE],
    objectIds,
    objectTypeCode: precreationObjectTypeCode || objectTypeCode,
  });

  const [tableFilters, setTableFilters] = useState(false);
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const [count, setCount] = useState<Record<number, number | null>>({});

  const cols = useMemo(
    () =>
      DocumentsCols.map((col) => {
        if (col.key === 'numberOfCopies') {
          return {
            ...col,
            formatter: (props: any) => {
              return (
                <Input
                  value={count[props.row.id]}
                  variant={'standard'}
                  onChange={(event) => {
                    const value = event.target.value;
                    setCount({
                      ...count,
                      [props.row.id]: value ? Number(value) : null,
                    });
                  }}
                  type={'number'}
                />
              );
            },
          };
        }
        return col;
      }),
    [count]
  );

  useEffect(() => {
    setFieldValue(
      CreateDocumentFormFields.DOCUMENTS,
      Object.keys(selected)
        .filter((key) => selected[Number(key)])
        .map((key: string) => ({
          id: Number(key),
          numberOfCopies: count[Number(key)] || 1,
        }))
    );
  }, [count, selected, setFieldValue]);

  if (step !== CreateDocumentSteps.DOCUMENTS) {
    return null;
  }
  return (
    <div className={styles.container}>
      <DataTable
        formKey={'documents'}
        showFilters={tableFilters}
        onRowSelect={setSelected}
        className={styles.table}
        cols={cols}
        rows={data?.data || []}
        loading={isLoading}
      >
        <ToolsPanel
          className={styles.tools}
          leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
          settings={[SettingsTypes.COLS]}
          setFilters={setTableFilters}
        />
      </DataTable>
    </div>
  );
};

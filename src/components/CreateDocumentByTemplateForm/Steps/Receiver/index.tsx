import React, { useCallback } from 'react';
import { useFormikContext } from 'formik';

import {
  CreateDocumentFormFields,
  CreateDocumentSteps,
} from '../../../../constants/createDocumentByTemplate';
import { ReceiverForm } from '../../../ReceiverForm';

export interface ReceiverProps {
  step: CreateDocumentSteps;
}

export const Receiver: React.FC<ReceiverProps> = ({ step }) => {
  const { setFieldValue } = useFormikContext<any>();

  const handleChange = useCallback(
    (value: Record<string, any>[]) => {
      setFieldValue(
        CreateDocumentFormFields.CONTRACTORS,
        value.map(
          ({
            contractorId,
            contractorContactPersonId,
            deliveryId,
            deliveryTypeCode,
            contactId,
          }) => ({
            contractorId,
            contractorContactPersonId,
            contractorDeliveryId: deliveryId,
            contractorAddressDeliveryTypeCode: deliveryTypeCode,
            contractorContactId: contactId,
          })
        )
      );
    },
    [setFieldValue]
  );

  if (step !== CreateDocumentSteps.RECEIVER) {
    return null;
  }

  return (
    <div>
      <ReceiverForm onChange={handleChange} />
    </div>
  );
};

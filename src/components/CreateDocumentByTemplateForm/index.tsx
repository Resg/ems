import React, { useCallback, useMemo, useState } from 'react';
import cn from 'classnames';
import { Form, Formik } from 'formik';
import { useSelector } from 'react-redux';

import { ArrowBack, ArrowForward, Check, Clear } from '@mui/icons-material';
import { Button, Step, StepLabel, Stepper } from '@mui/material';

import {
  CreateDocumentFormFields,
  CreateDocumentStepLabels,
  CreateDocumentSteps,
} from '../../constants/createDocumentByTemplate';
import { useCreateConclusionsCardMutation } from '../../services/api/conclusions';
import { useCreateDocumentMutation } from '../../services/api/document';
import { useGetUserQuery } from '../../services/api/user';
import { RootState, useAppDispatch } from '../../store';
import { addBreadCrumb } from '../../store/utils';
import { ProgressCircular } from '../ProgressCircular';

import { Documents } from './Steps/Documents';
import { Params } from './Steps/Params';
import { Receiver } from './Steps/Receiver';
import { Template } from './Steps/Tempalte';
import { ParamsValidation } from './validation/Params';
import { ReceiverValidation } from './validation/Receiver';
import { TemplateValidation } from './validation/Template';

import styles from './styles.module.scss';

export interface CreateDocumentByTemplateFormProps {
  className?: string;
  objectTypeCode?: string | null;
  objectIds?: string | null;
  onDocumentCreated?: (ids: number[]) => void;
  precreationObjectTypeCode?: string | null;
  isPreview?: boolean;
  onCancel?: () => void;
  setModalProgress?: (value: boolean) => void;
  setProgress?: (value: number) => void;
  setError?: (value: number) => void;
}

export const CreateDocumentByTemplateForm: React.FC<
  CreateDocumentByTemplateFormProps
> = ({
  className,
  onCancel,
  onDocumentCreated,
  precreationObjectTypeCode,
  objectTypeCode = 'COM',
  objectIds = '1',
  setModalProgress,
  setProgress,
  setError,
}) => {
  const dispatch = useAppDispatch();
  const isConclusion = precreationObjectTypeCode === 'CONCLUSION';
  const [step, setStep] = useState<CreateDocumentSteps>(
    isConclusion ? CreateDocumentSteps.PARAMS : CreateDocumentSteps.TEMPLATE
  );
  const [openModal, setOpenModal] = useState(false);
  const [success, setSuccess] = useState(false);
  const currentStep = useMemo(
    () => (isConclusion ? step : step - 1),
    [step, isConclusion]
  );
  const objStorage = objectTypeCode
    ? JSON.parse(
        localStorage?.getItem(precreationObjectTypeCode || objectTypeCode) ||
          '{}'
      )
    : null;

  const { data: user } = useGetUserQuery({});
  const [createDocument] = useCreateDocumentMutation();
  const [createConclusionsCard] = useCreateConclusionsCardMutation();
  const handleNext = useCallback(
    (value = 1) => {
      setStep(step + value);
    },
    [step]
  );

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const handleSubmit = useCallback(
    async (values: any) => {
      if (precreationObjectTypeCode === 'CONCLUSION') {
        onCancel?.();
        setModalProgress?.(true);
        const documentsData = objectIds?.split(',').map((el: string) => {
          return {
            ...values,
            rubricIds: values.rubricIds && [values.rubricIds],
            signerIds: values.signerIds && [values.signerIds],
            innerAddressees: values.innerAddressees && [values.innerAddressees],
            objectId: el,
          };
        });
        if (documentsData) {
          let counter = 0;
          let counterError = 0;
          setProgress?.(0);
          await Promise.all(
            documentsData.map(async (documentData: any) => {
              const response = await createConclusionsCard(documentData);
              counter++;
              setProgress?.(counter);
              if ('error' in response) {
                counterError++;
                setError?.(counterError);
              }
            })
          );
        }
      } else {
        const ids = objectIds?.split(',');
        const documentData = {
          ...values,
          rubricIds: values.rubricIds && [values.rubricIds],
          signerIds: values.signerIds && [values.signerIds],
          innerAddressees: values.innerAddressees && [values.innerAddressees],
          objectId: ids && Number(ids[ids?.length - 1]),
          requestIds: ids && ids.map(Number),
        };
        setOpenModal(true);

        const response = await createDocument({ documentData });

        if ('data' in response) {
          setSuccess(true);
          if (response.data.id) {
            if (!openFormInNewTab) {
              dispatch(
                addBreadCrumb([
                  {
                    pathname: `/documents?id=${response.data.id}`,
                    title: `Документ ШК: ${response.data.id}`,
                    type: 'object',
                  },
                ])
              );
            }
          }

          onDocumentCreated?.([response.data.id]);

          window.open(
            `/file_editor/${response.data.createdFileId}`,
            openFormInNewTab ? '_blank' : '_self'
          );

          onCancel?.();
        }
      }
    },
    [
      createDocument,
      onCancel,
      onDocumentCreated,
      precreationObjectTypeCode,
      objectIds,
      setModalProgress,
      setProgress,
      setError,
      openFormInNewTab,
    ]
  );

  const handleBack = useCallback(
    (value = 1) => {
      setStep(Math.max(step - value, 0));
    },
    [step]
  );

  const handleOpenModal = useCallback((value: boolean) => {
    setOpenModal(value);
  }, []);

  const validationSchema = useMemo(() => {
    switch (step) {
      case CreateDocumentSteps.PARAMS: {
        return ParamsValidation;
      }
      case CreateDocumentSteps.TEMPLATE: {
        return TemplateValidation;
      }
      case CreateDocumentSteps.RECEIVER: {
        return ReceiverValidation;
      }
    }
  }, [step]);

  return (
    <Formik
      initialValues={{
        methodOfCreation: 'BY_TEMPLATE',
        objectTypeCode,
        objectId: objectIds,
        precreationObjectTypeCode: precreationObjectTypeCode,
        documentAccessCode: 'COM',
        performer: {
          clerkId: user?.clerkId,
        },
        rubricIds: objStorage?.RUBRIC,
        parametersCode: objStorage?.PARAMETERS_CODE,
        signerIds: null,
        templateId: objStorage?.TEMPLATE,
        conclusionStatusCode: isConclusion ? 'N' : null,
        conclusionTypeCode: isConclusion ? 'Y' : null,
        conclusionSortId: isConclusion ? 1 : null,
        conclusionSignerId: null,
        outerAddressees: [],
        title: objStorage?.TITLE,
        typeId: objStorage?.TYPE_ID,
        clerkId: objStorage?.REGISTRATOR,
      }}
      onSubmit={handleSubmit}
      enableReinitialize
      validationSchema={validationSchema}
      validateOnMount
    >
      {({
        values,
        errors,
        handleSubmit,
        validateForm,
        setErrors,
        setTouched,
      }) => {
        return (
          <Form className={cn(styles.form, className)}>
            <div className={styles.container}>
              <div className={styles.header}>Создание документа</div>
              <Stepper activeStep={currentStep} className={styles.stepper}>
                {isConclusion && (
                  <Step>
                    <StepLabel
                      StepIconProps={{ className: styles['step-icon'] }}
                    >
                      {CreateDocumentStepLabels[CreateDocumentSteps.PARAMS]}
                    </StepLabel>
                  </Step>
                )}
                <Step>
                  <StepLabel StepIconProps={{ className: styles['step-icon'] }}>
                    {CreateDocumentStepLabels[CreateDocumentSteps.TEMPLATE]}
                  </StepLabel>
                </Step>
                {(values[CreateDocumentFormFields.PARAMETERS_CODE] ===
                  'COVER_LETTER' ||
                  values[CreateDocumentFormFields.PARAMETERS_CODE] ===
                    'COVER_LETTER_TO_POWER_AGENCIES') && (
                  <Step>
                    <StepLabel
                      StepIconProps={{ className: styles['step-icon'] }}
                    >
                      {CreateDocumentStepLabels[CreateDocumentSteps.RECEIVER]}
                    </StepLabel>
                  </Step>
                )}
                {values[CreateDocumentFormFields.PARAMETERS_CODE] && (
                  <Step>
                    <StepLabel
                      StepIconProps={{ className: styles['step-icon'] }}
                    >
                      {CreateDocumentStepLabels[CreateDocumentSteps.DOCUMENTS]}
                    </StepLabel>
                  </Step>
                )}
              </Stepper>
              <div className={styles.steps}>
                {isConclusion && <Params step={step} />}
                <Template
                  step={step}
                  objectTypeCode={objectTypeCode}
                  precreationObjectTypeCode={precreationObjectTypeCode}
                />
                <Receiver step={step} />
                <Documents
                  step={step}
                  objectTypeCode={objectTypeCode}
                  precreationObjectTypeCode={precreationObjectTypeCode}
                  objectIds={objectIds}
                />
              </div>
            </div>
            <div className={styles.actions}>
              {(isConclusion
                ? step !== CreateDocumentSteps.PARAMS
                : step !== CreateDocumentSteps.TEMPLATE) && (
                <Button
                  variant={'outlined'}
                  onClick={() => {
                    const parametersCode: string | null =
                      values[CreateDocumentFormFields.PARAMETERS_CODE];
                    if (
                      (step === CreateDocumentSteps.DOCUMENTS ||
                        step === CreateDocumentSteps.RECEIVER) &&
                      (parametersCode === 'COVER_LETTER' ||
                        parametersCode === 'COVER_LETTER_TO_POWER_AGENCIES')
                    ) {
                      handleBack();
                    } else {
                      handleBack(2);
                    }
                  }}
                  startIcon={<ArrowBack />}
                  className={styles.button}
                >
                  Назад
                </Button>
              )}
              {step !== CreateDocumentSteps.DOCUMENTS && (
                <Button
                  variant={
                    step === CreateDocumentSteps.PARAMS
                      ? 'contained'
                      : 'outlined'
                  }
                  onClick={() => {
                    validateForm(values).then((result) => {
                      setErrors(result);
                      const touched: Record<string, boolean> = {};
                      const resultKeys = Object.keys(result);
                      resultKeys.forEach((key) => (touched[key] = true));
                      setTouched(touched);
                      if (!resultKeys.length) {
                        const parametersCode: string | null =
                          values[CreateDocumentFormFields.PARAMETERS_CODE];
                        if (
                          step === CreateDocumentSteps.PARAMS ||
                          ((step === CreateDocumentSteps.TEMPLATE ||
                            step === CreateDocumentSteps.RECEIVER) &&
                            (parametersCode === 'COVER_LETTER' ||
                              parametersCode ===
                                'COVER_LETTER_TO_POWER_AGENCIES'))
                        ) {
                          handleNext();
                        } else {
                          handleNext(2);
                        }
                      }
                    });
                  }}
                  disabled={
                    (step == CreateDocumentSteps.PARAMS &&
                      (!values[CreateDocumentFormFields.STATUS] ||
                        !values[CreateDocumentFormFields.TYPE] ||
                        !values[CreateDocumentFormFields.VIEW] ||
                        !values[CreateDocumentFormFields.SIGN])) ||
                    (step == CreateDocumentSteps.RECEIVER &&
                      !values[CreateDocumentFormFields.CONTRACTORS].length) ||
                    (step == CreateDocumentSteps.TEMPLATE &&
                      (!values[CreateDocumentFormFields.PARAMETERS_CODE] ||
                        !values[CreateDocumentFormFields.SIGNER]))
                  }
                  endIcon={<ArrowForward />}
                  className={styles.button}
                >
                  Продолжить
                </Button>
              )}
              {step !== CreateDocumentSteps.PARAMS && (
                <Button
                  variant={'contained'}
                  disabled={
                    (step === CreateDocumentSteps.TEMPLATE &&
                      (!values[CreateDocumentFormFields.TEMPLATE] ||
                        !!values[CreateDocumentFormFields.PARAMETERS_CODE])) ||
                    (step == CreateDocumentSteps.RECEIVER &&
                      !values[CreateDocumentFormFields.CONTRACTORS].length)
                  }
                  onClick={() => handleSubmit()}
                  startIcon={<Check />}
                  className={styles.button}
                >
                  Сформировать
                </Button>
              )}
              <Button
                variant={'outlined'}
                onClick={() => onCancel?.()}
                startIcon={<Clear />}
                className={styles.button}
              >
                Отмена
              </Button>
            </div>
            <ProgressCircular
              openModal={openModal}
              onClose={() => handleOpenModal(false)}
              success={success}
            />
          </Form>
        );
      }}
    </Formik>
  );
};

import React, { useCallback, useEffect, useState } from 'react';

import { Check, ChevronLeft, ChevronRight } from '@mui/icons-material';
import { Button, Checkbox, FormControlLabel, Stack } from '@mui/material';

import { useCreateUserTableSettingsMutation } from '../../../services/api/userTableSettings';
import { useAppDispatch, useAppSelector } from '../../../store';
import {
  getDataColls,
  getDataGroups,
  getDataSorts,
  getDataTools,
  setDataTools,
} from '../../../store/userTableSettings';
import { Tool, useToolsContext } from '../ToolsContext';

import styles from './styles.module.scss';

interface ToolsProps {
  close: () => void;
}

interface ToolCheck {
  name: string;
  checked: boolean;
  disabled?: boolean;
}

const ToolsView: React.FC<ToolsProps> = ({ close }) => {
  const { setTools, formKey, ...context } = useToolsContext();

  const tools = Object.values(context.tools);
  const fast = tools.filter((tool) => tool.fast).map((tool) => tool.name);

  const dispatch = useAppDispatch();
  const dataTools = useAppSelector((state) => getDataTools(state, formKey));
  const dataColls = useAppSelector((state) => getDataColls(state, formKey));
  const dataGroups = useAppSelector((state) => getDataGroups(state, formKey));
  const dataSorts = useAppSelector((state) => getDataSorts(state, formKey));
  const [createUserTableSettings] = useCreateUserTableSettingsMutation();

  const toolsChecks: ToolCheck[] = [];
  const fastToolsChecks: ToolCheck[] = [];
  const [toTools, setToTools] = useState(toolsChecks);
  const [fromTools, setFromTools] = useState(fastToolsChecks);

  useEffect(() => {
    if (dataTools) {
      const changeTools = dataTools.reduce((acc, val) => {
        acc[val.name] = { name: val.name, fast: val.checked };
        return acc;
      }, {} as Record<string, Tool>);
      setTools(changeTools);
    }

    tools.forEach((tool) => {
      const isFast = Boolean(fast?.includes(tool.name));

      if (isFast) {
        fastToolsChecks.push({
          name: tool.name,
          checked: false,
        });
      }

      toolsChecks.push({
        name: tool.name,
        checked: isFast,
        disabled: isFast,
      });
    });
  }, [dataTools]);

  const handleToToolsCheckbox = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newTools = [...toTools];
    const tool = newTools.find((i) => i.name === e.target.name);

    if (tool) {
      tool.checked = e.target.checked;
      setToTools(newTools);
    }
  };

  const handleFromToolsCheckbox = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newTools = [...fromTools];
    const tool = newTools.find((i) => i.name === e.target.name);

    if (tool) {
      tool.checked = e.target.checked;
      setFromTools(newTools);
    }
  };

  const handleToTools = () => {
    const newTo = [...toTools];
    const newFrom = [...fromTools];

    newTo.forEach((to) => {
      if (to.checked && !to.disabled) {
        to.disabled = true;

        newFrom.push({
          name: to.name,
          checked: false,
          disabled: false,
        });
      }
    });

    setToTools(newTo);
    setFromTools(newFrom);
  };

  const handleFromTools = () => {
    let newFrom = [...fromTools];
    const newTo = [...toTools];

    newFrom.forEach((from) => {
      if (from.checked) {
        const toItem = newTo.find((i) => i.name === from.name);
        if (toItem) {
          toItem.checked = false;
          toItem.disabled = false;
        }

        newFrom = newFrom.filter((i) => i.name !== from.name);
      }
    });

    setFromTools(newFrom);
  };

  const handleApply = useCallback(() => {
    const newTools: Record<string, Tool> = {};
    toTools.forEach((tool) => {
      newTools[tool.name] = { name: tool.name, fast: tool.checked };
    });
    setTools(newTools);
    close();
    dispatch(setDataTools({ formKey: formKey, data: toTools }));

    if (toTools && formKey) {
      createUserTableSettings({
        formKey,
        data: {
          columnSettings: dataColls as any,
          sort: dataSorts,
          group: dataGroups,
          tools: toTools,
        },
      });
    }
  }, [toTools, setTools, close, dataColls, dataSorts, dataGroups]);

  return (
    <>
      <Stack
        direction="row"
        justifyContent="space-between"
        sx={{
          mt: 3,
          padding: '0 24px 24px',
          height: 512,
          boxSizing: 'border-box',
        }}
      >
        <div>
          <div className={styles.groupTitle}>Инструменты для выбора</div>
          <div className={styles.groupBox}>
            {toTools &&
              toTools.map((tool) => (
                <div key={`to-${tool.name}`}>
                  <FormControlLabel
                    label={tool.name}
                    control={
                      <Checkbox
                        name={tool.name as string}
                        checked={tool.checked}
                        disabled={tool.disabled}
                        onChange={handleToToolsCheckbox}
                      />
                    }
                  />
                </div>
              ))}
          </div>
        </div>
        <div className={styles.groupBtns}>
          <Button variant="outlined" sx={{ mb: 3 }} onClick={handleToTools}>
            <ChevronRight data-testid="right-tools" />
          </Button>
          <Button variant="outlined" onClick={handleFromTools}>
            <ChevronLeft data-testid="left-tools" />
          </Button>
        </div>
        <div>
          <div className={styles.groupTitle}>Отображаемые инструменты</div>
          <div className={styles.groupBox}>
            {fromTools &&
              fromTools.map((tool) => (
                <div key={`from-${tool.name}`}>
                  <FormControlLabel
                    label={tool.name}
                    control={
                      <Checkbox
                        name={tool.name as string}
                        checked={tool.checked}
                        onChange={handleFromToolsCheckbox}
                      />
                    }
                  />
                </div>
              ))}
          </div>
        </div>
      </Stack>
      <Stack
        direction="row"
        spacing={1.5}
        justifyContent="flex-end"
        alignItems="center"
        className={styles.popupBar}
      >
        <Button variant="contained" startIcon={<Check />} onClick={handleApply}>
          Применить
        </Button>
      </Stack>
    </>
  );
};

export default ToolsView;

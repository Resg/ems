import React, { useCallback, useEffect, useState } from 'react';

import { Check, ChevronLeft, ChevronRight } from '@mui/icons-material';
import { Button, Checkbox, FormControlLabel, Stack } from '@mui/material';

import { useCreateUserTableSettingsMutation } from '../../../services/api/userTableSettings';
import { useAppDispatch, useAppSelector } from '../../../store';
import {
  getDataColls,
  getDataGroups,
  getDataSorts,
  getDataTools,
  getUserTableSettings,
  setDataGroups,
} from '../../../store/userTableSettings';
import { useToolsContext } from '../ToolsContext';

import styles from './styles.module.scss';

export interface GroupsProps {
  close: () => void;
}

const GroupsView = ({ close }: GroupsProps) => {
  const { cols = [], groups = [], formKey, setGroups } = useToolsContext();

  const dispatch = useAppDispatch();
  const dataTools = useAppSelector((state) => getDataTools(state, formKey));
  const dataColls = useAppSelector((state) => getDataColls(state, formKey));
  const dataGroups = useAppSelector((state) => getDataGroups(state, formKey));
  const dataSorts = useAppSelector((state) => getDataSorts(state, formKey));
  const userTableSettings = useAppSelector((state) =>
    getUserTableSettings(state, formKey)
  );

  const [createUserTableSettings] = useCreateUserTableSettingsMutation();

  const [toGroups, setToGroups] = useState<any[]>([]);
  const [fromGroups, setFromGroups] = useState<any[]>([]);
  const [showGroupedQuantity, setShowGroupedQuantity] = useState(
    userTableSettings?.showGroupedQuantity || false
  );

  useEffect(() => {
    if (dataGroups) {
      setGroups?.(dataGroups);
    }

    const columns: any[] = [];
    const active: any[] = [];

    cols.forEach((col) => {
      const isGroup = groups.includes(col.key);

      if (isGroup) {
        active.push({
          name: col.name,
          checked: false,
        });
      } else {
        columns.push({
          name: col.name,
          checked: false,
        });
      }
    });

    setToGroups(columns);
    setFromGroups(active);
  }, [dataGroups, cols]);

  const handleApply = useCallback(() => {
    const grouping = fromGroups.map((group) => group.name);
    const groups = grouping.map((group) => {
      const key = cols.find((col) => col.name === group);

      if (key) {
        return key.key;
      } else {
        return '';
      }
    });
    setGroups?.(groups);

    if (formKey) {
      dispatch(setDataGroups({ formKey: formKey, data: groups }));
      createUserTableSettings({
        formKey,
        data: {
          columnSettings: dataColls as any,
          sort: dataSorts,
          group: groups,
          tools: dataTools,
          showGroupedQuantity,
        },
      });
    }

    close();
  }, [cols, fromGroups, setGroups, showGroupedQuantity]);

  const columnsChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newGroups = [...toGroups];
    const checkGroup = newGroups.find((i) => i.name === e.target.name);

    if (checkGroup) {
      checkGroup.checked = e.target.checked;
      setToGroups(newGroups);
    }
  };

  const activeChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newGroups = [...fromGroups];
    const checkGroup = newGroups.find((i) => i.name === e.target.name);

    if (checkGroup) {
      checkGroup.checked = e.target.checked;
      setFromGroups(newGroups);
    }
  };

  const handleToGroup = () => {
    const newGroups = [...fromGroups];
    let oldCols = [...toGroups];

    toGroups.forEach((col) => {
      if (col.checked) {
        col.checked = false;

        oldCols = oldCols.filter((i) => i.name !== col.name);

        newGroups.push(col);
      }
    });

    setToGroups(oldCols);
    setFromGroups(newGroups);
  };

  const handleFromGroup = () => {
    const newGroups = [...toGroups];
    let oldCols = [...fromGroups];

    fromGroups.forEach((col) => {
      if (col.checked) {
        col.checked = false;

        oldCols = oldCols.filter((i) => i.name !== col.name);

        newGroups.push(col);
      }
    });

    setFromGroups(oldCols);
    setToGroups(newGroups);
  };

  return (
    <>
      <Stack
        direction="row"
        justifyContent="space-between"
        sx={{
          mt: 3,
          height: 512,
          padding: '0 24px 24px',
          boxSizing: 'border-box',
        }}
      >
        <div>
          <div className={styles.groupTitle}>Все поля</div>
          <div className={styles.groupBox}>
            {toGroups.map((col) => (
              <div key={String(col.name)}>
                <FormControlLabel
                  label={col.name}
                  control={
                    <Checkbox
                      key={`groups-${col.name}`}
                      checked={col.checked}
                      name={`${col.name}`}
                      onChange={columnsChange}
                    />
                  }
                />
              </div>
            ))}
          </div>
        </div>
        <div className={styles.groupBtns}>
          <Button
            variant="outlined"
            sx={{ mb: 3 }}
            onClick={handleToGroup}
            disabled={fromGroups.length > 2}
          >
            <ChevronRight data-testid="right-group" />
          </Button>
          <Button variant="outlined" onClick={handleFromGroup}>
            <ChevronLeft data-testid="left-group" />
          </Button>
        </div>
        <div>
          <div className={styles.groupTitle}>Выбранные поля</div>
          <div className={styles.groupBox}>
            {fromGroups.map((group) => (
              <div key={group.name as string}>
                <FormControlLabel
                  label={group.name}
                  control={
                    <Checkbox
                      checked={group.checked}
                      name={`${group.name}`}
                      onChange={activeChange}
                    />
                  }
                />
              </div>
            ))}
          </div>
        </div>
      </Stack>
      <Stack
        sx={{
          padding: '0 24px 24px',
        }}
      >
        <FormControlLabel
          label={'Отображать количество в группах'}
          control={
            <Checkbox
              checked={showGroupedQuantity}
              onChange={(_, checked) => setShowGroupedQuantity(checked)}
            />
          }
        />
      </Stack>
      <Stack
        direction="row"
        spacing={1.5}
        justifyContent="flex-end"
        alignItems="center"
        className={styles.popupBar}
      >
        <Button variant="contained" startIcon={<Check />} onClick={handleApply}>
          Применить
        </Button>
      </Stack>
    </>
  );
};

export default GroupsView;

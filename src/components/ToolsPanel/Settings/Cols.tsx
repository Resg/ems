import React, { useCallback, useState } from 'react';

import { Check } from '@mui/icons-material';
import { Button, Checkbox, FormControlLabel, Stack } from '@mui/material';

import { useCreateUserTableSettingsMutation } from '../../../services/api/userTableSettings';
import { useAppDispatch, useAppSelector } from '../../../store';
import {
  getDataColls,
  getDataGroups,
  getDataSorts,
  getDataTools,
  setDataColls,
} from '../../../store/userTableSettings';
import { useToolsContext } from '../ToolsContext';

import styles from './styles.module.scss';

export interface ColsViewProps {
  close: () => void;
}

const Cols = ({ close }: ColsViewProps) => {
  const { cols = [], formKey, setCols } = useToolsContext();
  const [colsList, setColsList] = useState(cols);

  const dispatch = useAppDispatch();
  const dataTools = useAppSelector((state) => getDataTools(state, formKey));
  const dataColls = useAppSelector((state) => getDataColls(state, formKey));
  const dataGroups = useAppSelector((state) => getDataGroups(state, formKey));
  const dataSorts = useAppSelector((state) => getDataSorts(state, formKey));
  const [createUserTableSettings] = useCreateUserTableSettingsMutation();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newCols = [...colsList];
    const checkColIndex = newCols.findIndex((i) => i.name === e.target.name);

    if (checkColIndex !== -1) {
      const updatedCol = {
        ...newCols[checkColIndex],
        hidden: !e.target.checked,
      };
      newCols[checkColIndex] = updatedCol;

      setColsList(newCols);
    }
  };

  const handleApply = useCallback(() => {
    setCols?.(colsList);
    if (formKey && dataColls) {
      const result = dataColls.map((col) => {
        const check = colsList?.find((el) => el.name === col.name);
        return check ? { ...col, hidden: check.hidden } : col;
      });

      dispatch(
        setDataColls({
          formKey: formKey,
          data: result as any,
        })
      );
      createUserTableSettings({
        formKey,
        data: {
          columnSettings: result as any,
          sort: dataSorts,
          group: dataGroups,
          tools: dataTools,
        },
      });
    }
    close();
  }, [colsList, setCols, dataSorts, dataGroups, dataTools, dataColls]);

  return (
    <>
      <Stack direction="column" className={styles.scrollBox}>
        {colsList.map((col) => (
          <div key={col.key}>
            <FormControlLabel
              label={col.name}
              control={
                <Checkbox
                  key={`cols-${col.name}`}
                  checked={!col.hidden}
                  onChange={handleChange}
                  name={`${col.name}`}
                />
              }
            />
          </div>
        ))}
      </Stack>
      <Stack
        direction="row"
        spacing={1.5}
        justifyContent="flex-end"
        alignItems="center"
        className={styles.popupBar}
      >
        <Button variant="contained" startIcon={<Check />} onClick={handleApply}>
          Применить
        </Button>
      </Stack>
    </>
  );
};

export default Cols;

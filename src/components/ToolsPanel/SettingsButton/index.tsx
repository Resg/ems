import React, { useCallback, useState } from 'react';

import { Close, Settings } from '@mui/icons-material';
import { IconButton, Modal, Stack, Tab, Tabs } from '@mui/material';

import { RoundButton } from '../../RoundButton';
import { Cols, GroupsView, SettingsTypes, ToolsView } from '../Settings';

import styles from './styles.module.scss';

export interface SettingsButtonProps {
  settings: SettingsTypes[];
}

export const SettingsButton: React.FC<SettingsButtonProps> = ({ settings }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [tab, setTab] = useState<SettingsTypes>(settings[0]);

  const handleTabChange = useCallback(
    (e: React.SyntheticEvent, value: SettingsTypes) => setTab(value),
    []
  );

  const handleOpen = useCallback(() => setIsOpen(true), []);
  const handleClose = useCallback(() => setIsOpen(false), []);

  return (
    <div>
      <RoundButton icon={<Settings />} onClick={handleOpen} />
      <Modal open={isOpen} onClose={handleClose}>
        <div className={styles.popup}>
          <div className={styles.popupContent}>
            <Stack direction="row" spacing={1.5} justifyContent="space-between">
              <div className={styles.popupTitle}>Настройка формы</div>
              <Stack direction="row">
                <IconButton sx={{ mt: -1 }} onClick={handleClose}>
                  <Close />
                </IconButton>
              </Stack>
            </Stack>
            <Tabs
              className={styles.tabs}
              value={tab}
              onChange={handleTabChange}
            >
              {settings.map((setting) => (
                <Tab key={setting} label={setting} value={setting} />
              ))}
            </Tabs>
            {tab === SettingsTypes.GROUPS && <GroupsView close={handleClose} />}
            {tab === SettingsTypes.COLS && <Cols close={handleClose} />}
            {tab === SettingsTypes.TOOLS && <ToolsView close={handleClose} />}
          </div>
        </div>
      </Modal>
    </div>
  );
};

import React, { useCallback } from 'react';
import { useSelector } from 'react-redux';

import { Cancel, Delete } from '@mui/icons-material';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@mui/material';

import { RootState, useAppDispatch } from '../../../store';
import { setDeleteDialogOpen } from '../../../store/utils';
import Card from '../../Card';
import { ToolButton, ToolButtonProps } from '../ToolButton';

export interface DeleteToolButtonProps extends ToolButtonProps {
  title: string;
  description: string;
  onConfirm: () => void;
}

export const DeleteToolButton: React.FC<DeleteToolButtonProps> = ({
  title,
  description,
  onConfirm,
  ...toolButtonProps
}) => {
  const openList = useSelector(
    (state: RootState) => state.utils.isDeleteDialogOpen
  );
  const dispatch = useAppDispatch();
  const id = onConfirm.toString();

  const handleOpen = useCallback(
    (e: React.MouseEvent) => {
      dispatch(setDeleteDialogOpen({ [id]: true }));
    },
    [id]
  );

  const handleClose = useCallback(
    (e: React.MouseEvent) => {
      e.stopPropagation();
      dispatch(setDeleteDialogOpen({ [id]: false }));
    },
    [id]
  );

  const handleConfirm = useCallback(() => {
    onConfirm();
    dispatch(setDeleteDialogOpen({ [id]: false }));
  }, [onConfirm, id]);

  return (
    <>
      <ToolButton
        startIcon={<Delete />}
        {...toolButtonProps}
        onClick={handleOpen}
      />
      <Dialog open={openList[id]}>
        <Card>
          <DialogTitle>{title}</DialogTitle>
          <DialogContent>{description}</DialogContent>
          <DialogActions>
            <Button
              color={'error'}
              variant={'contained'}
              endIcon={<Delete />}
              onClick={handleConfirm}
            >
              Удалить
            </Button>
            <Button
              variant={'outlined'}
              endIcon={<Cancel />}
              onClick={handleClose}
            >
              Отмена
            </Button>
          </DialogActions>
        </Card>
      </Dialog>
    </>
  );
};

import React, { useCallback } from 'react';
import { useSelector } from 'react-redux';

import { Cancel } from '@mui/icons-material';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@mui/material';

import { RootState, useAppDispatch } from '../../../store';
import { setToolDialogOpen } from '../../../store/utils';
import Card from '../../Card';
import { ToolButton, ToolButtonProps } from '../ToolButton';

export interface ModalToolButtonProps extends ToolButtonProps {
  title: string;
  description: string;
  onConfirm: () => void;
  labelButton: string;
  startIcon?: React.ReactNode;
  endIcon?: React.ReactNode;
}

export const ModalToolButton: React.FC<ModalToolButtonProps> = ({
  title,
  description,
  onConfirm,
  labelButton,
  startIcon,
  endIcon,
  ...toolButtonProps
}) => {
  const open = useSelector((state: RootState) => state.utils.isToolDialogOpen);
  const dispatch = useAppDispatch();
  const id = onConfirm.toString();

  const handleOpen = useCallback(
    (e: React.MouseEvent) => {
      dispatch(setToolDialogOpen(true));
    },
    [id]
  );

  const handleClose = useCallback(
    (e: React.MouseEvent) => {
      e.stopPropagation();
      dispatch(setToolDialogOpen(false));
    },
    [id]
  );

  const handleConfirm = useCallback(() => {
    onConfirm();
    dispatch(setToolDialogOpen(false));
  }, [onConfirm, id]);

  return (
    <>
      <ToolButton
        startIcon={startIcon}
        {...toolButtonProps}
        onClick={handleOpen}
      />
      <Dialog open={open}>
        <Card>
          <DialogTitle>{title}</DialogTitle>
          <DialogContent>{description}</DialogContent>
          <DialogActions>
            <Button
              variant={'outlined'}
              endIcon={endIcon}
              onClick={handleConfirm}
            >
              {labelButton}
            </Button>
            <Button
              variant={'outlined'}
              endIcon={<Cancel />}
              onClick={handleClose}
            >
              Отмена
            </Button>
          </DialogActions>
        </Card>
      </Dialog>
    </>
  );
};

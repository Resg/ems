import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';

import { useAppSelector } from '../../../store';
import { getDataColls, getDataTools } from '../../../store/userTableSettings';
import { DefaultColumn } from '../../CustomDataGrid/types';

export interface Tool {
  name: string;
  fast: boolean;
}

interface ToolsContextState {
  tools: Record<string, Tool>;
  subTool: (tool: Tool) => void;
  setTools: (tools: Record<string, Tool>) => void;
  cols?: DefaultColumn[];
  setCols?: (cols: DefaultColumn[]) => void;
  groups?: string[];
  setGroups?: (groups: string[]) => void;
  formKey?: string;
}

export const ToolsContext = createContext<ToolsContextState>({
  tools: {},
  subTool: () => {},
  setTools: () => {},
});

export const useToolsContext = () => useContext(ToolsContext);

export interface ToolsContextProviderProps {
  children: React.ReactNode;
  cols?: DefaultColumn[];
  groups?: string[];
  formKey?: string;
}

export const ToolsContextProvider: React.FC<ToolsContextProviderProps> = ({
  children,
  cols,
  groups,
  formKey,
}) => {
  const toolOperator = useRef(new ToolOperator());

  const dataTools = useAppSelector((state) => getDataTools(state, formKey));
  const dataCols = useAppSelector((state) => getDataColls(state, formKey));

  const tools = toolOperator.current.tools;

  const [toolsState, setToolsState] = useState(tools);
  const [colsState, setColsState] = useState(cols);

  const subTool = useCallback(
    (tool: Tool) => {
      toolOperator.current.subTool(tool);
      setToolsState({ ...toolOperator.current.tools });
    },
    [setToolsState]
  );
  const setTools = useCallback(
    (tools: Record<string, Tool>) => {
      toolOperator.current.setTools(tools);
      setToolsState({ ...toolOperator.current.tools });
    },
    [setToolsState]
  );

  useEffect(() => {
    if (dataTools) {
      const changeTools = dataTools.reduce((acc, val) => {
        acc[val.name] = { name: val.name, fast: val.checked };
        return acc;
      }, {} as Record<string, Tool>);
      setTools(changeTools);
    }
  }, [dataTools]);

  useEffect(() => {
    if (dataCols) {
      setColsState?.(
        dataCols?.map((col) => ({
          key: col.key,
          name: col.name,
          hidden: col.hidden,
        }))
      );
    }
  }, [dataCols]);

  return (
    <ToolsContext.Provider
      value={{
        tools: toolsState,
        subTool,
        setTools,
        cols: colsState,
        groups,
        formKey,
      }}
    >
      {children}
    </ToolsContext.Provider>
  );
};

class ToolOperator {
  tools: Record<string, Tool> = {};
  subTool(tool: Tool) {
    this.tools[tool.name] = tool;
  }
  setTools(tools: Record<string, Tool>) {
    this.tools = tools;
  }
}

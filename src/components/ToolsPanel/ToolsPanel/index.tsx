import React, {
  cloneElement,
  CSSProperties,
  useCallback,
  useRef,
  useState,
} from 'react';
import cn from 'classnames';

import { FilterAlt, FilterAltOff, MoreVert } from '@mui/icons-material';
import { Menu } from '@mui/material';

import { DefaultColumn } from '../../CustomDataGrid/types';
import { HorizontalScroll } from '../../HorizontalScroll';
import { RoundButton } from '../../RoundButton';
import { SettingsTypes } from '../Settings';
import { SettingsButton } from '../SettingsButton';
import { ToolsContextProvider } from '../ToolsContext';

import styles from './styles.module.scss';

export interface ToolsPanelProps {
  children?: React.ReactNode;
  leftActions?: React.ReactNode;
  settings?: SettingsTypes[];
  cols?: DefaultColumn[];
  groups?: string[];
  className?: string;
  formKey?: string;
  setFilters?: (filters: boolean) => void;
}

export const ToolsPanel: React.FC<ToolsPanelProps> = ({
  formKey,
  groups,
  cols,
  settings = [SettingsTypes.TOOLS],
  children,
  leftActions,
  className,
  setFilters,
}) => {
  const [isOpenMenu, setIsOpenMenu] = useState(false);
  const [filters, setFiltersBtn] = useState(false);
  const buttonRef = useRef<HTMLButtonElement>(null);
  const leftRef = useRef<HTMLDivElement>(null);

  const handleOpenMenu = useCallback(() => {
    setIsOpenMenu(true);
  }, []);

  const handleCloseMenu = useCallback(() => {
    setIsOpenMenu(false);
  }, []);

  const rightContainerStyles = {
    '--max-width': `calc(100% - ${(leftRef.current?.clientWidth || 0) + 12}px)`,
  } as CSSProperties;

  return (
    <ToolsContextProvider cols={cols} groups={groups} formKey={formKey}>
      <div className={cn(styles.container, className)}>
        <div ref={leftRef} className={styles['left-container']}>
          {leftActions}
        </div>
        <div className={styles['right-container']} style={rightContainerStyles}>
          <HorizontalScroll maxWidth={`calc(100% - 96px)`}>
            {children}
          </HorizontalScroll>

          {children && (
            <RoundButton
              ref={buttonRef}
              icon={<MoreVert />}
              onClick={handleOpenMenu}
            />
          )}
          <Menu
            open={isOpenMenu}
            anchorEl={buttonRef.current}
            onClose={handleCloseMenu}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            className={styles.menu}
          >
            {children &&
              (Array.isArray(children) ? (
                children.map(
                  (child) =>
                    child && (
                      <div onClick={handleCloseMenu}>
                        {cloneElement(child, {
                          variant: 'menu',
                          key: child.props.label,
                        })}
                      </div>
                    )
                )
              ) : (
                <div onClick={handleCloseMenu}>
                  {cloneElement(children as React.ReactElement, {
                    variant: 'menu',
                  })}
                </div>
              ))}
          </Menu>
          {setFilters && (
            <RoundButton
              icon={filters ? <FilterAltOff /> : <FilterAlt />}
              onClick={() => {
                setFiltersBtn(!filters);
                setFilters(!filters);
              }}
            />
          )}
          <SettingsButton settings={settings} />
        </div>
      </div>
    </ToolsContextProvider>
  );
};

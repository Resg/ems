import React, { useState } from 'react';

import { Menu } from '@mui/material';

import { ToolButton, ToolButtonProps } from '..';

export const MenuToolButton: React.FC<ToolButtonProps> = ({
  children,
  ...toolButtonProps
}) => {
  const [anchorEl, setAnchorEl] = useState<Element | null>(null);

  const handleExpand = (e: React.MouseEvent) => {
    e.stopPropagation();
    setAnchorEl(e.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <ToolButton {...toolButtonProps} onClick={handleExpand} fast />
      <Menu
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={
          toolButtonProps.variant === 'menu'
            ? { vertical: 'top', horizontal: 'left' }
            : { vertical: 'bottom', horizontal: 'right' }
        }
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      >
        {children}
      </Menu>
    </>
  );
};

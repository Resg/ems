import React, { useEffect } from 'react';

import { Button, MenuItem } from '@mui/material';

import { useToolsContext } from '../ToolsContext';

import styles from './styles.module.scss';

export interface ToolButtonProps {
  variant?: 'button' | 'menu' | 'submenu';
  startIcon?: React.ReactNode;
  endIcon?: React.ReactNode;
  label: string;
  onClick?: (e: React.MouseEvent) => void;
  fast?: boolean;
  disabled?: boolean;
  children?: React.ReactNode;
}

export const ToolButton: React.FC<ToolButtonProps> = ({
  variant = 'button',
  startIcon,
  endIcon,
  onClick,
  label,
  disabled,
  children,
  fast: defaultFast = false,
}) => {
  const { tools, subTool } = useToolsContext();
  const currentTool = tools[label];
  const fast = currentTool?.fast;
  useEffect(() => {
    if (variant === 'button') {
      subTool({ name: label, fast: defaultFast });
    }
  }, [defaultFast, label, subTool, variant]);

  if (variant === 'button') {
    return (
      <Button
        startIcon={startIcon}
        disabled={disabled}
        variant="outlined"
        sx={{
          display: fast ? 'flex' : 'none',
          whiteSpace: 'nowrap',
          minWidth: label.length * 12 + 25,
          lineHeight: '24px',
          fontSize: 14,
          fontFamily: 'Roboto',
          fontWeight: 500,
          letterSpacing: 0.46,
          textTransform: 'uppercase',
        }}
        endIcon={endIcon}
        onClick={onClick}
      >
        {label}
        {children}
      </Button>
    );
  }

  if (['menu', 'submenu'].includes(variant)) {
    return (
      <MenuItem
        className={styles['menu-item']}
        onClick={onClick}
        disabled={disabled}
      >
        <div className={styles['menu-icon']}>{startIcon || endIcon}</div>
        {label}
        {children}
      </MenuItem>
    );
  }
  return null;
};

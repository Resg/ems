import React from 'react';
import { Link as ReactLink } from 'react-router-dom';

export type LinkProps = {
  children?: React.ReactNode;
  to: string;
  newTab?: boolean;
  className?: string;
  onClick?: (e?: React.MouseEvent) => void;
};

export const Link: React.FC<LinkProps> = ({
  children,
  to,
  newTab = false,
  className,
  onClick,
}) => {
  return (
    <ReactLink
      to={to}
      target={newTab ? '_blank' : '_self'}
      className={className}
      onClick={onClick}
    >
      {children}
    </ReactLink>
  );
};

import React from 'react';

import { ArrowBack, Close } from '@mui/icons-material';
import { IconButton, Modal, ModalProps, Stack } from '@mui/material';

import styles from './styles.module.scss';

interface PopupProps extends ModalProps {
  children: React.ReactElement;
  bar?: React.ReactNode;
  title?: string;
  onClose?: () => void;
  width?: string | number;
  height?: string | number;
  backButton?: boolean;
  onBack?: () => void;
  centred?: boolean;
  closeIcon?: boolean;
}

export const Popup: React.FC<PopupProps> = ({
  open,
  title = '',
  children,
  bar,
  onClose,
  width,
  height,
  backButton,
  onBack,
  centred,
  closeIcon = true,
  ...rest
}) => {
  const centredStyle: React.CSSProperties = centred
    ? {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        margin: '0 auto',
      }
    : {};

  return (
    <Modal open={open} {...rest}>
      <div
        className={styles.popup}
        style={{ width: width, height: height, ...centredStyle }}
      >
        <Stack
          direction="row"
          spacing={1.5}
          justifyContent="space-between"
          alignItems="center"
        >
          <Stack direction="row" spacing={1.5}>
            {backButton && (
              <IconButton sx={{ mt: -1 }}>
                <ArrowBack onClick={onBack} />
              </IconButton>
            )}
            <div className={styles.popupTitle}>{title}</div>
          </Stack>
          {closeIcon ? (
            <IconButton sx={{ mt: -1 }}>
              <Close onClick={onClose} />
            </IconButton>
          ) : null}
        </Stack>
        <div className={styles.contentBox}>{children}</div>
        {bar && <div className={styles.bar}>{bar}</div>}
      </div>
    </Modal>
  );
};

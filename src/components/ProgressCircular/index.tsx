import React from 'react';

import { Check } from '@mui/icons-material';
import { Button, CircularProgress, Fade, Typography } from '@mui/material';

import { Popup } from '../Popup';

import styles from './styles.module.scss';

type ProgressCircularProps = {
  openModal: boolean;
  success: boolean;
  onClose: () => void;
};

export const ProgressCircular: React.FC<ProgressCircularProps> = ({
  openModal,
  onClose,
  success = false,
}) => {
  return (
    <Popup
      open={openModal}
      onClose={onClose}
      title={'Обработка данных'}
      closeIcon={false}
    >
      <div className={styles.growColumn}>
        <div className={styles.container}>
          {success ? (
            <Typography>Завершено. Происходит переадресация</Typography>
          ) : (
            <Fade
              in={success === false}
              style={{
                transitionDelay: success ? '0ms' : '800ms',
              }}
              unmountOnExit
            >
              <CircularProgress size={80} />
            </Fade>
          )}
        </div>
        <div className={styles.btn}>
          <Button
            variant="contained"
            startIcon={<Check />}
            onClick={onClose}
            disabled={!success}
          >
            ОК
          </Button>
        </div>
      </div>
    </Popup>
  );
};

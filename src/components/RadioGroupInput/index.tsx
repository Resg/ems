import React, { useCallback } from 'react';
import { useFormikContext } from 'formik';

import { RadioGroup } from '@mui/material';

export interface RadioGroupInputProps {
  name: string;
  children: React.ReactNode;
  className?: string;
  row?: boolean;
  defaultValue?: any;
}

export const RadioGroupInput: React.FC<RadioGroupInputProps> = ({
  name,
  children,
  className,
  row,
  defaultValue,
}) => {
  const { values, setFieldValue } = useFormikContext<any>();

  const handleChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setFieldValue(name, event.target.value);
    },
    [name, setFieldValue]
  );

  return (
    <RadioGroup
      name={name}
      className={className}
      row={row}
      value={values[name]}
      onChange={handleChange}
      defaultValue={defaultValue}
    >
      {children}
    </RadioGroup>
  );
};

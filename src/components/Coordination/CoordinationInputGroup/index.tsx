import React, { useCallback, useState } from 'react';

import { Grid } from '@mui/material';

import {
  CoordinationCharacterType,
  CoordinationInput,
} from '../CoordinationInput';

interface CoordinationInputGroupProps {
  names: string[];
  charName?: string;
  labels: string[];
  type: 'latitude' | 'longtitude';
  charDependency?: boolean;
  xs?: number;
}

const typeChars = {
  latitude: ['N', 'S'] as CoordinationCharacterType[],
  longtitude: ['E', 'W'] as CoordinationCharacterType[],
};

export const CoordinationInputGroup: React.FC<CoordinationInputGroupProps> = ({
  names,
  charName,
  labels,
  type,
  charDependency = true,
  xs = 12,
}) => {
  const [character, setCharacter] = useState<CoordinationCharacterType>(
    typeChars[type][0]
  );

  const handleCharChange = useCallback(
    (newValue: CoordinationCharacterType) => {
      setCharacter((prevChar) => (newValue === prevChar ? prevChar : newValue));
    },
    []
  );

  return (
    <>
      <Grid item xs={xs / 2}>
        <CoordinationInput
          name={names[0]}
          charName={charName}
          label={labels[0]}
          type={type}
          character={character}
          onCharChange={charDependency ? handleCharChange : undefined}
        />
      </Grid>
      <Grid item xs={xs / 2}>
        <CoordinationInput
          name={names[1]}
          charName={charName}
          label={labels[1]}
          type={type}
          character={character}
          charDisabled={charDependency}
        />
      </Grid>
    </>
  );
};

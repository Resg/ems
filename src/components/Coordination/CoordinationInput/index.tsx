import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useFormikContext } from 'formik';
import { get } from 'lodash';
import InputMask from 'react-input-mask';

import { InputAdornment, TextFieldProps } from '@mui/material';

import Input from '../../Input';

import styles from './styles.module.scss';

export type CoordinationCharacterType = 'N' | 'S' | 'E' | 'W';

interface CoordinationInputProps {
  name: string;
  charName?: string;
  type: 'latitude' | 'longtitude';
  character?: CoordinationCharacterType;
  onCharChange?: (newValue: CoordinationCharacterType) => void;
  charDisabled?: boolean;
}

const typeChars = {
  latitude: ['N', 'S'] as CoordinationCharacterType[],
  longtitude: ['E', 'W'] as CoordinationCharacterType[],
};

export const CoordinationInput: React.FC<
  CoordinationInputProps & TextFieldProps & { notEditableField?: boolean }
> = ({
  name,
  charName,
  type,
  character = typeChars[type][0],
  onCharChange,
  charDisabled = false,
  ...restProps
}) => {
  const [inputValue, setInputValue] = useState('');
  const [inputCharacter, setInputCharacter] =
    useState<CoordinationCharacterType>(character);

  const { setFieldValue, values = {} as any } = useFormikContext();

  const value = Number(get(values, name));
  const charValue = charName && get(values, charName);

  const parseDecimal = useCallback(
    (value: number) => {
      const degree = Math.floor(value);
      const unparsedMinute = Number((value - degree).toFixed(5)) * 60;
      const unparsedSecond = Math.round(
        (unparsedMinute - Math.floor(unparsedMinute)) * 60
      );

      const minute = unparsedSecond >= 60 ? unparsedMinute + 1 : unparsedMinute;
      const second = unparsedSecond >= 60 ? 0 : unparsedSecond;

      return `${String(degree).padStart(
        type === 'latitude' ? 2 : 3,
        '0'
      )}°${String(Math.floor(minute)).padStart(2, '0')}'${String(
        second
      ).padStart(2, '0')}''`;
    },
    [type]
  );

  const handleBlur = useCallback(
    (event: React.FocusEvent<HTMLInputElement>) => {
      const value = event.target.value;
      const [degree, minute, second] = value.split(/°|''|'/);
      const decimalSign =
        inputCharacter === typeChars[type][0] || charName ? 1 : -1;
      const decimalValue =
        ((Number(degree) || 0) +
          (Number(minute?.replaceAll('_', '')) / 60 || 0) +
          (Number(second?.replaceAll('_', '')) / 3600 || 0)) *
        decimalSign;
      setFieldValue(name, decimalValue || undefined);
      decimalValue && setInputValue(parseDecimal(decimalValue));
    },
    [name, setFieldValue, inputCharacter, charName]
  );

  const handleChange = useCallback(
    (event: React.FocusEvent<HTMLInputElement>) => {
      const value = event.target.value;
      setInputValue((prev) => (value === prev ? prev : value));
    },
    []
  );

  const handleClick = useCallback(() => {
    if (charDisabled) return false;
    setInputCharacter((prev) => {
      return prev === typeChars[type][0]
        ? typeChars[type][1]
        : typeChars[type][0];
    });
  }, [charDisabled, type]);

  const mask = useMemo(() => {
    return type === 'latitude' ? "99°99'99''" : "999°99'99''";
  }, [type]);

  useEffect(() => {
    if (value) {
      setInputValue(parseDecimal(value));
    } else {
      setInputValue('');
    }
    if (charValue) {
      setInputCharacter((prev) => (prev === charValue ? prev : charValue));
    }
  }, [value, charValue]);

  useEffect(() => {
    if (value && !charName) {
      const newValue =
        inputCharacter === typeChars[type][0] ? Math.abs(value) : value * -1;
      setFieldValue(name, newValue);
    }
    if (value && charName) {
      setFieldValue(charName, inputCharacter);
    }
    onCharChange?.(inputCharacter);
  }, [inputCharacter]);

  useEffect(() => {
    setInputCharacter((prevChar) =>
      prevChar === character ? prevChar : character
    );
  }, [character]);

  const handleKeyDown = useCallback(
    (event: React.KeyboardEvent<HTMLInputElement>) => {
      const key = event.key.toUpperCase();
      let newChar: CoordinationCharacterType;
      const keycodes = {
        N: 'Т',
        S: 'Ы',
        E: 'У',
        W: 'Ц',
      };
      if (
        key === keycodes[typeChars[type][0]] ||
        key === 'ARROWUP' ||
        key === typeChars[type][0]
      ) {
        newChar = typeChars[type][0];
      } else if (
        key === keycodes[typeChars[type][1]] ||
        key === 'ARROWDOWN' ||
        key === typeChars[type][1]
      ) {
        newChar = typeChars[type][1];
      } else {
        return false;
      }
      setInputCharacter((prev) => (prev === newChar ? prev : newChar));
    },
    [type, setInputCharacter]
  );

  return (
    <InputMask
      mask={mask}
      maskChar="_"
      onChange={handleChange}
      onBlur={handleBlur}
      value={inputValue}
      onKeyDown={handleKeyDown}
    >
      <Input
        InputProps={{
          endAdornment: (
            <InputAdornment
              position="end"
              onClick={handleClick}
              className={
                charDisabled ? styles.adornmentDisabled : styles.adornment
              }
            >
              {inputCharacter}
            </InputAdornment>
          ),
        }}
        {...restProps}
      />
    </InputMask>
  );
};

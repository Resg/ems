import React, { ChangeEvent, useCallback, useMemo } from 'react';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { TextFieldProps } from '@mui/material';

import Input from '../Input';

export interface ListInputProps {
  name: string;
  separator?: 'coma' | 'line-break';
  caseInResults?: 'upper' | 'lower';
  listElementType?: 'string' | 'integer';
}

const separatorMap = {
  coma: ', ',
  'line-break': '\n',
};

export const ListInput: React.FC<ListInputProps & TextFieldProps> = ({
  name,
  separator = 'coma',
  caseInResults,
  listElementType = 'string',
  ...inputProps
}) => {
  const { setFieldValue, values, handleChange } = useFormikContext();
  const value = get(values, name);
  const newValue = useMemo(() => {
    return Array.isArray(value)
      ? value.join(separatorMap[separator])
      : value || '';
  }, [value]);

  const handleInput = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    if (listElementType === 'integer') {
      e.target.value = e.target.value.replace(/[^\d,\n ]/g, '');
    }
  }, []);

  const handleBlur = useCallback(
    (e: React.FocusEvent<HTMLInputElement>) => {
      const inputValue = e.target.value;
      let formValue;

      if (caseInResults === 'upper') {
        formValue = inputValue.toUpperCase();
      } else if (caseInResults === 'lower') {
        formValue = inputValue.toLowerCase();
      } else {
        formValue = inputValue;
      }

      formValue = formValue.trim().split(/[,;\s]+/);

      if (!formValue[formValue.length - 1]) formValue.pop();

      if (listElementType === 'integer') formValue = formValue.map(Number);

      setFieldValue(name, formValue);
    },
    [setFieldValue]
  );

  return (
    <Input
      name={name}
      onBlur={handleBlur}
      onChange={handleChange}
      onInput={handleInput}
      value={newValue}
      {...inputProps}
    />
  );
};

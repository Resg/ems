import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { useGetPersonalSettingsQuery } from '../../services/api/user';
import { setOpenFormInNewTab } from '../../store/utils';

export const Settings = () => {
  const dispatch = useDispatch();
  const { data: personalSettings } = useGetPersonalSettingsQuery({});
  useEffect(() => {
    dispatch(setOpenFormInNewTab(personalSettings?.openFormInNewTab || false));
  }, [personalSettings?.openFormInNewTab, dispatch]);
  return null;
};

import React, { useCallback, useMemo, useState } from 'react';
import { FormikProps } from 'formik';
import { useSelector } from 'react-redux';

import { Check, Close, DeleteSweep, Search } from '@mui/icons-material';
import { Button, Stack } from '@mui/material';

import { Popup } from '../../components/Popup';
import { ReceiverCols } from '../../constants/receiver';
import { useGetCounterpartiesQuery } from '../../services/api/dictionaries';
import { RootState } from '../../store';
import { prepareDataForCounterpartySearch } from '../../utils/counterpartySearch';
import { AdvancedCounterpartySearch } from '../AdvancedCounterpartySearch';
import { DataTable } from '../CustomDataGrid';
import { FilterBar, FormValues } from '../FilterBarNew';

import styles from './styles.module.scss';

type ContractorModalProps = {
  openClerkModal: boolean;
  onClose: () => void;
  addDoc: (value: any) => void;
  refethTable?: () => void;
};

export const ContractorSearchForm: React.FC<ContractorModalProps> = ({
  openClerkModal,
  onClose,
  refethTable,
  addDoc,
}) => {
  const filters = useSelector(
    (state: RootState) => state.filters.data['contractor-modal-form'] || {}
  );
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const { data, refetch, isLoading } = useGetCounterpartiesQuery({
    searchParameters: filters,
    page: page,
    size: perPage,
  });
  const [tableFilters, setTableFilters] = useState(false);

  const [selectedCounterparties, setSelectedCounterparties] = useState<
    Record<string, boolean>
  >({});

  const selectedDocuments = useMemo(() => {
    return Object.keys(selectedCounterparties)
      .map((el) => Number(el))
      .filter((el) => selectedCounterparties[el]);
  }, [selectedCounterparties]);

  const bar = (
    <Stack
      direction="row"
      justifyContent="flex-end"
      spacing={1.5}
      style={{ width: '100%' }}
    >
      <Button
        variant="contained"
        startIcon={<Check />}
        onClick={() => {
          addDoc(selectedDocuments);
          onClose();
        }}
        disabled={!selectedDocuments.length}
      >
        Выбрать
      </Button>
      <Button variant="outlined" startIcon={<Close />} onClick={onClose}>
        Отмена
      </Button>
    </Stack>
  );
  const createFilterButtons = useCallback(
    (
      onSearch: ({ values }: { values: FormValues }) => () => Promise<void>,
      props: FormikProps<FormValues>
    ) => {
      const { resetForm, values } = props;

      return (
        <>
          <Button
            color="primary"
            variant="contained"
            startIcon={<Search />}
            onClick={onSearch({
              values: prepareDataForCounterpartySearch(values as FormValues),
            })}
          >
            Поиск
          </Button>
          <Button
            color="primary"
            variant="outlined"
            startIcon={<DeleteSweep />}
            onClick={() => resetForm()}
          >
            Очистить
          </Button>
        </>
      );
    },
    []
  );

  return (
    <Popup
      open={Boolean(openClerkModal)}
      onClose={onClose}
      title={'Поиск контрагентов'}
      bar={bar}
      height={'760px'}
    >
      <div className={styles.popupContent}>
        <FilterBar
          formKey={'contractor-modal-form'}
          createFilterButtons={createFilterButtons}
          refetchSearch={() => refetch()}
        >
          <AdvancedCounterpartySearch />
        </FilterBar>
        <div className={styles['choose-container']}>
          <div className={styles.title}>Выбор контрагента</div>
        </div>

        <DataTable
          formKey={'contractor_modal_form'}
          cols={ReceiverCols}
          rows={data?.data || []}
          className={styles.table}
          onRowSelect={setSelectedCounterparties}
          loading={isLoading}
          showFilters={tableFilters}
        />
      </div>
    </Popup>
  );
};

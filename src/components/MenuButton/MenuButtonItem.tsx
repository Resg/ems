import React from 'react';

import { MenuItem, MenuItemProps } from '@mui/material';

import { Link, LinkProps } from '../Link';

import styles from './styles.module.scss';

export interface ButtonMenuItemProps extends MenuItemProps {
  children: React.ReactNode;
  icon?: React.ReactNode;
  linkProps?: LinkProps;
}

export const MenuButtonItem: React.FC<ButtonMenuItemProps> = ({
  children,
  icon,
  linkProps,
  ...itemProps
}) => {
  return (
    <MenuItem {...itemProps}>
      {linkProps ? (
        <Link className={styles.link} {...linkProps}>
          {icon && <div className={styles.menuIcon}>{icon}</div>}
          {children}
        </Link>
      ) : (
        <>
          {icon && <div className={styles.menuIcon}>{icon}</div>}
          {children}
        </>
      )}
    </MenuItem>
  );
};

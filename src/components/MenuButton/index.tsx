import React, { useState } from 'react';

import { Button, Menu } from '@mui/material';

import styles from './styles.module.scss';

export interface MenuButtonProps {
  title: string;
  children: React.ReactNode;
  startIcon?: React.ReactNode;
  endIcon?: React.ReactNode;
}

export const MenuButton: React.FC<MenuButtonProps> = ({
  title,
  children,
  startIcon,
  endIcon,
}) => {
  const [anchorEl, setAnchorEl] = useState<Element | null>(null);

  const handleExpand = (e: React.MouseEvent) => {
    setAnchorEl(e.currentTarget);
  };

  return (
    <div className={styles.box}>
      <Button
        variant="outlined"
        startIcon={startIcon}
        endIcon={endIcon}
        onClick={handleExpand}
      >
        {title}
      </Button>
      <Menu
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        onClose={() => setAnchorEl(null)}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        className={styles.menu}
      >
        {children}
      </Menu>
    </div>
  );
};

import React, { useCallback, useEffect, useMemo, useState } from 'react';
import cn from 'classnames';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';

import { Verified } from '@mui/icons-material';
import AssignmentIcon from '@mui/icons-material/Assignment';
import LeftArrIcon from '@mui/icons-material/ChevronLeft';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import DocIcon from '@mui/icons-material/InsertDriveFile';
import ListAltIcon from '@mui/icons-material/ListAlt';
import ListIcon from '@mui/icons-material/PlaylistAddCheck';
import TaskIcon from '@mui/icons-material/Task';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Drawer,
  Tooltip,
} from '@mui/material';

import { Routes } from '../../constants/routes';
import { useGetListDraftTasksQuery } from '../../services/api/drafts';
import {
  useGetIncomingDocumentTasksCountQuery,
  useGetIncomingDocumentTasksInWorkCountQuery,
  useGetIncomingRequestsCountQuery,
} from '../../services/api/incomingDocumentTasks';
import { useGetMyDocumentCountTasksQuery } from '../../services/api/myDocumentTasks';
import { useGetPersonalSettingsQuery } from '../../services/api/user';
import { RootState, useAppDispatch } from '../../store';
import { resetBreadCrumbs, setSideBarIsOpen } from '../../store/utils';
import { Link as LinkBase, LinkProps } from '../Link';
import { Logo } from '../Logo';

import styles from './Sidebar.module.scss';

export interface SidebarProps {}

const Sidebar: React.FC<SidebarProps> = () => {
  const { data: incomingData, refetch: updateIncomingData } =
    useGetIncomingDocumentTasksCountQuery({});
  const { data: dataInWork, refetch: updateInworkTasks } =
    useGetIncomingDocumentTasksInWorkCountQuery({});
  const { data: personalData } = useGetPersonalSettingsQuery({});
  const { data: myTaskData, refetch: updateMyTasks } =
    useGetMyDocumentCountTasksQuery({
      type: 'DOCUMENT',
    });
  const { data: draftData, refetch: updateDtaftData } =
    useGetListDraftTasksQuery({
      size: 0,
    });

  const { data: requestData, refetch: updateRequestData } =
    useGetIncomingRequestsCountQuery({});

  const isOpen = useSelector((state: RootState) => state.utils.sideBarIsOpen);
  const loading = useSelector((state: RootState) => state.utils.loading);
  const dispatch = useAppDispatch();

  const [expanded, setExpanded] = useState<boolean[]>(new Array(7).fill(false));
  const handleFolded = () => {
    dispatch(setSideBarIsOpen(!isOpen));
    setExpanded((prevState) => prevState.map((state) => false));
  };

  const handleUpdateCounter = () => {
    updateIncomingData();
    updateInworkTasks();
    updateMyTasks();
    updateDtaftData();
    updateRequestData();
  };

  const docunentTasksSum = useMemo(() => {
    if (incomingData && dataInWork) {
      return incomingData?.totalCount + dataInWork?.totalCount;
    }
  }, [incomingData?.totalCount, dataInWork?.totalCount]);

  const MINUTE_MS = 300000;

  useEffect(() => {
    if (personalData?.autoCountersUpdate) {
      const interval = setInterval(() => {
        handleUpdateCounter();
      }, MINUTE_MS);
      return () => clearInterval(interval);
    }
  }, [personalData?.autoCountersUpdate]);

  const handleTitleClick = (array: boolean[], index: number) => {
    if (!isOpen) {
      dispatch(setSideBarIsOpen(!isOpen));
    }
    setExpanded(changeAccordion(array, index));
  };

  return (
    <Drawer
      variant="permanent"
      classes={{
        paper: `${styles.drawer} ${!isOpen ? styles.drawer_closed : ''}`,
      }}
    >
      <div className={styles.drawerHeader}>
        <div className={styles.drawerLogo}>
          <Link to="/">
            <Logo size={40} loading={Boolean(loading)} />
          </Link>
        </div>
        <div
          className={cn({
            [styles.drawerTitle]: true,
            [styles.drawerTitle_hide]: !isOpen,
          })}
        >
          <div className={styles.logoText}>ИС Экспертиза ЭМС</div>
        </div>
      </div>
      <div
        className={cn({
          [styles.drawerMenuBox]: true,
          [styles.drawerMenuBox_hide]: !isOpen,
        })}
      >
        <Accordion className={styles.accordion} expanded={expanded[0]}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon className={styles.expandIcon} />}
            onClick={() => handleTitleClick(expanded, 0)}
          >
            <AssignmentIcon className={styles.menuIcon} />
            <span className={styles.menuTitle}>Заключения</span>
          </AccordionSummary>
          <AccordionDetails>
            <div className={styles.submenuOption}>
              <Link to={Routes.CONCLUSIONS.REVIEW.SEARCH}>
                Просмотр заключений
              </Link>
            </div>
          </AccordionDetails>
        </Accordion>
        <Accordion className={styles.accordion} expanded={expanded[1]}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon className={styles.expandIcon} />}
            onClick={() => handleTitleClick(expanded, 1)}
          >
            <AssignmentIcon className={styles.menuIcon} />
            <span className={styles.menuTitle}>Задачи по заявкам</span>
            <Tooltip title={'Обновить счётчики'} placement={'right'}>
              <div onClick={handleUpdateCounter} className={styles.badge}>
                {requestData?.totalCount}
              </div>
            </Tooltip>
          </AccordionSummary>
          <AccordionDetails>
            <div className={styles.submenuOption}>
              <Link to={Routes.REQUESTS_TASKS.INCOMING}>
                Поступившие
                <Tooltip title={'Обновить счётчики'} placement={'right'}>
                  <div onClick={handleUpdateCounter} className={styles.badge}>
                    {requestData?.totalCount}
                  </div>
                </Tooltip>
              </Link>
            </div>
          </AccordionDetails>
        </Accordion>
        <Accordion className={styles.accordion} expanded={expanded[2]}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon className={styles.expandIcon} />}
            onClick={() => handleTitleClick(expanded, 2)}
          >
            <TaskIcon className={styles.menuIcon} />
            <span className={styles.menuTitle}>Задачи по документам</span>
            <Tooltip title={'Обновить счётчики'} placement={'right'}>
              <div onClick={handleUpdateCounter} className={styles.badge}>
                {docunentTasksSum}
              </div>
            </Tooltip>
          </AccordionSummary>
          <AccordionDetails>
            <div className={styles.submenuOption}>
              <Link to={Routes.DOCUMENTS_TASKS.INCOMING}>
                Поступившие документы
                <Tooltip title={'Обновить счётчики'} placement={'right'}>
                  <div onClick={handleUpdateCounter} className={styles.badge}>
                    {incomingData?.totalCount}
                  </div>
                </Tooltip>
              </Link>
            </div>
            <div className={styles.submenuOption}>
              <Link to={Routes.DOCUMENTS_TASKS.IN_WORK}>
                В работе
                <Tooltip title={'Обновить счётчики'} placement={'right'}>
                  <div onClick={handleUpdateCounter} className={styles.badge}>
                    {dataInWork?.totalCount}
                  </div>
                </Tooltip>
              </Link>
            </div>
            <div className={styles.submenuOption}>
              <Link to="/in-progress">Ожидающие</Link>
            </div>
            <div className={styles.submenuOption}>
              <Link to={Routes.DOCUMENTS_TASKS.MY_TASKS}>
                Поставленные мной
                <Tooltip title={'Обновить счётчики'} placement={'right'}>
                  <div onClick={handleUpdateCounter} className={styles.badge}>
                    {myTaskData?.totalCount}
                  </div>
                </Tooltip>
              </Link>
            </div>
            <div className={styles.submenuOption}>
              <Link to={Routes.DOCUMENTS_TASKS.DRAFTS}>
                Черновики
                <Tooltip title={'Обновить счётчики'} placement={'right'}>
                  <div onClick={handleUpdateCounter} className={styles.badge}>
                    {draftData?.totalCount}
                  </div>
                </Tooltip>
              </Link>
            </div>
            <div className={styles.submenuOption}>
              <Link to={Routes.DOCUMENTS_TASKS.COMPLETED}>Выполненные</Link>
            </div>
          </AccordionDetails>
        </Accordion>
        <Accordion className={styles.accordion} expanded={expanded[3]}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon className={styles.expandIcon} />}
            onClick={() => handleTitleClick(expanded, 3)}
          >
            <ListIcon className={styles.menuIcon} />
            <span className={styles.menuTitle}>Заявки</span>
          </AccordionSummary>
          <AccordionDetails>
            <div className={styles.submenuOption}>
              <Link to={Routes.REQUESTS.SEARCH}>Поиск заявок</Link>
            </div>
            <div className={styles.submenuOption}>
              <Link to="/in-progress">Заключения ЭМС</Link>
            </div>
            <div className={styles.submenuOption}>
              <Link to="/in-progress">Свидетельства (Позывной)</Link>
            </div>
            <div className={styles.submenuOption}>
              <Link to="/in-progress">Заключения (Суда)</Link>
            </div>
          </AccordionDetails>
        </Accordion>
        <Accordion className={styles.accordion} expanded={expanded[4]}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon className={styles.expandIcon} />}
            onClick={() => handleTitleClick(expanded, 4)}
          >
            <DocIcon className={styles.menuIcon} />
            <span className={styles.menuTitle}>Документы</span>
          </AccordionSummary>
          <AccordionDetails>
            <div className={styles.submenuOption}>
              <Link to={Routes.DOCUMENTS.SEARCH}>Поиск документов</Link>
            </div>
            <div className={styles.submenuOption}>
              <Link to={Routes.DOCUMENTS.FAVORITE}>Избранные документы</Link>
            </div>
          </AccordionDetails>
        </Accordion>
        <Accordion className={styles.accordion} expanded={expanded[5]}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon className={styles.expandIcon} />}
            onClick={() => handleTitleClick(expanded, 5)}
          >
            <ListAltIcon className={styles.menuIcon} />
            <span className={styles.menuTitle}>Реестры</span>
          </AccordionSummary>
          <AccordionDetails>
            <Accordion className={styles.accordion_internal}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon className={styles.expandIcon} />}
              >
                Входящие
              </AccordionSummary>
              <AccordionDetails>
                <Accordion className={styles.accordion_internal_double}>
                  <AccordionSummary
                    expandIcon={
                      <ExpandMoreIcon className={styles.expandIcon} />
                    }
                  >
                    Документы
                  </AccordionSummary>
                  <AccordionDetails>
                    <div className={styles.accordionSubmenu}>
                      <Link to={Routes.REGISTRIES.INCOMING_DOCUMENTS_EXPECTED}>
                        Ожидаемые
                      </Link>
                    </div>
                    <div className={styles.accordionSubmenu}>
                      <Link to={Routes.REGISTRIES.INCOMING_DOCUMENTS_ACCEPTED}>
                        Принятые
                      </Link>
                    </div>
                    <div className={styles.accordionSubmenu}>
                      <Link to={Routes.REGISTRIES.INCOMING_DOCUMENTS_RETURNED}>
                        Возвращенные
                      </Link>
                    </div>
                  </AccordionDetails>
                </Accordion>
                <Accordion className={styles.accordion_internal_double}>
                  <AccordionSummary
                    expandIcon={
                      <ExpandMoreIcon className={styles.expandIcon} />
                    }
                  >
                    Исходящие пакеты
                  </AccordionSummary>
                  <AccordionDetails>
                    <div className={styles.accordionSubmenu}>
                      <Link to={Routes.REGISTRIES.INCOMING_PACKETS_EXPECTED}>
                        Ожидаемые
                      </Link>
                    </div>
                    <div className={styles.accordionSubmenu}>
                      <Link to={Routes.REGISTRIES.INCOMING_PACKETS_ACCEPTED}>
                        Принятые
                      </Link>
                    </div>
                    <div className={styles.accordionSubmenu}>
                      <Link to={Routes.REGISTRIES.INCOMING_PACKETS_RETURNED}>
                        Возвращенные
                      </Link>
                    </div>
                  </AccordionDetails>
                </Accordion>
              </AccordionDetails>
            </Accordion>
            <Accordion className={styles.accordion_internal}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon className={styles.expandIcon} />}
              >
                Исходящие
              </AccordionSummary>
              <AccordionDetails>
                <Accordion className={styles.accordion_internal_double}>
                  <AccordionSummary
                    expandIcon={
                      <ExpandMoreIcon className={styles.expandIcon} />
                    }
                  >
                    Документы
                  </AccordionSummary>
                  <AccordionDetails>
                    <div className={styles.accordionSubmenu}>
                      <Link to={Routes.REGISTRIES.OUTCOMING_DOCUMENTS_OPENED}>
                        Открытые
                      </Link>
                    </div>
                    <div className={styles.accordionSubmenu}>
                      <Link to={Routes.REGISTRIES.OUTCOMING_DOCUMENTS_CLOSED}>
                        Закрытые
                      </Link>
                    </div>
                    <div className={styles.accordionSubmenu}>
                      <Link to={Routes.REGISTRIES.OUTCOMING_DOCUMENTS_SENT}>
                        Отправленные
                      </Link>
                    </div>
                  </AccordionDetails>
                </Accordion>
                <Accordion className={styles.accordion_internal_double}>
                  <AccordionSummary
                    expandIcon={
                      <ExpandMoreIcon className={styles.expandIcon} />
                    }
                  >
                    Исходящие пакеты
                  </AccordionSummary>
                  <AccordionDetails>
                    <div className={styles.accordionSubmenu}>
                      <Link to={Routes.REGISTRIES.OUTCOMING_PACKETS_OPENED}>
                        Открытые
                      </Link>
                    </div>
                    <div className={styles.accordionSubmenu}>
                      <Link to={Routes.REGISTRIES.OUTCOMING_PACKETS_CLOSED}>
                        Закрытые
                      </Link>
                    </div>
                    <div className={styles.accordionSubmenu}>
                      <Link to={Routes.REGISTRIES.OUTCOMING_PACKETS_SENT}>
                        Отправленные
                      </Link>
                    </div>
                  </AccordionDetails>
                </Accordion>
              </AccordionDetails>
            </Accordion>
            <div className={styles.submenuOption}>
              <Link to={Routes.REGISTRIES.ARCHIVE}>Архив</Link>
            </div>
          </AccordionDetails>
        </Accordion>
        <Accordion className={styles.accordion} expanded={expanded[6]}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon className={styles.expandIcon} />}
            onClick={() => handleTitleClick(expanded, 6)}
          >
            <Verified className={styles.menuIcon} />
            <span className={styles.menuTitle}>Разрешения</span>
          </AccordionSummary>
          <AccordionDetails>
            <div className={styles.submenuOption}>
              <Link to={Routes.PERMISSIONS.SEARCH}>Поиск разрешений</Link>
            </div>
          </AccordionDetails>
        </Accordion>
      </div>
      <div
        className={cn({
          [styles.closeBtn]: true,
          [styles.closeBtn_closed]: !isOpen,
        })}
        onClick={handleFolded}
      >
        <LeftArrIcon data-testid="close-sidebar" />
      </div>
    </Drawer>
  );
};

const Link: React.FC<LinkProps> = (props) => {
  const location = useLocation();
  const dispatch = useAppDispatch();
  const handleClick = useCallback(() => {
    if (location.pathname !== props.to) {
      dispatch(resetBreadCrumbs());
    }
  }, [dispatch, location.pathname, props.to]);

  return <LinkBase {...props} onClick={handleClick} />;
};

const changeAccordion = (array: boolean[], index: number) => {
  const list = [...array];
  list[index] = !array[index];
  return list;
};

export default Sidebar;

import React, { useCallback, useMemo } from 'react';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { Checkbox, FormControlLabel } from '@mui/material';

export interface CheckboxInputProps {
  name: string;
  label?: string;
  className?: string;
  disabled?: boolean;
}

export const CheckboxInput: React.FC<CheckboxInputProps> = ({
  name,
  label,
  disabled,
}) => {
  const { values, setFieldValue } = useFormikContext<any>();

  const handleChange = useCallback(
    (_: React.SyntheticEvent, checked: boolean) => {
      setFieldValue(name, checked);
    },
    [name, setFieldValue]
  );

  const value = useMemo(() => Boolean(get(values, name)), [values, name]);

  return (
    <FormControlLabel
      name={name}
      label={label}
      onChange={handleChange}
      control={<Checkbox checked={value} disabled={disabled} />}
    />
  );
};

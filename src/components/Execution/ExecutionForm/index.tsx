import React, { useCallback, useMemo } from 'react';
import { Form, Formik } from 'formik';
import { get } from 'lodash';
import { useSelector } from 'react-redux';

import { Check, Clear, Close } from '@mui/icons-material';
import { Button, Dialog, Grid, IconButton } from '@mui/material';

import {
  ExecutionFields,
  ExecutionFormInitialValues,
  ExecutionLabels,
} from '../../../constants/execution';
import {
  useExecuteTaskMutation,
  useReassignedTaskMutation,
  useReassignTaskMutation,
  useReopenTaskMutation,
  useSendToContinueMutation,
  useSendToReexecutionMutation,
} from '../../../services/api/incomingDocumentTasks';
import { RootState, useAppDispatch } from '../../../store';
import { setOpenExecutionForm } from '../../../store/tasks';
import { ExecutionFormState } from '../../../types/execution';
import { massServerQuery } from '../../../utils/incomingDocumentTasks';
import { ClerkInput } from '../../ClerkInput';
import Input from '../../Input';
import { ExecutionButtonActions } from '../ExecutionButton/constants';

import styles from './styles.module.scss';

export interface ExecutionFormProps {}

export const ExecutionForm: React.FC<ExecutionFormProps> = () => {
  const [execute] = useExecuteTaskMutation();
  const [reassigned] = useReassignedTaskMutation();
  const [reopen] = useReopenTaskMutation();
  const [reassign] = useReassignTaskMutation();
  const [approve] = useSendToContinueMutation();
  const [reexecution] = useSendToReexecutionMutation();

  const { ids, statusId, isEds, type, action, setSelected } = useSelector(
    (state: RootState) => state.tasks.executionProps
  );

  const request = useMemo(() => {
    if (action === ExecutionButtonActions.REASSIGN) {
      return reassign;
    }
    if (action === ExecutionButtonActions.OPEN_AGAIN) {
      return reassigned;
    }
    if (action === ExecutionButtonActions.REOPEN) {
      return reopen;
    }
    if (action === ExecutionButtonActions.APPROVE) {
      return approve;
    }
    return execute;
  }, [action, execute, reassign, reassigned, reopen, reexecution]);

  const open = useSelector(
    (state: RootState) => state.tasks.isExecutionFormOpen
  );
  const dispatch = useAppDispatch();

  const handleClose = useCallback(
    () => dispatch(setOpenExecutionForm(false)),
    [dispatch]
  );

  const handleSubmit = useCallback(
    async (values: ExecutionFormState) => {
      const response = await massServerQuery(
        ids.map((id) => ({
          id,
          statusId,
          isEds,
          comments: values[ExecutionFields.COMMENTS],
          performerId: values[ExecutionFields.PERFORMER_ID],
          termWorkDays: values[ExecutionFields.TERM_WORK_DAYS],
          taskParamsComments: values[ExecutionFields.COMMENTS],
          requestManagerId: values[ExecutionFields.REQUEST_MANAGER],
        })),
        request
      );

      handleClose();
      setSelected && setSelected({});
    },
    [handleClose, ids, isEds, request, statusId]
  );

  const isShowRequestManger =
    type === 101 && action === ExecutionButtonActions.EXECUTE;
  const isShowPerformer =
    (type === 5 && action === ExecutionButtonActions.APPROVE) ||
    action === ExecutionButtonActions.REASSIGN ||
    action === ExecutionButtonActions.OPEN_AGAIN;
  const isShowTerm = type === 5 && action === ExecutionButtonActions.APPROVE;
  const isShowComments = action !== ExecutionButtonActions.REASSIGN;
  const isShowFiles = action !== ExecutionButtonActions.REASSIGN;
  const isReopen = action === ExecutionButtonActions.REOPEN;
  return (
    <Dialog open={open} maxWidth={'lg'} scroll="body">
      <div className={styles.card}>
        <div className={styles.title}>
          <div>Выполнение задачи ({action})</div>
          <IconButton className={styles['close-button']} onClick={handleClose}>
            <Close />
          </IconButton>
        </div>
        <Formik
          initialValues={ExecutionFormInitialValues}
          enableReinitialize
          onSubmit={handleSubmit}
        >
          {({ handleSubmit, handleChange, handleBlur, values, errors }) => {
            const comment = get(values, ExecutionFields.COMMENTS);

            const isCommentEmpty =
              isShowComments && isReopen
                ? !comment || comment.length < 5
                : false;

            return (
              <Form className={styles.form}>
                <Grid container rowSpacing={3} className={styles.container}>
                  {isShowRequestManger && (
                    <Grid item xs={12}>
                      <ClerkInput
                        name={ExecutionFields.REQUEST_MANAGER}
                        label={ExecutionLabels[ExecutionFields.REQUEST_MANAGER]}
                        required
                      />
                    </Grid>
                  )}
                  {isShowPerformer && (
                    <Grid item xs={12}>
                      <ClerkInput
                        name={ExecutionFields.PERFORMER_ID}
                        label={ExecutionLabels[ExecutionFields.PERFORMER_ID]}
                      />
                    </Grid>
                  )}
                  {isShowTerm && (
                    <Grid item xs={12}>
                      <Input
                        type="number"
                        name={ExecutionFields.TERM_WORK_DAYS}
                        label={ExecutionLabels[ExecutionFields.TERM_WORK_DAYS]}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values[ExecutionFields.TERM_WORK_DAYS]}
                        error={Boolean(errors[ExecutionFields.TERM_WORK_DAYS])}
                        helperText={
                          errors[ExecutionFields.TERM_WORK_DAYS] as string
                        }
                      />
                    </Grid>
                  )}
                  {/*{isShowFiles && (*/}
                  {/*  <Grid item xs={12}>*/}
                  {/*    <Input*/}
                  {/*      name={ExecutionFields.FILE_LIST}*/}
                  {/*      label={ExecutionLabels[ExecutionFields.FILE_LIST]}*/}
                  {/*      onChange={handleChange}*/}
                  {/*      onBlur={handleBlur}*/}
                  {/*      value={values[ExecutionFields.FILE_LIST]}*/}
                  {/*      error={Boolean(errors[ExecutionFields.FILE_LIST])}*/}
                  {/*      disabled*/}
                  {/*      helperText={errors[ExecutionFields.FILE_LIST] as string}*/}
                  {/*    />*/}
                  {/*  </Grid>*/}
                  {/*)}*/}
                  {isShowComments && (
                    <Grid item xs={12}>
                      <Input
                        InputProps={{ className: styles.comments }}
                        name={ExecutionFields.COMMENTS}
                        label={ExecutionLabels[ExecutionFields.COMMENTS]}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values[ExecutionFields.COMMENTS]}
                        error={Boolean(errors[ExecutionFields.COMMENTS])}
                        multiline
                        helperText={errors[ExecutionFields.COMMENTS] as string}
                      />
                    </Grid>
                  )}
                </Grid>
                <div className={styles['actions-container']}>
                  <Button
                    startIcon={<Check />}
                    className={styles.execute}
                    variant={'contained'}
                    disabled={isCommentEmpty}
                    onClick={() => handleSubmit()}
                  >
                    Выполнить
                  </Button>
                  <Button
                    startIcon={<Clear />}
                    className={styles.cancel}
                    variant={'outlined'}
                    onClick={handleClose}
                  >
                    Отмена
                  </Button>
                </div>
              </Form>
            );
          }}
        </Formik>
      </div>
    </Dialog>
  );
};

import {
  AssignmentInd,
  Cancel,
  Close,
  DomainVerification,
  Gesture,
  HistoryEdu,
  NoteAlt,
  NotInterested,
  PendingActions,
  PersonPinCircle,
  Redo,
  Restore,
  SvgIconComponent,
  TaskAlt,
  Undo,
} from '@mui/icons-material';

export enum ExecutionButtonActions {
  EXECUTE = 'Выполнить',
  ENDORSE = 'Завизировать',
  REJECT = 'Отклонить',
  CANCEL = 'Отменить',
  SEND = 'Отправить',
  SIGN = 'Подписать',
  INWORK = 'Взять в работу',
  REASSIGN = 'Переназначить',
  CONFIRM = 'На подтверждение автору',
  REEDIT = 'Вернуть на редактирование',
  REOPEN = 'Вернуть на доработку',
  OPEN_AGAIN = 'Поставить повторно',
  CLOSE = 'Закрыть',
  APPROVE = 'Утвердить',
  ACQUAINTED = 'Ознакомлен',
}

export const ExecutionButtonIcons: Record<
  ExecutionButtonActions,
  SvgIconComponent
> = {
  [ExecutionButtonActions.EXECUTE]: DomainVerification,
  [ExecutionButtonActions.ENDORSE]: HistoryEdu,
  [ExecutionButtonActions.REJECT]: NotInterested,
  [ExecutionButtonActions.CANCEL]: Close,
  [ExecutionButtonActions.SEND]: Redo,
  [ExecutionButtonActions.SIGN]: Gesture,
  [ExecutionButtonActions.INWORK]: PendingActions,
  [ExecutionButtonActions.REASSIGN]: PersonPinCircle,
  [ExecutionButtonActions.CONFIRM]: AssignmentInd,
  [ExecutionButtonActions.REEDIT]: NoteAlt,
  [ExecutionButtonActions.REOPEN]: Restore,
  [ExecutionButtonActions.OPEN_AGAIN]: Undo,
  [ExecutionButtonActions.CLOSE]: Cancel,
  [ExecutionButtonActions.APPROVE]: DomainVerification,
  [ExecutionButtonActions.ACQUAINTED]: TaskAlt,
};

export const ExecutionStatusIds: Record<string, number> = {
  [ExecutionButtonActions.EXECUTE]: 6,
  [ExecutionButtonActions.ENDORSE]: 11,
  [ExecutionButtonActions.REJECT]: 9,
  [ExecutionButtonActions.CANCEL]: 7,
  [ExecutionButtonActions.SEND]: 17,
  [ExecutionButtonActions.SIGN]: 10,
  [ExecutionButtonActions.APPROVE]: 8,
};

export const ExecutionFastButtons: ExecutionButtonActions[] = [
  ExecutionButtonActions.EXECUTE,
  ExecutionButtonActions.ACQUAINTED,
  ExecutionButtonActions.INWORK,
];

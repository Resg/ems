import React, { useCallback, useMemo } from 'react';

import { IconButton, Tooltip } from '@mui/material';

import {
  useGetAdditionalActionStatusQuery,
  useSendToAcquaintedMutation,
  useSendToContinueMutation,
} from '../../../services/api/incomingDocumentTasks';
import { useAppDispatch } from '../../../store';
import {
  setExecutionFormProps,
  setOpenExecutionForm,
} from '../../../store/tasks';
import {
  AdditionStatusRequestProps,
  Task,
} from '../../../types/incomingDocumentTasks';
import { ToolButton, ToolButtonProps } from '../../ToolsPanel';

import { ExecutionButtonActions } from './constants';

import styles from './styles.module.scss';

export interface ExecutionButtonProps<T> extends ToolButtonProps {
  rows: T[];
  ids: number[];
  statusId?: number;
  button?: 'tool' | 'round';
  setSelected?: (value: any) => void;
}

export const ExecutionToolButton: React.FC<
  ToolButtonProps & { button?: 'tool' | 'round'; shouldOpenForm?: boolean }
> = ({ onClick, button = 'tool', shouldOpenForm = true, ...props }) => {
  const dispatch = useAppDispatch();
  const handleOpenExecutionForm = useCallback(
    (e: React.MouseEvent) => {
      onClick?.(e);
      if (shouldOpenForm) {
        dispatch(setOpenExecutionForm(true));
      }
    },
    [dispatch, onClick]
  );
  if (button === 'tool') {
    return <ToolButton onClick={handleOpenExecutionForm} {...props} />;
  } else if (props.disabled) {
    return null;
  } else {
    return (
      <Tooltip title={props.label} placement={'left'}>
        <IconButton onClick={handleOpenExecutionForm} className={styles.button}>
          {props.startIcon || props.endIcon}
        </IconButton>
      </Tooltip>
    );
  }
};

export function ExecuteButton<T extends Record<string, any>>({
  rows,
  ids,
  statusId,
  setSelected,
  ...restProps
}: ExecutionButtonProps<T>) {
  const oneRow = useMemo(() => {
    return rows.length === 1;
  }, [rows]);

  const someRows = useMemo(() => {
    return rows.length;
  }, [rows]);

  const rowHasParam = useCallback(
    (prop: keyof Task) => {
      return !rows.some((row) => !row[prop]);
    },
    [rows]
  );

  const rowsTypeIdParam = useCallback(
    (prop: keyof Task) => {
      if (!someRows) {
        return false;
      }
      const typeId = rows[0].typeId;
      return !rows.some((row) => !row[prop] || typeId !== row.typeId);
    },
    [rows]
  );

  const actionAvailable = useMemo(() => {
    if (
      statusId === 6 ||
      statusId === 10 ||
      statusId === 11 ||
      statusId === 17
    ) {
      return rowsTypeIdParam('canExecute');
    }

    if (statusId === 7 || statusId === 9) {
      return rowsTypeIdParam('canReject') || rowsTypeIdParam('canExecute');
    }

    switch (restProps.label) {
      case ExecutionButtonActions.APPROVE:
        return rowsTypeIdParam('canExecute');
      case ExecutionButtonActions.ACQUAINTED:
        return someRows && rowHasParam('canMarkAsRead');
      case ExecutionButtonActions.REASSIGN:
        return someRows && rowHasParam('canReassignPerformer');
      case ExecutionButtonActions.CLOSE:
        return oneRow && rowHasParam('canClose');
      case ExecutionButtonActions.CONFIRM:
        return oneRow && rowHasParam('canSendToAuthor');
      case ExecutionButtonActions.REEDIT:
        return oneRow && rowHasParam('canReturnToEditor');
      case ExecutionButtonActions.REOPEN:
        return oneRow && rowHasParam('canReturnToAuthor');
      case ExecutionButtonActions.OPEN_AGAIN:
        return oneRow && rowHasParam('canSendToWorkFromRevision');
      default:
        return false;
    }
  }, [rows]);

  const statusQueryProps = useMemo(() => {
    if (!rows.length || !actionAvailable) {
      return {};
    }
    const props: AdditionStatusRequestProps = { statusId: statusId };
    props.typeId = rows[0].typeId;
    // if (rows.length === 1) { TODO: бэк работает не так как в описании
    props.id = rows[0].id;
    // }

    return props;
  }, [actionAvailable, rows]);

  const { data } = useGetAdditionalActionStatusQuery(statusQueryProps, {
    skip: !rows.length || !actionAvailable || !statusId,
  });

  const approveSpecialCase =
    restProps.label === ExecutionButtonActions.APPROVE &&
    (data?.isActionActive || (oneRow && rowHasParam('canContinue')));

  const shouldOpenForm =
    !approveSpecialCase &&
    restProps.label !== ExecutionButtonActions.ACQUAINTED;

  const [approve] = useSendToContinueMutation();
  const [sendToAcquainted] = useSendToAcquaintedMutation();

  const actionActive = data ? data.isActionActive : true;

  const dispatch = useAppDispatch();
  const handleClick = useCallback(() => {
    switch (restProps.label) {
      case ExecutionButtonActions.APPROVE: {
        if (data?.isActionActive || (oneRow && rowHasParam('canContinue'))) {
          approve({ id: rows[0].id });
        }
        break;
      }
      case ExecutionButtonActions.ACQUAINTED: {
        sendToAcquainted(rows[0].id);
        return;
      }
      default: {
        dispatch(
          setExecutionFormProps({
            isEds: false,
            statusId: statusId || 6,
            ids,
            action: restProps.label,
            type: rows[0].typeId,
            setSelected,
          })
        );
      }
    }
  }, [dispatch, ids, rows]);

  return (
    <ExecutionToolButton
      {...restProps}
      onClick={handleClick}
      disabled={!(actionAvailable && actionActive) && !approveSpecialCase}
      shouldOpenForm={shouldOpenForm}
    />
  );
}

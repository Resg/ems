import React, { useCallback } from 'react';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { AccountCircle } from '@mui/icons-material';

import { useGetClerksQuery } from '../../services/api/dictionaries';
import { useAppDispatch } from '../../store';
import {
  setEmployeeModalIsOpen,
  setIsModalMultiple,
  setKeyProperty,
  setOnEmployeeModalSubmit,
  setSelectedEmployee,
} from '../../store/utils';
import { EmployeeData } from '../../types/employee';
import {
  AutoCompleteInput,
  AutoCompleteInputProps,
} from '../AutoCompleteInput';
import { RoundButton } from '../RoundButton';

import styles from './styles.module.scss';

export interface ClerkInputProps
  extends Omit<AutoCompleteInputProps, 'options'> {
  options?: Array<{ label: string; value: any }>;
  property?: keyof EmployeeData;
}

export const ClerkInput: React.FC<ClerkInputProps> = ({
  name,
  disabled,
  multiple,
  options,
  property,
  ...restProps
}) => {
  const { data: clerks = [] } = useGetClerksQuery({});

  const { setFieldValue, values } = useFormikContext();

  const dispatch = useAppDispatch();

  const value = get(values, name);

  const handleClick = useCallback(() => {
    property && dispatch(setKeyProperty(property));
    dispatch(setIsModalMultiple(!!multiple));
    if (disabled) {
      return;
    }
    if (multiple) {
      dispatch(
        setOnEmployeeModalSubmit((ids: number[]) => setFieldValue(name, ids))
      );
      dispatch(
        setSelectedEmployee(
          value?.reduce(
            (acc: Record<number, boolean>, id: number) => ({
              ...acc,
              [id]: true,
            }),
            {} as Record<number, boolean>
          )
        )
      );
    } else {
      dispatch(
        setOnEmployeeModalSubmit((ids: number[]) => setFieldValue(name, ids[0]))
      );
      dispatch(setSelectedEmployee({ [value]: true }));
    }
    dispatch(setEmployeeModalIsOpen(true));
  }, [value, dispatch, disabled, multiple]);

  return (
    <div className={styles.row}>
      <AutoCompleteInput
        name={name}
        options={options || clerks}
        disabled={disabled}
        multiple={multiple}
        {...restProps}
      />
      <RoundButton
        icon={<AccountCircle />}
        onClick={handleClick}
        disabled={disabled}
      />
    </div>
  );
};

import React, { useMemo } from 'react';
import { format, isValid, parse } from 'date-fns';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { DesktopDatePicker } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';

import Input from '../Input';

export interface DatePickerProps {
  label: string;
  name: string;
  required?: boolean;
  className?: string;
  disabled?: boolean;
  notEditableField?: boolean;
  onChange?: (value: Date | null) => void;
}

export const DatePickerInput: React.FC<DatePickerProps> = ({
  label,
  name,
  required,
  className,
  disabled,
  notEditableField,
  onChange,
}) => {
  const { setFieldValue, values, errors, touched } = useFormikContext<any>();
  const isTouched = get(touched, name);

  const error: string = useMemo(() => {
    const error = errors[name];
    if (!error) {
      return '';
    }
    if (Array.isArray(error)) {
      return error[0] as string;
    } else {
      return error as string;
    }
  }, [errors, name]);

  const parsedValue = useMemo(() => {
    const val = get(values, name);
    return val ? parse(val, 'yyyy-MM-dd', new Date()) : null;
  }, [values, name]);
  const handleChange = (value: Date | null) => {
    if (value && isValid(value)) {
      setFieldValue(name, format(value, 'yyyy-MM-dd'));
    } else {
      setFieldValue(name, null);
    }
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <DesktopDatePicker
        label={label}
        className={className}
        onChange={onChange || handleChange}
        value={parsedValue}
        inputFormat="dd.MM.yyyy"
        disabled={disabled}
        renderInput={({ error: inputError, inputProps, ...params }) => {
          return (
            <Input
              helperText={isTouched && error}
              {...params}
              inputProps={{ ...inputProps, placeholder: 'дд.мм.гггг' }}
              error={isTouched && (inputError || Boolean(error))}
              required={required}
              notEditableField={notEditableField}
            />
          );
        }}
      />
    </LocalizationProvider>
  );
};

import React, { HTMLAttributes } from 'react';
import cn from 'classnames';

import styles from './styles.module.scss';

const Card: React.FC<HTMLAttributes<HTMLDivElement>> = ({ ...props }) => {
  const { className, children } = props;

  return (
    <div
      className={cn(className, {
        [styles.box]: true,
      })}
    >
      {children}
    </div>
  );
};

export default Card;

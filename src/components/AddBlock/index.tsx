import React from 'react';

import { AddBox } from '@mui/icons-material';
import { Button, Stack, StackProps } from '@mui/material';

import styles from './styles.module.scss';

export interface AddBlockProps {
  addText?: string;
  onAdd?: () => void;
  disabled?: boolean;
}

export const AddBlock: React.FC<AddBlockProps & StackProps> = ({
  addText = 'Добавить',
  onAdd,
  disabled,
  ...stackProps
}) => {
  return (
    <Stack direction="column" alignItems="flex-end" {...stackProps}>
      <div className={styles.line} />
      <Button startIcon={<AddBox />} onClick={onAdd} disabled={disabled}>
        {addText}
      </Button>
    </Stack>
  );
};

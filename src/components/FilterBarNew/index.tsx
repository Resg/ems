import React, { useCallback, useMemo, useState } from 'react';
import { Form, Formik, FormikProps } from 'formik';
import { useSelector } from 'react-redux';
import { createSearchParams, useSearchParams } from 'react-router-dom';

import { Close, DeleteSweep, Save, Search, Tune } from '@mui/icons-material';
import {
  Button,
  Divider,
  IconButton,
  OutlinedInput,
  Stack,
} from '@mui/material';

import {
  useCreateChipMutation,
  useGetSavedFiltersQuery,
} from '../../services/api/filters';
import { RootState, useAppDispatch } from '../../store';
import { setFilters } from '../../store/filters';
import { clearFalsyValues } from '../../utils/filterBar';
import Card from '../Card';
import Input from '../Input';

import { FiltersStack } from './FiltersStack';

import styles from './FilterBar.module.scss';

export type FilterBarProps = {
  formKey: string;
  placeholder?: string;
  children?: React.ReactNode;
  createFilterButtons?: (
    onSearch: ({ values }: { values: FormValues }) => () => Promise<void>,
    props: FormikProps<FormValues>
  ) => React.ReactNode;
  refetchSearch?: () => void;
  initValues?: any;
};

export interface FormValues {
  chipName: string;
  filters: Record<string, any>;
}

export const FilterBar: React.FC<FilterBarProps> = React.memo(
  ({
    children,
    formKey,
    placeholder = 'Поиск',
    createFilterButtons,
    refetchSearch,
    initValues,
  }) => {
    const initialValues: FormValues = useMemo(() => {
      return {
        chipName: '',
        filters: initValues || {},
      };
    }, [initValues]);
    const [isOpen, setIsOpen] = useState(false);
    const [chipWasClicked, setChipClicked] = useState(false);
    const [selectedChipId, setSelectedChipId] = useState<number | undefined>();
    const [searchParams, setSearchParams] = useSearchParams();
    const { data: filterStack, refetch } = useGetSavedFiltersQuery({ formKey });
    const [createChip] = useCreateChipMutation();
    const dispatch = useAppDispatch();
    const filtersSearch = useSelector(
      (state: RootState) => state.filters.data[formKey] || {}
    );

    const toggleOpened = useCallback((isOpened: boolean) => {
      setIsOpen(isOpened);
    }, []);

    const saveChipParameters = useCallback(
      ({ resetForm, values }: { resetForm: () => void; values: FormValues }) =>
        () => {
          createChip({
            data: {
              formKey,
              name: values.chipName,
              filter: JSON.stringify(values.filters),
            },
          })
            .then(() => {
              refetch();
            })
            .catch();
        },
      [createChip, formKey, refetch]
    );

    const searchWithChipParameters = useCallback(
      ({ values }: { values: FormValues }) =>
        async () => {
          toggleOpened(false);
          const response = await createChip({
            data: {
              formKey,
              name: values.chipName || '#_Поиск',
              filter: JSON.stringify(clearFalsyValues(values.filters)),
            },
          });

          if ('data' in response) {
            if (
              refetchSearch &&
              Object.keys(clearFalsyValues(values.filters)).length
            ) {
              refetchSearch();
            }
            setSelectedChipId(response.data.id);
            searchParams.set('chipsId', response.data.id);
            setSearchParams(searchParams);
            dispatch(
              setFilters({
                [formKey]: {
                  ...clearFalsyValues(values.filters),
                },
              })
            );
            refetch();
          }
        },
      [
        createChip,
        dispatch,
        formKey,
        refetch,
        setSearchParams,
        toggleOpened,
        filtersSearch,
        refetchSearch,
      ]
    );

    const chipClick = useCallback(
      ({
          resetForm,
          setFieldValue,
        }: {
          resetForm: () => void;
          setFieldValue: (field: string, value: any) => void;
        }) =>
        (
          currentChipId: number,
          chipName: string,
          chipFilters: string,
          selected = false
        ) => {
          setChipClicked(!selected);
          if (selected) {
            resetForm();
            setSelectedChipId(undefined);
            dispatch(setFilters({ [formKey]: {} }));
            searchParams.delete('chipsId');
            setSearchParams(createSearchParams(searchParams));
          } else {
            const filter = JSON.parse(chipFilters);
            setFieldValue('filters', filter);
            setFieldValue('chipName', chipName);
            setSelectedChipId(currentChipId);
            dispatch(setFilters({ [formKey]: filter }));
            searchParams.set('chipsId', currentChipId.toString());
            setSearchParams(searchParams);
            refetchSearch && refetchSearch();
          }
        },
      [dispatch, setSearchParams]
    );

    const globalSearch = useCallback(
      ({ values }: { values: FormValues }) =>
        async () => {
          toggleOpened(false);
          const response = await createChip({
            data: {
              formKey,
              name: '#_Поиск',
              filter: JSON.stringify({
                elasticSearch: values.filters.elasticSearch,
              }),
            },
          });

          if ('data' in response) {
            if (
              refetchSearch &&
              Object.keys(clearFalsyValues(values.filters)).length
            ) {
              refetchSearch();
            }
            setSelectedChipId(response.data.id);
            searchParams.set('chipsId', response.data.id);
            setSearchParams(searchParams);
            dispatch(
              setFilters({
                [formKey]: { elasticSearch: values.filters.elasticSearch },
              })
            );
            refetch();
          }
        },
      [
        createChip,
        dispatch,
        formKey,
        refetch,
        setSearchParams,
        toggleOpened,
        filtersSearch,
        refetchSearch,
      ]
    );

    // Не трогать
    // useEffect(() => {
    //   return () => {
    //     searchParams.delete('chipsId');
    //     setSearchParams(searchParams);
    //   }
    // }, []);

    return (
      <Formik<FormValues>
        initialValues={initialValues}
        onSubmit={() => {}}
        enableReinitialize
      >
        {(props) => {
          const {
            resetForm,
            values,
            setFieldValue,
            getFieldProps,
            handleChange,
          } = props;
          const handleEnterPress = async (
            e: React.KeyboardEvent<HTMLDivElement>
          ) => {
            if (e.key === 'Enter') {
              toggleOpened(false);
              const response = await createChip({
                data: {
                  formKey,
                  name: '#_Поиск',
                  filter: JSON.stringify({
                    elasticSearch: values.filters.elasticSearch,
                  }),
                },
              });

              if ('data' in response) {
                if (
                  refetchSearch &&
                  Object.keys(clearFalsyValues(values.filters)).length
                ) {
                  refetchSearch();
                }
                setSelectedChipId(response.data.id);
                searchParams.set('chipsId', response.data.id);
                setSearchParams(searchParams);
                dispatch(
                  setFilters({
                    [formKey]: { elasticSearch: values.filters.elasticSearch },
                  })
                );
                refetch();
              }
            }
          };

          return (
            <div className={styles.container}>
              <Card className={styles.filterBarContainer}>
                <Form>
                  <Stack
                    className={styles.filterBar}
                    direction="row"
                    spacing={1.5}
                    divider={<Divider orientation="vertical" flexItem />}
                  >
                    <OutlinedInput
                      className={styles.input}
                      size="small"
                      placeholder={placeholder}
                      onChange={handleChange}
                      value={values.filters.elasticSearch || ''}
                      name="filters.elasticSearch"
                      onKeyPress={handleEnterPress}
                      endAdornment={
                        <IconButton onClick={globalSearch({ values })}>
                          <Search />
                        </IconButton>
                      }
                    />
                    <Stack
                      direction="row"
                      spacing={1.5}
                      className={styles.filterStack}
                    >
                      <FiltersStack
                        formKey={formKey}
                        chipClick={chipClick({
                          resetForm,
                          setFieldValue,
                        })}
                        selectedChipId={selectedChipId}
                        refetch={refetch}
                        refetchSearch={refetchSearch!}
                      />
                    </Stack>
                    <Stack
                      className={styles.moreBtn}
                      direction="row"
                      spacing={1.5}
                    >
                      {isOpen ? (
                        <IconButton
                          className={styles.tuneBtn}
                          onClick={() => setIsOpen(false)}
                        >
                          <Close />
                        </IconButton>
                      ) : (
                        <IconButton
                          className={styles.tuneBtn}
                          onClick={() => setIsOpen(true)}
                        >
                          <Tune />
                        </IconButton>
                      )}
                    </Stack>
                  </Stack>
                  <Stack
                    className={isOpen ? styles.expanded : styles.collapsed}
                  >
                    {children}
                    <div className={styles.filterButtonsContainer}>
                      <div className={styles.parameterSearchContainer}>
                        <Input
                          id="chipName"
                          label="Введите название параметра поиска"
                          {...getFieldProps('chipName')}
                        />
                        <IconButton
                          className={styles.tuneBtn}
                          onClick={saveChipParameters({ resetForm, values })}
                        >
                          <Save />
                        </IconButton>
                      </div>
                      <div className={styles.filterButtons}>
                        {createFilterButtons?.(
                          searchWithChipParameters,
                          props
                        ) || (
                          <>
                            <Button
                              color="primary"
                              variant="outlined"
                              startIcon={<Search />}
                              onClick={searchWithChipParameters({ values })}
                            >
                              Поиск
                            </Button>
                            <Button
                              color="primary"
                              variant="outlined"
                              startIcon={<DeleteSweep />}
                              onClick={() => resetForm()}
                            >
                              Очистить
                            </Button>
                          </>
                        )}
                      </div>
                    </div>
                  </Stack>
                </Form>
              </Card>
            </div>
          );
        }}
      </Formik>
    );
  }
);

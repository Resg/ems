import React, { useCallback } from 'react';
import cn from 'classnames';
import { useSearchParams } from 'react-router-dom';

import { Chip } from '@mui/material';

import { useDeleteFilterChipMutation } from '../../services/api/filters';
import { useAppDispatch } from '../../store';
import { setFilters } from '../../store/filters';

import styles from './FilterBar.module.scss';

export type FilterChipProps = {
  id: number;
  formKey: string;
  label: string;
  filter: string;
  refetch: () => void;
  refetchSearch: () => void;
  onClick: (
    id: number,
    label: string,
    filter: string,
    selected: boolean
  ) => void;
  selected: boolean;
};

export const FilterChip: React.FC<FilterChipProps> = React.memo(
  ({
    id,
    label,
    filter,
    formKey,
    onClick,
    selected,
    refetch,
    refetchSearch,
  }) => {
    const [deleteChip] = useDeleteFilterChipMutation();
    const [searchParams, setSearchParams] = useSearchParams();
    const dispatch = useAppDispatch();

    const handleDelete = useCallback(async () => {
      await deleteChip({ id });
      refetch();
      dispatch(setFilters({ [formKey]: filter }));
      searchParams.delete('chipsId');
      setSearchParams(searchParams);
      refetchSearch();
    }, [deleteChip, id, refetch]);

    const clickChipHandler = useCallback(() => {
      onClick(id, label, filter, selected);
    }, [selected, onClick, id, filter, label]);

    return (
      <Chip
        className={cn(styles.chip, { [styles.chip_checked]: selected })}
        classes={{ deleteIcon: selected ? styles.iconChecked : '' }}
        id={id.toString()}
        label={label}
        onDelete={handleDelete}
        onClick={clickChipHandler}
      />
    );
  }
);

import React from 'react';

import { Stack } from '@mui/material';

import { useGetSavedFiltersQuery } from '../../services/api/filters';

import { FilterChip } from './FilterChip';

import styles from './FilterBar.module.scss';

export type FilterStackProps = {
  formKey: string;
  selectedChipId?: number;
  refetch: () => void;
  refetchSearch: () => void;
  chipClick: (
    currentChipId: number,
    chipName: string,
    chipFilters: string,
    selected: boolean
  ) => void;
};

export const FiltersStack: React.FC<FilterStackProps> = React.memo(
  ({ formKey, chipClick, selectedChipId, refetch, refetchSearch }) => {
    const { data, isLoading } = useGetSavedFiltersQuery({ formKey });
    return (
      <Stack direction="row" spacing={1.5} className={styles.tagBox}>
        {!isLoading &&
          data?.map((chip: any) => (
            <FilterChip
              key={chip.id}
              refetch={refetch}
              refetchSearch={refetchSearch}
              id={chip.id}
              label={chip.name}
              formKey={formKey}
              filter={chip.filter}
              onClick={chipClick}
              selected={chip.id === selectedChipId}
            />
          ))}
      </Stack>
    );
  }
);

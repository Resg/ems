import { useEffect } from 'react';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { Grid } from '@mui/material';

import {
  SearchFieldsPhysicalEntity,
  SearchLabelsPhysicalEntity,
} from '../../../constants/advancedCounterpartySearch';
import { CheckboxInput } from '../../CheckboxInput';
import { DateRange } from '../../DateRange';
import Input from '../../Input';

export const PhysicalEntityAdvancedSearch: React.FC = ({}) => {
  const { values, handleChange, setFieldValue } = useFormikContext<any>();

  useEffect(() => {
    const FieldsArray = [
      SearchFieldsPhysicalEntity.LAST_NAME,
      SearchFieldsPhysicalEntity.FIRST_NAME,
      SearchFieldsPhysicalEntity.MIDDLE_NAME,
      SearchFieldsPhysicalEntity.PASSPORT_SERIES,
      SearchFieldsPhysicalEntity.PASSPORT_NUMBER,
      SearchFieldsPhysicalEntity.INN,
      SearchFieldsPhysicalEntity.COMMENTS,
      SearchFieldsPhysicalEntity.CREATE_DATE_FROM,
      SearchFieldsPhysicalEntity.CREATE_DATE_TO,
    ];

    if (FieldsArray.some((element) => Boolean(get(values, element)))) {
      setFieldValue(SearchFieldsPhysicalEntity.PHYSICAL_ENTITY, true);
    }

    if (FieldsArray.every((element) => !get(values, element))) {
      setFieldValue(SearchFieldsPhysicalEntity.PHYSICAL_ENTITY, false);
    }
  }, [
    get(values, SearchFieldsPhysicalEntity.LAST_NAME),
    get(values, SearchFieldsPhysicalEntity.FIRST_NAME),
    get(values, SearchFieldsPhysicalEntity.MIDDLE_NAME),
    get(values, SearchFieldsPhysicalEntity.PASSPORT_SERIES),
    get(values, SearchFieldsPhysicalEntity.PASSPORT_NUMBER),
    get(values, SearchFieldsPhysicalEntity.INN),
    get(values, SearchFieldsPhysicalEntity.COMMENTS),
    get(values, SearchFieldsPhysicalEntity.CREATE_DATE_FROM),
    get(values, SearchFieldsPhysicalEntity.CREATE_DATE_TO),
  ]);

  return (
    <Grid container spacing={2} sx={{ mt: -1.5 }}>
      <Grid item xs={12}>
        <CheckboxInput
          name={SearchFieldsPhysicalEntity.PHYSICAL_ENTITY}
          label={
            SearchLabelsPhysicalEntity[
              SearchFieldsPhysicalEntity.PHYSICAL_ENTITY
            ]
          }
        />
      </Grid>
      <Grid item xs={4}>
        <Input
          name={SearchFieldsPhysicalEntity.LAST_NAME}
          label={
            SearchLabelsPhysicalEntity[SearchFieldsPhysicalEntity.LAST_NAME]
          }
          value={get(values, SearchFieldsPhysicalEntity.LAST_NAME) || ''}
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={4}>
        <Input
          name={SearchFieldsPhysicalEntity.FIRST_NAME}
          label={
            SearchLabelsPhysicalEntity[SearchFieldsPhysicalEntity.FIRST_NAME]
          }
          value={get(values, SearchFieldsPhysicalEntity.FIRST_NAME) || ''}
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={4}>
        <Input
          name={SearchFieldsPhysicalEntity.MIDDLE_NAME}
          label={
            SearchLabelsPhysicalEntity[SearchFieldsPhysicalEntity.MIDDLE_NAME]
          }
          value={get(values, SearchFieldsPhysicalEntity.MIDDLE_NAME) || ''}
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={4}>
        <Input
          name={SearchFieldsPhysicalEntity.PASSPORT_SERIES}
          label={
            SearchLabelsPhysicalEntity[
              SearchFieldsPhysicalEntity.PASSPORT_SERIES
            ]
          }
          value={get(values, SearchFieldsPhysicalEntity.PASSPORT_SERIES) || ''}
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={4}>
        <Input
          name={SearchFieldsPhysicalEntity.PASSPORT_NUMBER}
          label={
            SearchLabelsPhysicalEntity[
              SearchFieldsPhysicalEntity.PASSPORT_NUMBER
            ]
          }
          value={get(values, SearchFieldsPhysicalEntity.PASSPORT_NUMBER) || ''}
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={4}>
        <Input
          name={SearchFieldsPhysicalEntity.INN}
          label={SearchLabelsPhysicalEntity[SearchFieldsPhysicalEntity.INN]}
          value={get(values, SearchFieldsPhysicalEntity.INN) || ''}
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={8}>
        <Input
          name={SearchFieldsPhysicalEntity.COMMENTS}
          label={
            SearchLabelsPhysicalEntity[SearchFieldsPhysicalEntity.COMMENTS]
          }
          value={get(values, SearchFieldsPhysicalEntity.COMMENTS) || ''}
          onChange={handleChange}
          multiline
        />
      </Grid>
      <Grid item xs={4}>
        <DateRange
          nameFrom={SearchFieldsPhysicalEntity.CREATE_DATE_FROM}
          nameBefore={SearchFieldsPhysicalEntity.CREATE_DATE_TO}
          labelFrom={
            SearchLabelsPhysicalEntity[
              SearchFieldsPhysicalEntity.CREATE_DATE_FROM
            ]
          }
          gridContainerProps={{ xs: 12, sx: { mb: 0, mt: 0 }, spacing: 0 }}
          gridItemProps={{ xs: 12 }}
        />
      </Grid>
    </Grid>
  );
};

import { useCallback, useEffect } from 'react';
import { useFormikContext } from 'formik';
import { get } from 'lodash';
import InputMask from 'react-input-mask';
import { useSelector } from 'react-redux';

import { Grid } from '@mui/material';

import {
  SearchFields,
  SearchLabels,
} from '../../../constants/advancedCounterpartySearch';
import {
  useGetAsyncContractorsListMutation,
  useGetCounterpartiesAsyncMutation,
  useGetCountriesQuery,
  useGetOrganizationalLegalFormsQuery,
} from '../../../services/api/dictionaries';
import { RootState } from '../../../store';
import {
  Counterparty,
  OrganizationalLegalForm,
} from '../../../types/dictionaries';
import { AutoCompleteInput } from '../../AutoCompleteInput';
import { CheckboxInput } from '../../CheckboxInput';
import { DateRange } from '../../DateRange';
import { DebounceAutoComplete } from '../../DebounceAutoComplete';
import Input from '../../Input';

export const LegalEntityAdvancedSearch: React.FC = ({}) => {
  const { values, handleChange, setFieldValue } = useFormikContext<any>();

  const { data: contry } = useGetCountriesQuery();
  const { data: organizationalLegalForms } =
    useGetOrganizationalLegalFormsQuery();
  const [getCounterparties] = useGetCounterpartiesAsyncMutation();
  const [getContractorsList] = useGetAsyncContractorsListMutation();
  const docTypeId = useSelector(
    (state: RootState) => state.advancedCounterpartySearch.docType
  );

  const getOptions = useCallback(async (value: string) => {
    if (value) {
      const response = await getContractorsList({ name: value });
      if ('data' in response) {
        return response.data.data.map((option: Record<string, any>) => {
          return {
            label: option.name,
            value: option.id,
          };
        });
      }
      return [];
    }
    return [];
  }, []);

  const getValueOptions = useCallback(
    async (ids: number[]) => {
      if (ids && ids.length) {
        const counterparties = await getCounterparties({ ids });
        if ('data' in counterparties) {
          return (
            counterparties?.data.data.map((option: Counterparty) => {
              return {
                label: option.name,
                value: option.id,
              };
            }) || []
          );
        } else {
          return [];
        }
      }
      return [];
    },
    [getCounterparties]
  );

  useEffect(() => {
    const FieldsArray = [
      SearchFields.ORGANIZATION_LEGAL_FORM_ID,
      SearchFields.IS_BUDGET_ENTITY,
      SearchFields.NAME,
      SearchFields.FULL_NAME,
      SearchFields.INN,
      SearchFields.OGRN,
      SearchFields.COUNTRY_ID,
      SearchFields.PARENT_NAME,
      SearchFields.CONTACT_PERSON_NAME,
      SearchFields.COMMENTS,
      SearchFields.CREATE_DATE_FROM,
      SearchFields.CREATE_DATE_TO,
      SearchFields.IS_ON_HAND_ISSUANCE,
      SearchFields.IS_WORK_VIA_WEB_ALLOWED,
      SearchFields.IS_REORGANIZED,
    ];

    if (FieldsArray.some((element) => Boolean(get(values, element)))) {
      setFieldValue(SearchFields.LEGAL_ENTITY, true);
    }

    if (FieldsArray.every((element) => !get(values, element))) {
      setFieldValue(SearchFields.LEGAL_ENTITY, false);
    }
  }, [
    get(values, SearchFields.ORGANIZATION_LEGAL_FORM_ID),
    get(values, SearchFields.IS_BUDGET_ENTITY),
    get(values, SearchFields.NAME),
    get(values, SearchFields.FULL_NAME),
    get(values, SearchFields.INN),
    get(values, SearchFields.OGRN),
    get(values, SearchFields.COUNTRY_ID),
    get(values, SearchFields.PARENT_NAME),
    get(values, SearchFields.CONTACT_PERSON_NAME),
    get(values, SearchFields.COMMENTS),
    get(values, SearchFields.CREATE_DATE_FROM),
    get(values, SearchFields.CREATE_DATE_TO),
    get(values, SearchFields.IS_ON_HAND_ISSUANCE),
    get(values, SearchFields.IS_WORK_VIA_WEB_ALLOWED),
    get(values, SearchFields.IS_REORGANIZED),
  ]);

  useEffect(() => {
    if (docTypeId === 21) {
      setFieldValue(SearchFields.IS_OUTGOING_DOCUMENT_SENDING_FORBIDDEN, false);
    }
  }, [docTypeId]);

  useEffect(() => {
    setFieldValue(SearchFields.IS_ACTUAL, true);
  }, []);

  return (
    <Grid container spacing={2} sx={{ mt: -1.5 }}>
      <Grid item xs={12}>
        <CheckboxInput
          name={SearchFields.LEGAL_ENTITY}
          label={SearchLabels[SearchFields.LEGAL_ENTITY]}
        />
      </Grid>
      <Grid item xs={6}>
        <AutoCompleteInput
          name={SearchFields.ORGANIZATION_LEGAL_FORM_ID}
          label={SearchLabels[SearchFields.ORGANIZATION_LEGAL_FORM_ID]}
          options={
            organizationalLegalForms?.data?.map(
              (legalForm: OrganizationalLegalForm) => {
                return {
                  value: legalForm.id,
                  label: legalForm.name,
                };
              }
            ) || []
          }
        />
      </Grid>
      <Grid item xs={6}>
        <CheckboxInput
          name={SearchFields.IS_BUDGET_ENTITY}
          label={SearchLabels[SearchFields.IS_BUDGET_ENTITY]}
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          name={SearchFields.NAME}
          label={SearchLabels[SearchFields.NAME]}
          value={get(values, SearchFields.NAME) || ''}
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          name={SearchFields.FULL_NAME}
          label={SearchLabels[SearchFields.FULL_NAME]}
          value={get(values, SearchFields.FULL_NAME) || ''}
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={3}>
        <InputMask
          mask="999999999999"
          maskChar="_"
          value={get(values, SearchFields.INN) || ''}
          onChange={handleChange}
        >
          <Input
            name={SearchFields.INN}
            type="text"
            label={SearchLabels[SearchFields.INN]}
          />
        </InputMask>
      </Grid>
      <Grid item xs={3}>
        <InputMask
          mask="9999999999999"
          maskChar="_"
          value={get(values, SearchFields.OGRN) || ''}
          onChange={handleChange}
        >
          <Input
            name={SearchFields.OGRN}
            label={SearchLabels[SearchFields.OGRN]}
          />
        </InputMask>
      </Grid>
      <Grid item xs={6}>
        <AutoCompleteInput
          name={SearchFields.COUNTRY_ID}
          label={SearchLabels[SearchFields.COUNTRY_ID]}
          options={contry || []}
        />
      </Grid>
      <Grid item xs={6}>
        <DebounceAutoComplete
          name={SearchFields.PARENT_NAME}
          label={SearchLabels[SearchFields.PARENT_NAME]}
          getOptions={getOptions}
          getValueOptions={getValueOptions}
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          name={SearchFields.CONTACT_PERSON_NAME}
          label={SearchLabels[SearchFields.CONTACT_PERSON_NAME]}
          value={get(values, SearchFields.CONTACT_PERSON_NAME) || ''}
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          name={SearchFields.COMMENTS}
          label={SearchLabels[SearchFields.COMMENTS]}
          value={get(values, SearchFields.COMMENTS) || ''}
          onChange={handleChange}
          multiline
        />
      </Grid>
      <Grid item xs={6}>
        <DateRange
          nameFrom={SearchFields.CREATE_DATE_FROM}
          nameBefore={SearchFields.CREATE_DATE_TO}
          labelFrom={SearchLabels[SearchFields.CREATE_DATE_FROM]}
          gridContainerProps={{ xs: 12, sx: { mb: 0, mt: 0 }, spacing: 0 }}
          gridItemProps={{ xs: 12 }}
        />
      </Grid>
      <Grid item xs={6}>
        <CheckboxInput
          name={SearchFields.IS_ACTUAL}
          label={SearchLabels[SearchFields.IS_ACTUAL]}
        />
      </Grid>
      <Grid item xs={6}>
        <CheckboxInput
          name={SearchFields.IS_WORK_VIA_WEB_ALLOWED}
          label={SearchLabels[SearchFields.IS_WORK_VIA_WEB_ALLOWED]}
        />
      </Grid>
      <Grid item xs={6}>
        <CheckboxInput
          name={SearchFields.IS_ON_HAND_ISSUANCE}
          label={SearchLabels[SearchFields.IS_ON_HAND_ISSUANCE]}
        />
      </Grid>
      <Grid item xs={6}>
        <CheckboxInput
          name={SearchFields.IS_REORGANIZED}
          label={SearchLabels[SearchFields.IS_REORGANIZED]}
        />
      </Grid>
    </Grid>
  );
};

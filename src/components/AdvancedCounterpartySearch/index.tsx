import { useEffect, useState } from 'react';
import { useSearchParams } from 'react-router-dom';

import { Tab, Tabs } from '@mui/material';

import { SearchTabs } from '../../constants/advancedCounterpartySearch';
import { useAppDispatch } from '../../store';
import { setAdvancedCounterpartySearch } from '../../store/advancedCounterpartySearch';
import Card from '../Card';
import { TabPanel } from '../TabPanel';

import { LegalEntityAdvancedSearch } from './Entities/LegalEntity';
import { PhysicalEntityAdvancedSearch } from './Entities/PhysicalEntity';

import styles from './styles.module.scss';

export const AdvancedCounterpartySearch: React.FC = ({}) => {
  const [tabValue, setTabValue] = useState(SearchTabs.LEGAL_ENTITY);
  const [searchParams] = useSearchParams();
  const type = searchParams.get('documentType');
  const dispatch = useAppDispatch();

  const handleTabChange = (e: React.SyntheticEvent, newValue: SearchTabs) => {
    setTabValue(newValue);
  };

  useEffect(() => {
    if (type === 'DOCS_OUTCOMING') {
      dispatch(setAdvancedCounterpartySearch(21));
    }
  }, [type]);

  return (
    <Card className={styles.card}>
      <div className={styles.tabBar}>
        <Tabs
          value={tabValue}
          onChange={handleTabChange}
          scrollButtons={false}
          variant="scrollable"
        >
          {Object.values(SearchTabs).map((tab) => (
            <Tab key={tab} label={tab} value={tab} />
          ))}
        </Tabs>
      </div>
      <TabPanel value={tabValue} index={SearchTabs.LEGAL_ENTITY}>
        <LegalEntityAdvancedSearch />
      </TabPanel>
      <TabPanel value={tabValue} index={SearchTabs.PHYSICAL_ENTITY}>
        <PhysicalEntityAdvancedSearch />
      </TabPanel>
    </Card>
  );
};

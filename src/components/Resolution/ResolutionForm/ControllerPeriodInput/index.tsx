import React, { useEffect } from 'react';
import { addDays, format, isValid, parse } from 'date-fns';
import { useFormikContext } from 'formik';

import {
  ResolutionFields,
  ResolutionLabels,
} from '../../../../constants/resolution';
import Input from '../../../Input';

export interface ControllerPeriodInputProps {
  disabled: boolean;
}
export const ControllerPeriodInput: React.FC<ControllerPeriodInputProps> = ({
  disabled,
}) => {
  const { handleBlur, handleChange, values, setFieldValue, errors } =
    useFormikContext<any>();
  const isControlled = values[ResolutionFields.IS_CONTROLLED];
  const value = values[ResolutionFields.CONTROL_PERIOD] || '';

  const handleChangeValue = (event: React.ChangeEvent<HTMLInputElement>) => {
    const date = parse(values[ResolutionFields.DATE], 'yyyy-MM-dd', new Date());
    const value = Number(event.target.value);
    if (isValid(date) && value < 10000) {
      setFieldValue(
        ResolutionFields.CONTROL_DATE,
        format(addDays(date, value), 'yyyy-MM-dd')
      );
    }
    handleChange(event);
  };

  useEffect(() => {
    if (!isControlled) {
      setFieldValue(ResolutionFields.CONTROL_PERIOD, null);
    }
  }, [isControlled, setFieldValue]);

  return (
    <Input
      type="number"
      name={ResolutionFields.CONTROL_PERIOD}
      label={ResolutionLabels[ResolutionFields.CONTROL_PERIOD]}
      onChange={handleChangeValue}
      onBlur={handleBlur}
      value={value}
      disabled={disabled || !isControlled}
      error={Boolean(errors[ResolutionFields.CONTROL_PERIOD])}
      helperText={errors[ResolutionFields.CONTROL_PERIOD] as string}
    />
  );
};

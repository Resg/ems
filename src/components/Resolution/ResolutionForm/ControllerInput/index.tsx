import React, { useEffect } from 'react';
import { useFormikContext } from 'formik';

import { Grid } from '@mui/material';

import {
  ResolutionFields,
  ResolutionLabels,
} from '../../../../constants/resolution';
import { CheckboxInput } from '../../../CheckboxInput';
import { ClerkInput } from '../../../ClerkInput';

export interface ControllerInputProps {
  disabled?: boolean;
}

export const ControllerInput: React.FC<ControllerInputProps> = ({
  disabled,
}) => {
  const { values, setFieldValue } = useFormikContext<any>();

  const isControlled = values[ResolutionFields.IS_CONTROLLED];

  useEffect(() => {
    if (!isControlled) {
      setFieldValue(ResolutionFields.CONTROLLER_ID, null);
      setFieldValue(ResolutionFields.IS_ITEM_CONTROLLED, false);
    } else {
      setFieldValue(
        ResolutionFields.CONTROLLER_ID,
        values[ResolutionFields.EDITOR_ID]
      );
    }
  }, [isControlled]);

  return (
    <>
      <Grid item xs={4}>
        <CheckboxInput
          name={ResolutionFields.IS_CONTROLLED}
          label={ResolutionLabels[ResolutionFields.IS_CONTROLLED]}
          disabled={disabled}
        />
      </Grid>
      <Grid item xs={8}>
        <ClerkInput
          name={ResolutionFields.CONTROLLER_ID}
          label={ResolutionLabels[ResolutionFields.CONTROLLER_ID]}
          disabled={!values[ResolutionFields.IS_CONTROLLED] || disabled}
        />
      </Grid>
    </>
  );
};

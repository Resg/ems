import React, { useEffect } from 'react';
import { differenceInCalendarDays, parse } from 'date-fns';
import { useFormikContext } from 'formik';

import {
  ResolutionFields,
  ResolutionLabels,
} from '../../../../constants/resolution';
import { Resolution } from '../../../../types/resolution';
import { DatePickerInput } from '../../../DatePickerInput';

export interface ControlDateInputProps {
  disabled?: boolean;
}

export const ControlDateInput: React.FC<ControlDateInputProps> = ({
  disabled,
}) => {
  const { values, setFieldValue } = useFormikContext<Resolution>();
  const value = values[ResolutionFields.CONTROL_DATE];
  const date = values[ResolutionFields.DATE];
  const isControlled = values[ResolutionFields.IS_CONTROLLED];

  useEffect(() => {
    if (value && date) {
      const valueDate = parse(value, 'yyyy-MM-dd', new Date());
      const dateDate = parse(date, 'yyyy-MM-dd', new Date());

      const period = differenceInCalendarDays(valueDate, dateDate);
      setFieldValue(ResolutionFields.CONTROL_PERIOD, period);
    }
  }, [date, setFieldValue, value]);

  useEffect(() => {
    if (!isControlled) {
      setFieldValue(ResolutionFields.CONTROL_DATE, null);
    }
  }, [isControlled, setFieldValue]);

  return (
    <DatePickerInput
      label={ResolutionLabels[ResolutionFields.CONTROL_DATE]}
      name={ResolutionFields.CONTROL_DATE}
      disabled={disabled || !isControlled}
      required={isControlled}
    />
  );
};

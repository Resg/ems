import React, { useEffect } from 'react';
import { useFormikContext } from 'formik';

import { Grid } from '@mui/material';

import {
  ResolutionFields,
  ResolutionLabels,
} from '../../../../constants/resolution';
import { CheckboxInput } from '../../../CheckboxInput';
import Input from '../../../Input';

export interface ControllerItemInputProps {
  disabled?: boolean;
}

export const ControllerItemInput: React.FC<ControllerItemInputProps> = ({
  disabled,
}) => {
  const { values, setFieldValue, handleChange, handleBlur } =
    useFormikContext<any>();

  const isControlled = values[ResolutionFields.IS_ITEM_CONTROLLED];

  useEffect(() => {
    if (!isControlled) {
      setFieldValue(ResolutionFields.CONTROL_ITEM, '');
    }
  }, [isControlled]);

  return (
    <>
      <Grid item xs={4}>
        <CheckboxInput
          name={ResolutionFields.IS_ITEM_CONTROLLED}
          label={ResolutionLabels[ResolutionFields.IS_ITEM_CONTROLLED]}
          disabled={disabled}
        />
      </Grid>
      <Grid item xs={8}>
        <Input
          value={values[ResolutionFields.CONTROL_ITEM] || ''}
          name={ResolutionFields.CONTROL_ITEM}
          label={ResolutionLabels[ResolutionFields.CONTROL_ITEM]}
          onChange={handleChange}
          onBlur={handleBlur}
          disabled={!values[ResolutionFields.IS_ITEM_CONTROLLED] || disabled}
        />
      </Grid>
    </>
  );
};

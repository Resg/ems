import React, { useCallback, useMemo, useState } from 'react';
import { Form, Formik, FormikErrors, FormikTouched } from 'formik';
import { isEmpty } from 'lodash';

import {
  CancelScheduleSend,
  Edit,
  NotificationsOff,
  Save,
  Send,
} from '@mui/icons-material';
import { Grid } from '@mui/material';

import {
  DefaultResolution,
  ResolutionFields,
  ResolutionLabels,
} from '../../../constants/resolution';
import { useGetResolutionDescriptionsQuery } from '../../../services/api/dictionaries';
import {
  useApproveResolutionMutation,
  useGetResolutionQuery,
  useRevokeResolutionMutation,
  useRunResolutionMutation,
  useSaveResolutionMutation,
} from '../../../services/api/resolutions';
import { Resolution } from '../../../types/resolution';
import { AutoSuggestInput } from '../../AutoSuggestInput';
import { ClerkInput } from '../../ClerkInput';
import { DatePickerInput } from '../../DatePickerInput';
import Input from '../../Input';
import { RoundButton } from '../../RoundButton';
import { SettingsTypes, ToolButton, ToolsPanel } from '../../ToolsPanel';

import { ControlDateInput } from './ControlDateInput';
import { ControllerInput } from './ControllerInput';
import { ControllerItemInput } from './ControllerItemInput';
import { ControllerPeriodInput } from './ControllerPeriodInput';
import { validationSchema } from './validation';

import styles from './styles.module.scss';

export interface ResolutionProps {
  id?: number | null;
  documentIds?: number[];
  edit?: boolean;
  onClose: () => void;
  refetch: () => void;
}

export const ResolutionForm: React.FC<ResolutionProps> = ({
  documentIds,
  id,
  edit = false,
  onClose,
  refetch,
}) => {
  const [isEditMode, setIsEditMode] = useState(edit);
  const { data: descriptions = [] } = useGetResolutionDescriptionsQuery({});
  const { data } = useGetResolutionQuery({ id });

  const [saveResolution, { data: saveResolutionData = {} }] =
    useSaveResolutionMutation();
  const [runResolution] = useRunResolutionMutation();
  const [approveResolution] = useApproveResolutionMutation();
  const [revokeResolution] = useRevokeResolutionMutation();

  const handleSubmit = useCallback(
    async (values: Resolution) => {
      await saveResolution(values);
      setIsEditMode(false);
    },
    [saveResolution]
  );

  const handleRun = useCallback(
    (
        values: Resolution,
        validateForm: (values: Resolution) => PromiseLike<FormikErrors<any>>,
        setErrors: (errors: FormikErrors<any>) => void,
        setTouched: (touched: FormikTouched<any>) => void
      ) =>
      async () => {
        const errors = await validateForm(values);

        if (!isEmpty(errors)) {
          const touched: FormikTouched<any> = {};
          Object.keys(errors).forEach((key) => (touched[key] = true));
          setTouched(touched);
          setErrors(errors);
          return;
        }

        if (isEditMode) {
          const response = await saveResolution(values);
          if ('data' in response) {
            setIsEditMode(false);
            const runResponse = await runResolution(response.data);
            if ('data' in runResponse) {
              onClose();
              refetch();
            }
          }
        } else {
          const runResponse = await runResolution(values);
          if ('data' in runResponse) {
            onClose();
            refetch();
          }
        }
      },
    [isEditMode, runResolution, saveResolution]
  );

  const handleRevoke = useCallback(() => {
    if (id) {
      revokeResolution({ id });
    }
  }, [id, revokeResolution]);

  const handleApprove = useCallback(async () => {
    if (id) {
      approveResolution({ id });
    }
  }, [approveResolution, id]);

  const initialValues = useMemo(
    () => ({
      ...(data || DefaultResolution),
      ids: id ? [id] : [],
      documentIds: documentIds?.length ? documentIds : [],
      ...saveResolutionData,
    }),
    [data, documentIds, id, saveResolutionData]
  );

  const stopPropagation = useCallback(
    (e: React.KeyboardEvent<HTMLDivElement>) => e.stopPropagation(),
    []
  );

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      enableReinitialize
      onSubmit={handleSubmit}
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        errors,
        touched,
        validateForm,
        setTouched,
        setErrors,
        values,
      }) => {
        return (
          <Form className={styles.container}>
            <ToolsPanel
              leftActions={
                isEditMode ? (
                  <RoundButton onClick={() => handleSubmit()} icon={<Save />} />
                ) : (
                  <RoundButton
                    icon={<Edit />}
                    onClick={() => setIsEditMode(true)}
                  />
                )
              }
              settings={[SettingsTypes.TOOLS]}
              className={styles.tools}
            >
              <ToolButton
                label={'Запустить'}
                startIcon={<Send />}
                fast
                onClick={handleRun(values, validateForm, setErrors, setTouched)}
              />
              <ToolButton
                label={'Отозвать'}
                startIcon={<CancelScheduleSend />}
                fast
                onClick={handleRevoke}
                disabled={!id}
              />
              <ToolButton
                label={'Снять с контроля'}
                startIcon={<NotificationsOff />}
                fast
                onClick={handleApprove}
                disabled={!id}
              />
            </ToolsPanel>
            <Grid container columnSpacing={9} rowSpacing={3}>
              <Grid item xs={4}>
                <DatePickerInput
                  label={ResolutionLabels[ResolutionFields.DATE]}
                  name={ResolutionFields.DATE}
                  disabled={!isEditMode}
                />
              </Grid>
              <Grid item xs={8}>
                <ClerkInput
                  name={ResolutionFields.AUTHOR_ID}
                  label={ResolutionLabels[ResolutionFields.AUTHOR_ID]}
                  required
                  disabled={!isEditMode}
                  onKeyDown={stopPropagation}
                />
              </Grid>
              <Grid item xs={4}>
                <Input
                  name={ResolutionFields.STATUS}
                  label={ResolutionLabels[ResolutionFields.STATUS]}
                  disabled
                  value={values[ResolutionFields.STATUS] || ''}
                  onChange={handleChange}
                  notEditableField={true}
                  onBlur={handleBlur}
                />
              </Grid>
              <Grid item xs={8}>
                <ClerkInput
                  name={ResolutionFields.EDITOR_ID}
                  label={ResolutionLabels[ResolutionFields.EDITOR_ID]}
                  disabled={
                    Boolean(data?.[ResolutionFields.EDITOR_ID]) || !isEditMode
                  }
                  onKeyDown={stopPropagation}
                />
              </Grid>
              <Grid item xs={12}>
                <ClerkInput
                  name={ResolutionFields.EXECUTOR_IDS}
                  label={ResolutionLabels[ResolutionFields.EXECUTOR_IDS]}
                  multiple
                  required
                  disabled={!isEditMode}
                  onKeyDown={stopPropagation}
                />
              </Grid>
              <Grid item xs={12}>
                <AutoSuggestInput
                  name={ResolutionFields.DESCRIPTION}
                  label={ResolutionLabels[ResolutionFields.DESCRIPTION]}
                  required
                  inputClassName={styles.description}
                  disabled={!isEditMode}
                  options={descriptions}
                  multiline
                />
              </Grid>

              <ControllerInput disabled={!isEditMode} />
              <ControllerItemInput disabled={!isEditMode} />
              <Grid item xs={4}>
                <ControllerPeriodInput disabled={!isEditMode} />
              </Grid>
              <Grid item xs={4}>
                <ControlDateInput disabled={!isEditMode} />
              </Grid>
              <Grid item xs={4}>
                <DatePickerInput
                  label={ResolutionLabels[ResolutionFields.CANCEL_DATE]}
                  name={ResolutionFields.CANCEL_DATE}
                  disabled
                />
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

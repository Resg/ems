import { array, boolean, number, object, string } from 'yup';

import { ResolutionFields } from '../../../../constants/resolution';

export const validationSchema = object({
  [ResolutionFields.DATE]: string().nullable(),
  [ResolutionFields.AUTHOR_ID]: number()
    .nullable()
    .required('Обязательное поле'),
  [ResolutionFields.CANCEL_DATE]: string().nullable(),
  [ResolutionFields.CONTROL_PERIOD]: number().max(10000).min(0).nullable(),
  [ResolutionFields.IS_ITEM_CONTROLLED]: boolean().nullable(),
  [ResolutionFields.CONTROL_ITEM]: number().nullable(),
  [ResolutionFields.CONTROL_DATE]: string()
    .nullable()
    .when(ResolutionFields.IS_CONTROLLED, {
      is: true,
      then: (schema) => schema.required('Обязательное поле'),
    }),
  [ResolutionFields.CONTROLLER_ID]: number().nullable(),
  [ResolutionFields.EXECUTOR_IDS]: array(number())
    .nullable()
    .required('Обязательное поле'),
  [ResolutionFields.DESCRIPTION]: string()
    .nullable()
    .required('Обязательное поле'),
  [ResolutionFields.EDITOR_ID]: number().nullable(),
});

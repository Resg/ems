import React, { useCallback } from 'react';
import { useSelector } from 'react-redux';

import { Approval, Close } from '@mui/icons-material';
import { Dialog, IconButton, Tab, Tabs } from '@mui/material';

import { RootState, useAppDispatch } from '../../../store';
import {
  isResolutionCreateFormOpen,
  isResolutionFormOpen,
} from '../../../store/utils';
import Card from '../../Card';
import { ToolButton } from '../../ToolsPanel';
import { ToolButtonProps } from '../../ToolsPanel/ToolButton';
import { ResolutionForm } from '../ResolutionForm';

import styles from './styles.module.scss';

export interface ResolutionToolButtonProps extends Partial<ToolButtonProps> {
  documentIds?: number[];
  refetch?: () => void;
  id?: number | null;
  type?: 'create' | 'edit';
}

export const ResolutionToolButton: React.FC<ResolutionToolButtonProps> = ({
  documentIds,
  refetch,
  id,
  type,
  ...toolButtonProps
}) => {
  const open = useSelector((state: RootState) =>
    type === 'edit'
      ? state.utils.isResolutionFormOpen
      : state.utils.isResolutionCreateFormOpen
  );
  const dispatch = useAppDispatch();

  const handleOpen = useCallback(
    (e: React.MouseEvent) => {
      type === 'edit'
        ? dispatch(isResolutionFormOpen(true))
        : dispatch(isResolutionCreateFormOpen(true));
    },
    [dispatch, type]
  );

  const handleClose = useCallback(() => {
    type === 'edit'
      ? dispatch(isResolutionFormOpen(false))
      : dispatch(isResolutionCreateFormOpen(false));
  }, [dispatch, type]);

  return (
    <>
      <ToolButton
        label={'создать резолюцию'}
        onClick={handleOpen}
        disabled={!documentIds || !documentIds.length}
        fast={true}
        startIcon={<Approval />}
        {...toolButtonProps}
      />
      <Dialog open={open} maxWidth={'lg'} scroll="body">
        <Card className={styles.card}>
          <div className={styles.title}>
            <div>Резолюция {id ? id : ''}</div>
            <IconButton
              className={styles['close-button']}
              onClick={handleClose}
            >
              <Close />
            </IconButton>
          </div>
          <Tabs value={0} className={styles.tabs}>
            <Tab value={0} label="Общие" />
          </Tabs>
          <ResolutionForm
            onClose={handleClose}
            refetch={refetch as () => void}
            documentIds={documentIds}
            edit={id ? false : true}
            id={id}
          />
        </Card>
      </Dialog>
    </>
  );
};

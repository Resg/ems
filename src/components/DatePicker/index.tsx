import React from 'react';

import { TextFieldProps } from '@mui/material';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import {
  DatePicker as DatePickerBase,
  DatePickerProps,
} from '@mui/x-date-pickers/DatePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';

import Input from '../Input';

import styles from './styles.module.scss';

type DatePickerPropsType = {
  textFieldProps: TextFieldProps;
  notEditableField?: boolean;
};

export const DatePicker: React.FC<
  DatePickerPropsType & Omit<DatePickerProps<any, any>, 'renderInput'>
> = ({ notEditableField, textFieldProps, value = null, ...restProps }) => {
  const inputLabelProps = {
    classes: {
      root: textFieldProps.required ? styles.requiredFieldLabel : '',
      focused: textFieldProps.required ? styles.requiredFieldLabel : '',
    },
  };
  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <DatePickerBase
        renderInput={(params: TextFieldProps) => {
          const { InputProps, ...otherParams } = params;
          const newInputProps = {
            ...InputProps,
            classes: {
              notchedOutline: textFieldProps.required
                ? styles.requiredField
                : '',
              disabled: notEditableField
                ? styles.notEditableTextField
                : styles.editableTextField,
            },
          };
          return (
            <Input
              {...otherParams}
              {...textFieldProps}
              sx={textFieldProps.sx ?? { width: '100%' }}
              InputProps={newInputProps}
              InputLabelProps={inputLabelProps}
            />
          );
        }}
        value={value}
        {...restProps}
        disabled={notEditableField || restProps.disabled}
        inputFormat="dd.MM.yyyy"
      />
    </LocalizationProvider>
  );
};

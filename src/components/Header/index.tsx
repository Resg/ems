import cn from 'classnames';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';

import { RootState } from '../../store';
import { BreadCrumbs } from '../BreadCrumbs';
import { UserMenu } from '../UserMenu';

import styles from './Header.module.scss';

export interface HeaderProps {
  sidebar?: boolean;
}

export const Header = (props: HeaderProps) => {
  const location = useLocation();
  const isSideBarOpen = useSelector(
    (state: RootState) => state.utils.sideBarIsOpen
  );
  const isEditor = location.pathname.includes('file_editor');

  return (
    <div
      className={cn(styles.box, {
        [styles.box_sidebar]: isSideBarOpen,
      })}
    >
      <div className={styles.left}>
        <BreadCrumbs />
      </div>
      <UserMenu />
    </div>
  );
};

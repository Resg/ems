import React, { useCallback, useMemo, useState } from 'react';
import cn from 'classnames';
import { Column } from 'react-data-grid';
import { useSelector } from 'react-redux';

import { ChevronRight, DeleteOutline } from '@mui/icons-material';
import { Refresh } from '@mui/icons-material/';
import { FormControlLabel, Radio } from '@mui/material';

import {
  useDeleteConclusionAdditionsTemplatesMutation,
  useGetConclusionAdditionsTemplatesQuery,
} from '../../../services/api/dictionaries';
import { RootState } from '../../../store';
import { AdditionalConclusionData } from '../../../types/conclusions';
import { DataTable } from '../../CustomDataGrid';
import { FilterBar } from '../../FilterBarNew';
import { RadioGroupInput } from '../../RadioGroupInput';
import { RoundButton } from '../../RoundButton';
import { DeleteToolButton, ToolButton, ToolsPanel } from '../../ToolsPanel';

import { AdditionsCols } from './constants';

import styles from './styles.module.scss';

export interface ConclusionAdditionsProps {
  setSelected?: (rows: string[], remove: string[]) => void;
  conclusionId?: string;
}

export const ConclusionAdditions: React.FC<ConclusionAdditionsProps> = ({
  setSelected,
}) => {
  const filters = useSelector(
    (state: RootState) =>
      state.filters.data['conclusion-additions'] || { isOnlyActual: false }
  );
  const [deleteAddition] = useDeleteConclusionAdditionsTemplatesMutation();

  const {
    data = [],
    refetch,
    isLoading,
  } = useGetConclusionAdditionsTemplatesQuery(filters);

  const [tableFilters, setTableFilters] = useState(false);

  const cols: Column<AdditionalConclusionData>[] = useMemo(() => {
    return AdditionsCols.map((col) => ({
      ...col,
      formatter: ({ row, column }) => {
        return (
          <span
            className={cn(styles.cell, { [styles.redCell]: !row.isActual })}
          >
            {row[column.key as keyof AdditionalConclusionData]}
          </span>
        );
      },
    }));
  }, []);
  const [selectedRows, setSelectedRows] = useState<Record<string, boolean>>({});

  const handleRowSelect = useCallback(
    (selectedMap: Record<string, boolean>) => {
      const newSelected = Object.keys(selectedMap).filter(
        (key: string) => !selectedRows[key] && selectedMap[key]
      );
      const removeSelected = Object.keys(selectedRows).filter(
        (key: string) => selectedRows[key] && !selectedMap[key]
      );

      setSelected?.(newSelected, removeSelected);
      setSelectedRows(selectedMap);
    },
    [selectedRows, setSelected]
  );

  const selectedAdditionsIds = useMemo(
    () => Object.keys(selectedRows).filter((id) => selectedRows[id]),
    [selectedRows]
  );

  // useEffect(() => {
  //   setSelected?.(selectedAdditionsIds);
  // }, [selectedAdditionsIds, setSelected]);

  const handleDelete = useCallback(async () => {
    await deleteAddition({ id: Number(selectedAdditionsIds[0]) });
    refetch();
  }, [deleteAddition, refetch, selectedAdditionsIds]);
  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );
  const handleDetails = useCallback(() => {
    window
      .open(
        `conclusions/additionals/dictionaries?id=${selectedAdditionsIds[0]}`,
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  }, [selectedAdditionsIds, openFormInNewTab]);

  return (
    <div>
      <FilterBar
        formKey={'conclusion-additions'}
        refetchSearch={() => refetch()}
      >
        <RadioGroupInput name={'filters.isOnlyActual'} row defaultValue={false}>
          <FormControlLabel value={false} control={<Radio />} label={'Все'} />
          <FormControlLabel
            value={true}
            control={<Radio />}
            label={'Актуальные'}
          />
        </RadioGroupInput>
      </FilterBar>

      <DataTable
        formKey={'conclusion_additions'}
        showFilters={tableFilters}
        cols={cols}
        rows={data}
        onRowSelect={handleRowSelect}
        loading={isLoading}
      >
        <ToolsPanel
          className={styles.tools}
          setFilters={setTableFilters}
          leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
        >
          <ToolButton
            label={'Подробно'}
            endIcon={<ChevronRight />}
            disabled={selectedAdditionsIds.length !== 1}
            onClick={handleDetails}
            fast
          />
          <DeleteToolButton
            label={'Удалить'}
            startIcon={<DeleteOutline />}
            disabled={selectedAdditionsIds.length !== 1}
            fast
            description={`Вы действительно хотите удалить дополнительное условие?`}
            title={'Удаление дополнительного условия'}
            onConfirm={handleDelete}
          />
        </ToolsPanel>
      </DataTable>
    </div>
  );
};

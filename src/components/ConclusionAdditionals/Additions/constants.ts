import { Column } from 'react-data-grid';

import { AdditionalConclusionData } from '../../../types/conclusions';

export const AdditionsCols: Column<AdditionalConclusionData>[] = [
  {
    name: 'Наименование',
    key: 'name',
  },
  { name: 'Текст для заключения', key: 'conclusionText' },
  { name: 'Текст для разрешения', key: 'permissionText' },
];

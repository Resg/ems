import React, { useCallback, useState } from 'react';
import { useSelector } from 'react-redux';

import { Check, Close } from '@mui/icons-material';
import { Button, Dialog, Stack } from '@mui/material';

import { useAddAdditionalToConclusionMutation } from '../../../services/api/conclusions';
import { RootState, useAppDispatch } from '../../../store';
import { setConclusionAdditionsFormOpen } from '../../../store/utils';
import { ToolButton, ToolButtonProps } from '../../ToolsPanel';
import { ConclusionAdditions } from '../Additions';

import styles from './styles.module.scss';

export interface ConclusionAdditionsButtonProps extends ToolButtonProps {
  id: number;
  onConfirm?: () => void;
}

export const ConclusionAdditionsButton: React.FC<
  ConclusionAdditionsButtonProps
> = ({ id, onConfirm, ...toolsButtonProps }) => {
  const open = useSelector(
    (state: RootState) => state.utils.isConclusionAdditionsFormOpen
  );
  const dispatch = useAppDispatch();
  const [add] = useAddAdditionalToConclusionMutation();

  const handleOpen = useCallback(() => {
    dispatch(setConclusionAdditionsFormOpen(true));
  }, []);

  const handleClose = useCallback((e: React.MouseEvent) => {
    e.stopPropagation();
    dispatch(setConclusionAdditionsFormOpen(false));
  }, []);

  const [selectedRows, setSelectedRows] = useState<string[]>([]);

  const setSelected = useCallback(
    (newSelected: string[], removeSelected: string[]) => {
      setSelectedRows(
        [...selectedRows, ...newSelected].filter(
          (id) => !removeSelected.includes(id)
        )
      );
    },
    [selectedRows]
  );

  const handleConfirm = useCallback(async () => {
    await add({ id, additionsIds: selectedRows.map((id) => Number(id)) });
    dispatch(setConclusionAdditionsFormOpen(false));
    onConfirm?.();
  }, [add, handleClose, id, onConfirm, selectedRows]);

  return (
    <>
      <ToolButton {...toolsButtonProps} onClick={handleOpen} />
      <Dialog
        open={open}
        maxWidth={'lg'}
        PaperProps={{ className: styles.card }}
      >
        <ConclusionAdditions setSelected={setSelected} />
        <Stack
          direction="row"
          spacing={1.5}
          justifyContent="flex-end"
          alignItems="center"
          className={styles.actions}
        >
          <Button
            variant="contained"
            startIcon={<Check />}
            onClick={handleConfirm}
            disabled={!selectedRows.length}
          >
            Выбрать
          </Button>
          <Button
            variant="outlined"
            startIcon={<Close />}
            onClick={handleClose}
          >
            Отмена
          </Button>
        </Stack>
      </Dialog>
    </>
  );
};

import React, { useCallback, useEffect, useMemo, useState } from 'react';
import cn from 'classnames';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { Autocomplete, Chip } from '@mui/material';

import { Option } from '../../types/dictionaries';
import Input from '../Input';

import styles from '../AutoCompleteInput/styles.module.scss';

interface DebounceAutoCompleteProps {
  name: string;
  label: string;
  multiple?: boolean;
  required?: boolean;
  disabled?: boolean;
  className?: string;
  getOptions: (
    value: string,
    controller?: AbortController
  ) => Promise<Option[]>;
  getValueOptions: (value: any) => Promise<Option[]>;
}

export const DebounceAutoComplete: React.FC<DebounceAutoCompleteProps> = ({
  name,
  label,
  multiple,
  required,
  disabled,
  className,
  getOptions,
  getValueOptions,
}) => {
  const { setFieldValue, values, errors, setFieldTouched, touched } =
    useFormikContext<any>();

  const formValue = useMemo(() => get(values, name), [name, values]);

  const [inputValue, setInputValue] = useState('');
  const [controller] = useState(() => new AbortController());
  const [options, setOptions] = useState<Option[]>([]);
  const [value, setValue] = useState<Option | Option[] | null>(() => {
    return multiple ? [] : null;
  });

  useEffect(() => {
    const checkValue = multiple ? formValue && formValue.length : formValue;
    const optLength = multiple ? formValue && formValue.length : 1;
    if (checkValue) {
      const getValueOptionsParam = multiple ? formValue : [formValue];
      if (getValueOptionsParam) {
        getValueOptions(getValueOptionsParam).then((valOptions: Option[]) => {
          if (valOptions !== undefined) {
            setOptions(valOptions);
            setValue(() => (multiple ? valOptions : valOptions[0] || null));
          }
        });
      }
    } else if (!checkValue) {
      setValue(() => (multiple ? [] : null));
    }
  }, [formValue]);

  useEffect(() => {
    const timeOut = setTimeout(() => {
      getOptions(inputValue).then((options) => setOptions(options));
    }, 500);
    return () => {
      clearTimeout(timeOut);
      controller.abort();
    };
  }, [inputValue]);

  const handleChange = async (
    _event: React.SyntheticEvent,
    newValue: Option | Option[] | null
  ) => {
    setValue(newValue);
    setFieldValue(
      name,
      Array.isArray(newValue)
        ? newValue.map((option: Option) => option.value)
        : newValue?.value || null
    );
    setOptions(() =>
      Array.isArray(newValue) ? newValue : newValue ? [newValue] : []
    );
  };

  const isTouched = get(touched, name);

  const error: string = useMemo(() => {
    const error = get(errors, name);
    if (!error) {
      return '';
    }
    if (Array.isArray(error)) {
      return error[0] as string;
    } else {
      return error as string;
    }
  }, [errors, name]);

  const handleBlur = useCallback(() => {
    setFieldTouched(name, true, true);
  }, [name, setFieldTouched]);

  return (
    <Autocomplete
      size={'small'}
      value={value}
      inputValue={inputValue}
      className={cn(styles.autocomplete, className)}
      renderOption={(params, option) => {
        return (
          <li {...params} key={option.value}>
            {option.label}
          </li>
        );
      }}
      onInputChange={(_, rowValue) => {
        setInputValue(rowValue);
      }}
      renderInput={(params) => {
        return (
          <Input
            {...params}
            label={label}
            error={isTouched && Boolean(error)}
            helperText={isTouched && error}
            required={required}
          />
        );
      }}
      renderTags={(value, getTagProps) =>
        value.map((option, index) => (
          <Chip
            {...getTagProps({ index })}
            size={'small'}
            label={option.label}
            classes={{ disabled: styles.chip }}
          />
        ))
      }
      onBlur={handleBlur}
      multiple={multiple}
      onChange={handleChange}
      options={options}
      disabled={disabled}
      forcePopupIcon={false}
    />
  );
};

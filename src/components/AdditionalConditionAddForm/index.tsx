import React, { useCallback, useMemo, useState } from 'react';
import { useSearchParams } from 'react-router-dom';

import {
  Close,
  DeleteOutline,
  PlaylistAdd,
  Refresh,
} from '@mui/icons-material';
import { Button, Stack } from '@mui/material';

import { Popup } from '../../components/Popup';
import { RoundButton } from '../../components/RoundButton';
import { useCreateAdditionalConditionMutation } from '../../services/api/conclusions';
import {
  useDeleteAdditionalConditionListMutation,
  useGetAdditionalConditionListQuery,
} from '../../services/api/user';
import { DataTable } from '../CustomDataGrid';

import styles from './styles.module.scss';

interface TablePageProps {
  open: boolean;
  onClose: () => void;
}

export const AdditionalConditionAddForm: React.FC<TablePageProps> = ({
  open,
  onClose,
}) => {
  const [filters, setFilters] = useState(false);
  const [searchParams] = useSearchParams();
  const conclusionId = Number(searchParams.get('id'));

  const [pagerPage, setPagerPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});

  const { data, refetch, isLoading } = useGetAdditionalConditionListQuery({});

  const [deleteTemplate] = useDeleteAdditionalConditionListMutation();
  const [createTaskTemplate] = useCreateAdditionalConditionMutation();

  const selectedTemplates = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const dataId = useMemo(() => {
    return {
      id: selectedTemplates[0],
    };
  }, [selectedTemplates]);

  const rows = useMemo(() => {
    return data || [];
  }, [data]);

  const cols = useMemo(() => {
    return [
      {
        key: 'name',
        name: 'Наименование',
      },
    ];
  }, []);

  const handleDelete = useCallback(async () => {
    await deleteTemplate(selectedTemplates[0]);
    setSelected({});
    refetch();
  }, [selectedTemplates]);

  const handleCreate = useCallback(async () => {
    await createTaskTemplate({ conclusionId, dataId });
    setSelected({});
    onClose();
  }, [conclusionId, dataId, selectedTemplates, selected]);

  const handleCancell = useCallback(() => {
    onClose();
    setSelected({});
    refetch();
  }, [selectedTemplates]);

  const bar = useMemo(() => {
    return (
      <Stack
        direction="row"
        justifyContent="flex-end"
        spacing={1.5}
        style={{ width: '100%' }}
      >
        <Button
          variant="contained"
          startIcon={<PlaylistAdd />}
          onClick={handleCreate}
          disabled={selectedTemplates.length !== 1}
        >
          Добавить
        </Button>
        <Button
          variant="outlined"
          startIcon={<Close />}
          onClick={handleCancell}
        >
          Отмена
        </Button>
      </Stack>
    );
  }, [handleCreate]);

  return (
    <Popup
      open={Boolean(open)}
      onClose={onClose}
      title={'Добавление по набору'}
      bar={bar}
      width={840}
      height={'auto'}
    >
      <div className={styles.popupContent}>
        <div className={styles.title}>Доступные наборы</div>
        <Stack
          className={styles.toolBar}
          direction="row"
          spacing={1.5}
          justifyContent="space-between"
          sx={{ mb: 3 }}
        >
          <Stack direction="row" spacing={1.5}>
            <RoundButton icon={<Refresh />} onClick={() => refetch} />
          </Stack>
          <Stack direction="row" spacing={1.5}>
            <Button
              variant="outlined"
              startIcon={<DeleteOutline />}
              onClick={handleDelete}
              disabled={selectedTemplates.length !== 1}
            >
              Удалить
            </Button>
          </Stack>
        </Stack>
        <DataTable
          formKey={'available_kits'}
          rows={rows}
          cols={cols}
          onRowSelect={setSelected}
          loading={isLoading}
          showFilters={filters}
        />
      </div>
    </Popup>
  );
};

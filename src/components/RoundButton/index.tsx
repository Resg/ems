import React from 'react';
import cn from 'classnames';

import { IconButton } from '@mui/material';

import styles from './styles.module.scss';

interface RoundButtonProps {
  icon: React.ReactNode;
  disabled?: boolean;
  onClick?: (e: React.MouseEvent) => void;
}

export const RoundButton = React.forwardRef<
  HTMLButtonElement,
  RoundButtonProps
>((props, ref) => {
  const { icon, onClick, disabled } = props;

  return (
    <IconButton
      ref={ref}
      className={cn(styles.box, { [styles.box_disabled]: disabled })}
      onClick={onClick}
      disabled={disabled}
    >
      {icon}
    </IconButton>
  );
});

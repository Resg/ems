import React, { useCallback, useState } from 'react';
import { isEmpty } from 'lodash';
import { SnackbarKey, useSnackbar } from 'notistack';

import { Cancel, Close, InfoOutlined } from '@mui/icons-material';
import { Dialog, IconButton } from '@mui/material';

import { ErrorData } from '../../store/utils';
import Card from '../Card';

import styles from './styles.module.scss';

interface SnackbarCloseButtonProps {
  snackbarKey?: SnackbarKey;
  errorData: ErrorData;
}

export const SnackbarCloseButton: React.FC<SnackbarCloseButtonProps> = ({
  snackbarKey,
  errorData: { message, ...errorData },
}) => {
  const [open, setOpen] = useState(false);
  const { closeSnackbar } = useSnackbar();

  const handleOpen = useCallback(() => {
    setOpen(true);
  }, []);

  const handleClose = useCallback(() => {
    setOpen(false);
  }, []);

  return (
    <div>
      {!isEmpty(errorData) && (
        <>
          <IconButton onClick={handleOpen}>
            <InfoOutlined sx={{ color: 'white' }} />
          </IconButton>
          <Dialog open={open}>
            <Card className={styles.card}>
              <div className={styles.title}>
                <div>Информация об ошибке</div>
                <IconButton
                  className={styles['close-button']}
                  onClick={handleClose}
                >
                  <Close />
                </IconButton>
              </div>
              <p>{message}</p>
              <ul>
                <li>Login: {errorData.login}</li>
                <li>URL: {errorData.location}</li>
                <li>URL запроса: {errorData.url}</li>
                <li>Статус: {errorData.status}</li>
                <li>Тело запроса: {errorData.body}</li>
                <li>Ответ: {errorData.response}</li>
              </ul>
            </Card>
          </Dialog>
        </>
      )}
      <IconButton onClick={() => closeSnackbar(snackbarKey)}>
        <Cancel sx={{ color: 'white' }} />
      </IconButton>
    </div>
  );
};

import React, { useCallback, useMemo, useState } from 'react';

import {
  AddCircleOutline,
  Close,
  DeleteOutline,
  Refresh,
  Send,
} from '@mui/icons-material';
import { Button, Stack } from '@mui/material';

import { Popup } from '../../components/Popup';
import { RoundButton } from '../../components/RoundButton';
import { useCreateTaskRouteMutation } from '../../services/api/task';
import {
  useDeleteTaskTemplateMutation,
  useGetTaskTemplatesQuery,
  useGetUserQuery,
} from '../../services/api/user';
import { TaskRouteData } from '../../types/tasks';
import { DataTable } from '../CustomDataGrid';

import styles from './styles.module.scss';

interface TablePageProps {
  objectTypeCode: string;
  open: boolean;
  onClose: () => void;
  formName: string;
  id?: number;
}

export const TaskTemplateForm: React.FC<TablePageProps> = ({
  objectTypeCode,
  open,
  onClose,
  formName,
  id,
}) => {
  const [pagerPage, setPagerPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});

  const selectedTemplates = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const {
    data: taskTemplates,
    refetch,
    isLoading,
  } = useGetTaskTemplatesQuery({
    objectTypeCode,
    page: pagerPage,
    size: perPage,
    useCount: true,
  });
  const [deleteTemplate] = useDeleteTaskTemplateMutation();
  const [createTask] = useCreateTaskRouteMutation();
  const { data: userInfo } = useGetUserQuery({});

  const objPost = useMemo(() => {
    if (selectedTemplates) {
      return taskTemplates?.data?.find((obj: { id: number }) =>
        selectedTemplates?.includes(obj.id)
      );
    }
  }, [selectedTemplates, taskTemplates]);

  const parseTemplate = useMemo(() => {
    if (objPost) {
      return JSON.parse(objPost.template);
    }
  }, [objPost]);

  const rows = useMemo(() => {
    return taskTemplates?.data || [];
  }, [taskTemplates]);

  const cols = useMemo(() => {
    return [
      {
        key: 'name',
        name: 'Наименование',
      },
    ];
  }, []);

  const handleCancell = useCallback(() => {
    onClose();
    setSelected({});
    refetch();
  }, [selectedTemplates]);

  const handleDelete = useCallback(async () => {
    await deleteTemplate(selectedTemplates[0]);
    refetch();
  }, [selectedTemplates]);

  const sendForm = useCallback(
    async (data: TaskRouteData[] | null) => {
      await createTask({ taskData: data });
      onClose();
      setSelected({});
    },
    [selectedTemplates]
  );

  const handleSubmit = (saveMode: boolean) => {
    if (parseTemplate) {
      const data = parseTemplate.map((obj: any) => ({
        temporaryId: obj.temporaryId,
        typeId: obj.typeId,
        title: obj.title,
        comments: obj.comments,
        parentId: obj.parentId,
        temporaryParentId: obj.temporaryParentId,
        previousId: obj.previousId,
        temporaryPreviousId: obj.temporaryPreviousId,
        objectTypeCode: obj.objectTypeCode,
        objectId: id,
        temporaryОbjectId: obj.temporaryОbjectId,
        term: obj.term,
        planDate: obj.planDate,
        performerId: obj.performerId,
        authorId: obj.authorId,
        isControl: obj.isControl,
        sendForExecution: false,
      }));

      if (saveMode) {
        data[0].sendForExecution = true;
      }

      sendForm(data);
    } else {
      sendForm(null);
    }
  };

  const bar = useMemo(() => {
    return (
      <Stack
        direction="row"
        justifyContent="flex-end"
        spacing={1.5}
        style={{ width: '100%' }}
      >
        <Button
          variant="contained"
          startIcon={<Send />}
          onClick={() => handleSubmit(true)}
          disabled={selectedTemplates.length !== 1}
        >
          Создать и отправить на исполнение
        </Button>

        <Button
          variant="outlined"
          startIcon={<AddCircleOutline />}
          onClick={() => handleSubmit(false)}
          disabled={selectedTemplates.length !== 1}
        >
          Создать
        </Button>

        <Button
          variant="outlined"
          startIcon={<Close />}
          onClick={handleCancell}
        >
          Отмена
        </Button>
      </Stack>
    );
  }, [handleSubmit]);

  return (
    <Popup
      open={Boolean(open)}
      onClose={onClose}
      title={formName}
      bar={bar}
      width={750}
      height={790}
    >
      <div className={styles.popupContent}>
        <div className={styles.title}>Доступные шаблоны</div>
        <Stack
          className={styles.toolBar}
          direction="row"
          spacing={1.5}
          justifyContent="space-between"
          sx={{ mb: 3 }}
        >
          <Stack direction="row" spacing={1.5}>
            <RoundButton icon={<Refresh />} onClick={() => refetch} />
          </Stack>
          <Stack direction="row" spacing={1.5}>
            <Button
              variant="outlined"
              startIcon={<DeleteOutline />}
              onClick={handleDelete}
              disabled={selectedTemplates.length !== 1}
            >
              Удалить
            </Button>
          </Stack>
        </Stack>
        <DataTable
          formKey={'create_task_template'}
          rows={rows}
          cols={cols}
          onRowSelect={setSelected}
          loading={isLoading}
          pagerProps={{
            page: pagerPage,
            setPage: setPagerPage,
            perPage: perPage,
            setPerPage: setPerPage,
            total: taskTemplates?.totalCount || 0,
          }}
        />
      </div>
    </Popup>
  );
};

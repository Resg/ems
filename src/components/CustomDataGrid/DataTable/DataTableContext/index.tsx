import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useState,
} from 'react';
import { isEqual } from 'lodash';

import { DefaultRow } from '../../types';

interface DataTableContextState {
  selectedMap: Record<number, boolean>;
  onSelectRow: (event: React.MouseEvent, rowId: number) => void;
  onRowDoubleClick?: (row: any) => void;
  selectFieldName: string;
  filters?: Record<string, string>;
}

export const DataTableContext = createContext<DataTableContextState>({
  selectedMap: {},
  onSelectRow: () => {},
  onRowDoubleClick: () => {},
  selectFieldName: 'id',
});

export const useDataTableContext = () => useContext(DataTableContext);

export interface DataTableContextProviderProps<
  Row extends DefaultRow = DefaultRow
> {
  children: React.ReactNode;
  rows: Row[];
  onRowSelect?: (map: Record<string | number, boolean>) => void;
  onRowDoubleClick?: (row: Row) => void;
  initialSelected?: Record<number, boolean>;
  selectFieldName?: string;
  filters?: Record<string, string>;
}

export function DataTableContextProvider<Row extends DefaultRow = DefaultRow>({
  children,
  rows,
  onRowSelect,
  onRowDoubleClick,
  initialSelected,
  selectFieldName = 'id',
  filters,
}: DataTableContextProviderProps<Row>) {
  const [selectedMap, setSelectedMap] = useState<Record<number, boolean>>(
    initialSelected || {}
  );
  const [lastSelectedRow, setLastSelectedRow] = useState<string | number>();

  useEffect(() => {
    if (!initialSelected) {
      setSelectedMap({});
      onRowSelect?.({});
    }
  }, [rows]);

  useEffect(() => {
    setSelectedMap((prev) =>
      isEqual(initialSelected, prev) ? prev : initialSelected || {}
    );
    setLastSelectedRow(undefined);
  }, [initialSelected]);

  const handleSelectRow = useCallback(
    (event: React.MouseEvent, rowId: number) => {
      const value = !selectedMap[rowId];
      const map: Record<string | number, any> = { ...selectedMap };
      if (!event.shiftKey || !lastSelectedRow) {
        map[rowId] = value;
      } else {
        const selectedRowIndex = rows.findIndex((row) => row.id === rowId);
        const lastRowIndex = rows.findIndex(
          (row) => row.id === lastSelectedRow
        );
        const diff = Math.abs(selectedRowIndex - lastRowIndex);
        const min = Math.min(selectedRowIndex, lastRowIndex);

        for (let i = 0; i < diff + 1; i++) {
          map[rows[min + i].id] = true;
        }
      }
      setSelectedMap(map);
      onRowSelect?.(map);
      if (value) {
        setLastSelectedRow(rowId);
      } else if (rowId === lastSelectedRow) {
        setLastSelectedRow(undefined);
      }
    },
    [lastSelectedRow, onRowSelect, rows, selectedMap]
  );

  return (
    <DataTableContext.Provider
      value={{
        selectedMap,
        onSelectRow: handleSelectRow,
        onRowDoubleClick,
        selectFieldName,
        filters,
      }}
    >
      {children}
    </DataTableContext.Provider>
  );
}

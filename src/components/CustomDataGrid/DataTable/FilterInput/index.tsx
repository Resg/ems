import React from 'react';

import Input from '../../../Input';
import { useDataTableContext } from '../DataTableContext';

import styles from './styles.module.scss';

export interface FilterInputProps {
  name: string;
  setFilters: (filters: Record<string, string>) => void;
}

export const FilterInput: React.FC<FilterInputProps> = ({
  name,
  setFilters,
}) => {
  const { filters } = useDataTableContext();

  return (
    <Input
      name={name}
      value={filters?.[name]}
      onClick={(e) => e.stopPropagation()}
      className={styles.input}
      onChange={(e) =>
        setFilters({
          ...filters,
          [name]: e.target.value,
        })
      }
    />
  );
};

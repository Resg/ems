import React, {
  CSSProperties,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';
import cn from 'classnames';
import DataGrid, {
  Column,
  HeaderRendererProps,
  RowHeightArgs,
  SortColumn,
} from 'react-data-grid';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

import {
  useCreateUserTableSettingsMutation,
  useGetUserTableSettingsQuery,
} from '../../../services/api/userTableSettings';
import { useAppDispatch, useAppSelector } from '../../../store';
import {
  DataType,
  getDataColls,
  getDataGroups,
  getDataSorts,
  getDataTools,
  setDataColls,
  setDataSorts,
  setDataTools,
} from '../../../store/userTableSettings';
import { Sort } from '../../../types/userTableSettings';
import { useCtrlCheck } from '../../../utils/hooks/useCtrlCheck';
import { useWithDebounce } from '../../../utils/hooks/useWithDebounce';
import { SettingsTypes } from '../../ToolsPanel';
import {
  DraggableHeaderRenderer,
  DraggableHeaderRendererProps,
} from '../DraggableHeaderRenderer';
import Pager, { PagerProps } from '../Pager';
import { DefaultRow } from '../types';
import { sortTreeRows } from '../utils';

import { CreateSelectColumn } from './Columns/SelectColumn';
import { DataTableContextProvider } from './DataTableContext';
import { FilterInput } from './FilterInput';
import { loadColumns, loadRows } from './Loading';
import { RowRenderer } from './Row';

import 'react-data-grid/lib/styles.css';
import rowStyles from '../TreeDataGrid/Row/styles.module.scss';
import styles from './styles.module.scss';

export interface DataTableProps<Row> {
  cols: Column<Row>[];
  rows: Row[];
  className?: string;
  onRowDoubleClick?: (row: Row) => void;
  onRowSelect?: (map: Record<number, boolean>) => void;
  pagerProps?: PagerProps;
  reverseCols?: boolean;
  draggable?: boolean;
  name?: string;
  initialSelected?: Record<number, boolean>;
  style?: CSSProperties;
  height?: 'auto' | 'fullHeight' | 'perPage' | 'standard' | string;
  selectFieldName?: string;
  selectColumnProps?: Partial<Column<any>>;
  loading?: boolean;
  noSelect?: boolean;
  rowHeight?: number | ((args: RowHeightArgs<Row>) => number);
  enableVirtualization?: boolean;
  setSort?: (value: SortColumn[]) => void;
  isNeedHTML5Backend?: boolean;
  isNeedSort?: boolean;
  formKey: string;
  children?: React.ReactNode;
  showFilters?: boolean;
}

export function DataTable<Row extends DefaultRow = DefaultRow>({
  rows,
  cols,
  className,
  onRowDoubleClick,
  onRowSelect,
  pagerProps,
  reverseCols,
  draggable = true,
  initialSelected,
  style = {},
  height = 'standard',
  selectFieldName,
  selectColumnProps,
  loading,
  noSelect,
  rowHeight = 40,
  enableVirtualization = true,
  isNeedHTML5Backend = true,
  setSort,
  formKey,
  children,
  showFilters,
}: DataTableProps<Row>) {
  const [filters, setFilters] = useState<Record<string, string>>({});
  const filterKeys = Object.keys(filters);
  const headerHeight = showFilters ? 79 : 40;
  const filteredRows = useMemo(() => {
    return showFilters
      ? rows.filter((r) => {
          let res = true;

          filterKeys.forEach((k) => {
            if (!String(r[k]).includes(filters?.[k])) res = false;
          });

          return res;
        })
      : rows;
  }, [showFilters, rows, filters]);

  useEffect(() => {
    if (!showFilters) setFilters({});
  }, [showFilters]);

  const [createUserTableSettings] = useCreateUserTableSettingsMutation();

  const {
    data: userTableSettingsData,
    isLoading,
    error: userTableSettingsError,
  } = useGetUserTableSettingsQuery({ formKey });

  const dispatch = useAppDispatch();
  const dataTools = useAppSelector((state) => getDataTools(state, formKey));
  const dataColls = useAppSelector((state) => getDataColls(state, formKey));
  const dataGroups = useAppSelector((state) => getDataGroups(state, formKey));
  const dataSorts = useAppSelector((state) => getDataSorts(state, formKey));

  const SelectColumn = useMemo(() => CreateSelectColumn(selectColumnProps), []);
  const prepareCols = noSelect ? cols : [SelectColumn, ...cols];

  const [columns, setColumns] = useState<Column<Row>[] | DataType[] | null>(
    null
  );

  const [isNeedSort, setIsNeedSort] = useState<boolean>(false);
  const sortedRows = useMemo(() => {
    if (dataSorts && isNeedSort) return sortTreeRows(filteredRows, dataSorts);
    else return filteredRows;
  }, [sortTreeRows, rows, dataSorts, isNeedSort, filteredRows]);

  useEffect(() => dataSorts && setSort && setSort(dataSorts), [dataSorts]);

  useEffect(() => {
    if (rows.length > 1 && !setSort) {
      setIsNeedSort(true);
    }
  }, [rows]);

  useEffect(() => {
    if (filteredRows.length > 1 && !setSort) {
      setIsNeedSort(true);
    }
  }, [filteredRows]);

  useEffect(() => {
    if (dataColls) {
      const sortDataCols = [...dataColls].sort(
        (a, b) => a.ordinalNumber - b.ordinalNumber
      );
      setColumns(noSelect ? dataColls : [SelectColumn, ...sortDataCols]);
    }
  }, [dataColls]);

  const draggableColumns: (Column<Row> | DataType)[] | null = useMemo(() => {
    if (!dataColls) return null;

    if (!draggable) {
      return (
        columns?.map((column) => {
          return {
            ...column,
            headerCellClass: styles.header,
            cellClass: cn(styles.cell, { [styles.cell_noSelect]: noSelect }),
          };
        }) || null
      );
    }
    const headerRenderer = (props: HeaderRendererProps<Row>) => {
      return (
        <>
          <DraggableHeaderRenderer
            {...props}
            onColumnReorder={handleColumnsReorder}
          />
          {showFilters && props.column.key !== 'select' && (
            <FilterInput name={props.column.key} setFilters={setFilters} />
          )}
        </>
      );
    };

    const handleColumnsReorder: DraggableHeaderRendererProps<Row>['onColumnReorder'] =
      (sourceKey, targetKey) => {
        if (!columns) return;
        const sourceIndex = columns.findIndex((col) => col.key === sourceKey);
        const targetIndex = columns.findIndex((col) => col.key === targetKey);
        const reorderedColumns = [...columns];

        reorderedColumns.splice(
          targetIndex,
          0,
          reorderedColumns.splice(sourceIndex, 1)[0]
        );

        if (dataColls) {
          const columnSettings = dataColls.map((col: any) => {
            const result = {
              ...col,
              ordinalNumber: reorderedColumns.findIndex(
                (el) => el.name === col.name
              ),
              name: col.name,
              width: col.width,
              isVisible: !col.hidden,
            };
            return result;
          });

          createUserTableSettings({
            formKey,
            data: {
              columnSettings,
              sort: dataSorts,
              tools: dataTools,
            },
          });
        }

        setColumns(reorderedColumns);
      };

    const result = columns
      ?.filter((el) => !el?.hidden)
      .map((column) => {
        return {
          ...column,
          headerRenderer,
          headerCellClass: styles.header,
          cellClass: cn(styles.cell, { [styles.cell_noSelect]: noSelect }),
        };
      });
    return result || null;
  }, [columns, draggable, dataSorts, dataTools, filteredRows]);
  const tableStyles = useMemo(() => {
    const styles: Record<string, any> = { ...style };
    if (height === 'fullHeight') {
      styles['--table-height'] = '100%';
    } else if (height === 'auto') {
      styles['--table-height'] = `${
        (filteredRows.length || 1) * 40 + 3 + headerHeight
      }px`;
    } else if (height === 'perPage') {
      styles['--table-height'] = pagerProps?.perPage
        ? (pagerProps.perPage + 1) * 40 + 2 + 'px'
        : '100%';
    } else if (height === 'standard') {
      styles['--table-height'] = `${402 + headerHeight}px`;
    } else {
      styles['--table-height'] = height;
    }

    if (columns && columns.length < 10) {
      styles['--last-child-width'] = '100vw';
      styles['--last-child-maxwidth'] = '100%';
    }
    return styles as CSSProperties;
  }, [
    style,
    height,
    pagerProps?.perPage,
    rows.length,
    columns,
    filteredRows.length,
  ]);

  const handleColumnResize = useCallback(
    (idx: number, width: number) => {
      if (!dataColls) return;

      let index = 0;
      const columnSettings = dataColls.map((col: any) => {
        const result = {
          ...col,
          ordinalNumber: draggableColumns?.findIndex(
            (el) => el.name === col.name
          ),
          name: col.name,
          width: draggableColumns?.[idx].name === col.name ? width : col.width,
          isVisible: !col.hidden,
        };
        if (!col.hidden) index++;
        return result;
      });

      createUserTableSettings({
        formKey,
        data: {
          columnSettings,
          sort: dataSorts,
          tools: dataTools,
        },
      });
      dispatch(setDataColls({ formKey, data: columnSettings }));
    },
    [draggableColumns, dataSorts, dataTools]
  );

  const handleColumnResizeDebounced = useWithDebounce(handleColumnResize, 300);

  useEffect(() => {
    if (userTableSettingsData && userTableSettingsData.sort) {
      dispatch(setDataSorts({ formKey, data: userTableSettingsData.sort }));
    }

    if (userTableSettingsData?.columnSettings?.length) {
      dispatch(
        setDataColls({
          formKey: formKey,
          data: [...userTableSettingsData.columnSettings]?.sort(
            (a, b) => a.ordinalNumber - b.ordinalNumber
          ),
        })
      );
    } else if (!isLoading) {
      dispatch(setDataColls({ formKey, data: cols }));
    }

    if (userTableSettingsData?.tools?.length) {
      dispatch(
        setDataTools({
          formKey: formKey,
          data: userTableSettingsData?.tools,
        })
      );
    }
  }, [userTableSettingsData]);

  const ctrlPress = useCtrlCheck();
  const gridSortHandle = useCallback(
    (initialColls: Sort[]) => {
      let resultSort = initialColls;
      if (ctrlPress && dataSorts) {
        let newSortCheck: Sort[] = [...dataSorts];
        initialColls?.forEach((el) => {
          const index = newSortCheck?.findIndex(
            (item) => item?.columnKey === el?.columnKey
          );

          if (initialColls.length < newSortCheck.length) {
            newSortCheck = newSortCheck.filter(
              (item) => item?.columnKey === el?.columnKey
            );
          }
          index === -1 ? newSortCheck.push(el) : (newSortCheck[index] = el);
        });
        resultSort = newSortCheck;
      }
      if (dataColls) {
        createUserTableSettings({
          formKey,
          data: {
            columnSettings: dataColls as any,
            sort: resultSort,
            group: dataGroups,
            tools: dataTools,
          },
        });
      }
      dispatch(setDataSorts({ formKey, data: resultSort }));
    },
    [dataColls, dataGroups, dataTools, dataSorts]
  );

  const showSkeleton = loading || isLoading || !draggableColumns;
  const grid = (
    <DataTableContextProvider
      rows={sortedRows}
      onRowDoubleClick={onRowDoubleClick}
      onRowSelect={onRowSelect}
      initialSelected={initialSelected}
      selectFieldName={selectFieldName}
      filters={filters}
    >
      <>
        {React.isValidElement(children)
          ? React.cloneElement(children as React.ReactElement<any>, {
              formKey,
              cols,
              settings: [SettingsTypes.COLS, SettingsTypes.TOOLS],
            })
          : null}
        <DataGrid
          rows={showSkeleton ? (loadRows as Row[]) : sortedRows}
          rowClass={() => rowStyles.row}
          columns={
            showSkeleton
              ? (loadColumns as Column<Row>[])
              : (draggableColumns as Column<Row>[])
          }
          style={tableStyles}
          renderers={{ rowRenderer: RowRenderer }}
          defaultColumnOptions={{ resizable: true, sortable: true }}
          className={cn(styles.table, className)}
          rowHeight={rowHeight}
          headerRowHeight={headerHeight}
          enableVirtualization={enableVirtualization}
          sortColumns={dataSorts}
          onSortColumnsChange={isNeedSort || setSort ? gridSortHandle : null}
          onColumnResize={handleColumnResizeDebounced}
        />
      </>
      {pagerProps && <Pager {...pagerProps} loading={loading} />}
    </DataTableContextProvider>
  );
  return (
    <>
      {isNeedHTML5Backend ? (
        <DndProvider backend={HTML5Backend}>{grid}</DndProvider>
      ) : (
        grid
      )}
    </>
  );
}

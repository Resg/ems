import React, { useCallback } from 'react';
import { get } from 'lodash';
import { Column } from 'react-data-grid';

import { Checkbox } from '@mui/material';

import { useDataTableContext } from '../../DataTableContext';

import styles from './styles.module.scss';

const Select: React.FC<{ row: any }> = ({ row }) => {
  const { onSelectRow, selectedMap, selectFieldName } = useDataTableContext();

  const id = get(row, selectFieldName);

  const handleChange = useCallback(
    (event: React.MouseEvent) => {
      onSelectRow(event, id);
    },
    [onSelectRow, id]
  );

  return (
    <div className={styles.checkbox}>
      <Checkbox
        checked={Boolean(selectedMap[id])}
        onClick={handleChange}
        onDoubleClick={(e) => e.stopPropagation()}
      />
    </div>
  );
};

export const CreateSelectColumn = (
  props: Partial<Column<any>> = {}
): Column<any> => ({
  key: 'select',
  name: '',
  width: 44,
  minWidth: 44,
  headerCellClass: styles.cell,
  cellClass: styles.cell,
  sortable: false,
  resizable: false,
  formatter({ row }) {
    return <Select row={row} />;
  },
  ...props,
});

import React from 'react';
import { Row, RowRendererProps } from 'react-data-grid';

import { DefaultRow } from '../../types';
import { useDataTableContext } from '../DataTableContext';

export function RowRenderer<R extends DefaultRow = DefaultRow>(
  key: string | number,
  props: RowRendererProps<R>
) {
  const { selectedMap, onRowDoubleClick } = useDataTableContext();

  return (
    <Row
      key={key}
      {...props}
      aria-selected={selectedMap[props.row.id]}
      onDoubleClick={() => onRowDoubleClick?.(props.row)}
    />
  );
}

import { Skeleton } from '@mui/material';

import styles from './styles.module.scss';

export const loadColumns = [
  {
    key: 'loadSelect',
    name: '',
    width: 40,
    minWidth: 40,
    cellClass: styles.selectCell,
    headerCellClass: styles.selectCell,
    sortable: false,
    resizable: false,
    formatter({}) {
      return <Skeleton />;
    },
    headerRenderer({}) {
      return <Skeleton />;
    },
  },
  {
    key: 'load',
    name: '',
    cellClass: styles.cell,
    headerCellClass: styles.cell,
    sortable: false,
    resizable: false,
    formatter({}) {
      return <Skeleton />;
    },
    headerRenderer({}) {
      return <Skeleton />;
    },
  },
];

export const loadRows = [
  { id: 1, groupLevel: 1 },
  { id: 2, groupLevel: 1 },
  { id: 3, groupLevel: 1 },
  { id: 4, groupLevel: 1 },
  { id: 5, groupLevel: 1 },
];

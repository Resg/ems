import React, {
  CSSProperties,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';
import cn from 'classnames';
import DataGridBase, {
  Column,
  FormatterProps,
  HeaderRendererProps,
} from 'react-data-grid';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

import {
  useCreateUserTableSettingsMutation,
  useGetUserTableSettingsQuery,
} from '../../../services/api/userTableSettings';
import { useAppDispatch, useAppSelector } from '../../../store';
import {
  DataType,
  getDataColls,
  getDataGroups,
  getDataSorts,
  getDataTools,
  getUserTableSettings,
  setDataColls,
  setDataGroups,
  setDataSorts,
  setDataTools,
  setUserTableSettings,
} from '../../../store/userTableSettings';
import { Sort, UserTableSettings } from '../../../types/userTableSettings';
import { useCtrlCheck } from '../../../utils/hooks/useCtrlCheck';
import { useWithDebounce } from '../../../utils/hooks/useWithDebounce';
import { SettingsTypes } from '../../ToolsPanel';
import {
  DraggableHeaderRenderer,
  DraggableHeaderRendererProps,
} from '../DraggableHeaderRenderer';
import { PagerProps } from '../Pager';
import { DefaultColumn, DefaultRow, TreeRow } from '../types';
import { createTreeByGroups, sortTreeRows, toggleSubRow } from '../utils';

import { CreateGroupColumn } from './Columns/GroupColumn';
import { CreateSelectColumn } from './Columns/SelectColumn';
import { DataGridContextProvider } from './DataGridContext';
import { FilterInput } from './FilterInput';
import { loadColumns, loadRows } from './Loading';
import { RowRenderer } from './Row';

import 'react-data-grid/lib/styles.css';
import styles from './styles.module.scss';

export interface DataGridProps<Row extends DefaultRow = DefaultRow> {
  rows: Row[];
  cols: DefaultColumn[];
  groups: string[];
  onRowDoubleClick?: (row: TreeRow<Row>) => void;
  onRowSelect?: (map: Record<string | number, boolean>) => void;
  className?: string;
  expanded?: boolean;
  singleSelect?: boolean;
  childrenNoSelect?: boolean;
  createTree?: (rows: Row[], groups: string[]) => TreeRow<Row>[];
  createColumn?: (
    handleRowExpand: (rowId: string) => void,
    groups: string[],
    settings: UserTableSettings
  ) => Column<TreeRow<Row>>;
  initialSelected?: Record<string, boolean>;
  pagerProps?: PagerProps;
  onSelectAll?: (
    rows: TreeRow<Row>[],
    map: Record<string | number, boolean>,
    value: boolean
  ) => Record<string | number, boolean>;
  onCreateSelectColumn?: () => Column<any>;
  loading?: boolean;
  style?: CSSProperties;
  height?: 'auto' | 'fullHeight' | string;
  defaultSelected?: boolean;
  checkboxLevels?: number[];
  withSelect?: boolean;
  helperText?: string;
  isNeedHTML5Backend?: boolean;
  isParentFontBold?: boolean;
  formKey: string;
  children?: React.ReactNode;
  showFilters?: boolean;
  additionalColumnProps?: Record<string, Partial<Column<any>>>;
}

export function TreeDataGrid<Row extends DefaultRow>({
  cols,
  rows,
  groups,
  onRowDoubleClick,
  onRowSelect,
  className,
  expanded,
  singleSelect = false,
  childrenNoSelect = false,
  withSelect = true,
  createTree = (rows: Row[], groups: string[]) =>
    createTreeByGroups(rows, groups, expanded),
  createColumn = (
    handleRowExpand,
    groups: string[],
    settings: UserTableSettings
  ) => CreateGroupColumn(groups, handleRowExpand, cols.length + 1, settings),
  initialSelected,
  onSelectAll,
  onCreateSelectColumn = CreateSelectColumn,
  loading,
  style = {},
  height = 'fullHeight',
  defaultSelected,
  checkboxLevels,
  helperText,
  isNeedHTML5Backend = true,
  isParentFontBold = false,
  formKey,
  children,
  showFilters,
  additionalColumnProps,
}: DataGridProps<Row>) {
  const [filters, setFilters] = useState<Record<string, string>>({});
  const [expandedGroupsIds, setExpandedGroupsIds] = useState(new Set());
  const filterKeys = Object.keys(filters);
  const headerHeight = showFilters ? 79 : 40;
  useEffect(() => {
    if (!showFilters) setFilters({});
  }, [showFilters]);

  const [createUserTableSettings] = useCreateUserTableSettingsMutation();
  const { data: userTableSettingsData, isLoading } =
    useGetUserTableSettingsQuery({
      formKey,
    });

  const dispatch = useAppDispatch();
  const dataTools = useAppSelector((state) => getDataTools(state, formKey));
  const dataColls = useAppSelector((state) => getDataColls(state, formKey));
  const dataGroups = useAppSelector((state) => getDataGroups(state, formKey));
  const dataSorts = useAppSelector((state) => getDataSorts(state, formKey));
  const userTableSettings = useAppSelector((state) =>
    getUserTableSettings(state, formKey)
  );

  const showGroupedQuantity = useMemo(
    () => userTableSettings?.showGroupedQuantity,
    [userTableSettings]
  );

  useEffect(() => {
    if (!dataGroups) {
      dispatch(setDataGroups({ formKey: formKey, data: groups }));
    }
  }, [cols, groups]);
  const [treeRows, setTreeRows] = useState(createTree(rows, dataGroups || []));

  const sortedRows = useMemo(() => {
    if (dataSorts) return sortTreeRows(rows, dataSorts);
    else return rows;
  }, [sortTreeRows, rows, dataSorts]);

  const [openGroups, setOpenGroups] = useState<any>([]);

  const handleRowExpand = useCallback(
    (rowId: string) => {
      if (openGroups.includes(rowId))
        setOpenGroups([...openGroups.filter((el: any) => el !== rowId)]);
      else setOpenGroups([...openGroups, rowId]);
      setTreeRows(toggleSubRow(treeRows, rowId));
    },
    [treeRows]
  );

  const GroupColumn = useMemo(
    () =>
      createColumn(handleRowExpand, (dataGroups as string[]) || cols, {
        showGroupedQuantity,
      }),
    [dataColls?.length, dataGroups, handleRowExpand, showGroupedQuantity]
  );

  const SelectColumn = useMemo(() => onCreateSelectColumn(), []);

  const [columns, setColumns] = useState<
    Column<TreeRow<Row>>[] | DataType[] | null
  >(null);

  useEffect(() => {
    if (!columns) return;
    setColumns(
      withSelect
        ? [SelectColumn, GroupColumn, ...columns.slice(2)]
        : [GroupColumn, ...columns.slice(1)]
    );
  }, [GroupColumn, SelectColumn]);

  useEffect(() => {
    if (dataColls) {
      const sortDataCols = [...dataColls].sort(
        (a, b) => a.ordinalNumber - b.ordinalNumber
      );
      setColumns(
        withSelect
          ? [SelectColumn, GroupColumn, ...sortDataCols]
          : [GroupColumn, ...sortDataCols]
      );
    }
  }, [dataColls]);

  useEffect(() => {
    setTreeRows(createTree(sortedRows, dataGroups || []));
  }, [expanded, dataGroups, sortedRows]);

  const draggableColumns: Column<TreeRow<Row>>[] | null = useMemo(() => {
    if (!dataColls) return null;

    const headerRenderer = (props: HeaderRendererProps<TreeRow<Row>>) => {
      return (
        <>
          <DraggableHeaderRenderer
            {...props}
            onColumnReorder={handleColumnsReorder}
          />
          {showFilters && (
            <FilterInput name={props.column.key} setFilters={setFilters} />
          )}
        </>
      );
    };

    const handleColumnsReorder: DraggableHeaderRendererProps<
      TreeRow<Row>
    >['onColumnReorder'] = (sourceKey, targetKey) => {
      if (!columns) return;

      const sourceIndex = columns.findIndex((col) => col.key === sourceKey);
      const targetIndex = columns.findIndex((col) => col.key === targetKey);
      const reorderedColumns = [...columns];

      reorderedColumns.splice(
        targetIndex,
        0,
        reorderedColumns.splice(sourceIndex, 1)[0]
      );

      if (dataColls) {
        const columnSettings = dataColls.map((col: any) => {
          return {
            ...col,
            ordinalNumber: reorderedColumns.findIndex(
              (el) => el.name === col.name
            ),
            name: col.name,
            width: col.width,
            isVisible: !col.hidden,
          };
        });

        createUserTableSettings({
          formKey,
          data: {
            columnSettings,
            sort: dataSorts,
            group: dataGroups,
            tools: dataTools,
          },
        });
        dispatch(setDataColls({ formKey, data: columnSettings }));
      }

      setColumns(reorderedColumns);
    };

    const filteredColumns = columns?.filter((el) => !el?.hidden);
    const result = filteredColumns?.map((column, index) => {
      if (column.key === 'groupCol') {
        return {
          ...column,
          headerCellClass: styles.groupCol,
          cellClass: styles.groupCell,
        };
      }
      if (column.key === 'select') {
        return {
          ...column,
          headerCellClass: styles.cell,
          cellClass: styles.cell,
        };
      }
      return {
        colSpan: (args: any) => {
          if (index === filteredColumns.length - 1) {
            return cols.length + 1 - filteredColumns.length;
          }
          return undefined;
        },
        ...column,
        headerRenderer,
        headerCellClass: styles.header,
        cellClass: styles.cell,
        ...(additionalColumnProps?.[column.key] || {}),
      };
    });
    return result as Column<TreeRow<Row>>[];
  }, [
    columns,
    showFilters,
    dataSorts,
    dataGroups,
    dataTools,
    dataColls,
    additionalColumnProps,
  ]);

  const gridStyles = useMemo(() => {
    const styles: Record<string, any> = { ...style };
    if (height === 'fullHeight') {
      styles['--grid-height'] = '100%';
    } else if (height === 'auto') {
      styles['--grid-height'] = `min(${
        ((treeRows.length || 1) + 1) * 40 + 2 + headerHeight
      }px)`;
    } else {
      styles['--grid-height'] = height;
    }

    if (columns && columns?.length < 10) {
      styles['--last-child-width'] = '100vw';
    }
    return styles as CSSProperties;
  }, [height, style, treeRows.length, columns]);

  useEffect(() => {
    if (openGroups.length) {
      openGroups.forEach((el: string) => {
        setTreeRows((prev) => toggleSubRow(prev, el));
      });
    }
  }, [sortedRows]);

  const rowClass = useCallback(
    (treeRow: TreeRow<Row>) => {
      return treeRow.children && isParentFontBold
        ? styles['row-weighted']
        : styles['row'];
    },
    [isParentFontBold]
  );

  const handleColumnResize = useCallback(
    (idx: number, width: number) => {
      if (!dataColls) return;
      const columnSettings = dataColls.map((col: any) => {
        return {
          ...col,
          ordinalNumber: draggableColumns?.findIndex(
            (el) => el.name === col.name
          ),
          name: col.name,
          width: draggableColumns?.[idx].name === col.name ? width : col.width,
          isVisible: !col.hidden,
        };
      });

      createUserTableSettings({
        formKey,
        data: {
          columnSettings,
          sort: dataSorts,
          group: dataGroups,
          tools: dataTools,
        },
      });
      dispatch(setDataColls({ formKey, data: columnSettings }));
    },
    [draggableColumns, dataSorts, dataGroups, dataTools]
  );

  const handleColumnResizeDebounced = useWithDebounce(handleColumnResize, 300);

  useEffect(() => {
    if (userTableSettingsData && userTableSettingsData.sort) {
      dispatch(setDataSorts({ formKey, data: userTableSettingsData.sort }));
    }

    if (userTableSettingsData?.columnSettings?.length) {
      dispatch(
        setDataColls({
          formKey: formKey,
          data: [...userTableSettingsData.columnSettings]
            ?.map((col) => {
              const column = cols.find(
                (initialCol) => initialCol.key === col?.key
              ) as DefaultColumn & {
                formatter?: (
                  props: FormatterProps<any, any>
                ) => React.ReactNode;
              };

              const formatter = column?.formatter;

              return {
                ...col,
                formatter,
              };
            })
            .sort((a, b) => a.ordinalNumber - b.ordinalNumber),
        })
      );
    } else if (!isLoading) {
      dispatch(setDataColls({ formKey, data: cols }));
    }

    if (userTableSettingsData?.group?.length) {
      dispatch(
        setDataGroups({ formKey: formKey, data: userTableSettingsData?.group })
      );
    }

    if (userTableSettingsData?.tools?.length) {
      dispatch(
        setDataTools({
          formKey: formKey,
          data: userTableSettingsData?.tools,
        })
      );
    }

    if (userTableSettingsData?.showGroupedQuantity !== undefined) {
      const showGroupedQuantity = userTableSettingsData.showGroupedQuantity;
      dispatch(
        setUserTableSettings({
          formKey: formKey,
          data: {
            showGroupedQuantity,
          },
        })
      );
    }
  }, [userTableSettingsData]);

  const ctrlPress = useCtrlCheck();
  const gridSortHandle = useCallback(
    (initialColls: Sort[]) => {
      let resultSort = initialColls;
      if (ctrlPress && dataSorts) {
        let newSortCheck: Sort[] = [...dataSorts];
        initialColls?.forEach((el) => {
          const index = newSortCheck?.findIndex(
            (item) => item?.columnKey === el?.columnKey
          );

          if (initialColls.length < newSortCheck.length) {
            newSortCheck = newSortCheck.filter(
              (item) => item?.columnKey === el?.columnKey
            );
          }
          index === -1 ? newSortCheck.push(el) : (newSortCheck[index] = el);
        });
        resultSort = newSortCheck;
      }
      if (dataColls) {
        createUserTableSettings({
          formKey,
          data: {
            columnSettings: dataColls as any,
            sort: resultSort,
            group: dataGroups,
            tools: dataTools,
          },
        });
      }
      dispatch(setDataSorts({ formKey, data: resultSort }));
    },
    [dataColls, dataGroups, dataTools, dataSorts]
  );

  const filterRows = useCallback(
    (rows: TreeRow<Row>[]) => {
      if (showFilters) {
        const filter = (r: TreeRow<Row>) => {
          let res = true;

          filterKeys.forEach((k) => {
            if (
              !String(r[k]).toLowerCase().includes(filters?.[k].toLowerCase())
            )
              res = false;
          });

          return res;
        };

        const filtered = [...rows];

        return filtered.filter((r) => {
          if (!r.children) return filter(r);

          if (r.children && !filterRows([...r.children]).length) return false;

          return true;
        });
      } else {
        return rows;
      }
    },
    [showFilters, filterKeys, filters]
  );

  const showSkeleton = loading || !dataGroups || isLoading || !draggableColumns;

  const grid = (
    <DataGridContextProvider
      rows={treeRows}
      onRowDoubleClick={onRowDoubleClick}
      onRowSelect={onRowSelect}
      originalRows={rows}
      singleSelect={singleSelect}
      childrenNoSelect={childrenNoSelect}
      initialSelected={initialSelected}
      onSelectAll={onSelectAll}
      checkboxLevels={checkboxLevels}
      defaultSelected={defaultSelected}
      filters={filters}
    >
      <>
        {React.isValidElement(children)
          ? React.cloneElement(children as React.ReactElement<any>, {
              formKey,
              groups,
              cols,
              settings: [
                SettingsTypes.COLS,
                SettingsTypes.GROUPS,
                SettingsTypes.TOOLS,
              ],
            })
          : null}

        <DataGridBase
          rows={showSkeleton ? loadRows : filterRows(treeRows)}
          columns={
            showSkeleton
              ? (loadColumns as Column<TreeRow<Row>, unknown>[])
              : draggableColumns
          }
          defaultColumnOptions={{ resizable: true, sortable: true }}
          rowClass={rowClass}
          renderers={{ rowRenderer: RowRenderer }}
          style={gridStyles}
          rowHeight={40}
          headerRowHeight={headerHeight}
          expandedGroupIds={expandedGroupsIds}
          onExpandedGroupIdsChange={setExpandedGroupsIds}
          className={cn(styles.grid, className)}
          sortColumns={dataSorts}
          onSortColumnsChange={gridSortHandle || null}
          onColumnResize={handleColumnResizeDebounced}
        />
        <span className={styles.helperText}>{helperText}</span>
      </>
    </DataGridContextProvider>
  );

  return (
    <>
      {isNeedHTML5Backend ? (
        <DndProvider backend={HTML5Backend}>{grid}</DndProvider>
      ) : (
        grid
      )}
    </>
  );
}

import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react';

import { TreeRow } from '../../types';
import { selectAll, selectTreeRow } from '../../utils';

interface DataGridContextState {
  selectedMap: Record<string | number, boolean>;
  onSelectRow: (event: React.MouseEvent, rowId: string | number) => void;
  onRowDoubleClick?: (row: TreeRow<any>) => void;
  onSelectAll: (value: boolean) => void;
  selectedAll: boolean;
  checkboxLevels?: number[];
  filters?: Record<string, string>;
}

export const DataGridContext = createContext<DataGridContextState>({
  selectedMap: {},
  onSelectRow: () => {},
  onRowDoubleClick: () => {},
  onSelectAll: () => {},
  selectedAll: false,
});

export const useDataGridContext = () => useContext(DataGridContext);

export interface DataGridContextProviderProps {
  children: React.ReactNode;
  rows: TreeRow[];
  onRowSelect?: (map: Record<string | number, boolean>) => void;
  onRowDoubleClick?: (row: TreeRow<any>) => void;
  originalRows: any;
  initialSelected?: Record<string, boolean>;
  onSelectAll?: (
    rows: TreeRow<any>[],
    map: Record<string | number, boolean>,
    value: boolean
  ) => Record<string | number, boolean>;
  singleSelect?: boolean;
  childrenNoSelect?: boolean;
  defaultSelected?: boolean;
  checkboxLevels?: number[];
  filters?: Record<string, string>;
}

export const DataGridContextProvider: React.FC<
  DataGridContextProviderProps
> = ({
  children,
  rows,
  onRowSelect,
  onRowDoubleClick,
  originalRows,
  initialSelected,
  onSelectAll = selectAll,
  singleSelect,
  childrenNoSelect,
  defaultSelected,
  checkboxLevels,
  filters,
}) => {
  const [selectedMap, setSelectedMap] = useState<
    Record<string | number, boolean>
  >({});
  const [lastSelectedRow, setLastSelectedRow] = useState<string | number>();

  useEffect(() => {
    setSelectedMap(initialSelected || {});
    setLastSelectedRow(undefined);
  }, [originalRows, initialSelected]);

  const handleSelectRow = useCallback(
    (event: React.MouseEvent, rowId: number | string) => {
      const value = !selectedMap[rowId];
      let map: Record<string | number, any> = { ...selectedMap };
      if (!event.shiftKey || !lastSelectedRow) {
        map = selectTreeRow(
          rowId,
          rows,
          selectedMap,
          value,
          onSelectAll,
          singleSelect,
          childrenNoSelect
        );
      } else {
        const selectedRowIndex = rows.findIndex((row) => row.id === rowId);
        const lastRowIndex = rows.findIndex(
          (row) => row.id === lastSelectedRow
        );
        const diff = Math.abs(selectedRowIndex - lastRowIndex);
        const min = Math.min(selectedRowIndex, lastRowIndex);

        for (let i = 0; i < diff + 1; i++) {
          map = {
            ...map,
            ...selectTreeRow(
              rows[min + i].id,
              rows,
              map,
              true,
              onSelectAll,
              singleSelect
            ),
          };
        }
      }

      setSelectedMap({ ...map });
      onRowSelect?.({ ...map });
      if (value) {
        setLastSelectedRow(rowId);
      } else if (rowId === lastSelectedRow) {
        setLastSelectedRow(undefined);
      }
    },
    [lastSelectedRow, onRowSelect, rows, selectedMap]
  );

  const handleSelectAll = useCallback(
    (value: boolean) => {
      if (value === true) {
        const map = rows.reduce((acc, curr) => {
          return {
            ...acc,
            ...selectTreeRow(curr.id, rows, acc, value, selectAll),
          };
        }, {} as Record<string | number, any>);
        setSelectedMap(map);
        onRowSelect?.(map);
      } else {
        setSelectedMap({});
        onRowSelect?.({});
      }
    },
    [rows]
  );

  useEffect(() => {
    if (defaultSelected === true) {
      const map = rows.reduce((acc, curr) => {
        return {
          ...acc,
          ...selectTreeRow(curr.id, rows, acc, defaultSelected, selectAll),
        };
      }, {} as Record<string | number, any>);
      setSelectedMap(map);
    }
  }, [rows, defaultSelected]);

  const selectedAll = useMemo(() => {
    return rows.every((row) => selectedMap[row.id]);
  }, [rows, selectedMap]);

  return (
    <DataGridContext.Provider
      value={{
        selectedMap,
        onSelectRow: handleSelectRow,
        onRowDoubleClick,
        onSelectAll: handleSelectAll,
        selectedAll,
        checkboxLevels,
        filters,
      }}
    >
      {children}
    </DataGridContext.Provider>
  );
};

import React from 'react';
import { Row, RowRendererProps } from 'react-data-grid';

import { DefaultRow, TreeRow } from '../../types';
import { useDataGridContext } from '../DataGridContext';

export function RowRenderer<R extends DefaultRow = DefaultRow>(
  key: string | number,
  props: RowRendererProps<TreeRow<R>>
) {
  const { selectedMap, onRowDoubleClick } = useDataGridContext();

  return (
    <Row
      key={key}
      {...props}
      aria-selected={selectedMap[props.row.id]}
      onDoubleClick={() => onRowDoubleClick?.(props.row)}
    />
  );
}

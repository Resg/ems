import React from 'react';
import cn from 'classnames';

import RightArrIcon from '@mui/icons-material/ChevronRight';

import styles from './styles.module.scss';

export interface ExpanderCellProps {
  expanded: boolean;
}

export const ExpanderCell: React.FC<ExpanderCellProps> = ({ expanded }) => {
  return (
    <div>
      <span className={styles.arrWrap}>
        <RightArrIcon
          className={cn({
            [styles.arrIcon]: true,
            [styles.arrIcon_expand]: expanded,
          })}
        />
      </span>
    </div>
  );
};

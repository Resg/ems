import { Column } from 'react-data-grid';

import { UserTableSettings } from '../../../../../types/userTableSettings';
import { TreeRow } from '../../../types';

import { ExpanderCell } from './ExpanderCell';

import styles from './styles.module.scss';

export const CreateGroupColumn = (
  groups: string[],
  onCellExpand: (rowId: string) => void,
  colsSpan: number = 1,
  settings: UserTableSettings = {}
): Column<TreeRow<any>> => ({
  key: 'groupCol',
  name: '',
  width: 0,
  minWidth: 0,
  cellClass: styles.groupCol,
  headerCellClass: styles.groupHeaderCell,
  sortable: false,
  resizable: false,
  formatter: ({ row }) => {
    const hasChildren = row.children !== undefined;

    const lastChildrenArr: TreeRow[] = [];

    const findLastChildren = (row: TreeRow) => {
      if (row.children) {
        row.children.forEach((child: TreeRow) => findLastChildren(child));
      } else {
        lastChildrenArr.push(row);
      }
    };

    if (hasChildren) {
      findLastChildren(row);
    }

    return (
      <div
        className={styles.groupCell}
        onClick={(e) => {
          e.stopPropagation();
          onCellExpand(row.id);
        }}
      >
        {hasChildren && (
          <div
            style={{
              paddingLeft: row.groupLevel ? (row.groupLevel - 1) * 32 : 32,
            }}
          >
            <ExpanderCell expanded={row.isExpanded === true} />
          </div>
        )}

        <div className="rdg-cell-value">
          {!hasChildren && (
            <div
              style={{
                paddingLeft: row.groupLevel ? (row.groupLevel - 1) * 32 : 32,
              }}
            >
              {/* {column.key} */}
            </div>
          )}

          {hasChildren && !row.link && (
            <div className={styles.title}>
              {row[groups[row.groupLevel ? row.groupLevel - 1 : 0]]}
              {settings.showGroupedQuantity &&
                hasChildren &&
                ` [${lastChildrenArr.length}]`}
            </div>
          )}
        </div>
      </div>
    );
  },
  colSpan: (args: any) => {
    if (args.type === 'ROW' && args.row.children) return colsSpan;
    return undefined;
  },
});

export const CreateRightGroupColumn = (
  groups: string[],
  onCellExpand: (rowId: string) => void,
  name: string = ''
): Column<TreeRow<any>> => ({
  key: 'groupCol',
  name,
  // width: 0,
  // minWidth: 0,
  cellClass: styles.groupCol,
  headerCellClass: styles.groupHeaderCell,
  sortable: false,
  resizable: false,
  formatter: ({ row }) => {
    const hasChildren = Boolean(row.children);

    return (
      <div
        className={styles.groupCell}
        onClick={(e) => {
          e.stopPropagation();
          onCellExpand(row.id);
        }}
      >
        {hasChildren && (
          <div
            style={{
              paddingLeft: row.groupLevel ? (row.groupLevel - 1) * 32 : 32,
            }}
          >
            <ExpanderCell expanded={row.isExpanded} />
          </div>
        )}

        <div className="rdg-cell-value">
          {!hasChildren && (
            <div
              className={styles.subtitle}
              style={{
                paddingLeft: (row.groupLevel ? row.groupLevel * 24 : 32) + 'px',
              }}
            >
              {row.title}
            </div>
          )}

          {hasChildren && !row.link && (
            <div className={styles.subtitle}>{row.title}</div>
          )}
        </div>
      </div>
    );
  },
  colSpan: (args: any) => {
    if (args.type === 'ROW') return 1;
    return undefined;
  },
});

export const CreateDocumentGroupColumn = (
  groups: string[],
  onCellExpand: (rowId: string) => void
): Column<TreeRow<any>> => ({
  key: 'docCol',
  name: '',
  width: 0,
  minWidth: 0,
  cellClass: styles.groupCol,
  headerCellClass: styles.groupHeaderCell,
  sortable: false,
  resizable: false,
  formatter: ({ row }) => {
    const hasChildren = Boolean(row.children);
    return (
      <div
        className={styles.groupCell}
        onClick={(e) => {
          e.stopPropagation();
          onCellExpand(row.id);
        }}
      >
        {hasChildren && (
          <div
            style={{
              paddingLeft: row.groupLevel ? (row.groupLevel - 1) * 32 : 32,
            }}
          >
            <ExpanderCell expanded={row.isExpanded} />
          </div>
        )}

        <div className="rdg-cell-value">
          {hasChildren && (
            <div className={styles.subtitle}>
              {row[groups[row.groupLevel - 1]]}
            </div>
          )}
        </div>
      </div>
    );
  },
  colSpan: (args: any) => {
    if (args.type === 'ROW') return 2;
    return undefined;
  },
});

export const CreateClickableGroupColumn = (
  groups: string[],
  onCellExpand: (rowId: string) => void,
  colsSpan: number = 1
): Column<TreeRow<any>> => ({
  key: 'groupCol',
  name: '',
  width: 0,
  minWidth: 0,
  cellClass: styles.groupCol,
  headerCellClass: styles.groupHeaderCell,
  sortable: false,
  resizable: false,
  formatter: ({ row }) => {
    const hasChildren = Boolean(row.children);

    return (
      <div className={styles.groupCell}>
        {hasChildren && (
          <div
            onClick={(e) => {
              e.stopPropagation();
              onCellExpand(row.id);
            }}
            style={{
              paddingLeft: row.groupLevel ? (row.groupLevel - 1) * 32 : 32,
            }}
          >
            <ExpanderCell expanded={row.isExpanded === true} />
          </div>
        )}

        <div className="rdg-cell-value">
          {!hasChildren && (
            <div
              style={{
                paddingLeft: row.groupLevel ? (row.groupLevel - 1) * 32 : 32,
              }}
            />
          )}

          {hasChildren && !row.link && (
            <div className={styles.title}>
              {row[groups[row.groupLevel ? row.groupLevel - 1 : 0]]}
            </div>
          )}
        </div>
      </div>
    );
  },
  colSpan: (args: any) => {
    if (args.type === 'ROW' && args.row.children) return colsSpan;
    return undefined;
  },
});

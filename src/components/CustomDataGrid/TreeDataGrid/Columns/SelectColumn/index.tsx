import React, { useCallback } from 'react';
import { Column } from 'react-data-grid';

import { Checkbox } from '@mui/material';

import { TreeRow } from '../../../types';
import { useDataGridContext } from '../../DataGridContext';

import styles from './styles.module.scss';

const HeaderSelect: React.FC<{}> = ({}) => {
  const { selectedAll, onSelectAll } = useDataGridContext();

  const handleChange = useCallback(
    (event: React.MouseEvent) => {
      onSelectAll(!selectedAll);
    },
    [onSelectAll, selectedAll]
  );

  return (
    <Checkbox
      checked={selectedAll}
      onClick={handleChange}
      onDoubleClick={(e) => e.stopPropagation()}
    />
  );
};

export const Select: React.FC<{ row: TreeRow }> = ({ row }) => {
  const { onSelectRow, selectedMap, checkboxLevels } = useDataGridContext();

  const handleChange = useCallback(
    (event: React.MouseEvent) => {
      onSelectRow(event, row.id);
    },
    [onSelectRow, row.id]
  );

  if (checkboxLevels && !checkboxLevels.includes(row.groupLevel)) {
    return null;
  }

  return (
    <Checkbox
      checked={Boolean(selectedMap[row.id])}
      onClick={handleChange}
      onDoubleClick={(e) => e.stopPropagation()}
    />
  );
};

export const CreateSelectColumn = (): Column<any> => ({
  key: 'select',
  name: '',
  width: 44,
  minWidth: 44,
  cellClass: styles.cell,
  headerCellClass: styles.cell,
  sortable: false,
  resizable: false,
  formatter({ row }) {
    return <Select row={row} />;
  },
  headerRenderer({}) {
    return <HeaderSelect />;
  },
});

export const CreateLinkColumn = (): Column<any> => ({
  key: 'select',
  name: '',
  width: 44,
  minWidth: 44,
  cellClass: styles.cell,
  headerCellClass: styles.cell,
  sortable: false,
  resizable: false,
  formatter({ row }) {
    if (row.link) return <div />;
    return <Select row={row} />;
  },
  headerRenderer({}) {
    return <HeaderSelect />;
  },
});

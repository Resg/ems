import { useCallback, useMemo } from 'react';
import cn from 'classnames';

import LeftArrIcon from '@mui/icons-material/ChevronLeft';
import RightArrIcon from '@mui/icons-material/ChevronRight';
import FormControl from '@mui/material/FormControl';
import MenuItem from '@mui/material/MenuItem';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import Skeleton from '@mui/material/Skeleton';

import styles from './styles.module.scss';

export interface PagerProps {
  total: number;
  perPage: number;
  setPerPage: (perPage: number) => void;
  page: number;
  setPage: (page: number) => void;
  loading?: boolean;
  removeBorderStyles?: boolean;
}

const Pager: React.FC<PagerProps> = ({
  total,
  setPerPage,
  page,
  setPage,
  perPage,
  loading,
  removeBorderStyles = false,
}) => {
  const pagesAmount = useMemo(() => {
    return total / perPage;
  }, [total, perPage]);

  const handleChange = useCallback(
    (event: SelectChangeEvent<number>) => {
      setPerPage(Number(event.target.value));
    },
    [setPerPage]
  );

  const handleLeftClick = () => {
    setPage(page - 1);
  };

  const handleRightClick = () => {
    setPage(page + 1);
  };
  return (
    <div
      className={styles.box}
      style={{ border: removeBorderStyles ? 'none' : '1px solid #e0e0e0' }}
    >
      {loading ? (
        <Skeleton width={500} height={40} />
      ) : (
        <>
          <div>
            Строк на странице:
            <FormControl sx={{ ml: 1 }} variant="standard" size="small">
              <Select
                value={perPage}
                onChange={handleChange}
                sx={{
                  '&:before': {
                    display: 'none',
                  },
                  '& .MuiSelect-select': {
                    padding: 0,
                  },
                }}
              >
                <MenuItem value={10}>10</MenuItem>
                <MenuItem value={20}>20</MenuItem>
                <MenuItem value={50}>50</MenuItem>
                <MenuItem value={100}>100</MenuItem>
              </Select>
            </FormControl>
          </div>
          <div className={styles.amount} hidden={!total}>
            Загружено: {total}
          </div>
          <div
            className={cn(styles.left, {
              [styles.left_disable]: page === 1,
            })}
            onClick={handleLeftClick}
          >
            <LeftArrIcon fontSize="small" />
          </div>
          <div>Страница: {page}</div>
          <div
            className={cn(styles.right, {
              [styles.right_disable]: page >= pagesAmount,
            })}
            onClick={handleRightClick}
          >
            <RightArrIcon fontSize="small" />
          </div>
        </>
      )}
    </div>
  );
};

export default Pager;

import { ReactElement } from 'react';

export interface DefaultRow {
  id: number;
  [key: string]: any;
}

export type TreeRow<Row extends DefaultRow = DefaultRow> =
  | {
      id: string | number;
      parentId?: string;
      groupLevel: number;
      isExpanded?: boolean;
      link?: string;
      [key: string]: any;
      children?: TreeRow<Row>[] | Row[];
    }
  | Row;

export interface DefaultColumn {
  key: string;
  name: string | ReactElement;
  hidden?: boolean;
}

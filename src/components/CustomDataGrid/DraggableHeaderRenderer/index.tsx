import React from 'react';
import { headerRenderer, HeaderRendererProps } from 'react-data-grid';
import { useDrag, useDrop } from 'react-dnd';

import styles from './styles.module.scss';

export interface DraggableHeaderRendererProps<T>
  extends HeaderRendererProps<T> {
  onColumnReorder: (sourceKey: string, targetKey: string) => void;
}

const COLUMN_DRAG_TYPE = 'COLUMN_DRAG';

export function DraggableHeaderRenderer<R>({
  onColumnReorder,
  column,
  ...props
}: DraggableHeaderRendererProps<R>) {
  const [{ isDragging }, drag] = useDrag({
    type: COLUMN_DRAG_TYPE,
    item: { key: column.key },
    collect: (monitor) => ({ isDragging: monitor.isDragging() }),
  });
  const [{ isOver }, drop] = useDrop({
    accept: COLUMN_DRAG_TYPE,
    drop: ({ key }: { key: string }) => {
      onColumnReorder(key, column.key);
    },
    collect: (monitor) => ({
      canDrop: monitor.canDrop(),
      isOver: monitor.isOver(),
    }),
  });

  return (
    <div
      ref={(ref) => {
        drag(ref);
        drop(ref);
      }}
      style={{
        opacity: isDragging ? 0.5 : 1,
        backgroundColor: isOver ? '#ececec' : undefined,
        display: 'flex',
        alignItems: 'center',
        cursor: 'move',
        height: '40px',
        width: '100%',
      }}
      className={styles.header}
    >
      {headerRenderer({ column, ...props })}
    </div>
  );
}

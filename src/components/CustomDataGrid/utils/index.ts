import { groupBy } from 'lodash';
import { SortColumn } from 'react-data-grid';

import { DefaultRow, TreeRow } from '../types';

interface SortType {
  columnKey: string;
  direction: string;
}

export function createTreeByGroups<Row extends DefaultRow = DefaultRow>(
  rows: Row[],
  groups: (keyof Row)[],
  expanded: boolean = false,
  groupLevel: number = 1,
  parentId?: string
): TreeRow<Row>[] {
  if (!groups.length) {
    return rows.map((row) => ({ ...row, parentId }));
  }

  const [group, ...restGroups] = groups;
  const currentData = groupBy(rows, group);

  if (expanded) {
    return Object.keys(currentData).reduce((acc, key) => {
      const id = `${key}${parentId ? `-${parentId}` : ''}`;
      const children = createTreeByGroups(
        currentData[key],
        restGroups,
        expanded,
        groupLevel + 1,
        id
      );

      const currentRow = {
        id,
        groupLevel,
        parentId,
        [group]: key,
        isExpanded: expanded,
        children,
      };
      return [...acc, currentRow, ...children];
    }, [] as TreeRow<Row>[]);
  }

  return Object.keys(currentData).map((key) => {
    const id = `${key}${parentId ? `-${parentId}` : ''}`;

    return {
      id,
      groupLevel,
      parentId,
      [group]: key,
      isExpanded: expanded,
      children: createTreeByGroups(
        currentData[key],
        restGroups,
        expanded,
        groupLevel + 1,
        id
      ),
    };
  });
}

export function createTreeByRows<Row extends DefaultRow = DefaultRow>(
  rows: Row[],
  expanded: boolean = false,
  groupLevel: number = 1,
  parentId?: number
): TreeRow<Row>[] {
  let currentData;

  if (!parentId) {
    currentData = rows.filter((row) => !row.parentId);
  } else {
    currentData = rows.filter((row) => row.parentId === parentId);
  }

  if (expanded) {
    return currentData.reduce((acc, row) => {
      const children = row.id
        ? createTreeByRows(rows, expanded, groupLevel + 1, row.id)
        : [];
      const currentRow = {
        ...row,
        groupLevel,
        parentId: row.parentId,
        isExpanded: expanded,
        children: children.length ? children : undefined,
      };
      return [...acc, currentRow, ...children];
    }, [] as TreeRow<Row>[]);
  }

  return currentData.map((row) => {
    const children = row.id
      ? createTreeByRows(rows, expanded, groupLevel + 1, row.id)
      : [];
    return {
      groupLevel,
      parentId: row.parentId,
      isExpanded: expanded,
      children: children.length ? children : undefined,
      ...row,
    };
  });
}

export function toggleSubRow<Row extends DefaultRow = DefaultRow>(
  rows: TreeRow<Row>[],
  id: string | number
): TreeRow<Row>[] {
  const rowIndex = rows.findIndex((r) => String(r.id) === String(id));
  const row = rows[rowIndex];

  const children: TreeRow<Row>[] = row?.children;
  if (!children) return rows;

  let newRows = [...rows];
  newRows[rowIndex] = { ...row, isExpanded: !row.isExpanded };
  if (!row.isExpanded) {
    newRows.splice(rowIndex + 1, 0, ...children);
  } else {
    children?.forEach((entry) => {
      if (rows[rows.findIndex((r) => r.id === entry.id)].isExpanded) {
        newRows = toggleSubRow(newRows, String(entry.id));
      }
    });

    newRows[rowIndex].children = newRows.splice(rowIndex + 1, children.length);
  }
  return newRows;
}

export function selectRow<Row extends DefaultRow = DefaultRow>(
  rows: any,
  id?: number,
  parent?: number | string
): TreeRow<Row>[] {
  const newRows = [...rows];

  if (parent) {
    const parentIdx = rows.findIndex((r: any) => r.id === parent);
    const hh = rows[parentIdx].children.findIndex((r: any) => r.id === id);

    newRows[parentIdx].children[hh] = {
      ...newRows[parentIdx].children[hh],
      selected: !newRows[parentIdx].children[hh].selected,
    };

    return newRows;
  }

  const rowIndex = rows.findIndex((r: any) => r.id === id);
  newRows[rowIndex] = {
    ...newRows[rowIndex],
    selected: !newRows[rowIndex].selected,
  };

  return newRows;
}

export function selectAll<Row extends DefaultRow = DefaultRow>(
  rows: TreeRow<Row>[],
  map: Record<string | number, boolean>,
  value: boolean
): Record<string | number, boolean> {
  rows.forEach((row) => {
    map[row.id] = value;
    if (row.children) {
      map = { ...map, ...selectAll(row.children, map, value) };
    }
  });

  return { ...map };
}

function selectParent<Row extends DefaultRow = DefaultRow>(
  rows: TreeRow<Row>[],
  map: Record<string | number, boolean>,
  parentId: string | number
): Record<string | number, boolean> {
  const parentRow = rows.find((r) => r.id === parentId);
  if (!parentRow) {
    return map;
  }

  map[parentId] = !parentRow.children?.some((r: TreeRow<Row>) => !map[r.id]);

  if (parentRow.parentId) {
    map = { ...map, ...selectParent(rows, map, parentRow.parentId) };
  }

  return { ...map };
}

export function selectTreeRow<Row extends DefaultRow = DefaultRow>(
  rowId: string | number,
  rows: TreeRow<Row>[],
  map: Record<string | number, boolean>,
  value: boolean,
  selectAllFunc: (
    rows: TreeRow<Row>[],
    map: Record<string | number, boolean>,
    value: boolean
  ) => Record<string | number, boolean>,
  singleSelect?: boolean,
  childrenNoSelect?: boolean
): Record<string | number, boolean> {
  if (singleSelect) {
    map = {};
  }

  map[rowId] = value;

  const row = rows.find((r) => r.id === rowId);
  if (!row) {
    return map;
  }
  if (row.children && !childrenNoSelect) {
    map = { ...map, ...selectAllFunc(row.children, map, value) };
  }

  const { parentId } = row;

  if (parentId && !singleSelect && !childrenNoSelect) {
    map = { ...map, ...selectParent(rows, map, parentId) };
  }

  return map;
}

export function sortTreeRows<Row extends DefaultRow = DefaultRow>(
  rows: any,
  sortColumn: SortColumn[]
) {
  const sortedRows = rows.slice();
  function getComparator(sort: SortType) {
    return (a: Row, b: Row) => {
      try {
        const { columnKey, direction } = sort;

        const isNumericString = (str: string) => /^\d+$/.test(str);
        if (a[columnKey] === null && b[columnKey] === null) {
          return 0;
        } else if (a[columnKey] === null) return direction === 'ASC' ? 1 : -1;
        else if (b[columnKey] === null) return direction === 'ASC' ? -1 : 1;

        const isDateString = (str: string) =>
          /(null|\d{2}\.\d{2}\.\d{4})$/.test(str);
        if (isNumericString(a[columnKey]) && isNumericString(b[columnKey])) {
          return parseInt(a[columnKey], 10) - parseInt(b[columnKey], 10);
        } else if (isDateString(a[columnKey]) && isDateString(b[columnKey])) {
          return (
            (new Date(a[columnKey]?.split('.').reverse().join('.')) as any) -
            (new Date(b[columnKey]?.split('.').reverse().join('.')) as any)
          );
        }
        return a[columnKey].localeCompare(b[columnKey]);
      } catch (e) {}
    };
  }

  function sortRows(rows: Row[], sortColumns: SortColumn[]) {
    if (sortColumns.length === 0) return rows;
    return rows.sort((a: Row, b: Row) => {
      for (const sort of sortColumns) {
        const comparator = getComparator(sort);
        const compResult = comparator(a, b);
        if (compResult !== 0) {
          return sort.direction === 'ASC' ? compResult : -compResult;
        }
      }
      return 0;
    });
  }

  for (let rowI = 0; rowI < sortedRows.length; rowI++) {
    let countStr = 1;
    while (
      sortedRows[rowI + countStr] &&
      !sortedRows[rowI + countStr].children
    ) {
      countStr++;
    }
    const arr = sortRows(sortedRows.slice(rowI, rowI + countStr), sortColumn);
    let countStrJ = 0;
    for (let rowJ = rowI; rowJ < rowI + countStr; rowJ++) {
      sortedRows[rowJ] = arr[countStrJ];
      countStrJ++;
    }

    rowI += countStr;
  }

  return sortedRows as any;
}

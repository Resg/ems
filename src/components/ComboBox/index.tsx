import React from 'react';

import { Autocomplete, AutocompleteProps, TextFieldProps } from '@mui/material';

import EmsInput from '../Input';

import styles from './styles.module.scss';

export type ComboBoxPropsType = Omit<
  AutocompleteProps<any, any, any, any>,
  'renderInput'
> & {
  textFieldProps: TextFieldProps;
  notEditableField?: boolean;
};

export const ComboBox: React.FC<ComboBoxPropsType> = ({
  notEditableField,
  textFieldProps,
  sx = {},
  ...restProps
}) => {
  const inputLabelProps = {
    classes: {
      root: textFieldProps.required ? styles.requiredFieldLabel : '',
      focused: textFieldProps.required ? styles.requiredFieldLabel : '',
    },
  };

  return (
    <Autocomplete
      size={'small'}
      renderInput={(params) => {
        const { InputProps, ...otherParams } = params;
        const newInputProps = {
          ...InputProps,
          classes: {
            notchedOutline: textFieldProps.required ? styles.requiredField : '',
            disabled: notEditableField
              ? styles.notEditableTextField
              : styles.editableTextField,
          },
        };
        return (
          <EmsInput
            {...otherParams}
            {...textFieldProps}
            sx={textFieldProps.sx ?? { width: '100%' }}
            InputProps={newInputProps}
            InputLabelProps={inputLabelProps}
          />
        );
      }}
      {...restProps}
      disabled={restProps.disabled || notEditableField}
      sx={
        notEditableField
          ? sx
          : {
              ...sx,
              'div > div > div.Mui-disabled': { opacity: 1 },
              'div > div > div.Mui-disabled > svg': { opacity: 0.38 },
            }
      }
    />
  );
};

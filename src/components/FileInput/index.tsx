import React, { ChangeEvent, useCallback, useMemo, useRef } from 'react';
import cn from 'classnames';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { AttachFile } from '@mui/icons-material';
import { Chip, IconButton, TextField } from '@mui/material';

import styles from './styles.module.scss';

export interface FileInputProps {
  name: string;
  label: string;
  className?: string;
  multiple?: boolean;
  accept?: string;
  disabled?: boolean;
  required?: boolean;
}

export const FileInput: React.FC<FileInputProps> = ({
  name,
  className,
  label,
  multiple,
  accept = '.rtf',
  required,
  disabled,
}) => {
  const { values, setFieldValue, errors, touched } = useFormikContext<any>();
  const ref = useRef<HTMLInputElement>(null);
  const value: File = useMemo(() => get(values, name), [name, values]);

  const handleChange = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      setFieldValue(name, event.target.files?.item(0));
    },
    [name, setFieldValue]
  );

  const isTouched = get(touched, name);

  const error: string = useMemo(() => {
    const error = get(errors, name);
    if (!error) {
      return '';
    }
    if (Array.isArray(error)) {
      return error[0] as string;
    } else {
      return error as string;
    }
  }, [errors, name]);

  const handleDelete = useCallback(() => {
    setFieldValue(name, null);
    if (ref.current) {
      ref.current.value = '';
    }
  }, [name, setFieldValue]);

  return (
    <div className={cn(styles.container, className)}>
      <input
        type={'file'}
        hidden
        id={name}
        ref={ref}
        accept={accept}
        multiple={multiple}
        onChange={handleChange}
      />
      <TextField
        variant={'outlined'}
        fullWidth
        label={label}
        required={required}
        error={Boolean(error)}
        helperText={error}
        InputProps={{
          startAdornment: value && (
            <Chip label={'Файл'} onDelete={handleDelete} />
          ),
          className: styles.input,
          disabled: true,
          classes: {
            notchedOutline: !disabled && !error ? styles.border : '',
            disabled: !disabled && !error ? styles.border : '',
          },
        }}
      />
      <label htmlFor={disabled ? '' : name}>
        <IconButton
          className={styles.button}
          component={'span'}
          disabled={disabled}
        >
          <AttachFile />
        </IconButton>
      </label>
    </div>
  );
};

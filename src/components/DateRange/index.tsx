import React, { useEffect, useMemo, useState } from 'react';
import { endOfYear, format, isSameDay, startOfYear } from 'date-fns';
import { ru } from 'date-fns/locale';
import { useFormikContext } from 'formik';
import { get } from 'lodash';
import { DateRangePicker, Range } from 'react-date-range';
import InputMask from 'react-input-mask';

import { Close } from '@mui/icons-material';
import {
  Button,
  Grid,
  GridProps,
  Popover,
  Stack,
  TextField,
  TextFieldProps,
} from '@mui/material';

import Input from '../Input';

import { InputDate } from './InputDate';
import { customInputRanges, customStaticRange } from './StaticRange';

import 'react-date-range/dist/styles.css';
import 'react-date-range/dist/theme/default.css';
import styles from './styles.module.scss';

export interface DateRangeProps {
  nameFrom: string;
  nameBefore: string;
  labelFrom?: string;
  labelBefore?: string;
  required?: boolean;
  className?: string;
  disabled?: boolean;
  gridContainerProps?: GridProps;
  gridItemProps?: GridProps;
  textFieldProps?: TextFieldProps;
  clearField?: boolean;
}

interface RangeType {
  startDate: string | null;
  endDate: string | null;
  key: string;
}

export const DateRange: React.FC<DateRangeProps> = ({
  nameFrom,
  nameBefore,
  labelFrom,
  required,
  className,
  disabled,
  gridContainerProps = {},
  gridItemProps = {},
  textFieldProps = {},
}) => {
  const { setFieldValue, values } = useFormikContext<any>();

  const dateFrom = useMemo(() => get(values, nameFrom), [values]);
  const dateBefore = useMemo(() => get(values, nameBefore), [values]);

  const [range, setRange] = useState<any>([
    {
      startDate: dateFrom ? new Date(dateFrom) : null,
      endDate: dateBefore ? new Date(dateBefore) : null,
      key: 'selection',
    },
  ]);

  const [yearValue, setYearValue] = useState<string>('');

  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);

  const handlePopoverOpen = (event: React.MouseEvent<HTMLElement>) => {
    if (disabled) {
      return false;
    }
    setAnchorEl(event.currentTarget);
  };
  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const handleKeyUp = (event: React.KeyboardEvent) => {
    const ENTER = 'Enter';
    if (event.key === ENTER) {
      handlePopoverClose();
    }
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  const changeHandler = (datePeriod: Range) => {
    setFieldValue(nameFrom, format(datePeriod.startDate!, 'yyyy-MM-dd'));
    setFieldValue(nameBefore, format(datePeriod.endDate!, 'yyyy-MM-dd'));

    setRange(() => [
      {
        key: 'selection',
        startDate: datePeriod.startDate!,
        endDate: datePeriod.endDate!,
      },
    ]);
  };

  const fieldClearingHandler = () => {
    setFieldValue(nameFrom, '');
    setFieldValue(nameBefore, '');
  };

  useEffect(() => {
    setRange(() => [
      {
        key: 'selection',
        startDate: dateFrom ? new Date(dateFrom) : null,
        endDate: dateBefore ? new Date(dateBefore) : null,
      },
    ]);
  }, [dateFrom, dateBefore]);

  const getDisplayRange = (range: RangeType[]) => {
    const startDate = range[0].startDate;
    const endDate = range[0].endDate;

    if (!startDate && !endDate) return '';

    return `${startDate ? format(new Date(startDate), 'dd.MM.yyyy') : ''} - ${
      endDate ? format(new Date(endDate), 'dd.MM.yyyy') : ''
    }`;
  };

  const handleYearChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;
    const year = Number(value);

    if (year) {
      const startDate = startOfYear(new Date(year, 1, 1));
      const endDate = endOfYear(new Date(year, 1, 1));
      setFieldValue(nameFrom, format(startDate, 'yyyy-MM-dd'));
      setFieldValue(nameBefore, format(endDate, 'yyyy-MM-dd'));
    }
    setYearValue(value);
  };

  useEffect(() => {
    const currentRange = range[0];
    if (!currentRange.startDate || !currentRange.endDate) {
      setYearValue((prev) => (prev === '' ? prev : ''));
      return;
    }
    if (
      !isSameDay(
        currentRange.startDate as Date,
        startOfYear(currentRange.startDate) as Date
      )
    ) {
      setYearValue((prev) => (prev === '' ? prev : ''));
      return;
    }
    if (
      !isSameDay(
        currentRange.endDate as Date,
        endOfYear(currentRange.endDate) as Date
      )
    ) {
      setYearValue((prev) => (prev === '' ? prev : ''));
      return;
    }
    if (
      currentRange.startDate.getFullYear() !==
      currentRange.endDate.getFullYear()
    ) {
      setYearValue((prev) => (prev === '' ? prev : ''));
      return;
    }
    setYearValue(String(currentRange.endDate.getFullYear()));
  }, [range]);

  return (
    <div>
      <Grid container {...gridContainerProps}>
        <Grid item xs={12} {...gridItemProps}>
          <TextField
            label={labelFrom}
            required={required}
            disabled={disabled}
            size="small"
            variant="outlined"
            value={getDisplayRange(range)}
            onClick={handlePopoverOpen}
            aria-describedby={id}
            sx={textFieldProps.sx || { width: '100%' }}
            className={className}
            {...textFieldProps}
          />
        </Grid>
      </Grid>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        onClose={handlePopoverClose}
        onKeyUp={handleKeyUp}
      >
        <DateRangePicker
          onChange={(item) => changeHandler(item.selection)}
          editableDateInputs={true}
          moveRangeOnFirstSelection={false}
          monthDisplayFormat={'LLLL yyyy'}
          ranges={range}
          locale={ru}
          months={2}
          direction="horizontal"
          staticRanges={customStaticRange}
          inputRanges={customInputRanges}
          className={styles.wrapper}
        />
        <Stack direction="row" spacing={2} className={styles.dislayDateBox}>
          <Grid container columnSpacing={2}>
            <Grid item xs={5}>
              <InputDate name={nameFrom} autoFocus />
            </Grid>
            <Grid item xs={5}>
              <InputDate name={nameBefore} />
            </Grid>
            <Grid item xs={2}>
              <InputMask
                mask={'9999'}
                maskChar={null}
                value={yearValue}
                onChange={handleYearChange}
              >
                <Input label={'Год'} />
              </InputMask>
            </Grid>
          </Grid>
        </Stack>
        <Button
          variant="outlined"
          startIcon={<Close />}
          className={styles.button}
          onClick={fieldClearingHandler}
        >
          Очистить
        </Button>
      </Popover>
    </div>
  );
};

import React, { useEffect, useMemo, useState } from 'react';
import { format, isValid, parse } from 'date-fns';
import { useFormikContext } from 'formik';
import { get } from 'lodash';
import InputMask from 'react-input-mask';

import Input, { InputProps } from '../../components/Input';

export type InputDateProps = InputProps & {
  name: string;
};

export const InputDate: React.FC<InputDateProps> = ({
  placeholder,
  name,
  ...restProps
}) => {
  const { setFieldValue, values, setFieldError, touched, errors } =
    useFormikContext<any>();

  const date = useMemo(() => {
    const val = get(values, name);

    return val ? format(new Date(val), 'dd.MM.yyyy') : '';
  }, [values]);

  const [value, setValue] = useState<string>(date);

  const changeValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;
    setValue(value);
  };

  const handleMaskChange = (state: Record<string, any>) => {
    const { nextState, previousState } = state;
    const arrVal = nextState.value.split('.');

    if (Number(arrVal[0]) > 31 || Number(arrVal[1]) > 12) {
      return previousState;
    } else {
      return nextState;
    }
  };

  const onBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    const value = e.target.value;

    if (value) {
      const arrVal = value.split('.');
      arrVal.splice(0, 2, arrVal[1], arrVal[0]);

      if (isValid(new Date(arrVal.join('.')))) {
        setFieldValue(name, format(new Date(arrVal.join('.')), 'yyyy-MM-dd'));
      } else {
        setFieldValue(name, '');
      }
    } else {
      setFieldValue(name, '');
    }
  };

  useEffect(() => {
    setValue(date);
  }, [date]);

  const parsed = parse(value, 'dd.MM.yyyy', new Date());
  const error = value && !value.includes('_') && !isValid(parsed);

  return (
    <InputMask
      mask="99.99.9999"
      value={value}
      onChange={changeValue}
      beforeMaskedStateChange={handleMaskChange}
      name={name}
      onBlur={onBlur}
    >
      <Input
        {...restProps}
        error={!!error}
        helperText={error && 'Некорректная дата'}
      />
    </InputMask>
  );
};

import {
  addDays,
  addMonths,
  addYears,
  differenceInCalendarDays,
  endOfDay,
  endOfMonth,
  endOfWeek,
  endOfYear,
  isSameDay,
  startOfDay,
  startOfMonth,
  startOfWeek,
  startOfYear,
} from 'date-fns';
import { ru } from 'date-fns/locale';
import { Range } from 'react-date-range';

const getDefines = () => {
  return {
    startOfWeek: startOfWeek(new Date(), { locale: ru }),
    endOfWeek: endOfWeek(new Date(), { locale: ru }),
    startOfLastWeek: startOfWeek(addDays(new Date(), -7), { locale: ru }),
    endOfLastWeek: endOfWeek(addDays(new Date(), -7), { locale: ru }),
    startOfToday: startOfDay(new Date()),
    endOfToday: endOfDay(new Date()),
    startOfYesterday: startOfDay(addDays(new Date(), -1)),
    endOfYesterday: endOfDay(addDays(new Date(), -1)),
    startOfMonth: startOfMonth(new Date()),
    endOfMonth: endOfMonth(new Date()),
    startOfLastMonth: startOfMonth(addMonths(new Date(), -1)),
    endOfLastMonth: endOfMonth(addMonths(new Date(), -1)),
    startOfYear: startOfYear(new Date()),
    endOfYear: endOfYear(new Date()),
    startOfLastYear: startOfYear(addYears(new Date(), -1)),
    endOfLastYear: endOfYear(addYears(new Date(), -1)),
  };
};

export const customStaticRange = [
  {
    label: 'Сегодня',
    range: () => ({
      startDate: getDefines().startOfToday,
      endDate: getDefines().endOfToday,
    }),
    isSelected(range: Range) {
      const definedRange = this.range();
      return (
        isSameDay(range.startDate as Date, definedRange.startDate as Date) &&
        isSameDay(range.endDate as Date, definedRange.endDate as Date)
      );
    },
  },
  {
    label: 'Вчера',
    range: () => ({
      startDate: getDefines().startOfYesterday,
      endDate: getDefines().endOfYesterday,
    }),
    isSelected(range: Range) {
      const definedRange = this.range();
      return (
        isSameDay(range.startDate as Date, definedRange.startDate as Date) &&
        isSameDay(range.endDate as Date, definedRange.endDate as Date)
      );
    },
  },
  {
    label: 'Текущая неделя',
    range: () => ({
      startDate: getDefines().startOfWeek,
      endDate: getDefines().endOfWeek,
    }),
    isSelected(range: Range) {
      const definedRange = this.range();
      return (
        isSameDay(range.startDate as Date, definedRange.startDate as Date) &&
        isSameDay(range.endDate as Date, definedRange.endDate as Date)
      );
    },
  },
  {
    label: 'Предыдущая неделя',
    range: () => ({
      startDate: getDefines().startOfLastWeek,
      endDate: getDefines().endOfLastWeek,
    }),
    isSelected(range: Range) {
      const definedRange = this.range();
      return (
        isSameDay(range.startDate as Date, definedRange.startDate as Date) &&
        isSameDay(range.endDate as Date, definedRange.endDate as Date)
      );
    },
  },
  {
    label: 'Текущий месяц',
    range: () => ({
      startDate: getDefines().startOfMonth,
      endDate: getDefines().endOfMonth,
    }),
    isSelected(range: Range) {
      const definedRange = this.range();
      return (
        isSameDay(range.startDate as Date, definedRange.startDate as Date) &&
        isSameDay(range.endDate as Date, definedRange.endDate as Date)
      );
    },
  },
  {
    label: 'Предыдущий месяц',
    range: () => ({
      startDate: getDefines().startOfLastMonth,
      endDate: getDefines().endOfLastMonth,
    }),
    isSelected(range: Range) {
      const definedRange = this.range();
      return (
        isSameDay(range.startDate as Date, definedRange.startDate as Date) &&
        isSameDay(range.endDate as Date, definedRange.endDate as Date)
      );
    },
  },
  {
    label: 'Текущий год',
    range: () => ({
      startDate: getDefines().startOfYear,
      endDate: getDefines().endOfYear,
    }),
    isSelected(range: Range) {
      const definedRange = this.range();
      return (
        isSameDay(range.startDate as Date, definedRange.startDate as Date) &&
        isSameDay(range.endDate as Date, definedRange.endDate as Date)
      );
    },
  },
  {
    label: 'Предыдущий год',
    range: () => ({
      startDate: getDefines().startOfLastYear,
      endDate: getDefines().endOfLastYear,
    }),
    isSelected(range: Range) {
      const definedRange = this.range();
      return (
        isSameDay(range.startDate as Date, definedRange.startDate as Date) &&
        isSameDay(range.endDate as Date, definedRange.endDate as Date)
      );
    },
  },
];

export const customInputRanges = [
  {
    label: 'дней до сегодняшнего дня',
    range(value: number) {
      return {
        startDate: addDays(
          getDefines().startOfToday,
          (Math.max(Number(value), 1) - 1) * -1
        ),
        endDate: getDefines().endOfToday,
      };
    },
    getCurrentValue(range: Range) {
      if (!isSameDay(range.endDate as Date, getDefines().endOfToday as Date))
        return '-';
      if (!range.startDate) return '∞';
      return (
        differenceInCalendarDays(getDefines().endOfToday, range.startDate) + 1
      );
    },
  },
  {
    label: 'дней с сегодняшнего дня',
    range(value: number) {
      const today = new Date();
      return {
        startDate: today,
        endDate: addDays(today, Math.max(Number(value), 1) - 1),
      };
    },
    getCurrentValue(range: Range) {
      if (
        !isSameDay(range.startDate as Date, getDefines().startOfToday as Date)
      )
        return '-';
      if (!range.endDate) return '∞';
      return (
        differenceInCalendarDays(range.endDate, getDefines().startOfToday) + 1
      );
    },
  },
];

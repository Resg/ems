import React, { useCallback, useMemo } from 'react';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { FormControlLabel, Radio, RadioGroup } from '@mui/material';

export interface RadioButtonGroupProps {
  name: string;
  options: Array<{ label: string; value: any }>;
  className?: string;
  onChange?: (event: React.SyntheticEvent, value: any) => void;
  row?: boolean;
  defaultValue?: any;
}

export const RadioButtonGroup: React.FC<RadioButtonGroupProps> = ({
  name,
  options,
  className,
  onChange,
  row = false,
  defaultValue,
}) => {
  const { setFieldValue, values } = useFormikContext<any>();

  const handleChange = useCallback(
    (_event: React.SyntheticEvent, value: any) => {
      setFieldValue(name, value);
    },
    [name, setFieldValue]
  );

  const value = useMemo(() => {
    return get(values, name) || defaultValue;
  }, [values, name, defaultValue]);

  return (
    <RadioGroup
      name={name}
      value={value}
      onChange={onChange || handleChange}
      className={className}
      row={row}
    >
      {options.map((option, index) => {
        return (
          <FormControlLabel
            key={index}
            value={option.value}
            label={option.label}
            sx={{ width: 'fit-content' }}
            control={<Radio />}
          />
        );
      })}
    </RadioGroup>
  );
};

import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import cn from 'classnames';
import { groupBy } from 'lodash';
import { useSelector } from 'react-redux';
import AutoSizer from 'react-virtualized-auto-sizer';
import {
  VariableSizeNodeComponentProps,
  VariableSizeNodeData,
  VariableSizeTree,
} from 'react-vtree';

import {
  AccountCircle,
  Cancel,
  Check,
  ChevronRight,
} from '@mui/icons-material';
import { Box, Button, Stack, Tab, Tabs, TextField } from '@mui/material';

import {
  useGetDivisionsQuery,
  useGetEmployeesQuery,
} from '../../services/api/employee';
import { RootState, useAppDispatch } from '../../store';
import {
  resetEmployeeModal,
  selectEmployee,
  setEmployeeModalIsOpen,
} from '../../store/utils';
import {
  AllTreesNode,
  DivisionData,
  EmployeeData,
  EmployeeTreeNode,
} from '../../types/employee';
import Card from '../Card';
import { Popup } from '../Popup';

import styles from './styles.module.scss';

type StackElement = Readonly<{
  nestingLevel: number;
  node: EmployeeTreeNode;
}>;

type ExtendedData = VariableSizeNodeData &
  Readonly<{
    isLeaf: boolean;
    name: string;
    nestingLevel: number;
    employees: EmployeeData[];
  }>;

export const EmployeeSearchModal: React.FC = () => {
  const [tab, setTab] = useState(90);
  const [inputText, setInputText] = useState('');
  const [filteredEmployees, setFilteredEmployees] = useState([]);
  const treeRef = useRef<VariableSizeTree<ExtendedData>>(null);

  const keyProperty = useSelector(
    (state: RootState) => state.utils.keyProperty
  );

  const selectedEmployees = useSelector(
    (state: RootState) => state.utils.selectedEmployees
  );

  const onEmployeeModalSubmit = useSelector(
    (state: RootState) => state.utils.onEmployeeModalSubmit
  );

  const dispatch = useAppDispatch();

  const open = useSelector(
    (state: RootState) => state.utils.employeeModalIsOpen
  );

  const handleChange = useCallback(
    (event: React.SyntheticEvent, value: number) => {
      setTab(value);
    },
    []
  );

  const handleSelectEmployee = useCallback(
    (id: number) => dispatch(selectEmployee(id)),
    [dispatch]
  );

  const { data: divisions, refetch: refetchDivisions } = useGetDivisionsQuery(
    {}
  );

  const { data: employees, refetch: refetchEmployees } = useGetEmployeesQuery(
    {}
  );

  const [employeesFiltered, setEmployeesFiltered] = useState<EmployeeData[]>(
    []
  );

  const searchEmployees = useMemo(() => {
    return (
      employees?.data.filter((person) =>
        person.personFio.toLowerCase().includes(inputText)
      ) || []
    );
  }, [employees, inputText]);

  useEffect(() => {
    const timeoutId = setTimeout(
      () => setEmployeesFiltered(searchEmployees),
      1000
    );
    return () => clearTimeout(timeoutId);
  }, [searchEmployees]);

  const employeesGroupByDivisionId = useMemo(
    () => groupBy(employeesFiltered, 'divisionId'),
    [employeesFiltered]
  );

  const mainDivisions = useMemo(() => {
    return divisions?.data.filter(
      (el) => el.id === 90 || el.divisionParentId === 90
    );
  }, [divisions]);

  const mainDivisionsIds = useMemo(() => {
    return mainDivisions?.map((el) => el?.id);
  }, [mainDivisions]);

  const findDivisionChildren = useCallback(
    (parentDivId: number) => {
      const division: EmployeeTreeNode = {
        div: divisions?.data.find(
          (el) => el.id === parentDivId
        ) as DivisionData,
        children: [],
        employees: [...(employeesGroupByDivisionId[parentDivId] || [])],
      };

      divisions?.data.forEach((div) => {
        if (
          div.divisionParentId === parentDivId &&
          findDivisionChildren(div.id)
        ) {
          division.children.push(
            findDivisionChildren(div.id) as EmployeeTreeNode
          );
        }
      });

      if (division.children.length || division.employees.length) {
        return division;
      }
    },
    [divisions, employeesGroupByDivisionId]
  );

  const allTrees = useMemo(() => {
    const allTreesArray: AllTreesNode = {};
    mainDivisionsIds?.forEach(
      (id) => (allTreesArray[id] = findDivisionChildren(id) as EmployeeTreeNode)
    );
    return allTreesArray;
  }, [mainDivisionsIds, findDivisionChildren]);

  const tree = useMemo(() => allTrees[tab], [allTrees, tab]);

  const handleClose = useCallback(() => {
    dispatch(setEmployeeModalIsOpen(false));
    dispatch(resetEmployeeModal());
  }, [dispatch]);

  const handleSubmit = useCallback(() => {
    onEmployeeModalSubmit?.(
      Object.keys(selectedEmployees)
        .filter((id) => selectedEmployees[Number(id)])
        .map(Number)
    );
    dispatch(setEmployeeModalIsOpen(false));
  }, [selectedEmployees, onEmployeeModalSubmit, dispatch]);

  const itemHeight: number = 44;
  const lineHeight: number = 24;

  const Node: React.FC<VariableSizeNodeComponentProps<ExtendedData>> = ({
    height,
    data: { defaultHeight, employees, isLeaf, name, nestingLevel },
    isOpen,
    resize,
    style,
    toggle,
  }) => {
    const emplRefs = useMemo(
      () => employees?.map(() => React.createRef<HTMLDivElement>()),
      [employees]
    );

    const findMultipleLineElements = useCallback(() => {
      return (
        (emplRefs &&
          emplRefs.length &&
          emplRefs[0].current &&
          emplRefs.filter((el) => el.current!.offsetHeight > itemHeight)) ||
        []
      );
    }, [emplRefs]);

    useEffect(() => {
      const multipleLineElements = findMultipleLineElements();

      isOpen && emplRefs?.length
        ? resize(
            (emplRefs?.length + 1) * itemHeight +
              lineHeight * multipleLineElements.length,
            true
          )
        : resize(itemHeight, true);
    }, [isOpen, height, emplRefs]);

    return (
      <div style={style}>
        <div
          style={{
            alignItems: 'center',
            display: 'flex',
            marginLeft: nestingLevel * 30 + (isLeaf ? 54 : 0),
          }}
          onClick={toggle}
        >
          {!isLeaf && (
            <div className={styles.icon}>
              <ChevronRight
                className={cn(styles.chevron, {
                  [styles.chevron_opened]: isOpen,
                })}
              />
            </div>
          )}
          <div className={styles.divisionName}>
            <span>{name}</span>
          </div>
        </div>
        {isOpen && (
          <div
            style={{
              marginLeft: 30 + nestingLevel * 30,
            }}
          >
            <div>
              {employees?.map((person, i) => (
                <div
                  key={i}
                  className={cn(styles.employee, {
                    [styles.employee_selected]:
                      selectedEmployees[person[keyProperty] as number],
                  })}
                  onClick={() =>
                    handleSelectEmployee(person[keyProperty] as number)
                  }
                  ref={emplRefs[i]}
                >
                  <AccountCircle />
                  <span className={styles.name}>{person.personFio}</span>
                  {person.post && (
                    <div className={styles.post}>
                      <div className={styles.hyphen}>
                        &nbsp;&nbsp; - &nbsp;&nbsp;
                      </div>
                      {person.post ? `${person.post}` : null}
                    </div>
                  )}
                </div>
              ))}
            </div>
          </div>
        )}
      </div>
    );
  };

  const treeWalker = useMemo(() => {
    return function* treeWalker(
      refresh: boolean
    ): Generator<ExtendedData | string | symbol, void, boolean> {
      const stack: StackElement[] = [];

      stack.push({
        nestingLevel: 0,
        node: tree,
      });

      while (stack.length !== 0) {
        const { node, nestingLevel } = stack.pop()!;
        const id = node?.div?.id.toString() || '';

        const isOpened = yield refresh
          ? {
              employees: node?.employees,
              defaultHeight: (node?.employees.length + 1) * itemHeight,
              id,
              isLeaf:
                !node ||
                (node?.employees.length === 0 && node?.children.length === 0),
              isOpenByDefault: true,
              name: node?.div?.fullName,
              nestingLevel,
            }
          : id;

        if (node?.children.length !== 0 && isOpened) {
          for (let i = node?.children.length - 1; i >= 0; i--) {
            stack.push({
              nestingLevel: nestingLevel + 1,
              node: node?.children[i],
            });
          }
        }
      }
    };
  }, [tree]);

  useEffect(() => {
    if (treeRef.current) {
      treeRef.current.recomputeTree({
        refreshNodes: true,
        useDefaultOpenness: true,
      });
    }
  }, [tree]);

  return (
    <Popup
      open={open}
      onClose={handleClose}
      title="Выбор сотрудника ГРЧЦ"
      bar={
        <Stack
          direction="row"
          justifyContent="flex-end"
          spacing={1.5}
          style={{ width: '100%' }}
        >
          <Button
            variant="contained"
            startIcon={<Check />}
            onClick={handleSubmit}
          >
            Выбрать
          </Button>
          <Button
            variant={'outlined'}
            startIcon={<Cancel />}
            onClick={handleClose}
          >
            Отмена
          </Button>
        </Stack>
      }
    >
      <>
        <TextField
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            setInputText(e.target.value.toLowerCase())
          }
          fullWidth
          variant="outlined"
          label="ФИО"
          sx={{
            marginTop: 1.5,
          }}
          size="small"
        />
        <Box
          sx={{
            borderColor: 'rgba(0,0,0,0.12) !important',
            borderBottom: 1,
            width: '100%',
            marginBottom: 2.5,
          }}
        >
          <Tabs
            value={tab}
            onChange={handleChange}
            scrollButtons
            variant="scrollable"
          >
            <Tab label={'Все'} value={90} />
            {mainDivisions && mainDivisions.length
              ? mainDivisions
                  .filter((div) => div.id !== 90)
                  .map((div) => {
                    return (
                      <Tab label={div.shortName} value={div.id} key={div.id} />
                    );
                  })
              : null}
          </Tabs>
        </Box>
        <Card className={styles.card}>
          <AutoSizer disableWidth>
            {({ height }) => (
              <VariableSizeTree
                treeWalker={treeWalker}
                height={height}
                width="100%"
                ref={treeRef}
                className={styles.sizer}
              >
                {Node}
              </VariableSizeTree>
            )}
          </AutoSizer>
        </Card>
      </>
    </Popup>
  );
};

import React from 'react';
import { connect } from 'react-redux';

import { ErrorData, setError } from '../../store/utils';

interface Props {
  children: React.ReactNode;
  setError: (message: ErrorData) => void;
}

export class ErrorBoundaryClass extends React.Component<Props> {
  componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {
    this.props.setError({ message: error.name + ' ' + error.message });
  }

  render() {
    return this.props.children;
  }
}

export const ErrorBoundary = connect(null, {
  setError,
})(ErrorBoundaryClass);

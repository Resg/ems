import React, { memo, useEffect, useRef } from 'react';

import { useEventEmitter, useScript } from './utils/hooks';

export interface IContext {
  ooEditor?: React.ElementRef<any>;
  onlyOfficeId?: string;
  getDownloadURL?: () => Promise<unknown>;
}

export const Context = React.createContext<IContext>({});

export interface OnlyOfficeProps {
  children?: React.ReactNode;
  document?: Record<string, any>;
  documentType?: string;
  editorConfig?: Record<string, any>;
  events?: any;
  height?: string;
  onlyOfficeId?: string;
  scriptUrl: string;
  token?: string;
  type?: string;
  width?: string;
}

const BaseOnlyOffice = memo(
  ({
    children,
    document,
    documentType,
    editorConfig,
    events,
    height,
    onlyOfficeId,
    scriptUrl,
    token,
    type,
    width,
    ...rest
  }: OnlyOfficeProps) => {
    const config = {
      document,
      documentType,
      editorConfig,
      events,
      height,
      token,
      type,
      width,
      ...rest,
    };
    const ref = useRef<any>();
    const ooEditor = ref.current;
    const [loaded] = useScript(scriptUrl);
    const emitDownload = useEventEmitter('onDownloadAs');

    const onDownloadAs = (e: any) => {
      emitDownload(e.data);
      events.onDownloadAs && events.onDownloadAs(e);
    };
    const getDownloadURL = async () => {
      if (!ooEditor) {
        return;
      }
      ooEditor.downloadAs();
      return new Promise((resolve) => {
        // @ts-ignore
        window.addEventListener('onDownloadAs', (e) => resolve(e.detail), {
          once: true,
        });
      });
    };

    useEffect(() => {
      if (!loaded || !window.DocsAPI) return;
      // if (ooEditor) ooEditor.destroyEditor();
      ref.current = new window.DocsAPI.DocEditor(onlyOfficeId, {
        ...config,
        events: { ...events, onDownloadAs },
      });
    }, [loaded, config, onlyOfficeId]);

    return (
      <Context.Provider value={{ ooEditor, onlyOfficeId, getDownloadURL }}>
        {children}
      </Context.Provider>
    );
  }
);

export default BaseOnlyOffice;

import React, { useEffect, useState } from 'react';
import { Form, Formik, FormikProps, FormikValues } from 'formik';

import { ArrowForward } from '@mui/icons-material';
import { Button, Grid } from '@mui/material';

import { useGetTemplateVacantMutation } from '../../../services/api/user';
import Input from '../../Input';

import styles from './styles.module.scss';

interface FormPageProps {
  objectTypeCode: string;
  formRef: React.RefObject<FormikProps<FormikValues>>;
  setPage: (page: 1 | 2) => void;
  setSaveIsDisabled: (value: boolean) => void;
}
export const FormPage: React.FC<FormPageProps> = React.memo(
  ({ objectTypeCode, formRef, setPage, setSaveIsDisabled }) => {
    const [getTemplateVacant] = useGetTemplateVacantMutation();
    const [name, setName] = useState('');

    useEffect(() => {
      const timeOut = setTimeout(() => {
        if (name) {
          getTemplateVacant({ name, objectTypeCode }).then((res) => {
            if ('data' in res) {
              setSaveIsDisabled(!res.data.isTemplateNameVacant);
            } else {
              setSaveIsDisabled(true);
            }
          });
        } else {
          setSaveIsDisabled(true);
        }
      }, 500);
      return () => {
        clearTimeout(timeOut);
      };
    }, [name]);

    return (
      <Formik
        initialValues={{
          name: name,
        }}
        enableReinitialize
        onSubmit={() => {}}
        innerRef={formRef}
      >
        {({ values, setFieldValue }) => {
          const handleChange = async (
            e: React.FocusEvent<HTMLInputElement>
          ) => {
            const value = e.target.value;
            setFieldValue('name', value);
            setName(value);
          };
          return (
            <Form autoComplete="off">
              <Grid container spacing={2}>
                <Grid item xs={12} sx={{ mt: 3 }}>
                  <Input
                    name="name"
                    label="Наименование шаблона"
                    value={values.name}
                    onChange={handleChange}
                    required
                  />
                </Grid>
                <Grid
                  item
                  xs={12}
                  sx={{ mt: 3 }}
                  className={styles.buttonContainer}
                >
                  <Button
                    endIcon={<ArrowForward />}
                    onClick={() => setPage(2)}
                    className={styles.forwardButton}
                  >
                    Доступные шаблоны
                  </Button>
                </Grid>
              </Grid>
            </Form>
          );
        }}
      </Formik>
    );
  }
);

import React, { useCallback, useMemo, useState } from 'react';

import { DeleteOutline, Refresh } from '@mui/icons-material';
import { Button, Stack } from '@mui/material';

import {
  useDeleteTaskTemplateMutation,
  useGetTaskTemplatesQuery,
} from '../../../services/api/user';
import { DataTable } from '../../CustomDataGrid';
import { RoundButton } from '../../RoundButton';

interface TablePageProps {
  objectTypeCode: string;
}

export const TablePage: React.FC<TablePageProps> = ({ objectTypeCode }) => {
  const [pagerPage, setPagerPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const selectedTemplates = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const { data, refetch, isLoading } = useGetTaskTemplatesQuery({
    objectTypeCode,
  });
  const [deleteTemplate] = useDeleteTaskTemplateMutation();

  const rows = useMemo(() => {
    return data?.data || [];
  }, [data]);

  const cols = useMemo(() => {
    return [
      {
        key: 'name',
        name: 'Наименование',
      },
    ];
  }, []);

  const handleDelete = useCallback(async () => {
    await deleteTemplate(selectedTemplates[0]);
    refetch();
  }, [selectedTemplates]);

  return (
    <>
      <Stack
        direction="row"
        spacing={1.5}
        marginY={2}
        justifyContent="space-between"
      >
        <RoundButton icon={<Refresh />} onClick={() => refetch()} />
        <Button
          variant="outlined"
          startIcon={<DeleteOutline />}
          onClick={handleDelete}
          disabled={selectedTemplates.length !== 1}
        >
          Удалить
        </Button>
      </Stack>
      <DataTable
        formKey={'table_page_save_template'}
        rows={rows}
        cols={cols}
        onRowSelect={setSelected}
        loading={isLoading}
        pagerProps={{
          page: pagerPage,
          setPage: setPagerPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: data?.totalCount || 0,
        }}
      />
    </>
  );
};

import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { FormikProps, FormikValues } from 'formik';

import { Close, Save } from '@mui/icons-material';
import { Button, Stack } from '@mui/material';

import { useCreateTaskTemplateMutation } from '../../services/api/user';
import { ObjectTask } from '../../types/tasks';
import { CreateTemplateTree } from '../../utils/saveToTemplateModal';
import { Popup } from '../Popup';

import { FormPage } from './FormPage';
import { TablePage } from './TablePage';

interface SaveTaskToTemplateModalProps {
  open: 'task' | 'route' | null;
  setOpen: (open: 'task' | 'route' | null) => void;
  objectTypeCode: string;
  selectedId: number;
  rows: ObjectTask[];
}

export const SaveToTemplateModal: React.FC<SaveTaskToTemplateModalProps> = ({
  open,
  setOpen,
  objectTypeCode,
  selectedId,
  rows,
}) => {
  const [page, setPage] = useState<1 | 2>(1);
  const [saveIsDisabled, setSaveIsDisabled] = useState(true);
  const [createTemplate] = useCreateTaskTemplateMutation();
  const formRef = useRef<FormikProps<FormikValues>>(null);

  const handleSubmit = useCallback(async () => {
    const name = formRef.current?.values.name;

    if (name) {
      const template = CreateTemplateTree(rows, selectedId);
      const response = await createTemplate({ name, template, objectTypeCode });
      if ('data' in response) {
        setOpen(null);
      }
    }
  }, [formRef.current, rows]);

  useEffect(() => {
    setPage(1);
  }, [open]);

  const bar = useMemo(
    () => (
      <Stack
        direction="row"
        justifyContent="flex-end"
        spacing={1.5}
        style={{ width: '100%' }}
      >
        {page === 1 && (
          <Button
            variant="contained"
            startIcon={<Save />}
            onClick={() => handleSubmit()}
            disabled={saveIsDisabled}
          >
            Сохранить
          </Button>
        )}
        <Button
          variant="outlined"
          startIcon={<Close />}
          onClick={() => setOpen(null)}
        >
          Отмена
        </Button>
      </Stack>
    ),
    [page, handleSubmit, saveIsDisabled]
  );

  const title = useMemo(() => {
    if (page === 1) {
      return open === 'task'
        ? 'Сохранение задачи в шаблон'
        : 'Сохранение маршрута в шаблон';
    } else {
      return 'Доступные шаблоны';
    }
  }, [page, open]);

  const renderTemplatePage = useCallback(() => {
    if (page === 1) {
      return (
        <FormPage
          objectTypeCode={objectTypeCode}
          formRef={formRef}
          setPage={setPage}
          setSaveIsDisabled={setSaveIsDisabled}
        />
      );
    } else {
      return <TablePage objectTypeCode={objectTypeCode} />;
    }
  }, [page, objectTypeCode]);

  return (
    <Popup
      open={Boolean(open)}
      onClose={() => setOpen(null)}
      backButton={page === 2}
      onBack={() => setPage(1)}
      title={title}
      width={750}
      bar={bar}
    >
      {renderTemplatePage()}
    </Popup>
  );
};

import React, { useCallback, useMemo } from 'react';
import cn from 'classnames';
import { useFormikContext } from 'formik';

import { Autocomplete, SxProps } from '@mui/material';

import Input from '../Input';

import styles from './styles.module.scss';

export interface AutoSuggestInputProps {
  sx?: SxProps;
  disabled?: boolean;
  required?: boolean;
  name: string;
  label: string;
  options: string[];
  className?: string;
  inputClassName?: string;
  multiline?: boolean;
}

export const AutoSuggestInput: React.FC<AutoSuggestInputProps> = ({
  sx = {},
  disabled,
  required,
  label,
  name,
  options,
  className,
  inputClassName,
  multiline,
}) => {
  const { setFieldValue, values, errors, setFieldTouched, touched } =
    useFormikContext<any>();

  const isTouched = touched[name];

  const value = values[name];

  const error: string = useMemo(() => {
    const error = errors[name];
    if (!error) {
      return '';
    }
    if (Array.isArray(error)) {
      return error[0] as string;
    } else {
      return error as string;
    }
  }, [errors, name]);

  const handleBlur = useCallback(() => {
    setFieldTouched(name, true, true);
  }, [name, setFieldTouched]);

  return (
    <Autocomplete
      size={'small'}
      value={value}
      className={cn(styles.autocomplete, className)}
      freeSolo
      renderOption={(params, option) => {
        return (
          <li {...params} key={option.value}>
            {option}
          </li>
        );
      }}
      renderInput={(params) => {
        return (
          <Input
            {...params}
            label={label}
            error={isTouched && Boolean(error)}
            helperText={isTouched && error}
            required={required}
            multiline={multiline}
            InputProps={{ ...params.InputProps, className: inputClassName }}
          />
        );
      }}
      onInputChange={(_, value) => {
        setFieldValue(name, value);
      }}
      onBlur={handleBlur}
      onChange={(event, value: string) => {
        setFieldValue(name, value);
      }}
      options={options}
      disabled={disabled}
      sx={
        disabled
          ? sx
          : {
              ...sx,
              'div > div > div.Mui-disabled': { opacity: 1 },
              'div > div > div.Mui-disabled > svg': { opacity: 0.38 },
            }
      }
    />
  );
};

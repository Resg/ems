import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import { BuildCircle } from '@mui/icons-material';
import ChatIcon from '@mui/icons-material/ChatBubble';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import LoginIcon from '@mui/icons-material/Login';
import LogoutIcon from '@mui/icons-material/Logout';
import PersonIcon from '@mui/icons-material/Person';
import { Menu, MenuItem } from '@mui/material';

import { useGetUserQuery } from '../../services/api/user';
import { UserService } from '../../services/auth';
import { RootState } from '../../store';
import { FastSearch } from '../FastSearch';

import styles from './UserMenu.module.scss';

export const UserMenu = () => {
  const { data: user } = useGetUserQuery({});
  const [anchorEl, setAnchorEl] = useState<Element | null>(null);
  const open = Boolean(anchorEl);
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleExpandClick = (e: React.MouseEvent) => {
    setAnchorEl(e.currentTarget);
  };
  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const handleOpenSettings = () => {
    window.open(`/settings`, openFormInNewTab ? '_blank' : '_self')?.focus();
  };

  const handleOpenSystemSettings = () => {
    window.open(`/system`, openFormInNewTab ? '_blank' : '_self')?.focus();
  };

  return (
    <div className={styles.box}>
      <FastSearch />
      <div className={styles.message}>
        <div className={styles.badge}>
          <ChatIcon />
        </div>
      </div>
      <div className={styles.user}>
        <div className={styles.avatar} />
        <div>
          <div className={styles.userName}>
            {UserService.isLoggedIn() ? user?.personFio : 'Фамилия Имя'}
          </div>
          <div className={styles.userPosition}>{UserService.getUsername()}</div>
        </div>
        <div className={styles.expandBtn} onClick={handleExpandClick}>
          <ExpandMoreIcon />
        </div>
        <Menu open={open} anchorEl={anchorEl} onClose={handleClose}>
          <MenuItem onClick={handleOpenSettings}>
            <PersonIcon className={styles.menuIcon} />
            Профиль
          </MenuItem>
          <MenuItem onClick={handleOpenSystemSettings}>
            <BuildCircle className={styles.menuIcon} />О системе
          </MenuItem>
          {!UserService.isLoggedIn() && (
            <MenuItem onClick={() => UserService.doLogin()}>
              <LoginIcon className={styles.menuIcon} />
              Вход
            </MenuItem>
          )}
          {UserService.isLoggedIn() && (
            <MenuItem onClick={() => UserService.doLogout()}>
              <LogoutIcon className={styles.menuIcon} />
              Выход
            </MenuItem>
          )}
        </Menu>
      </div>
    </div>
  );
};

import React, { ChangeEvent, useRef } from 'react';

import { AttachFile } from '@mui/icons-material';
import { Button } from '@mui/material';

import styles from './styles.module.scss';

export interface AddFileType {
  documentId: number;
  file: FormData;
}

export interface FileLoaderProps {
  documentId: number;
  disabled?: boolean;
  label: string;
  refetchTable: () => void;
  addFile: (value: AddFileType) => void;
}

export const FileLoader: React.FC<FileLoaderProps> = ({
  documentId,
  disabled,
  label,
  refetchTable,
  addFile,
}) => {
  const ref = useRef<HTMLInputElement>(null);

  const handleChange = async (event: ChangeEvent<HTMLInputElement>) => {
    if (event?.target.files?.length) {
      const file = new FormData();
      file.append('file', event?.target?.files[0]);
      file.append('documentId', String(documentId));
      await addFile({ documentId, file });

      refetchTable();
    }
  };

  const handlePick = () => {
    if (ref.current) {
      ref.current.click();
    }
  };

  return (
    <div>
      <input
        type={'file'}
        className={styles.hidden}
        ref={ref}
        onChange={handleChange}
      />
      <Button
        variant="outlined"
        startIcon={<AttachFile />}
        onClick={handlePick}
        disabled={disabled}
      >
        {label}
      </Button>
    </div>
  );
};

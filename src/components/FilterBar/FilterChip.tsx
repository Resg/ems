import React, { useCallback } from 'react';
import cn from 'classnames';

import { Chip } from '@mui/material';

import { useDeleteFilterChipMutation } from '../../services/api/filters';

import styles from './FilterBar.module.scss';

export type FilterChipProps = {
  id: number;
  formKey: string;
  label: string;
  filter: string;
  refetch: () => void;
  onClick: (
    id: number,
    label: string,
    filter: string,
    selected: boolean
  ) => void;
  selected: boolean;
};

export const FilterChip: React.FC<FilterChipProps> = React.memo(
  ({ id, label, filter, formKey, onClick, selected, refetch }) => {
    const [deleteChip] = useDeleteFilterChipMutation();
    const handleDelete = useCallback(async () => {
      await deleteChip({ id });
      refetch();
    }, [deleteChip, id, refetch]);

    const clickChipHandler = useCallback(() => {
      onClick(id, label, filter, selected);
    }, [selected, onClick, id, filter, label]);

    return (
      <Chip
        className={cn(styles.chip, { [styles.chip_checked]: selected })}
        classes={{ deleteIcon: selected ? styles.iconChecked : '' }}
        id={id.toString()}
        label={label}
        onDelete={handleDelete}
        onClick={clickChipHandler}
      />
    );
  }
);

import React, { useCallback, useEffect, useState } from 'react';
import { Form, Formik } from 'formik';
import { createSearchParams, useSearchParams } from 'react-router-dom';
import { object, string } from 'yup';

import CloseIcon from '@mui/icons-material/Close';
import MoreIcon from '@mui/icons-material/MoreVert';
import SaveIcon from '@mui/icons-material/Save';
import SearchIcon from '@mui/icons-material/Search';
import TuneIcon from '@mui/icons-material/Tune';
import {
  Button,
  Divider,
  IconButton,
  OutlinedInput,
  Stack,
} from '@mui/material';

import {
  useCreateChipMutation,
  useGetSavedFiltersQuery,
} from '../../services/api/filters';
import { useAppDispatch } from '../../store';
import { setFilters } from '../../store/filters';
import Card from '../Card';
import Input from '../Input';

import { FiltersStack } from './FiltersStack';

import styles from './FilterBar.module.scss';

export type FilterBarProps = {
  formKey: string;
  placeholder?: string;
  children?: React.ReactNode;
};

interface FormValues {
  globalSearch: string;
  chipName: string;
  filters: {
    documentId: string;
  };
}

const initialValues: FormValues = {
  globalSearch: '',
  chipName: '',
  filters: {
    documentId: '',
  },
};

export const FilterBar: React.FC<FilterBarProps> = React.memo(
  ({ children, formKey, placeholder = 'Поиск' }) => {
    const [isOpen, setIsOpen] = useState(false);
    const [chipWasClicked, setChipClicked] = useState(false);
    const [selectedChipId, setSelectedChipId] = useState<number | undefined>();

    const [searchParams, setSearchParams] = useSearchParams();

    const { data: filterStack, refetch } = useGetSavedFiltersQuery({ formKey });
    const [createChip] = useCreateChipMutation();
    const dispatch = useAppDispatch();

    const validationSchema = object({
      globalSearch: string(),
      chipName: string(),
      documentId: string(),
    });

    const toggleOpened = useCallback((isOpened: boolean) => {
      setIsOpen(isOpened);
    }, []);

    const saveChipParameters = useCallback(
      ({ resetForm, values }: { resetForm: () => void; values: FormValues }) =>
        () => {
          createChip({
            data: {
              formKey,
              name: values.chipName,
              filter: JSON.stringify(values.filters),
            },
          })
            .then(() => {
              refetch();
              resetForm();
            })
            .catch();
        },
      [createChip, formKey, refetch]
    );

    const searchWithChipParameters = useCallback(
      ({ values }: { values: FormValues }) =>
        async () => {
          toggleOpened(false);
          const response = await createChip({
            data: {
              formKey,
              name: values.chipName || '#_Поиск',
              filter: JSON.stringify(values.filters),
            },
          });

          if ('data' in response) {
            setSelectedChipId(response.data.id);
            setSearchParams(createSearchParams({ chipsId: response.data.id }));
            dispatch(setFilters({ documentId: values.filters.documentId }));
            refetch();
          }
        },
      [createChip, dispatch, formKey, refetch, setSearchParams, toggleOpened]
    );

    const chipClick = useCallback(
      ({
          resetForm,
          setFieldValue,
        }: {
          resetForm: () => void;
          setFieldValue: (field: string, value: any) => void;
        }) =>
        (
          currentChipId: number,
          chipName: string,
          chipFilters: string,
          selected = false
        ) => {
          setChipClicked(!selected);
          if (selected) {
            resetForm();
            setSelectedChipId(undefined);
            dispatch(setFilters(JSON.parse(chipFilters).documentId));
            setSearchParams(
              createSearchParams({ chipsId: currentChipId.toString() })
            );
          } else {
            setFieldValue('filters', JSON.parse(chipFilters));
            setFieldValue('chipName', chipName);
            setSelectedChipId(currentChipId);
            dispatch(setFilters({}));
            setSearchParams(createSearchParams({}));
          }
        },
      [dispatch, setSearchParams]
    );

    const globalSearch = useCallback(
      ({ values }: { values: FormValues }) =>
        async () => {
          toggleOpened(false);
          const response = await createChip({
            data: {
              formKey,
              name: '#_Поиск',
              filter: JSON.stringify({
                documentId: values.globalSearch,
              }),
            },
          });

          if ('data' in response) {
            setSelectedChipId(response.data.id);
            setSearchParams(createSearchParams({ chipsId: response.data.id }));
            dispatch(setFilters({ documentId: values.filters.documentId }));
            refetch();
          }
        },
      [createChip, dispatch, formKey, refetch, setSearchParams, toggleOpened]
    );

    useEffect(() => {
      dispatch(setFilters({ [formKey]: {} }));
    }, []);

    useEffect(() => {
      if (
        !chipWasClicked &&
        !selectedChipId &&
        searchParams.get('chipsId') &&
        filterStack?.length
      ) {
        const selectedChip = filterStack.find(
          (chip: any) => `${chip.id}` === searchParams.get('chipsId')
        );
        if (selectedChip) {
          initialValues.filters = JSON.parse(selectedChip.filter);
          initialValues.chipName = selectedChip.name;
          setSelectedChipId(selectedChip.id);
          dispatch(setFilters(JSON.parse(selectedChip.filter)));
          setSearchParams(
            createSearchParams({ chipsId: selectedChip.id.toString() })
          );
        }
      }
    }, [
      searchParams,
      chipWasClicked,
      filterStack,
      selectedChipId,
      dispatch,
      setSearchParams,
    ]);

    return (
      <Formik<FormValues> initialValues={initialValues} onSubmit={() => {}}>
        {({ resetForm, values, setFieldValue, getFieldProps }) => {
          return (
            <div className={styles.container}>
              <Card className={styles.filterBarContainer}>
                <Form>
                  <Stack
                    className={styles.filterBar}
                    direction="row"
                    spacing={1.5}
                    divider={<Divider orientation="vertical" flexItem />}
                  >
                    <OutlinedInput
                      className={styles.input}
                      size="small"
                      placeholder={placeholder}
                      {...getFieldProps('globalSearch')}
                      endAdornment={
                        <IconButton onClick={globalSearch({ values })}>
                          <SearchIcon />
                        </IconButton>
                      }
                    />
                    <Stack
                      direction="row"
                      spacing={1.5}
                      className={styles.filterStack}
                    >
                      <FiltersStack
                        formKey={formKey}
                        chipClick={chipClick({ resetForm, setFieldValue })}
                        selectedChipId={selectedChipId}
                        refetch={refetch}
                      />
                      <IconButton className={styles.tuneBtn}>
                        <MoreIcon />
                      </IconButton>
                    </Stack>
                    <Stack
                      className={styles.moreBtn}
                      direction="row"
                      spacing={1.5}
                    >
                      {isOpen ? (
                        <IconButton
                          className={styles.tuneBtn}
                          onClick={() => setIsOpen(false)}
                        >
                          <CloseIcon />
                        </IconButton>
                      ) : (
                        <IconButton
                          className={styles.tuneBtn}
                          onClick={() => setIsOpen(true)}
                        >
                          <TuneIcon />
                        </IconButton>
                      )}
                    </Stack>
                  </Stack>
                  <Stack
                    className={isOpen ? styles.expanded : styles.collapsed}
                  >
                    <Input
                      id="documentId"
                      label="Штрих-код"
                      className={styles.inputContainer}
                      {...getFieldProps('filters.documentId')}
                    />
                    <div className={styles.filterButtonsContainer}>
                      <div className={styles.parameterSearchContainer}>
                        <Input
                          id="chipName"
                          label="Введите название параметра поиска"
                          {...getFieldProps('chipName')}
                        />
                        <IconButton
                          className={styles.tuneBtn}
                          onClick={saveChipParameters({ resetForm, values })}
                        >
                          <SaveIcon />
                        </IconButton>
                      </div>
                      <div className={styles.filterButtons}>
                        <Button
                          color="primary"
                          variant="contained"
                          onClick={searchWithChipParameters({ values })}
                        >
                          Поиск
                        </Button>
                        <Button color="primary" variant="outlined">
                          Очистить вкладку
                        </Button>
                        <Button color="primary" variant="outlined">
                          Очистить всё
                        </Button>
                      </div>
                    </div>
                  </Stack>
                </Form>
              </Card>
              <div className={styles.contentContainer}>{children}</div>
            </div>
          );
        }}
      </Formik>
    );
  }
);

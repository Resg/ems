import { useCallback, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';

import {
  Cancel,
  Check,
  ChevronRight,
  Close,
  DeleteOutline,
} from '@mui/icons-material';
import {
  Button,
  Card,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Stack,
} from '@mui/material';

import { DataTable } from '../../components/CustomDataGrid/DataTable';
import { FilterBar } from '../../components/FilterBarNew';
import { Popup } from '../../components/Popup';
import { RequestSearchModalCols } from '../../constants/requestSearch';
import { Routes } from '../../constants/routes';
import { useAddDocumentRequestMutation } from '../../services/api/document';
import {
  useAddLinkRequestMutation,
  useGetCommonDataQuery,
  useGetRequestsQuery,
} from '../../services/api/request';
import { RootState } from '../../store';

import styles from './styles.module.scss';

type RequestSearchModalProps = {
  open: boolean;
  onClose: () => void;
  id: number;
  requestId?: number;
  refetchTable: () => void;
};

export const RequestSearchModal: React.FC<RequestSearchModalProps> = ({
  open,
  onClose,
  id,
  requestId,
  refetchTable,
}) => {
  const filters = useSelector(
    (state: RootState) => state.filters.data['request-search-modal']
  );

  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const [popup, setPopup] = useState(false);
  const [secondPopup, setSecondPopup] = useState(false);
  const { data: reqData } = useGetCommonDataQuery(
    { id: requestId as number },
    { skip: !requestId }
  );
  const [controller, setController] = useState([new AbortController()]);

  const {
    data: fetchedData,
    currentData,
    refetch,
  } = useGetRequestsQuery(
    {
      searchParameters: {
        elasticSearch: filters?.elasticSearch || undefined,
        technologyId: reqData?.technology,
        serviceId: reqData?.radioService,
        bandReceiveId: reqData?.frequencyRangeRx,
        bandTransmitId: reqData?.frequencyRangeTx,
        requestId: reqData?.id,
      },
      page: page,
      size: perPage,
      useCount: true,
      abortController: controller[controller.length - 1],
    },
    { skip: !filters, refetchOnMountOrArgChange: true }
  );

  const [addLinkRequest] = useAddLinkRequestMutation();
  const [addDocumentRequest] = useAddDocumentRequestMutation();

  const data = filters ? fetchedData : currentData;

  const handleSetPage = useCallback(
    (value: number) => {
      controller[controller.length - 1].abort();
      setController([...controller, new AbortController()]);
      setPage(value);
    },
    [controller]
  );

  const selectedSolution = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const rows = useMemo(() => {
    return data?.data || [];
  }, [data, filters]);

  const handleAdd = useCallback(
    async (el: boolean) => {
      if (selectedSolution) {
        await addLinkRequest({
          requestId,
          id: selectedSolution[0],
          copyAgreementLetters: el,
        });
        onClose();
        setPopup(false);
        setSecondPopup(false);
        setPage(1);
      }
    },
    [selectedSolution, requestId]
  );

  const addDocRequest = useCallback(async () => {
    await addDocumentRequest({
      requestIds: selectedSolution,
      documentId: id,
    });
    refetchTable();
    onClose();
  }, [selectedSolution, requestId, id]);

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );
  const handleDetails = useCallback(() => {
    window
      .open(
        Routes.REQUESTS.REQUEST.replace(':id', selectedSolution[0]),
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  }, [selectedSolution, openFormInNewTab]);

  const handleRowDoubleClick = useCallback(
    (e: { id: number }) => {
      if (e.id) {
        window
          .open(
            Routes.REQUESTS.REQUEST.replace(':id', e.id),
            openFormInNewTab ? '_blank' : '_self'
          )
          ?.focus();
      }
    },
    [openFormInNewTab]
  );

  const handleCloseDialog = useCallback((e: React.MouseEvent) => {
    e.stopPropagation();
    setPopup(false);
    setSecondPopup(false);
  }, []);

  const handleApproveModal = useCallback((e: React.MouseEvent, el: boolean) => {
    e.stopPropagation();
    setPopup(false);
    setSecondPopup(el);
  }, []);

  const bar = (
    <Stack
      direction="row"
      justifyContent="flex-end"
      spacing={1.5}
      style={{ width: '100%' }}
    >
      <Button
        variant="outlined"
        startIcon={<ChevronRight />}
        onClick={handleDetails}
        disabled={selectedSolution.length !== 1}
      >
        Подробно
      </Button>

      <Button
        variant="contained"
        startIcon={<Check />}
        onClick={() => (requestId ? setPopup(true) : addDocRequest())}
        disabled={!selectedSolution.length}
      >
        Выбрать
      </Button>
      <Button variant="outlined" startIcon={<Close />} onClick={onClose}>
        Отмена
      </Button>
    </Stack>
  );

  return (
    <Popup
      open={Boolean(open)}
      onClose={onClose}
      title={'Поиск заявок'}
      height={820}
      centred
      bar={bar}
    >
      <div className={styles.popupContent}>
        <FilterBar
          formKey={'request-search-modal'}
          placeholder="Номер заявки"
          refetchSearch={() => refetch()}
        ></FilterBar>
        <div className={styles.searchResultText}>РЕЗУЛЬТАТ ПОИСКА</div>
        <DataTable
          formKey={'search_for_applications'}
          cols={RequestSearchModalCols}
          rows={rows}
          onRowSelect={setSelected}
          onRowDoubleClick={handleRowDoubleClick}
          pagerProps={{
            page: page,
            setPage: handleSetPage,
            perPage: perPage,
            setPerPage: setPerPage,
            total: data?.totalCount || 0,
          }}
        />
        <Dialog open={popup}>
          <Card>
            <DialogTitle>Добавление заявок</DialogTitle>
            <DialogContent>
              Частотная часть (строки ПЧТР) и Решения ГКРЧ будут изменены в
              текущей заявке, предыдущие данные будут потеряны. Продолжить?
            </DialogContent>

            <DialogActions>
              <Button
                variant="contained"
                endIcon={<Cancel />}
                onClick={(event) => handleApproveModal(event, true)}
              >
                Да
              </Button>
              <Button
                variant="outlined"
                endIcon={<DeleteOutline />}
                onClick={(event) => handleApproveModal(event, false)}
              >
                Нет
              </Button>
            </DialogActions>
          </Card>
        </Dialog>
        <Dialog open={secondPopup} onClose={handleCloseDialog}>
          <Card>
            <DialogTitle>Добавление заявок</DialogTitle>

            <DialogContent>Прикрепить согласующие письма?</DialogContent>
            <DialogActions>
              <Button
                variant="contained"
                endIcon={<Cancel />}
                onClick={() => handleAdd(true)}
              >
                Да
              </Button>
              <Button
                variant="outlined"
                endIcon={<DeleteOutline />}
                onClick={() => handleAdd(false)}
              >
                Нет
              </Button>
            </DialogActions>
          </Card>
        </Dialog>
      </div>
    </Popup>
  );
};

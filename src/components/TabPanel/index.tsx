import React from 'react';

interface TabPanelProps {
  index: number | string;
  value: number | string;
  children?: React.ReactNode;
  className?: string;
}

export const TabPanel = (props: TabPanelProps) => {
  const { children, index, value, className } = props;

  return (
    <div role="tabpanel" hidden={value !== index} className={className}>
      {value === index && children}
    </div>
  );
};

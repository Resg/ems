import React, { useCallback, useState } from 'react';
import { useSnackbar } from 'notistack';
import { useSelector } from 'react-redux';

import { ContentCopy, InsertDriveFileOutlined } from '@mui/icons-material';
import { IconButton } from '@mui/material';
import { Button, Card, Dialog, Stack } from '@mui/material';

import { useGetFileInfoMutation } from '../../services/api/files';
import { _axios } from '../../services/axios';
import { RootState } from '../../store';
import { copyToClipboard } from '../../utils/copyToClipboard';
import { DocIcon, ExcelColor, Image, PdfIcon, Rtf, Sig, Zip } from '../Icons';

import styles from './styles.module.scss';

export interface FileLinkProps {
  name: string;
  rowId: number;
}

const getIcon = (name: string) => {
  if (name) {
    const dot = name.lastIndexOf('.');
    const ext = name.slice(dot + 1);

    switch (ext) {
      case 'docx':
      case 'doc':
        return <DocIcon />;
      case 'xlsx':
      case 'xls':
        return <ExcelColor />;
      case 'pdf':
        return <PdfIcon />;
      case 'rtf':
        return <Rtf />;
      case 'sig':
        return <Sig />;
      case 'jpeg':
      case 'jpg':
      case 'png':
        return <Image />;
      case '7z':
      case 'zip':
        return <Zip />;
      default:
        return <InsertDriveFileOutlined />;
    }
  } else {
    return null;
  }
};

export const FileLink = (props: FileLinkProps) => {
  const [getFile] = useGetFileInfoMutation();
  const [openModal, setOpenModal] = useState(false);
  const { name, rowId } = props;
  const ext = getIcon(name);
  const { enqueueSnackbar } = useSnackbar();
  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const handleClick = useCallback(async () => {
    const response = await getFile({ id: Number(rowId.toString()) });
    if ('data' in response) {
      if (response?.data?.canOpenInR7Office)
        window.open(
          `/file_editor/${rowId.toString()}`,
          openFormInNewTab ? '_blank' : '_self'
        );
      else {
        setOpenModal(true);
      }
    }
  }, [rowId?.toString(), openFormInNewTab]);

  const handleModal = async () => {
    const response = await getFile({ id: Number(rowId.toString()) });
    if ('data' in response) {
      _axios
        .get(response.data.url, { responseType: 'blob' })
        .then((file: any) => {
          const url = URL.createObjectURL(file.data as Blob);
          const link = document.createElement('a');
          link.href = url;
          link.download = response.data.title;
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
          URL.revokeObjectURL(url);
          setOpenModal(false);
        });
    }
  };

  const onCopy = useCallback(() => {
    copyToClipboard(name, () =>
      enqueueSnackbar('Скопировано в буфер обмена', {
        autoHideDuration: 3000,
        variant: 'info',
        anchorOrigin: { horizontal: 'right', vertical: 'top' },
      })
    );
  }, [name]);

  return (
    <>
      <div className={styles.linkBox}>
        <div className={styles.link} onClick={handleClick}>
          {ext}
          <div className={styles.name}>{name}</div>
        </div>
        <IconButton size="small" onClick={onCopy} className={styles.copyIcon}>
          <ContentCopy fontSize="inherit" />
        </IconButton>
      </div>
      <Dialog open={openModal}>
        <Card className={styles.card}>
          <div className={styles.dialogText}>
            К сожалению, данный файл невозможно открыть.
            <br />
            Хотите ли Вы его скачать?
          </div>
          <Stack
            direction="row"
            justifyContent="flex-end"
            spacing={1.5}
            width="100%"
          >
            <Button variant="contained" onClick={() => handleModal()}>
              Да
            </Button>
            <Button variant="outlined" onClick={() => setOpenModal(false)}>
              Нет
            </Button>
          </Stack>
        </Card>
      </Dialog>
    </>
  );
};

import React, { useCallback, useMemo } from 'react';
import cn from 'classnames';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { Checkbox } from '@mui/material';

import styles from './styles.module.scss';

export interface CheckboxInputProps {
  name: string;
  label: string;
  className?: string;
  disabled?: boolean;
  title?: string;
  text?: string;
}

export const CheckboxSettingsInput: React.FC<CheckboxInputProps> = ({
  name,
  className,
  label,
  disabled,
  title,
  text,
}) => {
  const { values, setFieldValue } = useFormikContext<any>();

  const handleChange = useCallback(
    (_: React.ChangeEvent, checked: boolean) => {
      setFieldValue(name, checked);
    },
    [name, setFieldValue]
  );

  const value = useMemo(() => Boolean(get(values, name)), [values, name]);

  return (
    <div className={cn(styles.container, className)}>
      <Checkbox
        name={name}
        checked={value}
        disabled={disabled}
        onChange={handleChange}
      />
      <div className={styles.label}>
        {title}
        <div className={styles.text}>{text}</div>
      </div>
    </div>
  );
};

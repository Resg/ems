import React from 'react';

import { Checkbox as CheckboxBase, CheckboxProps } from '@mui/material';

export const Checkbox: React.FC<
  CheckboxProps & { notEditableField?: boolean }
> = ({ disabled, notEditableField, ...restProps }) => {
  return (
    <CheckboxBase disabled={notEditableField || disabled} {...restProps} />
  );
};

import React, { useCallback } from 'react';
import { useSelector } from 'react-redux';

import { Cancel, Delete, DeleteOutline } from '@mui/icons-material';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@mui/material';

import { RootState, useAppDispatch } from '../../../store';
import { setDeleteDialogOpen, setError } from '../../../store/utils';
import Card from '../../Card';
import { ToolButtonProps } from '../../ToolsPanel';

export interface DeleteToolButtonProps extends ToolButtonProps {
  title: string;
  description: string;
  onConfirm: () => void;
  disabled: boolean;
  isHasSign?: boolean;
  setSelected: (value: any) => void;
}

export const DeleteButton: React.FC<DeleteToolButtonProps> = ({
  title,
  description,
  onConfirm,
  disabled,
  isHasSign,
  setSelected,
}) => {
  const open = useSelector(
    (state: RootState) => state.utils.isDeleteDialogOpen
  );
  const dispatch = useAppDispatch();
  const id = onConfirm.toString();

  const handleOpen = useCallback(() => {
    if (isHasSign === false) {
      dispatch(setDeleteDialogOpen({ [id]: true }));
    } else {
      dispatch(
        setError({
          message:
            'По документу есть находящиеся на выполнении задачи "На визирование" и/или "На подписание. Удаление запрещено.',
        })
      );
      setSelected({});
    }
  }, [isHasSign]);

  const handleClose = useCallback((e: React.MouseEvent) => {
    e.stopPropagation();
    dispatch(setDeleteDialogOpen({ [id]: false }));
  }, []);

  const handleConfirm = useCallback(() => {
    onConfirm();
    dispatch(setDeleteDialogOpen({ [id]: false }));
  }, [onConfirm]);

  return (
    <>
      <Button
        variant="outlined"
        startIcon={<DeleteOutline />}
        onClick={handleOpen}
        disabled={disabled}
      >
        удалить
      </Button>

      <Dialog open={open[id]}>
        <Card>
          <DialogTitle>{title}</DialogTitle>
          <DialogContent>{description}</DialogContent>
          <DialogActions>
            <Button
              color={'error'}
              variant={'contained'}
              endIcon={<Delete />}
              onClick={handleConfirm}
            >
              Удалить
            </Button>
            <Button
              variant={'outlined'}
              endIcon={<Cancel />}
              onClick={handleClose}
            >
              Отмена
            </Button>
          </DialogActions>
        </Card>
      </Dialog>
    </>
  );
};

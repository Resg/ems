import React from 'react';

import { LoaderLogo } from './LoaderLogo.jsx';

interface LogoProps {
  loading?: boolean;
  size?: number;
}

export const Logo: React.FC<LogoProps> = ({ loading, size = 40 }) => {
  return loading ? (
    <LoaderLogo />
  ) : (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={size}
      height={size}
      viewBox="0 0 40 40"
      fill="none"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M40 20C40 31.0457 31.0457 40 20 40C8.95431 40 0 31.0457 0 20C0 8.95431 8.95431 0 20 0C31.0457 0 40 8.95431 40 20ZM19.6901 35.1221L13.8811 11.8873L11.8529 20H11.8539V20.0098L11.3515 22.0193H5.00839C5.98216 29.3181 12.1611 34.9708 19.6901 35.1221ZM4.87476 20L9.8248 20L13.2038 6.48394C8.26357 8.97289 4.87477 14.0907 4.87476 20ZM14.4217 5.93665C16.1478 5.25142 18.0299 4.87476 20 4.87476C27.6766 4.87476 34.0175 10.5936 34.9946 18.0034L22.4921 18.0034L21.9897 20.0128V20.0167L19.9658 28.1126L14.4217 5.93665ZM35.1252 20.0227C35.1131 28.2851 28.476 34.9944 20.2415 35.1233L24.0167 20.0227L35.1252 20.0227Z"
        fill="url(#paint0_linear_101_2)"
      />
      <defs>
        <linearGradient
          id="paint0_linear_101_2"
          x1="39.3027"
          y1="-0.750055"
          x2="0.370826"
          y2="40.4416"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#2973E0" />
          <stop offset="1" stopColor="#8BD8E2" />
        </linearGradient>
      </defs>
    </svg>
  );
};

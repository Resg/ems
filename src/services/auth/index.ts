import Keycloak from 'keycloak-js';

import { KEYCLOACK_JSON } from '../../config';

export const authClient = new Keycloak(KEYCLOACK_JSON);

/**
 * Initializes Keycloak instance and calls the provided callback function if successfully authenticated.
 *
 * @param onAuthenticatedCallback
 */
const initKeycloak = (onAuthenticatedCallback: () => any) => {
  authClient
    .init({
      onLoad: 'check-sso',
      silentCheckSsoRedirectUri:
        window.location.origin + '/silent-check-sso.html',
      pkceMethod: 'S256',
    })
    .then((authenticated) => {
      if (!authenticated) {
        console.log('user is not authenticated..!');
      }
      onAuthenticatedCallback();
    })
    .catch(console.error);
};

const doLogin = authClient.login;

const doLogout = authClient.logout;

const getToken = () => authClient.token;

const isLoggedIn = () => Boolean(authClient.token);

const updateToken = (successCallback: () => any) =>
  authClient.updateToken(5).then(successCallback).catch(doLogin);

const getUsername = () => authClient.tokenParsed?.preferred_username;

const hasRole = (roles: string[]) =>
  roles.some((role) => authClient.hasRealmRole(role));

export const UserService = {
  initKeycloak,
  doLogin,
  doLogout,
  isLoggedIn,
  getToken,
  updateToken,
  getUsername,
  hasRole,
};

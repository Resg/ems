import axios, { AxiosError } from 'axios';

import { BaseQueryFn } from '@reduxjs/toolkit/query';

import { API_URL } from '../../config';
import { RequestOptions } from '../../types/common';
import { UserService } from '../auth';

export const HttpMethods = {
  GET: 'GET',
  POST: 'POST',
  DELETE: 'DELETE',
};

export const getAxiosClient = (baseURL: string) => {
  return axios.create({
    baseURL,
  });
};

export const _axios = getAxiosClient(API_URL);

export const initAxios = () => {
  _axios.interceptors.request.use((config) => {
    if (UserService.isLoggedIn()) {
      const cb = () => {
        config.headers = config.headers || {};
        config.headers.Authorization = `Bearer ${UserService.getToken()}`;
        return Promise.resolve(config);
      };
      return UserService.updateToken(cb);
    }
  });
};

export const axiosBaseQuery =
  (): BaseQueryFn<RequestOptions> => async (requestOpts) => {
    try {
      const response = await _axios({
        ...requestOpts,
      });
      return response;
    } catch (axiosError) {
      const err = axiosError as AxiosError;
      return {
        error: {
          status: err.response?.status,
          data: err.response?.data,
          request: {
            ...requestOpts,
            url: err.config?.baseURL + requestOpts.url,
          },
          showError: err.config?.baseURL ? true : false,
        },
      };
    }
  };

import { createApi } from '@reduxjs/toolkit/query/react';

import { ListResponse } from '../../../types/api';
import {
  ConclusionAdditional,
  ConclusionContractorType,
  ConclusionReviewFormData,
  CreateAdditionalConclusionData,
} from '../../../types/conclusions';
import {
  AdditionalConclusionData,
  ConclusionAppendice,
  ConclusionConditions,
  ConclusionCreateData,
  ConclusionData,
  ConclusionSearchType,
  ConclusionType,
  ConclusionUpdateData,
  UpdateAdditionalConclusionData,
} from '../../../types/conclusions';
import { axiosBaseQuery } from '../../axios';

export const conclusionsApi = createApi({
  reducerPath: 'conclusionApi',
  baseQuery: axiosBaseQuery(),
  tagTypes: ['APPENDICES'],
  endpoints: (builder) => ({
    getConclusionData: builder.query<ConclusionData, { id?: number }>({
      query: ({ id }) => ({
        url: `/conclusions/${id}`,
        method: 'GET',
      }),
    }),
    updateConclusion: builder.mutation<
      any,
      { id: number; data: ConclusionUpdateData }
    >({
      query: ({ id, data }) => ({
        url: `/conclusions/${id}`,
        method: 'PUT',
        data: data,
      }),
    }),
    createConclusion: builder.mutation<any, { data: ConclusionCreateData }>({
      query: ({ data }) => ({
        url: `/conclusions`,
        method: 'POST',
        data: data,
      }),
    }),
    getConclusionAppendix: builder.query<any, { id: string }>({
      query: ({ id }) => ({
        url: `/conclusions/appendices/${id}`,
        method: 'GET',
      }),
      providesTags: ['APPENDICES'],
    }),
    createConclusionAppendix: builder.mutation<any, { data: FormData }>({
      query: ({ data }) => ({
        url: '/conclusions/appendices',
        method: 'POST',
        data,
      }),
      invalidatesTags: ['APPENDICES'],
    }),
    updateConclusionAppendix: builder.mutation<
      any,
      { data: FormData; id: number }
    >({
      query: ({ data, id }) => ({
        url: `/conclusions/appendices/${id}`,
        method: 'PUT',
        data,
      }),
      invalidatesTags: ['APPENDICES'],
    }),
    getConclusions: builder.query<
      ListResponse<ConclusionType[]>,
      ConclusionSearchType
    >({
      query: ({ searchParameters, page, size }) => ({
        url: '/conclusions/search',
        method: 'POST',
        data: {
          searchParameters,
          page,
          size,
          useCount: true,
        },
      }),
    }),
    createConclusionsCard: builder.mutation<void, any>({
      query: (data = {}) => ({
        url: '/conclusions/subsequent-document-formation',
        method: 'POST',
        data: data,
      }),
    }),
    getConclusionAppendices: builder.query<
      ListResponse<ConclusionAppendice[]>,
      { id: number; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ id, page = 1, size = 10, useCount = true }) => ({
        url: `/conclusions/${id}/appendices`,
        params: { page, size, useCount },
        method: 'GET',
      }),
    }),
    deleteAppendice: builder.mutation<any, { id: number }>({
      query: ({ id }) => ({
        url: `/conclusions/appendices/${id}`,
        method: 'DELETE',
      }),
    }),
    getConclusionAdditionals: builder.query<
      ListResponse<ConclusionAdditional[]>,
      { id: number }
    >({
      query: ({ id }) => ({
        url: `/conclusions/${id}/additional-conclusion-conditions`,
        method: 'GET',
      }),
    }),
    deleteAdditionals: builder.mutation<any, { ids: string[] }>({
      query: ({ ids }) => ({
        url: `/conclusions/additional-conclusion-conditions/${ids.join(',')}`,
        method: 'DELETE',
      }),
    }),
    recountAdditionals: builder.mutation<any, { id: number; ids: number[] }>({
      query: ({ id, ids }) => ({
        url: `/conclusions/${id}/additional-conclusion-conditions/${ids.join(
          ','
        )}/ordinals-number-recount`,
        method: 'POST',
      }),
    }),
    getAdditionalConclusionData: builder.query<
      AdditionalConclusionData,
      { id: number }
    >({
      query: ({ id }) => ({
        url: `/conclusions/additional-conclusion-conditions/${id}`,
        method: 'GET',
      }),
    }),
    updateAdditionalConclusion: builder.mutation<
      any,
      { id: number; data: UpdateAdditionalConclusionData }
    >({
      query: ({ id, data }) => ({
        url: `/conclusions/additional-conclusion-conditions/${id}`,
        method: 'PUT',
        data: data,
      }),
    }),
    addAdditionalToConclusion: builder.mutation<
      any,
      { id: number; additionsIds: number[] }
    >({
      query: ({ id, additionsIds }) => ({
        url: `/conclusions/${id}/additional-conclusion-conditions`,
        method: 'POST',
        data: { ids: additionsIds },
      }),
    }),
    createAdditionalCondition: builder.mutation<
      any,
      { conclusionId: number; dataId: CreateAdditionalConclusionData }
    >({
      query: ({ conclusionId, dataId }) => ({
        url: `/conclusions/${conclusionId}/additional-conclusion-conditions/added-by-set`,
        method: 'POST',
        data: dataId,
      }),
    }),
    getConclusionFiles: builder.query<any, { id: string }>({
      query: ({ id }) => ({
        url: `/files/conclusion/${id}`,
        method: 'GET',
      }),
    }),
    getConclusionContractors: builder.query<
      ConclusionContractorType[],
      { conclusionId: string | number; defaultsForPermission?: boolean }
    >({
      query: ({ conclusionId, defaultsForPermission = true }) => ({
        url: `/conclusions/${conclusionId}/contractors`,
        method: 'GET',
        params: { defaultsForPermission },
      }),
    }),

    getConclusionsForForm: builder.query<
      ListResponse<ConclusionReviewFormData[]>,
      ConclusionSearchType
    >({
      query: ({ searchParameters, page, size, sort }) => ({
        url: '/conclusions/view/search',
        method: 'POST',
        data: {
          searchParameters,
          page,
          size,
          useCount: true,
          sort,
        },
      }),
    }),
    getConclusionConditions: builder.query<
      ListResponse<ConclusionConditions[]>,
      { conclusionId: number; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ conclusionId, page = 1, size = 10, useCount = true }) => ({
        url: `/conclusions/${conclusionId}/additional-conclusion-conditions`,
        params: { page, size, useCount },
        method: 'GET',
      }),
    }),
  }),
});

export const {
  useGetConclusionDataQuery,
  useUpdateConclusionMutation,
  useCreateConclusionMutation,
  useGetConclusionAppendixQuery,
  useUpdateConclusionAppendixMutation,
  useCreateConclusionAppendixMutation,
  useGetConclusionsQuery,
  useDeleteAppendiceMutation,
  useGetConclusionAppendicesQuery,
  useGetConclusionAdditionalsQuery,
  useDeleteAdditionalsMutation,
  useRecountAdditionalsMutation,
  useGetAdditionalConclusionDataQuery,
  useUpdateAdditionalConclusionMutation,
  useAddAdditionalToConclusionMutation,
  useCreateAdditionalConditionMutation,
  useCreateConclusionsCardMutation,
  useGetConclusionFilesQuery,
  useGetConclusionContractorsQuery,
  useGetConclusionsForFormQuery,
  useGetConclusionConditionsQuery,
} = conclusionsApi;

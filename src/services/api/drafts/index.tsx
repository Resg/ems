import { createApi } from '@reduxjs/toolkit/query/react';

import { ListResponse } from '../../../types/api';
import { IncomingDraftsData } from '../../../types/drafts';
import { axiosBaseQuery } from '../../axios';

export const draftsApi = createApi({
  reducerPath: 'draftsApi',
  baseQuery: axiosBaseQuery(),
  tagTypes: ['DRAFTS'],
  endpoints: (builder) => ({
    getListDraftTasks: builder.query<
      ListResponse<IncomingDraftsData[]>,
      { page?: number; size?: number; useCount?: boolean; sort?: string }
    >({
      query: ({ page = 1, size = 10, useCount = true }) => ({
        url: `/tasks/document/created?FilterDto`,
        params: { page, size, useCount },
        method: 'GET',
      }),
      providesTags: ['DRAFTS'],
    }),
  }),
});

export const { useGetListDraftTasksQuery } = draftsApi;

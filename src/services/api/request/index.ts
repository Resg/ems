import { createApi } from '@reduxjs/toolkit/query/react';

import { ListResponse, SearchDataType } from '../../../types/api';
import { RequestType } from '../../../types/commonRequestForm';
import { LinkRequests } from '../../../types/linkRequests';
import {
  AnnulledConclusionData,
  AnnulledPermissions,
  ConclusionClient,
  ConclusionData,
  ConclusionDocumentsData,
  CoordinationCoutry,
  CoordinationCoutryParams,
  EditStageData,
  MPZData,
  PCTRData,
  RequestData,
  RequestDocument,
  RequestStage,
  StageCreateData,
  TabParameters,
  UpdateStageData,
} from '../../../types/requests';
import { RequestSolution } from '../../../types/solution';
import { axiosBaseQuery } from '../../axios';

export const requestAPI = createApi({
  reducerPath: 'requestApi',
  baseQuery: axiosBaseQuery(),
  tagTypes: [
    'DOCUMENTS',
    'INITIAL_DOCUMENTS',
    'SOLUTION',
    'CONCLUSION',
    'ZE_FOR_CANCELLATION',
    'MPZ',
    'COORDINATION_COUNTRY',
    'COORDINATION_COUNTRIES',
    'REQUESTS',
    'ANNULLED_PERMISSIONS',
    'PERMISSIONS_LIST',
    'LINKED_TASK',
  ],
  endpoints: (builder) => ({
    getRequestStages: builder.query<
      ListResponse<RequestStage[]>,
      { id: number; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ id, page = 1, size = 10, useCount = true }) => ({
        url: `/requests/${id}/stages`,
        params: { page, size, useCount },
        method: 'GET',
      }),
    }),
    deleteStage: builder.mutation<any, { id: number }>({
      query: ({ id }) => ({
        url: `/requests/stages/${id}`,
        method: 'DELETE',
      }),
    }),
    closeStage: builder.mutation<any, { id: number }>({
      query: ({ id }) => ({
        url: `/requests/stages/${id}/closed`,
        method: 'POST',
        data: {},
      }),
    }),
    getRequest: builder.query<RequestData, { id: number }>({
      query: ({ id }) => ({
        url: `/requests/${id}`,
        method: 'GET',
      }),
    }),
    getStageDetail: builder.query<RequestStage, { id: number }>({
      query: ({ id }) => ({
        url: `/requests/stages/${id}/`,
        method: 'GET',
      }),
    }),
    getStageCreateData: builder.query<
      ListResponse<StageCreateData[]>,
      { reqIds: string[] | number[] }
    >({
      query: ({ reqIds }) => ({
        url: `/requests/request/stages?requestIds=${reqIds.join(
          ','
        )}&sort=stageName`,
        method: 'GET',
      }),
    }),
    getCalculatedPlanDate: builder.query<
      { planDate: string },
      {
        startDate: string;
        controlPeriod: string | number;
        controlPeriodType: string;
      }
    >({
      query: ({ startDate, controlPeriod, controlPeriodType }) => ({
        url: `/requests/stages/culculate-plan-date`,
        params: { startDate, controlPeriod, controlPeriodType },
        method: 'GET',
      }),
    }),
    updateStage: builder.mutation<any, { stageData: UpdateStageData }>({
      query: ({ stageData }) => ({
        url: `/requests/stages`,
        method: 'PUT',
        data: stageData,
      }),
    }),
    createStage: builder.mutation<any, { stageData: EditStageData }>({
      query: ({ stageData }) => ({
        url: `/requests/stages`,
        method: 'POST',
        data: stageData,
      }),
    }),
    createRequest: builder.mutation<RequestType, RequestType>({
      query: (data) => ({
        url: '/requests/create',
        method: 'POST',
        data,
      }),
    }),
    saveRequest: builder.mutation<
      RequestType,
      { data: RequestType; id: number }
    >({
      query: ({ data, id }) => ({
        url: `/requests/${id}/common`,
        method: 'POST',
        data,
      }),
    }),
    getRequestInitiatingDocuments: builder.query<
      ListResponse<RequestDocument[]>,
      { id: number }
    >({
      query: ({ id }) => ({
        url: `/requests/${id}/initiating-documents`,
        method: 'GET',
      }),
      providesTags: ['INITIAL_DOCUMENTS'],
    }),
    getRequestDocuments: builder.query<
      ListResponse<RequestDocument[]>,
      { id: number }
    >({
      query: ({ id }) => ({
        url: `/requests/${id}/documents?objectType=REQUEST`,
        method: 'GET',
      }),
      providesTags: ['DOCUMENTS'],
    }),
    deleteInitialDocument: builder.mutation<
      void,
      { id: number; initialIds: number[] }
    >({
      query: ({ id, initialIds }) => ({
        url: `/requests/${id}/initiating-documents?documentIds=${initialIds.join(
          ', '
        )}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['INITIAL_DOCUMENTS'],
    }),

    deleteDocument: builder.mutation<
      void,
      { id: number; documentIds: number[] }
    >({
      query: ({ id, documentIds }) => ({
        url: `/requests/${id}/documents?documentIds=${documentIds.join(', ')}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['DOCUMENTS'],
    }),
    makeDocumentMaterial: builder.mutation<
      void,
      { id: number; documentId: number }
    >({
      query: ({ id, documentId }) => ({
        url: `/requests/${id}/documents/${documentId}/make-materials`,
        method: 'POST',
      }),
      invalidatesTags: ['INITIAL_DOCUMENTS'],
    }),
    addDocumentsToRequest: builder.mutation<
      void,
      { id: number; documentIds: number[]; isInitialDocument?: boolean }
    >({
      query: ({ id, documentIds, isInitialDocument = false }) => ({
        url: `/requests/${id}/documents`,
        method: 'POST',
        data: { documentIds, isInitialDocument },
      }),
      invalidatesTags: (result, error, arg) => {
        return ['INITIAL_DOCUMENTS', 'DOCUMENTS'];
      },
    }),
    makeInitial: builder.mutation<void, { id: number; documentIds: number[] }>({
      query: ({ id, documentIds }) => ({
        url: `/requests/${id}/initiating-documents`,
        method: 'POST',
        data: { documentIds },
      }),
      invalidatesTags: ['INITIAL_DOCUMENTS'],
    }),
    confirmRequest: builder.mutation<void, { id: number }>({
      query: ({ id }) => ({
        url: `/requests/${id}/documents/add-to-case`,
        method: 'POST',
      }),
    }),
    getSolutions: builder.query<
      ListResponse<RequestSolution[]>,
      { id: number; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ id, page = 1, size = 10, useCount = true }) => ({
        url: `/requests/${id}/gkrh-solutions`,
        params: { page, size, useCount },
        method: 'GET',
      }),
      providesTags: ['SOLUTION'],
    }),
    deleteSolution: builder.mutation<
      void,
      { requestId: number; solutionId: number }
    >({
      query: ({ requestId, solutionId }) => ({
        url: `/requests/${requestId}/gkrh-solutions/${solutionId}`,
        method: 'DELETE',
      }),
      invalidatesTags: [{ type: 'SOLUTION' }],
    }),
    addSolution: builder.mutation<
      void,
      { requestId: number; solutionId: number }
    >({
      query: ({ requestId, solutionId }) => ({
        url: `/requests/${requestId}/gkrh-solutions/${solutionId}`,
        method: 'POST',
      }),
      invalidatesTags: [{ type: 'SOLUTION' }],
    }),
    getConclusion: builder.query<
      ListResponse<ConclusionData[]>,
      { id: number; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ id, page = 1, size = 10, useCount = true }) => ({
        url: `/requests/${id}/conclusion`,
        params: { page, size, useCount },
        method: 'GET',
      }),
      providesTags: ['CONCLUSION'],
    }),
    deleteConclusion: builder.mutation<void, { id: number }>({
      query: ({ id }) => ({
        url: `/conclusions/${id}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['CONCLUSION'],
    }),
    getConclusionDocuments: builder.query<
      ListResponse<ConclusionDocumentsData[]>,
      { id: number; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ id, page = 1, size = 10, useCount = true }) => ({
        url: `/conclusions/${id}/documents`,
        params: { page, size, useCount },
        method: 'GET',
      }),
    }),
    deleteConclusionDocuments: builder.mutation<void, { id: number }>({
      query: ({ id }) => ({
        url: `/conclusions/documents/${id}`,
        method: 'DELETE',
      }),
    }),
    getConclusionClients: builder.query<
      ListResponse<ConclusionClient[]>,
      { id: number }
    >({
      query: ({ id }) => ({
        url: `/requests/${id}/conclusion-clients`,
        method: 'GET',
      }),
    }),
    getConclusionSigners: builder.query<
      { id: number; name: string },
      { id: number }
    >({
      query: ({ id }) => ({
        url: `/requests/${id}/conclusion-signer`,
        method: 'GET',
      }),
    }),
    getCommonData: builder.query<RequestType, { id: number }>({
      query: ({ id }) => ({
        url: `/requests/${id}`,
        method: 'GET',
      }),
    }),
    getConclusionNumber: builder.query<{ number: string }, { id: number }>({
      query: ({ id }) => ({
        url: `/requests/${id}/conclusion-number`,
        method: 'GET',
      }),
    }),
    getAnnulledConclusion: builder.query<
      ListResponse<AnnulledConclusionData[]>,
      { id: number; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ id, page = 1, size = 10, useCount = true }) => ({
        url: `/requests/${id}/should-be-annulled-conclusions`,
        params: { page, size, useCount },
        method: 'GET',
      }),
      providesTags: ['ZE_FOR_CANCELLATION'],
    }),
    deleteAnnulledConclusion: builder.mutation<
      void,
      { id: number; conclusinId: number }
    >({
      query: ({ id, conclusinId }) => ({
        url: `/requests/${id}/should-be-annulled-conclusions/${conclusinId}`,
        method: 'DELETE',
      }),
      invalidatesTags: [{ type: 'ZE_FOR_CANCELLATION' }],
    }),
    addAnnulledConclusion: builder.mutation<
      void,
      { requestId: number; selectedConclusions: number[] }
    >({
      query: ({ requestId, selectedConclusions }) => ({
        url: `/requests/${requestId}/should-be-annulled-conclusions`,
        method: 'POST',
        data: { conclusionIds: [...selectedConclusions] },
      }),
      invalidatesTags: [{ type: 'ZE_FOR_CANCELLATION' }],
    }),
    getTabParameters: builder.query<TabParameters, { id: number }>({
      query: ({ id }) => ({
        url: `/requests/${id}/tab-parameters`,
        method: 'GET',
      }),
    }),
    getMPZData: builder.query<
      MPZData & { requestId: number; id: number },
      { requestId: number }
    >({
      query: ({ requestId }) => ({
        url: `/requests/${requestId}/international-legal-protection`,
        method: 'GET',
      }),
      providesTags: ['MPZ'],
    }),
    createMPZData: builder.mutation<void, { data: MPZData; requestId: number }>(
      {
        query: ({ data, requestId }) => ({
          url: `/requests/${requestId}/international-legal-protection`,
          method: 'POST',
          data,
        }),
        invalidatesTags: ['MPZ'],
      }
    ),
    putMPZData: builder.mutation<
      void,
      { data: MPZData; requestId: number; id: number }
    >({
      query: ({ data, requestId, id }) => ({
        url: `/requests/${requestId}/international-legal-protection/${id}`,
        method: 'PUT',
        data,
      }),
      invalidatesTags: ['MPZ'],
    }),
    getCoordinationCountries: builder.query<
      ListResponse<CoordinationCoutry[]>,
      { ilpId?: number; page: number; size: number }
    >({
      query: ({ ilpId, page = 1, size = 10 }) => ({
        url: `/requests/international-legal-protection/${ilpId}/coordination-countries`,
        method: 'GET',
        params: { page, size, useCount: true },
      }),
      providesTags: ['COORDINATION_COUNTRIES'],
    }),
    deleteCoordinationCountry: builder.mutation<
      void,
      { coordinationId: number }
    >({
      query: ({ coordinationId }) => ({
        url: `/requests/international-legal-protection/coordination-countries/${coordinationId}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['COORDINATION_COUNTRIES'],
    }),
    getCoordinationCountry: builder.query<
      CoordinationCoutry,
      { coordinationId: number }
    >({
      query: ({ coordinationId }) => ({
        url: `/requests/international-legal-protection/coordination-countries/${coordinationId}`,
        method: 'GET',
      }),
      providesTags: ['COORDINATION_COUNTRY'],
    }),
    addCoordinationCountry: builder.mutation<
      void,
      { data: CoordinationCoutryParams; ilpId: number }
    >({
      query: ({ data, ilpId }) => ({
        url: `/requests/international-legal-protection/${ilpId}/coordination-countries`,
        method: 'POST',
        data,
      }),
      invalidatesTags: ['COORDINATION_COUNTRIES'],
    }),
    saveCoordinationCountry: builder.mutation<
      void,
      { data: CoordinationCoutryParams; ilpId: number; coordinationId: number }
    >({
      query: ({ data, ilpId, coordinationId }) => ({
        url: `/requests/international-legal-protection/${ilpId}/coordination-countries/${coordinationId}`,
        method: 'PUT',
        data,
      }),
      invalidatesTags: ['COORDINATION_COUNTRIES', 'COORDINATION_COUNTRY'],
    }),
    getRequests: builder.query<ListResponse<RequestData[]>, SearchDataType>({
      query: ({ abortController, ...data }) => ({
        url: '/requests/search',
        method: 'POST',
        signal: abortController?.signal,
        data,
      }),
      providesTags: ['REQUESTS'],
    }),
    deleteRequest: builder.mutation<void, number>({
      query: (id) => ({
        url: `/requests/${id}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['REQUESTS'],
    }),
    deleteAnnulledPermission: builder.mutation<
      void,
      { requestId: number; permissionId: number }
    >({
      query: ({ requestId, permissionId }) => ({
        url: `/requests/${requestId}/should-be-annulled-permissions/${permissionId}`,
        method: 'DELETE',
      }),
      invalidatesTags: [{ type: 'ANNULLED_PERMISSIONS' }],
    }),
    getAnnulledPermissions: builder.query<
      ListResponse<AnnulledPermissions[]>,
      { requestId: number; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ requestId, page = 2, size = 10, useCount = true }) => ({
        url: `/requests/${requestId}/should-be-annulled-permissions`,
        params: { page, size, useCount },
        method: 'GET',
      }),
      providesTags: ['ANNULLED_PERMISSIONS'],
    }),
    addAnnulledPermission: builder.mutation<
      void,
      { requestId: number; permissionIds: (string | number)[] }
    >({
      query: ({ requestId, permissionIds }) => ({
        url: `/requests/${requestId}/should-be-annulled-permissions/`,
        method: 'POST',
        data: { permissionIds },
      }),
      invalidatesTags: [{ type: 'ANNULLED_PERMISSIONS' }],
    }),
    getDefultData: builder.query<
      RequestType,
      { incomingDocumentId: number | null }
    >({
      query: ({ incomingDocumentId }) => ({
        url: `/requests/defaults?incomingDocument=${incomingDocumentId}`,
        method: 'GET',
      }),
    }),
    deleteLinkRequest: builder.mutation<
      void,
      { id: number; bondedRequestId: number }
    >({
      query: ({ id, bondedRequestId }) => ({
        url: `/requests/${id}/bonded-requests/${bondedRequestId}`,
        method: 'DELETE',
      }),
      invalidatesTags: [{ type: 'LINKED_TASK' }],
    }),
    getLinkRequest: builder.query<
      ListResponse<LinkRequests[]>,
      { requestId: number; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ requestId, page = 1, size = 10, useCount = true }) => ({
        url: `/requests/${requestId}/bonded-requests?FilterDto`,
        params: { page, size, useCount },
        method: 'GET',
      }),
      providesTags: ['LINKED_TASK'],
    }),

    addLinkRequest: builder.mutation<
      void,
      {
        requestId?: number | string[];
        id: number;
        copyAgreementLetters: boolean;
      }
    >({
      query: ({ requestId, id, copyAgreementLetters }) => ({
        url: `/requests/${requestId}/bonded-requests`,
        method: 'POST',
        data: { id, copyAgreementLetters },
      }),
      invalidatesTags: [{ type: 'LINKED_TASK' }],
    }),
    getRequestForm: builder.query<any, { requestId?: number }>({
      query: ({ requestId }) => ({
        url: `/requests/${requestId}/request-form`,
        method: 'GET',
      }),
      providesTags: ['LINKED_TASK'],
    }),
    getRequestContractors: builder.query<any, { requestId: string | number }>({
      query: ({ requestId }) => ({
        url: `/requests/${requestId}/clients`,
        method: 'GET',
      }),
    }),
    getKZContract: builder.query<
      { isKzContract: boolean },
      { id: string | number }
    >({
      query: ({ id }) => ({
        url: `/requests/${id}/is-kz-contract`,
        method: 'GET',
      }),
    }),
    getRequestTabs: builder.query<any, { id: number }>({
      query: ({ id }) => ({
        url: `/requests/${id}/tab-parameters`,
        method: 'GET',
      }),
    }),
    getSPermissionsList: builder.query<
      ListResponse<AnnulledPermissions[]>,
      { id: number; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ id, page = 1, size = 10, useCount = true }) => ({
        url: `/requests/${id}/bonded-permissions`,
        method: 'GET',
        params: { page, size, useCount },
      }),
    }),
    addPermissionsList: builder.mutation<
      void,
      { requestId: number; permissionIds: (string | number)[] }
    >({
      query: ({ requestId, permissionIds }) => ({
        url: `/requests/${requestId}/bonded-permissions/`,
        method: 'POST',
        data: { permissionIds },
      }),
      invalidatesTags: [{ type: 'PERMISSIONS_LIST' }],
    }),
    deletePermissionsList: builder.mutation<
      void,
      { requestId: number; permissionId: number }
    >({
      query: ({ requestId, permissionId }) => ({
        url: `/requests/${requestId}/bonded-permissions/${permissionId}`,
        method: 'DELETE',
      }),
      invalidatesTags: [{ type: 'PERMISSIONS_LIST' }],
    }),
    getRequestPCTR: builder.query<
      ListResponse<PCTRData[]>,
      { id: number; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ id, page = 1, size = 10, useCount = true }) => ({
        url: `/freqplans/requests/${id}/sps`,
        params: { page, size, useCount },
        method: 'GET',
      }),
    }),
    getPCTRCodeForm: builder.query<any, { requestId: number }>({
      query: ({ requestId }) => ({
        url: `/requests/${requestId}/frequency-form/`,
        method: 'GET',
      }),
    }),
  }),
});

export const {
  useGetRequestStagesQuery,
  useGetRequestQuery,
  useDeleteStageMutation,
  useCloseStageMutation,
  useGetStageDetailQuery,
  useGetStageCreateDataQuery,
  useGetCalculatedPlanDateQuery,
  useUpdateStageMutation,
  useCreateStageMutation,
  useCreateRequestMutation,
  useSaveRequestMutation,
  useMakeDocumentMaterialMutation,
  useGetRequestInitiatingDocumentsQuery,
  useGetRequestDocumentsQuery,
  useDeleteDocumentMutation,
  useDeleteInitialDocumentMutation,
  useAddDocumentsToRequestMutation,
  useMakeInitialMutation,
  useConfirmRequestMutation,
  useGetSolutionsQuery,
  useDeleteSolutionMutation,
  useGetConclusionQuery,
  useDeleteConclusionMutation,
  useGetConclusionDocumentsQuery,
  useDeleteConclusionDocumentsMutation,
  useGetConclusionClientsQuery,
  useGetConclusionSignersQuery,
  useAddSolutionMutation,
  useGetCommonDataQuery,
  useGetConclusionNumberQuery,
  useGetAnnulledConclusionQuery,
  useAddAnnulledConclusionMutation,
  useDeleteAnnulledConclusionMutation,
  useGetTabParametersQuery,
  useGetMPZDataQuery,
  useCreateMPZDataMutation,
  usePutMPZDataMutation,
  useGetCoordinationCountriesQuery,
  useDeleteCoordinationCountryMutation,
  useGetCoordinationCountryQuery,
  useAddCoordinationCountryMutation,
  useSaveCoordinationCountryMutation,
  useGetRequestsQuery,
  useDeleteRequestMutation,
  useDeleteAnnulledPermissionMutation,
  useGetAnnulledPermissionsQuery,
  useAddAnnulledPermissionMutation,
  useGetDefultDataQuery,
  useDeleteLinkRequestMutation,
  useGetLinkRequestQuery,
  useAddLinkRequestMutation,
  useGetRequestFormQuery,
  useGetRequestContractorsQuery,
  useGetKZContractQuery,
  useGetRequestTabsQuery,
  useGetSPermissionsListQuery,
  useAddPermissionsListMutation,
  useDeletePermissionsListMutation,
  useGetRequestPCTRQuery,
  useGetPCTRCodeFormQuery,
} = requestAPI;

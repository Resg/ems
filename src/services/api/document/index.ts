import { createApi } from '@reduxjs/toolkit/query/react';

import { ListResponse, SearchDataType } from '../../../types/api';
import {
  ChangeDataSignType,
  CommonDocumentType,
  DocumentBoundType,
  DocumentDeliveryType,
  DocumentFavoriteType,
  DocumentFileType,
  DocumentFollowedType,
  DocumentForTask,
  DocumentHolderType,
  DocumentIncludedFileType,
  DocumentLinkType,
  DocumentOutgoingPacketsType,
  DocumentOutgoingPacketsTypeFiles,
  DocumentOwnerType,
  DocumentPacketsType,
  DocumentProtocolType,
  DocumentRequestsType,
  DocumentRubricType,
  DocumentSightingType,
  DocumentSignedType,
  DocumentsSearchType,
  HistoryData,
  IncludedDocument,
  ParentDocument,
  RegistryPacketsType,
} from '../../../types/documents';
import { axiosBaseQuery } from '../../axios';

export const documentApi = createApi({
  reducerPath: 'documentApi',
  baseQuery: axiosBaseQuery(),
  tagTypes: ['DOCUMENT_REQUESTS'],
  endpoints: (builder) => ({
    getDocumentById: builder.query<CommonDocumentType, { id: number | string }>(
      {
        query: ({ id }) => ({
          url: `/documents/${id}`,
          method: 'GET',
        }),
      }
    ),
    getDocumentOwners: builder.query<
      ListResponse<DocumentOwnerType[]>,
      { id: number | string }
    >({
      query: ({ id }) => ({
        url: `/documents/${id}/owners`,
        method: 'GET',
      }),
    }),
    getDocumentFiles: builder.query<
      ListResponse<DocumentFileType[]>,
      { id: number | string }
    >({
      query: ({ id }) => ({
        url: `/documents/${id}/files`,
        method: 'GET',
      }),
    }),
    getIncludedDocumentFiles: builder.query<
      ListResponse<DocumentIncludedFileType[]>,
      { id: number | string }
    >({
      query: ({ id }) => ({
        url: `/documents/${id}/included-document/files`,
        method: 'GET',
      }),
    }),
    getDocumentFilesRequest: builder.mutation<
      ListResponse<DocumentFileType[]>,
      { id: number | string }
    >({
      query: ({ id }) => ({
        url: `/documents/${id}/files`,
        method: 'GET',
      }),
    }),
    getIncludedDocumentFilesRequest: builder.mutation<
      ListResponse<DocumentIncludedFileType[]>,
      { id: number | string }
    >({
      query: ({ id }) => ({
        url: `/documents/${id}/included-document/files`,
        method: 'GET',
      }),
    }),
    getDocumentSightings: builder.query<
      ListResponse<DocumentSightingType[]>,
      { id: number | string; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ id, ...params }) => ({
        url: `/documents/${id}/sightings`,
        method: 'GET',
        params: params,
      }),
    }),
    getDocumentRubrics: builder.query<
      ListResponse<DocumentRubricType[]>,
      { id: number | string }
    >({
      query: ({ id }) => ({
        url: `/documents/${id}/rubrics`,
        method: 'GET',
      }),
    }),
    getDocumentHolders: builder.query<
      ListResponse<DocumentHolderType[]>,
      { id: number | string }
    >({
      query: ({ id }) => ({
        url: `/documents/${id}/holders`,
        method: 'GET',
      }),
    }),
    getBoundedDocs: builder.query<
      ListResponse<DocumentBoundType[]>,
      { id: number | string }
    >({
      query: ({ id }) => ({
        url: `/documents/${id}/bonded-documents`,
        method: 'GET',
      }),
    }),
    getDocumentRequests: builder.query<
      ListResponse<DocumentRequestsType[]>,
      { id: number | string; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ id, ...params }) => ({
        url: `/documents/${id}/requests`,
        params: params,
        method: 'GET',
      }),
      providesTags: ['DOCUMENT_REQUESTS'],
    }),
    getDocumentsFollowed: builder.query<
      ListResponse<DocumentFollowedType[]>,
      { id: number | string; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ id, ...params }) => ({
        url: `/documents/${id}/followed-documents`,
        params: params,
        method: 'GET',
      }),
    }),
    deleteDocumentFollowed: builder.mutation<any, { id: number | string }>({
      query: ({ id }) => ({
        url: `/documents/followed-documents/${id}`,
        method: 'DELETE',
      }),
    }),
    updateDocument: builder.mutation<
      any,
      { id: number | string; documentData: any }
    >({
      query: ({ id, documentData }) => ({
        url: `/documents/${id}`,
        method: 'PUT',
        data: documentData,
      }),
    }),
    searchDocument: builder.query<
      DocumentsSearchType[],
      { searchData?: SearchDataType }
    >({
      query: ({ searchData }) => ({
        url: `/documents/queries`,
        method: 'POST',
        data: searchData,
      }),
    }),
    setDocumentLink: builder.mutation<any, { linkData: DocumentLinkType }>({
      query: ({ linkData }) => ({
        url: `/documents/links`,
        method: 'POST',
        data: linkData,
      }),
    }),
    deleteDocumentLinks: builder.mutation<any, { ids: string[] }>({
      query: ({ ids }) => ({
        url: `/documents/links?ids=${ids.join(',')}`,
        method: 'DELETE',
      }),
    }),
    createDocument: builder.mutation<any, { documentData: any }>({
      query: ({ documentData }) => ({
        url: `/documents`,
        method: 'POST',
        data: documentData,
      }),
    }),
    getDocumentsListForTask: builder.query<
      ListResponse<DocumentForTask[]>,
      { mainId: number | string }
    >({
      query: ({ mainId }) => ({
        url: `/documents/${mainId}/document-list-for-task`,
        method: 'GET',
      }),
    }),
    setDocumentToFavorites: builder.mutation<any, { ids: number[] }>({
      query: (ids) => ({
        url: `/documents/favorites`,
        method: 'POST',
        data: ids,
      }),
    }),
    getIncludedDocumentsList: builder.query<
      ListResponse<IncludedDocument[]>,
      {
        objectIds: string;
        objectTypeCode: string;
        templateId?: number;
        registrySearchType?: string;
      }
    >({
      query: (params) => ({
        url: `/documents/document-list-to-include`,
        method: 'GET',
        params,
      }),
    }),
    addDocumentsToFavorites: builder.mutation<void, { documentIds: number[] }>({
      query: ({ documentIds }) => ({
        url: `/documents/favorites`,
        method: 'POST',
        data: { ids: documentIds },
      }),
    }),
    getDocumentsCopiesTree: builder.query<
      ParentDocument[],
      { documentIds: number; sort?: string }
    >({
      query: ({ documentIds, sort = 'orderNumber' }) => ({
        url: '/documents/copies/tree',
        method: 'GET',
        params: { documentIds, sort },
      }),
    }),
    addDocumentRequest: builder.mutation<
      void,
      { requestIds: number[]; documentId: number }
    >({
      query: ({ requestIds, documentId }) => ({
        url: `/documents/requests`,
        method: 'POST',
        data: { requestIds, documentId },
      }),
    }),
    addFileToDocument: builder.mutation<
      void,
      { documentId: number; file: FormData }
    >({
      query: ({ documentId, file }) => ({
        url: `/documents/${documentId}/files`,
        method: 'POST',
        data: file,
      }),
    }),
    getHistory: builder.query<
      ListResponse<HistoryData[]>,
      { documentId: number; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ documentId, ...params }) => ({
        url: `/documents/${documentId}/registry-history-events?sort=date&sort=time`,
        params: params,
        method: 'GET',
      }),
    }),
    getDocumentProtocol: builder.query<
      ListResponse<DocumentProtocolType[]>,
      { id: number; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ id, ...params }) => ({
        url: `/documents/${id}/history-events?sort=date desc`,
        method: 'GET',
        params: params,
      }),
    }),
    changeDateSign: builder.mutation<
      void,
      { id: number | string; data: ChangeDataSignType }
    >({
      query: ({ id, data }) => ({
        url: `/documents/${id}`,
        method: 'POST',
        data: data,
      }),
    }),
    getDocumentOutgoingPackets: builder.query<
      ListResponse<DocumentOutgoingPacketsType[]>,
      { packetId: number; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ packetId, page = 1, size = 10, useCount = true }) => ({
        url: `/documents/packets/${packetId}/document-list`,
        params: { page, size, useCount },
        method: 'GET',
      }),
    }),
    getDocumentOutgoingPacketsFiles: builder.query<
      DocumentOutgoingPacketsTypeFiles[],
      { documentId: number }
    >({
      query: ({ documentId }) => ({
        url: `/documents/${documentId}/packets/files`,
        method: 'GET',
      }),
    }),
    getFavoriteDocuments: builder.query<
      ListResponse<DocumentFavoriteType[]>,
      { page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ ...params }) => ({
        url: '/documents/favorites',
        method: 'GET',
        params: params,
      }),
    }),
    deleteFavoriteDocument: builder.mutation<void, { ids: number[] }>({
      query: ({ ids }) => ({
        url: `/documents/favorites/?ids=${ids}`,
        method: 'DELETE',
      }),
    }),
    addFavoriteDocument: builder.mutation<void, { ids: number[] }>({
      query: ({ ids }) => ({
        url: `/documents/favorites/`,
        method: 'POST',
        data: { ids },
      }),
    }),
    getPacketsDocument: builder.query<
      DocumentPacketsType,
      { id: number | string }
    >({
      query: ({ id }) => ({
        url: `/documents/packets/${id}`,
        method: 'GET',
      }),
    }),

    updatePacketsDocument: builder.mutation<any, { id: number; data: any }>({
      query: ({ id, data }) => ({
        url: `/documents/packets/${id}`,
        method: 'PUT',
        data: data,
      }),
    }),
    getIsDocumentSigned: builder.query<DocumentSignedType, { id: number }>({
      query: ({ id }) => ({
        url: `/documents/${id}/tasks/has-sight-or-sign-on-execution`,
        method: 'GET',
      }),
    }),
    getRegistryPackets: builder.query<
      ListResponse<RegistryPacketsType[]>,
      { id: number; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ id, page = 1, size = 10, useCount = true }) => ({
        url: `/documents/packets/${id}/registries`,
        params: { page, size, useCount },
        method: 'GET',
      }),
    }),
    getDocumentsByFileId: builder.query<
      { data: { id: string }[] },
      { fileId: string }
    >({
      query: ({ fileId }) => ({
        url: '/documents',
        method: 'GET',
        params: { fileId },
      }),
    }),
    getDocumentDelivery: builder.query<
      ListResponse<DocumentDeliveryType[]>,
      { documentId: number; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ documentId, ...params }) => ({
        url: `/documents/${documentId}/packets?sort=localNumber`,
        method: 'GET',
        params: params,
      }),
    }),
    deleteDocumentRequest: builder.mutation<
      void,
      { id: string | number; requestId: string | number }
    >({
      query: ({ id, requestId }) => ({
        url: `/documents/${id}/requests/${requestId}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['DOCUMENT_REQUESTS'],
    }),
    recreateDocumentFile: builder.mutation<
      DocumentFileType,
      { documentId: number | string }
    >({
      query: ({ documentId }) => ({
        url: `documents/${documentId}/files/recreate`,
        method: 'POST',
      }),
    }),
  }),
});

export const {
  useGetDocumentByIdQuery,
  useDeleteDocumentFollowedMutation,
  useGetIncludedDocumentFilesQuery,
  useGetIncludedDocumentFilesRequestMutation,
  useGetDocumentFilesQuery,
  useGetDocumentFilesRequestMutation,
  useGetBoundedDocsQuery,
  useSearchDocumentQuery,
  useSetDocumentLinkMutation,
  useGetDocumentOwnersQuery,
  useCreateDocumentMutation,
  useUpdateDocumentMutation,
  useGetDocumentRubricsQuery,
  useGetDocumentRequestsQuery,
  useGetDocumentsFollowedQuery,
  useGetDocumentsListForTaskQuery,
  useSetDocumentToFavoritesMutation,
  useGetIncludedDocumentsListQuery,
  useAddDocumentsToFavoritesMutation,
  useGetDocumentsCopiesTreeQuery,
  useAddFileToDocumentMutation,
  useDeleteDocumentLinksMutation,
  useAddDocumentRequestMutation,
  useGetHistoryQuery,
  useGetDocumentProtocolQuery,
  useChangeDateSignMutation,
  useGetDocumentSightingsQuery,
  useGetDocumentOutgoingPacketsQuery,
  useGetDocumentOutgoingPacketsFilesQuery,
  useGetFavoriteDocumentsQuery,
  useDeleteFavoriteDocumentMutation,
  useAddFavoriteDocumentMutation,
  useGetPacketsDocumentQuery,
  useUpdatePacketsDocumentMutation,
  useGetIsDocumentSignedQuery,
  useGetRegistryPacketsQuery,
  useGetDocumentsByFileIdQuery,
  useGetDocumentDeliveryQuery,
  useDeleteDocumentRequestMutation,
  useRecreateDocumentFileMutation,
} = documentApi;

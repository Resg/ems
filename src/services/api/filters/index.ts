import { createApi } from '@reduxjs/toolkit/query/react';

import { FilterChip } from '../../../types/filterStack';
import { UserService } from '../../auth';
import { axiosBaseQuery } from '../../axios';

export const filtersApi = createApi({
  reducerPath: 'filtersApi',
  baseQuery: axiosBaseQuery(),
  tagTypes: ['Chip'],
  endpoints: (builder) => ({
    getSavedFilters: builder.query<FilterChip[], { formKey: string }>({
      query: ({ formKey }) => ({
        url: `/userprofile/${UserService.getUsername()}/search-panel-chipses?formKey=${formKey}`,
        method: 'GET',
      }),
      providesTags: (result) =>
        result
          ? result.map(({ id }: { id: number }) => ({ type: 'Chip', id }))
          : ['Chip'],
    }),
    createChip: builder.mutation<
      any,
      {
        data: { name: string; formKey: string; filter?: string };
      }
    >({
      query: ({ data }) => ({
        url: `/userprofile/${UserService.getUsername()}/search-panel-chipses`,
        method: 'POST',
        data,
      }),
      invalidatesTags: ['Chip'],
    }),
    deleteFilterChip: builder.mutation<any, { id: number }>({
      query: ({ id }) => ({
        url: `/userprofile/${UserService.getUsername()}/search-panel-chipses/${id}`,
        method: 'DELETE',
        data: {},
      }),
      invalidatesTags: (result) => {
        return ['Chip'];
      },
    }),
  }),
});

export const {
  useCreateChipMutation,
  useDeleteFilterChipMutation,
  useGetSavedFiltersQuery,
} = filtersApi;

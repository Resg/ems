import { createApi } from '@reduxjs/toolkit/query/react';

import { ListResponse } from '../../../types/api';
import {
  AddDocumentToRegistryParams,
  AddPacketToRegistryParams,
  RegistriesSearchDataType,
  Registry,
  RegistryDocuments,
  RegistryPackets,
  RegistrySearchType,
  SaveRegistryParams,
} from '../../../types/registries';
import { axiosBaseQuery } from '../../axios';

export const registryApi = createApi({
  reducerPath: 'registryApi',
  baseQuery: axiosBaseQuery(),
  endpoints: (builder) => ({
    getRegistry: builder.query<Registry, { id?: number }>({
      query: ({ id }) => ({
        url: `/registries/${id}`,
        method: 'GET',
      }),
    }),
    getRegistryDocuments: builder.query<
      ListResponse<RegistryDocuments[]>,
      { id: number; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ id, page = 1, size = 10, useCount = true }) => ({
        url: `/registries/${id}/documents`,
        params: { page, size, useCount },
        method: 'GET',
      }),
    }),
    deleteRegistryDocuments: builder.mutation<
      void,
      { id: number; registryId?: number; copyId?: number }
    >({
      query: ({ id, registryId, copyId }) => ({
        url: `/registries/${registryId}/documents/${id}?copyId=${copyId}`,
        method: 'DELETE',
      }),
    }),
    saveRegistry: builder.mutation<void, SaveRegistryParams>({
      query: (data) => ({
        url: `/registries/${data.id}`,
        method: 'PUT',
        data,
      }),
    }),
    getRegistries: builder.query<
      ListResponse<RegistrySearchType[]>,
      RegistriesSearchDataType
    >({
      query: (data) => ({
        url: `/registries/search`,
        method: 'POST',
        data,
      }),
    }),
    addDocumentToRegistry: builder.mutation<void, AddDocumentToRegistryParams>({
      query: (data) => ({
        url: '/registries/documents',
        method: 'POST',
        data,
      }),
    }),
    deleteRegistry: builder.mutation<any, { id: number }>({
      query: ({ id }) => ({
        url: `/registries/${id}`,
        method: 'DELETE',
      }),
    }),
    getAcceptedRegistry: builder.mutation<any, { ids: number[] }>({
      query: ({ ids }) => ({
        url: `/registries/accepted`,
        method: 'POST',
        data: { ids },
      }),
    }),
    returnRegistry: builder.mutation<any, { id: number }>({
      query: ({ id }) => ({
        url: `/registries/${id}/returned`,
        method: 'POST',
      }),
    }),
    closeRegistries: builder.mutation<any, { ids: number[] }>({
      query: ({ ids }) => ({
        url: `/registries/closed`,
        method: 'POST',
        data: { ids },
      }),
    }),
    openRegistries: builder.mutation<any, { ids: number[] }>({
      query: ({ ids }) => ({
        url: `/registries/opened`,
        method: 'POST',
        data: { ids },
      }),
    }),
    sentRegistries: builder.mutation<any, { ids: number[] }>({
      query: ({ ids }) => ({
        url: `/registries/sent`,
        method: 'POST',
        data: { ids },
      }),
    }),
    sentAgainRegistries: builder.mutation<any, { ids: number[] }>({
      query: ({ ids }) => ({
        url: `/registries/sent-again`,
        method: 'POST',
        data: { ids },
      }),
    }),
    reportRegistries: builder.mutation<any, { ids: number[] }>({
      query: ({ ids }) => ({
        url: `/registries/reports`,
        method: 'POST',
        data: { ids },
      }),
    }),
    archiveRegistries: builder.mutation<any, { ids: number[] }>({
      query: ({ ids }) => ({
        url: `/registries/archived`,
        method: 'POST',
        data: { ids },
      }),
    }),
    addPacketToRegistry: builder.mutation<void, AddPacketToRegistryParams>({
      query: (data) => ({
        url: '/registries/packets',
        method: 'POST',
        data,
      }),
    }),
    getRegistryPackets: builder.query<
      ListResponse<RegistryPackets[]>,
      { id: number; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ id, page = 1, size = 10, useCount = true }) => ({
        url: `/registries/${id}/packets`,
        params: { page, size, useCount },
        method: 'GET',
      }),
    }),
    deleteRegistryPackets: builder.mutation<
      void,
      { id?: number; packetId: number }
    >({
      query: ({ id, packetId }) => ({
        url: `/registries/${id}/packets/${packetId}`,
        method: 'DELETE',
      }),
    }),
    returnRegistryPacket: builder.mutation<
      void,
      { ids: number[]; registryId?: number }
    >({
      query: ({ registryId, ids }) => ({
        url: `/registries/packets/returned`,
        method: 'POST',
        data: { registryId, ids },
      }),
    }),
    returnRegistryDocument: builder.mutation<
      void,
      { document?: any; registryId?: number }
    >({
      query: ({ registryId, document }) => ({
        url: `/registries/documents/returned`,
        method: 'POST',
        data: { registryId, document },
      }),
    }),
    includeRegistryDocument: builder.mutation<
      void,
      { ids: number[]; registryId?: number }
    >({
      query: ({ registryId, ids }) => ({
        url: `/registries/documents/included`,
        method: 'POST',
        data: { registryId, ids },
      }),
    }),
  }),
});

export const {
  useGetRegistryQuery,
  useGetRegistryDocumentsQuery,
  useDeleteRegistryDocumentsMutation,
  useSaveRegistryMutation,
  useGetRegistriesQuery,
  useDeleteRegistryMutation,
  useGetAcceptedRegistryMutation,
  useReturnRegistryMutation,
  useCloseRegistriesMutation,
  useOpenRegistriesMutation,
  useSentRegistriesMutation,
  useSentAgainRegistriesMutation,
  useReportRegistriesMutation,
  useArchiveRegistriesMutation,
  useAddDocumentToRegistryMutation,
  useAddPacketToRegistryMutation,
  useGetRegistryPacketsQuery,
  useDeleteRegistryPacketsMutation,
  useReturnRegistryPacketMutation,
  useReturnRegistryDocumentMutation,
  useIncludeRegistryDocumentMutation,
} = registryApi;

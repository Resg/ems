import { createApi } from '@reduxjs/toolkit/query/react';

import { ListResponse } from '../../../types/api';
import {
  AdditionalTemplate,
  SettingsTemplate,
  TaskTemplate,
  UpdateSettingsTemplate,
  UserInfo,
} from '../../../types/user';
import { UserService } from '../../auth';
import { axiosBaseQuery } from '../../axios';

export const userApi = createApi({
  reducerPath: 'userApi',
  baseQuery: axiosBaseQuery(),
  tagTypes: ['TASK_TEMPLATES', 'ADDITIONAL'],
  endpoints: (builder) => ({
    getUser: builder.query<UserInfo, { login?: string }>({
      query: (params = { login: UserService.getUsername() }) => ({
        url: '/userprofile/current-user',
        method: 'GET',
        params,
      }),
    }),
    getTemplateVacant: builder.mutation<
      { isTemplateNameVacant: boolean },
      { login?: string; name: string; objectTypeCode: string }
    >({
      query: ({ login = UserService.getUsername(), name, objectTypeCode }) => ({
        url: `/userprofile/${login}/task-templates/is_template_name_vacant`,
        method: 'GET',
        params: { name, objectTypeCode },
      }),
    }),
    getTaskTemplates: builder.query<
      ListResponse<TaskTemplate[]>,
      {
        objectTypeCode: string;
        login?: string;
        page?: number;
        size?: number;
        useCount?: boolean;
      }
    >({
      query: ({ login = UserService.getUsername(), ...params }) => ({
        url: `/userprofile/${login}/task-templates`,
        method: 'GET',
        params: params,
      }),
      providesTags: ['TASK_TEMPLATES'],
    }),
    createTaskTemplate: builder.mutation<
      void,
      { name: string; template?: string; objectTypeCode: string }
    >({
      query: (data) => ({
        url: `/userprofile/${UserService.getUsername()}/task-templates`,
        method: 'POST',
        data,
      }),
      invalidatesTags: ['TASK_TEMPLATES'],
    }),
    deleteTaskTemplate: builder.mutation<void, number>({
      query: (id) => ({
        url: `/userprofile/${UserService.getUsername()}/task-templates/${id}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['TASK_TEMPLATES'],
    }),
    getAdditionalConditionList: builder.query<
      AdditionalTemplate[],
      { login?: string }
    >({
      query: ({ login = UserService.getUsername() }) => ({
        url: `/userprofile/additional-conclusion-condition-template-sets`,
        method: 'GET',
      }),
      providesTags: ['ADDITIONAL'],
    }),
    deleteAdditionalConditionList: builder.mutation<void, number>({
      query: (setId) => ({
        url: `/userprofile/additional-conclusion-condition-template-sets/${setId}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['ADDITIONAL'],
    }),
    createAdditionalConditionList: builder.mutation<
      void,
      { name: string; ids?: number[] }
    >({
      query: (data) => ({
        url: `/userprofile/additional-conclusion-condition-template-sets`,
        method: 'POST',
        data,
      }),
      invalidatesTags: ['ADDITIONAL'],
    }),
    getPersonalSettings: builder.query<SettingsTemplate, { login?: string }>({
      query: ({ login = UserService.getUsername() }) => ({
        url: `/userprofile/${login}/personal-settings`,
        method: 'GET',
      }),
    }),
    updatePersonalSettings: builder.mutation<
      any,
      { login?: string; data: UpdateSettingsTemplate }
    >({
      query: ({ login = UserService.getUsername(), data }) => ({
        url: `/userprofile/${login}/personal-settings`,
        method: 'PUT',
        data: data,
      }),
    }),
  }),
});

export const {
  useGetUserQuery,
  useGetTemplateVacantMutation,
  useGetTaskTemplatesQuery,
  useCreateTaskTemplateMutation,
  useDeleteTaskTemplateMutation,
  useGetAdditionalConditionListQuery,
  useDeleteAdditionalConditionListMutation,
  useCreateAdditionalConditionListMutation,
  useGetPersonalSettingsQuery,
  useUpdatePersonalSettingsMutation,
} = userApi;

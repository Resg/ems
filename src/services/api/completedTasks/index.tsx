import { createApi } from '@reduxjs/toolkit/query/react';

import { IncomingDocTasksType } from '../../../types/incomingDocumentTasks';
import { axiosBaseQuery } from '../../axios';

interface CompletedTasksParameters {
  searchParameters?: {
    elasticSearch?: number;

    isOnlyMain?: boolean;
    typeId?: number;
    authorIds?: Array<number>;
    createDateFrom?: string;
    createDateTo?: string;
    planDateFrom?: string;
    planDateTo?: string;
    prepareDateFrom?: string;
    prepareDateTo?: string;
    completeDateFrom?: string;
    completeDateTo?: string;

    documentTypeId?: number;
    documentNumber?: number;
    documentInternalNumber?: number;
    documentDateFrom?: string;
    documentDateTo?: string;
    documentAuthorIds?: Array<number>;
    rubricIds?: Array<number>;
    contractorIds?: Array<number>;
    documentDescription?: string;

    resolutionContent?: string;
    documentIds?: Array<number>;
  };
  sort?: string;
  page?: number;
  size?: number;
  useCount?: boolean;
}

export const completedTasksApi = createApi({
  reducerPath: 'completedTasksApi',
  baseQuery: axiosBaseQuery(),
  endpoints: (builder) => ({
    getCompletedTasks: builder.query<
      IncomingDocTasksType,
      CompletedTasksParameters
    >({
      query: (data = {}) => ({
        url: '/tasks/document/completed/search',
        method: 'POST',
        data,
      }),
    }),
  }),
});

export const { useGetCompletedTasksQuery } = completedTasksApi;

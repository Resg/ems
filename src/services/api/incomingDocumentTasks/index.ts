import { createApi } from '@reduxjs/toolkit/query/react';

import { ListResponse } from '../../../types/api';
import {
  AdditionStatusRequestProps,
  ExecuteRequestProps,
  IncomingDocTasksType,
  TaskData,
  updateTaskData,
} from '../../../types/incomingDocumentTasks';
import {
  IIncomingRequest,
  IRequestQueryParameters,
} from '../../../types/incomingRequests';
import { ObjectTask } from '../../../types/tasks';
import { axiosBaseQuery } from '../../axios';

export interface IncomingDocSearchParameters {
  searchParameters?: {
    elasticSearch?: string;
    inWorkManual?: boolean;
    documentId?: number;
    fileIds?: number[];
    document?: {
      isOnlyMain: boolean;
    };
    documentIds?: number[];
  };
  sort?: string;
  page?: number;
  size?: number;
  useCount?: boolean;
}

interface RevokeTasksParameters {
  ids: number[];
}

export const incomingDocumentTasksApi = createApi({
  reducerPath: 'incomingDocumentTasksApi',
  baseQuery: axiosBaseQuery(),
  tagTypes: ['TASKS', 'REQUESTS'],
  endpoints: (builder) => ({
    getObjectTasks: builder.query<
      ListResponse<ObjectTask[]>,
      {
        id: number;
        type: string;
        isSetByCurrentUser?: boolean;
        page?: number;
        size?: number;
        useCount?: boolean;
      }
    >({
      query: ({ id, type, ...params }) => ({
        url: `/tasks?objectTypeCode=${type}&objectId=${id}`,
        method: 'GET',
        params,
      }),
      transformResponse: (response: ListResponse<ObjectTask[]>) => {
        return {
          ...response,
          data: response.data.map((task) => ({
            ...task,
            completeDate: task.completeDate
              ? task.completeDate.replace(/T/g, ' ')
              : task.completeDate,
            termLabel: task.termLabel
              ? task.termLabel.replace(/T/g, ' ')
              : task.termLabel,
            createDate: task.createDate
              ? task.createDate.replace(/T/g, ' ')
              : task.createDate,
          })),
        };
      },
      providesTags: ['TASKS', 'REQUESTS'],
    }),
    getIncomingDocumentTasks: builder.query<
      IncomingDocTasksType,
      IncomingDocSearchParameters
    >({
      query: (data = {}) => ({
        url: '/tasks/document/incoming/search',
        method: 'POST',
        data,
      }),
      providesTags: (result) =>
        result
          ? [
              ...result.data.map(({ id }) => ({
                type: 'TASKS' as const,
                id,
              })),
              { type: 'TASKS', id: 'LIST' },
            ]
          : [{ type: 'TASKS', id: 'LIST' }],
    }),
    getIncomingRequests: builder.query<
      IIncomingRequest,
      IRequestQueryParameters
    >({
      query: ({ sort }) => ({
        url: '/tasks/request/incoming/search',
        method: 'POST',
        data: {
          sort,
        },
      }),
      providesTags: ['REQUESTS'],
    }),
    getAdditionalActionStatus: builder.query<
      { isActionActive: boolean },
      AdditionStatusRequestProps
    >({
      query: ({ typeId, id, statusId = 6 }) => ({
        url: '/tasks/actions/is-active',
        method: 'GET',
        params: { typeId, id, statusId },
      }),
    }),
    getTaskInWork: builder.mutation<void, number>({
      query: (id) => ({
        url: `/tasks/${id}/in-work`,
        method: 'POST',
        data: {},
      }),
      invalidatesTags: ['TASKS'],
    }),
    revokeTasks: builder.mutation<void, RevokeTasksParameters>({
      query: (data) => ({
        url: '/tasks/revoke-take-in-work',
        method: 'POST',
        data,
      }),
      invalidatesTags: ['TASKS', 'REQUESTS'],
    }),
    executeTask: builder.mutation<void, ExecuteRequestProps>({
      query: ({ id, ...data }) => ({
        url: `/tasks/${id}/execute`,
        method: 'POST',
        data,
      }),
      invalidatesTags: [{ type: 'TASKS', id: 'LIST' }, { type: 'REQUESTS' }],
    }),
    reassignTask: builder.mutation<void, ExecuteRequestProps>({
      query: ({ id, performerId }) => ({
        url: `/tasks/${id}/reassign`,
        method: 'POST',
        data: { performerId },
      }),
      invalidatesTags: [{ type: 'TASKS', id: 'LIST' }, { type: 'REQUESTS' }],
    }),
    reassignedTask: builder.mutation<void, ExecuteRequestProps>({
      query: ({ id, performerId, comments }) => ({
        url: `/tasks/${id}/reassigned`,
        method: 'POST',
        data: { performerId, comments },
      }),
      invalidatesTags: [{ type: 'TASKS', id: 'LIST' }, { type: 'REQUESTS' }],
    }),
    reopenTask: builder.mutation<void, ExecuteRequestProps>({
      query: ({ id, comments }) => ({
        url: `/tasks/${id}/returned-to-author`,
        method: 'POST',
        data: { comments },
      }),
      invalidatesTags: [{ type: 'TASKS', id: 'LIST' }, { type: 'REQUESTS' }],
    }),
    getTask: builder.query<TaskData, any>({
      query: (id) => ({
        url: `/tasks/${id}`,
        method: 'GET',
      }),
      providesTags: ['TASKS'],
    }),
    updateTask: builder.mutation<
      any,
      { id: number; updateData: updateTaskData }
    >({
      query: ({ id, updateData }) => ({
        url: `/tasks/${id}`,
        method: 'PUT',
        data: updateData,
      }),
    }),
    sendToExecution: builder.mutation<void, number>({
      query: (id) => ({
        url: `/tasks/${id}/prepared`,
        method: 'POST',
        data: {},
      }),
      invalidatesTags: ['TASKS', 'REQUESTS'],
    }),
    deleteTask: builder.mutation<void, number>({
      query: (id) => ({
        url: `/tasks/${id}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['TASKS', 'REQUESTS'],
    }),
    revokeTask: builder.mutation<void, number>({
      query: (id) => ({
        url: `/tasks/${id}/revoked`,
        method: 'POST',
        data: {},
      }),
      invalidatesTags: ['TASKS', 'REQUESTS'],
    }),
    getIncomingDocumentTasksCount: builder.query<
      IncomingDocTasksType,
      IncomingDocSearchParameters
    >({
      query: (data = {}) => ({
        url: '/tasks/document/incoming/search',
        method: 'POST',
        data: {
          size: 0,
          useCount: true,
        },
      }),
    }),
    getIncomingDocumentTasksInWorkCount: builder.query<
      IncomingDocTasksType,
      IncomingDocSearchParameters
    >({
      query: (data = {}) => ({
        url: '/tasks/document/incoming/search',
        method: 'POST',
        data: {
          size: 0,
          useCount: true,
          searchParameters: {
            inWorkManual: true,
          },
        },
      }),
    }),
    getIncomingRequestsCount: builder.query<
      IIncomingRequest,
      IRequestQueryParameters
    >({
      query: () => ({
        url: '/tasks/request/incoming/search',
        method: 'POST',
        data: {
          size: 0,
          useCount: true,
        },
      }),
    }),
    sendToReexecution: builder.mutation<void, number>({
      query: (id) => ({
        url: `/tasks/${id}/reexecution`,
        method: 'POST',
        data: {},
      }),
      invalidatesTags: ['TASKS', 'REQUESTS'],
    }),
    sendToContinue: builder.mutation<void, { id: number }>({
      query: ({ id }) => ({
        url: `/tasks/${id}/continue`,
        method: 'POST',
        data: {},
      }),
      invalidatesTags: ['TASKS', 'REQUESTS'],
    }),
    sendToAcquainted: builder.mutation<void, number>({
      query: (id) => ({
        url: `/tasks/${id}/acquainted`,
        method: 'POST',
        data: {},
      }),
      invalidatesTags: ['TASKS', 'REQUESTS'],
    }),
  }),
});

export const {
  useGetObjectTasksQuery,
  useGetIncomingDocumentTasksQuery,
  useGetIncomingRequestsQuery,
  useGetTaskInWorkMutation,
  useRevokeTasksMutation,
  useGetAdditionalActionStatusQuery,
  useExecuteTaskMutation,
  useReassignedTaskMutation,
  useReassignTaskMutation,
  useReopenTaskMutation,
  useGetTaskQuery,
  useUpdateTaskMutation,
  useSendToExecutionMutation,
  useDeleteTaskMutation,
  useRevokeTaskMutation,
  useGetIncomingDocumentTasksCountQuery,
  useGetIncomingDocumentTasksInWorkCountQuery,
  useGetIncomingRequestsCountQuery,
  useSendToReexecutionMutation,
  useSendToContinueMutation,
  useSendToAcquaintedMutation,
} = incomingDocumentTasksApi;

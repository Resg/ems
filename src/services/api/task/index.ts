import { createApi } from '@reduxjs/toolkit/query/react';

import { ListResponse } from '../../../types/api';
import {
  ObjectTask,
  SiblingType,
  TaskDetails,
  TaskRouteData,
  TaskSubtaskUpdateData,
} from '../../../types/tasks';
import { axiosBaseQuery } from '../../axios';

export const taskAPI = createApi({
  reducerPath: 'taskApi',
  baseQuery: axiosBaseQuery(),
  tagTypes: ['DOCUMENT_TASKS'],
  endpoints: (builder) => ({
    getObjectTasks: builder.query<
      ListResponse<ObjectTask[]>,
      {
        id: number;
        type: string;
        isSetByCurrentUser?: boolean;
        page?: number;
        size?: number;
        useCount?: boolean;
      }
    >({
      query: ({ id, type, ...params }) => ({
        url: `/tasks?objectTypeCode=${type}&objectId=${id}`,
        method: 'GET',
        params,
      }),
      transformResponse: (response: ListResponse<ObjectTask[]>) => {
        return {
          ...response,
          data: response.data.map((task) => ({
            ...task,
            completeDate: task.completeDate
              ? task.completeDate.replace(/T/g, ' ')
              : task.completeDate,
            termLabel: task.termLabel
              ? task.termLabel.replace(/T/g, ' ')
              : task.termLabel,
            createDate: task.createDate
              ? task.createDate.replace(/T/g, ' ')
              : task.createDate,
          })),
        };
      },
      providesTags: ['DOCUMENT_TASKS'],
    }),
    createTaskRoute: builder.mutation<
      any,
      { taskData: TaskRouteData[] | null }
    >({
      query: ({ taskData }) => ({
        url: '/tasks',
        method: 'POST',
        data: taskData,
      }),
      invalidatesTags: ['DOCUMENT_TASKS'],
    }),
    getTaskChildren: builder.query<
      ListResponse<SiblingType[]>,
      { parentId?: number }
    >({
      query: ({ parentId }) => ({
        url: `/tasks/${parentId}/children`,
        method: 'GET',
      }),
    }),
    revokeTask: builder.mutation<void, number>({
      query: (id) => ({
        url: `/tasks/${id}/revoked`,
        method: 'POST',
        data: {},
      }),
      invalidatesTags: ['DOCUMENT_TASKS'],
    }),
    revokeToWorkResolution: builder.mutation<
      void,
      { id: number; comments?: string }
    >({
      query: ({ id, comments }) => ({
        url: `/tasks/${id}/revoked`,
        method: 'POST',
        data: { comments },
      }),
      invalidatesTags: ['DOCUMENT_TASKS'],
    }),
    sendAuthorResolution: builder.mutation<
      void,
      { id: number; comments?: string }
    >({
      query: ({ id, comments }) => ({
        url: `/tasks/${id}/sent-to-author`,
        method: 'POST',
        data: { comments },
      }),
      invalidatesTags: ['DOCUMENT_TASKS'],
    }),
    getOptionTaskChildren: builder.query<
      any,
      { parentId?: number; excludedTaskId?: number }
    >({
      query: ({ parentId, excludedTaskId }) => ({
        url: `/tasks/${parentId}/children`,
        params: { excludedTaskId },
        method: 'GET',
      }),
      transformResponse: (value: any) => {
        return (
          value.data.map((option: any) => ({
            label: option.title,
            value: option.id,
          })) || []
        );
      },
    }),
    getDetailedDataOnTheTaskSubtask: builder.query<TaskDetails, { id: number }>(
      {
        query: ({ id }) => ({
          url: `/tasks/${id}`,
          method: 'GET',
        }),
      }
    ),
    getTaskData: builder.query<
      ListResponse<SiblingType[]>,
      { parentId: number; excludedTaskId?: number }
    >({
      query: ({ parentId, excludedTaskId }) => ({
        url: `/tasks/${parentId}/children`,
        method: 'GET',
        params: { excludedTaskId },
      }),
    }),
    updateTaskSubtask: builder.mutation<
      void,
      { id: number; data: TaskSubtaskUpdateData }
    >({
      query: ({ id, data }) => ({
        url: `/tasks/${id}`,
        method: 'PUT',
        data: data,
      }),
    }),
  }),
});

export const {
  useGetObjectTasksQuery,
  useCreateTaskRouteMutation,
  useGetTaskChildrenQuery,
  useRevokeTaskMutation,
  useRevokeToWorkResolutionMutation,
  useSendAuthorResolutionMutation,
  useGetDetailedDataOnTheTaskSubtaskQuery,
  useGetTaskDataQuery,
  useUpdateTaskSubtaskMutation,
  useGetOptionTaskChildrenQuery,
} = taskAPI;

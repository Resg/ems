import { createApi } from '@reduxjs/toolkit/query/react';

import { MyDocTasksType } from '../../../types/myDocumentTasks';
import { axiosBaseQuery } from '../../axios';

export const myDocumentTasksApi = createApi({
  reducerPath: 'myDocumentTasksApi',
  baseQuery: axiosBaseQuery(),
  endpoints: (builder) => ({
    getMyDocumentTasks: builder.query<MyDocTasksType, { type: string }>({
      query: ({ type }) => ({
        url: `/tasks?objectTypeCode=${type}&isSetByCurrentUser=true`,
        method: 'GET',
      }),
    }),
    getMyDocumentCountTasks: builder.query<
      MyDocTasksType,
      { type: string; page?: number; size?: number; useCount?: boolean }
    >({
      query: ({ type, size = 0, useCount = true }) => ({
        url: `/tasks?objectTypeCode=${type}&isSetByCurrentUser=true`,
        params: { size, useCount },
        method: 'GET',
      }),
    }),
  }),
});

export const { useGetMyDocumentTasksQuery, useGetMyDocumentCountTasksQuery } =
  myDocumentTasksApi;

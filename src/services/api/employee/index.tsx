import { createApi } from '@reduxjs/toolkit/query/react';

import { ListResponse } from '../../../types/api';
import { DivisionData, EmployeeData } from '../../../types/employee';
import { axiosBaseQuery } from '../../axios';

export const employeeAPI = createApi({
  reducerPath: 'employeeApi',
  baseQuery: axiosBaseQuery(),

  endpoints: (builder) => ({
    getDivisions: builder.query<ListResponse<DivisionData[]>, {}>({
      query: () => ({
        url: `/dictionaries/divisions/`,
        method: 'GET',
      }),
    }),
    getEmployees: builder.query<
      ListResponse<EmployeeData[]>,
      { isFired?: boolean; isOld?: boolean; sort?: string }
    >({
      query: ({ isFired = false, isOld = false, sort = 'personFio' }) => ({
        url: `/dictionaries/clerks`,
        method: 'GET',
        params: { isFired, isOld, sort },
      }),
    }),
  }),
});

export const { useGetDivisionsQuery, useGetEmployeesQuery } = employeeAPI;

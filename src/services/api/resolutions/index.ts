import { createApi } from '@reduxjs/toolkit/query/react';

import { ListResponse } from '../../../types/api';
import { DocumentResolution, Resolution } from '../../../types/resolution';
import { axiosBaseQuery } from '../../axios';

export const resolutionApi = createApi({
  reducerPath: 'resolutionApi',
  baseQuery: axiosBaseQuery(),
  tagTypes: ['RESOLUTION', 'RESOLUTION_LIST'],
  endpoints: (builder) => ({
    getResolution: builder.query<Resolution, { id?: number | null }>({
      query: ({ id }) => ({
        url: `/resolutions/${id || ''}`,
        method: 'GET',
      }),
      providesTags: ['RESOLUTION'],
    }),
    saveResolution: builder.mutation<Resolution, Resolution>({
      query: (data) => ({
        url: '/resolutions',
        data,
        method: 'POST',
      }),
      invalidatesTags: ['RESOLUTION'],
    }),
    runResolution: builder.mutation<Resolution, Resolution>({
      query: (data) => ({
        url: '/resolutions/run',
        data,
        method: 'POST',
      }),
      invalidatesTags: ['RESOLUTION'],
    }),
    revokeResolution: builder.mutation<
      Resolution,
      { id: number; comment?: string }
    >({
      query: ({ id, comment = '' }) => ({
        url: `/resolutions/${id}/revoke-to-editor`,
        data: { comment },
        method: 'POST',
      }),
      invalidatesTags: ['RESOLUTION'],
    }),
    approveResolution: builder.mutation<Resolution, { id: number }>({
      query: ({ id }) => ({
        url: `/resolutions/${id}/approve`,
        data: {},
        method: 'POST',
      }),
      invalidatesTags: ['RESOLUTION'],
    }),
    getResolutionList: builder.query<
      ListResponse<DocumentResolution[]>,
      number
    >({
      query: (documentId) => ({
        url: '/resolutions/search',
        method: 'POST',
        data: {
          searchParameters: {
            documentId,
          },
        },
      }),
      providesTags: ['RESOLUTION_LIST'],
    }),
    deleteResolution: builder.mutation<void, number>({
      query: (id) => ({
        url: `/resolutions/${id}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['RESOLUTION_LIST'],
    }),
  }),
});

export const {
  useGetResolutionQuery,
  useSaveResolutionMutation,
  useApproveResolutionMutation,
  useRunResolutionMutation,
  useRevokeResolutionMutation,
  useGetResolutionListQuery,
  useDeleteResolutionMutation,
} = resolutionApi;

import { createApi } from '@reduxjs/toolkit/query/react';

import { UserTableSettings } from '../../../types/userTableSettings';
import { UserService } from '../../auth';
import { axiosBaseQuery } from '../../axios';

export const userTableSettingsApi = createApi({
  reducerPath: 'userTableSettingsApi',
  baseQuery: axiosBaseQuery(),
  tagTypes: ['USER_TABLE_SETTINGS'],
  endpoints: (builder) => ({
    getUserTableSettings: builder.query<UserTableSettings, { formKey: string }>(
      {
        query: ({ formKey }) => ({
          url: `/userprofile/${UserService.getUsername()}/user-table-settings/${formKey}`,
          method: 'GET',
        }),
        providesTags: ['USER_TABLE_SETTINGS'],
      }
    ),
    createUserTableSettings: builder.mutation<
      void,
      { formKey: string; data: UserTableSettings }
    >({
      query: ({ formKey, data }) => ({
        url: `/userprofile/${UserService.getUsername()}/user-table-settings/${formKey}`,
        method: 'POST',
        data,
      }),
      invalidatesTags: ['USER_TABLE_SETTINGS'],
    }),
    editUserTableSettings: builder.mutation<
      void,
      { formKey: string; data: UserTableSettings }
    >({
      query: ({ formKey, data }) => ({
        url: `/userprofile/${UserService.getUsername()}/user-table-settings/${formKey}`,
        method: 'PUT',
        data,
      }),
      invalidatesTags: ['USER_TABLE_SETTINGS'],
    }),

    deleteUserTableSettings: builder.mutation<void, { formKey: string }>({
      query: ({ formKey }) => ({
        url: `/userprofile/${UserService.getUsername()}/user-table-settings/${formKey}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['USER_TABLE_SETTINGS'],
    }),
  }),
});

export const {
  useGetUserTableSettingsQuery,
  useCreateUserTableSettingsMutation,
  useEditUserTableSettingsMutation,
  useDeleteUserTableSettingsMutation,
} = userTableSettingsApi;

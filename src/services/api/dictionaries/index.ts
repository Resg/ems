import { createApi } from '@reduxjs/toolkit/query/react';

import { ListResponse } from '../../../types/api';
import {
  AdditionalConclusionData,
  AdditionalConclusionDataTemplate,
  AdditionalConclusionRequestProps,
  UpdateAdditionalConclusionDataTemplate,
} from '../../../types/conclusions';
import {
  AdditionalConclusionDictionaryData,
  AdressesTransofrmListType,
  CommunicationChannel,
  ConclusionAnnulment,
  ConclusionData,
  ConclusionPeriod,
  ConclusionState,
  ContractorParamsType,
  ContractorType,
  CoordinationStatus,
  Counterparty,
  CounterpartyPerson,
  CounterpartySearchParameters,
  CounterpartySearchWithPagination,
  Country,
  DefaultAddresseeDeliveryType,
  DeliveryType,
  DictionariesAccessType,
  DictionariesClerkType,
  DictionariesRubricType,
  DocumentTemplate,
  FederalDistrictsType,
  FrequencyBandSearchType,
  FrequencyBandType,
  FrequencyMeasure,
  InnerAddress,
  InternalRegistryStates,
  MailPackage,
  MailPackageSearch,
  MPZState,
  NetTypesParamsType,
  NetTypesType,
  Option,
  OrganizationalLegalForm,
  PermissionTypesType,
  ProgramType,
  RadioServiceType,
  RegionType,
  RegistrationState,
  RegistryName,
  RegistryPacketName,
  RequestModalParams,
  RequestModalSolution,
  RequestTypesType,
  RoutesType,
  ServiceMRFCType,
  SSEResponseType,
  SSEType,
  TaskType,
  TechnologyType,
  TechologyParamsType,
} from '../../../types/dictionaries';
import { axiosBaseQuery } from '../../axios';

export const dictionariesApi = createApi({
  reducerPath: 'dictionariesApi',
  baseQuery: axiosBaseQuery(),
  endpoints: (builder) => ({
    getDocumentAccesses: builder.query<
      ListResponse<DictionariesAccessType[]>,
      { sort?: string }
    >({
      query: ({ sort = 'name' }) => ({
        url: `/dictionaries/document-accesses`,
        method: 'GET',
        params: { sort },
      }),
    }),
    getDocumentRubrics: builder.query<
      ListResponse<DictionariesRubricType[]>,
      {
        documentTypeId?: number;
        isActual?: boolean;
        objectTypeCode?: number;
        sort?: string;
      }
    >({
      query: ({
        documentTypeId,
        objectTypeCode,
        isActual = true,
        sort = 'name',
      }) => ({
        url: '/dictionaries/rubrics',
        params: { documentTypeId, isActual, objectTypeCode, sort },
        method: 'GET',
      }),
    }),
    getDocumentTemplates: builder.query<Option[], { rubricIds: number[] }>({
      query: (params) => ({
        url: '/dictionaries/templates',
        params: { rubricIds: params.rubricIds.join(', ') },
        method: 'GET',
      }),
      transformResponse: (value: any) => {
        return value.data.map((option: any) => ({
          label: option.description,
          value: option.id,
        }));
      },
    }),
    getDocumentTemplatesTree: builder.query<
      ListResponse<DocumentTemplate[]>,
      { rubricId: number; sort: string }
    >({
      query: ({ rubricId, sort }) => ({
        url: '/dictionaries/templates/tree',
        params: { rubricId, sort },
        method: 'GET',
      }),
    }),
    getDocumentClerks: builder.query<
      ListResponse<DictionariesClerkType[]>,
      { isFired?: boolean; isOld?: boolean; sort?: string }
    >({
      query: ({ isFired = false, isOld = false, sort = 'personFio' }) => ({
        url: '/dictionaries/clerks',
        params: { isFired, isOld, sort },
        method: 'GET',
      }),
    }),
    getDocumentTypes: builder.query<
      Option[],
      { includeIrrelevant?: boolean; sort?: string }
    >({
      query: ({ includeIrrelevant = false, sort = 'name' }) => ({
        url: '/dictionaries/document-types?FilterDto',
        params: { includeIrrelevant },
        method: 'GET',
      }),
      transformResponse: (value: any) => {
        return value.data.map((option: any) => ({
          label: option.name,
          value: option.id,
        }));
      },
    }),
    getRubricsTypes: builder.query<Option[], {}>({
      query: () => ({
        url: '/dictionaries/rubric-types?FilterDto',
        method: 'GET',
      }),
      transformResponse: (value: any) => {
        return value.data.map((option: any) => ({
          label: option.classifierName,
          value: option.classifierFullCode,
        }));
      },
    }),
    getStatuses: builder.query<Option[], {}>({
      query: () => ({
        url: '/dictionaries/document-status?FilterDto',
        method: 'GET',
      }),
      transformResponse: (value: any) => {
        return value.data.map((option: any) => ({
          label: option.status,
          value: option.statusCode,
        }));
      },
    }),
    getContractors: builder.query<Option[], { q?: string }>({
      query: ({ q = '' }) => ({
        url: '/dictionaries/contractors/queries/outer-document-addresses',
        method: 'GET',
        // body: { searchParameters: { elasticSearch: q } },
      }),
      transformResponse: (value: any) => {
        return value.data.map((option: any) => ({
          label: option.fullName,
          value: option.id,
        }));
      },
    }),
    getCounterparties: builder.query<
      ListResponse<Counterparty[]>,
      CounterpartySearchWithPagination
    >({
      query: ({ searchParameters, page = 1, size = 10, useCount = true }) => ({
        url: '/dictionaries/contractors/search',
        method: 'POST',
        data: { searchParameters, page, size, useCount },
      }),
    }),
    getDefaultCounterparties: builder.query<
      number[],
      CounterpartySearchParameters
    >({
      query: ({ templateId, objectId, objectTypeCode, requestIds }) => ({
        url: `/dictionaries/contractors/default-outer-document-addressees`,
        params: { templateId, objectId, objectTypeCode, requestIds },
        method: 'GET',
      }),
      transformResponse: (value: any) => {
        return value.data.map((option: any) => option.id);
      },
    }),
    getDefaultAddresseeDelivery: builder.mutation<
      ListResponse<DefaultAddresseeDeliveryType[]>,
      { contractorId: number; deliveryTypeCode?: string }
    >({
      query: ({ contractorId, deliveryTypeCode }) => ({
        url: `/dictionaries/contractors/default-outer-document-addressee-delivery-types`,
        params: { contractorId, deliveryTypeCode },
        method: 'GET',
      }),
    }),
    getCounterpartiesAsync: builder.mutation<
      ListResponse<Counterparty[]>,
      CounterpartySearchParameters
    >({
      query: (params) => ({
        url: '/dictionaries/contractors/search',
        method: 'POST',
        data: { searchParameters: params },
      }),
    }),

    getInnerAddresses: builder.query<
      ListResponse<InnerAddress[]>,
      {
        id?: number;
        typeCode?: 'CLERK' | 'DIVISION';
        sort?: string;
        showNotActual?: boolean;
      }
    >({
      query: ({
        id,
        typeCode,
        sort = 'typeCode desc, name',
        showNotActual = true,
      }) => ({
        url: '/dictionaries/division-clerk-union',
        method: 'GET',
        params: {
          id,
          typeCode,
          sort,
          showNotActual,
        },
      }),
    }),
    getDivisions: builder.query<Option[], {}>({
      query: () => ({
        url: '/dictionaries/divisions/',
        method: 'GET',
      }),
      transformResponse: (value: any) => {
        return value.data.map((option: any) => ({
          label: option.fullName,
          value: option.id,
          parent: option.divisionParentId,
        }));
      },
    }),
    getDocumentRubricsOptions: builder.query<
      Option[],
      {
        documentTypeId?: number;
        isActual?: boolean;
        objectTypeCode?: string;
        sort?: string;
        classifierFullCode?: string;
      }
    >({
      query: ({
        documentTypeId,
        objectTypeCode,
        isActual = true,
        sort = 'name',
        classifierFullCode,
      }) => ({
        url: '/dictionaries/rubrics',
        params: {
          documentTypeId,
          isActual,
          objectTypeCode,
          sort,
          classifierFullCode,
        },
        method: 'GET',
      }),
      transformResponse: (value: any) => {
        if (!value) {
          return [];
        }
        return value.data.map((option: any) => ({
          label: option.name,
          value: option.id,
        }));
      },
    }),
    getAccesses: builder.query<Option[], { sort?: string }>({
      query: ({ sort = 'name' }) => ({
        url: `/dictionaries/document-accesses`,
        method: 'GET',
        params: { sort },
      }),
      transformResponse: (value: any) => {
        if (!value) {
          return [];
        }
        return value.data.map((option: any) => ({
          label: option.name,
          value: option.code,
        }));
      },
    }),
    getClerks: builder.query<
      (Option & { isDefaultRegistrar?: boolean })[],
      { isFired?: boolean; isOld?: boolean; sort?: string }
    >({
      query: ({ isFired = false, isOld = false, sort = 'personFio' }) => ({
        url: '/dictionaries/clerks',
        params: { isFired, isOld, sort },
        method: 'GET',
      }),
      transformResponse: (value: any) => {
        if (!value) {
          return [];
        }
        return value.data.map((option: any) => ({
          label: option.personFio,
          value: option.id,
          divisionId: option.divisionId,
          isDefaultRegistrar: option.isDefaultRegistrar,
          isDefaultOutgoingDocumentsSigner:
            option.isDefaultOutgoingDocumentsSigner,
        }));
      },
    }),
    getResolutionDescriptions: builder.query<string[], {}>({
      query: () => ({
        url: '/dictionaries/resolution-descriptions',
        method: 'GET',
      }),
      transformResponse(value?: { data: { description: string }[] }) {
        if (!value) {
          return [];
        }
        return value.data.map((option) => option.description);
      },
    }),
    getRoutesTypes: builder.query<RoutesType[], { typeCode: string }>({
      query: ({ typeCode }) => ({
        url: `/dictionaries/route-types/new-route-formation-data?objectTypeCode=${typeCode}`,
        method: 'GET',
      }),
    }),
    getRequestTypes: builder.query<RequestTypesType[], void>({
      query: () => ({
        url: '/dictionaries/request-types',
        method: 'GET',
      }),
    }),
    getRequestStages: builder.query<ListResponse<RequestTypesType[]>, void>({
      query: () => ({
        url: '/dictionaries/request-stages',
        method: 'GET',
        params: { sort: 'name' },
      }),
    }),
    getRadioService: builder.query<
      RadioServiceType[],
      { serviceMRFCId?: number }
    >({
      query: ({ serviceMRFCId }) => ({
        url: '/dictionaries/radio-services',
        method: 'GET',
        params: { serviceMRFCId },
      }),
    }),
    getServiceMRFC: builder.query<ServiceMRFCType[], { radioservice?: number }>(
      {
        query: ({ radioservice }) => ({
          url: '/dictionaries/services-mrfc',
          method: 'GET',
          params: { radioservice },
        }),
      }
    ),
    getTechnologies: builder.query<
      ListResponse<TechnologyType[]>,
      TechologyParamsType
    >({
      query: (data) => ({
        url: '/dictionaries/technologies/search',
        method: 'POST',
        data: {
          searchParameters: data,
        },
      }),
    }),
    getFrequencyBands: builder.query<
      FrequencyBandType[],
      FrequencyBandSearchType
    >({
      query: ({ radioService, serviceMrfc, technology, direction }) => ({
        url: '/dictionaries/frequency-bands',
        params: { radioService, serviceMrfc, technology, direction },
        method: 'GET',
      }),
    }),
    getSSE: builder.query<SSEResponseType, void>({
      query: () => ({
        url: '/dictionaries/sse',
        method: 'GET',
      }),
    }),
    getMultiplexes: builder.query<SSEType[], void>({
      query: () => ({
        url: '/dictionaries/multiplexes',
        method: 'GET',
      }),
    }),
    getPrograms: builder.query<ProgramType[], void>({
      query: () => ({
        url: '/dictionaries/programs',
        method: 'GET',
      }),
    }),
    getNetTypes: builder.query<NetTypesType[], NetTypesParamsType>({
      query: ({ serviceMrfc }) => ({
        url: '/dictionaries/net-types',
        params: { serviceMRFC: serviceMrfc },
        method: 'GET',
      }),
    }),
    getNetCategories: builder.query<ProgramType[], void>({
      query: () => ({
        url: '/dictionaries/net-categories',
        method: 'GET',
      }),
    }),
    getTechnologiesRKN: builder.query<ProgramType[], void>({
      query: () => ({
        url: '/dictionaries/technology-rkn',
        method: 'GET',
      }),
    }),
    getRegions: builder.query<RegionType[], void>({
      query: () => ({
        url: '/dictionaries/regions',
        method: 'GET',
      }),
    }),
    getContractorsList: builder.query<
      ListResponse<ContractorType[]>,
      ContractorParamsType & { abortController?: AbortController }
    >({
      query: ({ name, selectionType, documentTypeId, abortController }) => ({
        url: '/dictionaries/contractors',
        params: { name, selectionType, documentTypeId },
        method: 'GET',
        signal: abortController?.signal,
      }),
    }),
    getAsyncContractorsList: builder.mutation<
      ListResponse<ContractorType[]>,
      ContractorParamsType & { abortController?: AbortController }
    >({
      query: ({
        name,
        selectionType,
        documentTypeId,
        abortController,
        isOnlyActual,
      }) => ({
        url: '/dictionaries/contractors',
        params: { name, selectionType, documentTypeId, isOnlyActual },
        method: 'GET',
        signal: abortController?.signal,
      }),
    }),
    getTaskTypes: builder.query<
      ListResponse<TaskType[]>,
      {
        objectTypeCode?: string;
        parentTypeId?: number;
        rootObjectTypeCode?: string;
      }
    >({
      query: ({ objectTypeCode, parentTypeId, rootObjectTypeCode }) => ({
        url: '/dictionaries/task-types',
        params: { objectTypeCode, parentTypeId, rootObjectTypeCode },
        method: 'GET',
      }),
    }),
    getConclusionStatuses: builder.query<Option[], {}>({
      query: () => ({
        url: `/dictionaries/conclusion-states?sort=name`,
        method: 'GET',
      }),
      transformResponse: (value: ConclusionData[]) => {
        if (!value) {
          return [];
        }
        return value.map((option: ConclusionData) => ({
          label: option.name,
          value: option.code,
        }));
      },
    }),
    getConclusionTypes: builder.query<Option[], {}>({
      query: () => ({
        url: `/dictionaries/conclusion-types?sort=name`,
        method: 'GET',
      }),
      transformResponse: (value: ConclusionData[]) => {
        if (!value) {
          return [];
        }
        return value.map((option: ConclusionData) => ({
          label: option.name,
          value: option.code,
        }));
      },
    }),
    getConclusionSorts: builder.query<Option[], {}>({
      query: () => ({
        url: `/dictionaries/conclusion-sorts`,
        method: 'GET',
      }),
      transformResponse: (value: { id: number; name: string }[]) => {
        if (!value) {
          return [];
        }
        return value.map((option) => ({
          label: option.name,
          value: option.id,
        }));
      },
    }),
    getDeliveryTypes: builder.mutation<
      DeliveryType[],
      { id: number; documentId?: number; existedItemsIds?: string[] }
    >({
      query: ({ id, documentId, existedItemsIds }) => ({
        url: `/dictionaries/contractors/${id}/delivery-types-for-outgoing-document?documentIsEds=true`,
        method: 'GET',
        params: { documentId, existedItemsIds: existedItemsIds?.join(',') },
      }),
    }),
    getCounterpartyPersons: builder.mutation<
      Option[],
      { id?: number; fullName?: string }
    >({
      query: ({ id, fullName }) => ({
        url: `/dictionaries/contact-persons`,
        method: 'GET',
        params: { contractorId: id, contactFio: fullName },
      }),
      transformResponse: (value: ListResponse<CounterpartyPerson[]>) => {
        if (!value) {
          return [];
        }
        return value.data.map((option: CounterpartyPerson) => ({
          label: option.contactFio,
          value: option.contactPersonId,
        }));
      },
    }),

    getCounterpartyPersonsList: builder.mutation<Option[], { ids: number[] }>({
      query: ({ ids }) => ({
        url: `/dictionaries/contact-persons?ids=${ids}`,
        method: 'GET',
      }),
      transformResponse: (value: ListResponse<CounterpartyPerson[]>) => {
        if (!value) {
          return [];
        }
        return value.data.map((option: CounterpartyPerson) => ({
          label: option.contactFio,
          value: option.contactPersonId,
        }));
      },
    }),

    getConclusionPeriods: builder.query<Option[], {}>({
      query: () => ({
        url: '/dictionaries/conclusion-periods?sort=sortOrder',
        method: 'GET',
      }),
      transformResponse: (value: ListResponse<ConclusionPeriod[]>) => {
        return value.data.map((option: any) => ({
          label: option.value,
          value: option.code,
        }));
      },
    }),
    getConclusionStates: builder.query<Option[], {}>({
      query: () => ({
        url: '/dictionaries/frequency-states?sort=name',
        method: 'GET',
      }),
      transformResponse: (value: ListResponse<ConclusionState[]>) => {
        return value.data.map((option: ConclusionState) => ({
          label: option.name,
          value: option.code,
        }));
      },
    }),
    getConclusionAnnulments: builder.query<Option[], {}>({
      query: () => ({
        url: '/dictionaries/conclusion-annulment-reasons',
        method: 'GET',
      }),
      transformResponse: (value: ListResponse<ConclusionAnnulment[]>) => {
        return value.data.map((option: ConclusionAnnulment) => ({
          label: option.value,
          value: option.code,
        }));
      },
    }),
    getSolutionModalData: builder.query<
      ListResponse<RequestModalSolution[]>,
      RequestModalParams
    >({
      query: ({
        page,
        size,
        useCount = true,
        number,
        datePeriodStart,
        datePeriodEnd,
      }) => ({
        url: '/dictionaries/gkrh-solutions',
        params: {
          page,
          size,
          useCount,
          number,
          datePeriodStart,
          datePeriodEnd,
        },
        method: 'GET',
      }),
    }),
    getCommunicationChannels: builder.query<
      CommunicationChannel[],
      { bandTx?: number }
    >({
      query: ({ bandTx }) => ({
        url: '/dictionaries/communication-channels',
        method: 'GET',
        params: { bandTx },
      }),
    }),
    getRegistrationStates: builder.query<Option[], void>({
      query: () => ({
        url: '/dictionaries/registration-states',
        method: 'GET',
      }),
      transformResponse: (response: ListResponse<RegistrationState[]>) => {
        return response.data.map((state) => {
          return { value: state.code, label: state.name };
        });
      },
    }),
    getMPZStates: builder.query<Option[], void>({
      query: () => ({
        url: '/dictionaries/international-legal-protection-states',
        method: 'GET',
      }),
      transformResponse: (response: ListResponse<MPZState[]>) => {
        return response.data.map((state) => {
          return { value: state.code, label: state.name };
        });
      },
    }),
    getCoordinationStatuses: builder.query<Option[], void>({
      query: () => ({
        url: '/dictionaries/coordination-status',
        method: 'GET',
      }),
      transformResponse: (response: ListResponse<CoordinationStatus[]>) => {
        return response.data.map((status) => {
          return { value: status.code, label: status.name };
        });
      },
    }),
    getCountries: builder.query<Option[], void>({
      query: () => ({
        url: '/dictionaries/countries',
        method: 'GET',
        params: { sort: 'name' },
      }),
      transformResponse: (response: ListResponse<Country[]>) => {
        return response.data.map((contry) => {
          return { value: contry.id, label: contry.name };
        });
      },
    }),
    getConclusionAdditionsTemplates: builder.query<
      AdditionalConclusionData[],
      AdditionalConclusionRequestProps
    >({
      query: (data) => ({
        url: '/dictionaries/additional-conclusion-condition-templates/search',
        method: 'POST',
        data: {
          searchParameters: data,
        },
      }),
    }),
    deleteConclusionAdditionsTemplates: builder.mutation<void, { id: number }>({
      query: ({ id }) => ({
        url: `/dictionaries/additional-conclusion-condition-templates/${id}`,
        method: 'DELETE',
      }),
    }),
    getConclusionAdditionsDictionaryTemplates: builder.query<
      AdditionalConclusionDictionaryData,
      { id: number }
    >({
      query: ({ id }) => ({
        url: `/dictionaries/additional-conclusion-condition-templates/${id}`,
        method: 'GET',
      }),
    }),
    getRegistryNames: builder.query<
      Option[],
      { documentIds: number; sort?: string }
    >({
      query: ({ documentIds, sort = 'name' }) => ({
        url: '/dictionaries/registry-names/for-document-list',
        method: 'GET',
        params: { documentIds, sort },
      }),
      transformResponse: (response: ListResponse<RegistryName[]>) => {
        return response.data.map((registry) => {
          return { value: registry.id, label: registry.name };
        });
      },
    }),
    getRegistryPacketNames: builder.query<
      Option[],
      { id?: number; templateTypeId?: number; type?: string; sort?: string }
    >({
      query: ({ id, templateTypeId, type, sort = 'name' }) => ({
        url: '/dictionaries/registry-names',
        method: 'GET',
        params: { id, templateTypeId, type, sort },
      }),
      transformResponse: (response: ListResponse<RegistryPacketName[]>) => {
        return response.data.map((registry) => {
          return { value: registry.id, label: registry.name };
        });
      },
    }),
    getFrequencyMeasures: builder.query<Option[], void>({
      query: () => ({
        url: '/dictionaries/frequency-measure',
        method: 'GET',
      }),
      transformResponse: (response: ListResponse<FrequencyMeasure[]>) => {
        return response.data.map((measure) => {
          return {
            value: measure.koefficientToStandart,
            label: measure.abbreviation,
          };
        });
      },
    }),
    getOrganizationalLegalForms: builder.query<
      ListResponse<OrganizationalLegalForm[]>,
      void
    >({
      query: () => ({
        url: '/dictionaries/organizational-legal-forms?sort=name',
        method: 'GET',
      }),
    }),
    getContractorsWithParametrs: builder.query<Option[], ContractorParamsType>({
      query: ({ name, selectionType, documentTypeId }) => ({
        url: '/dictionaries/contractors',
        params: { name, selectionType, documentTypeId },
        method: 'GET',
      }),
      transformResponse: (response: ListResponse<ContractorType[]>) => {
        return response.data.map((contractor) => {
          return {
            value: contractor.id,
            label: contractor.name,
          };
        });
      },
    }),
    getAdditionalConclusionDataTemplate: builder.query<
      AdditionalConclusionDataTemplate,
      { id: number }
    >({
      query: ({ id }) => ({
        url: `/dictionaries/additional-conclusion-condition-templates/${id}`,
        method: 'GET',
      }),
    }),
    updateAdditionalConclusionTemplate: builder.mutation<
      AdditionalConclusionDataTemplate,
      { id: number; data: UpdateAdditionalConclusionDataTemplate }
    >({
      query: ({ id, data }) => ({
        url: `/dictionaries/additional-conclusion-condition-templates/${id}`,
        method: 'PUT',
        data: data,
      }),
    }),
    createAdditionalConclusionTemplate: builder.mutation<
      AdditionalConclusionDataTemplate,
      { data: UpdateAdditionalConclusionDataTemplate }
    >({
      query: ({ data }) => ({
        url: `/dictionaries/additional-conclusion-condition-templates`,
        method: 'POST',
        data: data,
      }),
    }),
    getPermissionTypes: builder.query<
      ListResponse<PermissionTypesType[]>,
      void
    >({
      query: () => ({
        url: '/dictionaries/permission-and-permission-base-document-types',
        method: 'GET',
      }),
    }),
    getFederalDistrics: builder.query<Option[], { sort?: string }>({
      query: ({ sort = 'federalDistrict' }) => ({
        url: '/dictionaries/federal-districts',
        method: 'GET',
        params: { sort },
      }),
      transformResponse: (value: any) => {
        return value.data.map((option: FederalDistrictsType) => ({
          value: option.federalDistrictCode,
          label: option.federalDistrict,
        }));
      },
    }),
    getInternalRegistryStates: builder.query<Option[], { sort?: string }>({
      query: ({ sort = 'name' }) => ({
        url: `/dictionaries/internal-registry-states`,
        params: { sort },
        method: 'GET',
      }),
      transformResponse: (response: ListResponse<InternalRegistryStates[]>) => {
        return response.data.map((state) => {
          return {
            value: state.code,
            label: state.name,
          };
        });
      },
    }),
    getSendingMethods: builder.query<
      Option[],
      { sort?: string; isActual?: boolean }
    >({
      query: ({ sort = 'name', isActual = true }) => ({
        url: `/dictionaries/sending-methods`,
        params: { sort, isActual },
        method: 'GET',
      }),
      transformResponse: (response: ListResponse<InternalRegistryStates[]>) => {
        return response.data.map((state) => {
          return {
            value: state.code,
            label: state.name,
          };
        });
      },
    }),
    getListOfCounterpartyIndex: builder.query<
      Option[],
      { id?: number; addressTypeCode?: string; deliveryTypeCode?: string }
    >({
      query: ({ id, addressTypeCode, deliveryTypeCode }) => ({
        url: `/dictionaries/contractors/${id}/addresses`,
        method: 'GET',
        params: { addressTypeCode, deliveryTypeCode },
      }),
      transformResponse: (value: any) => {
        return value.data.map((state: AdressesTransofrmListType) => {
          return {
            label: state.index,
            value: state.index,
          };
        });
      },
    }),
    getListOfCounterpartyAdresses: builder.query<
      Option[],
      { id?: number; addressTypeCode?: string; deliveryTypeCode?: string }
    >({
      query: ({ id, addressTypeCode, deliveryTypeCode }) => ({
        url: `/dictionaries/contractors/${id}/addresses`,
        method: 'GET',
        params: { addressTypeCode, deliveryTypeCode },
      }),
      transformResponse: (value: any) => {
        return value.data.map((state: AdressesTransofrmListType) => {
          return {
            label: state.address,
            value: state.address,
          };
        });
      },
    }),
    getMailPackages: builder.query<
      ListResponse<MailPackage[]>,
      MailPackageSearch
    >({
      query: ({ searchParameters, page = 1, size = 10, useCount = true }) => ({
        url: '/post-packets/search',
        method: 'POST',
        data: { searchParameters, page, size, useCount },
      }),
    }),
    getStagesTypes: builder.query<
      any,
      { parentTypeId: number; parentId: number } | null
    >({
      query: (params) => ({
        url: `/dictionaries/stage-types/new-stage-formation-data`,
        params: params ? params : {},
        method: 'GET',
      }),
    }),
  }),
});

export const {
  useGetDocumentAccessesQuery,
  useGetClerksQuery,
  useGetDocumentClerksQuery,
  useGetDocumentRubricsQuery,
  useGetDocumentTemplatesQuery,
  useGetDocumentTypesQuery,
  useGetDocumentRubricsOptionsQuery,
  useGetContractorsQuery,
  useGetDivisionsQuery,
  useGetInnerAddressesQuery,
  useGetAccessesQuery,
  useGetRubricsTypesQuery,
  useGetStatusesQuery,
  useGetResolutionDescriptionsQuery,
  useGetRoutesTypesQuery,
  useGetRequestTypesQuery,
  useGetRadioServiceQuery,
  useGetServiceMRFCQuery,
  useGetTechnologiesQuery,
  useGetFrequencyBandsQuery,
  useGetSSEQuery,
  useGetMultiplexesQuery,
  useGetProgramsQuery,
  useGetNetTypesQuery,
  useGetNetCategoriesQuery,
  useGetTechnologiesRKNQuery,
  useGetRegionsQuery,
  useGetContractorsListQuery,
  useGetAsyncContractorsListMutation,
  useGetTaskTypesQuery,
  useGetConclusionStatusesQuery,
  useGetConclusionTypesQuery,
  useGetConclusionSortsQuery,
  useGetDocumentTemplatesTreeQuery,
  useGetCounterpartiesQuery,
  useGetDefaultCounterpartiesQuery,
  useGetCounterpartiesAsyncMutation,
  useGetDeliveryTypesMutation,
  useGetCounterpartyPersonsMutation,
  useGetConclusionPeriodsQuery,
  useGetConclusionStatesQuery,
  useGetConclusionAnnulmentsQuery,
  useGetSolutionModalDataQuery,
  useGetCommunicationChannelsQuery,
  useGetRegistrationStatesQuery,
  useGetMPZStatesQuery,
  useGetCoordinationStatusesQuery,
  useGetCountriesQuery,
  useGetConclusionAdditionsTemplatesQuery,
  useDeleteConclusionAdditionsTemplatesMutation,
  useGetRegistryNamesQuery,
  useGetRegistryPacketNamesQuery,
  useGetFrequencyMeasuresQuery,
  useGetOrganizationalLegalFormsQuery,
  useGetContractorsWithParametrsQuery,
  useGetConclusionAdditionsDictionaryTemplatesQuery,
  useGetRequestStagesQuery,
  useGetCounterpartyPersonsListMutation,
  useGetAdditionalConclusionDataTemplateQuery,
  useUpdateAdditionalConclusionTemplateMutation,
  useCreateAdditionalConclusionTemplateMutation,
  useGetPermissionTypesQuery,
  useGetFederalDistricsQuery,
  useGetInternalRegistryStatesQuery,
  useGetSendingMethodsQuery,
  useGetListOfCounterpartyIndexQuery,
  useGetListOfCounterpartyAdressesQuery,
  useGetDefaultAddresseeDeliveryMutation,
  useGetMailPackagesQuery,
  useGetStagesTypesQuery,
} = dictionariesApi;

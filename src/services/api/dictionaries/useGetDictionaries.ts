import {
  useGetDocumentAccessesQuery,
  useGetDocumentClerksQuery,
  useGetDocumentRubricsQuery,
  useGetDocumentTemplatesQuery,
} from './index';

export interface GetDictionariesProps {
  accesses?: {
    sort?: string;
  };
  rubrics?: {
    documentTypeId?: number;
  };
  templates?: {
    rubricIds: number[];
  };
  clerks?: {
    isFired?: boolean;
  };
}

export const useGetDictionaries = ({
  accesses = {},
  rubrics = {},
  templates = { rubricIds: [] },
  clerks = {},
}: GetDictionariesProps) => {
  const { data: accessesData, isLoading: isFetchingAccesses } =
    useGetDocumentAccessesQuery(accesses);
  const { data: clerksData, isLoading: isFetchingClerks } =
    useGetDocumentClerksQuery(clerks);
  const { data: templatesData, isLoading: isFetchingTemplates } =
    useGetDocumentTemplatesQuery(templates, {
      skip: !templates.rubricIds.length,
    });
  const { data: rubricsData, isLoading: isFetchingRubrics } =
    useGetDocumentRubricsQuery(rubrics);

  return {
    dictionariesAccesses: accessesData?.data || [],
    dictionariesRubrics: rubricsData?.data || [],
    dictionariesTemplates: templatesData || [],
    dictionariesClerks: clerksData?.data || [],
    isLoading:
      isFetchingClerks ||
      isFetchingAccesses ||
      isFetchingRubrics ||
      isFetchingTemplates,
  };
};

import { createApi } from '@reduxjs/toolkit/query/react';

import { SearchResult } from '../../../types/fastSearch';
import { axiosBaseQuery } from '../../axios';

export const fastSearchApi = createApi({
  reducerPath: 'fastSearchApi',
  baseQuery: axiosBaseQuery(),
  tagTypes: ['FAST_SEARCH'],
  endpoints: (builder) => ({
    getFastSearch: builder.query<SearchResult, { searchValue: string }>({
      query: ({ searchValue }) => ({
        url: `/search-engine/`,
        params: { q: searchValue },
        method: 'GET',
      }),
    }),
  }),
});

export const { useGetFastSearchQuery } = fastSearchApi;

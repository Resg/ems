import { createApi } from '@reduxjs/toolkit/query/react';

import {
  CalculateDate,
  CalculateDateParams,
} from '../../../types/documentSearch';
import { axiosBaseQuery } from '../../axios';

export const documentSearchApi = createApi({
  reducerPath: 'documentSearchApi',
  baseQuery: axiosBaseQuery(),
  endpoints: (builder) => ({
    getDocuments: builder.query({
      query: (data) => ({
        url: '/documents/search',
        method: 'POST',
        data,
      }),
    }),
    getCalculateDate: builder.mutation<CalculateDate, CalculateDateParams>({
      query: ({ startDate, numberOfDays, dayType }) => ({
        url: '/documents/calculate-date',
        method: 'GET',
        params: { startDate, numberOfDays, dayType },
      }),
    }),
  }),
});

export const { useGetDocumentsQuery, useGetCalculateDateMutation } =
  documentSearchApi;

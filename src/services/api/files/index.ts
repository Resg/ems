import { createApi } from '@reduxjs/toolkit/query/react';

import { File } from '../../../types/files';
import { axiosBaseQuery } from '../../axios';

export const filesApi = createApi({
  reducerPath: 'filesApi',
  baseQuery: axiosBaseQuery(),
  endpoints: (builder) => ({
    getFileById: builder.query<File, { id: number }>({
      query: ({ id }) => ({
        url: `/files/${id}/content`,
        method: 'POST',
        body: {},
      }),
    }),
    deleteDocumentFile: builder.mutation<void, { id: number | string }>({
      query: ({ id }) => ({
        url: `/files/${id}/document`,
        method: 'DELETE',
      }),
    }),
    getFileInfo: builder.mutation<File, { id: number }>({
      query: ({ id }) => ({
        url: `/files/${id}/content`,
        method: 'POST',
        body: {},
      }),
    }),
  }),
});

export const {
  useGetFileByIdQuery,
  useDeleteDocumentFileMutation,
  useGetFileInfoMutation,
} = filesApi;

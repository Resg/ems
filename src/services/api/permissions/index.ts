import { createApi } from '@reduxjs/toolkit/query/react';

import { ListResponse } from '../../../types/api';
import {
  PermissionContractorType,
  PermissionSearchType,
  PermissionType,
} from '../../../types/permissions';
import { axiosBaseQuery } from '../../axios';

export const permissionApi = createApi({
  reducerPath: 'permissionApi',
  baseQuery: axiosBaseQuery(),
  endpoints: (builder) => ({
    getPermissions: builder.query<
      ListResponse<PermissionType[]>,
      PermissionSearchType
    >({
      query: ({ searchParameters, page, size }) => ({
        url: '/permissions/search',
        method: 'POST',
        data: {
          searchParameters,
          page,
          size,
          useCount: true,
        },
      }),
    }),
    getPermissionData: builder.query<any, { id?: number }>({
      query: ({ id }) => ({
        url: `/permissions/${id}`,
        method: 'GET',
      }),
    }),
    updatePermission: builder.mutation<any, { id: number; data: any }>({
      query: ({ id, data }) => ({
        url: `/permissions/${id}`,
        method: 'PUT',
        data: data,
      }),
    }),
    createPermission: builder.mutation<any, { data: any }>({
      query: ({ data }) => ({
        url: `/permissions`,
        method: 'POST',
        data: data,
      }),
    }),
    getPermissionContractorsById: builder.query<
      PermissionContractorType[],
      { permissionId: string | number }
    >({
      query: ({ permissionId }) => ({
        url: `/permissions/${permissionId}/permission-clients`,
        method: 'GET',
      }),
    }),
    getPermissionContractors: builder.query<
      PermissionContractorType[],
      { id: string | number }
    >({
      query: ({ id }) => ({
        url: `/permissions/${id}/contractors`,
        method: 'GET',
      }),
    }),
    getPermissionTerm: builder.query<
      { permissionExpired: string | number },
      { permissionId: string | number }
    >({
      query: ({ permissionId }) => ({
        url: `/permissions/${permissionId}/permission-term`,
        method: 'GET',
      }),
    }),
    selectNewPermission: builder.mutation<any, { data: any }>({
      query: ({ data }) => ({
        url: `/permissions/selection-new-request`,
        method: 'POST',
        data: data,
      }),
    }),
    getPermissionFiles: builder.query<
      any,
      { objectId: string | number; objectType: string }
    >({
      query: ({ objectId, objectType }) => ({
        url: `/files`,
        method: 'POST',
        data: { objectId, objectType },
      }),
    }),
    deletePermission: builder.mutation<any, { id: string }>({
      query: ({ id }) => ({
        url: `/permissions/${id}`,
        method: 'DELETE',
      }),
    }),
  }),
});

export const {
  useGetPermissionsQuery,
  useGetPermissionDataQuery,
  useUpdatePermissionMutation,
  useCreatePermissionMutation,
  useGetPermissionContractorsQuery,
  useGetPermissionContractorsByIdQuery,
  useGetPermissionTermQuery,
  useSelectNewPermissionMutation,
  useGetPermissionFilesQuery,
  useDeletePermissionMutation,
} = permissionApi;

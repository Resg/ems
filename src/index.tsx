import React from 'react';
import { SnackbarProvider } from 'notistack';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import { ErrorBoundary } from './components/ErrorBoundary';
import { UserService } from './services/auth';
import { initAxios } from './services/axios';
import App from './App';
import { store } from './store';

// import reportWebVitals from './reportWebVitals';
import './index.css';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

// reportWebVitals();
UserService.initKeycloak(() =>
  root.render(
    // <React.StrictMode>

    <Provider store={store}>
      <ErrorBoundary>
        <BrowserRouter>
          <SnackbarProvider hideIconVariant maxSnack={3}>
            <App />
          </SnackbarProvider>
        </BrowserRouter>
      </ErrorBoundary>
    </Provider>

    // </React.StrictMode>
  )
);
initAxios();

export const completedTasksCols = [
  { name: 'Тип', key: 'type', hidden: false },
  { name: 'Автор', key: 'author', hidden: false },
  { name: 'Дата создания', key: 'createDate', hidden: false },
  { name: 'Дата поступления', key: 'prepareDate', hidden: false },
  { name: 'Плановая дата выполнения', key: 'planDate', hidden: false },
  { name: 'Дата выполнения', key: 'completeDate', hidden: false },
  { name: 'Время выполнения', key: 'completeTime', hidden: false },
  { name: 'Статус', key: 'status', hidden: false },
  { name: 'Содержание резолюции', key: 'resolutionContent', hidden: false },
  {
    name: 'Пункт резолюции',
    key: 'resolutionControlParagraphString',
    hidden: false,
  },
  { name: 'Тип документа', key: 'documentType', hidden: false },
  { name: 'Рубрика', key: 'rubricList', hidden: false },
  { name: 'Содержание документа', key: 'documentDescription', hidden: false },
  { name: 'Корреспондент', key: 'contractorList', hidden: false },
  { name: 'Номер документа', key: 'documentNumber', hidden: false },
  { name: 'Дата документа', key: 'documentDate', hidden: false },
  { name: 'Статус документа', key: 'documentStatus', hidden: false },
  {
    name: 'Внутренний номер документа',
    key: 'documentInternalNumber',
    hidden: false,
  },
  {
    name: 'Автор документа',
    key: 'documentCreatedNameDateTime',
    hidden: false,
  },
  { name: 'Штриx-код документа', key: 'documentId', hidden: false },
  { name: 'Штриx-код ЭРК', key: 'mainDocumentIdList', hidden: false },
  { name: 'Штриx-код ДЭРК', key: 'includedDocumentIdList', hidden: false },
  { name: 'Номер заявки', key: 'requestList', hidden: false },
  { name: 'ИД задачи', key: 'id', hidden: true },
  { name: 'ИД резолюции', key: 'resolutionId', hidden: true },
];

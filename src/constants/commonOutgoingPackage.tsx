import { Checkbox } from '@mui/material';

import { FileLink } from '../components/FileLink';

import styles from '../containers/OutgoingPackets/Common/styles.module.scss';

export enum OutgoingPackageFormFields {
  NUMBER = 'localNumber',
  STATUS = 'status',
  CHANGE_DATE = 'changeDateTime',
  CONTRACTOR_SHORT_NAME = 'contractorShortName',
  CONTRACTOR_FULL_NAME = 'contractorFullName',
  DELIVERY_TYPE = 'deliveryType',
  TRUSTED_PERSON_NAME = 'trustedPersonName',
  RECIPIENT_PERSON = 'recipientPersonId',
  IS_EDITED_ADDRESS = 'isEditedAddress',
  INDEX = 'index',
  ADDRESS = 'address',
  EMAIL = 'email',
  FAX = 'fax',
  SENDING_DATE_TIME = 'sendingDateTime',
  DELIVERY_DATE_TIME = 'deliveryDateTime',
  COUNT_PAGES = 'countPages',
  COMMENTS = 'comments',
}

export const OutgoingPackageFormLabels: Record<
  OutgoingPackageFormFields,
  string
> = {
  [OutgoingPackageFormFields.NUMBER]: 'Номер',
  [OutgoingPackageFormFields.STATUS]: 'Состояние',
  [OutgoingPackageFormFields.CHANGE_DATE]: 'Дата изменения состояния',
  [OutgoingPackageFormFields.CONTRACTOR_SHORT_NAME]:
    'Краткое наименование адресата',
  [OutgoingPackageFormFields.CONTRACTOR_FULL_NAME]:
    'Полное наименование адресата',
  [OutgoingPackageFormFields.DELIVERY_TYPE]: 'Способ отправки',
  [OutgoingPackageFormFields.TRUSTED_PERSON_NAME]: 'Доверенное лицо',
  [OutgoingPackageFormFields.RECIPIENT_PERSON]: 'Кому',
  [OutgoingPackageFormFields.IS_EDITED_ADDRESS]: 'Ручной ввод',
  [OutgoingPackageFormFields.INDEX]: 'Индекс',
  [OutgoingPackageFormFields.ADDRESS]: 'Адрес',
  [OutgoingPackageFormFields.EMAIL]: 'E-mail',
  [OutgoingPackageFormFields.FAX]: 'Факс',
  [OutgoingPackageFormFields.SENDING_DATE_TIME]: 'Дата и время отправки',
  [OutgoingPackageFormFields.DELIVERY_DATE_TIME]: 'Дата и время доставки',
  [OutgoingPackageFormFields.COUNT_PAGES]: 'Количество страниц',
  [OutgoingPackageFormFields.COMMENTS]: 'Примечание',
};

export const commonDetailDefaults = {
  localNumber: '',
  status: '',
  changeDate: '',
  contractorShortName: '',
  contractorFullName: '',
  deliveryType: '',
  trustedPersonName: '',
  recipientPersonId: '',
  isEditedAddress: '',
  index: '',
  address: '',
  email: '',
  fax: '',
  sendingDateTime: '',
  deliveryDateTime: '',
  countPages: '',
  comments: '',
};

export const DocumentsTableCols = [
  {
    key: 'orderNumber',
    name: '№ п/п',
  },
  {
    key: 'toSendFile',
    name: 'К отправке',
    width: 100,
    formatter: ({ row }: any) => {
      return (
        <Checkbox
          checked={row.toSendFile}
          disabled
          className={styles.checkbox}
        />
      );
    },
  },
  {
    key: 'localNumber',
    name: 'Внутренний номер',
  },
  {
    key: 'rubricList',
    name: 'Рубрика',
  },
  {
    key: 'number',
    name: 'Номер',
  },

  {
    key: 'requestNumber',
    name: 'Номер заявки',
  },
  {
    key: 'fileName',
    name: 'Имя файла',
    formatter: ({ row }: any) => {
      return <FileLink name={row.fileName} rowId={row.fileId} />;
    },
  },
];

export const MailPackagesCols = [
  {
    key: 'localNumber',
    name: 'Номер',
  },
  {
    key: 'deliveryName',
    name: 'Способ отправки',
  },

  {
    key: 'sendingDateTime',
    name: 'Дата и время отправки',
  },
  {
    key: 'deliveryDate',
    name: 'Дата вручения',
  },
  {
    key: 'returnDate',
    name: 'Дата возврата',
  },
  {
    key: 'sendingsCount',
    name: 'Количество отправок ИП',
  },
  {
    key: 'notificationNumber',
    name: 'Номер уведомления',
  },
  {
    key: 'postRegistryNumber',
    name: 'Номер почтового реестра',
  },
  {
    key: 'postRegistryDateTime',
    name: 'Дата и время почтового реестра',
  },
  {
    key: 'comment',
    name: 'Примечание к почтовому пакету',
  },
];

export const RegistryCols = [
  {
    key: 'id',
    name: 'ИД',
    hidden: true,
  },
  {
    key: 'number',
    name: 'Номер',
    hidden: false,
  },
  {
    key: 'dateTime',
    name: 'Дата и время создания',
    hidden: false,
  },

  {
    key: 'type',
    name: 'Наименование',
    hidden: false,
  },
];

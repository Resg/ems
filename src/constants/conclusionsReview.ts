export enum ConclusionReviewTabs {
  COMMON = 'COMMON',
  CONDITIONS = 'CONDITIONS',
  ATTACHMENTS = 'ATTACHMENTS',
}

export enum ConclusionReviewFields {
  NUMBER = 'number',
  DATE = 'date',
  END_DATE = 'dateValidity',
  STATUS = 'status',
  TYPE = 'type',
  SORT = 'sort',
}

export const ConclusionReviewTabsLabel: Record<ConclusionReviewTabs, string> = {
  [ConclusionReviewTabs.COMMON]: 'Общие',
  [ConclusionReviewTabs.CONDITIONS]: 'Дополнительные условия',
  [ConclusionReviewTabs.ATTACHMENTS]: 'Приложения',
};

export const ConclusionReviewLabels: Record<ConclusionReviewFields, string> = {
  [ConclusionReviewFields.NUMBER]: 'Номер',
  [ConclusionReviewFields.DATE]: 'Дата',
  [ConclusionReviewFields.END_DATE]: 'Дата окончания действия',
  [ConclusionReviewFields.STATUS]: 'Статус',
  [ConclusionReviewFields.TYPE]: 'Тип',
  [ConclusionReviewFields.SORT]: 'Вид',
};
export const СonclusionReviewCols = [
  {
    key: 'number',
    name: 'Номер',
    hidden: false,
  },
  {
    key: 'date',
    name: 'Дата',
    hidden: false,
  },
  {
    key: 'validUntil',
    name: 'Дата окончания действия',
    hidden: false,
  },
  {
    key: 'sortName',
    name: 'Вид',
    hidden: false,
  },
  {
    key: 'contractorName',
    name: 'Заявитель',
    hidden: false,
  },
];

export enum ConclusionReviewSearchTabs {
  CONCLUSION = 'CONCLUSION',
}

export const ConclusionReviewSearchTabsLabel: Record<
  ConclusionReviewSearchTabs,
  string
> = {
  [ConclusionReviewSearchTabs.CONCLUSION]: 'Заключения',
};

export enum ConclusionReviewSearchFields {
  NUMBER = 'filters.number',
  CONTRACTOR = 'filters.contractorId',
  DATE_FROM = 'filters.dateFrom',
  DATE_TO = 'filters.dateTo',
  DATE = 'filters.isDate',
}

export const ConclusionReviewSearchLabel: Record<any, string> = {
  [ConclusionReviewSearchFields.NUMBER]: 'Номер заключения',
  [ConclusionReviewSearchFields.CONTRACTOR]: 'Заявитель',
  [ConclusionReviewSearchFields.DATE_FROM]: 'Дата заключения',
};

export const ConclusionReviewClearMap = {
  [ConclusionReviewSearchTabs.CONCLUSION]: {
    [ConclusionReviewSearchFields.NUMBER]: '',
    [ConclusionReviewSearchFields.CONTRACTOR]: '',
    [ConclusionReviewSearchFields.DATE_FROM]: '',
    [ConclusionReviewSearchFields.DATE_TO]: '',
    [ConclusionReviewSearchFields.DATE]: false,
  },
};

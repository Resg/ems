export enum RegistryTabs {
  COMMON = 'common',
  DOCUMENTS = 'documents',
  OUTCOMING_PACKETS = 'outcoming-packets',
}

export const RegistryTabsLabels: Record<RegistryTabs, string> = {
  [RegistryTabs.COMMON]: 'Общие',
  [RegistryTabs.DOCUMENTS]: 'Документы',
  [RegistryTabs.OUTCOMING_PACKETS]: 'Исходящие пакеты',
};

export enum RegistryFields {
  NUMBER = 'number',
  NAME = 'name',
  SENDER_DIVISION = 'senderDivision',
  RECIPIENT_DIVISION = 'recipientDivision',
  DATE = 'date',
  STATE = 'state',
  SENT_DATE_TIME_NAME = 'sentDateTimeName',
  RECEIVED_DATE_TIME_NAME = 'receivedDateTimeName',
  COMMENTS = 'comments',
}

export const RegistryLabels: Record<RegistryFields, string> = {
  [RegistryFields.NUMBER]: 'Номер',
  [RegistryFields.NAME]: 'Наименование',
  [RegistryFields.SENDER_DIVISION]: 'Отправитель',
  [RegistryFields.RECIPIENT_DIVISION]: 'Адресат',
  [RegistryFields.DATE]: 'Дата составления',
  [RegistryFields.STATE]: 'Состояние',
  [RegistryFields.SENT_DATE_TIME_NAME]: 'Информация об отправке',
  [RegistryFields.RECEIVED_DATE_TIME_NAME]: 'Информация о приеме',
  [RegistryFields.COMMENTS]: 'Примечание',
};

export const DefaultRegistry = {
  number: '',
  name: '',
  senderDivision: null,
  recipientDivision: null,
  date: null,
  state: '',
  sentDateTimeName: '',
  receivedDateTimeName: '',
  comments: '',
};

export const RegistrySearchCols = [
  {
    key: 'id',
    name: 'ИД',
  },
  {
    key: 'number',
    name: 'Номер',
  },
  {
    key: 'date',
    name: 'Дата создания',
  },
  {
    key: 'sentDateTimeName',
    name: 'Отправлен',
  },
  {
    key: 'sentTime',
    name: 'Время отправки',
  },
  {
    key: 'receivedDateTimeName',
    name: 'Принят',
  },
  {
    key: 'receivedTime',
    name: 'Время принятия',
  },
  {
    key: 'senderDivision',
    name: 'Подразделение-отправитель',
  },
  {
    key: 'senderName',
    name: 'Сотрудник-отправитель',
  },
  {
    key: 'name',
    name: 'Наименование',
  },
  {
    key: 'recipientDivision',
    name: 'Подразделение-получатель',
  },
  {
    key: 'recipientName',
    name: 'Сотрудник-получатель',
  },
  {
    key: 'type',
    name: 'Тип',
    archive: true,
  },
  {
    key: 'state',
    name: 'Статус',
    archive: true,
  },
  {
    key: 'template',
    name: 'Объект передачи',
    archive: true,
  },
];
export enum AddToRegistryFields {
  ADDRESSEE = 'addressee',
  REGISTRY_NAME_ID = 'registryNameId',
  CREATE_NEW = 'createNew',
  COMMENTS = 'comments',
}

export const AddToRegistryLabels: Record<AddToRegistryFields, string> = {
  [AddToRegistryFields.ADDRESSEE]: 'Адресат',
  [AddToRegistryFields.REGISTRY_NAME_ID]: 'Наименование',
  [AddToRegistryFields.CREATE_NEW]: '',
  [AddToRegistryFields.COMMENTS]: 'Примечание',
};

export const DefaultAddToRegistry = {
  addressee: null,
  registryNameId: null,
  createNew: 'false',
  comments: '',
};

export const AddToRegistryCols = [
  {
    key: 'number',
    name: 'Номер документа',
  },
  {
    key: 'code',
    name: 'Штрих-код документа',
  },
  {
    key: 'orderNumberName',
    name: 'Экземпляр',
  },
];

export const RegistryOutPacketsCols = [
  {
    key: 'number',
    name: 'Номер исх. пакета',
  },
  {
    key: 'dateSign',
    name: 'Дата подписания',
  },
  {
    key: 'dateRegistration',
    name: 'Дата регистрации',
  },
  {
    key: 'contractorName',
    name: 'Адресат',
  },
  {
    key: 'typeDelivery',
    name: 'Способ отправки',
  },
  {
    key: 'rubricList',
    name: 'Рубрики',
  },
  {
    key: 'state',
    name: 'Статус',
  },
];

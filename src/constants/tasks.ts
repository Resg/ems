import { ExecutionButtonActions } from '../components/Execution/ExecutionButton/constants';

export enum TaskTabs {
  COMMON = 'common',
  DOCUMENTS = 'documents',
}

export const TaskTabsLabels: Record<TaskTabs, string> = {
  [TaskTabs.COMMON]: 'Общие',
  [TaskTabs.DOCUMENTS]: 'Связанные документы',
};

export const DocumentTasksExecutionButtons = [
  ExecutionButtonActions.EXECUTE,
  ExecutionButtonActions.ENDORSE,
  ExecutionButtonActions.REJECT,
  ExecutionButtonActions.CANCEL,
  ExecutionButtonActions.SEND,
  ExecutionButtonActions.SIGN,
  ExecutionButtonActions.APPROVE,
  ExecutionButtonActions.REASSIGN,
  ExecutionButtonActions.CONFIRM,
  ExecutionButtonActions.REEDIT,
];

export const RequestTasksExecutionButtons = [
  ExecutionButtonActions.EXECUTE,
  ExecutionButtonActions.ENDORSE,
  ExecutionButtonActions.REJECT,
  ExecutionButtonActions.CANCEL,
  ExecutionButtonActions.SEND,
  ExecutionButtonActions.SIGN,
  ExecutionButtonActions.APPROVE,
  ExecutionButtonActions.REASSIGN,
  ExecutionButtonActions.CONFIRM,
  ExecutionButtonActions.REEDIT,
];

import { ExecutionButtonActions } from '../components/Execution/ExecutionButton/constants';

export const MyTasksColumns = [
  {
    key: 'title',
    name: 'Задача',
    width: 150,
    hidden: false,
  },
  {
    key: 'author',
    name: 'Автор',
    width: 150,
    hidden: false,
  },
  {
    key: 'performer',
    name: 'Исполнитель',
    width: 200,
  },
  {
    key: 'comments',
    name: 'Комментарий',
    width: 200,
  },
  {
    key: 'performerComment',
    name: 'Комментарий по результату',
    width: 220,
  },
  {
    key: 'status',
    name: 'Статус',
    width: 100,
  },
  {
    key: 'termLabel',
    name: 'Дата исполнения/Срок исполнения (р/д)',
    width: 275,
  },
  {
    key: 'completeDate',
    name: 'Дата и время исполнения фактическая',
    width: 295,
  },
  {
    key: 'result',
    name: 'Результат',
    width: 190,
  },
  {
    key: 'createDate',
    name: 'Дата и время создания',
    width: 190,
  },
  {
    key: 'editor',
    name: 'Редактор',
    width: 190,
  },
  {
    key: 'id',
    name: 'Номер задачи',
    width: 150,
  },
  {
    key: 'externalTaskId',
    name: 'Номер задачи внешний',
    width: 180,
  },
  {
    key: 'objectLabel',
    name: 'Объект',
    width: 100,
  },
];

export const IncomingMyTasksExecutionButtons = [
  ExecutionButtonActions.EXECUTE,
  ExecutionButtonActions.ENDORSE,
  ExecutionButtonActions.REJECT,
  ExecutionButtonActions.CANCEL,
  ExecutionButtonActions.SEND,
  ExecutionButtonActions.SIGN,
  ExecutionButtonActions.APPROVE,
  ExecutionButtonActions.REASSIGN,
  ExecutionButtonActions.CONFIRM,
  ExecutionButtonActions.REEDIT,
];

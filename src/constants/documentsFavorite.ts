export const documentsFavoriteCols = [
  { name: 'Тип', key: 'type', hidden: false },
  { name: 'Номер', key: 'number', hidden: false },
  { name: 'Внутренний номер', key: 'localNumber', hidden: false },
  { name: 'Дата', key: 'date', hidden: false },
  { name: 'Корреспондент/Адресат', key: 'contractorList', hidden: false },
  { name: 'Рубрики', key: 'rubricList', hidden: false },
  { name: 'Содержание', key: 'description', hidden: false },
  { name: 'Примечание', key: 'comments', hidden: false },
  { name: 'Исполнитель', key: 'performer', hidden: false },
  { name: 'Статус', key: 'status', hidden: false },
];

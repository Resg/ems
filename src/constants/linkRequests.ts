export const LinkRequestsCols = [
  {
    key: 'number',
    name: 'Номер',
  },
  {
    key: 'date',
    name: 'Дата',
  },
  {
    key: 'clientName',
    name: 'Заявитель',
  },
  {
    key: 'manager',
    name: 'Менеджер',
  },
  {
    key: 'lastStage',
    name: 'Последний этап',
  },
  {
    key: 'lastStageDate',
    name: 'Дата последнего этапа',
  },
];

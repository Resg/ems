export enum TasksCompletedSearchTabs {
  TASK = 'TASK',
  RESOLUTION = 'RESOLUTION',
  DOCUMENT = 'DOCUMENT',
  BARCODE_LIST = 'BARCODE_LIST',
}

export const TasksCompletedSearchLabel: Record<
  TasksCompletedSearchTabs,
  string
> = {
  [TasksCompletedSearchTabs.TASK]: 'Задача',
  [TasksCompletedSearchTabs.RESOLUTION]: 'Резолюция',
  [TasksCompletedSearchTabs.DOCUMENT]: 'Документ',
  [TasksCompletedSearchTabs.BARCODE_LIST]: 'Список Штрих-кодов',
};

export enum TasksCompletedSearchFields {
  ISONLYMAIN = 'filters.isOnlyMain',
  TYPE = 'filters.typeId',
  AUTHOR = 'filters.authorIds',
  DATECREATIONFROM = 'filters.createDateFrom',
  DATECREATIONTO = 'filters.createDateTo',
  DATEPREPAREFROM = 'filters.prepareDateFrom',
  DATEPREPARETO = 'filters.prepareDateTo',
  DATEPLANFROM = 'filters.planDateFrom',
  DATEPLANTO = 'filters.planDateTo',
  DATECOMPLETEFROM = 'filters.completeDateFrom',
  DATECOMPLETETO = 'filters.completeDateTo',
  DOCUMENT_TYPE = 'filters.documentTypeId',
  NUMBER = 'filters.documentNumber',
  DOCUMENT_INTERNAL_NUMBER = 'filters.documentInternalNumber',
  DOCUMENT_DATE_FROM = 'filters.documentDateFrom',
  DOCUMENT_DATE_TO = 'filters.documentDateTo',
  RUBRICS = 'filters.rubricIds',
  CONTRACTORS = 'filters.contractorIds',
  DOCUMENTS_AUTHORS = 'filters.documentAuthorIds',
  DOCUMENTS_DESCRIPTION = 'filters.documentDescription',
  RESOLUTION_CONTENT = 'filters.resolutionContent',
  DOCUMENT_IDS = 'filters.documentIds',
}

export const TasksCompletedSearchLabels: Record<string, string> = {
  [TasksCompletedSearchFields.TYPE]: 'Тип',
  [TasksCompletedSearchFields.AUTHOR]: 'Автор',
  [TasksCompletedSearchFields.RESOLUTION_CONTENT]: 'Содержание',
  [TasksCompletedSearchFields.DOCUMENT_IDS]: 'Список штрих-кодов',
  [TasksCompletedSearchFields.DOCUMENT_TYPE]: 'Тип',
  [TasksCompletedSearchFields.NUMBER]: 'Номер',
  [TasksCompletedSearchFields.DOCUMENT_INTERNAL_NUMBER]: 'Внутренний номер',
  [TasksCompletedSearchFields.RUBRICS]: 'Рубрики',
  [TasksCompletedSearchFields.CONTRACTORS]: 'Корреспондент',
  [TasksCompletedSearchFields.DOCUMENTS_AUTHORS]: 'Автор',
  [TasksCompletedSearchFields.DOCUMENTS_DESCRIPTION]: 'Содержание',
};

export const TasksCompletedClearMap = {
  [TasksCompletedSearchTabs.TASK]: {
    [TasksCompletedSearchFields.ISONLYMAIN]: null,
    [TasksCompletedSearchFields.TYPE]: '',
    [TasksCompletedSearchFields.AUTHOR]: '',
    [TasksCompletedSearchFields.DATECREATIONFROM]: '',
    [TasksCompletedSearchFields.DATECREATIONTO]: '',
    [TasksCompletedSearchFields.DATEPREPAREFROM]: '',
    [TasksCompletedSearchFields.DATEPREPARETO]: '',
    [TasksCompletedSearchFields.DATEPLANFROM]: '',
    [TasksCompletedSearchFields.DATEPLANTO]: '',
    [TasksCompletedSearchFields.DATECOMPLETEFROM]: '',
    [TasksCompletedSearchFields.DATECOMPLETETO]: '',
  },
  [TasksCompletedSearchTabs.RESOLUTION]: {
    [TasksCompletedSearchFields.RESOLUTION_CONTENT]: '',
  },
  [TasksCompletedSearchTabs.DOCUMENT]: {},
  [TasksCompletedSearchTabs.BARCODE_LIST]: {
    [TasksCompletedSearchFields.DOCUMENT_IDS]: '',
  },
  [TasksCompletedSearchTabs.RESOLUTION]: {},
  [TasksCompletedSearchTabs.DOCUMENT]: {
    [TasksCompletedSearchFields.DOCUMENT_TYPE]: '',
    [TasksCompletedSearchFields.NUMBER]: '',
    [TasksCompletedSearchFields.DOCUMENT_INTERNAL_NUMBER]: '',
    [TasksCompletedSearchFields.DOCUMENT_DATE_FROM]: '',
    [TasksCompletedSearchFields.DOCUMENT_DATE_TO]: '',
    [TasksCompletedSearchFields.RUBRICS]: [],
    [TasksCompletedSearchFields.CONTRACTORS]: [],
    [TasksCompletedSearchFields.DOCUMENTS_AUTHORS]: [],
    [TasksCompletedSearchFields.DOCUMENTS_DESCRIPTION]: '',
  },
  [TasksCompletedSearchTabs.BARCODE_LIST]: {},
};

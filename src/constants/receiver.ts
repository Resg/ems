export const ReceiverCols = [
  {
    key: 'type',
    name: 'Тип',
    width: 40,
  },
  {
    key: 'name',
    name: 'Краткое наименование/ФИО',
    width: 220,
  },
  {
    key: 'fullName',
    name: 'Полное наименование',
    width: 190,
  },
  {
    key: 'ownership',
    name: 'Организационно-правовая форма ЮЛ',
    width: 300,
  },
  {
    key: 'inn',
    name: 'ИНН',
    width: 200,
  },
  {
    key: 'ogrn',
    name: 'ОГРН',
    width: 180,
  },
  {
    key: 'legalAddress',
    name: 'Юридический адрес',
    width: 200,
  },
  {
    key: 'postalAddress',
    name: 'Почтовый адрес',
    width: 200,
  },
  {
    key: 'comments',
    name: 'Примечание',
    width: 200,
  },
];

export const DeliveryCols = [
  {
    key: 'contractorName',
    name: 'Контрагент',
    headerRenderer: () => {
      return null;
    },
    formatter: () => null,
    width: 80,
  },
  { key: 'deliveryType', name: 'Способ доставки' },
  {
    key: 'addressAndContactInfoView',
    name: 'Адрес/Контактная информация',
  },
  {
    key: 'fullName',
    name: 'Кому',
    sortable: false,
  },
  {
    key: 'selectDelivery',
    name: 'Выбрать',
    sortable: false,
    width: 100,
  },
];

export const DeliveryGroupCols = [
  { key: 'contractorName', name: 'Контрагент' },
  { key: 'deliveryType', name: 'Способ доставки' },
  {
    key: 'addressAndContactInfoView',
    name: 'Адрес/Контактная информация',
  },
];

export enum DetailedStageDataFields {
  NAME = 'name',
  EXECUTE_AFTER = 'executeAfter',
  COMMENTS = 'comments',
}

export const DetailedStageDataLabels: Record<DetailedStageDataFields, string> =
  {
    [DetailedStageDataFields.NAME]: 'Наименование',
    [DetailedStageDataFields.EXECUTE_AFTER]: 'Выполнять после',
    [DetailedStageDataFields.COMMENTS]: 'Комментарий',
  };

export const DefaultDetailedStageData = {
  name: null,
  executeAfter: [],
  comments: '',
};

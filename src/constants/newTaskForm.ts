export enum TaskFormFields {
  TYPE_ID = 'typeId',
  COMMENTS = 'comments',
  TERM = 'term',
  PERFORMER_ID = 'performerId',
  IS_CONTROL = 'isControl',
  PREVIOUS_ID = 'previousId',
}

export const TaskFormLabels: Record<TaskFormFields, string> = {
  [TaskFormFields.TYPE_ID]: 'Тип задачи',
  [TaskFormFields.COMMENTS]: 'Комментарий',
  [TaskFormFields.TERM]: 'Срок исполнения (р/д)',
  [TaskFormFields.PERFORMER_ID]: 'Исполнитель',
  [TaskFormFields.IS_CONTROL]: 'Контроль исполнения',
  [TaskFormFields.PREVIOUS_ID]: 'Выполнять после',
};

export const DefaultTaskFormFields = {
  typeId: null,
  comments: '',
  term: '',
  performerId: null,
  isControl: false,
  previousId: null,
};

export const SolutionsCols = [
  {
    key: 'index',
    name: 'Номер п/п',
  },
  {
    key: 'number',
    name: 'Номер решения',
  },
  {
    key: 'date',
    name: 'Дата решения',
  },
  {
    key: 'dateEnd',
    name: 'Дата окончания действия',
  },
  {
    key: 'logDescription',
    name: 'Добавлено в заявку',
  },
];

export const SolutionModalCols = [
  {
    key: 'number',
    name: 'Номер решения',
  },
  {
    key: 'date',
    name: 'Дата решения',
  },
  {
    key: 'dateEnd',
    name: 'Дата окончания действия',
  },
];

export enum SolutionModalFields {
  DATE_PERIOD_START = 'filters.datePeriodStart',
  DATE_PERIOD_END = 'filters.datePeriodEnd',
}

export const SolutionModalLabels: Record<SolutionModalFields, string> = {
  [SolutionModalFields.DATE_PERIOD_START]: 'Начало периода',
  [SolutionModalFields.DATE_PERIOD_END]: 'Окончание периода',
};

export const DefaultSolutionModal = {
  datePeriodStart: null,
  datePeriodEnd: null,
};

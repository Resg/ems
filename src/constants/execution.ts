export enum ExecutionFields {
  REQUEST_MANAGER = 'requestManagerId',
  PERFORMER_ID = 'performerId',
  TERM_WORK_DAYS = 'termWorkDays',
  FILE_LIST = 'fileList',
  COMMENTS = 'comments',
}

export const ExecutionLabels: Record<ExecutionFields, string> = {
  [ExecutionFields.COMMENTS]: 'Комментарий',
  [ExecutionFields.FILE_LIST]: 'Файлы',
  [ExecutionFields.PERFORMER_ID]: 'Исполнитель',
  [ExecutionFields.REQUEST_MANAGER]: 'Менеджер заявки',
  [ExecutionFields.TERM_WORK_DAYS]: 'Срок (р/д)',
};

export const ExecutionFormInitialValues = {
  [ExecutionFields.COMMENTS]: '',
  [ExecutionFields.FILE_LIST]: '',
  [ExecutionFields.PERFORMER_ID]: null,
  [ExecutionFields.REQUEST_MANAGER]: null,
  [ExecutionFields.TERM_WORK_DAYS]: null,
};

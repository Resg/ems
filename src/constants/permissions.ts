export const PermissionCols = [
  {
    key: 'contractorName',
    name: 'Заявитель',
    hidden: false,
  },
  {
    key: 'number',
    name: 'Номер',
    hidden: false,
  },
  {
    key: 'date',
    name: 'Дата',
    hidden: false,
  },
  {
    key: 'dateValidity',
    name: 'Срок действия',
    hidden: false,
  },
  {
    key: 'dateAnnulment',
    name: 'Дата аннулирования',
    hidden: false,
  },
  {
    key: 'type',
    name: 'Тип',
    hidden: false,
  },
  {
    key: 'state',
    name: 'Состояние',
    hidden: false,
  },
  {
    key: 'baseDocument',
    name: 'Тип основания',
    hidden: false,
  },
  {
    key: 'serviceMRFC',
    name: 'Служба ГРЧЦ',
    hidden: false,
  },
  {
    key: 'technology',
    name: 'Технология',
    hidden: false,
  },
  {
    key: 'regionList',
    name: 'Регион',
    hidden: false,
  },
  {
    key: 'federalDistrictList',
    name: 'Федеральный округ',
    hidden: false,
  },
  {
    key: 'stationsCount',
    name: 'Количество РЭС',
    hidden: false,
  },
];

export enum PermissionTabs {
  COMMON = 'common',
  PCTR = 'pctr',
  SCHEME = 'scheme',
  REQUESTS = 'requests',
  PROTOCOL = 'protocol',
}

export const PermissionTabsLabels: Record<PermissionTabs, string> = {
  [PermissionTabs.COMMON]: 'Общие',
  [PermissionTabs.PCTR]: 'ПЧТР',
  [PermissionTabs.SCHEME]: 'Схема',
  [PermissionTabs.REQUESTS]: 'Заявки',
  [PermissionTabs.PROTOCOL]: 'Протокол',
};

export enum PermissionFormFields {
  NUMBER = 'number',
  DATE = 'date',
  END_DATE = 'dateValidity',
  TYPE = 'type',
  STATE = 'stateCode',
  BASE_DOCUMENT = 'baseDocument',
  BASE_DOCUMENT_NUMBER = 'baseDocumentNumber',
  OBJECTID = 'baseObjectId',
  DATE_ANNULMENT = 'dateAnnulment',
  CLIENT = 'contractorIds',
  REASON = 'annulmentReasonCode',
  COMMENT = 'comment',
}

export const PermissionFormLabels: Record<PermissionFormFields, string> = {
  [PermissionFormFields.NUMBER]: 'Номер',
  [PermissionFormFields.DATE]: 'Дата',
  [PermissionFormFields.END_DATE]: 'Срок действия',
  [PermissionFormFields.TYPE]: 'Тип',
  [PermissionFormFields.STATE]: 'Состояние',
  [PermissionFormFields.BASE_DOCUMENT]: 'Тип основания',
  [PermissionFormFields.BASE_DOCUMENT_NUMBER]: 'Основание',
  [PermissionFormFields.DATE_ANNULMENT]: 'Дата аннулирования',
  [PermissionFormFields.CLIENT]: 'Заявитель',
  [PermissionFormFields.REASON]: 'Причина аннулирования',
  [PermissionFormFields.COMMENT]: 'Примечание',
  [PermissionFormFields.OBJECTID]: 'ИД объекта',
};

export const PermissionFormDefaults = {
  [PermissionFormFields.NUMBER]: '',
  [PermissionFormFields.DATE]: '',
  [PermissionFormFields.END_DATE]: '',
  [PermissionFormFields.TYPE]: '',
  [PermissionFormFields.STATE]: '',
  [PermissionFormFields.BASE_DOCUMENT]: '',
  [PermissionFormFields.BASE_DOCUMENT_NUMBER]: '',
  [PermissionFormFields.DATE_ANNULMENT]: '',
  [PermissionFormFields.CLIENT]: null,
  [PermissionFormFields.REASON]: '',
  [PermissionFormFields.COMMENT]: '',
  [PermissionFormFields.OBJECTID]: '',
};

export enum PermissionSearchTabs {
  PERMISSION = 'PERMISSION',
  LIST_OF_NUMBERS = 'LIST_OF_NUMBERS',
  REQUEST = 'REQUEST',
}

export const PermissionSearchLabel: Record<PermissionSearchTabs, string> = {
  [PermissionSearchTabs.PERMISSION]: 'Разрешение',
  [PermissionSearchTabs.LIST_OF_NUMBERS]: 'Список номеров',
  [PermissionSearchTabs.REQUEST]: 'Заявка',
};

export enum PermissionSearchFields {
  CONTRACTOR = 'filters.contractorIds',
  NUMBER = 'filters.number',
  DATE_FROM = 'filters.dateFrom',
  DATE_TO = 'filters.dateTo',
  DATE_VALIDITY_FROM = 'filters.dateValidityFrom',
  DATE_VALIDITY_TO = 'filters.dateValidityTo',
  DATE_ANNULMENT_FROM = 'filters.dateAnnulmentFrom',
  DATE_ANNULMENT_TO = 'filters.dateAnnulmentTo',
  CODE = 'filters.typeCodes',
  STATECODE = 'filters.stateCodes',
  BASEDOCUMENTCODES = 'filters.baseDocumentCodes',
  REGION = 'filters.aoguids',
  FEDERALDISTRICTCODE = 'filters.federalDistrictCode',
  NUMBERS = 'filters.numbers',
}

export const PermissionSearchLabels: Record<string, string> = {
  [PermissionSearchFields.CONTRACTOR]: 'Заявитель',
  [PermissionSearchFields.NUMBER]: 'Номер',
  [PermissionSearchFields.DATE_FROM]: 'Дата',
  [PermissionSearchFields.DATE_VALIDITY_FROM]: 'Срок действия',
  [PermissionSearchFields.DATE_ANNULMENT_FROM]: 'Дата аннулирования',
  [PermissionSearchFields.CODE]: 'Тип',
  [PermissionSearchFields.STATECODE]: 'Состояние',
  [PermissionSearchFields.BASEDOCUMENTCODES]: 'Тип основания',
  [PermissionSearchFields.REGION]: 'Регион',
  [PermissionSearchFields.FEDERALDISTRICTCODE]: 'Федеральный округ',
};

export enum ApplicationSearchFields {
  REQUEST_NUMBER = 'filters.requestNumber',
  REQUEST_DATE_FROM = 'filters.requestDateFrom',
  REQUEST_DATE_TO = 'filters.requestDateTo',
  SERVICE_MRFC = 'filters.serviceMRFCIds',
  COMMUNICATION_TECNOLOGY = 'filters.technologyIds',
}

export const ApplicationSearchLabels: Record<string, string> = {
  [ApplicationSearchFields.REQUEST_NUMBER]: 'Номер',
  [ApplicationSearchFields.REQUEST_DATE_FROM]: 'Дата',
  [ApplicationSearchFields.SERVICE_MRFC]: 'Служба ГРЧЦ',
  [ApplicationSearchFields.COMMUNICATION_TECNOLOGY]: 'Технология связи',
};

export const PermissionClearMap = {
  [PermissionSearchTabs.PERMISSION]: {
    [PermissionSearchFields.CONTRACTOR]: [],
    [PermissionSearchFields.NUMBER]: '',
    [PermissionSearchFields.DATE_FROM]: '',
    [PermissionSearchFields.DATE_TO]: '',
    [PermissionSearchFields.DATE_VALIDITY_FROM]: '',
    [PermissionSearchFields.DATE_VALIDITY_TO]: '',
    [PermissionSearchFields.DATE_ANNULMENT_FROM]: '',
    [PermissionSearchFields.DATE_ANNULMENT_TO]: '',
    [PermissionSearchFields.CODE]: [],
    [PermissionSearchFields.STATECODE]: [],
    [PermissionSearchFields.BASEDOCUMENTCODES]: [],
    [PermissionSearchFields.REGION]: '',
    [PermissionSearchFields.FEDERALDISTRICTCODE]: '',
  },
  [PermissionSearchTabs.LIST_OF_NUMBERS]: {},
  [PermissionSearchTabs.REQUEST]: {
    [ApplicationSearchFields.REQUEST_NUMBER]: '',
    [ApplicationSearchFields.REQUEST_DATE_FROM]: '',
    [ApplicationSearchFields.REQUEST_DATE_TO]: '',
    [ApplicationSearchFields.SERVICE_MRFC]: [],
    [ApplicationSearchFields.COMMUNICATION_TECNOLOGY]: [],
  },
};

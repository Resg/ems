import { ExecutionButtonActions } from '../components/Execution/ExecutionButton/constants';

export const DraftsCols = [
  {
    key: 'type',
    name: 'Тип задачи',
  },
  {
    key: 'status',
    name: 'Статус',
  },
  {
    key: 'documentType',
    name: 'Тип документа',
  },
  {
    key: 'rubricList',
    name: 'Рубрика',
  },
  {
    key: 'documentDescription',
    name: 'Содержание',
  },
  {
    key: 'contractorList',
    name: 'Корреспондент',
  },

  {
    key: 'documentNumber',
    name: 'Номер документа',
  },
  {
    key: 'documentDate',
    name: 'Дата документа',
  },
  {
    key: 'author',
    name: 'Автор',
  },
  {
    key: 'planDate',
    name: 'Срок исполнения',
  },
  {
    key: 'documentLocalNumber',
    name: 'Внутренний номер документа',
  },
  {
    key: 'documentCreatedNameDateTime',
    name: 'Автор документа',
  },
];

export const IncomingDraftsExecutionButtons = [
  ExecutionButtonActions.EXECUTE,
  ExecutionButtonActions.ENDORSE,
  ExecutionButtonActions.REJECT,
  ExecutionButtonActions.CANCEL,
  ExecutionButtonActions.SEND,
  ExecutionButtonActions.SIGN,
  ExecutionButtonActions.INWORK,
  ExecutionButtonActions.REASSIGN,
  ExecutionButtonActions.CONFIRM,
  ExecutionButtonActions.REEDIT,
  ExecutionButtonActions.REOPEN,
  ExecutionButtonActions.OPEN_AGAIN,
  ExecutionButtonActions.APPROVE,
];

import { format } from 'date-fns';

export enum DetailFormFields {
  NAME = 'stageName',
  PERIOD = 'controlPeriod',
  PERIOD_TYPE = 'controlPeriodType',
  PLAN_DATE = 'planDate',
  DIVISION = 'parentDivisionName',
  GROUP = 'divisionName',
  CLERK = 'performerClerkName',
  COMMENTS = 'comments',
  START_DATE = 'startDate',
  END_DATE = 'endDate',
  CREATOR = 'creatorDateName',
  EDITOR = 'editorDateName',
}

export const detailFormLabels: Record<DetailFormFields, string> = {
  [DetailFormFields.NAME]: 'Наименование',
  [DetailFormFields.PERIOD]: 'Контрольный срок',
  [DetailFormFields.PERIOD_TYPE]: 'Тип контрольного срока',
  [DetailFormFields.PLAN_DATE]: 'Дата контрольного срока',
  [DetailFormFields.DIVISION]: 'Подразделение',
  [DetailFormFields.GROUP]: 'Группа или отдел',
  [DetailFormFields.CLERK]: 'Сотрудник',
  [DetailFormFields.COMMENTS]: 'Комментарии',
  [DetailFormFields.START_DATE]: 'Начало этапа',
  [DetailFormFields.END_DATE]: 'Завершение этапа',
  [DetailFormFields.CREATOR]: 'Ввел',
  [DetailFormFields.EDITOR]: 'Последние',
};

export const stageDetailDefaults = {
  controlPeriod: '',
  controlPeriodType: '',
  planDate: '',
  parentDivisionName: null,
  divisionName: null,
  performerClerkName: null,
  comments: '',
  startDate: format(new Date(), 'yyyy-MM-dd'),
  endDate: '',
  creatorDateName: '',
  editorDateName: '',
};

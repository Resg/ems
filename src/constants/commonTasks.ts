import { format } from 'date-fns';

export enum DetailFormFields {
  PREVIOUS_TITLE = 'previousTitle',
  PREVIOUS_TITLE_STAGE = 'previousTitleStage',
  TITLE = 'title',
  TYPE_TITLE = 'typeTitle',
  AUTHOR = 'authorId',
  PERFORMER = 'performerId',
  TODO_AFTER = 'previousId',
  TERM = 'term',
  PLAN_DATE = 'planDate',
  COMMENTS = 'comments',
  PERFORMER_COMMENT = 'performerComment',
  IS_CONTROL = 'isControl',
}

export const detailFormLabels: Record<DetailFormFields, string> = {
  [DetailFormFields.PREVIOUS_TITLE]: 'Основная задача',
  [DetailFormFields.PREVIOUS_TITLE_STAGE]: 'Этап',
  [DetailFormFields.TITLE]: 'Тип подзадачи',
  [DetailFormFields.TYPE_TITLE]: 'Задача',
  [DetailFormFields.AUTHOR]: 'Автор',
  [DetailFormFields.PERFORMER]: 'Исполнитель',
  [DetailFormFields.TODO_AFTER]: 'Выполнять после',
  [DetailFormFields.TERM]: 'Срок исполнения (р/д)',
  [DetailFormFields.PLAN_DATE]: 'Срок исполнения (дата)',
  [DetailFormFields.COMMENTS]: 'Комментарии',
  [DetailFormFields.PERFORMER_COMMENT]: 'Комментарии исполнителя',
  [DetailFormFields.IS_CONTROL]: 'Контроль исполнения',
};

export const commonDetailDefaults = {
  title: '',
  previousTitle: '',
  authorId: null,
  performerId: null,
  previousId: null,
  term: null,
  planDate: format(new Date(), 'yyyy-MM-dd'),
  comments: '',
  performerComment: '',
  isControl: false,
};

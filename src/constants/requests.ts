import { ExecutionButtonActions } from '../components/Execution/ExecutionButton/constants';

export const RequestStageCols = [
  {
    key: 'stageName',
    name: 'Наименование',
  },
  {
    key: 'performerClerkName',
    name: 'Исполнитель',
  },
  {
    key: 'startDate',
    name: 'Дата начала этапа',
  },
  {
    key: 'planDate',
    name: 'Плановая дата',
  },
  {
    key: 'endDate',
    name: 'Фактическая дата',
  },
];

export enum RequestTabs {
  COMMON = 'common',
  DOCUMENTS = 'documents',
  PCTR = 'pctr',
  STAGES = 'stages',
  CONCLUSIONS = 'conclusions',
  PERMITS = 'permissions',
  SOLUTIONS = 'solutions',
  TASKS = 'tasks',
  CLIENT_STATIONS = 'clientStations',
  LINKED_TASK = 'linkedRequests',
  MPZ = 'mpz',
  LIST_OF_FREQUENCIES = 'frequenciesList',
  ZE_FOR_CANCELLATION = 'zeForCancellation',
  ANNULLED_PERMISSIONS = 'annulledPermissions',
}

export const RequestTabsLabels: Record<RequestTabs, string> = {
  [RequestTabs.COMMON]: 'Общие',
  [RequestTabs.DOCUMENTS]: 'Документы',
  [RequestTabs.PCTR]: 'ПЧТР',
  [RequestTabs.STAGES]: 'Этапы',
  [RequestTabs.CONCLUSIONS]: 'Заключения',
  [RequestTabs.PERMITS]: 'Разрешения',
  [RequestTabs.SOLUTIONS]: 'Решения ГКРЧ',
  [RequestTabs.TASKS]: 'Задачи',
  [RequestTabs.CLIENT_STATIONS]: 'Список абонентских станций',
  [RequestTabs.LINKED_TASK]: 'Связанные заявки',
  [RequestTabs.MPZ]: 'МПЗ',
  [RequestTabs.ANNULLED_PERMISSIONS]: 'Список РИЧ на аннулирование',
  [RequestTabs.LIST_OF_FREQUENCIES]: 'Список РИЧ',
  [RequestTabs.ZE_FOR_CANCELLATION]: 'Список ЗЭ на аннулирование',
};

export const RequestTasksCols = [
  {
    key: 'title',
    name: 'Задача',
    width: 300,
    hidden: false,
  },
  {
    key: 'author',
    name: 'Автор',
    width: 150,
    hidden: false,
  },
  {
    key: 'performer',
    name: 'Исполнитель',
    width: 200,
  },
  {
    key: 'comments',
    name: 'Комментарий',
    width: 200,
  },
  {
    key: 'performerComment',
    name: 'Комментарий по результату',
    width: 220,
  },
  {
    key: 'status',
    name: 'Статус',
    width: 100,
  },
  {
    key: 'termLabel',
    name: 'Дата исполнения/Срок исполнения (р/д)',
    width: 275,
  },
  {
    key: 'completeDate',
    name: 'Дата и время исполнения фактическая',
    width: 295,
  },
  {
    key: 'result',
    name: 'Результат',
    width: 190,
  },
  {
    key: 'createDate',
    name: 'Дата и время создания',
    width: 190,
  },
  {
    key: 'editor',
    name: 'Редактор',
    width: 190,
  },
  {
    key: 'id',
    name: 'Номер задачи',
    width: 150,
  },
  {
    key: 'externalTaskId',
    name: 'Номер задачи внешний',
    width: 180,
  },
  {
    key: 'objectLabel',
    name: 'Объект',
    width: 100,
  },
];

export const RequestConclusionsCols = [
  {
    key: 'number',
    name: 'Номер',
  },
  {
    key: 'date',
    name: 'Дата',
  },
  {
    key: 'dateValidity',
    name: 'Дата окончания действия',
  },
  {
    key: 'state',
    name: 'Состояние',
  },
  {
    key: 'dateAnnulment',
    name: 'Дата аннулирования',
  },
  {
    key: 'status',
    name: 'Статус',
  },
  {
    key: 'type',
    name: 'Тип',
  },
  {
    key: 'sort',
    name: 'Вид',
  },
  {
    key: 'createdDateTimeName',
    name: 'Ввел',
  },
];

export const IncomingRequestExecutionButtons = [
  ExecutionButtonActions.EXECUTE,
  ExecutionButtonActions.APPROVE,
  ExecutionButtonActions.ENDORSE,
  ExecutionButtonActions.REJECT,
  ExecutionButtonActions.CANCEL,
  ExecutionButtonActions.REASSIGN,
  ExecutionButtonActions.CONFIRM,
  ExecutionButtonActions.REEDIT,
  ExecutionButtonActions.REOPEN,
];

export const RequestConclusionDocumentsCols = [
  {
    key: 'title',
    name: 'Наименование',
  },
  {
    key: 'type',
    name: 'Тип',
  },
  {
    key: 'id',
    name: 'Штрих-код',
  },
  {
    key: 'number',
    name: 'Номер',
  },
  {
    key: 'rubricList',
    name: 'Рубрика',
  },
  {
    key: 'performer',
    name: 'Исполнитель',
  },
  {
    key: 'description',
    name: 'Содержание',
  },
  {
    key: 'date',
    name: 'Дата',
  },
  {
    key: 'dateSign',
    name: 'Дата подписания',
  },
  {
    key: 'recipientList',
    name: 'Корреспондент',
  },
  {
    key: 'contractorDate',
    name: 'Дата корреспондента',
  },
  {
    key: 'contractorNumber',
    name: 'Номер корреспондента',
  },
];
export const InitialDocumentsCols = [
  {
    key: 'type',
    name: 'Тип',
    //width: 150,
  },
  {
    key: 'id',
    name: 'Штрих-код',
    //width: 150,
  },
  {
    key: 'number',
    name: 'Номер',
  },
  {
    key: 'date',
    name: 'Дата',
    //width: 150,
  },
  {
    key: 'description',
    name: 'Содержание',
    //width: 150,
  },
  {
    key: 'rubricList',
    name: 'Рубрики',
  },
  {
    key: 'contractorList',
    name: 'Корреспонденты',
    //width: 150,
  },
  {
    key: 'contractorDate',
    name: 'Дата корреспондента',
    //width: 150,
  },
  {
    key: 'contractorNumber',
    name: 'Номер корреспондента',
    //width: 150,
  },
];

export const RequestDocumentsCols = [
  {
    key: 'title',
    name: 'Наименование',
    //width: 150,
  },
  {
    key: 'type',
    name: 'Тип',
    //width: 150,
  },
  {
    key: 'id',
    name: 'Штрих-код',
    //width: 150,
  },
  {
    key: 'number',
    name: 'Номер',
    //width: 150,
  },
  {
    key: 'date',
    name: 'Дата',
    //width: 150,
  },
  {
    key: 'description',
    name: 'Содержание',
    //width: 150,
  },
  {
    key: 'dateSign',
    name: 'Дата подписания',
  },

  {
    key: 'performer',
    name: 'Исполнитель',
    //width: 150,
  },
  {
    key: 'rubricList',
    name: 'Рубрики',
    //width: 150,
  },
  {
    key: 'recipientList',
    name: 'Корреспонденты',
    //width: 150,
  },
  {
    key: 'contractorDate',
    name: 'Дата корреспондента',
    //width: 150,
  },
  {
    key: 'contractorNumber',
    name: 'Номер корреспондента',
    //width: 150,
  },
];

export const RequestAnnulledPermissionsCols = [
  {
    key: 'contractorName',
    name: 'Заявитель',
  },
  {
    key: 'number',
    name: 'Номер',
  },
  {
    key: 'date',
    name: 'Дата',
  },
  {
    key: 'dateValidity',
    name: 'Срок действия',
  },
  {
    key: 'dateAnnulment',
    name: 'Дата аннулирования',
  },
  {
    key: 'type',
    name: 'Тип',
  },
  {
    key: 'state',
    name: 'Состояние',
  },
  {
    key: 'baseDocument',
    name: 'Тип основания',
  },
  {
    key: 'serviceMRFC',
    name: 'Служба ГРЧЦ',
  },
  {
    key: 'technology',
    name: 'Технология',
  },
  {
    key: 'regionList',
    name: 'Регион',
  },
  {
    key: 'federalDistrictList',
    name: 'Федеральный округ',
  },
  {
    key: 'stationCount',
    name: 'Количество РЭС',
  },
];

export const RequestConclusionPCTRCols = [
  {
    key: 'spsNumber',
    name: '№ п.п.',
  },
  {
    key: 'stationNumber',
    name: '№ БС',
  },
  {
    key: 'address',
    name: 'Адрес установки',
  },
  {
    key: 'additionAddress',
    name: 'Дополнительная информация о месте установки',
  },
  {
    key: 'stationPoint',
    name: 'Координаты',
  },
  {
    key: 'afsHeight',
    name: 'Высота подвеса ант. от уровня земли, м',
  },
  {
    key: 'angleAsimuth',
    name: 'Аз-т главного лепестка излуч-я ант., град.',
  },
  {
    key: 'antennaGain',
    name: 'Коэф-т усиле-ния ант., дБ',
  },
  {
    key: 'antennaLoss',
    name: 'Потери в фидере, дБ',
  },
  {
    key: 'powerTransm',
    name: 'Мощность несущей РПД, Вт',
  },
  {
    key: 'channels',
    name: 'Каналы',
  },
  {
    key: 'freqTransm',
    name: 'Частоты РПД/РПМ, МГц',
  },

  {
    key: 'stationState',
    name: 'Состояние РЭС',
  },
  {
    key: 'etsVidId',
    name: 'Вид РЭС',
  },
  {
    key: 'afsHeightSea',
    name: 'Высота подвеса ант. от уровня моря, м',
  },
  {
    key: 'angleElevation',
    name: 'Угол места ДНА, град.',
  },
  {
    key: 'apertureAngleH',
    name: 'Ширина ДНА в гор. пл-ти, град.',
  },
  {
    key: 'apertureAngleV',
    name: 'Ширина ДНА в верт. пл-ти, град.',
  },
  {
    key: 'classEmi',
    name: 'Класс излучения',
  },
  {
    key: 'polarization',
    name: 'Поляризация',
  },
  {
    key: 'cellRadius',
    name: 'Радиус зоны БС км',
  },
];

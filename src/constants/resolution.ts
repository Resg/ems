export enum ResolutionFields {
  AUTHOR_ID = 'authorId',
  CANCEL_DATE = 'cancelControlDate',
  CONTROL_DATE = 'controlDate',
  CONTROL_ITEM = 'controlItem',
  CONTROL_PERIOD = 'controlPeriod',
  CONTROLLER_ID = 'controllerId',
  DATE = 'date',
  DESCRIPTION = 'description',
  DOCUMENT_IDS = 'documentIds',
  EDITOR_ID = 'editorId',
  EXECUTOR_IDS = 'executorIds',
  IDS = 'ids',
  IS_CONTROLLED = 'isControlled',
  IS_ITEM_CONTROLLED = 'isItemControlled',
  PARENT_ID = 'parentId',
  STATUS = 'status',
}

export const ResolutionLabels: Record<ResolutionFields, string> = {
  [ResolutionFields.AUTHOR_ID]: 'Автор',
  [ResolutionFields.DATE]: 'Дата резолюции',
  [ResolutionFields.EXECUTOR_IDS]: 'Исполнители',
  [ResolutionFields.EDITOR_ID]: 'Редактор',
  [ResolutionFields.DESCRIPTION]: 'Содержание',
  [ResolutionFields.IS_CONTROLLED]: 'Контроль исполнения',
  [ResolutionFields.IS_ITEM_CONTROLLED]: 'По пункту',
  [ResolutionFields.CONTROLLER_ID]: 'Контроллер',
  [ResolutionFields.CONTROL_ITEM]: 'Пункт',
  [ResolutionFields.CONTROL_PERIOD]: 'Срок плановый',
  [ResolutionFields.CONTROL_DATE]: 'Плановая дата',
  [ResolutionFields.CANCEL_DATE]: 'Дата снятия контроля',
  [ResolutionFields.STATUS]: 'Статус',
  [ResolutionFields.DOCUMENT_IDS]: 'Документы',
  [ResolutionFields.IDS]: 'id',
  [ResolutionFields.PARENT_ID]: '',
};

export const DefaultResolution = {
  authorId: null,
  cancelControlDate: null,
  controlDate: null,
  controlItem: null,
  controlPeriod: null,
  controllerId: null,
  date: null,
  description: null,
  documentIds: null,
  editorId: null,
  executorIds: [],
  ids: null,
  isControlled: false,
  isItemControlled: false,
  parentId: null,
  status: null,
};

export const ResolutionDateFields = [
  'date',
  'controlDate',
  'cancelControlDate',
];

export const DocumentResolutionCols = [
  {
    key: 'authorName',
    name: 'Автор',
    hidden: false,
  },
  {
    key: 'performerName',
    name: 'Исполнитель',
    hidden: false,
  },
  {
    key: 'content',
    name: 'Содержание',
    hidden: false,
  },
  {
    key: 'date',
    name: 'Дата резолюции',
    hidden: false,
  },
  {
    key: 'executionTerm',
    name: 'Срок исполнения',
    hidden: false,
  },
  {
    key: 'statusName',
    name: 'Статус',
    hidden: false,
  },
  {
    key: 'executionDate',
    name: 'Дата исполнения',
    hidden: false,
  },
  {
    key: 'controlParagraphNumber',
    name: 'Пункт',
    hidden: true,
  },
  {
    key: 'comment',
    name: 'Комментарий',
    hidden: true,
  },
  {
    key: 'createDate',
    name: 'Дата создания',
    hidden: true,
  },
  {
    key: 'controlCancelDate',
    name: 'Дата снятия с контроля',
    hidden: true,
  },
  {
    key: 'hasSubresolution',
    name: 'Есть подчиненные',
    hidden: true,
  },
  {
    key: 'parentResolutionTitle',
    name: 'Номер родительской резолюции',
    hidden: true,
  },
  {
    key: 'id',
    name: 'Номер резолюции',
    hidden: true,
  },
  {
    key: 'subResolutionIdsString',
    name: 'Подчиненные резолюции',
    hidden: true,
  },
];

export enum TasksStagesFields {
  STAGES = 'stages',
  NAME = 'name',
  TERM = 'term',
  PLANE_DATE = 'planDate',
  CONTROL = 'isControl',
  CLERKS = 'clerks',
  COMMENTS = 'comments',
}

export const TasksStagesLabels = {
  [TasksStagesFields.NAME]: 'Тип задачи',
  [TasksStagesFields.TERM]: 'Срок исполнения (р/д)',
  [TasksStagesFields.PLANE_DATE]: 'Срок исполнения (дата)',
  [TasksStagesFields.CONTROL]: 'Контроль исполнения',
  [TasksStagesFields.CLERKS]: 'Исполнители',
  [TasksStagesFields.COMMENTS]: 'Комментарий',
};

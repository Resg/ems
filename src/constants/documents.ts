export enum DocumentTypes {
  DOCS_PREDPR = 'DOCS_PREDPR',
  DOCS_INCOMING = 'DOCS_INCOMING',
  DOCS_PODR = 'DOCS_PODR',
  DOCS_OUTCOMING = 'DOCS_OUTCOMING',
  DOCS_OUTCOMING_DRKK = 'DOCS_OUTCOMING_DRKK',
}

export const documentTypesLabels = {
  [DocumentTypes.DOCS_PREDPR]: 'Внутренний предприятия',
  [DocumentTypes.DOCS_INCOMING]: 'Входящий',
  [DocumentTypes.DOCS_PODR]: 'Внутренний подразделения',
  [DocumentTypes.DOCS_OUTCOMING]: 'Исходящий',
  [DocumentTypes.DOCS_OUTCOMING_DRKK]: 'ДЭРК',
};

export enum DocumentTabs {
  COMMON = 'common',
  FOLLOWED_DOCUMENTS = 'followed-documents',
  RESOLUTIONS = 'resolutions',
  BONDS = 'bonds',
  REQUESTS = 'requests',
  HISTORY = 'history',
  PROTOCOL = 'protocol',
  DELIVERY = 'delivery',
  TASKS = 'tasks',
}

export const DocumentTabsLabels: Record<DocumentTabs, string> = {
  [DocumentTabs.COMMON]: 'Общие',
  [DocumentTabs.FOLLOWED_DOCUMENTS]: 'Сопровождаемые документы',
  [DocumentTabs.RESOLUTIONS]: 'Резолюции',
  [DocumentTabs.BONDS]: 'Связки',
  [DocumentTabs.REQUESTS]: 'Заявки',
  [DocumentTabs.HISTORY]: 'История',
  [DocumentTabs.PROTOCOL]: 'Протокол',
  [DocumentTabs.DELIVERY]: 'Доставка',
  [DocumentTabs.TASKS]: 'Задачи',
};

export const documentReceiversCols = [
  {
    key: 'contractorName',
    name: 'Наименование',
  },
  {
    key: 'contractorContactPerson',
    name: 'Кому',
  },
  {
    key: 'contractorAddressDeliveryType',
    name: 'Способ отправки',
  },
  {
    key: 'contractorContactInfo',
    name: 'Контактная информация',
  },
  {
    key: 'contractorAddressPostIndex',
    name: 'Индекс',
  },
  {
    key: 'contractorAddress',
    name: 'Почтовый адрес',
  },
  {
    key: 'packetOutgoingSendDate',
    name: 'Дата отправки',
  },
  {
    key: 'packetPostNotificationNumber',
    name: 'Номер уведомления',
  },
  {
    key: 'packetOutgoingDeliveryDate',
    name: 'Дата доставки',
  },
  {
    key: 'packetPostReturnDate',
    name: 'Дата возврата',
  },
  {
    key: 'comments',
    name: 'Примечание',
  },
  {
    key: 'packetOutgoingStatus',
    name: 'Статус',
  },
  {
    key: 'packetOutgoingPagesCount',
    name: 'Состав ИП',
  },
];

export const HistoryCols = [
  {
    key: 'name',
    name: 'Событие',
    hidden: false,
  },
  {
    key: 'numberName',
    name: 'Экз. номер',
    hidden: false,
  },
  {
    key: 'registryNum',
    name: 'Номер реестра',
    hidden: false,
  },
  {
    key: 'registryName',
    name: 'Тип',
    hidden: false,
  },
  {
    key: 'date',
    name: 'Дата',
    hidden: false,
  },
  {
    key: 'time',
    name: 'Время',
    hidden: false,
  },
  {
    key: 'clerkName',
    name: 'Автор',
    hidden: false,
  },
  {
    key: 'registryTargetClerkName',
    name: 'Адресат: Сотрудник ',
    hidden: false,
  },
  {
    key: 'registryTargetDivision',
    name: 'Адресат: Подразделение',
    hidden: false,
  },
  {
    key: 'requestManagerName',
    name: 'Исполнитель',
    hidden: false,
  },
  {
    key: 'documentHolder',
    name: 'Местоположение',
    hidden: false,
  },

  {
    key: 'outgoingPackageNumber',
    name: 'Номер ИП',
    hidden: true,
  },
  {
    key: 'postPackageLocalNumber',
    name: 'Номер ПП',
    hidden: true,
  },
];

export const protocolCols = [
  {
    key: 'type',
    name: 'Действие',
    width: 315,
    resizable: false,
  },
  {
    key: 'date',
    name: 'Дата',
    width: 165,
    resizable: false,
  },
  {
    key: 'shortReport',
    name: 'Краткое описание',
    width: 370,
    resizable: false,
  },
  {
    key: 'information',
    name: 'Подробная информация',
    width: 505,
    resizable: false,
  },
  {
    key: 'clerkShortName',
    name: 'Произвел',
    hidden: false,
  },
];

export const DeliveryCols = [
  {
    key: 'localNumber',
    name: 'Номер ИП',
    hidden: false,
  },
  {
    key: 'clientShortName',
    name: 'Адресат - краткое наименование',
    width: 260,
    hidden: false,
  },
  {
    key: 'clientFullName',
    name: 'Адресат - полное наименование',
    width: 260,
    hidden: false,
  },
  {
    key: 'ownType',
    name: 'ОПФ',
    hidden: false,
  },
  {
    key: 'index',
    name: 'Индекс',
    hidden: false,
  },
  {
    key: 'address',
    name: 'Адрес',
    hidden: false,
  },
  {
    key: 'email',
    name: 'e-mail',
    hidden: false,
  },
  {
    key: 'fax',
    name: 'Факс',
    hidden: false,
  },
  {
    key: 'recipientPersonName',
    name: 'Кому',
    hidden: false,
  },
  {
    key: 'deliveryTypeName',
    name: 'Способ отправки',
    width: 150,
    hidden: false,
  },
  {
    key: 'status',
    name: 'Статус',
    hidden: false,
  },

  {
    key: 'sendingDate',
    name: 'Дата отправки',
    width: 130,
    hidden: false,
  },
  {
    key: 'deliveryDate',
    name: 'Дата доставки',
    width: 130,
    hidden: false,
  },
  {
    key: 'comment',
    name: 'Примечание',
    hidden: false,
  },
  {
    key: 'returnPostPacketeDate',
    name: 'Дата возврата',
    width: 130,
    hidden: false,
  },
  {
    key: 'trustedPersonName',
    name: 'Доверенное лицо',
    width: 160,
    hidden: false,
  },
  {
    key: 'notificationNumber',
    name: 'Номер уведомления',
    width: 170,
    hidden: false,
  },
  {
    key: 'documentDate',
    name: 'Дата регистрации',
    width: 160,
    hidden: false,
  },

  {
    key: 'signDate',
    name: 'Дата подписи',
    width: 130,
    hidden: false,
  },
  {
    key: 'countSendingOutgoingPacket',
    name: 'Количество отправок исходящего пакета',
    width: 320,
    hidden: false,
  },
];

export enum MPZFields {
  STATE_CODE = 'stateCode',
  MPZ = 'MPZ',
  COMPLETION_DATE = 'completionDate',
  DATE = 'date',
  SEND_DATE = 'sendDate',
  PUBLISH_SENT = 'publishSent',
  REQUEST_SENT = 'requestSent',
  ADMINISTRATION_ID = 'administrationId',
  RECEIVE_DATE = 'receiveDate',
  PUBLISH_DATE = 'publishDate',
  REQUEST_RECEIVED = 'requestReceived',
  REGISTRATION_STATE_CODE = 'registrationStateCode',
  COMMENTS = 'comments',
}

export const MPZLabels: Record<MPZFields, string> = {
  [MPZFields.STATE_CODE]: 'Координация',
  [MPZFields.MPZ]: 'МПЗ',
  [MPZFields.COMPLETION_DATE]: 'Дата завершения координации',
  [MPZFields.DATE]: 'Дата завершения',
  [MPZFields.SEND_DATE]: 'Отправлено в МСЭ',
  [MPZFields.PUBLISH_SENT]: 'Направлено на публикацию',
  [MPZFields.REQUEST_SENT]: 'Отправлен запрос',
  [MPZFields.ADMINISTRATION_ID]: 'Adm.ID',
  [MPZFields.RECEIVE_DATE]: 'Зарегистрировано в МСРЧ',
  [MPZFields.PUBLISH_DATE]: 'Дата публикации',
  [MPZFields.REQUEST_RECEIVED]: 'Получен запрос',
  [MPZFields.REGISTRATION_STATE_CODE]: 'Состояние регистрации в МСЭ',
  [MPZFields.COMMENTS]: 'Примечание',
};

export const DefaultMPZ = {
  stateCode: null,
  completionDate: null,
  sendDate: null,
  administrationId: '',
  receiveDate: null,
  registrationStateCode: null,
  comments: '',
};

export const CountriesTableCols = [
  {
    key: 'orderNumber',
    name: '№ п/п',
    hidden: false,
    width: 100,
  },
  {
    key: 'countryName',
    name: 'Страна-координатор',
    hidden: false,
    width: 250,
  },
  {
    key: 'sendDate',
    name: 'Дата отправки запроса',
    hidden: false,
    width: 250,
  },
  {
    key: 'receiveDate',
    name: 'Дата получения ответа',
    hidden: false,
    width: 250,
  },
  {
    key: 'status',
    name: 'Статус',
    hidden: false,
    width: 600,
  },
];

export enum CountrySelectionFields {
  COUNTRY_ID = 'countryId',
  SEND_DATE = 'sendDate',
  RECEIVE_DATE = 'receiveDate',
  STATUS_CODE = 'statusCode',
  COMMENTS = 'comments',
  UPDATED_TIME_NAME = 'updatedDateTimeName',
}

export const CountrySelectionLabels: Record<CountrySelectionFields, string> = {
  [CountrySelectionFields.COUNTRY_ID]: 'Страна',
  [CountrySelectionFields.SEND_DATE]: 'Отправлен запрос',
  [CountrySelectionFields.RECEIVE_DATE]: 'Получен ответ',
  [CountrySelectionFields.STATUS_CODE]: 'Статус',
  [CountrySelectionFields.COMMENTS]: 'Примечание',
  [CountrySelectionFields.UPDATED_TIME_NAME]: 'Последние изменения',
};

export const DefaultCountrySelection = {
  countryId: null,
  sendDate: null,
  receiveDate: null,
  statusCode: null,
  comments: '',
  updatedDateTimeName: '',
};

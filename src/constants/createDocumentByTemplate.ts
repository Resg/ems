export enum CreateDocumentSteps {
  PARAMS,
  TEMPLATE,
  RECEIVER,
  DOCUMENTS,
}

export const CreateDocumentStepLabels = {
  [CreateDocumentSteps.PARAMS]: 'Параметры заключения',
  [CreateDocumentSteps.TEMPLATE]: '',
  [CreateDocumentSteps.RECEIVER]: 'Адресаты',
  [CreateDocumentSteps.DOCUMENTS]: 'Сопровождаемые документы',
};

export const Steps = [
  CreateDocumentSteps.PARAMS,
  CreateDocumentSteps.TEMPLATE,
  CreateDocumentSteps.RECEIVER,
  CreateDocumentSteps.DOCUMENTS,
];

export enum CreateDocumentFormFields {
  STATUS = 'conclusionStatusCode',
  TYPE = 'conclusionTypeCode',
  VIEW = 'conclusionSortId',
  SIGN = 'conclusionSignerId',

  RUBRIC = 'rubricIds',
  TEMPLATE = 'templateId',
  RECEIVER = 'innerAddressees.clerkId',
  SIGNER = 'signerIds',
  PARAMETERS_CODE = 'parametersCode',
  REGISTRATOR = 'registrator.clerkId',

  TITLE = 'title',
  TYPE_ID = 'typeId',
  CONTRACTORS = 'outerAddressees',
  DOCUMENTS = 'followedDocuments',
}

export const CreateDocumentFormLabels = {
  [CreateDocumentFormFields.STATUS]: 'Статус',
  [CreateDocumentFormFields.TYPE]: 'Тип',
  [CreateDocumentFormFields.VIEW]: 'Вид',
  [CreateDocumentFormFields.SIGN]: 'Подпись',

  [CreateDocumentFormFields.RUBRIC]: 'Рубрика',
  [CreateDocumentFormFields.TEMPLATE]: 'Шаблон документа',
  [CreateDocumentFormFields.RECEIVER]: 'Адресат',
  [CreateDocumentFormFields.SIGNER]: 'Подписант',
};
// Штрих-код	id
// Номер	number
// Наименование	title
// Количество экземпляров
// numberOfCopies
//
// Ввод числового значения, значение по умолчанию 1
//
// Содержание	description
// Тип	type
//   Рубрики	rubricList
// Дата	date
// Комментарии	comments
export const DocumentsCols = [
  {
    key: 'id',
    name: 'Штрих-код',
  },
  {
    key: 'number',
    name: 'Номер',
  },
  {
    key: 'title',
    name: 'Наименование',
  },
  {
    key: 'numberOfCopies',
    name: 'Количество экземпляров',
  },
  {
    key: 'description',
    name: 'Содержание',
  },
  {
    key: 'type',
    name: 'Тип',
  },
  {
    key: 'rubricList',
    name: 'Рубрики',
  },
  {
    key: 'date',
    name: 'Дата',
  },
  {
    key: 'comments',
    name: 'Комментарии',
  },
];

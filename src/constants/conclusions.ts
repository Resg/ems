export enum ConclusionTabs {
  COMMON = 'common',
  CONDITIONS = 'conditions',
  DOCUMENTS = 'documents',
  PCTR = 'pctr',
  ATTACHMENTS = 'attachments',
}

export const ConclusionTabsLabels: Record<ConclusionTabs, string> = {
  [ConclusionTabs.COMMON]: 'Общие',
  [ConclusionTabs.CONDITIONS]: 'Дополнительные условия',
  [ConclusionTabs.DOCUMENTS]: 'Документы',
  [ConclusionTabs.PCTR]: 'ПЧТР',
  [ConclusionTabs.ATTACHMENTS]: 'Приложения',
};

export enum ConclusionFormFields {
  NUMBER = 'number',
  CLIENT = 'contractorIds',
  DATE = 'date',
  END_DATE = 'dateValidity',
  PERIOD = 'validityPeriodCode',
  STATUS = 'statusCode',
  TYPE = 'typeCode',
  SORT = 'sortId',
  SIGN = 'signerId',
  STATE = 'stateCode',
  DATE_ANNULMENT = 'dateAnnulment',
  REASON = 'annulmentReasonCode',
  COMMENT = 'comment',
}

export const ConclusionFormLabels: Record<ConclusionFormFields, string> = {
  [ConclusionFormFields.NUMBER]: 'Номер',
  [ConclusionFormFields.CLIENT]: 'Заявитель',
  [ConclusionFormFields.DATE]: 'Дата',
  [ConclusionFormFields.END_DATE]: 'Дата окончания действия',
  [ConclusionFormFields.PERIOD]: 'Срок действия',
  [ConclusionFormFields.STATUS]: 'Статус',
  [ConclusionFormFields.TYPE]: 'Тип',
  [ConclusionFormFields.SORT]: 'Вид',
  [ConclusionFormFields.SIGN]: 'Подпись',
  [ConclusionFormFields.STATE]: 'Состояние',
  [ConclusionFormFields.DATE_ANNULMENT]: 'Дата аннулирования',
  [ConclusionFormFields.REASON]: 'Причина аннулирования',
  [ConclusionFormFields.COMMENT]: 'Примечание',
};

export const ConclusionFormDefaults = {
  [ConclusionFormFields.NUMBER]: '',
  [ConclusionFormFields.CLIENT]: [],
  [ConclusionFormFields.DATE]: '',
  [ConclusionFormFields.END_DATE]: '',
  [ConclusionFormFields.PERIOD]: '',
  [ConclusionFormFields.STATUS]: '',
  [ConclusionFormFields.TYPE]: '',
  [ConclusionFormFields.SORT]: null,
  [ConclusionFormFields.SIGN]: '',
  [ConclusionFormFields.STATE]: '',
  [ConclusionFormFields.DATE_ANNULMENT]: '',
  [ConclusionFormFields.REASON]: '',
  [ConclusionFormFields.COMMENT]: '',
};

export enum ConclusionAppendicesFields {
  NAME = 'name',
  TEXT = 'text',
  FILE = 'fileContentBinary',
}

export const DefaultConclusionAppendicesFields = {
  name: '',
  text: '',
  fileContentBinary: null,
};

export const ConclusionAppendicesLabels = {
  [ConclusionAppendicesFields.NAME]: 'Наименование',
  [ConclusionAppendicesFields.TEXT]: 'Текст',
  [ConclusionAppendicesFields.FILE]: 'Файл',
};

export const additionalsCols = [
  {
    key: 'ordinalNumber',
    name: 'Номер п/п',
    width: 100,
  },
  {
    key: 'name',
    name: 'Наименование',
  },
  {
    key: 'conclusionText',
    name: 'Текст для заключения',
    width: 600,
  },
  {
    key: 'permissionText',
    name: 'Текст для разрешения',
    width: 700,
  },
];

export const appendicesCols = [
  {
    key: 'name',
    name: 'Наименование',
  },
  {
    key: 'text',
    name: 'Текст',
  },
  {
    key: 'createDate',
    name: 'Дата создания',
  },
];

export const ConclusionCols = [
  {
    key: 'number',
    name: 'Номер',
    hidden: false,
  },
  {
    key: 'date',
    name: 'Дата',
    hidden: false,
  },
  {
    key: 'dateValidity',
    name: 'Дата окончания действия',
    hidden: false,
  },
  {
    key: 'dateAnnulment',
    name: 'Дата аннулирования',
    hidden: false,
  },
  {
    key: 'state',
    name: 'Состояние',
    hidden: false,
  },
  {
    key: 'status',
    name: 'Статус',
    hidden: false,
  },
  {
    key: 'type',
    name: 'Тип',
    hidden: false,
  },
  {
    key: 'author',
    name: 'Автор',
    hidden: false,
  },
];

export enum AdditionalConditionsFields {
  NUMBER = 'ordinalNumber',
  NAME = 'name',
  CONCLUSION = 'conclusionText',
  PERMISSION = 'permissionText',
}

export const AdditionalConditionsLabels: Record<
  AdditionalConditionsFields,
  string
> = {
  [AdditionalConditionsFields.NUMBER]: 'Номер п/п',
  [AdditionalConditionsFields.NAME]: 'Наименование',
  [AdditionalConditionsFields.CONCLUSION]: 'Текст для заключения',
  [AdditionalConditionsFields.PERMISSION]: 'Текст для разрешения',
};

export const AdditionalConditionDocumentDefaults = {
  ordinalNumber: '',
  name: '',
  conclusionText: '',
  permissionText: '',
};
export const ConclusionModalCols = [
  {
    key: 'number',
    name: 'Номер',
    hidden: false,
  },
  {
    key: 'date',
    name: 'Дата',
    hidden: false,
  },
  {
    key: 'dateValidity',
    name: 'Дата окончания действия',
    hidden: false,
  },
  {
    key: 'state',
    name: 'Состояние',
    hidden: false,
  },
  {
    key: 'dateAnnulment',
    name: 'Дата аннулирования',
    hidden: false,
  },
  {
    key: 'status',
    name: 'Статус',
    hidden: false,
  },
  {
    key: 'type',
    name: 'Тип',
    hidden: false,
  },
  {
    key: 'sort',
    name: 'Вид',
    hidden: false,
  },
  {
    key: 'author',
    name: 'Автор',
    hidden: false,
  },
];

export enum AdditionalConditionsDictionariesFields {
  NAME = 'name',
  CONCLUSION = 'conclusionText',
  PERMISSION = 'permissionText',
  ISACTUAL = 'isActual',
}

export const AdditionalConditionsDictionariesLabels: Record<
  AdditionalConditionsDictionariesFields,
  string
> = {
  [AdditionalConditionsDictionariesFields.NAME]: 'Наименование',
  [AdditionalConditionsDictionariesFields.CONCLUSION]: 'Текст для заключения',
  [AdditionalConditionsDictionariesFields.PERMISSION]: 'Текст для разрешения',
  [AdditionalConditionsDictionariesFields.ISACTUAL]: 'Актуальное',
};

export const AdditionalConditionDictionariesDefaults = {
  name: '',
  conclusionText: '',
  permissionText: '',
  isActual: false,
};

export enum SearchTabs {
  LEGAL_ENTITY = 'Юридическое лицо',
  PHYSICAL_ENTITY = 'Физическое лицо',
}

export enum SearchFields {
  LEGAL_ENTITY = 'filters.legalEntity',
  ORGANIZATION_LEGAL_FORM_ID = 'filters.organizationalLegalFormId',
  IS_BUDGET_ENTITY = 'filters.isBudgetEntity',
  NAME = 'filters.name',
  FULL_NAME = 'filters.fullName',
  INN = 'filters.innLegalEntity',
  OGRN = 'filters.ogrn',
  COUNTRY_ID = 'filters.countryId',
  PARENT_NAME = 'filters.parentName',
  CONTACT_PERSON_NAME = 'filters.contactPersonName',
  COMMENTS = 'filters.commentsLegalEntity',
  CREATE_DATE_FROM = 'filters.createDateFrom',
  CREATE_DATE_TO = 'filters.createDateTo',
  IS_ACTUAL = 'filters.isActual',
  IS_WORK_VIA_WEB_ALLOWED = 'filters.isWorkViaWebAllowed',
  IS_ON_HAND_ISSUANCE = 'filters.isOnHandIssuance',
  IS_REORGANIZED = 'filters.isReorganized',
  IS_OUTGOING_DOCUMENT_SENDING_FORBIDDEN = 'filters.isOutgoingDocumentsSendingForbidden',
}

export enum SearchFieldsPhysicalEntity {
  PHYSICAL_ENTITY = 'filters.physicalEntity',
  LAST_NAME = 'filters.lastName',
  FIRST_NAME = 'filters.firstName',
  MIDDLE_NAME = 'filters.middleName',
  PASSPORT_SERIES = 'filters.passportSeries',
  PASSPORT_NUMBER = 'filters.passportNumber',
  INN = 'filters.innPhysicalEntity',
  COMMENTS = 'filters.commentsPhysicalEntity',
  CREATE_DATE_FROM = 'filters.createDateFromPhysicalEntity',
  CREATE_DATE_TO = 'filters.createDateToPhysicalEntity',
}

export const SearchLabels: Record<SearchFields, string> = {
  [SearchFields.LEGAL_ENTITY]: 'Юридическое лицо',
  [SearchFields.ORGANIZATION_LEGAL_FORM_ID]: 'Тип',
  [SearchFields.IS_BUDGET_ENTITY]: 'Бюджетная организация',
  [SearchFields.NAME]: 'Краткое наименование',
  [SearchFields.FULL_NAME]: 'Полное наименование',
  [SearchFields.INN]: 'ИНН',
  [SearchFields.OGRN]: 'ОГРН',
  [SearchFields.COUNTRY_ID]: 'Страна',
  [SearchFields.PARENT_NAME]: 'Родительская организация',
  [SearchFields.CONTACT_PERSON_NAME]: 'Контактное лицо',
  [SearchFields.COMMENTS]: 'Примечание',
  [SearchFields.CREATE_DATE_FROM]: 'Дата ввода',
  [SearchFields.CREATE_DATE_TO]: 'До',
  [SearchFields.IS_ACTUAL]: 'Только актуальные',
  [SearchFields.IS_WORK_VIA_WEB_ALLOWED]: 'Разрешена работа через web ',
  [SearchFields.IS_ON_HAND_ISSUANCE]: 'Выдача документов на руки',
  [SearchFields.IS_REORGANIZED]: 'C учетом реорганизации',
  [SearchFields.IS_OUTGOING_DOCUMENT_SENDING_FORBIDDEN]: '',
};

export const SearchLabelsPhysicalEntity: Record<
  SearchFieldsPhysicalEntity,
  string
> = {
  [SearchFieldsPhysicalEntity.PHYSICAL_ENTITY]: 'Физическое лицо',
  [SearchFieldsPhysicalEntity.LAST_NAME]: 'Фамилия',
  [SearchFieldsPhysicalEntity.FIRST_NAME]: 'Имя',
  [SearchFieldsPhysicalEntity.MIDDLE_NAME]: 'Отчество',
  [SearchFieldsPhysicalEntity.PASSPORT_SERIES]: 'Серия паспорта',
  [SearchFieldsPhysicalEntity.PASSPORT_NUMBER]: 'Номер паспорта',
  [SearchFieldsPhysicalEntity.INN]: 'ИНН',
  [SearchFieldsPhysicalEntity.COMMENTS]: 'Примечание',
  [SearchFieldsPhysicalEntity.CREATE_DATE_FROM]: 'Дата ввода',
  [SearchFieldsPhysicalEntity.CREATE_DATE_TO]: 'До',
};

export const SearchInitialValues = {
  [SearchFields.ORGANIZATION_LEGAL_FORM_ID]: null,
  [SearchFields.IS_BUDGET_ENTITY]: false,
  [SearchFields.NAME]: '',
  [SearchFields.FULL_NAME]: '',
  [SearchFields.INN]: '',
  [SearchFields.OGRN]: '',
  [SearchFields.COUNTRY_ID]: null,
  [SearchFields.PARENT_NAME]: null,
  [SearchFields.CONTACT_PERSON_NAME]: '',
  [SearchFields.COMMENTS]: '',
  [SearchFields.CREATE_DATE_FROM]: '',
  [SearchFields.CREATE_DATE_TO]: '',
  [SearchFields.IS_ACTUAL]: false,
  [SearchFields.IS_WORK_VIA_WEB_ALLOWED]: false,
  [SearchFields.IS_ON_HAND_ISSUANCE]: false,
  [SearchFields.IS_REORGANIZED]: false,
  [SearchFields.IS_OUTGOING_DOCUMENT_SENDING_FORBIDDEN]: false,
};

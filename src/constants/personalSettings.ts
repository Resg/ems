export enum PersonalSettingsFormFields {
  IS_OPEN_FORM_IN_NEW_TAB = 'openFormInNewTab',
  IS_AUTO_COUNTERS_UPDATE = 'autoCountersUpdate',
}

export const PersonalSettingsFormLabels: Record<
  PersonalSettingsFormFields,
  string
> = {
  [PersonalSettingsFormFields.IS_OPEN_FORM_IN_NEW_TAB]:
    'Открывать форму в новой вкладке браузера',
  [PersonalSettingsFormFields.IS_AUTO_COUNTERS_UPDATE]:
    'Автоматическое обновление счетчиков в главном меню',
};

export const PersonalSettingsDefaults = {
  openFormInNewTab: '',
  autoCountersUpdate: '',
};

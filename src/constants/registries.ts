export const RegistryDocumentsCols = [
  {
    key: 'orderNumberInRegistry',
    name: '№ п/п',
  },
  {
    key: 'type',
    name: 'Тип документа',
  },
  {
    key: 'id',
    name: 'Штрих-код',
  },
  {
    key: 'number',
    name: 'Номер',
  },
  {
    key: 'date',
    name: 'Дата',
  },
  {
    key: 'compound',
    name: 'Состав',
  },
  {
    key: 'rubricList',
    name: 'Список рубрик',
  },
  {
    key: 'contractorList',
    name: 'Кор. / Адр.',
  },
  {
    key: 'orderNumberName',
    name: 'Экземпляр',
  },
  {
    key: 'status',
    name: 'Статус',
  },
];

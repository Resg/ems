import { array, number, object, string } from 'yup';

import { PermissionFormFields } from '../../../../constants/permissions';

export const validationSchema = object({
  [PermissionFormFields.NUMBER]: string().required('Обязательное поле'),
  [PermissionFormFields.DATE]: string()
    .nullable()
    .required('Обязательное поле'),
  [PermissionFormFields.END_DATE]: string().nullable(),
  [PermissionFormFields.TYPE]: string(),
  [PermissionFormFields.STATE]: string(),
  [PermissionFormFields.BASE_DOCUMENT]: string(),
  [PermissionFormFields.BASE_DOCUMENT_NUMBER]: string(),
  [PermissionFormFields.DATE_ANNULMENT]: string().nullable(),
  [PermissionFormFields.CLIENT]: array()
    .of(number())
    .nullable()
    .min(1, 'Обязательное поле'),
  [PermissionFormFields.REASON]: string().nullable(),
  [PermissionFormFields.COMMENT]: string().nullable(),
  [PermissionFormFields.OBJECTID]: number().nullable(),
});

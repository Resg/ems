import React, { useCallback, useMemo, useState } from 'react';
import { format } from 'date-fns';
import { Form, Formik } from 'formik';
import { useSnackbar } from 'notistack';
import { useSelector } from 'react-redux';
import {
  matchPath,
  useLocation,
  useNavigate,
  useSearchParams,
} from 'react-router-dom';

import {
  AddTask,
  Cancel,
  DeleteOutline,
  Edit,
  EditOff,
  OpenInBrowser,
  PersonPin,
  Save,
} from '@mui/icons-material';
import { Box, Button, Card, Dialog, Grid, Stack } from '@mui/material';

import { AutoCompleteInput } from '../../../components/AutoCompleteInput';
import { ContractorSearchForm } from '../../../components/ContractorSearchForm';
import { DatePickerInput } from '../../../components/DatePickerInput';
import { DebounceAutoComplete } from '../../../components/DebounceAutoComplete';
import Input from '../../../components/Input';
import { RoundButton } from '../../../components/RoundButton';
import { ToolButton, ToolsPanel } from '../../../components/ToolsPanel';
import {
  PermissionFormDefaults,
  PermissionFormFields,
  PermissionFormLabels,
} from '../../../constants/permissions';
import { Routes } from '../../../constants/routes';
import { useGetConclusionContractorsQuery } from '../../../services/api/conclusions';
import {
  useGetAsyncContractorsListMutation,
  useGetConclusionAnnulmentsQuery,
  useGetConclusionStatesQuery,
  useGetCounterpartiesAsyncMutation,
  useGetPermissionTypesQuery,
} from '../../../services/api/dictionaries';
import {
  useCreatePermissionMutation,
  useGetPermissionContractorsByIdQuery,
  useGetPermissionContractorsQuery,
  useGetPermissionDataQuery,
  useGetPermissionTermQuery,
  useSelectNewPermissionMutation,
  useUpdatePermissionMutation,
} from '../../../services/api/permissions';
import { useGetRequestContractorsQuery } from '../../../services/api/request';
import { RootState } from '../../../store';
import { Counterparty } from '../../../types/dictionaries';
import { ConclusionClient } from '../../../types/requests';
import { filesToTableAdapter } from '../../../utils/documents';
import { FilesTable } from '../../documents/FilesTable';

import { validationSchema } from './validation';

import styles from './styles.module.scss';

export const PermissionsCommon = () => {
  const [searchParams] = useSearchParams();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const navigate = useNavigate();
  const location = useLocation();
  const id = Number(
    matchPath(Routes.PERMISSIONS.PERMISSION_TABS, location.pathname)?.params
      ?.id || ''
  );
  const [editMode, setEditMode] = useState(!id);
  const [openModal, setOpenModal] = useState(false);
  const [openSaveModal, setOpenSaveModal] = useState(false);
  const [openExtractModal, setOpenExtractModal] = useState(false);
  const [isAnnulmentBasePermission, setIsAnnulmentBasePermission] =
    useState(false);
  const { data, refetch } = useGetPermissionDataQuery({ id }, { skip: !id });
  const baseObjectId =
    data?.baseObjectId ||
    (searchParams.get('baseObjectId')
      ? String(searchParams.get('baseObjectId'))
      : null);
  const baseDocumentCode =
    data?.baseDocumentCodes ||
    (searchParams.get('baseDocumentCode')
      ? String(searchParams.get('baseDocumentCode'))
      : null);
  const baseDocumentNumber =
    data?.baseDocumentNumber ||
    (searchParams.get('baseDocumentNumber')
      ? String(searchParams.get('baseDocumentNumber'))
      : null);
  const { data: basePermission } = useGetPermissionDataQuery(
    { id: baseObjectId },
    { skip: baseDocumentCode !== 'LIC_REASON_OTHER_LIC' || !!id }
  );
  const { data: reasons = [] } = useGetConclusionAnnulmentsQuery({});
  const [update] = useUpdatePermissionMutation();
  const [create] = useCreatePermissionMutation();
  const [getCounterparties] = useGetCounterpartiesAsyncMutation();
  const [getContractorsList] = useGetAsyncContractorsListMutation();
  const [selectNewPermission] = useSelectNewPermissionMutation();

  const { data: permissionType } = useGetPermissionTypesQuery();

  const { data: contractors } = useGetPermissionContractorsQuery(
    { id: id },
    { skip: !id }
  );

  const { data: permissionContractor } = useGetPermissionContractorsByIdQuery(
    { permissionId: baseObjectId },
    { skip: baseDocumentCode !== 'LIC_REASON_OTHER_LIC' || !!id }
  );
  const { data: conclusionContractor } = useGetConclusionContractorsQuery(
    { conclusionId: baseObjectId },
    { skip: baseDocumentCode !== 'LIC_REASON_CONCL' || !!id }
  );
  const { data: requestContractor } = useGetRequestContractorsQuery(
    { requestId: baseObjectId },
    { skip: baseDocumentCode !== 'LIC_REASON_REQ' || !!id }
  );
  const { data: states = [] } = useGetConclusionStatesQuery({});

  const { data: permissionTerm } = useGetPermissionTermQuery(
    { permissionId: baseObjectId || '' },
    { skip: baseDocumentCode !== 'LIC_REASON_OTHER_LIC' || !!id }
  );

  const defaultContractor = useMemo(() => {
    if (permissionContractor && permissionContractor?.length)
      return permissionContractor.map((el) => el.id);
    if (conclusionContractor && conclusionContractor?.length)
      return conclusionContractor.map((el) => el.id);
    if (requestContractor?.data && requestContractor?.data?.length)
      return requestContractor?.data.map((el: ConclusionClient) => el.id);
  }, [permissionContractor, conclusionContractor, requestContractor]);

  const baseDocumentDate = useMemo(() => {
    if (basePermission?.date) {
      return format(new Date(basePermission?.date), 'dd.MM.yyyy');
    }
  }, [basePermission]);

  const permissionTypes = useMemo(() => {
    return (
      permissionType?.data.map((el) => {
        return {
          value: el.code,
          label: el.permissionType,
        };
      }) || []
    );
  }, [permissionType?.data]);

  const permissionBaseTypes = useMemo(() => {
    return (
      permissionType?.data.map((el) => {
        return {
          value: el.code,
          label: el.baseDocumentType,
        };
      }) || []
    );
  }, [permissionType?.data]);

  const type = useMemo(
    () =>
      permissionBaseTypes.filter(
        (el) => el.value === data?.baseDocumentCodes
      )[0]?.value ||
      permissionTypes.filter((el) => el.value === baseDocumentCode)[0]?.value ||
      [],
    [data, permissionTypes, baseDocumentCode]
  );

  const typeBase = useMemo(
    () =>
      permissionBaseTypes.filter(
        (el) => el.value === data?.baseDocumentCodes
      )[0]?.value ||
      permissionBaseTypes.filter((el) => el.value === baseDocumentCode)[0]
        ?.value ||
      [],
    [data, permissionBaseTypes, baseDocumentCode]
  );

  const initialState = useMemo(
    () =>
      data?.state ||
      states.filter((state) => state.value === 'Разрешение')[0]?.value,
    [data, states]
  );

  const { cols, rows } = filesToTableAdapter(undefined);
  const rowsForTable = rows;

  const initialValues = {
    ...(data || PermissionFormDefaults),
    contractorIds: contractors?.map((el) => el.id) || defaultContractor || [],
    type: type || '',
    baseDocument: typeBase || '',
    stateCode: initialState,
    baseDocumentNumber: baseDocumentNumber || '',
    isAnnulmentBasePermission: isAnnulmentBasePermission,
    baseObjectId: baseObjectId,
  };

  const handleSubmit = async (values: any) => {
    if (!id) {
      const createData = {
        [PermissionFormFields.NUMBER]: values.number,
        [PermissionFormFields.DATE]: values.date,
        [PermissionFormFields.END_DATE]: values.dateValidity,
        [PermissionFormFields.STATE]: values.stateCode,
        [PermissionFormFields.DATE_ANNULMENT]: values.dateAnnulment,
        [PermissionFormFields.CLIENT]: values.contractorIds,
        [PermissionFormFields.REASON]: values.annulmentReasonCode,
        [PermissionFormFields.OBJECTID]: values.baseObjectId,
        [PermissionFormFields.COMMENT]: values.comment,
        baseDocumentCode: baseDocumentCode,
        isAnnulmentBasePermission,
        requestId:
          baseDocumentCode === 'LIC_REASON_REQ'
            ? Number(baseObjectId)
            : undefined,
      };

      const response = await create({ data: createData });
      if ('data' in response) {
        setOpenSaveModal(false);
        navigate(`/permissions?id=${response.data.id}`);
        setEditMode(false);
      }
    } else {
      setEditMode(false);

      const updateData = {
        [PermissionFormFields.NUMBER]: values.number,
        [PermissionFormFields.DATE]: values.date,
        [PermissionFormFields.END_DATE]: values.dateValidity,
        [PermissionFormFields.STATE]: values.stateCode,
        [PermissionFormFields.DATE_ANNULMENT]: values.dateAnnulment,
        [PermissionFormFields.CLIENT]: values.contractorIds,
        [PermissionFormFields.REASON]: values.annulmentReasonCode,
        [PermissionFormFields.COMMENT]: values.comment,
        requestId:
          baseDocumentCode === 'LIC_REASON_REQ'
            ? Number(baseObjectId)
            : undefined,
      };

      await update({ id: id, data: updateData });
      window.location.reload();
    }
  };

  const handleExtractModal = async () => {
    const obj = {
      permissionId: id,
      requestId: data?.requestId,
    };
    const response = await selectNewPermission({ data: obj });
    if ('data' in response) {
      setOpenExtractModal(false);
      navigate(Routes.REQUESTS.REQUEST.replace(':id', data?.requestId));
    }
    if ('error' in response) {
      const key = enqueueSnackbar(
        'Ошибка при попытке выделить разрешения в заявку.',
        {
          autoHideDuration: 10000,
          onClick: () => closeSnackbar(key),
          variant: 'error',
          anchorOrigin: { horizontal: 'right', vertical: 'top' },
          style: { whiteSpace: 'pre-line' },
        }
      );
    }
  };

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const getOptions = useCallback(async (value: string) => {
    if (value) {
      const response = await getContractorsList({
        name: value,
        isOnlyActual: true,
      });
      if ('data' in response) {
        return response.data.data.map((option: Record<string, any>) => {
          return {
            label: option.name,
            value: option.id,
          };
        });
      }
      return [];
    }
    return [];
  }, []);

  const getValueOptions = useCallback(
    async (ids: number[]) => {
      if (ids && ids.length) {
        const counterparties = await getCounterparties({ ids });
        if ('data' in counterparties) {
          return (
            counterparties?.data.data.map((option: Counterparty) => {
              return {
                label: option.name,
                value: option.id,
              };
            }) || []
          );
        } else {
          return [];
        }
      }
      return [];
    },
    [getCounterparties]
  );

  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        enableReinitialize
        onSubmit={handleSubmit}
      >
        {({
          values,
          handleChange,
          handleSubmit,
          errors,
          setFieldValue,
          touched,
          setTouched,
          handleReset,
        }) => {
          const handleAdd = (value: number[]) => {
            setFieldValue(PermissionFormFields.CLIENT, value[0]);
          };

          return (
            <>
              <Form>
                <Box my={3}>
                  <ToolsPanel
                    leftActions={
                      !editMode ? (
                        <RoundButton
                          icon={<Edit />}
                          onClick={() => setEditMode(true)}
                        />
                      ) : (
                        <>
                          <RoundButton
                            icon={<Save />}
                            onClick={() =>
                              !id && permissionTerm?.permissionExpired
                                ? setOpenSaveModal(true)
                                : handleSubmit()
                            }
                          />
                          <RoundButton
                            icon={<EditOff />}
                            onClick={() => {
                              handleReset();
                              setEditMode(false);
                            }}
                          />
                        </>
                      )
                    }
                  >
                    {data?.canExtractPermission ? (
                      <ToolButton
                        label={'Выделить разрешение'}
                        onClick={() => setOpenExtractModal(true)}
                        fast={true}
                        startIcon={<AddTask />}
                      />
                    ) : null}
                  </ToolsPanel>
                </Box>
                <Grid container columns={6} columnSpacing={9} rowSpacing={3}>
                  <Grid item xs={3}>
                    <Input
                      name={PermissionFormFields.NUMBER}
                      label={PermissionFormLabels[PermissionFormFields.NUMBER]}
                      disabled={!editMode}
                      value={values[PermissionFormFields.NUMBER]}
                      onChange={handleChange}
                      error={Boolean(
                        errors[PermissionFormFields.NUMBER] &&
                          touched[PermissionFormFields.NUMBER]
                      )}
                      onBlur={() =>
                        setTouched({ [PermissionFormFields.NUMBER]: true })
                      }
                      helperText={errors[PermissionFormFields.NUMBER] as string}
                      required
                    />
                  </Grid>
                  <Grid item xs={3}>
                    <DatePickerInput
                      label={PermissionFormLabels[PermissionFormFields.DATE]}
                      name={PermissionFormFields.DATE}
                      disabled={!editMode}
                      required
                    />
                  </Grid>
                  <Grid item xs={3}>
                    <DatePickerInput
                      label={
                        PermissionFormLabels[PermissionFormFields.END_DATE]
                      }
                      name={PermissionFormFields.END_DATE}
                      disabled={!editMode}
                    />
                  </Grid>
                  <Grid item xs={3}>
                    <AutoCompleteInput
                      name={PermissionFormFields.TYPE}
                      label={PermissionFormLabels[PermissionFormFields.TYPE]}
                      disabled
                      options={permissionTypes}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <AutoCompleteInput
                      name={PermissionFormFields.STATE}
                      label={PermissionFormLabels[PermissionFormFields.STATE]}
                      options={states}
                      disabled={!editMode}
                    />
                  </Grid>
                  <Grid item xs={3}>
                    <AutoCompleteInput
                      name={PermissionFormFields.BASE_DOCUMENT}
                      label={
                        PermissionFormLabels[PermissionFormFields.BASE_DOCUMENT]
                      }
                      options={permissionBaseTypes}
                      disabled
                    />
                  </Grid>
                  <Grid item xs={3} className={styles.container}>
                    <Input
                      name={PermissionFormFields.BASE_DOCUMENT_NUMBER}
                      label={
                        PermissionFormLabels[
                          PermissionFormFields.BASE_DOCUMENT_NUMBER
                        ]
                      }
                      disabled
                      value={values[PermissionFormFields.BASE_DOCUMENT_NUMBER]}
                    />
                    <RoundButton
                      icon={<OpenInBrowser />}
                      onClick={() => {
                        baseDocumentCode === 'LIC_REASON_REQ'
                          ? window
                              .open(
                                Routes.REQUESTS.REQUEST.replace(
                                  ':id',
                                  baseObjectId
                                ),
                                openFormInNewTab ? '_blank' : '_self'
                              )
                              ?.focus()
                          : baseDocumentCode === 'LIC_REASON_CONCL'
                          ? window
                              .open(
                                `/conclusions?id=${baseObjectId}`,
                                openFormInNewTab ? '_blank' : '_self'
                              )
                              ?.focus()
                          : window
                              .open(
                                `/permissions?id=${baseObjectId}`,
                                openFormInNewTab ? '_blank' : '_self'
                              )
                              ?.focus();
                      }}
                    />
                  </Grid>
                  <Grid item xs={6} className={styles.container}>
                    <DebounceAutoComplete
                      name={PermissionFormFields.CLIENT}
                      label={PermissionFormLabels[PermissionFormFields.CLIENT]}
                      getOptions={getOptions}
                      getValueOptions={getValueOptions}
                      disabled={!editMode}
                      required
                      multiple
                    />
                    <RoundButton
                      icon={<PersonPin />}
                      onClick={() => setOpenModal(true)}
                      disabled={!editMode}
                    />
                    <ContractorSearchForm
                      openClerkModal={openModal}
                      onClose={() => setOpenModal(false)}
                      addDoc={handleAdd}
                    />
                  </Grid>
                  <Grid item xs={3}>
                    <DatePickerInput
                      label={
                        PermissionFormLabels[
                          PermissionFormFields.DATE_ANNULMENT
                        ]
                      }
                      name={PermissionFormFields.DATE_ANNULMENT}
                      disabled={!editMode}
                    />
                  </Grid>
                  <Grid item xs={3}>
                    <AutoCompleteInput
                      name={PermissionFormFields.REASON}
                      label={PermissionFormLabels[PermissionFormFields.REASON]}
                      options={reasons}
                      disabled={!editMode}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <FilesTable
                      label="Файлы"
                      columns={cols}
                      rows={rowsForTable}
                      tableHeight="100px"
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <Input
                      name={PermissionFormFields.COMMENT}
                      label={PermissionFormLabels[PermissionFormFields.COMMENT]}
                      multiline
                      value={values[PermissionFormFields.COMMENT]}
                      onChange={handleChange}
                      disabled={!editMode}
                    />
                  </Grid>
                </Grid>
              </Form>
              <Dialog open={openSaveModal}>
                <Card className={styles.card}>
                  <div className={styles.dialogText}>
                    Период действия разрешения № {baseDocumentNumber} от{' '}
                    {baseDocumentDate} закончился. Аннулировать разрешение?
                  </div>
                  <Stack
                    direction="row"
                    justifyContent="flex-end"
                    spacing={1.5}
                    width="100%"
                  >
                    <Button
                      variant="contained"
                      endIcon={<Cancel />}
                      onClick={() => {
                        setIsAnnulmentBasePermission(true);
                        handleSubmit();
                      }}
                    >
                      Да (по умолчанию)
                    </Button>
                    <Button
                      variant="outlined"
                      endIcon={<DeleteOutline />}
                      onClick={() => handleSubmit()}
                    >
                      Нет
                    </Button>
                  </Stack>
                </Card>
              </Dialog>

              <Dialog open={openExtractModal}>
                <Card className={styles.card}>
                  <div className={styles.dialogText}>
                    Выделить Разрешение в отдельную заявку?
                  </div>
                  <Stack
                    direction="row"
                    justifyContent="flex-end"
                    spacing={1.5}
                    width="100%"
                  >
                    <Button
                      variant="contained"
                      endIcon={<Cancel />}
                      onClick={() => handleExtractModal()}
                    >
                      Да
                    </Button>
                    <Button
                      variant="outlined"
                      endIcon={<DeleteOutline />}
                      onClick={() => setOpenExtractModal(false)}
                    >
                      Нет
                    </Button>
                  </Stack>
                </Card>
              </Dialog>
            </>
          );
        }}
      </Formik>
    </>
  );
};

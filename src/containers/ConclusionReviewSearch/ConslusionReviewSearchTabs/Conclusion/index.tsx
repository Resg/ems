import React, { useCallback, useEffect, useMemo } from 'react';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { Checkbox, FormControlLabel, Grid } from '@mui/material';

import { DateRange } from '../../../../components/DateRange';
import { DebounceAutoComplete } from '../../../../components/DebounceAutoComplete';
import { FieldWrapper } from '../../../../components/FieldWrapper';
import Input from '../../../../components/Input';
import {
  ConclusionReviewSearchFields,
  ConclusionReviewSearchLabel,
} from '../../../../constants/conclusionsReview';
import {
  useGetAsyncContractorsListMutation,
  useGetCounterpartiesAsyncMutation,
} from '../../../../services/api/dictionaries';
import { Counterparty } from '../../../../types/dictionaries';

import styles from './styles.module.scss';

interface ConclusionProps {}

export const Conclusion: React.FC<ConclusionProps> = () => {
  const {
    handleChange,
    values = {} as any,
    setFieldValue,
  } = useFormikContext();

  const [getCounterparties] = useGetCounterpartiesAsyncMutation();
  const [getContractorsList] = useGetAsyncContractorsListMutation();

  const getOptions = useCallback(async (value: string) => {
    if (value) {
      const response = await getContractorsList({ name: value });
      if ('data' in response) {
        return response.data.data.map((option: Record<string, any>) => {
          return {
            label: option.name,
            value: option.id,
          };
        });
      }
      return [];
    }
    return [];
  }, []);

  const getValueOptions = useCallback(
    async (ids: number[]) => {
      if (ids && ids.length) {
        const counterparties = await getCounterparties({ ids });
        if ('data' in counterparties) {
          return (
            counterparties?.data.data.map((option: Counterparty) => {
              return {
                label: option.name,
                value: option.id,
              };
            }) || []
          );
        } else {
          return [];
        }
      }
      return [];
    },
    [getCounterparties]
  );

  const isDate = useMemo(
    () => get(values, ConclusionReviewSearchFields.DATE),
    [values]
  );

  useEffect(() => {
    if (!isDate) {
      setFieldValue(ConclusionReviewSearchFields.DATE_FROM, '');
      setFieldValue(ConclusionReviewSearchFields.DATE_TO, '');
    }
  }, [isDate]);

  return (
    <Grid container spacing={2}>
      <Grid item xs={6}>
        <Input
          name={ConclusionReviewSearchFields.NUMBER}
          label={
            ConclusionReviewSearchLabel[ConclusionReviewSearchFields.NUMBER]
          }
          value={get(values, ConclusionReviewSearchFields.NUMBER) || ''}
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={6} className={styles.dateInput}>
        <FieldWrapper
          name={ConclusionReviewSearchFields.DATE}
          defaultValue=""
          className={styles.checkbox}
        >
          <FormControlLabel
            label=""
            control={
              <Checkbox
                name={ConclusionReviewSearchFields.DATE}
                checked={
                  get(values, ConclusionReviewSearchFields.DATE) || false
                }
                onChange={() =>
                  setFieldValue(ConclusionReviewSearchFields.DATE, !isDate)
                }
              />
            }
          />
        </FieldWrapper>
        <DateRange
          nameFrom={ConclusionReviewSearchFields.DATE_FROM}
          nameBefore={ConclusionReviewSearchFields.DATE_TO}
          labelFrom={
            ConclusionReviewSearchLabel[ConclusionReviewSearchFields.DATE_FROM]
          }
          className={styles.dateRange}
          disabled={!isDate}
          clearField
        />
      </Grid>
      <Grid item xs={12}>
        <DebounceAutoComplete
          name={ConclusionReviewSearchFields.CONTRACTOR}
          label={
            ConclusionReviewSearchLabel[ConclusionReviewSearchFields.CONTRACTOR]
          }
          getOptions={getOptions}
          getValueOptions={getValueOptions}
        />
      </Grid>
    </Grid>
  );
};

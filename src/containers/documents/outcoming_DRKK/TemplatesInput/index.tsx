import React, { useEffect } from 'react';
import { useFormikContext } from 'formik';

import { AutoCompleteInput } from '../../../../components/AutoCompleteInput';
import {
  OutcomingDRKKFields,
  OutcomingDRKKLabels,
} from '../../../../constants/outcomingDRKKForm';
import { useGetDocumentTemplatesQuery } from '../../../../services/api/dictionaries';

export interface TemplatesInputProps {
  disabled: boolean;
}

export const TemplatesInput: React.FC<TemplatesInputProps> = ({ disabled }) => {
  const { values, setFieldValue } = useFormikContext<any>();
  const rubriks = values.rubricList;
  const { data: templates = [] } = useGetDocumentTemplatesQuery(
    { rubricIds: rubriks },
    { skip: !rubriks.length }
  );

  useEffect(() => {
    !rubriks.length && setFieldValue(OutcomingDRKKFields.TEMPLATE, null);
  }, [rubriks]);

  return (
    <AutoCompleteInput
      name={OutcomingDRKKFields.TEMPLATE}
      label={OutcomingDRKKLabels[OutcomingDRKKFields.TEMPLATE]}
      options={rubriks.length ? templates : []}
      disabled={disabled}
    />
  );
};

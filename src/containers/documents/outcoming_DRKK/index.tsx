import React, { useCallback, useMemo, useState } from 'react';
import { Form, Formik } from 'formik';
import { useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';

import {
  Article,
  DeleteSweep,
  Edit,
  InsertDriveFile,
  PlaylistAdd,
  QrCode,
  ReadMore,
  Save,
} from '@mui/icons-material';
import { Grid } from '@mui/material';

import { AutoCompleteInput } from '../../../components/AutoCompleteInput';
import { CheckboxInput } from '../../../components/CheckboxInput';
import { ClerkInput } from '../../../components/ClerkInput';
import { DatePickerInput } from '../../../components/DatePickerInput';
import { BarCode } from '../../../components/Icons';
import Input from '../../../components/Input';
import { ProgressCircular } from '../../../components/ProgressCircular';
import { RoundButton } from '../../../components/RoundButton';
import {
  SettingsTypes,
  ToolButton,
  ToolsPanel,
} from '../../../components/ToolsPanel';
import {
  DefaultOutcomingDRKK,
  OutcomingDRKKFields,
  OutcomingDRKKLabels,
} from '../../../constants/outcomingDRKKForm';
import AddToRegistryPopup from '../../../containers/Registry/AddToRegistryPopup';
import { useGetDictionaries } from '../../../services/api/dictionaries/useGetDictionaries';
import {
  useChangeDateSignMutation,
  useCreateDocumentMutation,
  useGetDocumentByIdQuery,
  useGetDocumentOwnersQuery,
  useGetDocumentRubricsQuery,
  useRecreateDocumentFileMutation,
  useUpdateDocumentMutation,
} from '../../../services/api/document';
import { useGetUserQuery } from '../../../services/api/user';
import { _axios } from '../../../services/axios';
import { RootState } from '../../../store';
import type { CommonDocumentFormData } from '../../../types/documents';
import { rubricsToFormAdapter } from '../../../utils/dictionaries';
import {
  outcomingDRKKCreateData,
  outcomingDRKKUpdateData,
  ownersToFormAdapter,
  sortByLabel,
} from '../../../utils/documents';

import { TemplatesInput } from './TemplatesInput';
import { validationSchema } from './validation';

export interface OutcomingDRKKProps {
  formData?: CommonDocumentFormData;
}

export const OutcomingDRKK: React.FC<OutcomingDRKKProps> = ({ formData }) => {
  const canEdit = formData?.canEdit !== false;
  const navigate = useNavigate();
  const [popupMode, setPopupMode] = useState<'documents' | 'packets' | null>(
    null
  );
  const [changeDate] = useChangeDateSignMutation();
  const [createDocument] = useCreateDocumentMutation();
  const [updateDocument] = useUpdateDocumentMutation();
  const [recreateDocumentFile] = useRecreateDocumentFileMutation();
  const { id = '' } = useParams();
  const [editMode, setEditMode] = useState(!id);
  const [openModal, setOpenModal] = useState(false);
  const [success, setSuccess] = useState(false);
  const { data: owners } = useGetDocumentOwnersQuery({ id }, { skip: !id });
  const { data: docRubrics } = useGetDocumentRubricsQuery(
    { id },
    { skip: !id }
  );
  const { data: userInfo } = useGetUserQuery({});

  const { data: documentData } = useGetDocumentByIdQuery({ id }, { skip: !id });

  const { clerkNameSigner, clerkNameExe } = ownersToFormAdapter(owners?.data);
  const rubricList = useMemo(
    () => docRubrics?.data.map((i) => i.id) || [],
    [docRubrics]
  );

  const { dictionariesRubrics } = useGetDictionaries({
    rubrics: {
      documentTypeId: formData?.typeId ?? 100,
    },
    templates: {
      rubricIds: rubricList,
    },
  });

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const handleSubmit = async (values: any) => {
    if (formData) {
      values.dateSign &&
        changeDate({ id, data: { id, dateSign: values.dateSign } });
      updateDocument({ id, documentData: outcomingDRKKUpdateData(values) });
      setEditMode(false);
    } else {
      const res = await createDocument({
        documentData: outcomingDRKKCreateData(userInfo!.divisionId, values),
      });

      if ('data' in res) {
        navigate(`/documents?id=${res.data.id}`);
        window.location.reload();
      }
    }
  };
  const handleDetailsBarCode = useCallback(() => {
    window
      .open(
        `http://10.177.5.121:14000/barcode?code=${id}&barType=CODE128&Code128Set=C&x=0.05&barHeightCM=0.5`,
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  }, [id, openFormInNewTab]);

  const handleDetailsQRcode = useCallback(() => {
    _axios
      .get(
        `/documents/qrcode/document-file-archive-url?documentId=${id}&format=PNG`,
        { responseType: 'blob' }
      )
      .then((response: any) => {
        const blob = new Blob([response.data], { type: response.data.type });
        const url = URL.createObjectURL(blob);
        window.open(url, openFormInNewTab ? '_blank' : '_self');
      });
  }, [id, openFormInNewTab]);

  const initialValues = useMemo(
    () => ({
      ...(formData || DefaultOutcomingDRKK),
      dateCreate: formData?.dateCreate.split('T')[0] || null,
      signerIds: clerkNameSigner?.length ? clerkNameSigner : [],
      rubricList,
      performer: clerkNameExe ? clerkNameExe.clerkId : null,
    }),
    [id, formData, clerkNameSigner, clerkNameExe]
  );

  const handleRecreate = async () => {
    setOpenModal(true);

    const response = await recreateDocumentFile({ documentId: id });
    if ('data' in response) {
      setSuccess(true);
      window.open(
        `/file_editor/${response.data.id}`,
        openFormInNewTab ? '_blank' : '_self'
      );
    }
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      enableReinitialize
      onSubmit={handleSubmit}
    >
      {({
        handleChange,
        values,
        errors,
        touched,
        handleSubmit,
        setTouched,
      }) => {
        return (
          <Form>
            <Grid container spacing={2} sx={{ mt: 0 }}>
              <Grid item container xs={12}>
                <ToolsPanel
                  settings={[SettingsTypes.TOOLS]}
                  leftActions={
                    <>
                      {(editMode || !canEdit) && (
                        <RoundButton
                          onClick={() => handleSubmit()}
                          icon={<Save />}
                        />
                      )}
                      {!editMode && (
                        <RoundButton
                          onClick={() => setEditMode(true)}
                          icon={<Edit />}
                        />
                      )}
                    </>
                  }
                >
                  <ToolButton
                    label={'Перейти к объекту'}
                    startIcon={<ReadMore />}
                    disabled
                    fast
                  />
                  <ToolButton
                    label={'перейти к ЭРК'}
                    startIcon={<Article />}
                    disabled
                    fast
                  />
                  <ToolButton
                    label={'сформировать документ'}
                    startIcon={<InsertDriveFile />}
                    onClick={handleRecreate}
                    disabled={formData?.objectTypeCode !== 'CONCLUSION'}
                    fast
                  />
                  <ToolButton
                    label={'добавить в реестр'}
                    startIcon={<PlaylistAdd />}
                    onClick={() => setPopupMode('documents')}
                    disabled={!id}
                    fast
                  />
                  <ToolButton
                    label={'удалить из реестра'}
                    startIcon={<DeleteSweep />}
                    disabled
                    fast
                  />
                  <ToolButton
                    label={'Печать штрих-кода'}
                    startIcon={<BarCode />}
                    disabled={!id}
                    onClick={() => handleDetailsBarCode()}
                  />
                  <ToolButton
                    label={'Печать QR-кода'}
                    startIcon={<QrCode />}
                    disabled={!id}
                    onClick={() => handleDetailsQRcode()}
                  />
                </ToolsPanel>
              </Grid>
              <Grid item container columns={4} spacing={2} sx={{ mt: 0 }}>
                <Grid item xs={1}>
                  <Input
                    name={OutcomingDRKKFields.CODE}
                    label={OutcomingDRKKLabels[OutcomingDRKKFields.CODE]}
                    value={formData && values[OutcomingDRKKFields.CODE]}
                    disabled
                  />
                </Grid>
                <Grid item xs={1}>
                  <Input
                    name={OutcomingDRKKFields.NUMBER}
                    label={OutcomingDRKKLabels[OutcomingDRKKFields.NUMBER]}
                    value={formData && values[OutcomingDRKKFields.NUMBER]}
                    disabled
                  />
                </Grid>
                <Grid item xs={1}>
                  <DatePickerInput
                    name={OutcomingDRKKFields.DATE_CREATE}
                    label={OutcomingDRKKLabels[OutcomingDRKKFields.DATE_CREATE]}
                    disabled
                  />
                </Grid>
                <Grid item xs={1}>
                  <DatePickerInput
                    name={OutcomingDRKKFields.DATE_SIGN}
                    label={OutcomingDRKKLabels[OutcomingDRKKFields.DATE_SIGN]}
                    disabled={!id}
                  />
                </Grid>
                <Grid item xs={1}>
                  <CheckboxInput
                    name={OutcomingDRKKFields.ACCESS}
                    label={OutcomingDRKKLabels[OutcomingDRKKFields.ACCESS]}
                    disabled={!editMode}
                  />
                </Grid>
                <Grid item xs={1}>
                  <CheckboxInput
                    name={OutcomingDRKKFields.EDS}
                    label={OutcomingDRKKLabels[OutcomingDRKKFields.EDS]}
                    disabled
                  />
                </Grid>
                <Grid item xs={4}>
                  <ClerkInput
                    name={OutcomingDRKKFields.SIGNERS}
                    label={OutcomingDRKKLabels[OutcomingDRKKFields.SIGNERS]}
                    multiple
                    disabled={!editMode}
                    required
                  />
                </Grid>
                <Grid item xs={4}>
                  <AutoCompleteInput
                    name={OutcomingDRKKFields.RUBRICKS}
                    label={OutcomingDRKKLabels[OutcomingDRKKFields.RUBRICKS]}
                    options={sortByLabel(
                      rubricsToFormAdapter(dictionariesRubrics)
                    )}
                    disabled={!editMode}
                    multiple
                    required
                  />
                </Grid>
                <Grid item xs={2}>
                  <Input
                    name={OutcomingDRKKFields.CONTENT}
                    label={OutcomingDRKKLabels[OutcomingDRKKFields.CONTENT]}
                    value={formData && values[OutcomingDRKKFields.CONTENT]}
                    onChange={handleChange}
                    error={Boolean(
                      errors[OutcomingDRKKFields.CONTENT] &&
                        touched[OutcomingDRKKFields.CONTENT]
                    )}
                    onBlur={() =>
                      setTouched({ [OutcomingDRKKFields.CONTENT]: true })
                    }
                    helperText={errors[OutcomingDRKKFields.CONTENT]}
                    disabled={!editMode}
                    multiline
                    required
                  />
                </Grid>
                <Grid item xs={2}>
                  <Input
                    name={OutcomingDRKKFields.COMMENTS}
                    label={OutcomingDRKKLabels[OutcomingDRKKFields.COMMENTS]}
                    value={formData && values[OutcomingDRKKFields.COMMENTS]}
                    onChange={handleChange}
                    multiline
                    disabled={!editMode}
                  />
                </Grid>
                <Grid item xs={2}>
                  <Input
                    name={OutcomingDRKKFields.STRUCTURE}
                    label={OutcomingDRKKLabels[OutcomingDRKKFields.STRUCTURE]}
                    value={formData && values[OutcomingDRKKFields.STRUCTURE]}
                    disabled
                  />
                </Grid>
                <Grid item xs={2}></Grid>
                <Grid item xs={2}>
                  <ClerkInput
                    name={OutcomingDRKKFields.PERFORMER}
                    label={OutcomingDRKKLabels[OutcomingDRKKFields.PERFORMER]}
                    disabled={!editMode}
                  />
                </Grid>
                <Grid item xs={2}>
                  <TemplatesInput disabled={!editMode} />
                </Grid>
              </Grid>
            </Grid>
            <AddToRegistryPopup
              mode={popupMode}
              setMode={setPopupMode}
              documentIds={Number(id)}
            />
            <ProgressCircular
              openModal={openModal}
              onClose={() => setOpenModal(false)}
              success={success}
            />
          </Form>
        );
      }}
    </Formik>
  );
};

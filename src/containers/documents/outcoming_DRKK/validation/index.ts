import { array, boolean, date, number, object, string } from 'yup';

import { OutcomingDRKKFields } from '../../../../constants/outcomingDRKKForm';

export const validationSchema = object({
  [OutcomingDRKKFields.CODE]: number().nullable(),
  [OutcomingDRKKFields.NUMBER]: string().nullable(),
  [OutcomingDRKKFields.DATE_CREATE]: date().nullable(),
  [OutcomingDRKKFields.DATE_SIGN]: date().nullable(),
  [OutcomingDRKKFields.ACCESS]: boolean(),
  [OutcomingDRKKFields.EDS]: boolean(),
  [OutcomingDRKKFields.SIGNERS]: array()
    .of(number())
    .min(1, 'Обязательное поле'),
  [OutcomingDRKKFields.RUBRICKS]: array()
    .of(number())
    .min(1, 'Обязательное поле'),
  [OutcomingDRKKFields.CONTENT]: string().required('Обязательное поле'),
  [OutcomingDRKKFields.COMMENTS]: string().nullable(),
  [OutcomingDRKKFields.STRUCTURE]: string().nullable(),
  [OutcomingDRKKFields.PERFORMER]: number().nullable(),
  [OutcomingDRKKFields.TEMPLATE]: number().nullable(),
});

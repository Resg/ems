import { array, number, object, string } from 'yup';

export const validationSchema = object({
  documentAccessCode: string().required('Обязательное поле'),
  clerkNameSigner: array().of(number()).min(1, 'Обязательное поле'),
  rubricList: array().of(number()).min(1, 'Обязательное поле'),
  description: string().required('Обязательное поле'),
});

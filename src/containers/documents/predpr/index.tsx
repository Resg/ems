import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Form, Formik, useFormikContext } from 'formik';
import { isEqual } from 'lodash';
import { useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';

import {
  Cancel,
  CheckCircle,
  DeleteSweep,
  Edit,
  InsertDriveFile,
  PlaylistAdd,
  QrCode,
  RemoveCircle,
  Save,
  Unpublished,
} from '@mui/icons-material';
import { Grid } from '@mui/material';

import { AutoCompleteInput } from '../../../components/AutoCompleteInput';
import { CheckboxInput } from '../../../components/CheckboxInput';
import { ClerkInput } from '../../../components/ClerkInput';
import { DatePickerInput } from '../../../components/DatePickerInput';
import { BarCode } from '../../../components/Icons';
import Input from '../../../components/Input';
import { RoundButton } from '../../../components/RoundButton';
import {
  SettingsTypes,
  ToolButton,
  ToolsPanel,
} from '../../../components/ToolsPanel';
import {
  ControlFields,
  DefaultPredprDocument,
  PredprDocumentFields,
  PredprDocumentLabels,
} from '../../../constants/predprDocumentForm';
import { useGetDictionaries } from '../../../services/api/dictionaries/useGetDictionaries';
import {
  useCreateDocumentMutation,
  useGetDocumentByIdQuery,
  useGetDocumentOwnersQuery,
  useGetDocumentRubricsQuery,
  useUpdateDocumentMutation,
} from '../../../services/api/document';
import { useGetUserQuery } from '../../../services/api/user';
import { _axios } from '../../../services/axios';
import { RootState } from '../../../store';
import {
  CommonDocumentFormData,
  DocumentOwnerType,
  DocumentRubricType,
} from '../../../types/documents';
import { prepareFieldsData } from '../../../utils/documents';
import AddToRegistryPopup from '../../Registry/AddToRegistryPopup';

import { validationSchema } from './validation';

import styles from '../styles.module.scss';

export interface PredprDocumentFormProps {
  formData?: CommonDocumentFormData;
  editMode?: boolean;
}

const PredprDocumentForm: React.FC<PredprDocumentFormProps> = ({
  formData,
  editMode,
}) => {
  const [popupMode, setPopupMode] = useState<'documents' | 'packets' | null>(
    null
  );
  const { data: user } = useGetUserQuery({});
  const [isEditMode, setEditMode] = useState(!!editMode);

  const navigate = useNavigate();
  const { id = '' } = useParams();

  const { data: documentOwners } = useGetDocumentOwnersQuery(
    { id },
    { skip: !id }
  );
  const { data: documentRubrics } = useGetDocumentRubricsQuery(
    { id },
    { skip: !id }
  );
  const { data: documentData } = useGetDocumentByIdQuery({ id }, { skip: !id });
  const [updateDocument] = useUpdateDocumentMutation();
  const [createDocument] = useCreateDocumentMutation();

  const getRubricList = useMemo(() => {
    return (
      documentRubrics?.data.map((rubric: DocumentRubricType) => {
        return rubric.id;
      }) || []
    );
  }, [documentRubrics]);

  const UpdateRubricListIds = () => {
    const { values = {} as any } = useFormikContext();
    useEffect(() => {
      setRubricList((prevState) => {
        const newState = values.rubricList || [];
        return isEqual(newState, prevState) ? prevState : newState;
      });
    }, [values.rubricList]);

    return null;
  };

  const [rubricListIds, setRubricList] = useState<number[]>(
    getRubricList || []
  );
  const {
    dictionariesAccesses = [],
    dictionariesRubrics = [],
    dictionariesClerks = [],
    dictionariesTemplates = [],
  } = useGetDictionaries({
    rubrics: {
      documentTypeId: 23,
    },
    templates: {
      rubricIds: rubricListIds,
    },
  });

  const getClerkList = useCallback(
    (usageCode: string) => {
      return dictionariesClerks.length > 0 && documentOwners
        ? documentOwners.data
            .filter((clerk: DocumentOwnerType) => clerk.usageCode === usageCode)
            .map((clerk) => clerk.personId)
        : [];
    },
    [documentOwners, dictionariesClerks]
  );

  const defaultRegister = useMemo(() => {
    return dictionariesClerks.length > 0
      ? dictionariesClerks.find((clerk) => clerk.isDefaultRegistrar)?.personId
      : null;
  }, [dictionariesClerks]);

  const handleSubmit = async (values = {} as any) => {
    const data = prepareFieldsData(
      !formData,
      values,
      dictionariesClerks,
      documentOwners?.data
    );
    if (formData) {
      await updateDocument({ id: values.id, documentData: data });
      setEditMode(false);
    } else {
      const res = await createDocument({
        documentData: { ...data, typeId: 23 },
      });
      if ('data' in res) {
        navigate(`/documents?id=${res.data.id}`);
        window.location.reload();
      }
    }
  };

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const handleDetailsBarCode = useCallback(() => {
    window
      .open(
        `http://10.177.5.121:14000/barcode?code=${id}&barType=CODE128&Code128Set=C&x=0.05&barHeightCM=0.5`,
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  }, [id, openFormInNewTab]);

  const handleDetailsQRcode = useCallback(() => {
    _axios
      .get(
        `/documents/qrcode/document-file-archive-url?documentId=${id}&format=PNG`,
        { responseType: 'blob' }
      )
      .then((response: any) => {
        const blob = new Blob([response.data], { type: response.data.type });
        const url = URL.createObjectURL(blob);
        window.open(url, openFormInNewTab ? '_blank' : '_self');
      });
  }, [id, openFormInNewTab]);

  const initialValues = useMemo(
    () => ({
      ...(formData || DefaultPredprDocument),
      documentAccessCode: formData ? formData.documentAccessCode : 'COM',
      clerkNameSigner: getClerkList('SIGNER'),
      clerkNameReceiver: getClerkList('RECEIVER'),
      clerkNameExe: formData ? getClerkList('EXE')[0] : user?.personId,
      clerkNameRegister: formData
        ? getClerkList('REGISTER')[0]
        : defaultRegister,
      rubricList: getRubricList,
    }),
    [
      formData,
      id,
      documentOwners,
      documentRubrics,
      dictionariesClerks,
      dictionariesRubrics,
    ]
  );

  const renderControlItem = useCallback(() => {
    if (formData) {
      const controlIcon = [
        <RemoveCircle color="disabled" />,
        <CheckCircle color="error" />,
        <Unpublished />,
      ][formData.isControl];
      const controlLabel = ControlFields[formData.isControl];

      return (
        <div className={styles.controlItem}>
          {controlIcon}
          {controlLabel}
        </div>
      );
    }
  }, [formData]);

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      enableReinitialize
      onSubmit={handleSubmit}
    >
      {({
        handleSubmit,
        handleReset,
        handleChange,
        values,
        errors,
        touched,
        setTouched,
      }) => {
        return (
          <Form>
            <ToolsPanel
              leftActions={
                isEditMode ? (
                  <>
                    <RoundButton
                      onClick={() => handleSubmit()}
                      icon={<Save />}
                    />
                    <RoundButton
                      icon={<Cancel />}
                      onClick={() => {
                        handleReset();
                        setEditMode(false);
                      }}
                    />
                  </>
                ) : (
                  <RoundButton
                    icon={<Edit />}
                    onClick={() => setEditMode(true)}
                  />
                )
              }
              settings={[SettingsTypes.TOOLS]}
              className={styles.tools}
            >
              <ToolButton
                label={'Сформировать документ'}
                startIcon={<InsertDriveFile />}
                fast
              />
              <ToolButton
                label={'Добавить в реестр'}
                startIcon={<PlaylistAdd />}
                onClick={() => setPopupMode('documents')}
                fast
              />
              <ToolButton
                label={'Удалить из реестра'}
                startIcon={<DeleteSweep />}
                fast
              />
              <ToolButton
                label={'Печать штрих-кода'}
                startIcon={<BarCode />}
                onClick={() => handleDetailsBarCode()}
                disabled={!id}
              />
              <ToolButton
                label={'Печать QR-кода'}
                startIcon={<QrCode />}
                onClick={() => handleDetailsQRcode()}
                disabled={!id}
              />
            </ToolsPanel>
            <Grid container columnSpacing={4} rowSpacing={2}>
              <Grid item xs={3}>
                <Input
                  name={PredprDocumentFields.ID}
                  label={PredprDocumentLabels[PredprDocumentFields.ID]}
                  value={values[PredprDocumentFields.ID] || ''}
                  notEditableField={isEditMode}
                  disabled
                />
              </Grid>
              <Grid item xs={3}>
                <Input
                  name={PredprDocumentFields.NUMBER}
                  label={PredprDocumentLabels[PredprDocumentFields.NUMBER]}
                  value={values[PredprDocumentFields.NUMBER] || ''}
                  notEditableField={isEditMode}
                  disabled
                />
              </Grid>
              <Grid item xs={3}>
                <DatePickerInput
                  name={PredprDocumentFields.DATE_SIGN}
                  label={PredprDocumentLabels[PredprDocumentFields.DATE_SIGN]}
                  notEditableField={isEditMode}
                  disabled
                />
              </Grid>
              <Grid item xs={3}>
                <DatePickerInput
                  name={PredprDocumentFields.DATE}
                  label={PredprDocumentLabels[PredprDocumentFields.DATE]}
                  notEditableField={isEditMode}
                  disabled
                />
              </Grid>
              <Grid item xs={6}>
                <AutoCompleteInput
                  name={PredprDocumentFields.DOCUMENT_ACCESS}
                  label={
                    PredprDocumentLabels[PredprDocumentFields.DOCUMENT_ACCESS]
                  }
                  options={dictionariesAccesses.map((access) => {
                    return { value: access.code, label: access.name };
                  })}
                  disabled={!isEditMode}
                  required
                />
              </Grid>
              <Grid item xs={2}>
                <CheckboxInput
                  name={PredprDocumentFields.IS_CONFIDENTIAL}
                  label={
                    PredprDocumentLabels[PredprDocumentFields.IS_CONFIDENTIAL]
                  }
                  disabled={!isEditMode}
                />
              </Grid>
              <Grid item xs={2}>
                <CheckboxInput
                  name={PredprDocumentFields.IS_EDS}
                  label={PredprDocumentLabels[PredprDocumentFields.IS_EDS]}
                  disabled={!isEditMode}
                />
              </Grid>
              <Grid item xs={2}>
                {renderControlItem()}
              </Grid>
              <Grid item xs={6}>
                <ClerkInput
                  name={PredprDocumentFields.CLERK_NAME_SIGNER}
                  label={
                    PredprDocumentLabels[PredprDocumentFields.CLERK_NAME_SIGNER]
                  }
                  options={dictionariesClerks.map((clerk) => {
                    return { value: clerk.personId, label: clerk.personFio };
                  })}
                  property={'personId'}
                  multiple
                  disabled={!isEditMode}
                  required
                />
              </Grid>
              <Grid item xs={6}>
                <ClerkInput
                  name={PredprDocumentFields.CLERK_NAME_RECEIVER}
                  label={
                    PredprDocumentLabels[
                      PredprDocumentFields.CLERK_NAME_RECEIVER
                    ]
                  }
                  property={'personId'}
                  options={dictionariesClerks.map((clerk) => {
                    return { value: clerk.personId, label: clerk.personFio };
                  })}
                  multiple
                  disabled={!isEditMode}
                  showCollapse
                />
              </Grid>
              <Grid item xs={12}>
                <AutoCompleteInput
                  name={PredprDocumentFields.RUBRIC_LIST}
                  label={PredprDocumentLabels[PredprDocumentFields.RUBRIC_LIST]}
                  options={dictionariesRubrics.map((rubric) => {
                    return { value: rubric.id, label: rubric.name };
                  })}
                  multiple
                  disabled={!isEditMode}
                  required
                />
              </Grid>
              <Grid item xs={6}>
                <Input
                  name={PredprDocumentFields.DESCRIPTION}
                  label={PredprDocumentLabels[PredprDocumentFields.DESCRIPTION]}
                  value={values[PredprDocumentFields.DESCRIPTION] || ''}
                  error={Boolean(
                    errors[PredprDocumentFields.DESCRIPTION] &&
                      touched[PredprDocumentFields.DESCRIPTION]
                  )}
                  helperText={errors[PredprDocumentFields.DESCRIPTION]}
                  onBlur={() =>
                    setTouched({ [PredprDocumentFields.DESCRIPTION]: true })
                  }
                  onChange={handleChange}
                  disabled={!isEditMode}
                  multiline
                  required
                />
              </Grid>
              <Grid item xs={6}>
                <Input
                  name={PredprDocumentFields.COMMENTS}
                  label={PredprDocumentLabels[PredprDocumentFields.COMMENTS]}
                  value={values[PredprDocumentFields.COMMENTS] || ''}
                  onChange={handleChange}
                  disabled={!isEditMode}
                  multiline
                />
              </Grid>
              <Grid item xs={6}>
                <Input
                  name={PredprDocumentFields.COMPOUND}
                  label={PredprDocumentLabels[PredprDocumentFields.COMPOUND]}
                  value={values[PredprDocumentFields.COMPOUND] || ''}
                  onChange={handleChange}
                  disabled={!isEditMode}
                />
              </Grid>
              <Grid item xs={6}>
                <CheckboxInput
                  name={PredprDocumentFields.IS_ACTUAL}
                  label={PredprDocumentLabels[PredprDocumentFields.IS_ACTUAL]}
                  disabled={!isEditMode}
                />
              </Grid>
              <Grid item xs={6}>
                <ClerkInput
                  name={PredprDocumentFields.CLERK_NAME_EXE}
                  label={
                    PredprDocumentLabels[PredprDocumentFields.CLERK_NAME_EXE]
                  }
                  property={'personId'}
                  options={dictionariesClerks.map((clerk) => {
                    return { value: clerk.personId, label: clerk.personFio };
                  })}
                  disabled={!isEditMode}
                />
              </Grid>
              <Grid item xs={6}>
                <AutoCompleteInput
                  name={PredprDocumentFields.TEMPLATE_ID}
                  label={PredprDocumentLabels[PredprDocumentFields.TEMPLATE_ID]}
                  options={dictionariesTemplates}
                  disabled={!isEditMode}
                />
              </Grid>
              <Grid item xs={12}>
                <ClerkInput
                  name={PredprDocumentFields.CLERK_NAME_REGISTER}
                  label={
                    PredprDocumentLabels[
                      PredprDocumentFields.CLERK_NAME_REGISTER
                    ]
                  }
                  property={'personId'}
                  options={dictionariesClerks.map((clerk) => {
                    return { value: clerk.personId, label: clerk.personFio };
                  })}
                  disabled={!isEditMode}
                />
              </Grid>
            </Grid>
            <UpdateRubricListIds />
            <AddToRegistryPopup
              mode={popupMode}
              setMode={setPopupMode}
              documentIds={Number(id)}
            />
          </Form>
        );
      }}
    </Formik>
  );
};

export default PredprDocumentForm;

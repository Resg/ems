import React from 'react';

import BoundsList from './BoundsList';

export const Bonds: React.FC<{ docId: number }> = ({ docId }) => {
  return <BoundsList docId={docId} />;
};

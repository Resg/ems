import { useCallback, useEffect, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import { matchPath, useLocation, useSearchParams } from 'react-router-dom';

import AddIcon from '@mui/icons-material/Add';
import AddLinkIcon from '@mui/icons-material/AddLink';
import RightArrIcon from '@mui/icons-material/ChevronRight';
import DelIcon from '@mui/icons-material/DeleteSweep';
import { Box } from '@mui/material';

import { TreeDataGrid } from '../../../../../components/CustomDataGrid';
import { CreateClickableGroupColumn } from '../../../../../components/CustomDataGrid/TreeDataGrid/Columns/GroupColumn';
import { CreateLinkColumn } from '../../../../../components/CustomDataGrid/TreeDataGrid/Columns/SelectColumn';
import { TreeRow } from '../../../../../components/CustomDataGrid/types';
import { createTreeByRows } from '../../../../../components/CustomDataGrid/utils';
import {
  DeleteToolButton,
  ToolButton,
  ToolsPanel,
} from '../../../../../components/ToolsPanel';
import { Routes } from '../../../../../constants/routes';
import {
  useDeleteDocumentLinksMutation,
  useGetBoundedDocsQuery,
  useGetDocumentFilesRequestMutation,
  useGetIncludedDocumentFilesRequestMutation,
} from '../../../../../services/api/document';
import { RootState } from '../../../../../store';
import {
  DocumentFileType,
  DocumentIncludedFileType,
} from '../../../../../types/documents';
import { boundsToRowAdapter } from '../../../../../utils/documents';

import BoundsModal from './BoundsModal';

import styles from './styles.module.scss';

export type BoundsFiles = {
  name: string;
  id: number;
  docId: number;
  files?: DocumentFileType[];
  includedFiles?: DocumentIncludedFileType[];
};

const BoundsList = ({ docId }: { docId: number }) => {
  const [selectRoot, setSelectRoot] = useState(false);
  const [tableFilters, setTableFilters] = useState(false);
  const [searchParams] = useSearchParams();

  const location = useLocation();
  const id = Number(
    matchPath(Routes.DOCUMENTS.DOCUMENT_TABS, location.pathname)?.params?.id ||
      ''
  );

  const { data, status, refetch, isLoading } = useGetBoundedDocsQuery(
    { id: id || '' },
    { skip: !id }
  );
  const refethTable = () => refetch();
  const bounds = useMemo(() => data?.data || [], [status]);
  const [getDocumentFiles] = useGetDocumentFilesRequestMutation();
  const [deleteBounds] = useDeleteDocumentLinksMutation();
  const [getIncludedDocumentFiles] =
    useGetIncludedDocumentFilesRequestMutation();
  const hasBounds = bounds.length > 0;
  const [boundsData, setBoundsData] = useState<BoundsFiles[]>([]);
  const { cols, rows } = useMemo(
    () => boundsToRowAdapter(boundsData),
    [boundsData]
  );
  const rootIds: number[] = useMemo(
    () => rows.filter((r: TreeRow) => !r.parentId).map((i: TreeRow) => i.id),
    [rows]
  );
  const [modal, setModal] = useState(false);
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const selectedArr = useMemo(
    () => Object.keys(selected).filter((key) => selected[Number(key)]),
    [selected]
  );

  const getFiles = useCallback(async () => {
    return Promise.all(
      bounds.map(async (bound) => {
        const boundId =
          bound.fromDocumentId === docId
            ? bound.toDocumentId
            : bound.fromDocumentId;
        const boundName =
          bound.fromDocumentId === docId
            ? bound.toDocumentInfo
            : bound.fromDocumentInfo;
        const row: BoundsFiles = {
          name: boundName,
          id: bound.id,
          docId: boundId,
        };

        await getDocumentFiles({ id: boundId }).then((response) => {
          if ('data' in response) {
            row.files = response.data.data;
          }
        });

        await getIncludedDocumentFiles({ id: boundId }).then((response) => {
          if ('data' in response) {
            row.includedFiles = response.data.data;
          }
        });

        return row;
      })
    );
  }, [bounds, docId, getDocumentFiles, getIncludedDocumentFiles]);

  const handleDelete = useCallback(async () => {
    const boundsToDelete = selectedArr.filter((s) =>
      rootIds.includes(Number(s))
    );
    await deleteBounds({ ids: boundsToDelete });
    refetch();
  }, [selectedArr, rootIds]);

  useEffect(() => {
    getFiles().then((res) => {
      setBoundsData(res);
    });
  }, [docId, bounds, getFiles]);

  useEffect(() => {
    setSelectRoot(
      Boolean(selectedArr.find((s) => rootIds.includes(Number(s))))
    );
  }, [selectedArr]);
  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );
  const handleRowDoubleClick = useCallback(
    (row: any) => {
      if (row.docId) {
        window
          .open(
            Routes.DOCUMENTS.DOCUMENT.replace(':id', row.docId),
            openFormInNewTab ? '_blank' : '_self'
          )
          ?.focus();
      }
    },
    [openFormInNewTab]
  );

  return (
    <div className={styles.content}>
      {hasBounds && rows.length > 0 ? (
        <div className={styles.tableWrap}>
          <TreeDataGrid
            showFilters={tableFilters}
            formKey={'bounds_list'}
            groups={['name']}
            createTree={(r) => createTreeByRows(r, true)}
            cols={cols}
            className={styles.table}
            height={'fullHeight'}
            initialSelected={selected}
            rows={rows}
            onRowDoubleClick={handleRowDoubleClick}
            createColumn={(handleRowExpand: (rowId: string) => void) =>
              CreateClickableGroupColumn(['name'], handleRowExpand, 2)
            }
            onRowSelect={setSelected}
            onCreateSelectColumn={CreateLinkColumn}
            childrenNoSelect
            loading={isLoading}
          >
            <ToolsPanel setFilters={setTableFilters}>
              <ToolButton
                label="Подробно"
                endIcon={<RightArrIcon />}
                disabled={selectedArr.length !== 1}
                fast
              />
              <ToolButton
                label="Добавить документ"
                startIcon={<AddIcon />}
                onClick={() => setModal(true)}
                fast
              />
              <ToolButton
                label="Добавить связь с резолюцией"
                startIcon={<AddLinkIcon />}
                disabled
                fast
              />
              <DeleteToolButton
                label="Удалить"
                onConfirm={handleDelete}
                disabled={!selectRoot}
                startIcon={<DelIcon />}
                description={`Вы действительно хотите удалить связки?`}
                title={'Удаление связок с документами'}
                fast
              />
            </ToolsPanel>
          </TreeDataGrid>
        </div>
      ) : (
        <Box mb={3}>
          <ToolsPanel>
            <ToolButton
              label="Подробно"
              endIcon={<RightArrIcon />}
              disabled={selectedArr.length !== 1}
              fast
            />
            <ToolButton
              label="Добавить документ"
              startIcon={<AddIcon />}
              onClick={() => setModal(true)}
              fast
            />
            <ToolButton
              label="Добавить связь с резолюцией"
              startIcon={<AddLinkIcon />}
              disabled
              fast
            />
            <DeleteToolButton
              label="Удалить"
              onConfirm={handleDelete}
              disabled={!selectRoot}
              startIcon={<DelIcon />}
              description={`Вы действительно хотите удалить связки?`}
              title={'Удаление связок с документами'}
              fast
            />
          </ToolsPanel>
        </Box>
      )}
      <BoundsModal
        open={modal}
        docId={docId}
        onClose={() => setModal(false)}
        refethTable={refethTable}
      />
    </div>
  );
};

export default BoundsList;

import { useCallback, useEffect, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';

import { Check, ChevronRight, Close } from '@mui/icons-material/';
import { Button, Stack } from '@mui/material';

import { DataTable } from '../../../../../components/CustomDataGrid/DataTable';
import { FilterBar } from '../../../../../components/FilterBarNew';
import { Popup } from '../../../../../components/Popup';
import { useSetDocumentLinkMutation } from '../../../../../services/api/document';
import { useGetDocumentsQuery } from '../../../../../services/api/documentSearch';
import { RootState } from '../../../../../store';
import { DocumentSearchType } from '../../../../../types/documentSearch';

import styles from './styles.module.scss';

const modCols = [
  {
    key: 'number',
    name: 'Номер',
  },
  {
    key: 'date',
    name: 'Дата',
  },
  {
    key: 'localNumber',
    name: 'Внутренний номер',
  },
  {
    key: 'type',
    name: 'Тип',
  },
  {
    key: 'rubricList',
    name: 'Рубрики',
  },
  {
    key: 'performer',
    name: 'Исполнитель',
  },
  {
    key: 'contractorList',
    name: 'Корреспонденты',
  },
];

export interface BoundsModalProps {
  open: boolean;
  docId: number;
  onClose: () => void;
  refethTable: () => void;
  includeDoc?: (el: number[]) => void;
}

interface Row {
  id: number;
  number: string;
  date: string;
  localNumber: string;
  type: string;
  rubricList: string;
  performer: string;
  contractorList: string;
}

const BoundsModal = ({
  open = false,
  onClose,
  docId,
  refethTable,
  includeDoc,
}: BoundsModalProps) => {
  const filters = useSelector(
    (state: RootState) => state.filters.data['document-search-for-bounds'] || {}
  );
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const { elasticSearch }: { elasticSearch?: string } = filters;

  const { data, refetch, isLoading } = useGetDocumentsQuery(
    {
      searchParameters: {
        elasticSearch: elasticSearch,
      },
      page,
      size: perPage,
      useCount: true,
    },
    {
      skip: !elasticSearch,
    }
  );

  const [rows, setRows] = useState<Row[]>([]);
  const [selectedRows, setSelectedRows] = useState<Record<number, boolean>>({});

  const [setDocumentLink] = useSetDocumentLinkMutation();
  useEffect(() => {
    const docs = data?.data?.map((row: DocumentSearchType) => {
      return {
        id: row.id,
        number: row.number,
        date: row.date,
        localNumber: row.localNumber,
        type: row.type,
        rubricList: row.rubricList,
        performer: row.performer,
        contractorList: row.contractorList,
      };
    });

    setRows(docs || []);
  }, [data?.data]);

  const addDoc = async () => {
    const selectIds = Object.entries(selectedRows)
      .filter((arr) => arr[1])
      .map((arr) => arr[0]);

    const linkData = {
      fromDocumentId: docId,
      toDocumentIds: selectIds,
    };

    if (selectIds.length > 0) {
      try {
        await setDocumentLink({ linkData });
        refethTable();
        onClose();
      } catch (error) {
        // dispatch(addAppError({ id: Symbol('id'), error: error as Error }));
      }
    }
  };

  const selectedIds = useMemo(() => {
    return Object.keys(selectedRows)
      .map((el) => Number(el))
      .filter((el) => selectedRows[el]);
  }, [selectedRows]);
  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );
  const openPage = useCallback(
    (path: string = '') => {
      window
        .open(`/documents${path}`, openFormInNewTab ? '_blank' : '_self')
        ?.focus();
    },
    [selectedRows, openFormInNewTab]
  );

  const bar = (
    <Stack
      direction="row"
      justifyContent="flex-end"
      spacing={1.5}
      style={{ width: '100%' }}
    >
      <Button
        variant="contained"
        startIcon={<Check />}
        onClick={() => (includeDoc ? includeDoc(selectedIds) : addDoc())}
        disabled={!selectedIds.length}
      >
        Выбрать
      </Button>

      <Button
        variant="contained"
        startIcon={<ChevronRight />}
        onClick={() => openPage(`?id=${selectedIds[0]}`)}
        disabled={selectedIds.length !== 1}
      >
        Подробно
      </Button>
      <Button variant="outlined" startIcon={<Close />} onClick={onClose}>
        Отмена
      </Button>
    </Stack>
  );

  return (
    <Popup
      open={Boolean(open)}
      onClose={onClose}
      title={'Создание связей'}
      height={820}
      bar={bar}
      centred
    >
      <div className={styles.popupContent}>
        <FilterBar
          formKey={'document-search-for-bounds'}
          placeholder="Штрих-код/Номер заявки"
          refetchSearch={() => refetch()}
        />
        <div className={styles.searchResultText}>РЕЗУЛЬТАТ ПОИСКА</div>
        <DataTable
          formKey={'document-search-for-bounds'}
          cols={modCols}
          rows={rows}
          onRowSelect={setSelectedRows}
          className={styles.table}
          loading={isLoading}
          height="perPage"
          pagerProps={{
            page: page,
            setPage: setPage,
            perPage: perPage,
            setPerPage: setPerPage,
            total: data?.totalCount || 0,
          }}
        />
      </div>
    </Popup>
  );
};

export default BoundsModal;

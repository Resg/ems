import { useCallback, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import { matchPath, useLocation, useSearchParams } from 'react-router-dom';

import { Add, ChevronRight, DeleteSweep, Refresh } from '@mui/icons-material';

import { DataTable } from '../../../../components/CustomDataGrid';
import { ExcelIcon } from '../../../../components/Icons';
import { RequestSearchModal } from '../../../../components/RequestSearchModal';
import { RoundButton } from '../../../../components/RoundButton';
import { ToolButton, ToolsPanel } from '../../../../components/ToolsPanel';
import { Routes } from '../../../../constants/routes';
import {
  useDeleteDocumentRequestMutation,
  useGetDocumentRequestsQuery,
} from '../../../../services/api/document';
import { RootState } from '../../../../store';
import { requestsToRowAdapter } from '../../../../utils/documents';

import styles from './styles.module.scss';

export const Requests = () => {
  const [deleteDocumentRequest] = useDeleteDocumentRequestMutation();
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [searchParams] = useSearchParams();
  const location = useLocation();
  const id = Number(
    matchPath(Routes.DOCUMENTS.DOCUMENT_TABS, location.pathname)?.params?.id ||
      ''
  );
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const selectedArr = useMemo(
    () => Object.keys(selected).filter((key) => selected[Number(key)]),
    [selected]
  );
  const [openModal, setOpenModal] = useState(false);
  const {
    data: requests,
    isLoading,
    refetch,
  } = useGetDocumentRequestsQuery(
    {
      id: id || '',
      page,
      size: perPage,
      useCount: true,
    },
    { skip: !id }
  );
  const { cols, rows } = useMemo(
    () => requestsToRowAdapter(requests?.data),
    [requests]
  );
  const selectedRow = useMemo(
    () => rows.find((i) => i.id === Number(selectedArr[0])),
    [selectedArr]
  );

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const rowDoubleHandler = useCallback(
    (e: any) => {
      window
        .open(
          Routes.REQUESTS.REQUEST.replace(':id', e.id),
          openFormInNewTab ? '_blank' : '_self'
        )
        ?.focus();
    },
    [openFormInNewTab]
  );

  const openRequest = useCallback(() => {
    if (selectedRow) {
      window
        .open(
          Routes.REQUESTS.REQUEST.replace(':id', selectedRow.id),
          openFormInNewTab ? '_blank' : '_self'
        )
        ?.focus();
    }
  }, [selectedArr, openFormInNewTab]);

  const handleRowDoubleClick = useCallback(
    (row: typeof rows[number]) => {
      window
        .open(
          Routes.REQUESTS.REQUEST.replace(':id', row.id),
          openFormInNewTab ? '_blank' : '_self'
        )
        ?.focus();
    },
    [openFormInNewTab]
  );

  const handleDelete = useCallback(() => {
    deleteDocumentRequest({ id, requestId: selectedArr[0] });
  }, [selectedArr, id]);

  return (
    <div className={styles.content}>
      <DataTable
        formKey={'bookmark_requests'}
        cols={cols}
        rows={rows}
        onRowDoubleClick={rowDoubleHandler}
        onRowSelect={setSelected}
        height={'fullHeight'}
        loading={isLoading}
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: requests?.totalCount || 0,
        }}
      >
        <ToolsPanel
          leftActions={
            <>
              <RoundButton icon={<Refresh />} onClick={refetch} />
              <RoundButton icon={<ExcelIcon />} />
            </>
          }
          className={styles.toolsPanel}
        >
          <ToolButton
            label="Подробно"
            startIcon={<ChevronRight />}
            disabled={selectedArr.length !== 1}
            onClick={openRequest}
            fast
          />
          <ToolButton
            label="Добавить"
            startIcon={<Add />}
            onClick={() => setOpenModal(true)}
            fast
          />
          <ToolButton
            label="Удалить"
            startIcon={<DeleteSweep />}
            disabled
            fast
          />
        </ToolsPanel>
      </DataTable>
      <RequestSearchModal
        id={Number(id)}
        open={openModal}
        onClose={() => setOpenModal(false)}
        refetchTable={refetch}
      />
      <RequestSearchModal
        id={Number(id)}
        open={openModal}
        onClose={() => setOpenModal(false)}
        refetchTable={refetch}
      />
    </div>
  );
};

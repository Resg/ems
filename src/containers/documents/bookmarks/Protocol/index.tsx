import React, { useMemo, useState } from 'react';

import { Refresh } from '@mui/icons-material';

import { DataTable } from '../../../../components/CustomDataGrid';
import { RoundButton } from '../../../../components/RoundButton';
import { SettingsTypes, ToolsPanel } from '../../../../components/ToolsPanel';
import { protocolCols } from '../../../../constants/documents';
import { useGetDocumentProtocolQuery } from '../../../../services/api/document';
import { protocolToRowsAdapter } from '../../../../utils/documents';

import styles from './styles.module.scss';

export interface ProtocolProps {
  docId: number;
}

export const Protocol: React.FC<ProtocolProps> = ({ docId }) => {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [cols, setCols] = useState(protocolCols);
  const { data, refetch, isLoading } = useGetDocumentProtocolQuery(
    { id: docId, page: page, size: perPage, useCount: true },
    { skip: !docId }
  );
  const rows = useMemo(() => protocolToRowsAdapter(data?.data), [data]);

  return (
    <div className={styles.content}>
      <DataTable
        formKey={'bookmarks_protocol'}
        cols={cols}
        rows={rows}
        loading={isLoading}
        className={styles.table}
        height="fullHeight"
        noSelect
        enableVirtualization={false}
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: data?.totalCount || 0,
        }}
      >
        <ToolsPanel
          settings={[SettingsTypes.COLS]}
          leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
        />
      </DataTable>
    </div>
  );
};

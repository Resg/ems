import React, { useEffect, useRef, useState } from 'react';

import { Close, ExpandLess, ExpandMore } from '@mui/icons-material';
import { Button, Stack } from '@mui/material';

import styles from './styles.module.scss';

export interface CollapseStageProps {
  children: React.ReactNode;
  onChange?: (opened: boolean) => void;
  onDelete?: () => void;
  open?: boolean;
  disableDelete?: boolean;
}

export const CollapseStage: React.FC<CollapseStageProps> = ({
  children,
  onChange,
  onDelete,
  disableDelete,
  open = true,
}) => {
  const [height, setHeight] = useState(0);
  const [opened, setOpened] = useState(open);
  const ref = useRef<HTMLDivElement | null>(null);
  const btnText = opened ? 'Cвернуть' : 'Подробнее';

  useEffect(() => {
    setOpened(open);
  }, [open]);

  useEffect(() => {
    setHeight((ref.current && ref.current.clientHeight) || 0);
  });

  const handleChange = () => {
    setOpened(!opened);

    onChange && onChange(opened);
  };

  return (
    <div>
      <div className={styles.container} style={{ height: opened ? height : 0 }}>
        <div ref={ref}>{children}</div>
      </div>
      <div className={styles.line} />
      <Stack direction="row" justifyContent="flex-end" spacing={1.5}>
        <Button
          startIcon={opened ? <ExpandLess /> : <ExpandMore />}
          onClick={handleChange}
        >
          {btnText}
        </Button>
        <Button
          startIcon={<Close />}
          onClick={onDelete}
          disabled={disableDelete}
        >
          Удалить этап
        </Button>
      </Stack>
    </div>
  );
};

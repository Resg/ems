import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { format } from 'date-fns';
import { FieldArray, Form, Formik } from 'formik';
import { useSnackbar } from 'notistack';

import { AddBox, Close, Save, Send } from '@mui/icons-material';
import { Button, Stack } from '@mui/material';

import { Popup } from '../../../../components/Popup';
import {
  useGetRoutesTypesQuery,
  useGetStagesTypesQuery,
} from '../../../../services/api/dictionaries';
import {
  useGetDocumentByIdQuery,
  useGetDocumentsListForTaskQuery,
} from '../../../../services/api/document';
import { useCreateTaskRouteMutation } from '../../../../services/api/task';
import { useGetUserQuery } from '../../../../services/api/user';
import { RoutesType } from '../../../../types/dictionaries';

import { Stage } from './Stage';
import type { StageData, StageFormType, TaskType } from './types';
import { validationSchema } from './validation';

import styles from './styles.module.scss';

export interface TasksModalProps {
  open: boolean;
  mainId: number | string;
  onClose?: () => void;
  editable?: boolean;
  stagesEditData: {
    parentTypeId: number;
    parentId: number;
  } | null;
}

export const TasksModal = ({
  open,
  onClose,
  mainId,
  editable,
  stagesEditData,
}: TasksModalProps) => {
  const { data: mainDocument, isLoading } = useGetDocumentByIdQuery(
    { id: mainId },
    { skip: !mainId }
  );
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [openPopup, setOpenPopup] = useState(false);
  const { data } = useGetRoutesTypesQuery(
    { typeCode: mainDocument?.objectName || '' },
    { skip: !mainDocument }
  );
  const { data: docsList } = useGetDocumentsListForTaskQuery(
    { mainId },
    { skip: !mainId }
  );

  const [addStagesCount, setAddStagesCount] = useState<number | null>(null);
  const [openedStage, setOpenedStage] = useState(1);
  const [stageInfo, setStageInfo] = useState<{ [key: string]: StageData }>({});
  const [createTaskRoute] = useCreateTaskRouteMutation();
  const ref = useRef<HTMLDivElement | null>(null);
  const { data: addStagesData, refetch } = useGetStagesTypesQuery(
    stagesEditData,
    {
      skip: !editable || !stagesEditData,
    }
  );

  const dataStagesCount = useMemo(
    () =>
      addStagesData?.filter((i: any) => i.recordTypeCode === 'STAGE').length ||
      [],
    [addStagesData]
  );

  const taskTypes = useMemo(() => {
    const taskTypesData = addStagesData ? addStagesData : data;

    return taskTypesData?.reduce(
      (acc: { id: number; name: string }[], cur: RoutesType) => {
        if (cur.recordTypeCode === 'TASK') {
          acc.push({ id: cur.id, name: cur.name });
        }
        return acc;
      },
      []
    );
  }, [data, addStagesData]);
  const stagesObj = useMemo(() => {
    const stageData = editable ? addStagesData : data;

    return stageData?.find((i: any) => i.recordTypeCode === 'STAGE');
  }, [editable, addStagesData, data]);

  const routeObj = useMemo(() => {
    return data?.find((i: any) => i.recordTypeCode === 'ROUTE');
  }, [data]);

  const { data: userInfo } = useGetUserQuery({});
  const taskRows = useMemo(
    () =>
      docsList?.data.map((doc) => {
        return {
          id: doc.id,
          barCode: doc.id,
          typeCode: doc.typeCode,
          type: doc.type,
          date: doc.date
            ? String(format(new Date(doc.date.split('T')[0]), 'dd.MM.yyyy'))
            : '',
          name: doc.title,
          number: doc.number,
          isMain: doc.isMain,
        };
      }) || null,
    [docsList]
  );

  const typeId = editable ? stagesObj?.id : routeObj?.id;
  const objectTypeCode = stagesObj?.objectTypeCode;
  const temporaryОbjectId = useMemo(() => {
    const check = /^TASK_/.test(stagesObj?.objectTypeCode);

    return check ? routeObj?.id : mainId;
  }, [stagesObj]);

  const canOpen = useMemo(
    () =>
      (editable && dataStagesCount === 1 && taskTypes?.length) ||
      (!editable && taskTypes?.length),
    [editable, taskTypes, dataStagesCount]
  );

  useEffect(() => {
    refetch();
  }, [open]);

  useEffect(() => {
    if (editable) {
      const count = addStagesData?.find(
        (i: any) => i.recordTypeCode === 'STAGE'
      )?.existedRecordsByTypeCount;
      setAddStagesCount(count);
    } else {
      setAddStagesCount(null);
    }
  });

  useEffect(() => {
    let text;

    if (editable && dataStagesCount > 1) {
      text =
        'Найдено более одного кандидата для создания этапа. Обратитесь к разработчикам.';
    }

    if (!taskTypes?.length) {
      text =
        'Не найдено ни одного кандидата для создания задачи. Обратитесь к разработчикам.';
    }

    if (!canOpen && open && !isLoading) {
      const key = enqueueSnackbar(text, {
        autoHideDuration: 5000,
        onClick: () => closeSnackbar(key),
        variant: 'info',
        anchorOrigin: { horizontal: 'right', vertical: 'top' },
      });

      onClose && onClose();
    }

    if (open && canOpen) {
      setOpenPopup(true);
    }
  }, [canOpen, editable, open]);

  const getObjectId = useCallback(() => {
    const tc = stagesObj?.objectTypeCode;
    const check = /^TASK_/.test(tc);

    return check ? stagesEditData?.parentId : mainId;
  }, [stagesObj, mainId]);

  const routeData = {
    temporaryId: 1,
    typeId: typeId,
    objectTypeCode: routeObj?.objectTypeCode,
    objectId: mainId,
    performerId: userInfo?.clerkId,
    authorId: userInfo?.clerkId || 0,
    isControl: false,
    sendForExecution: true,
  };

  const updateStageInfo = useCallback(
    (num: number, data: any) => {
      const newStageInfo = Object.assign({}, stageInfo);
      newStageInfo[num] = data;

      setStageInfo(newStageInfo);
    },
    [stageInfo]
  );

  const handleClose = () => {
    setOpenedStage(1);
    setStageInfo({});
    refetch();
    setOpenPopup(false);

    onClose && onClose();
  };

  const handleSubmit = useCallback(
    (action: 'save' | 'send') => async () => {
      let counter = editable ? 0 : 1;
      const saveData = [];
      const stagesKeys = Object.keys(stageInfo);

      if (!editable) {
        saveData.push(routeData);
      }

      stagesKeys.forEach((key) => {
        counter++;
        const stage = stageInfo[key];

        if (editable) {
          stage.objectId = getObjectId();
          stage.parentId = stagesEditData?.parentId;
        }

        if (!editable) {
          stage.temporaryParentId = 1;
          stage.temporaryОbjectId = temporaryОbjectId;
        }

        stage.temporaryId = counter;
        saveData.push(stage);

        stage.tasks?.forEach((task: TaskType) => {
          counter++;
          task.temporaryId = counter;
          task.temporaryParentId = stage.temporaryId;

          saveData.push(task);
        });

        delete stage.tasks;
      });

      if (action === 'save') {
        saveData[0].sendForExecution = false;
      }

      if (editable && action === 'send') {
        saveData[0].sendForExecution = true;
      }

      await createTaskRoute({ taskData: saveData });
      handleClose();
    },
    [stageInfo, routeData]
  );

  const bar = (
    <Stack
      direction="row"
      justifyContent="flex-end"
      spacing={1.5}
      style={{ width: '100%' }}
    >
      <Button
        variant="contained"
        startIcon={<Send />}
        onClick={handleSubmit('send')}
      >
        Отправить на исполнение
      </Button>
      <Button
        variant="outlined"
        startIcon={<Save />}
        onClick={handleSubmit('save')}
      >
        Сохранить
      </Button>
      <Button variant="outlined" startIcon={<Close />} onClick={handleClose}>
        Отмена
      </Button>
    </Stack>
  );

  const emptyStage = {
    name: null,
    clerks: [],
    term: '',
    planDate: null,
    comments: '',
    isControl: false,
  };

  const stageData = useMemo(() => {
    return {
      name: stagesObj?.name,
      addStagesMode: editable,
      typeId: stagesObj?.id,
      taskTypes: taskTypes || [],
      authorId: userInfo?.clerkId || 0,
      docRows: taskRows,
      objectTypeCode,
    };
  }, [
    editable,
    objectTypeCode,
    stagesObj?.id,
    stagesObj?.name,
    taskRows,
    taskTypes,
    userInfo?.clerkId,
  ]);

  return (
    <>
      <Popup
        open={openPopup}
        onClose={handleClose}
        title={editable ? 'Добавление этапов в маршрут' : 'Создание маршрута'}
        bar={bar}
        height={732}
        centred
      >
        <div ref={ref} className={styles.content}>
          <>
            <Formik
              initialValues={{ stages: [{ ...emptyStage }] }}
              onSubmit={() => {}}
              enableReinitialize
              validationSchema={validationSchema}
            >
              {({ values }) => {
                return (
                  <Form autoComplete="off">
                    <FieldArray
                      name="stages"
                      render={(arrayHelpers) => {
                        return (
                          <>
                            {values.stages.map(
                              (stage: StageFormType, index: number) => {
                                return (
                                  <Stage
                                    key={index}
                                    data={stageData}
                                    index={index}
                                    onChange={updateStageInfo}
                                    onDelete={() => {
                                      arrayHelpers.remove(index);
                                      setOpenedStage(openedStage - 1);
                                    }}
                                    open={index + 1 === openedStage}
                                    disableDelete={values.stages.length < 2}
                                    count={
                                      addStagesCount
                                        ? addStagesCount + index + 1
                                        : index + 1
                                    }
                                  />
                                );
                              }
                            )}
                            <Stack direction="row" justifyContent="flex-end">
                              <Button
                                startIcon={<AddBox />}
                                onClick={() => {
                                  arrayHelpers.push(emptyStage);
                                  setOpenedStage(openedStage + 1);
                                  setTimeout(
                                    () =>
                                      ref.current?.scrollTo(
                                        0,
                                        ref.current.scrollHeight
                                      ),
                                    100
                                  );
                                }}
                              >
                                Добавить этап
                              </Button>
                            </Stack>
                          </>
                        );
                      }}
                    />
                  </Form>
                );
              }}
            </Formik>
          </>
        </div>
      </Popup>
    </>
  );
};

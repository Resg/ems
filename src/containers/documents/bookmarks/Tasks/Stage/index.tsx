import React, {
  ChangeEvent,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';
import { FormikValues, useFormikContext } from 'formik';

import { FormControlLabel, Grid } from '@mui/material';

import { Checkbox } from '../../../../../components/Checkbox';
import { ClerkInput } from '../../../../../components/ClerkInput';
import { ComboBox } from '../../../../../components/ComboBox';
import { DataTable } from '../../../../../components/CustomDataGrid';
import { DatePicker } from '../../../../../components/DatePicker';
import Input from '../../../../../components/Input';
import {
  TasksStagesFields,
  TasksStagesLabels,
} from '../../../../../constants/taskStage';
import { useGetClerksQuery } from '../../../../../services/api/dictionaries';
import { CollapseStage } from '../CollapseStage';
import { docCols } from '../tasksModalData';
import type { StageData, StageIncomingData, TaskType } from '../types';

import styles from '../styles.module.scss';

export interface StageProps {
  data: StageIncomingData;
  index: number;
  onChange: (num: number, data: StageData) => void;
  onDelete?: () => void;
  open?: boolean;
  count?: number;
  disableDelete?: boolean;
}

export const Stage: React.FC<StageProps> = ({
  data,
  index,
  onChange,
  onDelete,
  open,
  count,
  disableDelete,
}) => {
  const { data: clerksList = [] } = useGetClerksQuery({});
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const [tableFilters, setTableFilters] = useState(false);
  const selectedArr = useMemo(
    () => Object.keys(selected).filter((key) => selected[Number(key)]),
    [selected]
  );
  const { errors, setFieldValue, touched, values } =
    useFormikContext<FormikValues>();
  const vals = values['stages'][index];
  const initialSelected = useMemo(() => {
    const mainDocs = data.docRows
      ?.filter((doc) => doc.isMain)
      .map((doc) => doc.id);
    const allDocs = data.docRows?.map((doc) => doc.id);
    const arr: Record<number, boolean> = {};

    if ([2, 3].includes(values.name?.id || 0)) {
      allDocs?.forEach((doc) => (arr[doc] = true));
    } else {
      mainDocs?.forEach((doc) => (arr[doc] = true));
    }

    setSelected(arr);

    return arr;
  }, [values.name]);
  const clerksStr = useMemo(
    () =>
      vals.clerks.reduce((str: string, item: number, i: number) => {
        const name = clerksList.find((i) => i.value === item)?.label;
        return str + (i === 0 ? name : `, ${name}`);
      }, ''),
    [vals.clerks]
  );
  const stageName = `${data.name} ${count}`;
  const stageData: StageData = {
    temporaryId: null,
    typeId: data.typeId,
    title: stageName,
    temporaryParentId: null,
    objectTypeCode: data.objectTypeCode,
    performerId: data.authorId,
    authorId: data.authorId,
    isControl: false,
    sendForExecution: false,
    tasks: [],
  };

  const getStageInfo = useCallback(() => {
    const tasks: TaskType[] = [];

    vals.clerks.forEach((clerk: number) => {
      const docs = selectedArr.length ? selectedArr : [];

      docs.forEach((doc) => {
        const typeCode =
          data.docRows?.find((i) => i.id === Number(doc))?.typeCode || '';

        tasks.push({
          typeId: vals.name?.id || 0,
          temporaryParentId: null,
          comments: vals.comments,
          objectTypeCode: typeCode,
          objectId: Number(doc),
          term: vals.term ? Number(vals.term) : '',
          planDate: vals.planDate,
          performerId: clerk,
          authorId: data.authorId,
          isControl: vals.isControl,
          sendForExecution: false,
        });
      });
    });

    stageData.tasks = tasks;

    return stageData;
  }, [vals, selected]);

  useEffect(() => {
    onChange(index, getStageInfo());
  }, [values, selected]);

  const handleTermChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      if (vals.planDate) {
        setFieldValue(
          `${TasksStagesFields.STAGES}[${index}].${TasksStagesFields.PLANE_DATE}`,
          null
        );
      }

      setFieldValue(
        `${TasksStagesFields.STAGES}[${index}].${TasksStagesFields.TERM}`,
        e.target.value
      );
    },
    [vals]
  );

  const handleDateChange = useCallback(
    (value: string) => {
      if (vals.term) {
        setFieldValue(
          `${TasksStagesFields.STAGES}[${index}].${TasksStagesFields.TERM}`,
          ''
        );
      }

      setFieldValue(
        `${TasksStagesFields.STAGES}[${index}].${TasksStagesFields.PLANE_DATE}`,
        value
      );
    },
    [vals]
  );

  return (
    <>
      <div className={styles.subtitle}>
        {stageName}
        {!open && (vals.name?.name ? `: ${vals.name.name}` : '')}
      </div>
      {!open && clerksStr && (
        <div className={styles.stageInfo}>
          <span>Исполнители:</span> {clerksStr}
        </div>
      )}
      <CollapseStage
        open={open}
        onDelete={onDelete}
        disableDelete={disableDelete}
      >
        <Grid container spacing={2} sx={{ mt: -1.5 }}>
          <Grid item xs={3}>
            <ComboBox
              options={data.taskTypes}
              getOptionLabel={(option) => option.name}
              onChange={(e, value) =>
                setFieldValue(
                  `${TasksStagesFields.STAGES}[${index}].${TasksStagesFields.NAME}`,
                  value
                )
              }
              textFieldProps={{
                label: TasksStagesLabels[TasksStagesFields.NAME],
                name: `${TasksStagesFields.STAGES}[${index}].${TasksStagesFields.NAME}`,
                error: touched[vals.name] && !!errors[vals.name],
                required: true,
                helperText:
                  touched[vals.name] && (errors[vals.name] as React.ReactNode),
              }}
            />
          </Grid>
          <Grid item xs={3}>
            <Input
              label={TasksStagesLabels[TasksStagesFields.TERM]}
              onChange={handleTermChange}
              value={vals.term}
            />
          </Grid>
          <Grid item xs={3}>
            <DatePicker
              onChange={handleDateChange}
              value={vals.planDate}
              textFieldProps={{
                label: TasksStagesLabels[TasksStagesFields.PLANE_DATE],
              }}
            />
          </Grid>
          <Grid item xs={3}>
            <FormControlLabel
              label={TasksStagesLabels[TasksStagesFields.CONTROL]}
              onChange={(e, checked) =>
                setFieldValue(
                  `${TasksStagesFields.STAGES}[${index}].${TasksStagesFields.CONTROL}`,
                  checked
                )
              }
              control={<Checkbox />}
            />
          </Grid>
          <Grid item xs={6}>
            <ClerkInput
              name={`${TasksStagesFields.STAGES}[${index}].${TasksStagesFields.CLERKS}`}
              label={TasksStagesLabels[TasksStagesFields.CLERKS]}
              multiple
              required
              matchFrom="start"
            />
          </Grid>
          <Grid item xs={6}>
            <Input
              multiline
              label={TasksStagesLabels[TasksStagesFields.COMMENTS]}
              onChange={(e) =>
                setFieldValue(
                  `${TasksStagesFields.STAGES}[${index}].${TasksStagesFields.COMMENTS}`,
                  e.target.value
                )
              }
            />
          </Grid>

          <Grid item xs={12} sx={{ pb: 1.5 }}>
            {data.docRows && (
              <>
                <DataTable
                  formKey={'bookmarks_tasks_stage'}
                  rows={data.docRows}
                  cols={docCols}
                  initialSelected={initialSelected}
                  onRowSelect={setSelected}
                  showFilters={tableFilters}
                  height="auto"
                />
              </>
            )}
          </Grid>
        </Grid>
      </CollapseStage>
    </>
  );
};

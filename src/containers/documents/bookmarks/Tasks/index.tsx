import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';

import {
  AddCircleOutline,
  ChevronRight,
  DeleteOutline,
  ExpandMore,
  Refresh,
  Restore,
  Send,
  SignalCellularAlt,
} from '@mui/icons-material';

import { TaskTemplateForm } from '../../../../components/CreateTaskTemplateForm';
import { TreeDataGrid } from '../../../../components/CustomDataGrid';
import { CreateRightGroupColumn } from '../../../../components/CustomDataGrid/TreeDataGrid/Columns/GroupColumn';
import { DefaultRow } from '../../../../components/CustomDataGrid/types';
import { createTreeByRows } from '../../../../components/CustomDataGrid/utils';
import { ExecuteButton } from '../../../../components/Execution';
import {
  ExecutionButtonIcons,
  ExecutionStatusIds,
} from '../../../../components/Execution/ExecutionButton/constants';
import { MenuButtonItem } from '../../../../components/MenuButton/MenuButtonItem';
import { RoundButton } from '../../../../components/RoundButton';
import { SaveToTemplateModal } from '../../../../components/SaveToTemplateModal';
import { DeleteToolButton } from '../../../../components/ToolsPanel';
import {
  SettingsTypes,
  ToolButton,
  ToolsPanel,
} from '../../../../components/ToolsPanel';
import { MenuToolButton } from '../../../../components/ToolsPanel/MenuToolButton';
import { DocumentTasksExecutionButtons } from '../../../../constants/tasks';
import {
  useDeleteTaskMutation,
  useGetObjectTasksQuery,
  useRevokeTaskMutation,
  useSendToExecutionMutation,
  useSendToReexecutionMutation,
} from '../../../../services/api/incomingDocumentTasks';
import { RootState } from '../../../../store';
import { CommonDocumentType } from '../../../../types/documents';
import { tasksToRowAdapter } from '../../../../utils/tasks';

import { AddTasksToStageModal } from './AddTasksToStageModal';
import { TasksModal } from './TasksModal';

import styles from './styles.module.scss';

export interface TasksProps {
  docData: CommonDocumentType;
}
const groups = ['parentId', 'id'];

export const Tasks: React.FC<TasksProps> = ({ docData }) => {
  const { id, objectName } = docData;

  const [stageModal, setStageModal] = useState(false);
  const [editStages, setEditStages] = useState(false);
  const [taskTemplate, setTaskTemplate] = useState(false);
  const [addTaskModal, setAddTaskModal] = useState(false);
  const [tableFilters, setTableFilters] = useState(false);
  const [templateModal, setTemplateModal] = useState<'task' | 'route' | null>(
    null
  );
  const {
    data: tasks,
    refetch,
    isLoading,
  } = useGetObjectTasksQuery({ id: id, type: objectName }, { skip: !id });
  const [revokeTask] = useRevokeTaskMutation();
  const [deleteTask] = useDeleteTaskMutation();
  const [sendToExecution] = useSendToExecutionMutation();
  const [reexecution] = useSendToReexecutionMutation();
  const [selected, setSelected] = useState<Record<number, boolean>>({});

  useEffect(() => {
    setSelected({});
  }, [tasks?.data]);

  const selectedArr = useMemo(
    () =>
      Object.keys(selected)
        .filter((key) => selected[Number(key)])
        .map(Number),
    [selected]
  );

  const selectedRows = useMemo(
    () => tasks?.data.filter((task) => selectedArr.includes(task.id)) || [],
    [tasks?.data, selectedArr]
  );

  const { cols, rows } = useMemo(
    () => tasksToRowAdapter(tasks?.data || []),
    [tasks]
  );
  const singleSelected = selectedArr.length === 1;
  const stagesEditData = useMemo(() => {
    if (!selectedRows.length) {
      return null;
    }

    return {
      parentTypeId: selectedRows[0]?.typeId,
      parentId: selectedRows[0]?.id,
    };
  }, [selectedRows]);

  const firstSelected = useMemo(() => {
    return rows.find((row) => row.id === Number(selectedArr[0]));
  }, [rows, selectedArr]);

  const checkName = firstSelected && /^Подготовка/.test(firstSelected.title);
  const detailsDisabled = !singleSelected || checkName;

  const handleCloseStageModal = useCallback(() => {
    setStageModal(false);
    setEditStages(false);
    refetch();
  }, [refetch]);

  const handleCloseTaskTemplateModal = useCallback(() => {
    setTaskTemplate(false);
    refetch();
  }, [refetch]);

  const handleCloseAddTasksToStageModal = useCallback(() => {
    setAddTaskModal(false);
    refetch();
  }, [refetch]);

  const templateButtonEnabled = useMemo(() => {
    return singleSelected && checkName;
  }, [singleSelected, checkName]);

  const handleRevoke = useCallback(() => {
    revokeTask(selectedArr[0]);
  }, [revokeTask, selectedArr]);

  const handleReexecute = useCallback(() => {
    reexecution(selectedArr[0]);
  }, [reexecution, selectedArr]);

  const revokeDisabled = useMemo(() => {
    return !(singleSelected && selectedRows[0]?.canRevokeFromExecution);
  }, [singleSelected, selectedRows]);

  const sendReexecutionDisabled = useMemo(() => {
    return !(singleSelected && selectedRows[0]?.canReturnToExecution);
  }, [singleSelected, selectedRows]);

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const handleDetails = useCallback(() => {
    if (selectedRows[0].typeId === 8) {
      window
        .open(
          `/detailed/stage/data?taskId=${selectedArr[0]}`,
          openFormInNewTab ? '_blank' : '_self'
        )
        ?.focus();
    } else
      window
        .open(
          `/tasks/details/${selectedArr[0]}`,
          openFormInNewTab ? '_blank' : '_self'
        )
        ?.focus();
  }, [selectedArr, selectedRows, openFormInNewTab]);

  const rowDoubleHandler = (e: any) => {
    const doubleClickRow = tasks?.data.filter((task) => task.id === e.id)[0];

    if (doubleClickRow?.title && /^Подготовка/.test(doubleClickRow.title)) {
      return false;
    }

    if (doubleClickRow?.typeId === 8) {
      window
        .open(
          `/detailed/stage/data?taskId=${e.id}`,
          openFormInNewTab ? '_blank' : '_self'
        )
        ?.focus();
    } else
      window
        .open(`/tasks/details/${e.id}`, openFormInNewTab ? '_blank' : '_self')
        ?.focus();
  };

  const deleteDisabled = useMemo(() => {
    return !(singleSelected && selectedRows[0]?.canDelete);
  }, [singleSelected, selectedRows]);

  const handleDelete = useCallback(() => {
    deleteTask(selectedArr[0]);
  }, [deleteTask, selectedArr]);

  const sendExeDisabled = useMemo(() => {
    return !(singleSelected && selectedRows[0]?.canSendToExecution);
  }, [singleSelected, selectedRows]);

  const handleSendExe = useCallback(() => {
    sendToExecution(selectedArr[0]);
  }, [sendToExecution, selectedArr]);

  const addStages = useCallback(() => {
    setEditStages(true);
    setTimeout(() => setStageModal(true), 200);
  }, []);

  const taskButtonDisabled = useMemo(() => {
    return !(
      singleSelected &&
      selectedRows[0]?.canCreateSubtask &&
      selectedRows[0]?.typeId === 8
    );
  }, [singleSelected, selectedRows]);

  return (
    <div className={styles.pageContent}>
      <TreeDataGrid
        formKey={'bookmarks_tasks'}
        showFilters={tableFilters}
        cols={cols}
        rows={rows}
        groups={groups}
        loading={isLoading}
        className={styles.table}
        onRowSelect={setSelected}
        height={'fullHeight'}
        onRowDoubleClick={rowDoubleHandler}
        expanded
        createTree={(rows: DefaultRow[]) => createTreeByRows(rows, true)}
        createColumn={(handleRowExpand: (rowId: string) => void) =>
          CreateRightGroupColumn(groups, handleRowExpand)
        }
        childrenNoSelect
      >
        <ToolsPanel
          settings={[SettingsTypes.TOOLS]}
          leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
          className={styles.tools}
          setFilters={setTableFilters}
        >
          {[
            <ToolButton
              label="подробно"
              endIcon={<ChevronRight />}
              disabled={!singleSelected}
              onClick={() => handleDetails()}
              fast
            />,
            <MenuToolButton
              label="создать"
              startIcon={<AddCircleOutline />}
              endIcon={<ExpandMore />}
            >
              <MenuButtonItem onClick={() => setStageModal(true)}>
                Маршрут
              </MenuButtonItem>
              <MenuButtonItem
                icon={<SignalCellularAlt />}
                disabled={!templateButtonEnabled}
                onClick={addStages}
              >
                Этапы
              </MenuButtonItem>
              <MenuButtonItem
                icon={<SignalCellularAlt />}
                disabled={taskButtonDisabled}
                onClick={() => setAddTaskModal(true)}
              >
                Задачи
              </MenuButtonItem>
              <MenuButtonItem onClick={() => setTaskTemplate(true)}>
                Маршрут по шаблону
              </MenuButtonItem>
            </MenuToolButton>,
            ...DocumentTasksExecutionButtons.map((action) => {
              const ButtonIcon = ExecutionButtonIcons[action];
              return (
                <ExecuteButton
                  startIcon={<ButtonIcon />}
                  statusId={ExecutionStatusIds[action]}
                  rows={selectedRows}
                  ids={selectedArr}
                  label={action}
                  key={action}
                />
              );
            }),
            <ToolButton
              label={'Отправить на исполнение'}
              startIcon={<Send />}
              onClick={handleSendExe}
              disabled={sendExeDisabled}
            />,
            <DeleteToolButton
              label={'Удалить'}
              startIcon={<DeleteOutline />}
              title={'Удаление задач'}
              description={`Вы действительно хотите удалить задачу?`}
              onConfirm={handleDelete}
              disabled={deleteDisabled}
            />,
            <ToolButton
              label="Отозвать на доработку"
              startIcon={<Restore />}
              onClick={handleRevoke}
              disabled={revokeDisabled}
            />,
            <ToolButton
              label="Сохранить в шаблон"
              startIcon={<AddCircleOutline />}
              onClick={() => setTemplateModal('route')}
              disabled={!templateButtonEnabled}
              fast
            />,
          ]}
        </ToolsPanel>
      </TreeDataGrid>
      <TasksModal
        mainId={id}
        open={stageModal}
        onClose={handleCloseStageModal}
        editable={editStages}
        stagesEditData={stagesEditData}
      />
      <SaveToTemplateModal
        open={templateModal}
        setOpen={setTemplateModal}
        objectTypeCode={objectName}
        selectedId={Number(selectedArr[0])}
        rows={tasks?.data || []}
      />
      <TaskTemplateForm
        objectTypeCode={objectName}
        open={taskTemplate}
        onClose={handleCloseTaskTemplateModal}
        formName={'Создание маршрута по шаблону'}
        id={id}
      />
      {selectedRows[0] && addTaskModal && (
        <AddTasksToStageModal
          docData={docData}
          stage={selectedRows[0]}
          open={addTaskModal}
          onClose={handleCloseAddTasksToStageModal}
        />
      )}
    </div>
  );
};

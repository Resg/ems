import { array, boolean, date, number, object, string } from 'yup';

export const validationSchema = object({
  stages: array().of(
    object({
      name: object({
        id: number(),
        name: string(),
      }).required('Обязательное поле'),
      clerks: array().of(number()).required('Обязательное поле'),
      term: string(),
      planDate: string().nullable(),
      comments: string(),
      isControl: boolean(),
    })
  ),
});

export const taskValidationSchema = object({
  tasks: array().of(
    object({
      name: number().nullable().required('Обязательное поле'),
      clerks: array().of(number()).min(1, 'Обязательное поле'),
      term: string(),
      planDate: date()
        .nullable()
        .min(new Date(), 'Дата не может быть меньше текущей'),
      comments: string(),
      isControl: boolean(),
      docs: array().of(string()).min(1),
    })
  ),
});

import { ClerkForFormType } from '../../../../types/dictionaries';

export interface TaskType {
  temporaryId?: number;
  typeId: number;
  comments: string;
  temporaryParentId: number | null;
  objectTypeCode: string;
  objectId: number;
  term: string | number;
  planDate: string | null;
  performerId: number;
  authorId: number;
  isControl: boolean;
  sendForExecution: boolean;
}

export interface TaskDocRow {
  barCode: number;
  date: string;
  id: number;
  name: string;
  number: string;
  type: string;
  typeCode: string;
  isMain?: boolean;
}

export interface StageFormType {
  name: { id: number; name: string } | null;
  clerks: ClerkForFormType[];
  term: string;
  planDate: string | null;
  comments: string;
  isControl: boolean;
}

export interface StageIncomingData {
  name: string;
  addStagesMode?: boolean;
  typeId: number;
  taskTypes: { id: number; name: string }[];
  authorId: number;
  objectTypeCode: string | null;
  docRows: TaskDocRow[] | null;
}

export interface StageData {
  temporaryId: number | null;
  typeId: number | null;
  title: string;
  temporaryParentId: number | null;
  objectId?: string | number;
  objectTypeCode: string | null;
  temporaryОbjectId?: number | string;
  parentId?: number;
  performerId: number;
  authorId: number;
  isControl: boolean;
  sendForExecution: boolean;
  tasks?: TaskType[];
}

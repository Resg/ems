export const docCols = [
  {
    key: 'barCode',
    name: 'Штрих-код',
  },
  {
    key: 'type',
    name: 'Тип',
  },
  {
    key: 'number',
    name: 'Номер',
  },
  {
    key: 'date',
    name: 'Дата',
  },
  {
    key: 'name',
    name: 'Наименование',
  },
];

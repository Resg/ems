import { useCallback, useEffect, useMemo, useState } from 'react';
import { format } from 'date-fns';
import { FieldArray, Form, Formik } from 'formik';
import { isEmpty } from 'lodash';
import { useSnackbar } from 'notistack';

import { AddBox, Close, Save, Send } from '@mui/icons-material';
import { Button, Grid, Stack } from '@mui/material';

import { AutoCompleteInput } from '../../../../components/AutoCompleteInput';
import { CheckboxInput } from '../../../../components/CheckboxInput';
import { ClerkInput } from '../../../../components/ClerkInput';
import { DataTable } from '../../../../components/CustomDataGrid';
import { DatePickerInput } from '../../../../components/DatePickerInput';
import Input from '../../../../components/Input';
import { Popup } from '../../../../components/Popup';
import { SnackbarCloseButton } from '../../../../components/SnackbarCloseButton';
import {
  TasksStagesFields,
  TasksStagesLabels,
} from '../../../../constants/taskStage';
import { useGetTaskTypesQuery } from '../../../../services/api/dictionaries';
import { useGetDocumentsListForTaskQuery } from '../../../../services/api/document';
import { useCreateTaskRouteMutation } from '../../../../services/api/task';
import { useGetUserQuery } from '../../../../services/api/user';
import { CommonDocumentFormData } from '../../../../types/documents';
import { ObjectTask, TaskRouteData } from '../../../../types/tasks';

import { docCols } from './tasksModalData';
import { taskValidationSchema } from './validation';

import styles from './styles.module.scss';

export interface AddTasksToStageModalProps {
  docData: CommonDocumentFormData;
  open: boolean;
  stage: ObjectTask;
  onClose: () => void;
}

export const AddTasksToStageModal = ({
  docData,
  open,
  stage,
  onClose,
}: AddTasksToStageModalProps) => {
  const [tableFilters, setTableFilters] = useState(false);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const { data: docsList } = useGetDocumentsListForTaskQuery({
    mainId: docData.id,
  });
  const { data: taskTypes } = useGetTaskTypesQuery({
    parentTypeId: stage.typeId,
  });
  const { data: userInfo } = useGetUserQuery({});
  const [createTaskRoute] = useCreateTaskRouteMutation();

  const taskTypesOptions = useMemo(
    () =>
      taskTypes?.data
        .filter((type) => type.recordTypeCode === 'TASK')
        .map((type) => ({
          value: type.id,
          label: type.name,
        })) || [],
    [taskTypes]
  );

  useEffect(() => {
    if (taskTypes?.data && !taskTypes?.data.length) {
      const message =
        'Не найдено ни одного кандидата для создания задачи. Обратитесь к разработчикам.';
      const key = enqueueSnackbar(message, {
        autoHideDuration: 5000,
        onClick: () => {
          closeSnackbar(key);
          onClose();
        },
        variant: 'info',
        anchorOrigin: { horizontal: 'right', vertical: 'top' },
        action: (key) => (
          <SnackbarCloseButton snackbarKey={key} errorData={{ message: '' }} />
        ),
      });
    }
  }, [taskTypes]);

  const taskRows = useMemo(
    () =>
      docsList?.data.map((doc) => {
        return {
          id: doc.id,
          barCode: doc.id,
          typeCode: doc.typeCode,
          type: doc.type,
          date: doc.date
            ? String(format(new Date(doc.date.split('T')[0]), 'dd.MM.yyyy'))
            : '',
          name: doc.title,
          number: doc.number,
          isMain: doc.isMain,
        };
      }) || [],
    [docsList]
  );

  const emptyTask = {
    name: null,
    clerks: [],
    term: '',
    planDate: null,
    comments: '',
    isControl: false,
    docs: docsList?.data.filter((doc) => doc.isMain).map((doc) => doc.id) || [],
  };

  const initialValues = useMemo(() => {
    return {
      tasks: [emptyTask],
    };
  }, []);

  const handleSubmit = useCallback(
    async (values: Record<string, any>, sendForExecution: boolean) => {
      const data = [] as TaskRouteData[];
      let counter = 1;

      values.tasks.forEach((task: typeof emptyTask) => {
        task.clerks.forEach((clerk) => {
          task.docs.forEach((doc) => {
            data.push({
              temporaryId: counter,
              typeId: task.name || 0,
              comments: task.comments,
              parentId: stage.id,
              objectTypeCode: docsList?.data.find(
                (document) => document.id === doc
              )?.typeCode,
              objectId: doc,
              term: Number(task.term),
              planDate: task.planDate,
              performerId: clerk,
              authorId: userInfo?.clerkId || 0,
              isControl: task.isControl,
              sendForExecution,
            });

            counter++;
          });
        });
      });

      const response = await createTaskRoute({ taskData: data });
      if ('data' in response) {
        onClose();
      }
    },
    [stage, userInfo, docsList, createTaskRoute, onClose]
  );

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={() => {}}
      enableReinitialize
      validationSchema={taskValidationSchema}
      validateOnMount
    >
      {({ values, errors, handleChange, setFieldValue }) => {
        const handleDateChange = (value: Date | null, index: number) => {
          if (value) {
            setFieldValue(`tasks[${index}].${TasksStagesFields.TERM}`, '');
            setFieldValue(
              `tasks[${index}].${TasksStagesFields.PLANE_DATE}`,
              format(value, 'yyyy-MM-dd')
            );
          }
        };

        const handleTermChange = (
          e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
          index: number
        ) => {
          const term = Number(e.target.value);
          setFieldValue(
            `tasks[${index}].${TasksStagesFields.TERM}`,
            term > 0 ? term : ''
          );
          setFieldValue(
            `tasks[${index}].${TasksStagesFields.PLANE_DATE}`,
            null
          );
        };

        return (
          <Popup
            open={open}
            onClose={onClose}
            title={'Добавление задачи в этап'}
            bar={
              <Stack
                direction="row"
                justifyContent="flex-end"
                spacing={1.5}
                style={{ width: '100%' }}
              >
                <Button
                  variant="contained"
                  startIcon={<Send />}
                  onClick={() => handleSubmit(values, true)}
                  disabled={!isEmpty(errors)}
                >
                  Отправить на исполнение
                </Button>
                <Button
                  variant="outlined"
                  startIcon={<Save />}
                  onClick={() => handleSubmit(values, false)}
                  disabled={!isEmpty(errors)}
                >
                  Сохранить
                </Button>
                <Button
                  variant="outlined"
                  startIcon={<Close />}
                  onClick={onClose}
                >
                  Отмена
                </Button>
              </Stack>
            }
            height={732}
          >
            <Form autoComplete="off">
              <div className={styles.subtitle}>{stage.title}</div>
              <FieldArray
                name="tasks"
                render={({ push }) => {
                  return (
                    <>
                      {values.tasks.map((_, index: number) => {
                        const name = values.tasks[index].name as number | null;

                        const initialSelected = values.tasks[index].docs.reduce(
                          (acc, cur) => {
                            acc[cur] = true;
                            return acc;
                          },
                          {} as Record<number, boolean>
                        );

                        return (
                          <Grid container spacing={2} sx={{ p: 2 }} key={index}>
                            <Grid item xs={3}>
                              <AutoCompleteInput
                                name={`tasks[${index}].${TasksStagesFields.NAME}`}
                                label={
                                  TasksStagesLabels[TasksStagesFields.NAME]
                                }
                                options={taskTypesOptions}
                                required
                              />
                            </Grid>
                            <Grid item xs={3}>
                              <Input
                                name={`tasks[${index}].${TasksStagesFields.TERM}`}
                                label={
                                  TasksStagesLabels[TasksStagesFields.TERM]
                                }
                                value={values.tasks[index].term || ''}
                                onChange={(event) =>
                                  handleTermChange(event, index)
                                }
                              />
                            </Grid>
                            <Grid item xs={3}>
                              <DatePickerInput
                                name={`tasks[${index}].${TasksStagesFields.PLANE_DATE}`}
                                label={
                                  TasksStagesLabels[
                                    TasksStagesFields.PLANE_DATE
                                  ]
                                }
                                onChange={(value) =>
                                  handleDateChange(value, index)
                                }
                              />
                            </Grid>
                            <Grid item xs={3}>
                              <CheckboxInput
                                name={`tasks[${index}].${TasksStagesFields.CONTROL}`}
                                label={
                                  TasksStagesLabels[TasksStagesFields.CONTROL]
                                }
                              />
                            </Grid>
                            <Grid item xs={6}>
                              <ClerkInput
                                name={`tasks[${index}].${TasksStagesFields.CLERKS}`}
                                label={
                                  TasksStagesLabels[TasksStagesFields.CLERKS]
                                }
                                multiple
                                required
                                matchFrom="start"
                              />
                            </Grid>
                            <Grid item xs={6}>
                              <Input
                                name={`tasks[${index}].${TasksStagesFields.COMMENTS}`}
                                label={
                                  TasksStagesLabels[TasksStagesFields.COMMENTS]
                                }
                                value={values.tasks[index].comments || ''}
                                onChange={handleChange}
                                multiline
                              />
                            </Grid>
                            <Grid item xs={12}>
                              <DataTable
                                formKey={'add_task_to_stage_modal'}
                                rows={taskRows}
                                cols={docCols}
                                initialSelected={initialSelected}
                                showFilters={tableFilters}
                                onRowSelect={(docs) => {
                                  setFieldValue(
                                    `tasks[${index}].docs`,
                                    Object.entries(docs)
                                      .filter(([_, value]) => value)
                                      .map(([key, _]) => key)
                                  );
                                }}
                                height="auto"
                              />
                            </Grid>
                            <Grid item xs={12}>
                              <div className={styles.hor} />
                            </Grid>
                          </Grid>
                        );
                      })}
                      <Stack direction="row" justifyContent="flex-end">
                        <Button
                          startIcon={<AddBox />}
                          onClick={() => push(emptyTask)}
                        >
                          Добавить задачу
                        </Button>
                      </Stack>
                    </>
                  );
                }}
              />
            </Form>
          </Popup>
        );
      }}
    </Formik>
  );
};

import { useCallback, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import { matchPath, useLocation, useSearchParams } from 'react-router-dom';

import { Add, ChevronRight, DeleteSweep, Refresh } from '@mui/icons-material';

import { DataTable } from '../../../../components/CustomDataGrid';
import { ExcelIcon } from '../../../../components/Icons';
import { RoundButton } from '../../../../components/RoundButton';
import {
  DeleteToolButton,
  SettingsTypes,
  ToolButton,
  ToolsPanel,
} from '../../../../components/ToolsPanel';
import { Routes } from '../../../../constants/routes';
import {
  useDeleteDocumentFollowedMutation,
  useGetDocumentsFollowedQuery,
} from '../../../../services/api/document';
import { RootState } from '../../../../store';
import {
  FollowedDocumentCols,
  followedToRowAdapter,
} from '../../../../utils/documents';

import styles from './styles.module.scss';

export const Followed = () => {
  const [cols, setCols] = useState(FollowedDocumentCols);
  const [page, setPage] = useState(1);
  const location = useLocation();
  const [perPage, setPerPage] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const selectedArr = useMemo(
    () => Object.keys(selected).filter((key) => selected[Number(key)]),
    [selected]
  );
  const [searchParams] = useSearchParams();
  const id = Number(
    matchPath(Routes.DOCUMENTS.DOCUMENT_TABS, location.pathname)?.params?.id ||
      ''
  );
  const {
    data: followed,
    isLoading,
    refetch,
  } = useGetDocumentsFollowedQuery(
    {
      id: id || '',
      page,
      size: perPage,
      useCount: true,
    },
    { skip: !id }
  );

  const [deleteDocument] = useDeleteDocumentFollowedMutation();
  const { rows } = useMemo(
    () => followedToRowAdapter(followed?.data),
    [followed]
  );

  const handleDelete = () => {
    deleteDocument({ id: selectedArr[0] });
  };

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const visibleColumns = useMemo(
    () => cols.filter((col) => !col.hidden),
    [cols]
  );

  const rowDoubleHandler = useCallback(
    (e: any) => {
      if (e.id) {
        window
          .open(
            Routes.DOCUMENTS.DOCUMENT.replace(':id', e.id),
            openFormInNewTab ? '_blank' : '_self'
          )
          ?.focus();
      }
    },
    [openFormInNewTab]
  );

  const viewFollowed = useCallback(() => {
    window
      .open(
        `/documents?id=${selectedArr[0]}`,
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  }, [selectedArr, openFormInNewTab]);

  return (
    <div className={styles.content}>
      <DataTable
        formKey={'bookmarks_follwed'}
        cols={cols}
        rows={rows}
        onRowSelect={setSelected}
        onRowDoubleClick={rowDoubleHandler}
        height={'fullHeight'}
        loading={isLoading}
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: followed?.totalCount || 0,
        }}
      >
        <ToolsPanel
          leftActions={
            <>
              <RoundButton icon={<Refresh />} onClick={refetch} />
              <RoundButton icon={<ExcelIcon />} />
            </>
          }
          settings={[SettingsTypes.COLS, SettingsTypes.TOOLS]}
          className={styles.toolsPanel}
        >
          <ToolButton
            label="Подробно"
            endIcon={<ChevronRight />}
            disabled={selectedArr.length !== 1}
            onClick={viewFollowed}
            fast
          />
          <ToolButton label="Добавить" startIcon={<Add />} fast />
          <DeleteToolButton
            label="Удалить"
            startIcon={<DeleteSweep />}
            disabled={selectedArr.length !== 1}
            fast
            description="Вы действительно хотите удалить документ?"
            title="Удаление документа"
            onConfirm={handleDelete}
          />
        </ToolsPanel>
      </DataTable>
    </div>
  );
};

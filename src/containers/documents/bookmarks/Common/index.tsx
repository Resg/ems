import React, { useMemo } from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { matchPath, useLocation, useSearchParams } from 'react-router-dom';

import { Grid } from '@mui/material';

import { Routes } from '../../../../constants/routes';
import { FilesTable } from '../../../../containers/documents/FilesTable';
import {
  useGetDocumentFilesQuery,
  useGetIncludedDocumentFilesQuery,
} from '../../../../services/api/document';
import { CommonDocumentType } from '../../../../types/documents';
import {
  filesToTableAdapter,
  includedFilesToRowAdapter,
} from '../../../../utils/documents';
import IncomingDocumentForm from '../../incoming/IncomingDocumentForm';
import { OutcomingDocumentForm } from '../../outcoming';
import { OutcomingDRKK } from '../../outcoming_DRKK';
import PodrDocumentForm from '../../podr';
import PredprDocumentForm from '../../predpr';
import { SightingsTable } from '../../SightingsTable';

export const Common: React.FC<{ docData: CommonDocumentType }> = ({
  docData,
}) => {
  const [searchParams] = useSearchParams();
  const location = useLocation();
  const id = Number(
    matchPath(Routes.DOCUMENTS.DOCUMENT_TABS, location.pathname)?.params?.id ||
      ''
  );
  const { data: documentFiles, refetch } = useGetDocumentFilesQuery(
    { id: id || '' },
    { skip: !id }
  );

  const { data: includedFiles } = useGetIncludedDocumentFilesQuery(
    { id: id || '' },
    { skip: !id }
  );

  const refetchTable = () => refetch();

  const docTypes = {
    DOCS_INCOMING: <IncomingDocumentForm formData={{ ...docData }} />,
    DOCS_OUTCOMING: (
      <OutcomingDocumentForm
        isNeedHTML5Backend={false}
        formData={{ ...docData }}
      />
    ),
    DOCS_PREDPR: <PredprDocumentForm formData={{ ...docData }} />,
    DOCS_PODR: <PodrDocumentForm formData={{ ...docData }} />,
    DOCS_OUTCOMING_DRKK: <OutcomingDRKK formData={{ ...docData }} />,
  };

  const updatedRows = useMemo(() => {
    return documentFiles?.data || [];
  }, [documentFiles?.data]);

  const { cols, rows } = filesToTableAdapter(updatedRows);

  const rowsForTable = includedFiles?.data?.length
    ? rows.concat(includedFilesToRowAdapter(includedFiles?.data))
    : rows;

  return (
    <Grid container spacing={2}>
      <DndProvider backend={HTML5Backend}>
        <Grid item>
          {docTypes[docData.objectName as keyof typeof docTypes]}
        </Grid>
        <Grid item xs={12}>
          <FilesTable
            label="Файлы"
            columns={cols}
            rows={rowsForTable}
            refetchTable={refetchTable}
            isNeedHTML5Backend={false}
          />
        </Grid>
        {(docData.objectName as keyof typeof docTypes) !== 'DOCS_INCOMING' ? (
          <Grid item xs={12}>
            <SightingsTable
              label="Визирующие"
              id={id}
              isNeedHTML5Backend={false}
            />
          </Grid>
        ) : null}
      </DndProvider>
    </Grid>
  );
};

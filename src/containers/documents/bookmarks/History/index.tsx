import React, { useCallback, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import { matchPath, useLocation, useSearchParams } from 'react-router-dom';

import { ChevronRight, Refresh } from '@mui/icons-material';

import { DataTable } from '../../../../components/CustomDataGrid';
import { RoundButton } from '../../../../components/RoundButton';
import { ToolButton, ToolsPanel } from '../../../../components/ToolsPanel';
import { HistoryCols } from '../../../../constants/documents';
import { Routes } from '../../../../constants/routes';
import {
  useGetDocumentByIdQuery,
  useGetHistoryQuery,
} from '../../../../services/api/document';
import { RootState } from '../../../../store';
import { historyToRowAdapter } from '../../../../utils/history';

import styles from './styles.module.scss';

export const History = () => {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const [cols, setCols] = useState(HistoryCols);
  const [searchParams] = useSearchParams();
  const location = useLocation();
  const id = Number(
    matchPath(Routes.DOCUMENTS.DOCUMENT_TABS, location.pathname)?.params?.id ||
      ''
  );

  const { data, isLoading, refetch } = useGetHistoryQuery(
    {
      documentId: Number(id),
      page,
      size: perPage,
      useCount: true,
    },
    { skip: !id }
  );

  const { data: document } = useGetDocumentByIdQuery(
    { id: Number(id) },
    { skip: !id }
  );

  const selectedArr = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const detailsId = useMemo(() => {
    return (
      data?.data?.filter((obj) => selectedArr.includes(obj.orderNumber)) || []
    );
  }, [selected]);

  const visibleColumns = useMemo(
    () => cols.filter((col: { hidden: boolean }) => !col.hidden),
    [cols]
  );

  const columnsCheck = useMemo(() => {
    if (document?.typeId === 21) {
      return HistoryCols;
    }
    return visibleColumns;
  }, [document]);

  const rows = useMemo(() => {
    return historyToRowAdapter(data?.data);
  }, [data]);

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const handleDetails = useCallback(() => {
    window
      .open(
        `/registries/${detailsId[0].registryId}`,
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  }, [detailsId, openFormInNewTab]);

  const handleRowDoubleClick = useCallback(
    (e: any) => {
      if (e.id) {
        window
          .open(
            `/registries/${detailsId[0].registryId}`,
            openFormInNewTab ? '_blank' : '_self'
          )
          ?.focus();
      }
    },
    [selected, openFormInNewTab]
  );

  return (
    <div className={styles.content}>
      <DataTable
        formKey={'bookmarks_history'}
        cols={columnsCheck}
        loading={isLoading}
        rows={rows}
        height="fullHeight"
        onRowDoubleClick={handleRowDoubleClick}
        onRowSelect={setSelected}
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: data?.totalCount || 0,
        }}
        selectFieldName={'orderNumber'}
      >
        <ToolsPanel
          className={styles.toolBar}
          leftActions={
            <>
              <RoundButton icon={<Refresh />} onClick={refetch} />
            </>
          }
        >
          <ToolButton
            label="Открыть реестр"
            endIcon={<ChevronRight />}
            onClick={handleDetails}
            disabled={selectedArr.length !== 1}
            fast
          />
        </ToolsPanel>
      </DataTable>
    </div>
  );
};

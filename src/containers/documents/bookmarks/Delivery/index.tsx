import React, { useCallback, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';

import { ChevronRight, PlaylistAdd, Refresh } from '@mui/icons-material';

import { DataTable } from '../../../../components/CustomDataGrid';
import { RoundButton } from '../../../../components/RoundButton';
import {
  SettingsTypes,
  ToolButton,
  ToolsPanel,
} from '../../../../components/ToolsPanel';
import { DeliveryCols } from '../../../../constants/documents';
import { useGetDocumentDeliveryQuery } from '../../../../services/api/document';
import { RootState } from '../../../../store';
import { deliveryToRowsAdapter } from '../../../../utils/documents';
import AddToRegistryPopup from '../../../Registry/AddToRegistryPopup';

import styles from './styles.module.scss';

export interface DeliveryProps {
  docId: number;
}

export const Delivery: React.FC<DeliveryProps> = ({ docId }) => {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [cols, setCols] = useState(DeliveryCols);
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const [popupMode, setPopupMode] = useState<'documents' | 'packets' | null>(
    null
  );

  const selectedArr = useMemo(
    () => Object.keys(selected).filter((key) => selected[Number(key)]),
    [selected]
  );
  const visibleColumns = useMemo(
    () => cols.filter((col) => !col.hidden),
    [cols]
  );

  const { data, refetch, isLoading } = useGetDocumentDeliveryQuery(
    { documentId: docId, page: page, size: perPage, useCount: true },
    { skip: !docId }
  );
  const rows = useMemo(() => deliveryToRowsAdapter(data?.data), [data]);
  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const handleDetails = useCallback(() => {
    window
      .open(
        `/registries/outgoingpacket/${selectedArr[0]}`,
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  }, [selectedArr, openFormInNewTab]);

  const rowDoubleHandler = useCallback(
    (e: any) => {
      if (e.id) {
        window
          .open(
            `/registries/outgoingpacket/${e.id}`,
            openFormInNewTab ? '_blank' : '_self'
          )
          ?.focus();
      }
    },
    [openFormInNewTab]
  );

  return (
    <div className={styles.content}>
      <DataTable
        formKey={'bookmarks_delivery'}
        cols={visibleColumns}
        rows={rows}
        loading={isLoading}
        className={styles.table}
        height="fullHeight"
        onRowSelect={setSelected}
        onRowDoubleClick={rowDoubleHandler}
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: data?.totalCount || 0,
        }}
      >
        <ToolsPanel
          settings={[SettingsTypes.COLS]}
          leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
        >
          <ToolButton
            label={'Подробно'}
            startIcon={<ChevronRight />}
            fast
            onClick={handleDetails}
            disabled={selectedArr.length !== 1}
          />
          <ToolButton
            label={'Добавить во внутренний реестр'}
            startIcon={<PlaylistAdd />}
            fast
            onClick={() => setPopupMode('documents')}
            disabled={selectedArr.length !== 1}
          />
        </ToolsPanel>
      </DataTable>
      <AddToRegistryPopup
        mode={popupMode}
        setMode={setPopupMode}
        documentIds={Number(docId)}
      />
    </div>
  );
};

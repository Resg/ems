import React, { useCallback, useMemo, useState } from 'react';
import { useLocation, useParams, useSearchParams } from 'react-router-dom';

import {
  AddCircle,
  Approval,
  Check,
  ChevronRight,
  DeleteOutline,
  DriveFileRenameOutline,
  ExpandMore,
  Pattern,
  Refresh,
  ReplyAll,
} from '@mui/icons-material';

import { TreeDataGrid } from '../../../../components/CustomDataGrid';
import { CreateRightGroupColumn } from '../../../../components/CustomDataGrid/TreeDataGrid/Columns/GroupColumn';
import { DefaultRow } from '../../../../components/CustomDataGrid/types';
import { createTreeByRows } from '../../../../components/CustomDataGrid/utils';
import { MenuButtonItem } from '../../../../components/MenuButton/MenuButtonItem';
import { Popup } from '../../../../components/Popup';
import {
  ResolutionForm,
  ResolutionToolButton,
} from '../../../../components/Resolution';
import { RoundButton } from '../../../../components/RoundButton';
import {
  DeleteToolButton,
  SettingsTypes,
  ToolButton,
  ToolsPanel,
} from '../../../../components/ToolsPanel';
import { MenuToolButton } from '../../../../components/ToolsPanel/MenuToolButton';
import { DocumentResolutionCols } from '../../../../constants/resolution';
import {
  useDeleteResolutionMutation,
  useGetResolutionListQuery,
  useRevokeResolutionMutation,
} from '../../../../services/api/resolutions';
import {
  useRevokeToWorkResolutionMutation,
  useSendAuthorResolutionMutation,
} from '../../../../services/api/task';
import { useGetUserQuery } from '../../../../services/api/user';

import styles from './styles.module.scss';

export const Resolutions: React.FC = () => {
  const [open, setOpen] = useState(false);
  const [clickId, setClickId] = useState<null | number>(null);
  const { data: user } = useGetUserQuery({});
  const [searchParams] = useSearchParams();
  const location = useLocation();
  const { id } = useParams();
  const documentId = Number(id);

  const { data, refetch, isLoading } = useGetResolutionListQuery(documentId);
  const rows = useMemo(() => {
    return (
      data?.data.map((resolution) => ({
        ...resolution,
        parentId: resolution.parentResolutionId,
      })) || []
    );
  }, [data?.data]);

  const [deleteResolution] = useDeleteResolutionMutation();
  const [revokeToWork] = useRevokeToWorkResolutionMutation();
  const [sendAuthor] = useSendAuthorResolutionMutation();
  const [revokeToEditor] = useRevokeResolutionMutation();

  const [cols, setCols] = useState(DocumentResolutionCols);
  const [selected, setSelected] = useState<number[]>([]);
  const [filters, setFilters] = useState(false);

  const firstSelected = useMemo(() => {
    return rows.find((resolution) => resolution.id === selected[0]);
  }, [rows, selected]);

  const visibleColumns = useMemo(
    () => cols.filter((col) => !col.hidden),
    [cols]
  );

  const handleSelected = useCallback((value: Record<string, boolean>) => {
    const selectedIds = Object.keys(value)
      .map((el) => Number(el))
      .filter((el) => value[el]);
    setSelected(selectedIds);
  }, []);

  const handleDelete = useCallback(async () => {
    await deleteResolution(selected[0]);
    refetch();
  }, [selected]);

  const handleRevokeToWork = useCallback(async () => {
    await revokeToWork({ id: selected[0] });
    refetch();
  }, [selected]);

  const handleRevokeToEditor = useCallback(async () => {
    await revokeToEditor({ id: selected[0] });
    refetch();
  }, [selected]);

  const handleSendAuthor = useCallback(async () => {
    await sendAuthor({ id: selected[0] });
    refetch();
  }, [selected]);

  const rowDoubleHandler = useCallback(
    (row: any) => {
      setClickId(row.id);
      setOpen(true);
    },
    [selected]
  );

  const handleRevokeToWorkDisabled = useMemo(() => {
    if (firstSelected && user) {
      return !(
        selected.length === 1 &&
        firstSelected.authorClerkId === user.clerkId &&
        firstSelected.statusName === 'Готова к выполнению'
      );
    }
    return true;
  }, [firstSelected, user, selected]);

  const handleRevokeToEditorDisabled = useMemo(() => {
    if (firstSelected && user) {
      return !(
        selected.length === 1 &&
        firstSelected.editorClerkId === user.clerkId &&
        firstSelected.editorClerkId !== firstSelected.authorClerkId &&
        firstSelected.statusName === 'Проект'
      );
    }
    return true;
  }, [firstSelected, user, selected]);

  const handleSendAuthorDisabled = useMemo(() => {
    if (firstSelected && user) {
      return !(
        selected.length === 1 &&
        firstSelected.editorClerkId === user.clerkId &&
        firstSelected.editorClerkId !== firstSelected.authorClerkId &&
        firstSelected.statusName === 'Редактирование'
      );
    }
    return true;
  }, [firstSelected, user, selected]);

  return (
    <div className={styles.content}>
      <div className={styles.tableWrap}>
        <TreeDataGrid
          cols={cols}
          rows={rows}
          groups={['parentResolutionId']}
          formKey={'Resolutions'}
          onRowSelect={handleSelected}
          loading={isLoading}
          createTree={(rows: DefaultRow[]) => createTreeByRows(rows, true)}
          createColumn={(handleRowExpand: (rowId: string) => void) =>
            CreateRightGroupColumn(
              ['parentResolutionId'],
              handleRowExpand,
              'Тип'
            )
          }
          className={styles.table}
          childrenNoSelect
          onRowDoubleClick={rowDoubleHandler}
          expanded
          showFilters={filters}
        >
          <ToolsPanel
            setFilters={setFilters}
            leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
            settings={[SettingsTypes.COLS, SettingsTypes.TOOLS]}
            className={styles.toolsPanel}
          >
            <MenuToolButton
              label="Создать"
              startIcon={<AddCircle />}
              endIcon={<ExpandMore />}
            >
              <ResolutionToolButton
                label="Резолюцию"
                documentIds={[documentId]}
                variant="menu"
                refetch={refetch}
              />
              <MenuButtonItem icon={<Approval />}>
                Резолюцию по шаблону
              </MenuButtonItem>
              <MenuButtonItem icon={<Approval />}>
                Подчиненную резолюцию
              </MenuButtonItem>
            </MenuToolButton>

            <ToolButton
              label="Подробно"
              startIcon={<ChevronRight />}
              disabled={selected.length !== 1}
              fast
            />
            <DeleteToolButton
              label="Удалить"
              startIcon={<DeleteOutline />}
              disabled={selected.length !== 1}
              fast
              description="Вы действительно хотите удалить выбранные резолюции?"
              title="Удаление резолюций"
              onConfirm={handleDelete}
            />
            <ToolButton
              startIcon={<Pattern />}
              label="Сохранить в шаблон"
              disabled
            />
            <ToolButton
              startIcon={<ReplyAll />}
              label="Отозвать на доработку"
              onClick={handleRevokeToWork}
              disabled={handleRevokeToWorkDisabled}
            />
            <ToolButton
              startIcon={<DriveFileRenameOutline />}
              label="Отозвать на редактирование"
              onClick={handleRevokeToEditor}
              disabled={handleRevokeToEditorDisabled}
            />
            <ToolButton
              startIcon={<Check />}
              label="Отправить на утверждение"
              onClick={handleSendAuthor}
              disabled={handleSendAuthorDisabled}
            />
          </ToolsPanel>
        </TreeDataGrid>
      </div>
      <Popup
        title={`Резолюция ${clickId}`}
        open={open}
        onClose={() => setOpen(false)}
      >
        <ResolutionForm
          onClose={() => setOpen(false)}
          refetch={refetch}
          id={clickId}
        />
      </Popup>
    </div>
  );
};

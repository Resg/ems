import React, { useState } from 'react';

import { Box, Tab } from '@mui/material';

import { DataTable } from '../../components/CustomDataGrid';
import { useGetDocumentSightingsQuery } from '../../services/api/document';
import { sightingsToTableAdapter } from '../../utils/documents';

import styles from './styles.module.scss';

export interface SightingsTableProps {
  label: string;
  id: number | null;
  isNeedHTML5Backend?: boolean;
}

export const SightingsTable: React.FC<SightingsTableProps> = ({
  label,
  id,
  isNeedHTML5Backend = true,
}) => {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);

  const { data: slightings, isLoading } = useGetDocumentSightingsQuery(
    {
      id: id || '',
      page,
      size: perPage,
      useCount: true,
    },
    {
      skip: !id,
    }
  );

  const { cols, rows } = sightingsToTableAdapter(slightings?.data);

  return (
    <Box sx={{ mt: 1, mr: 1.5 }}>
      <Tab label={label} />
      <DataTable
        formKey={'sightings_table'}
        cols={cols}
        rows={rows}
        loading={isLoading}
        className={styles.table}
        noSelect
        height={'auto'}
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: slightings?.totalCount || 0,
        }}
        isNeedHTML5Backend={isNeedHTML5Backend}
      />
    </Box>
  );
};

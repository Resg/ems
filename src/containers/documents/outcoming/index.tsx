import React, { useCallback, useMemo, useState } from 'react';
import { Form, Formik } from 'formik';
import { useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';

import {
  Edit,
  InsertDriveFile,
  PlaylistAdd,
  QrCode,
  Save,
  StarOutline,
} from '@mui/icons-material';
import { Grid } from '@mui/material';

import { AutoCompleteInput } from '../../../components/AutoCompleteInput';
import { CheckboxInput } from '../../../components/CheckboxInput';
import { ClerkInput } from '../../../components/ClerkInput';
import { DatePickerInput } from '../../../components/DatePickerInput';
import { BarCode } from '../../../components/Icons';
import Input from '../../../components/Input';
import { RoundButton } from '../../../components/RoundButton';
import {
  SettingsTypes,
  ToolButton,
  ToolsPanel,
} from '../../../components/ToolsPanel';
import {
  DefaultOutcomingDocument,
  OutcomingDocumentFields,
  OutcomingDocumentLabels,
} from '../../../constants/outcomingDocumentForm';
import {
  useGetAccessesQuery,
  useGetDocumentRubricsOptionsQuery,
} from '../../../services/api/dictionaries';
import {
  useCreateDocumentMutation,
  useGetDocumentByIdQuery,
  useGetDocumentFilesQuery,
  useGetDocumentOwnersQuery,
  useGetDocumentRubricsQuery,
  useSetDocumentToFavoritesMutation,
  useUpdateDocumentMutation,
} from '../../../services/api/document';
import { useGetUserQuery } from '../../../services/api/user';
import { _axios } from '../../../services/axios';
import { RootState, useAppDispatch } from '../../../store';
import { setAdvancedCounterpartySearch } from '../../../store/advancedCounterpartySearch';
import { setError } from '../../../store/utils';
import type { CommonDocumentFormData } from '../../../types/documents';
import {
  outcomingCreateAdapter,
  outcomingUpdateAdapter,
  sortByLabel,
} from '../../../utils/documents';
import AddToRegistryPopup from '../../Registry/AddToRegistryPopup';

import { Receivers, ReceiversRow } from './Receivers/Receivers';
import { TemplatesInput } from './TemplatesInput';
import { validationSchema } from './validation';

import styles from './styles.module.scss';

export interface OutcomingDocumentFormProps {
  formData?: CommonDocumentFormData;
  isNeedHTML5Backend?: boolean;
}

export const OutcomingDocumentForm: React.FC<OutcomingDocumentFormProps> = ({
  formData,
  isNeedHTML5Backend = true,
}) => {
  const [popupMode, setPopupMode] = useState<'documents' | 'packets' | null>(
    null
  );
  const { id = '' } = useParams();
  const navigate = useNavigate();
  const [setFavorite] = useSetDocumentToFavoritesMutation();
  const [updateDocument] = useUpdateDocumentMutation();
  const [createDocument] = useCreateDocumentMutation();
  const { data: userInfo } = useGetUserQuery({});
  const { data: accesses = [] } = useGetAccessesQuery({});
  const { data: rubrics = [] } = useGetDocumentRubricsOptionsQuery({
    documentTypeId: 21,
  });
  const { data: files, refetch } = useGetDocumentFilesQuery(
    { id },
    { skip: !id }
  );
  const { data: owners } = useGetDocumentOwnersQuery({ id }, { skip: !id });
  const { data: docRubrics } = useGetDocumentRubricsQuery(
    { id },
    { skip: !id }
  );
  const { data: documentData } = useGetDocumentByIdQuery({ id }, { skip: !id });
  const canEdit = formData?.canEdit;
  const [editMode, setEditMode] = useState(!id);
  const defaultGreef = accesses.find((i) => i.label === 'Общий')?.value;
  const finalFile = files?.data.find((f) => f.type === 'F')?.name || '';
  const rubricList = useMemo(
    () => docRubrics?.data.map((i) => i.id) || [],
    [docRubrics]
  );
  const dispatch = useAppDispatch();

  if (formData?.typeId) {
    dispatch(setAdvancedCounterpartySearch(formData?.typeId));
  }

  const getSignersIds = useMemo(() => {
    const signers = owners?.data.filter((o) => o.usageCode === 'SIGNER');
    const signersIds = new Set();

    signers?.forEach((i) => signersIds.add(i.clerkId));

    return [...signersIds];
  }, [owners?.data]);

  const getReceivers = useMemo(() => {
    return (owners?.data.filter((o) => o.usageCode === 'RECEIVER') ||
      []) as ReceiversRow[];
  }, [owners]);

  const getPerformer = useMemo(() => {
    const performer = owners?.data.find((o) => o.usageCode === 'EXE');
    return performer?.clerkId || null;
  }, [owners?.data]);

  const initialValues = useMemo(
    () => ({
      ...(formData || DefaultOutcomingDocument),
      rubricList,
      performer: id ? getPerformer : userInfo?.clerkId,
      signerIds: id ? getSignersIds : [],
      documentAccessCode: !id ? defaultGreef : formData?.documentAccessCode,
      file: id ? finalFile : '',
    }),
    [
      formData,
      rubricList,
      id,
      getPerformer,
      userInfo?.clerkId,
      getSignersIds,
      defaultGreef,
      finalFile,
    ]
  );

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const addToFavorite = () => {
    id && setFavorite({ ids: [Number(id)] });
  };

  const handleSubmit = async (values: any) => {
    if (id && values.outerAddressees.length) {
      const updateData = outcomingUpdateAdapter(values);

      updateDocument({
        id,
        documentData: updateData,
      });
      setEditMode(false);
    } else if (values.outerAddressees.length) {
      const newDoc = await createDocument({
        documentData: outcomingCreateAdapter(values),
      });

      if ('data' in newDoc) {
        navigate(`/documents?id=${newDoc.data.id}`);
        window.location.reload();
      }
    } else if (!values.outerAddressees.length) {
      dispatch(setError({ message: 'Заполните таблицу "Адресаты"' }));
    }
  };
  const handleDetailsBarCode = useCallback(() => {
    window
      .open(
        `http://10.177.5.121:14000/barcode?code=${id}&barType=CODE128&Code128Set=C&x=0.05&barHeightCM=0.5`,
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  }, [id, openFormInNewTab]);

  const handleDetailsQRcode = useCallback(() => {
    _axios
      .get(
        `/documents/qrcode/document-file-archive-url?documentId=${id}&format=PNG`,
        { responseType: 'blob' }
      )
      .then((response: any) => {
        const blob = new Blob([response.data], { type: response.data.type });
        const url = URL.createObjectURL(blob);
        window.open(url, openFormInNewTab ? '_blank' : '_self');
      });
  }, [id, openFormInNewTab]);

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      enableReinitialize
      onSubmit={handleSubmit}
    >
      {({
        handleChange,
        values,
        errors,
        touched,
        handleBlur,
        handleSubmit,
        setTouched,
      }) => {
        return (
          <Form>
            <Grid xs={12} container spacing={2}>
              <Grid item container direction="row">
                <ToolsPanel
                  settings={[SettingsTypes.TOOLS]}
                  leftActions={
                    <>
                      {canEdit && !editMode && (
                        <RoundButton
                          onClick={() => setEditMode(true)}
                          icon={<Edit />}
                        />
                      )}
                      {(!id || (editMode && formData)) && (
                        <RoundButton
                          onClick={() => handleSubmit()}
                          icon={<Save />}
                        />
                      )}
                    </>
                  }
                  className={styles.tools}
                >
                  <ToolButton
                    label={'Сформировать документ'}
                    startIcon={<InsertDriveFile />}
                    disabled
                    fast
                  />
                  <ToolButton
                    label={'Добавить в реестр'}
                    startIcon={<PlaylistAdd />}
                    onClick={() => setPopupMode('documents')}
                    disabled={!id}
                    fast
                  />
                  <ToolButton
                    label={'Добавить в избранное'}
                    startIcon={<StarOutline />}
                    onClick={addToFavorite}
                    disabled={!id}
                  />
                  <ToolButton
                    label={'Печать штрих-кода'}
                    startIcon={<BarCode />}
                    onClick={() => handleDetailsBarCode()}
                    disabled={!id}
                  />
                  <ToolButton
                    label={'Печать QR-кода'}
                    startIcon={<QrCode />}
                    onClick={() => handleDetailsQRcode()}
                    disabled={!id}
                  />
                </ToolsPanel>
              </Grid>
              <Grid item container columns={4} spacing={2}>
                <Grid item xs={1}>
                  <Input
                    name={OutcomingDocumentFields.CODE}
                    label={
                      OutcomingDocumentLabels[OutcomingDocumentFields.CODE]
                    }
                    value={formData && values.id}
                    disabled
                  />
                </Grid>
                <Grid item xs={1}>
                  <Input
                    name={OutcomingDocumentFields.NUMBER}
                    label={
                      OutcomingDocumentLabels[OutcomingDocumentFields.NUMBER]
                    }
                    value={formData && values.number}
                    disabled
                  />
                </Grid>
                <Grid item xs={1}>
                  <DatePickerInput
                    name={OutcomingDocumentFields.DATE_SIGN}
                    label={
                      OutcomingDocumentLabels[OutcomingDocumentFields.DATE_SIGN]
                    }
                    disabled
                  />
                </Grid>
                <Grid item xs={1}>
                  <DatePickerInput
                    name={OutcomingDocumentFields.DATE}
                    label={
                      OutcomingDocumentLabels[OutcomingDocumentFields.DATE]
                    }
                    disabled
                  />
                </Grid>
              </Grid>
              <Grid item container columns={4} spacing={2}>
                <Grid item xs={2}>
                  <AutoCompleteInput
                    name={OutcomingDocumentFields.GREEF}
                    label={
                      OutcomingDocumentLabels[OutcomingDocumentFields.GREEF]
                    }
                    options={accesses}
                    disabled={!editMode}
                    required
                  />
                </Grid>
                <Grid item xs={1}>
                  <CheckboxInput
                    name={OutcomingDocumentFields.ACCESS}
                    label={
                      OutcomingDocumentLabels[OutcomingDocumentFields.ACCESS]
                    }
                    disabled={!editMode}
                  />
                </Grid>
                <Grid item xs={1}>
                  <CheckboxInput
                    name={OutcomingDocumentFields.EDS}
                    label={OutcomingDocumentLabels[OutcomingDocumentFields.EDS]}
                    disabled={!editMode}
                  />
                </Grid>
              </Grid>
              <Grid item container columns={1}>
                <Grid item xs={1}>
                  <ClerkInput
                    name={OutcomingDocumentFields.SIGNERS}
                    label={
                      OutcomingDocumentLabels[OutcomingDocumentFields.SIGNERS]
                    }
                    multiple
                    required
                    disabled={!editMode}
                  />
                </Grid>
              </Grid>
              <Grid xs={12} item container>
                <Receivers
                  rows={getReceivers}
                  editMode={editMode}
                  isNeedHTML5Backend={isNeedHTML5Backend}
                />
              </Grid>
              <Grid item container columns={4} spacing={2}>
                <Grid item xs={4}>
                  <AutoCompleteInput
                    name={OutcomingDocumentFields.RUBRICKS}
                    label={
                      OutcomingDocumentLabels[OutcomingDocumentFields.RUBRICKS]
                    }
                    options={sortByLabel(rubrics)}
                    disabled={!editMode}
                    required
                    multiple
                  />
                </Grid>
                <Grid item xs={2}>
                  <Input
                    name={OutcomingDocumentFields.CONTENT}
                    label={
                      OutcomingDocumentLabels[OutcomingDocumentFields.CONTENT]
                    }
                    value={values[OutcomingDocumentFields.CONTENT]}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    error={Boolean(
                      errors[OutcomingDocumentFields.CONTENT] &&
                        touched[OutcomingDocumentFields.CONTENT]
                    )}
                    helperText={
                      touched[OutcomingDocumentFields.CONTENT] &&
                      (errors[OutcomingDocumentFields.CONTENT] as string)
                    }
                    required
                    multiline
                    disabled={!editMode}
                  />
                </Grid>
                <Grid item xs={2}>
                  <Input
                    name={OutcomingDocumentFields.COMMENTS}
                    label={
                      OutcomingDocumentLabels[OutcomingDocumentFields.COMMENTS]
                    }
                    value={formData && values.comments}
                    disabled={!editMode}
                    multiline
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={2}>
                  <Input
                    name={OutcomingDocumentFields.STRUCTURE}
                    label={
                      OutcomingDocumentLabels[OutcomingDocumentFields.STRUCTURE]
                    }
                    value={
                      formData && values[OutcomingDocumentFields.STRUCTURE]
                    }
                    disabled={!editMode}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={2}>
                  <Input
                    name={OutcomingDocumentFields.DESTINATION_FILE}
                    label={
                      OutcomingDocumentLabels[
                        OutcomingDocumentFields.DESTINATION_FILE
                      ]
                    }
                    disabled
                    value={values[OutcomingDocumentFields.DESTINATION_FILE]}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={2}>
                  <ClerkInput
                    name={OutcomingDocumentFields.PERFORMER}
                    label={
                      OutcomingDocumentLabels[OutcomingDocumentFields.PERFORMER]
                    }
                    disabled={!editMode}
                  />
                </Grid>
                <Grid item xs={2}>
                  <TemplatesInput disabled={!editMode} />
                </Grid>
              </Grid>
            </Grid>
            <AddToRegistryPopup
              mode={popupMode}
              setMode={setPopupMode}
              documentIds={Number(id)}
            />
          </Form>
        );
      }}
    </Formik>
  );
};

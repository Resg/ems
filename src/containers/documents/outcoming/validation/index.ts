import { array, boolean, date, number, object, string } from 'yup';

import { OutcomingDocumentFields } from '../../../../constants/outcomingDocumentForm';

export const validationSchema = object({
  [OutcomingDocumentFields.CODE]: number().nullable(),
  [OutcomingDocumentFields.NUMBER]: string().nullable(),
  [OutcomingDocumentFields.DATE_SIGN]: date().nullable(),
  [OutcomingDocumentFields.DATE]: date().nullable(),
  [OutcomingDocumentFields.GREEF]: string().required('Обязательное поле'),
  [OutcomingDocumentFields.ACCESS]: boolean(),
  [OutcomingDocumentFields.EDS]: boolean(),

  [OutcomingDocumentFields.SIGNERS]: array()
    .of(number())
    .min(1, 'Обязательное поле'),

  [OutcomingDocumentFields.RUBRICKS]: array()
    .of(number())
    .min(1, 'Обязательное поле'),
  [OutcomingDocumentFields.CONTENT]: string()
    .nullable()
    .required('Обязательное поле'),
  [OutcomingDocumentFields.COMMENTS]: string().nullable(),
  [OutcomingDocumentFields.STRUCTURE]: string().nullable(),
  [OutcomingDocumentFields.PERFORMER]: number(),
  [OutcomingDocumentFields.TEMPLATE]: number().nullable(),
});

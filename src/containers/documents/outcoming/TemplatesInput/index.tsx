import React from 'react';
import { useFormikContext } from 'formik';

import { AutoCompleteInput } from '../../../../components/AutoCompleteInput';
import {
  OutcomingDocumentFields,
  OutcomingDocumentLabels,
} from '../../../../constants/outcomingDocumentForm';
import { useGetDocumentTemplatesQuery } from '../../../../services/api/dictionaries';

export interface TemplatesInputProps {
  disabled: boolean;
}

export const TemplatesInput: React.FC<TemplatesInputProps> = ({ disabled }) => {
  const { values } = useFormikContext<any>();
  const rubriks = values.rubricList;
  const { data: templates = [] } = useGetDocumentTemplatesQuery(
    { rubricIds: rubriks },
    { skip: !rubriks.length }
  );

  return (
    <AutoCompleteInput
      name={OutcomingDocumentFields.TEMPLATE}
      label={OutcomingDocumentLabels[OutcomingDocumentFields.TEMPLATE]}
      options={templates}
      disabled={disabled}
    />
  );
};

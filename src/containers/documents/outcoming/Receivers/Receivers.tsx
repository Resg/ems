import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useFormikContext } from 'formik';

import {
  Cancel,
  Check,
  DeleteOutline,
  Edit,
  ExpandMore,
  Refresh,
} from '@mui/icons-material';
import { Button, Dialog, Grid, Stack } from '@mui/material';

import Card from '../../../../components/Card';
import { DataTable } from '../../../../components/CustomDataGrid';
import { ReceiverForm } from '../../../../components/ReceiverForm';
import { RoundButton } from '../../../../components/RoundButton';
import {
  SettingsTypes,
  ToolButton,
  ToolsPanel,
} from '../../../../components/ToolsPanel';
import { documentReceiversCols } from '../../../../constants/documents';
import { DeliveryType } from '../../../../types/dictionaries';

import styles from './styles.module.scss';

export interface ReceiversRow {
  id: number;
  address?: string;
  contractorId: number;
  contractorName: string;
  contractorContactPerson: string;
  contractorContactPersonId: number;
  contractorAddressDeliveryType: string;
  contractorAddressDeliveryTypeCode: string;
  contractorContactInfo: string;
  contractorAddressPostIndex: string;
  contractorAddress: string;
  contractorContactId: number;
  packetOutgoingSendDate: string;
  packetPostNotificationNumber: string;
  packetOutgoingDeliveryDate: string;
  packetPostReturnDate: string;
  comments: string;
  packetOutgoingStatus: string;
  packetOutgoingPagesCount: number;
  contractorDeliveryId: number;
}

export interface ReceiversProps {
  rows: ReceiversRow[];
  editMode?: boolean;
  isNeedHTML5Backend?: boolean;
}

export const Receivers: React.FC<ReceiversProps> = ({
  rows,
  editMode,
  isNeedHTML5Backend = true,
}) => {
  const [open, setOpen] = useState(false);
  const [tableFilters, setTableFilters] = useState(false);
  const handleOpen = useCallback(() => setOpen(true), []);
  const handleClose = useCallback(() => setOpen(false), []);
  const { values, setFieldValue, errors, setErrors, setFieldError } =
    useFormikContext<any>();
  const [receiverData, setReceiverData] = useState<Record<string, any>[]>([]);
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const selectedArr = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const handleApply = useCallback(() => {
    setFieldValue(
      'outerAddressees',
      receiverData.map((receiver) => ({
        ...receiver,
        contractorContactInfo:
          receiver.contractorContactInfo || receiver.addressAndContactInfoView,
        contractorId: receiver.contractorId,
        contractorName: receiver.contractorName,
        contractorContactPersonId: receiver.contractorContactPersonId,
        contractorDeliveryId: receiver.deliveryId,
        contractorAddressDeliveryTypeCode: receiver.deliveryTypeCode,
        contractorContactId: receiver.contactId,
        contractorAddressDeliveryType: receiver.deliveryType,
        contractorContactPerson: receiver.fullName,
      }))
    );
    handleClose();
  }, [handleClose, receiverData, setFieldValue]);

  const resultRows = useMemo(
    () =>
      (values.outerAddressees
        ? values.outerAddressees
        : rows) as ReceiversRow[],
    [rows, values.outerAddressees]
  );

  const handleDelete = useCallback(() => {
    const newArr = resultRows.filter((el) => !selectedArr.includes(el.id));
    setFieldValue('outerAddressees', newArr);
    setSelected({});
  }, [selected, resultRows]);

  useEffect(() => {
    if (!('outerAddressees' in values)) {
      setFieldValue('outerAddressees', rows);
    }
  }, [rows, values]);

  const initialReceivers: Partial<DeliveryType>[] = useMemo(() => {
    return resultRows.map((row) => ({
      ...row,
      address: row.contractorAddress || row.address,
      addressAndContactInfoView: row.contractorAddress || row.address,
      contactPersonId: row.contractorContactPersonId,
      fullName: row.contractorContactPersonId,
      deliveryTypeCode: row.contractorAddressDeliveryTypeCode,
      deliveryType: row.contractorAddressDeliveryType,
      contactId: row.contractorContactId,
      deliveryId: row.contractorDeliveryId,
      id: (row.contractorDeliveryId?.toString() +
        row.contractorId?.toString() +
        row.contractorContactId?.toString()) as unknown as number,
      initialId: row.id,
    }));
  }, [resultRows]);

  return (
    <Grid container columns={1} spacing={2} width="100%">
      <Grid item xs={1}>
        <div className={styles.title}>Адресаты</div>
      </Grid>
      {editMode && (
        <Grid item xs={1}>
          <ToolsPanel
            settings={[SettingsTypes.TOOLS]}
            leftActions={<RoundButton icon={<Refresh />} />}
          >
            <ToolButton label={'Подробно'} startIcon={<ExpandMore />} fast />
            <ToolButton
              label={'Редактировать'}
              startIcon={<Edit />}
              onClick={handleOpen}
              fast
            />
            <ToolButton
              label={'Удалить'}
              startIcon={<DeleteOutline />}
              onClick={handleDelete}
              fast
            />
          </ToolsPanel>
        </Grid>
      )}
      <Dialog open={open} maxWidth={'lg'}>
        <Card className={styles.card}>
          <div className={styles.blockTitle}>
            Редактирование списка адресатов
          </div>
          <ReceiverForm
            initialReceivers={initialReceivers}
            onChange={setReceiverData}
          />
        </Card>
        <Stack
          direction="row"
          spacing={1.5}
          justifyContent="flex-end"
          alignItems="center"
          className={styles.actions}
        >
          <Button
            variant="contained"
            startIcon={<Check />}
            onClick={handleApply}
            disabled={!receiverData.length}
          >
            Выбрать
          </Button>
          <Button
            variant="outlined"
            startIcon={<Cancel />}
            onClick={handleClose}
          >
            Отмена
          </Button>
        </Stack>
      </Dialog>
      <Grid item xs={1}>
        <DataTable
          formKey={'outcoming_receivers'}
          height={'auto'}
          cols={documentReceiversCols}
          rows={resultRows}
          onRowSelect={setSelected}
          initialSelected={selected}
          className={styles.table}
          showFilters={tableFilters}
          isNeedHTML5Backend={isNeedHTML5Backend}
        />
      </Grid>
    </Grid>
  );
};

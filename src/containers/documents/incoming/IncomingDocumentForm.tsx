import React, { useCallback, useMemo, useState } from 'react';
import { Form, Formik } from 'formik';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import {
  AddCircle,
  Article,
  Check,
  ExpandMore,
  PlaylistAdd,
  QrCode,
  Radio,
  Water,
} from '@mui/icons-material';
import { Grid } from '@mui/material';

import { AutoCompleteInput } from '../../../components/AutoCompleteInput';
import { CheckboxInput } from '../../../components/CheckboxInput';
import { DatePickerInput } from '../../../components/DatePickerInput';
import { BarCode } from '../../../components/Icons';
import Input from '../../../components/Input';
import { MenuButtonItem } from '../../../components/MenuButton/MenuButtonItem';
import {
  SettingsTypes,
  ToolButton,
  ToolsPanel,
} from '../../../components/ToolsPanel';
import { MenuToolButton } from '../../../components/ToolsPanel/MenuToolButton';
import {
  DefaultIncomingDocument,
  IncomingDocumentFields,
  IncomingDocumentLabels,
} from '../../../constants/incomingDocumentForm';
import { Routes } from '../../../constants/routes';
import {
  useGetDocumentByIdQuery,
  useGetDocumentOwnersQuery,
  useGetDocumentRubricsQuery,
} from '../../../services/api/document';
import { _axios } from '../../../services/axios';
import { RootState } from '../../../store';
import { CommonDocumentFormData } from '../../../types/documents';
import {
  idClerkFields,
  idRubricFields,
  optionsClerkFields,
  optionsRubricFields,
} from '../../../utils/documents';
import { ownersToFormAdapter } from '../../../utils/documents';
import AddToRegistryPopup from '../../Registry/AddToRegistryPopup';

export interface IncomingDocumentFormProps {
  formData: CommonDocumentFormData;
}

const IncomingDocumentForm: React.FC<IncomingDocumentFormProps> = ({
  formData,
}) => {
  const [popupMode, setPopupMode] = useState<'documents' | 'packets' | null>(
    null
  );

  const { id = '' } = useParams();

  const { data: documentOwners } = useGetDocumentOwnersQuery({ id });
  const { data: documentRubrics } = useGetDocumentRubricsQuery({ id });
  const { data: documentData } = useGetDocumentByIdQuery({ id }, { skip: !id });
  const {
    contractorDocumentDate,
    contractorContactPerson,
    contractorDocumentNumber,
    commentsSender,
    clerkNameReceiver = [],
    clerkNameAddressee = [],
    clerkNameRegister = [],
  } = ownersToFormAdapter(documentOwners?.data);

  const initialValues = useMemo(
    () => ({
      ...(formData || DefaultIncomingDocument),
      ids: id ? [id] : [],
      rubricList: idRubricFields(documentRubrics?.data),
      clerkNameReceiver: idClerkFields(clerkNameReceiver),
      clerkNameAddressee: clerkNameAddressee.clerkId,
      clerkNameRegister: clerkNameRegister.clerkId,
      contractorDocumentDate,
    }),
    [formData, id, documentRubrics, documentOwners]
  );

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const handleDetailsBarCode = useCallback(() => {
    window
      .open(
        `http://10.177.5.121:14000/barcode?code=${id}&barType=CODE128&Code128Set=C&x=0.05&barHeightCM=0.5`,
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  }, [id, openFormInNewTab]);

  const handleDetailsQRcode = useCallback(() => {
    _axios
      .get(
        `/documents/qrcode/document-file-archive-url?documentId=${id}&format=PNG`,
        { responseType: 'blob' }
      )
      .then((response: any) => {
        const blob = new Blob([response.data], { type: response.data.type });
        const url = URL.createObjectURL(blob);
        window.open(url, openFormInNewTab ? '_blank' : '_self');
      });
  }, [id, openFormInNewTab]);

  return (
    <Formik
      initialValues={initialValues}
      enableReinitialize
      onSubmit={() => {}}
    >
      {() => {
        return (
          <Form>
            <Grid container spacing={2}>
              <Grid item container direction="row" justifyContent="flex-end">
                <ToolsPanel settings={[SettingsTypes.TOOLS]}>
                  <MenuToolButton
                    label={'создать заявку'}
                    startIcon={<AddCircle />}
                    endIcon={<ExpandMore />}
                  >
                    <MenuButtonItem
                      icon={<Article />}
                      linkProps={{
                        to: `${Routes.REQUESTS.CREATE}?incomingDocumentId=${id}`,
                        newTab: true,
                      }}
                    >
                      Общая форма
                    </MenuButtonItem>
                    <MenuButtonItem
                      icon={<Radio />}
                      linkProps={{
                        to: Routes.REQUESTS.CREATE,
                        newTab: true,
                      }}
                    >
                      Заявка радиолюбителя
                    </MenuButtonItem>
                    <MenuButtonItem
                      icon={<Water />}
                      linkProps={{
                        to: Routes.REQUESTS.CREATE,
                        newTab: true,
                      }}
                    >
                      Заявка "Морская служба"
                    </MenuButtonItem>
                  </MenuToolButton>
                  <ToolButton
                    label={'Добавить в реестр'}
                    startIcon={<PlaylistAdd />}
                    onClick={() => setPopupMode('documents')}
                    fast
                  />
                  <ToolButton
                    label={'Печать штрих-кода'}
                    startIcon={<BarCode />}
                    disabled={!id}
                    onClick={() => handleDetailsBarCode()}
                  />
                  <ToolButton
                    label={'Печать QR-кода'}
                    startIcon={<QrCode />}
                    disabled={!id}
                    onClick={() => handleDetailsQRcode()}
                  />
                  <ToolButton label={'выполнить'} startIcon={<Check />} fast />
                </ToolsPanel>
              </Grid>
              <Grid item container columns={5} spacing={2}>
                <Grid item xs={1}>
                  <Input
                    name={IncomingDocumentFields.CODE}
                    label={IncomingDocumentLabels[IncomingDocumentFields.CODE]}
                    value={formData.id}
                    disabled
                  />
                </Grid>
                <Grid item xs={1}>
                  <Input
                    name={IncomingDocumentFields.NUMBER}
                    label={
                      IncomingDocumentLabels[IncomingDocumentFields.NUMBER]
                    }
                    value={formData.number}
                    disabled
                  />
                </Grid>
                <Grid item xs={1}>
                  <DatePickerInput
                    name={IncomingDocumentFields.REGISTRATION_DATE}
                    label={
                      IncomingDocumentLabels[
                        IncomingDocumentFields.REGISTRATION_DATE
                      ]
                    }
                    disabled
                  />
                </Grid>
                <Grid item xs={1}>
                  <AutoCompleteInput
                    name={IncomingDocumentFields.GREEF}
                    label={IncomingDocumentLabels[IncomingDocumentFields.GREEF]}
                    options={[
                      {
                        label: formData.documentAccess,
                        value: formData.documentAccess,
                      },
                    ]}
                    disabled
                  />
                </Grid>
                <Grid item xs={1}>
                  <CheckboxInput
                    name={IncomingDocumentFields.RESTRAINED_ACCESS}
                    label={
                      IncomingDocumentLabels[
                        IncomingDocumentFields.RESTRAINED_ACCESS
                      ]
                    }
                    disabled
                  />
                </Grid>
              </Grid>

              <Grid item container columns={5} spacing={2}>
                <Grid item xs={1}>
                  <Input
                    name={IncomingDocumentFields.CORRESPONDENT}
                    label={
                      IncomingDocumentLabels[
                        IncomingDocumentFields.CORRESPONDENT
                      ]
                    }
                    value={formData.contractorList}
                    disabled
                  />
                </Grid>
                <Grid item xs={1}>
                  <Input
                    name={IncomingDocumentFields.ASSIGNEE}
                    label={
                      IncomingDocumentLabels[IncomingDocumentFields.ASSIGNEE]
                    }
                    value={contractorContactPerson}
                    disabled
                  />
                </Grid>
                <Grid item xs={1}>
                  <Input
                    name={IncomingDocumentFields.OUTGOING_NUMBER}
                    label={
                      IncomingDocumentLabels[
                        IncomingDocumentFields.OUTGOING_NUMBER
                      ]
                    }
                    value={contractorDocumentNumber}
                    disabled
                  />
                </Grid>
                <Grid item xs={1}>
                  <DatePickerInput
                    name={IncomingDocumentFields.DATE}
                    label={IncomingDocumentLabels[IncomingDocumentFields.DATE]}
                    disabled
                  />
                </Grid>
                <Grid item xs={1}>
                  <Input
                    name={IncomingDocumentFields.ASSIGNEE_ANNOTATION}
                    label={
                      IncomingDocumentLabels[
                        IncomingDocumentFields.ASSIGNEE_ANNOTATION
                      ]
                    }
                    value={commentsSender}
                    disabled
                  />
                </Grid>
              </Grid>

              <Grid item container columns={5} spacing={2}>
                <Grid item xs={1}>
                  <AutoCompleteInput
                    name={IncomingDocumentFields.RUBRICKS}
                    label={`${
                      IncomingDocumentLabels[IncomingDocumentFields.RUBRICKS]
                    }*`}
                    options={optionsRubricFields(documentRubrics?.data) || []}
                    multiple
                    disabled
                  />
                </Grid>
                <Grid item xs={2}>
                  <Input
                    name={IncomingDocumentFields.CONTENT}
                    label={`${
                      IncomingDocumentLabels[IncomingDocumentFields.CONTENT]
                    }*`}
                    disabled
                    value={formData.description || ''}
                    multiline
                  />
                </Grid>
                <Grid item xs={2}>
                  <Input
                    name={IncomingDocumentFields.RUBRICKS_ANNOTATION}
                    label={
                      IncomingDocumentLabels[
                        IncomingDocumentFields.RUBRICKS_ANNOTATION
                      ]
                    }
                    value={formData.comments || ''}
                    multiline
                    disabled
                  />
                </Grid>
              </Grid>

              <Grid item container columns={5} spacing={2}>
                <Grid item xs={1}>
                  <AutoCompleteInput
                    name={IncomingDocumentFields.RECEIVER}
                    label={
                      IncomingDocumentLabels[IncomingDocumentFields.RECEIVER]
                    }
                    options={optionsClerkFields(clerkNameReceiver)}
                    multiple
                    disabled
                  />
                </Grid>
                <Grid item xs={1}>
                  <AutoCompleteInput
                    name={IncomingDocumentFields.ADDRESSEE}
                    label={
                      IncomingDocumentLabels[IncomingDocumentFields.ADDRESSEE]
                    }
                    options={[
                      {
                        label: clerkNameAddressee.clerkName,
                        value: clerkNameAddressee.clerkId,
                      },
                    ]}
                    disabled
                  />
                </Grid>
                <Grid item xs={1}>
                  <Input
                    name={IncomingDocumentFields.DELIVERY_TYPE}
                    label={
                      IncomingDocumentLabels[
                        IncomingDocumentFields.DELIVERY_TYPE
                      ]
                    }
                    value={formData.deliveryType || ''}
                    disabled
                  />
                </Grid>
                <Grid item xs={1}>
                  <AutoCompleteInput
                    name={IncomingDocumentFields.REGISTRATOR}
                    label={
                      IncomingDocumentLabels[IncomingDocumentFields.REGISTRATOR]
                    }
                    options={[
                      {
                        label: clerkNameRegister.clerkName,
                        value: clerkNameRegister.clerkId,
                      },
                    ]}
                    disabled
                  />
                </Grid>
                <Grid item xs={1}>
                  <Input
                    name={IncomingDocumentFields.STRUCTURE}
                    label={
                      IncomingDocumentLabels[IncomingDocumentFields.STRUCTURE]
                    }
                    value={formData.compound || ''}
                    disabled
                  />
                </Grid>
              </Grid>
            </Grid>
            <AddToRegistryPopup
              mode={popupMode}
              setMode={setPopupMode}
              documentIds={Number(id)}
            />
          </Form>
        );
      }}
    </Formik>
  );
};

export default IncomingDocumentForm;

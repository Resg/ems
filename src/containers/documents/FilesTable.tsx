import React, { useCallback, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import { matchPath, useLocation } from 'react-router-dom';

import { DeleteOutline, Edit, OpenInNew } from '@mui/icons-material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreIcon from '@mui/icons-material/MoreVert';
import RefreshIcon from '@mui/icons-material/Refresh';
import SettingsIcon from '@mui/icons-material/Settings';
import { Button, Card, Dialog, Grid, Stack, Tab } from '@mui/material';

import { DeleteButton } from '../../components/Buttons/DeleteButton';
import { TreeDataGrid } from '../../components/CustomDataGrid';
import { createTreeByRows } from '../../components/CustomDataGrid/utils';
import { Dropzone } from '../../components/Dropzone';
import { FileLoader } from '../../components/FileLoader';
import { RoundButton } from '../../components/RoundButton';
import { Routes } from '../../constants/routes';
import {
  useAddFileToDocumentMutation,
  useGetDocumentByIdQuery,
  useGetIsDocumentSignedQuery,
} from '../../services/api/document';
import {
  useDeleteDocumentFileMutation,
  useGetFileInfoMutation,
} from '../../services/api/files';
import { _axios } from '../../services/axios';
import { RootState } from '../../store';

import styles from './styles.module.scss';

export const FilesTable: React.FC<any> = (props) => {
  const {
    label,
    columns,
    rows,
    isReceiverTable,
    refetchTable,
    isLoading,
    isHidden = false,
    tableHeight = '400px',
    isNeedHTML5Backend = true,
  } = props;

  const [selected, setSelected] = useState<Record<string, boolean>>({});
  const [getFile] = useGetFileInfoMutation();
  const [openModal, setOpenModal] = useState(false);
  const [tableFilters, setTableFilters] = useState(false);

  const [addFileToDocument] = useAddFileToDocumentMutation();
  const location = useLocation();
  const id = Number(
    matchPath(Routes.DOCUMENTS.DOCUMENT_TABS, location.pathname)?.params?.id ||
      ''
  );

  const selectedArr = useMemo(
    () => Object.keys(selected).filter((key) => selected[Number(key)]),
    [selected]
  );
  const { data: documentData } = useGetDocumentByIdQuery({ id }, { skip: !id });
  const [deleteDocumentFile] = useDeleteDocumentFileMutation();
  const { data: isSignedDocument } = useGetIsDocumentSignedQuery(
    { id: Number(id) },
    { skip: !id }
  );
  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );
  const handleOpen = useCallback(async () => {
    const response = await getFile({ id: Number(selectedArr[0]) });
    if ('data' in response) {
      if (response?.data?.canOpenInR7Office)
        window.open(
          `/file_editor/${selectedArr[0]}`,
          openFormInNewTab ? '_blank' : '_self'
        );
      else {
        setOpenModal(true);
      }
    }
  }, [selectedArr, openFormInNewTab]);

  const handleDelete = useCallback(async () => {
    await deleteDocumentFile({ id: Number(selectedArr[0]) });
    setSelected({});
    refetchTable();
  }, [documentData, selectedArr]);

  const handleModal = async () => {
    const response = await getFile({ id: Number(selectedArr[0]) });
    if ('data' in response) {
      _axios
        .get(response.data.url, { responseType: 'blob' })
        .then((file: any) => {
          const url = URL.createObjectURL(file.data as Blob);
          const link = document.createElement('a');
          link.href = url;
          link.download = response.data.title;
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
          URL.revokeObjectURL(url);
          setOpenModal(false);
        });
    }
  };

  return (
    <>
      <Grid item container spacing={2}>
        <Grid item>
          <Tab label={label} />
        </Grid>
        <Grid item container direction="row" justifyContent="space-between">
          <Stack direction="row" spacing={2}>
            <RoundButton icon={<RefreshIcon />} onClick={refetchTable} />
          </Stack>
          {isReceiverTable ? (
            <Stack direction="row" spacing={2}>
              <Button variant="outlined" endIcon={<ExpandMoreIcon />} disabled>
                подробно
              </Button>
              <Button variant="outlined" startIcon={<Edit />} disabled>
                редактировать
              </Button>
              <Button variant="outlined" startIcon={<DeleteOutline />} disabled>
                удалить
              </Button>
              <RoundButton icon={<MoreIcon />} />
              <RoundButton icon={<SettingsIcon />} />
            </Stack>
          ) : (
            <Stack direction="row" spacing={2}>
              <Button
                variant="outlined"
                startIcon={<OpenInNew />}
                disabled={!(selectedArr.length === 1)}
                onClick={handleOpen}
              >
                открыть
              </Button>
              {!isHidden ? (
                <FileLoader
                  documentId={Number(id)}
                  label={'Добавить'}
                  refetchTable={refetchTable}
                  addFile={addFileToDocument}
                />
              ) : null}
              {!isHidden ? (
                <DeleteButton
                  label={'Удалить'}
                  startIcon={<DeleteOutline />}
                  disabled={
                    !(
                      selectedArr.length === 1 &&
                      documentData?.canDeleteFile === true
                    )
                  }
                  description={`Вы действительно хотите удалить файл?`}
                  title={'Удаление файла'}
                  onConfirm={handleDelete}
                  isHasSign={isSignedDocument?.hasSightOrSignOnExecution}
                  setSelected={setSelected}
                />
              ) : null}
              {!isHidden ? (
                <>
                  <RoundButton icon={<MoreIcon />} />
                  <RoundButton icon={<SettingsIcon />} />
                </>
              ) : null}
            </Stack>
          )}
        </Grid>
        <Grid item xs={12}>
          {!isHidden ? (
            <Dropzone
              documentId={Number(id)}
              refetchTable={refetchTable}
              addFile={addFileToDocument}
            >
              <TreeDataGrid
                formKey={'files_table'}
                cols={columns}
                rows={rows}
                groups={['name']}
                onRowSelect={setSelected}
                createTree={(r) => createTreeByRows(r, true)}
                className={styles.table}
                height={'auto'}
                loading={isLoading}
                helperText={'Перетащите файлы для загрузки'}
                isNeedHTML5Backend={isNeedHTML5Backend}
                showFilters={tableFilters}
              />
            </Dropzone>
          ) : (
            <TreeDataGrid
              formKey={'files_table'}
              cols={columns}
              rows={rows}
              groups={['name']}
              onRowSelect={setSelected}
              createTree={(r) => createTreeByRows(r, true)}
              className={styles.table}
              height={'auto'}
              loading={isLoading}
              isNeedHTML5Backend={isNeedHTML5Backend}
              showFilters={tableFilters}
            />
          )}
        </Grid>
      </Grid>
      <Dialog open={openModal}>
        <Card className={styles.card}>
          <div className={styles.dialogText}>
            К сожалению, данный файл невозможно открыть.
            <br />
            Хотите ли Вы его скачать?
          </div>
          <Stack
            direction="row"
            justifyContent="flex-end"
            spacing={1.5}
            width="100%"
          >
            <Button variant="contained" onClick={() => handleModal()}>
              Да
            </Button>
            <Button variant="outlined" onClick={() => setOpenModal(false)}>
              Нет
            </Button>
          </Stack>
        </Card>
      </Dialog>
    </>
  );
};

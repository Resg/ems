import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Form, Formik, useFormikContext } from 'formik';
import { useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';

import {
  Cancel,
  Edit,
  InsertDriveFile,
  QrCode,
  Save,
} from '@mui/icons-material';
import { Box, Grid } from '@mui/material';

import { AutoCompleteInput } from '../../../components/AutoCompleteInput';
import { CheckboxInput } from '../../../components/CheckboxInput';
import { ClerkInput } from '../../../components/ClerkInput';
import { DatePickerInput } from '../../../components/DatePickerInput';
import { BarCode } from '../../../components/Icons';
import Input from '../../../components/Input';
import { RoundButton } from '../../../components/RoundButton';
import {
  SettingsTypes,
  ToolButton,
  ToolsPanel,
} from '../../../components/ToolsPanel';
import {
  DefaultPodrDocument,
  PodrDocumentFields,
  PodrDocumentLabels,
} from '../../../constants/podrDocumentForm';
import { useGetDictionaries } from '../../../services/api/dictionaries/useGetDictionaries';
import {
  useCreateDocumentMutation,
  useGetDocumentByIdQuery,
  useGetDocumentOwnersQuery,
  useGetDocumentRubricsQuery,
  useUpdateDocumentMutation,
} from '../../../services/api/document';
import { useGetUserQuery } from '../../../services/api/user';
import { _axios } from '../../../services/axios';
import { RootState } from '../../../store';
import {
  CommonDocumentFormData,
  DocumentOwnerType,
  DocumentRubricType,
} from '../../../types/documents';
import { prepareFieldsData } from '../../../utils/documents';

import { validationSchema } from './validation';

export interface PodrDocumentFormProps {
  formData?: CommonDocumentFormData;
  editMode?: boolean;
}

const PodrDocumentForm: React.FC<PodrDocumentFormProps> = ({
  formData,
  editMode,
}) => {
  const { data: user } = useGetUserQuery({});
  const [isEditMode, setEditMode] = useState(!!editMode);
  const [rubricListIds, setRubricList] = useState<number[]>([]);

  const navigate = useNavigate();
  const { id = '' } = useParams();

  const { data: documentOwners } = useGetDocumentOwnersQuery(
    { id },
    { skip: !id }
  );
  const { data: documentRubrics } = useGetDocumentRubricsQuery(
    { id },
    { skip: !id }
  );
  const { data: documentData } = useGetDocumentByIdQuery({ id }, { skip: !id });
  const [updateDocument] = useUpdateDocumentMutation();
  const [createDocument] = useCreateDocumentMutation();

  const {
    dictionariesAccesses = [],
    dictionariesRubrics = [],
    dictionariesClerks = [],
    dictionariesTemplates = [],
  } = useGetDictionaries({
    rubrics: {
      documentTypeId: 22,
    },
    templates: {
      rubricIds: rubricListIds,
    },
  });

  const getRubricList = useMemo(() => {
    return dictionariesRubrics.length > 0 && documentRubrics
      ? documentRubrics.data.map((rubric: DocumentRubricType) => {
          return rubric.id;
        })
      : [];
  }, [documentRubrics, dictionariesRubrics]);

  const getClerkList = useCallback(
    (usageCode: string) => {
      return dictionariesClerks.length > 0 && documentOwners
        ? documentOwners.data
            .filter((clerk: DocumentOwnerType) => clerk.usageCode === usageCode)
            .map((clerk: DocumentOwnerType) => clerk.personId)
        : [];
    },
    [documentOwners, dictionariesClerks]
  );

  const UpdateRubricListIds = () => {
    const { values = {} as any } = useFormikContext();
    useEffect(() => {
      setRubricList(values.rubricList || []);
    }, [values.rubricList]);
    return null;
  };

  const handleSubmit = async (values = {} as any) => {
    const data = prepareFieldsData(
      !formData,
      values,
      dictionariesClerks,
      documentOwners?.data
    );
    if (formData) {
      await updateDocument({ id: values.id, documentData: data });
      setEditMode(false);
    } else {
      const res = await createDocument({
        documentData: { ...data, typeId: 22 },
      });
      if ('data' in res) {
        navigate(`/documents?id=${res.data.id}`);
        window.location.reload();
      }
    }
  };

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const handleDetailsBarCode = useCallback(() => {
    window
      .open(
        `http://10.177.5.121:14000/barcode?code=${id}&barType=CODE128&Code128Set=C&x=0.05&barHeightCM=0.5`,
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  }, [id, openFormInNewTab]);

  const handleDetailsQRcode = useCallback(() => {
    _axios
      .get(
        `/documents/qrcode/document-file-archive-url?documentId=${id}&format=PNG`,
        { responseType: 'blob' }
      )
      .then((response: any) => {
        const blob = new Blob([response.data], { type: response.data.type });
        const url = URL.createObjectURL(blob);
        window.open(url, openFormInNewTab ? '_blank' : '_self');
      });
  }, [id, openFormInNewTab]);

  const initialValues = useMemo(
    () => ({
      ...(formData || DefaultPodrDocument),
      divisionName: formData ? formData.divisionName : user?.division,
      documentAccessCode: formData ? formData.documentAccessCode : 'COM',
      clerkNameSigner: getClerkList('SIGNER'),
      clerkNameReceiver: getClerkList('RECEIVER'),
      clerkNameExe: formData ? getClerkList('EXE')[0] : user?.personId,
      rubricList: getRubricList,
    }),
    [
      formData,
      id,
      documentOwners,
      documentRubrics,
      dictionariesClerks,
      dictionariesRubrics,
    ]
  );

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      enableReinitialize
      onSubmit={handleSubmit}
    >
      {({
        handleSubmit,
        handleReset,
        handleChange,
        values,
        errors,
        touched,
        setTouched,
      }) => {
        return (
          <Form>
            <Box my={3}>
              <ToolsPanel
                leftActions={
                  isEditMode ? (
                    <>
                      <RoundButton
                        onClick={() => handleSubmit()}
                        icon={<Save />}
                      />
                      <RoundButton
                        icon={<Cancel />}
                        onClick={() => {
                          handleReset();
                          setEditMode(false);
                        }}
                      />
                    </>
                  ) : (
                    <RoundButton
                      icon={<Edit />}
                      onClick={() => setEditMode(true)}
                    />
                  )
                }
                settings={[SettingsTypes.TOOLS]}
              >
                <ToolButton
                  label={'Сформировать документ'}
                  startIcon={<InsertDriveFile />}
                  fast
                />
                <ToolButton
                  label={'Печать штрих-кода'}
                  startIcon={<BarCode />}
                  onClick={() => handleDetailsBarCode()}
                  disabled={!id}
                />
                <ToolButton
                  label={'Печать QR-кода'}
                  startIcon={<QrCode />}
                  onClick={() => handleDetailsQRcode()}
                  disabled={!id}
                />
              </ToolsPanel>
            </Box>
            <Grid container columnSpacing={4} rowSpacing={2}>
              <Grid item xs={3}>
                <Input
                  name={PodrDocumentFields.ID}
                  label={PodrDocumentLabels[PodrDocumentFields.ID]}
                  value={values[PodrDocumentFields.ID] || ''}
                  notEditableField={isEditMode}
                  disabled
                />
              </Grid>
              <Grid item xs={3}>
                <Input
                  name={PodrDocumentFields.NUMBER}
                  label={PodrDocumentLabels[PodrDocumentFields.NUMBER]}
                  value={values[PodrDocumentFields.NUMBER] || ''}
                  notEditableField={isEditMode}
                  disabled
                />
              </Grid>
              <Grid item xs={3}>
                <DatePickerInput
                  name={PodrDocumentFields.DATE}
                  label={PodrDocumentLabels[PodrDocumentFields.DATE]}
                  notEditableField={isEditMode}
                  disabled
                />
              </Grid>
              <Grid item xs={3}>
                <Input
                  name={PodrDocumentFields.DEVISION_NAME}
                  label={PodrDocumentLabels[PodrDocumentFields.DEVISION_NAME]}
                  value={values[PodrDocumentFields.DEVISION_NAME] || ''}
                  notEditableField={isEditMode}
                  disabled
                />
              </Grid>
              <Grid item xs={6}>
                <AutoCompleteInput
                  name={PodrDocumentFields.DOCUMENT_ACCESS}
                  label={PodrDocumentLabels[PodrDocumentFields.DOCUMENT_ACCESS]}
                  options={dictionariesAccesses.map((access) => {
                    return { value: access.code, label: access.name };
                  })}
                  disabled={!isEditMode}
                  required
                />
              </Grid>
              <Grid item xs={2.3}>
                <CheckboxInput
                  name={PodrDocumentFields.IS_CONFIDENTIAL}
                  label={PodrDocumentLabels[PodrDocumentFields.IS_CONFIDENTIAL]}
                  disabled={!isEditMode}
                />
              </Grid>
              <Grid item xs={3.7}>
                <CheckboxInput
                  name={PodrDocumentFields.MANUAL_SIGN}
                  label={PodrDocumentLabels[PodrDocumentFields.MANUAL_SIGN]}
                  disabled={!isEditMode}
                />
              </Grid>
              <Grid item xs={6}>
                <ClerkInput
                  name={PodrDocumentFields.CLERK_NAME_SIGNER}
                  label={
                    PodrDocumentLabels[PodrDocumentFields.CLERK_NAME_SIGNER]
                  }
                  options={dictionariesClerks.map((clerk) => {
                    return { value: clerk.personId, label: clerk.personFio };
                  })}
                  property={'personId'}
                  multiple
                  disabled
                />
              </Grid>
              <Grid item xs={6}>
                <ClerkInput
                  name={PodrDocumentFields.CLERK_NAME_RECEIVER}
                  label={
                    PodrDocumentLabels[PodrDocumentFields.CLERK_NAME_RECEIVER]
                  }
                  options={dictionariesClerks.map((clerk) => {
                    return { value: clerk.personId, label: clerk.personFio };
                  })}
                  property={'personId'}
                  multiple
                  disabled={!isEditMode}
                  required
                />
              </Grid>
              <Grid item xs={12}>
                <AutoCompleteInput
                  name={PodrDocumentFields.RUBRIC_LIST}
                  label={PodrDocumentLabels[PodrDocumentFields.RUBRIC_LIST]}
                  options={dictionariesRubrics.map((rubric) => {
                    return { value: rubric.id, label: rubric.name };
                  })}
                  multiple
                  disabled={!isEditMode}
                  required
                />
              </Grid>
              <Grid item xs={6}>
                <Input
                  name={PodrDocumentFields.DESCRIPTION}
                  label={PodrDocumentLabels[PodrDocumentFields.DESCRIPTION]}
                  value={values[PodrDocumentFields.DESCRIPTION] || ''}
                  onChange={handleChange}
                  disabled={!isEditMode}
                  error={Boolean(
                    errors[PodrDocumentFields.DESCRIPTION] &&
                      touched[PodrDocumentFields.DESCRIPTION]
                  )}
                  helperText={errors[PodrDocumentFields.DESCRIPTION]}
                  onBlur={() =>
                    setTouched({ [PodrDocumentFields.DESCRIPTION]: true })
                  }
                  multiline
                  required
                />
              </Grid>
              <Grid item xs={6}>
                <Input
                  name={PodrDocumentFields.COMMENTS}
                  label={PodrDocumentLabels[PodrDocumentFields.COMMENTS]}
                  value={values[PodrDocumentFields.COMMENTS] || ''}
                  onChange={handleChange}
                  disabled={!isEditMode}
                  multiline
                />
              </Grid>
              <Grid item xs={6}>
                <Input
                  name={PodrDocumentFields.COMPOUND}
                  label={PodrDocumentLabels[PodrDocumentFields.COMPOUND]}
                  value={values[PodrDocumentFields.COMPOUND] || ''}
                  onChange={handleChange}
                  disabled={!isEditMode}
                />
              </Grid>
              <Grid item xs={6}>
                <CheckboxInput
                  name={PodrDocumentFields.IS_ACTUAL}
                  label={PodrDocumentLabels[PodrDocumentFields.IS_ACTUAL]}
                  disabled={!isEditMode}
                />
              </Grid>
              <Grid item xs={6}>
                <ClerkInput
                  name={PodrDocumentFields.CLERK_NAME_EXE}
                  label={PodrDocumentLabels[PodrDocumentFields.CLERK_NAME_EXE]}
                  options={dictionariesClerks.map((clerk) => {
                    return { value: clerk.personId, label: clerk.personFio };
                  })}
                  property={'personId'}
                  disabled={!isEditMode}
                />
              </Grid>
              <Grid item xs={6}>
                <AutoCompleteInput
                  name={PodrDocumentFields.TEMPLATE_ID}
                  label={PodrDocumentLabels[PodrDocumentFields.TEMPLATE_ID]}
                  options={dictionariesTemplates}
                  disabled={!isEditMode}
                />
              </Grid>
            </Grid>
            <UpdateRubricListIds />
          </Form>
        );
      }}
    </Formik>
  );
};
export default PodrDocumentForm;

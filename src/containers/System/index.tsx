import React from 'react';

import { Box, Grid } from '@mui/material';

import styles from './styles.module.scss';

export const System: React.FC = () => {
  const jsonData = require('../../../package.json');

  return (
    <Box my={3}>
      <Grid container rowSpacing={2} columnSpacing={12}>
        <Grid item xs={12}>
          <div className={styles.label}>
            Версия приложения ИС Экспертиза ЭМС:
            <div className={styles.text}>{jsonData.version}</div>
          </div>
        </Grid>
      </Grid>
    </Box>
  );
};

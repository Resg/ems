import React, { useMemo, useState } from 'react';
import { Form, Formik } from 'formik';

import { Edit, EditOff, Save } from '@mui/icons-material';
import { Grid, Stack } from '@mui/material';

import { AutoCompleteInput } from '../../../components/AutoCompleteInput';
import { CheckboxInput } from '../../../components/CheckboxInput';
import { ClerkInput } from '../../../components/ClerkInput';
import { DatePickerInput } from '../../../components/DatePickerInput';
import Input from '../../../components/Input';
import { RoundButton } from '../../../components/RoundButton';
import {
  commonDetailDefaults,
  DetailFormFields,
  detailFormLabels,
} from '../../../constants/commonTasks';
import {
  useGetTaskQuery,
  useUpdateTaskMutation,
} from '../../../services/api/incomingDocumentTasks';
import { useGetOptionTaskChildrenQuery } from '../../../services/api/task';

import { validationSchema } from './validation';

export interface CommonProps {
  id: number;
}

export const Common = ({ id }: CommonProps) => {
  const [editMode, setEditMode] = useState(!id);
  const { data, refetch } = useGetTaskQuery(id);
  const { data: parentData } = useGetTaskQuery(data?.parentId, {
    skip: !data?.parentId,
  });
  const { data: taskChildren = [] } = useGetOptionTaskChildrenQuery(
    { parentId: parentData?.id, excludedTaskId: data?.id },
    { skip: !parentData?.id || !data?.id }
  );
  const [update] = useUpdateTaskMutation();

  const taskCheck = useMemo(() => {
    if (data?.parentId === null) {
      return false;
    }
    return true;
  }, [data]);

  const requestCheck = useMemo(() => {
    if (data?.objectTypeCode === 'REQUEST') {
      return false;
    }
    return true;
  }, [data]);

  const CheckLabelPreviousTitle = useMemo(() => {
    if (data?.parentId && parentData?.typeId !== 8) {
      return detailFormLabels[DetailFormFields.PREVIOUS_TITLE];
    }
    return detailFormLabels[DetailFormFields.PREVIOUS_TITLE_STAGE];
  }, [data, parentData]);

  const ChekLabelTitle = useMemo(() => {
    if (data?.parentId && parentData?.typeId !== 8) {
      return detailFormLabels[DetailFormFields.TITLE];
    }
    return detailFormLabels[DetailFormFields.TYPE_TITLE];
  }, [data, parentData]);

  const toDoValues = useMemo(() => {
    return taskChildren.filter((child: { value: number | undefined }) => {
      return child.value !== data?.previousId;
    });
  }, [taskChildren, data]);

  const intialtodoValue2 = useMemo(() => {
    if (data) {
      return [
        { label: data.previousTitle, value: data.previousId },
        ...toDoValues,
      ];
    }
    return [];
  }, [data, toDoValues]);

  const initialValues = useMemo(() => {
    return (
      {
        title: data?.title,
        previousTitle: parentData?.title,
        authorId: data?.authorId,
        performerId: data?.performerId,
        previousId: data?.previousId,
        term: data?.term,
        planDate: data?.planDate,
        comments: data?.comments,
        performerComment: data?.performerComment,
        isControl: data?.isControl,
        id: data?.id,
      } || commonDetailDefaults
    );
  }, [data, taskChildren, parentData]);

  const handleSubmit = async (values: any) => {
    const updateData = {
      id: values.id,
      title: values.title,
      comment: values.comments,
      previousId: values.previousId,
      term: values.term,
      performerId: values.performerId,
      isControl: values.isControl,
      authorId: values.authorId,
      NULLS: true,
      dateTarget: values.planDate,
    };

    setEditMode(false);
    await update({ id: id, updateData: updateData });
    refetch();
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      enableReinitialize
      onSubmit={handleSubmit}
    >
      {({
        values,
        handleReset,
        handleChange,
        setFieldValue,
        errors,
        touched,
        handleSubmit,
        handleBlur,
      }) => {
        const handleEditOff = () => {
          handleReset();
          setEditMode(false);
        };

        return (
          <Form>
            <Stack my={3} spacing={2} direction="row">
              {!editMode && (
                <RoundButton
                  icon={<Edit />}
                  onClick={() => setEditMode(true)}
                />
              )}
              {editMode && (
                <>
                  <RoundButton icon={<Save />} onClick={() => handleSubmit()} />
                  <RoundButton icon={<EditOff />} onClick={handleEditOff} />
                </>
              )}
            </Stack>
            <Grid container columns={4} spacing={2}>
              {taskCheck && (
                <Grid item xs={4}>
                  <Input
                    name={DetailFormFields.PREVIOUS_TITLE}
                    label={CheckLabelPreviousTitle}
                    value={values[DetailFormFields.PREVIOUS_TITLE]}
                    disabled
                    InputLabelProps={{
                      shrink: Boolean(values[DetailFormFields.PREVIOUS_TITLE]),
                    }}
                  />
                </Grid>
              )}

              {taskCheck ? (
                <Grid item xs={2}>
                  <Input
                    name={DetailFormFields.TITLE}
                    label={ChekLabelTitle}
                    value={values[DetailFormFields.TITLE] || ''}
                    disabled
                    InputLabelProps={{
                      shrink: Boolean(values[DetailFormFields.TITLE]),
                    }}
                    required
                  />
                </Grid>
              ) : (
                <Grid item xs={2}>
                  <Input
                    name={DetailFormFields.TITLE}
                    label={ChekLabelTitle}
                    value={values[DetailFormFields.TITLE] || ''}
                    disabled
                    InputLabelProps={{
                      shrink: Boolean(values[DetailFormFields.TITLE]),
                    }}
                    required
                  />
                </Grid>
              )}
              <Grid item xs={2}>
                <ClerkInput
                  name={DetailFormFields.AUTHOR}
                  label={detailFormLabels[DetailFormFields.AUTHOR]}
                  disabled={!editMode || data?.canEditAuthor == false}
                  required
                />
              </Grid>
              {taskCheck ? (
                <Grid item xs={2}>
                  <ClerkInput
                    name={DetailFormFields.PERFORMER}
                    label={detailFormLabels[DetailFormFields.PERFORMER]}
                    disabled={!editMode || data?.canEditPerformer == false}
                    required
                  />
                </Grid>
              ) : (
                <Grid item xs={4}>
                  <ClerkInput
                    name={DetailFormFields.PERFORMER}
                    label={detailFormLabels[DetailFormFields.PERFORMER]}
                    disabled={!editMode || data?.canEditPerformer == false}
                    required
                  />
                </Grid>
              )}
              {taskCheck && (
                <Grid item xs={2}>
                  <AutoCompleteInput
                    name={DetailFormFields.TODO_AFTER}
                    label={detailFormLabels[DetailFormFields.TODO_AFTER]}
                    options={intialtodoValue2}
                    disabled={!editMode || data?.canEditTaskRelations == false}
                  />
                </Grid>
              )}

              <Grid item xs={2}>
                <Input
                  type="number"
                  name={DetailFormFields.TERM}
                  label={detailFormLabels[DetailFormFields.TERM]}
                  value={values[DetailFormFields.TERM]}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  disabled={!editMode || data?.canEditDateTarget == false}
                  error={Boolean(errors[DetailFormFields.TERM])}
                  helperText={errors[DetailFormFields.TERM] as string}
                  InputLabelProps={{
                    shrink: Boolean(values[DetailFormFields.TERM]),
                  }}
                />
              </Grid>
              {requestCheck && (
                <Grid item xs={2}>
                  <DatePickerInput
                    label={detailFormLabels[DetailFormFields.PLAN_DATE]}
                    name={DetailFormFields.PLAN_DATE}
                    disabled={!editMode || data?.canEditDateTarget == false}
                  />
                </Grid>
              )}
              <Grid item xs={4}>
                <Input
                  name={DetailFormFields.COMMENTS}
                  label={detailFormLabels[DetailFormFields.COMMENTS]}
                  value={values[DetailFormFields.COMMENTS]}
                  onChange={handleChange}
                  multiline
                  disabled={!editMode || data?.canEditComments == false}
                  error={Boolean(errors[DetailFormFields.COMMENTS])}
                  helperText={errors[DetailFormFields.COMMENTS] as string}
                  InputLabelProps={{
                    shrink: Boolean(values[DetailFormFields.COMMENTS]),
                  }}
                />
              </Grid>
              <Grid item xs={4}>
                <Input
                  name={DetailFormFields.PERFORMER_COMMENT}
                  label={detailFormLabels[DetailFormFields.PERFORMER_COMMENT]}
                  value={values[DetailFormFields.PERFORMER_COMMENT]}
                  multiline
                  disabled
                  error={Boolean(errors[DetailFormFields.PERFORMER_COMMENT])}
                  helperText={
                    errors[DetailFormFields.PERFORMER_COMMENT] as string
                  }
                />
              </Grid>
              <Grid item xs={1}>
                <CheckboxInput
                  name={DetailFormFields.IS_CONTROL}
                  label={detailFormLabels[DetailFormFields.IS_CONTROL]}
                  disabled={!editMode || data?.canEditNotifyOnEnd == false}
                />
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

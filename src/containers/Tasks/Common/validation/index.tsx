import { boolean, date, number, object, string } from 'yup';

import { DetailFormFields } from '../../../../constants/commonTasks';

export const validationSchema = object({
  [DetailFormFields.PREVIOUS_TITLE]: string(),
  [DetailFormFields.TITLE]: string().required('Обязательное поле'),
  [DetailFormFields.AUTHOR]: string().required('Обязательное поле'),
  [DetailFormFields.PERFORMER]: string().required('Обязательное поле'),
  [DetailFormFields.TODO_AFTER]: string().nullable(),
  [DetailFormFields.TERM]: number().nullable(),
  [DetailFormFields.PLAN_DATE]: date().nullable(),
  [DetailFormFields.COMMENTS]: string().nullable(),
  [DetailFormFields.PERFORMER_COMMENT]: string().nullable(),
  [DetailFormFields.IS_CONTROL]: boolean().nullable(),
});

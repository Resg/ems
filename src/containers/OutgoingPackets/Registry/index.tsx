import React, { useCallback, useMemo, useState } from 'react';
import { format } from 'date-fns';
import { useParams } from 'react-router';

import { ChevronRight, Refresh } from '@mui/icons-material';
import { Box } from '@mui/material';

import { DataTable } from '../../../components/CustomDataGrid';
import { RoundButton } from '../../../components/RoundButton';
import {
  SettingsTypes,
  ToolButton,
  ToolsPanel,
} from '../../../components/ToolsPanel';
import { RegistryCols } from '../../../constants/commonOutgoingPackage';
import { useGetRegistryPacketsQuery } from '../../../services/api/document';

import styles from './styles.module.scss';

interface RegistryProps {}

export const Registry = () => {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});

  const { id } = useParams();

  const { data, refetch, isLoading } = useGetRegistryPacketsQuery(
    { id: Number(id), page, size: perPage },
    { skip: !id }
  );
  const [cols, setCols] = useState(RegistryCols);

  const rows = useMemo(() => {
    return (
      data?.data.map(({ id, number, dateTime, type }) => {
        return {
          id,
          number,
          dateTime:
            dateTime &&
            String(format(new Date(dateTime), 'dd.MM.yyyy HH:mm:ss')),
          type,
        };
      }) || []
    );
  }, [data?.data]);

  const selectedArr = useMemo(
    () => Object.keys(selected).filter((key) => selected[Number(key)]),
    [selected]
  );

  const handleDetails = useCallback(() => {
    window.open(`/registries/${selectedArr[0]}`, '_blank')?.focus();
  }, [selectedArr]);

  const handleRowDoubleClick = useCallback((e: { id: number }) => {
    if (e.id) {
      window.open(`/registries/${e.id}`, '_blank')?.focus();
    }
  }, []);

  return (
    <div className={styles.content}>
      <Box my={3}>
        <ToolsPanel
          leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
          settings={[SettingsTypes.COLS]}
          cols={cols}
        >
          <ToolButton
            label={'Подробно'}
            startIcon={<ChevronRight />}
            onClick={handleDetails}
            disabled={selectedArr.length !== 1}
            fast
          />
        </ToolsPanel>
      </Box>
      <DataTable
        formKey={'OutgoinPacketsRegistry'}
        rows={rows}
        cols={cols.filter((col) => !col.hidden)}
        onRowSelect={setSelected}
        loading={isLoading}
        height={'fullHeight'}
        onRowDoubleClick={handleRowDoubleClick}
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: data?.totalCount || 0,
        }}
      />
    </div>
  );
};

import React, { useMemo, useState } from 'react';
import { format } from 'date-fns';
import { useParams } from 'react-router';

import { Refresh } from '@mui/icons-material';
import { Box } from '@mui/material';

import { DataTable } from '../../../components/CustomDataGrid';
import { RoundButton } from '../../../components/RoundButton';
import { ToolsPanel } from '../../../components/ToolsPanel';
import { MailPackagesCols } from '../../../constants/commonOutgoingPackage';
import { useGetMailPackagesQuery } from '../../../services/api/dictionaries';

import styles from './styles.module.scss';

interface MailPackagesProps {}

export const MailPackages = () => {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});

  const { id } = useParams();

  const searchNum = useMemo(() => {
    return { packetId: id };
  }, [page]);

  const {
    data: mailData,
    refetch,
    isLoading,
  } = useGetMailPackagesQuery(
    {
      searchParameters: searchNum,
      page: page,
      size: perPage,
      useCount: true,
    },
    { skip: !id }
  );

  const rows = useMemo(() => {
    return (
      mailData?.data.map(
        ({
          id,
          deliveryDate,
          localNumber,
          returnDate,
          sendingDateTime,
          comment,
          deliveryName,
          notificationNumber,
          postRegistryNumber,
          postRegistryDateTime,
          sendingsCount,
        }) => {
          return {
            id,
            deliveryDate:
              deliveryDate && new Date(deliveryDate).toLocaleDateString(),
            localNumber,
            returnDate: returnDate && new Date(returnDate).toLocaleDateString(),
            sendingDateTime:
              sendingDateTime &&
              String(format(new Date(sendingDateTime), 'dd.MM.yyyy HH:mm:ss')),
            comment,
            deliveryName,
            notificationNumber,
            postRegistryNumber,
            postRegistryDateTime:
              postRegistryDateTime &&
              String(
                format(new Date(postRegistryDateTime), 'dd.MM.yyyy HH:mm:ss')
              ),
            sendingsCount,
          };
        }
      ) || []
    );
  }, [mailData?.data]);

  return (
    <div className={styles.content}>
      <Box my={3}>
        <ToolsPanel
          leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
        ></ToolsPanel>
      </Box>
      <DataTable
        formKey={'OutgoingPacketsMailPackages'}
        rows={rows}
        cols={MailPackagesCols}
        onRowSelect={setSelected}
        loading={isLoading}
        noSelect
        height={'fullHeight'}
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: mailData?.totalCount || 0,
        }}
      />
    </div>
  );
};

import { number, object, string } from 'yup';

import { OutgoingPackageFormFields } from '../../../../constants/commonOutgoingPackage';

export const validationSchema = object({
  [OutgoingPackageFormFields.INDEX]: string()
    .min(6, 'Допустимое значение: шесть цифр')
    .max(6, 'Допустимое значение: шесть цифр')
    .nullable(),
  [OutgoingPackageFormFields.EMAIL]: string()
    .email('Неверный E-mail')
    .nullable(),
  [OutgoingPackageFormFields.COUNT_PAGES]: number().nullable(),
});

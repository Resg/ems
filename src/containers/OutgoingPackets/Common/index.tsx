import React, { useCallback, useMemo, useState } from 'react';
import { Form, Formik } from 'formik';
import { get } from 'lodash';
import { useSelector } from 'react-redux';

import {
  ChevronRight,
  Edit,
  EditOff,
  OpenInNew,
  Save,
} from '@mui/icons-material';
import RefreshIcon from '@mui/icons-material/Refresh';
import SettingsIcon from '@mui/icons-material/Settings';
import { Button, Grid, Stack, Tab } from '@mui/material';

import { AutoCompleteInput } from '../../../components/AutoCompleteInput';
import { CheckboxInput } from '../../../components/CheckboxInput';
import { DataTable } from '../../../components/CustomDataGrid';
import { DateTimePickerInput } from '../../../components/DateTimePickerInput';
import { DebounceAutoComplete } from '../../../components/DebounceAutoComplete';
import Input from '../../../components/Input';
import { RoundButton } from '../../../components/RoundButton';
import {
  commonDetailDefaults,
  DocumentsTableCols,
  OutgoingPackageFormFields,
  OutgoingPackageFormLabels,
} from '../../../constants/commonOutgoingPackage';
import {
  useGetCounterpartyPersonsListMutation,
  useGetCounterpartyPersonsMutation,
  useGetListOfCounterpartyAdressesQuery,
  useGetListOfCounterpartyIndexQuery,
  useGetSendingMethodsQuery,
} from '../../../services/api/dictionaries';
import {
  useGetDocumentOutgoingPacketsQuery,
  useGetPacketsDocumentQuery,
  useUpdatePacketsDocumentMutation,
} from '../../../services/api/document';
import { RootState, useAppDispatch } from '../../../store';
import { setError } from '../../../store/utils';

import { validationSchema } from './validation';

import styles from './styles.module.scss';

export interface CommonProps {
  id: number;
}

export const Common = ({ id }: CommonProps) => {
  const [editMode, setEditMode] = useState(!id);
  const [page, setPage] = useState(1);
  const [size, setSize] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});

  const { data: sendingMethods = [] } = useGetSendingMethodsQuery({});
  const { data: tableData, isLoading } = useGetDocumentOutgoingPacketsQuery(
    { packetId: id, page, size },
    { skip: !id }
  );
  const { data: documentData, refetch: updateDocumentData } =
    useGetPacketsDocumentQuery({ id }, { skip: !id });

  const { data: indexData = [] } = useGetListOfCounterpartyIndexQuery(
    {
      id: documentData?.contractorId,
      addressTypeCode: documentData?.addressTypeCode,
      deliveryTypeCode: documentData?.deliveryTypeCode,
    },
    { skip: !documentData }
  );
  const { data: adressesData = [] } = useGetListOfCounterpartyAdressesQuery(
    {
      id: documentData?.contractorId,
      addressTypeCode: documentData?.addressTypeCode,
      deliveryTypeCode: documentData?.deliveryTypeCode,
    },
    { skip: !documentData }
  );
  const dispatch = useAppDispatch();

  const selectedArr = useMemo(
    () => Object.keys(selected).filter((key) => selected[Number(key)]),
    [selected]
  );

  const selectedDocuments = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const detailsId = useMemo(() => {
    return tableData?.data?.filter((obj) =>
      selectedDocuments.includes(obj.fileId as any)
    );
  }, [selected]);

  const [getPersons] = useGetCounterpartyPersonsMutation();
  const [getPersonsList] = useGetCounterpartyPersonsListMutation();
  const [update] = useUpdatePacketsDocumentMutation();

  const initialValues = useMemo(() => {
    return (
      {
        localNumber: documentData?.localNumber,
        status: documentData?.status,
        changeDateTime: documentData?.changeDateTime,
        contractorShortName: documentData?.contractorShortName,
        contractorFullName: documentData?.contractorFullName,
        deliveryType: documentData?.deliveryTypeCode,
        trustedPersonName: documentData?.trustedPersonName,
        recipientPersonId: documentData?.recipientPersonId,
        isEditedAddress: documentData?.isEditedAddress,
        index: documentData?.index,
        address: documentData?.address,
        email: documentData?.email,
        fax: documentData?.fax,
        sendingDateTime: documentData?.sendingDateTime,
        deliveryDateTime: documentData?.deliveryDateTime,
        countPages: documentData?.countPages,
        comments: documentData?.comments,
      } || commonDetailDefaults
    );
  }, [documentData]);

  const getOptionsPerson = useCallback(
    async (value: string) => {
      if (value) {
        const response = await getPersons({ fullName: value });
        if ('data' in response) {
          return response.data;
        }
        return [];
      }
      return [];
    },
    [getPersons]
  );

  const getValueOptionsPerson = useCallback(
    async (ids: number[]) => {
      if (ids && ids.length) {
        const counterparties = await getPersonsList({ ids });
        if ('data' in counterparties) {
          return counterparties.data || [];
        }
        return [];
      }
      return [];
    },
    [getPersonsList]
  );

  const rows = useMemo(() => {
    return (
      tableData?.data.map(
        ({
          id,
          orderNumber,
          toSendFile,
          localNumber,
          rubricList,
          number,
          requestNumber,
          name,
          fileName,
          contentFileId,
          fileId,
        }) => {
          return {
            id,
            orderNumber,
            toSendFile,
            localNumber,
            rubricList,
            number,
            requestNumber,
            name,
            fileName,
            contentFileId,
            fileId,
          };
        }
      ) || []
    );
  }, [tableData?.data]);

  const handleOpen = useCallback(() => {
    window.open(`/file_editor/${selectedArr[0]}`, '_blank');
  }, [selectedArr]);

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const handleRowDoubleClick = useCallback(
    (e: any) => {
      if (e.id) {
        window
          .open(`/documents?id=${e.id}`, openFormInNewTab ? '_blank' : '_self')
          ?.focus();
      }
    },
    [openFormInNewTab]
  );

  const handleDetails = useCallback(() => {
    if (detailsId) {
      window
        .open(
          `/documents?id=${detailsId[0].id}`,
          openFormInNewTab ? '_blank' : '_self'
        )
        ?.focus();
    }
  }, [detailsId, openFormInNewTab]);

  const handleSubmit = useCallback(
    async (values: any) => {
      const updateData = {
        id: id,
        deliveryTypeCode: values.deliveryType,
        recipientPersonId: values.recipientPersonId,
        index: values.index,
        address: values.address,
        email: values.email,
        fax: values.fax,
        comments: values.comments,
        recipientPersonFullName: documentData?.recipientPersonFullName,
        addressTypeCode: documentData?.addressTypeCode,
        name: documentData?.name,
        aoguid: documentData?.aoguid,
        deliveryDateTime: values.deliveryDateTime,
        sendingDateTime: documentData?.sendingDateTime,
        trustedPersonId: documentData?.trustedPersonId,
        countPages: values.countPages,
        document: tableData?.data?.map((el) => {
          return {
            id: el.id,
            fileId: el.fileId,
            orderNumber: el.orderNumber,
            countCopies: el.countCopies,
            toSendFile: el.toSendFile,
          };
        }),
      };

      await update({ id: id, data: updateData });
      setEditMode(false);
      updateDocumentData();
    },
    [documentData, update, tableData]
  );

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      enableReinitialize
      onSubmit={handleSubmit}
    >
      {({
        values,
        handleReset,
        handleChange,
        setFieldValue,
        errors,
        touched,
        handleSubmit,
        setTouched,
      }) => {
        const handleEditOff = () => {
          handleReset();
          setEditMode(false);
        };
        return (
          <Form>
            <Stack my={3} spacing={2} direction="row">
              {!editMode && (
                <RoundButton
                  icon={<Edit />}
                  onClick={() => {
                    if (documentData?.canEdit) {
                      setEditMode(true);
                    } else {
                      dispatch(
                        setError({ message: 'Редактирование запрещено' })
                      );
                    }
                  }}
                />
              )}
              {editMode && (
                <>
                  <RoundButton icon={<Save />} onClick={() => handleSubmit()} />
                  <RoundButton icon={<EditOff />} onClick={handleEditOff} />
                </>
              )}
            </Stack>
            <Grid container rowSpacing={2} columnSpacing={12}>
              <Grid item xs={4}>
                <Input
                  name={OutgoingPackageFormFields.NUMBER}
                  label={
                    OutgoingPackageFormLabels[OutgoingPackageFormFields.NUMBER]
                  }
                  value={values[OutgoingPackageFormFields.NUMBER]}
                  InputLabelProps={{
                    shrink: Boolean(values[OutgoingPackageFormFields.NUMBER]),
                  }}
                  disabled
                />
              </Grid>
              <Grid item xs={4}>
                <Input
                  name={OutgoingPackageFormFields.STATUS}
                  label={
                    OutgoingPackageFormLabels[OutgoingPackageFormFields.STATUS]
                  }
                  value={values[OutgoingPackageFormFields.STATUS]}
                  InputLabelProps={{
                    shrink: Boolean(values[OutgoingPackageFormFields.STATUS]),
                  }}
                  disabled
                />
              </Grid>

              <Grid item xs={4}>
                <DateTimePickerInput
                  name={OutgoingPackageFormFields.CHANGE_DATE}
                  label={
                    OutgoingPackageFormLabels[
                      OutgoingPackageFormFields.CHANGE_DATE
                    ]
                  }
                  disabled
                />
              </Grid>
              <Grid item xs={4}>
                <Input
                  name={OutgoingPackageFormFields.CONTRACTOR_SHORT_NAME}
                  label={
                    OutgoingPackageFormLabels[
                      OutgoingPackageFormFields.CONTRACTOR_SHORT_NAME
                    ]
                  }
                  value={
                    values[OutgoingPackageFormFields.CONTRACTOR_SHORT_NAME]
                  }
                  InputLabelProps={{
                    shrink: Boolean(
                      values[OutgoingPackageFormFields.CONTRACTOR_SHORT_NAME]
                    ),
                  }}
                  disabled
                />
              </Grid>
              <Grid item xs={8}>
                <Input
                  name={OutgoingPackageFormFields.CONTRACTOR_FULL_NAME}
                  label={
                    OutgoingPackageFormLabels[
                      OutgoingPackageFormFields.CONTRACTOR_FULL_NAME
                    ]
                  }
                  value={values[OutgoingPackageFormFields.CONTRACTOR_FULL_NAME]}
                  InputLabelProps={{
                    shrink: Boolean(
                      values[OutgoingPackageFormFields.CONTRACTOR_FULL_NAME]
                    ),
                  }}
                  disabled
                />
              </Grid>
              {documentData?.deliveryTypeCode === 'LHAND' ? (
                <Grid item xs={6}>
                  <AutoCompleteInput
                    name={OutgoingPackageFormFields.DELIVERY_TYPE}
                    label={
                      OutgoingPackageFormLabels[
                        OutgoingPackageFormFields.DELIVERY_TYPE
                      ]
                    }
                    options={sendingMethods}
                    disabled={!editMode}
                  />
                </Grid>
              ) : (
                <Grid item xs={12}>
                  <AutoCompleteInput
                    name={OutgoingPackageFormFields.DELIVERY_TYPE}
                    label={
                      OutgoingPackageFormLabels[
                        OutgoingPackageFormFields.DELIVERY_TYPE
                      ]
                    }
                    options={sendingMethods}
                    disabled={!editMode}
                  />
                </Grid>
              )}
              {documentData?.deliveryTypeCode === 'LHAND' && (
                <Grid item xs={6}>
                  <Input
                    name={OutgoingPackageFormFields.TRUSTED_PERSON_NAME}
                    label={
                      OutgoingPackageFormLabels[
                        OutgoingPackageFormFields.TRUSTED_PERSON_NAME
                      ]
                    }
                    value={
                      values[OutgoingPackageFormFields.TRUSTED_PERSON_NAME] ||
                      ''
                    }
                    InputLabelProps={{
                      shrink: Boolean(
                        values[OutgoingPackageFormFields.TRUSTED_PERSON_NAME]
                      ),
                    }}
                    onChange={handleChange}
                    disabled
                  />
                </Grid>
              )}
              <Grid item xs={12}>
                <DebounceAutoComplete
                  name={OutgoingPackageFormFields.RECIPIENT_PERSON}
                  label={
                    OutgoingPackageFormLabels[
                      OutgoingPackageFormFields.RECIPIENT_PERSON
                    ]
                  }
                  getOptions={getOptionsPerson}
                  getValueOptions={getValueOptionsPerson}
                  disabled={!editMode}
                />
              </Grid>
              <Grid item xs={12} className={styles.fieldsHeader}>
                адрес отправки
              </Grid>
              <Grid item xs={12}>
                <CheckboxInput
                  name={OutgoingPackageFormFields.IS_EDITED_ADDRESS}
                  label={
                    OutgoingPackageFormLabels[
                      OutgoingPackageFormFields.IS_EDITED_ADDRESS
                    ]
                  }
                  disabled={!editMode}
                />
              </Grid>
              {values.isEditedAddress ? (
                <Grid item xs={6}>
                  <Input
                    name={OutgoingPackageFormFields.INDEX}
                    label={
                      OutgoingPackageFormLabels[OutgoingPackageFormFields.INDEX]
                    }
                    value={values[OutgoingPackageFormFields.INDEX] || ''}
                    InputLabelProps={{
                      shrink: Boolean(values[OutgoingPackageFormFields.INDEX]),
                    }}
                    disabled={!editMode}
                    onChange={handleChange}
                    error={Boolean(
                      errors[OutgoingPackageFormFields.INDEX] &&
                        touched[OutgoingPackageFormFields.INDEX]
                    )}
                    onBlur={() =>
                      setTouched({ [OutgoingPackageFormFields.INDEX]: true })
                    }
                    helperText={errors[OutgoingPackageFormFields.INDEX]}
                  />
                </Grid>
              ) : (
                <Grid item xs={6}>
                  <AutoCompleteInput
                    name={OutgoingPackageFormFields.INDEX}
                    label={
                      OutgoingPackageFormLabels[OutgoingPackageFormFields.INDEX]
                    }
                    options={indexData}
                    disabled={!editMode}
                  />
                </Grid>
              )}
              {values.isEditedAddress ? (
                <Grid item xs={6}>
                  <Input
                    name={OutgoingPackageFormFields.ADDRESS}
                    label={
                      OutgoingPackageFormLabels[
                        OutgoingPackageFormFields.ADDRESS
                      ]
                    }
                    value={values[OutgoingPackageFormFields.ADDRESS] || ''}
                    InputLabelProps={{
                      shrink: Boolean(
                        values[OutgoingPackageFormFields.ADDRESS]
                      ),
                    }}
                    disabled={!editMode}
                    onChange={handleChange}
                  />
                </Grid>
              ) : (
                <Grid item xs={6}>
                  <AutoCompleteInput
                    name={OutgoingPackageFormFields.ADDRESS}
                    label={
                      OutgoingPackageFormLabels[
                        OutgoingPackageFormFields.ADDRESS
                      ]
                    }
                    options={adressesData}
                    disabled={!editMode}
                  />
                </Grid>
              )}
              <Grid item xs={6}>
                <Input
                  name={OutgoingPackageFormFields.EMAIL}
                  label={
                    OutgoingPackageFormLabels[OutgoingPackageFormFields.EMAIL]
                  }
                  value={values[OutgoingPackageFormFields.EMAIL] || ''}
                  InputLabelProps={{
                    shrink: Boolean(values[OutgoingPackageFormFields.EMAIL]),
                  }}
                  disabled={!editMode || values.isEditedAddress === false}
                  onChange={handleChange}
                  error={Boolean(
                    errors[OutgoingPackageFormFields.EMAIL] &&
                      touched[OutgoingPackageFormFields.EMAIL]
                  )}
                  onBlur={() =>
                    setTouched({ [OutgoingPackageFormFields.EMAIL]: true })
                  }
                  helperText={errors[OutgoingPackageFormFields.EMAIL]}
                />
              </Grid>
              <Grid item xs={6}>
                <Input
                  name={OutgoingPackageFormFields.FAX}
                  label={
                    OutgoingPackageFormLabels[OutgoingPackageFormFields.FAX]
                  }
                  value={values[OutgoingPackageFormFields.FAX] || ''}
                  InputLabelProps={{
                    shrink: Boolean(values[OutgoingPackageFormFields.FAX]),
                  }}
                  disabled={
                    !(
                      editMode &&
                      get(values, OutgoingPackageFormFields.IS_EDITED_ADDRESS)
                    )
                  }
                  onChange={handleChange}
                />
              </Grid>
              <Grid item xs={4}>
                <DateTimePickerInput
                  name={OutgoingPackageFormFields.SENDING_DATE_TIME}
                  label={
                    OutgoingPackageFormLabels[
                      OutgoingPackageFormFields.SENDING_DATE_TIME
                    ]
                  }
                  disabled
                />
              </Grid>
              <Grid item xs={4}>
                <DateTimePickerInput
                  name={OutgoingPackageFormFields.DELIVERY_DATE_TIME}
                  label={
                    OutgoingPackageFormLabels[
                      OutgoingPackageFormFields.DELIVERY_DATE_TIME
                    ]
                  }
                  disabled={!editMode}
                />
              </Grid>
              <Grid item xs={4}>
                <Input
                  name={OutgoingPackageFormFields.COUNT_PAGES}
                  label={
                    OutgoingPackageFormLabels[
                      OutgoingPackageFormFields.COUNT_PAGES
                    ]
                  }
                  value={values[OutgoingPackageFormFields.COUNT_PAGES] || ''}
                  InputLabelProps={{
                    shrink: Boolean(
                      values[OutgoingPackageFormFields.COUNT_PAGES]
                    ),
                  }}
                  disabled={!editMode}
                  onChange={handleChange}
                  error={Boolean(
                    errors[OutgoingPackageFormFields.COUNT_PAGES] &&
                      touched[OutgoingPackageFormFields.COUNT_PAGES]
                  )}
                  onBlur={() =>
                    setTouched({
                      [OutgoingPackageFormFields.COUNT_PAGES]: true,
                    })
                  }
                  helperText={errors[OutgoingPackageFormFields.COUNT_PAGES]}
                />
              </Grid>
              <Grid item>
                <Tab label={'Документы'} />
              </Grid>
              <Grid
                item
                container
                direction="row"
                justifyContent="space-between"
              >
                <Stack direction="row" spacing={2}>
                  <RoundButton icon={<RefreshIcon />} />
                </Stack>
                <Stack direction="row" spacing={2}>
                  <Button
                    variant="outlined"
                    endIcon={<ChevronRight />}
                    onClick={handleDetails}
                    disabled={selectedArr.length !== 1}
                  >
                    подробно
                  </Button>
                  <Button
                    variant="outlined"
                    startIcon={<OpenInNew />}
                    disabled={selectedArr.length !== 1}
                    onClick={handleOpen}
                  >
                    открыть файл
                  </Button>
                  <RoundButton icon={<SettingsIcon />} />
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <DataTable
                  formKey={'OutgoingPakcetsCommon'}
                  cols={DocumentsTableCols}
                  rows={rows}
                  onRowSelect={setSelected}
                  onRowDoubleClick={handleRowDoubleClick}
                  loading={isLoading}
                  pagerProps={{
                    page: page,
                    setPage: setPage,
                    perPage: size,
                    setPerPage: setSize,
                    total: tableData?.totalCount || 0,
                  }}
                  selectFieldName={'fileId'}
                  height={'auto'}
                  className={styles.table}
                />
              </Grid>
              <Grid item xs={12}>
                <Input
                  name={OutgoingPackageFormFields.COMMENTS}
                  label={
                    OutgoingPackageFormLabels[
                      OutgoingPackageFormFields.COMMENTS
                    ]
                  }
                  value={values[OutgoingPackageFormFields.COMMENTS] || ''}
                  InputLabelProps={{
                    shrink: Boolean(values[OutgoingPackageFormFields.COMMENTS]),
                  }}
                  multiline
                  disabled={!editMode}
                  onChange={handleChange}
                />
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

import { Grid } from '@mui/material';

import { ListInput } from '../../../../components/ListInput';
import { RegistrySearchFields } from '../../../../constants/registrySearch';

export const Numbers = () => {
  return (
    <Grid container direction="column" spacing={1} sx={{ mb: 2 }}>
      <Grid item>Список номеров реестров</Grid>
      <Grid item>
        <ListInput
          name={RegistrySearchFields.NUMBERS}
          separator="line-break"
          multiline
          rows={15}
        />
      </Grid>
    </Grid>
  );
};

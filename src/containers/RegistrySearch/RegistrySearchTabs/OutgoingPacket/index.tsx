import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { Grid } from '@mui/material';

import { DateRange } from '../../../../components/DateRange';
import Input from '../../../../components/Input';
import {
  RegistrySearchFields,
  RegistrySearchLabels,
} from '../../../../constants/registrySearch';

export const OutgoingPacket = () => {
  const { handleChange, values = {} as any } = useFormikContext();

  return (
    <Grid container spacing={2}>
      <Grid item xs={6}>
        <Input
          name={RegistrySearchFields.OUTGOING_PACKET_NUMBER}
          label={
            RegistrySearchLabels[RegistrySearchFields.OUTGOING_PACKET_NUMBER]
          }
          value={get(values, RegistrySearchFields.OUTGOING_PACKET_NUMBER) || ''}
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={6}>
        <DateRange
          nameFrom={RegistrySearchFields.OUTGOING_PACKET_DATE_FROM}
          nameBefore={RegistrySearchFields.OUTGOING_PACKET_DATE_TO}
          labelFrom={
            RegistrySearchLabels[RegistrySearchFields.OUTGOING_PACKET_DATE_FROM]
          }
        />
      </Grid>
    </Grid>
  );
};

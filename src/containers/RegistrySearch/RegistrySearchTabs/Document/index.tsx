import { useEffect } from 'react';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { Grid } from '@mui/material';

import { AutoCompleteInput } from '../../../../components/AutoCompleteInput';
import { DateRange } from '../../../../components/DateRange';
import Input from '../../../../components/Input';
import {
  RegistrySearchFields,
  RegistrySearchLabels,
} from '../../../../constants/registrySearch';
import {
  useGetDocumentRubricsOptionsQuery,
  useGetDocumentTypesQuery,
} from '../../../../services/api/dictionaries';

export const Document = () => {
  const {
    handleChange,
    values = {} as any,
    setFieldValue,
  } = useFormikContext();

  const documentTypeId = get(values, RegistrySearchFields.DOCUMENT_TYPE_IDS);

  const { data: typeOptions = [] } = useGetDocumentTypesQuery({
    includeIrrelevant: true,
  });
  const { data: rubricOptions = [] } = useGetDocumentRubricsOptionsQuery({
    documentTypeId,
  });

  useEffect(() => {
    if (![21, 22].includes(documentTypeId)) {
      setFieldValue(RegistrySearchFields.DOCUMENT_CREATE_DATE_FROM, '');
      setFieldValue(RegistrySearchFields.DOCUMENT_CREATE_DATE_TO, '');
    }
  }, [documentTypeId]);

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <AutoCompleteInput
          name={RegistrySearchFields.DOCUMENT_TYPE_IDS}
          label={RegistrySearchLabels[RegistrySearchFields.DOCUMENT_TYPE_IDS]}
          options={typeOptions}
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          name={RegistrySearchFields.DOCUMENT_NUMBER}
          label={RegistrySearchLabels[RegistrySearchFields.DOCUMENT_NUMBER]}
          value={get(values, RegistrySearchFields.DOCUMENT_NUMBER) || ''}
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          name={RegistrySearchFields.DOCUMENT_NUMBER_LOCAL}
          label={
            RegistrySearchLabels[RegistrySearchFields.DOCUMENT_NUMBER_LOCAL]
          }
          value={get(values, RegistrySearchFields.DOCUMENT_NUMBER_LOCAL) || ''}
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={6}>
        <DateRange
          nameFrom={RegistrySearchFields.DOCUMENT_DATE_FROM}
          nameBefore={RegistrySearchFields.DOCUMENT_DATE_TO}
          labelFrom={
            RegistrySearchLabels[RegistrySearchFields.DOCUMENT_DATE_FROM]
          }
        />
      </Grid>
      <Grid item xs={6}>
        <DateRange
          nameFrom={RegistrySearchFields.DOCUMENT_CREATE_DATE_FROM}
          nameBefore={RegistrySearchFields.DOCUMENT_CREATE_DATE_TO}
          labelFrom={
            RegistrySearchLabels[RegistrySearchFields.DOCUMENT_CREATE_DATE_FROM]
          }
          disabled={![21, 22].includes(documentTypeId)}
        />
      </Grid>
      <Grid item xs={12}>
        <AutoCompleteInput
          name={RegistrySearchFields.DOCUMENT_RUBRIC_IDS}
          label={RegistrySearchLabels[RegistrySearchFields.DOCUMENT_RUBRIC_IDS]}
          options={rubricOptions}
          multiple
        />
      </Grid>
    </Grid>
  );
};

import React, { useEffect, useMemo } from 'react';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { Checkbox, FormControlLabel, Grid } from '@mui/material';

import { AutoCompleteInput } from '../../../../components/AutoCompleteInput';
import { CheckboxInput } from '../../../../components/CheckboxInput';
import { ClerkInput } from '../../../../components/ClerkInput';
import { DateRange } from '../../../../components/DateRange';
import Input from '../../../../components/Input';
import {
  RegistrySearchFields,
  RegistrySearchLabels,
} from '../../../../constants/registrySearch';
import {
  useGetDivisionsQuery,
  useGetInternalRegistryStatesQuery,
  useGetRegistryPacketNamesQuery,
} from '../../../../services/api/dictionaries';

interface ParametersType {
  viewCode: string;
  isDocumentRegistry?: boolean;
  isOutgoingPacketRegistry?: boolean;
  stateCode: string[];
}

interface RegistryProps {
  params: ParametersType;
}

export const Registry: React.FC<RegistryProps> = ({ params }) => {
  const {
    handleChange,
    setFieldValue,
    values = {} as any,
  } = useFormikContext();

  const { data: packetRegistryNames = [] } = useGetRegistryPacketNamesQuery({
    type: 'INNER',
  });
  const { data: divisions } = useGetDivisionsQuery({});
  const { data: states = [] } = useGetInternalRegistryStatesQuery({});

  const isArchive = useMemo(
    () =>
      get(values, RegistrySearchFields.ARCHIVE) ||
      params.viewCode === 'ARCHIVE',
    [values, params]
  );

  const isDocument = useMemo(
    () =>
      get(values, RegistrySearchFields.IS_DOCUMENT_REGISTRY) ||
      get(values, RegistrySearchFields.IS_DOCUMENT_REGISTRY) === false
        ? get(values, RegistrySearchFields.IS_DOCUMENT_REGISTRY)
        : params.isDocumentRegistry,
    [isArchive, values]
  );
  const isPacket = useMemo(
    () =>
      get(values, RegistrySearchFields.IS_OUTGOING_PACKET_REGISTRY) ||
      get(values, RegistrySearchFields.IS_OUTGOING_PACKET_REGISTRY) === false
        ? get(values, RegistrySearchFields.IS_OUTGOING_PACKET_REGISTRY)
        : params.isOutgoingPacketRegistry,
    [isArchive, values]
  );
  const statesCodes = useMemo(
    () => states.filter((state) => params.stateCode.includes(state.value)),
    [states, params]
  );

  useEffect(() => {
    setFieldValue(RegistrySearchFields.ARCHIVE, isArchive);
    setFieldValue(RegistrySearchFields.IS_DOCUMENT_REGISTRY, isDocument);
    setFieldValue(RegistrySearchFields.IS_OUTGOING_PACKET_REGISTRY, isPacket);
  }, [isArchive, isDocument, isPacket]);

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Input
          name={RegistrySearchFields.NUMBER}
          label={RegistrySearchLabels[RegistrySearchFields.NUMBER]}
          value={get(values, RegistrySearchFields.NUMBER) || ''}
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={4}>
        <FormControlLabel
          label={RegistrySearchLabels[RegistrySearchFields.ARCHIVE]}
          control={
            <Checkbox
              name={RegistrySearchFields.ARCHIVE}
              checked={isArchive}
              onChange={() =>
                setFieldValue(RegistrySearchFields.ARCHIVE, !isArchive)
              }
              disabled={params.viewCode === 'ARCHIVE'}
            />
          }
        />
      </Grid>
      <Grid item xs={8}>
        <CheckboxInput
          name={RegistrySearchFields.IS_MY_REGISTRY}
          label={RegistrySearchLabels[RegistrySearchFields.IS_MY_REGISTRY]}
          disabled={params.viewCode !== 'ARCHIVE'}
        />
      </Grid>
      <Grid item xs={12}>
        Объект передачи:
      </Grid>
      <Grid item xs={4}>
        <FormControlLabel
          label={
            RegistrySearchLabels[RegistrySearchFields.IS_DOCUMENT_REGISTRY]
          }
          control={
            <Checkbox
              name={RegistrySearchFields.IS_DOCUMENT_REGISTRY}
              checked={isDocument}
              onChange={() =>
                setFieldValue(
                  RegistrySearchFields.IS_DOCUMENT_REGISTRY,
                  !isDocument
                )
              }
              disabled={params.viewCode !== 'ARCHIVE'}
            />
          }
        />
      </Grid>
      <Grid item xs={8}>
        <FormControlLabel
          label={
            RegistrySearchLabels[
              RegistrySearchFields.IS_OUTGOING_PACKET_REGISTRY
            ]
          }
          control={
            <Checkbox
              name={RegistrySearchFields.IS_OUTGOING_PACKET_REGISTRY}
              checked={isPacket}
              onChange={() =>
                setFieldValue(
                  RegistrySearchFields.IS_OUTGOING_PACKET_REGISTRY,
                  !isPacket
                )
              }
              disabled={params.viewCode !== 'ARCHIVE'}
            />
          }
        />
      </Grid>
      <Grid item xs={12}>
        <AutoCompleteInput
          name={RegistrySearchFields.STATES}
          label={RegistrySearchLabels[RegistrySearchFields.STATES]}
          options={statesCodes}
          initialValue={statesCodes}
          multiple
          disabled
        />
      </Grid>
      <Grid item xs={12}>
        <AutoCompleteInput
          name={RegistrySearchFields.NAME}
          label={RegistrySearchLabels[RegistrySearchFields.NAME]}
          options={packetRegistryNames}
        />
      </Grid>
      <Grid item xs={4}>
        <DateRange
          nameFrom={RegistrySearchFields.DATE_FROM}
          nameBefore={RegistrySearchFields.DATE_TO}
          labelFrom={RegistrySearchLabels[RegistrySearchFields.DATE_FROM]}
        />
      </Grid>
      <Grid item xs={4}>
        <DateRange
          nameFrom={RegistrySearchFields.SEND_DATE_FROM}
          nameBefore={RegistrySearchFields.SEND_DATE_TO}
          labelFrom={RegistrySearchLabels[RegistrySearchFields.SEND_DATE_FROM]}
        />
      </Grid>
      <Grid item xs={4}>
        <DateRange
          nameFrom={RegistrySearchFields.DELIVERY_DATE_FROM}
          nameBefore={RegistrySearchFields.DELIVERY_DATE_TO}
          labelFrom={
            RegistrySearchLabels[RegistrySearchFields.DELIVERY_DATE_FROM]
          }
        />
      </Grid>
      <Grid item xs={12}>
        <AutoCompleteInput
          name={RegistrySearchFields.AUTHOR_DIVISIONS}
          label={RegistrySearchLabels[RegistrySearchFields.AUTHOR_DIVISIONS]}
          options={
            divisions?.map((division) => {
              return { value: division.value, label: division.label };
            }) || []
          }
          multiple
        />
      </Grid>
      <Grid item xs={12}>
        <ClerkInput
          name={RegistrySearchFields.AUTHOR_CLERKS}
          label={RegistrySearchLabels[RegistrySearchFields.AUTHOR_CLERKS]}
          multiple
        />
      </Grid>
      <Grid item xs={12}>
        <AutoCompleteInput
          name={RegistrySearchFields.TARGET_DIVISIONS}
          label={RegistrySearchLabels[RegistrySearchFields.TARGET_DIVISIONS]}
          options={
            divisions?.map((division) => {
              return { value: division.value, label: division.label };
            }) || []
          }
          multiple
        />
      </Grid>
      <Grid item xs={12}>
        <ClerkInput
          name={RegistrySearchFields.TARGET_CLERKS}
          label={RegistrySearchLabels[RegistrySearchFields.TARGET_CLERKS]}
          multiple
        />
      </Grid>
      <Grid item xs={12}>
        <Input
          name={RegistrySearchFields.COMMENT}
          label={RegistrySearchLabels[RegistrySearchFields.COMMENT]}
          value={get(values, RegistrySearchFields.COMMENT) || ''}
          onChange={handleChange}
          multiline
          rows={3}
        />
      </Grid>
    </Grid>
  );
};

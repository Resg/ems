import { useCallback, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';

import {
  Add,
  ChevronRight,
  DeleteOutline,
  Refresh,
  Undo,
} from '@mui/icons-material';

import { DataTable } from '../../../components/CustomDataGrid/DataTable';
import { RoundButton } from '../../../components/RoundButton';
import { ToolButton, ToolsPanel } from '../../../components/ToolsPanel';
import { DeleteToolButton } from '../../../components/ToolsPanel/DeleteToolButton';
import { RegistryDocumentsCols } from '../../../constants/registries';
import { Routes } from '../../../constants/routes';
import {
  useDeleteRegistryDocumentsMutation,
  useGetRegistryDocumentsQuery,
  useIncludeRegistryDocumentMutation,
  useReturnRegistryDocumentMutation,
} from '../../../services/api/registries';
import { RootState } from '../../../store';
import { Registry } from '../../../types/registries';
import { RegistryDocumentsToRowAdapter } from '../../../utils/registry';
import BoundsModal from '../../documents/bookmarks/Bonds/BoundsList/BoundsModal';

import styles from './styles.module.scss';

interface ConclusionsProps {
  id: number;
  dataRegistry?: Registry;
}

export const RegistryDocuments: React.FC<ConclusionsProps> = ({
  id,
  dataRegistry,
}) => {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const [openModal, setOpenModal] = useState(false);

  const { data, refetch, isLoading } = useGetRegistryDocumentsQuery({
    id,
    page,
    size: perPage,
  });

  const [deleteRegistryDoc] = useDeleteRegistryDocumentsMutation();
  const [returnDocument] = useReturnRegistryDocumentMutation();
  const [includeDocument] = useIncludeRegistryDocumentMutation();

  const stateCode = useMemo(() => {
    return dataRegistry?.stateCode;
  }, [dataRegistry]);

  const rows = useMemo(() => {
    return RegistryDocumentsToRowAdapter(data?.data);
  }, [data?.data]);

  const selectedDocuments = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const dataElement = useMemo(() => {
    return data?.data?.find(
      (copy: { copyId: number }) => copy.copyId === selectedDocuments[0]
    );
  }, [selected]);

  const dataReturnElements = useMemo(() => {
    return data?.data
      ?.filter((obj) => selectedDocuments.includes(obj.copyId))
      .map((obj) => ({ id: obj.id, copyId: obj.copyId }));
  }, [selected]);

  const handleIncludeDocument = useCallback(
    async (selectedIds: number[]) => {
      includeDocument({
        registryId: id,
        ids: selectedIds,
      }).then((response) => {
        if (!('error' in response)) {
          setOpenModal(false);
          refetch();
        }
      });
    },
    [selectedDocuments]
  );

  const handleReturnDocument = useCallback(async () => {
    await returnDocument({
      registryId: dataElement?.registryId,
      document: dataReturnElements,
    });
    refetch();
  }, [selectedDocuments]);

  const detailsId = useMemo(() => {
    return data?.data?.filter((obj) => selectedDocuments.includes(obj.copyId));
  }, [selected]);

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const handleDetails = useCallback(() => {
    if (detailsId) {
      window
        .open(
          Routes.DOCUMENTS.DOCUMENT.replace(':id', detailsId[0].id),
          openFormInNewTab ? '_blank' : '_self'
        )
        ?.focus();
    }
  }, [detailsId, openFormInNewTab]);

  const handleDelete = useCallback(async () => {
    if (dataElement) {
      await deleteRegistryDoc({
        id: dataElement?.id,
        registryId: dataElement?.registryId,
        copyId: dataElement?.copyId,
      });
      refetch();
    }
  }, [dataElement]);

  const rowDoubleHandler = useCallback(
    (e: any) => {
      if (e.id) {
        window
          .open(
            Routes.DOCUMENTS.DOCUMENT.replace(':id', e.id),
            openFormInNewTab ? '_blank' : '_self'
          )
          ?.focus();
      }
    },
    [openFormInNewTab]
  );

  return (
    <>
      <DataTable
        formKey={'registry_documents'}
        cols={RegistryDocumentsCols}
        rows={rows}
        onRowSelect={setSelected}
        onRowDoubleClick={rowDoubleHandler}
        loading={isLoading}
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: data?.totalCount || 0,
        }}
        className={styles.table}
        selectFieldName={'copyId'}
      >
        <ToolsPanel
          className={styles.toolBar}
          leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
        >
          <ToolButton
            label="Подробно"
            startIcon={<ChevronRight />}
            disabled={selectedDocuments.length !== 1}
            onClick={() => handleDetails()}
            fast
          />
          {(stateCode === 'N' || stateCode === 'V') && (
            <>
              <ToolButton
                label={'Добавить'}
                startIcon={<Add />}
                onClick={(e: React.MouseEvent) => {
                  e.stopPropagation();
                  setOpenModal(true);
                }}
                fast
              />
              <BoundsModal
                onClose={() => setOpenModal(false)}
                open={openModal}
                docId={id}
                refethTable={refetch}
                includeDoc={handleIncludeDocument}
              />
            </>
          )}
          {(stateCode === 'N' || stateCode === 'V') && (
            <DeleteToolButton
              label={'Удалить'}
              startIcon={<DeleteOutline />}
              disabled={selectedDocuments.length !== 1}
              fast
              description={`Вы действительно хотите удалить из реестра документ № ${
                data?.data?.find(
                  (conclusion: { id: number }) =>
                    conclusion.id === dataElement?.id
                )?.number
              }?`}
              title={'Удаление документов из реестра'}
              onConfirm={handleDelete}
            />
          )}
          {stateCode === 'S' && (
            <ToolButton
              label={'Вернуть'}
              startIcon={<Undo />}
              disabled={!selectedDocuments.length}
              onClick={() => handleReturnDocument()}
              fast
            ></ToolButton>
          )}
        </ToolsPanel>
      </DataTable>
    </>
  );
};

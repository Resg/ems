import React, { useCallback, useMemo, useState } from 'react';
import { Form, Formik } from 'formik';
import { number, object, string } from 'yup';

import { Edit, EditOff, Save } from '@mui/icons-material';
import { Grid, Stack } from '@mui/material';

import { AutoCompleteInput } from '../../../components/AutoCompleteInput';
import { DatePickerInput } from '../../../components/DatePickerInput';
import Input from '../../../components/Input';
import { RoundButton } from '../../../components/RoundButton';
import {
  DefaultRegistry,
  RegistryFields,
  RegistryLabels,
} from '../../../constants/registry';
import { useGetInnerAddressesQuery } from '../../../services/api/dictionaries';
import { useSaveRegistryMutation } from '../../../services/api/registries';
import { Registry } from '../../../types/registries';

import styles from './styles.module.scss';

interface CommonProps {
  data?: Registry;
}

const validationSchema = object({
  [RegistryFields.NUMBER]: string().required('Обязательное поле'),
  [RegistryFields.SENDER_DIVISION]: number().required('Обязательное поле'),
  [RegistryFields.RECIPIENT_DIVISION]: number().required('Обязательное поле'),
});

export const Common: React.FC<CommonProps> = ({ data }) => {
  const canEdit = useMemo(() => {
    return data && ['N', 'C', 'V'].includes(data?.stateCode);
  }, [data]);

  const getNumId = useCallback((value: string) => {
    return Number(value.split('_')[0]);
  }, []);

  const [editMode, setEditMode] = useState(false);
  const { data: innerAddresses } = useGetInnerAddressesQuery({});
  const [saveRegistry] = useSaveRegistryMutation();

  const initialValues = useMemo(() => {
    return {
      ...(data || DefaultRegistry),
      [RegistryFields.SENDER_DIVISION]:
        data?.senderDivisionId || data?.senderId,
      [RegistryFields.RECIPIENT_DIVISION]:
        data?.recipientDivisionId || data?.recipientId,
    };
  }, [data]);

  const innerAddressesOptions = useMemo(() => {
    return (
      innerAddresses?.data?.map((option) => ({
        label: option.name,
        value: getNumId(option.id),
      })) || []
    );
  }, [innerAddresses]);

  const handleSubmit = useCallback(
    async (values = {} as any) => {
      const sender = innerAddresses?.data?.find(
        (address) =>
          getNumId(address.id) === values[RegistryFields.SENDER_DIVISION]
      );
      const recipient = innerAddresses?.data?.find(
        (address) =>
          getNumId(address.id) === values[RegistryFields.RECIPIENT_DIVISION]
      );
      const sendData = data && {
        id: data.id,
        date: data.date,
        number: values.number,
        senderDivisionId:
          sender && sender.typeCode === 'DIVISION'
            ? getNumId(sender.id)
            : undefined,
        senderId:
          sender && sender.typeCode === 'CLERK'
            ? getNumId(sender.id)
            : undefined,
        recipientDivisionId:
          recipient && recipient.typeCode === 'DIVISION'
            ? getNumId(recipient.id)
            : undefined,
        recipientId:
          recipient && recipient.typeCode === 'CLERK'
            ? getNumId(recipient.id)
            : undefined,
        typeCode: data.typeCode,
        templateCode: data.templateCode,
        comments: values.comments,
        nameId: data.nameId,
      };

      if (sendData) {
        await saveRegistry(sendData);
        window.location.reload();
      }
    },
    [innerAddresses, data]
  );

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      enableReinitialize
      onSubmit={handleSubmit}
    >
      {({ values, handleChange, handleSubmit, handleReset }) => {
        return (
          <Form>
            <Stack className={styles.tabBar}>
              {editMode ? (
                <>
                  <RoundButton icon={<Save />} onClick={() => handleSubmit()} />
                  <RoundButton
                    icon={<EditOff />}
                    onClick={() => {
                      setEditMode(false);
                      handleReset();
                    }}
                  />
                </>
              ) : (
                canEdit && (
                  <RoundButton
                    onClick={() => setEditMode(true)}
                    icon={<Edit />}
                  />
                )
              )}
            </Stack>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <Input
                  name={RegistryFields.NUMBER}
                  label={RegistryLabels[RegistryFields.NUMBER]}
                  value={values[RegistryFields.NUMBER] || ''}
                  onChange={handleChange}
                  disabled={!editMode}
                  required
                />
              </Grid>
              <Grid item xs={6}>
                <Input
                  name={RegistryFields.NAME}
                  label={RegistryLabels[RegistryFields.NAME]}
                  value={values[RegistryFields.NAME] || ''}
                  onChange={handleChange}
                  disabled
                />
              </Grid>
              <Grid item xs={6}>
                <AutoCompleteInput
                  name={RegistryFields.SENDER_DIVISION}
                  label={RegistryLabels[RegistryFields.SENDER_DIVISION]}
                  options={innerAddressesOptions}
                  disabled={!editMode}
                  required
                />
              </Grid>
              <Grid item xs={6}>
                <AutoCompleteInput
                  name={RegistryFields.RECIPIENT_DIVISION}
                  label={RegistryLabels[RegistryFields.RECIPIENT_DIVISION]}
                  options={innerAddressesOptions}
                  disabled={!editMode}
                  required
                />
              </Grid>
              <Grid item xs={6}>
                <DatePickerInput
                  name={RegistryFields.DATE}
                  label={RegistryLabels[RegistryFields.DATE]}
                  disabled={!editMode}
                />
              </Grid>
              <Grid item xs={6}>
                <Input
                  name={RegistryFields.STATE}
                  label={RegistryLabels[RegistryFields.STATE]}
                  value={values[RegistryFields.STATE]}
                  onChange={handleChange}
                  disabled
                />
              </Grid>
              <Grid item xs={6}>
                <Input
                  name={RegistryFields.SENT_DATE_TIME_NAME}
                  label={RegistryLabels[RegistryFields.SENT_DATE_TIME_NAME]}
                  value={values[RegistryFields.SENT_DATE_TIME_NAME] || ''}
                  onChange={handleChange}
                  disabled
                />
              </Grid>
              <Grid item xs={6}>
                <Input
                  name={RegistryFields.RECEIVED_DATE_TIME_NAME}
                  label={RegistryLabels[RegistryFields.RECEIVED_DATE_TIME_NAME]}
                  value={values[RegistryFields.RECEIVED_DATE_TIME_NAME] || ''}
                  onChange={handleChange}
                  disabled
                />
              </Grid>
              <Grid item xs={12}>
                <Input
                  name={RegistryFields.COMMENTS}
                  label={RegistryLabels[RegistryFields.COMMENTS]}
                  value={values[RegistryFields.COMMENTS] || ''}
                  onChange={handleChange}
                  multiline
                  disabled={!editMode}
                />
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

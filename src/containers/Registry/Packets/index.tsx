import { useCallback, useEffect, useMemo, useState } from 'react';

import {
  ChevronRight,
  DeleteOutline,
  Refresh,
  Undo,
} from '@mui/icons-material';

import { DataTable } from '../../../components/CustomDataGrid/DataTable';
import { RoundButton } from '../../../components/RoundButton';
import { ToolButton, ToolsPanel } from '../../../components/ToolsPanel';
import { DeleteToolButton } from '../../../components/ToolsPanel/DeleteToolButton';
import { RegistryOutPacketsCols } from '../../../constants/registry';
import {
  useDeleteRegistryPacketsMutation,
  useGetRegistryPacketsQuery,
  useReturnRegistryPacketMutation,
} from '../../../services/api/registries';
import { Registry } from '../../../types/registries';
import { RegistryPacketsToRowAdapter } from '../../../utils/registry';

import styles from './styles.module.scss';

interface RegistryProps {
  id: number;
  dataRegistry?: Registry;
}

export const RegistryPackets: React.FC<RegistryProps> = ({
  id,
  dataRegistry,
}) => {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});

  const { data, refetch, isLoading } = useGetRegistryPacketsQuery({
    id,
    page,
    size: perPage,
  });

  const stateCode = useMemo(() => {
    return dataRegistry?.stateCode;
  }, [dataRegistry]);

  const templateCode = useMemo(() => {
    return dataRegistry?.templateCode;
  }, [dataRegistry]);

  const [deleteRegistryDoc] = useDeleteRegistryPacketsMutation();

  const [returnPacket] = useReturnRegistryPacketMutation();

  const rows = useMemo(() => {
    return RegistryPacketsToRowAdapter(data?.data);
  }, [data?.data]);

  const selectedDocuments = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const regId = useMemo(() => {
    return data?.data?.find(
      (conclusion) => conclusion.id === selectedDocuments[0]
    )?.registryId;
  }, [selected]);

  const handleReturnPacket = useCallback(async () => {
    await returnPacket({
      ids: selectedDocuments,
      registryId: regId,
    });
  }, [selectedDocuments]);

  const handleDelete = useCallback(async () => {
    await deleteRegistryDoc({
      id: regId,
      packetId: selectedDocuments[0],
    });
    refetch();
  }, [selectedDocuments]);

  const handleDetails = useCallback(() => {
    window
      .open(`/registries/outgoingpacket/${selectedDocuments[0]}`, '_blank')
      ?.focus();
  }, [selectedDocuments]);

  useEffect(() => {
    refetch();
  }, [page, perPage]);

  return (
    <>
      <DataTable
        formKey={'registry_packets'}
        cols={RegistryOutPacketsCols}
        rows={rows}
        onRowSelect={setSelected}
        loading={isLoading}
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: data?.totalCount || 0,
        }}
        className={styles.table}
      >
        <ToolsPanel
          className={styles.toolBar}
          leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
        >
          <ToolButton
            label="Подробно"
            startIcon={<ChevronRight />}
            disabled={selectedDocuments.length !== 1}
            fast
          />

          <DeleteToolButton
            label={'Удалить'}
            startIcon={<DeleteOutline />}
            disabled={
              !(
                selectedDocuments.length == 1 &&
                stateCode === 'N' &&
                'S' &&
                templateCode === 'IP'
              )
            }
            fast
            description={`Вы действительно хотите удалить исходящий документ № ${
              data?.data?.find(
                (conclusion) => conclusion.id === selectedDocuments[0]
              )?.number
            }?`}
            title={'Удаление иcходящих документов'}
            onConfirm={handleDelete}
          />
          {stateCode === 'S' && (
            <ToolButton
              label={'Вернуть'}
              startIcon={<Undo />}
              disabled={!selectedDocuments.length}
              onClick={() => handleReturnPacket()}
              fast
            ></ToolButton>
          )}
        </ToolsPanel>
      </DataTable>
    </>
  );
};

import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import {
  Form,
  Formik,
  FormikProps,
  FormikValues,
  useFormikContext,
} from 'formik';
import { isEmpty, isEqual } from 'lodash';
import { number, object, string } from 'yup';

import { Close, Save } from '@mui/icons-material';
import { Button, Grid, Stack } from '@mui/material';

import { AutoCompleteInput } from '../../../components/AutoCompleteInput';
import { TreeDataGrid } from '../../../components/CustomDataGrid';
import { CreateDocumentGroupColumn } from '../../../components/CustomDataGrid/TreeDataGrid/Columns/GroupColumn';
import {
  DefaultColumn,
  DefaultRow,
} from '../../../components/CustomDataGrid/types';
import { createTreeByRows } from '../../../components/CustomDataGrid/utils';
import Input from '../../../components/Input';
import { Popup } from '../../../components/Popup';
import { RadioButtonGroup } from '../../../components/RadioButtonGroup';
import {
  AddToRegistryCols,
  AddToRegistryFields,
  AddToRegistryLabels,
  DefaultAddToRegistry,
} from '../../../constants/registry';
import {
  useGetInnerAddressesQuery,
  useGetRegistryNamesQuery,
  useGetRegistryPacketNamesQuery,
} from '../../../services/api/dictionaries';
import { useGetDocumentsCopiesTreeQuery } from '../../../services/api/document';
import {
  useAddDocumentToRegistryMutation,
  useAddPacketToRegistryMutation,
} from '../../../services/api/registries';
import { useAppDispatch } from '../../../store';
import { setUserMessage } from '../../../store/utils';

import { CreateSelectColumnConditional, selectAllHandler } from './utils';

import styles from './styles.module.scss';

interface AddToRegistryPopupProps {
  mode: 'documents' | 'packets' | null;
  setMode: (mode: 'documents' | 'packets' | null) => void;
  documentIds: number;
}

const AddToRegistryPopup: React.FC<AddToRegistryPopupProps> = ({
  mode,
  setMode,
  documentIds,
}) => {
  const dispatch = useAppDispatch();
  const formRef = useRef<FormikProps<FormikValues>>(null);
  const [selected, setSelected] = useState<Record<string, boolean>>({});
  const [errors, setErrors] = useState<Record<string, any>>({});
  const [tableFilters, setTableFilters] = useState(false);

  const selectedDocuments = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const { data: innerAddresses } = useGetInnerAddressesQuery({
    sort: 'typeCode asc, name',
    showNotActual: false,
  });
  const { data: docRegistryNames = [] } = useGetRegistryNamesQuery(
    { documentIds },
    { skip: mode !== 'documents' }
  );
  const { data: packetRegistryNames = [] } = useGetRegistryPacketNamesQuery(
    { templateTypeId: 4004 },
    { skip: mode !== 'packets' }
  );
  const { data: documents, isLoading } = useGetDocumentsCopiesTreeQuery(
    { documentIds },
    { skip: mode !== 'documents' }
  );
  const [addDocument] = useAddDocumentToRegistryMutation();
  const [addPacket] = useAddPacketToRegistryMutation();

  const rows = useMemo(() => {
    const rows: DefaultRow[] = [];
    documents?.map((parent) => {
      const rowParent = {
        id: parent.id,
        number: parent.number,
        code: parent.id,
        orderNumberName: '',
      };
      rows.push(rowParent);
      if (parent.children) {
        parent.children.map((child) => {
          const rowChild = { ...child, id: child.copyId };
          rows.push(rowChild);

          if (selectedDocuments.includes(rowChild.id)) {
            rowParent.orderNumberName += ` ${rowChild.orderNumberShortName}`;
          }
        });
      }
    });

    return rows;
  }, [documents, selectedDocuments]);

  const registryNameIdOptions = useMemo(() => {
    return mode === 'documents' ? docRegistryNames : packetRegistryNames;
  }, [mode, docRegistryNames, packetRegistryNames]);

  useEffect(() => {
    const initialSelected: Record<string, boolean> = {};
    documents?.map((parent) => {
      if (parent.children) {
        const children = parent.children;
        const firstSelected = children.find((child) => child.isBusy === false);
        if (firstSelected) {
          initialSelected[
            String(firstSelected.copyId) as keyof typeof initialSelected
          ] = true;
          if (parent.children.length === 1) {
            initialSelected[String(parent.id) as keyof typeof initialSelected] =
              true;
          }
        }
      }
    });
    setSelected(initialSelected);
  }, [documents]);

  const handleSubmit = useCallback(() => {
    const form = formRef?.current;
    const formValues = form?.values;
    if (formValues) {
      const addressee = innerAddresses?.data.find(
        (address) => address.id === formValues.addressee
      );
      const addresseeKey =
        addressee && addressee.typeCode === 'CLERK' ? 'clerkId' : 'divisionId';
      const createNew = Boolean(formValues.createNew === 'true');
      const document: Array<{ id: number; copyId: number[] }> = [];

      documents?.forEach((parent) => {
        const docObj: { id: number; copyId: number[] } = {
          id: 0,
          copyId: [],
        };
        if (parent.children) {
          parent.children.forEach((child) => {
            if (selectedDocuments.includes(child.copyId)) {
              docObj.id = child.parentId;
              docObj.copyId.push(child.copyId);
            }
          });
          if (docObj.id) {
            document.push(docObj);
          }
        }
      });

      const queryData = {
        [addresseeKey]: Number(addressee?.id.split('_')[0]),
        typeCode: 'INNER',
        createNew,
        registryNameId: formValues.registryNameId,
        comments: formValues.comments,
        document,
      };

      addDocument(queryData).then((response) => {
        if ('data' in response) {
          setMode(null);
          dispatch(
            setUserMessage({
              message: `Документы успешно добавлены в реестр №${formValues.registryNameId}`,
              type: 'success',
            })
          );
        }
      });
    }
  }, [formRef?.current?.values, innerAddresses, selectedDocuments, rows]);

  const groups = ['number'];

  const cols = useMemo(
    () =>
      AddToRegistryCols.map((col) => {
        return {
          ...col,
          formatter: ({
            row,
            column,
          }: {
            row: DefaultRow;
            column: DefaultColumn;
          }) => {
            return (
              <div className={row.isBusy ? styles.busyRegistry : ''}>
                {row[column.key]}
              </div>
            );
          },
        };
      }),
    [rows]
  );

  const validationSchema = useMemo(() => {
    return object({
      [AddToRegistryFields.ADDRESSEE]: string()
        .nullable()
        .required('Обязательное поле'),
      [AddToRegistryFields.REGISTRY_NAME_ID]: number()
        .nullable()
        .required('Обязательное поле'),
    });
  }, [registryNameIdOptions]);

  const ErrorsUpdater = () => {
    const { errors } = useFormikContext();

    useEffect(() => {
      setErrors((prev) => (isEqual(errors, prev) ? prev : errors));
    }, [errors]);

    return null;
  };

  return (
    <Popup
      open={Boolean(mode)}
      onClose={() => setMode(null)}
      title="Добавление в реестр"
      width={1200}
      height={715}
      bar={
        <Stack
          direction="row"
          justifyContent="flex-end"
          spacing={1.5}
          style={{ width: '100%' }}
        >
          <Button
            variant="contained"
            startIcon={<Save />}
            onClick={() => handleSubmit()}
            disabled={!isEmpty(errors) || !selectedDocuments.length}
          >
            Добавить
          </Button>
          <Button
            variant="outlined"
            startIcon={<Close />}
            onClick={() => {
              setMode(null);
            }}
          >
            Отмена
          </Button>
        </Stack>
      }
    >
      <Formik
        initialValues={DefaultAddToRegistry}
        validationSchema={validationSchema}
        validateOnMount
        enableReinitialize
        innerRef={formRef}
        onSubmit={handleSubmit}
      >
        {({ values, handleChange }) => {
          const showComments = Boolean(
            values[AddToRegistryFields.CREATE_NEW] === 'true'
          );

          return (
            <Form>
              <Grid container spacing={2} sx={{ mt: 1, pb: 2 }}>
                <Grid item xs={12}>
                  <AutoCompleteInput
                    name={AddToRegistryFields.ADDRESSEE}
                    label={AddToRegistryLabels[AddToRegistryFields.ADDRESSEE]}
                    options={
                      innerAddresses?.data.map((address) => {
                        return {
                          value: address.id,
                          label: address.name,
                        };
                      }) || []
                    }
                    required
                  />
                </Grid>
                <Grid item xs={12}>
                  {registryNameIdOptions.length ? (
                    <AutoCompleteInput
                      name={AddToRegistryFields.REGISTRY_NAME_ID}
                      label={
                        AddToRegistryLabels[
                          AddToRegistryFields.REGISTRY_NAME_ID
                        ]
                      }
                      options={registryNameIdOptions}
                      required
                    />
                  ) : (
                    <div className={styles.busyRegistry}>
                      Нет значений для поля "Наименование"
                    </div>
                  )}
                </Grid>
                <Grid item xs={12} className={styles.label}>
                  Экзепляры
                </Grid>
                {mode === 'documents' && (
                  <Grid item xs={12} className={styles.documentContainer}>
                    <TreeDataGrid
                      formKey={'add_to_registry'}
                      rows={rows}
                      cols={cols}
                      groups={groups}
                      onRowSelect={setSelected}
                      initialSelected={selected}
                      loading={isLoading}
                      createTree={(rows: DefaultRow[]) =>
                        createTreeByRows(rows, true)
                      }
                      createColumn={(
                        handleRowExpand: (rowId: string) => void
                      ) => CreateDocumentGroupColumn(groups, handleRowExpand)}
                      onSelectAll={selectAllHandler}
                      onCreateSelectColumn={CreateSelectColumnConditional}
                      isParentFontBold
                      showFilters={tableFilters}
                    />
                  </Grid>
                )}
                <Grid item xs={12}>
                  <RadioButtonGroup
                    name={AddToRegistryFields.CREATE_NEW}
                    options={[
                      { value: false, label: 'Автоматический' },
                      { value: true, label: 'Oтдельный' },
                    ]}
                    className={styles.radioGroup}
                    row
                  />
                </Grid>
                {showComments && (
                  <Grid item xs={12}>
                    <Input
                      name={AddToRegistryFields.COMMENTS}
                      label={AddToRegistryLabels[AddToRegistryFields.COMMENTS]}
                      value={values[AddToRegistryFields.COMMENTS]}
                      onChange={handleChange}
                      multiline
                    />
                  </Grid>
                )}
              </Grid>
              <ErrorsUpdater />
            </Form>
          );
        }}
      </Formik>
    </Popup>
  );
};

export default AddToRegistryPopup;

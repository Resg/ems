import { Column } from 'react-data-grid';

import { Select } from '../../../components/CustomDataGrid/TreeDataGrid/Columns/SelectColumn';
import { DefaultRow, TreeRow } from '../../../components/CustomDataGrid/types';

import styles from './styles.module.scss';

export function selectAllHandler<Row extends DefaultRow = DefaultRow>( // в случае отсутствия чекбоксов у некоторых строк
  rows: TreeRow<Row>[],
  map: Record<string | number, boolean>,
  value: boolean
): Record<string | number, boolean> {
  rows.forEach((row) => {
    if (row.isBusy === false) {
      map[row.id] = value;
    }
    if (row.children) {
      map = { ...map, ...selectAllHandler(row.children, map, value) };
    }
  });
  return { ...map };
}

export const CreateSelectColumnConditional = (): Column<any> => ({
  key: 'select',
  name: '',
  width: 44,
  minWidth: 44,
  cellClass: styles.cell,
  sortable: false,
  resizable: false,
  formatter({ row }) {
    if (
      row.isBusy === false ||
      (row.children &&
        row.children.some(
          (child: Record<string, any>) => child.isBusy === false
        ))
    ) {
      return <Select row={row} />;
    }
    return null;
  },
});

import React, { useCallback, useMemo } from 'react';
import { useFormikContext } from 'formik';
import { get, groupBy, sortBy } from 'lodash';

import { Grid } from '@mui/material';

import { AutoCompleteInput } from '../../../../components/AutoCompleteInput';
import { DateRange } from '../../../../components/DateRange';
import { DebounceAutoComplete } from '../../../../components/DebounceAutoComplete';
import Input from '../../../../components/Input';
import {
  PermissionSearchFields,
  PermissionSearchLabels,
} from '../../../../constants/permissionSearch';
import {
  useGetAsyncContractorsListMutation,
  useGetConclusionStatesQuery,
  useGetCounterpartiesAsyncMutation,
  useGetFederalDistricsQuery,
  useGetPermissionTypesQuery,
  useGetRegionsQuery,
} from '../../../../services/api/dictionaries';
import { Counterparty } from '../../../../types/dictionaries';

interface PermissionProps {}

export const Permission: React.FC<PermissionProps> = () => {
  const {
    handleChange,
    values = {} as any,
    setFieldValue,
  } = useFormikContext();

  const [getCounterparties] = useGetCounterpartiesAsyncMutation();
  const [getContractorsList] = useGetAsyncContractorsListMutation();
  const { data: permissionType } = useGetPermissionTypesQuery();
  const { data: states = [] } = useGetConclusionStatesQuery({});
  const { data: regions } = useGetRegionsQuery();
  const { data: federalDistricts = [] } = useGetFederalDistricsQuery({});

  const groupedTypes = useMemo(
    () =>
      Object.entries(
        groupBy(
          sortBy(permissionType?.data, 'permissionType').reverse(),
          'permissionType'
        )
      ).map((el) => {
        return {
          label: el[0],
          value: el[1].map((item) => item.code),
        };
      }) || [],
    [permissionType?.data]
  );

  const groupedTypesBase = useMemo(
    () =>
      Object.entries(
        groupBy(
          sortBy(permissionType?.data, 'baseDocumentType'),
          'baseDocumentType'
        )
      ).map((el) => {
        return {
          label: el[0],
          value: el[1].map((item) => item.code),
        };
      }) || [],
    [permissionType?.data]
  );

  const regionsOption = useMemo(
    () =>
      regions?.map((region) => {
        return {
          value: region.aoguid,
          label: region.name,
        };
      }) || [],
    [regions]
  );

  const getOptions = useCallback(async (value: string) => {
    if (value) {
      const response = await getContractorsList({ name: value });
      if ('data' in response) {
        return response.data.data.map((option: Record<string, any>) => {
          return {
            label: option.name,
            value: option.id,
          };
        });
      }
      return [];
    }
    return [];
  }, []);

  const getValueOptions = useCallback(
    async (ids: number[]) => {
      if (ids && ids.length) {
        const counterparties = await getCounterparties({ ids });
        if ('data' in counterparties) {
          return (
            counterparties?.data.data.map((option: Counterparty) => {
              return {
                label: option.name,
                value: option.id,
              };
            }) || []
          );
        } else {
          return [];
        }
      }
      return [];
    },
    [getCounterparties]
  );

  return (
    <Grid container rowSpacing={2} columnSpacing={12}>
      <Grid item xs={6}>
        <DebounceAutoComplete
          name={PermissionSearchFields.CONTRACTOR}
          label={PermissionSearchLabels[PermissionSearchFields.CONTRACTOR]}
          getOptions={getOptions}
          getValueOptions={getValueOptions}
          multiple
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          name={PermissionSearchFields.NUMBER}
          label={PermissionSearchLabels[PermissionSearchFields.NUMBER]}
          onChange={handleChange}
          value={get(values, PermissionSearchFields.NUMBER) || ''}
        />
      </Grid>
      <Grid item xs={6}>
        <DateRange
          nameFrom={PermissionSearchFields.DATE_FROM}
          nameBefore={PermissionSearchFields.DATE_TO}
          labelFrom={PermissionSearchLabels[PermissionSearchFields.DATE_FROM]}
        />
      </Grid>
      <Grid item xs={6}>
        <DateRange
          nameFrom={PermissionSearchFields.DATE_VALIDITY_FROM}
          nameBefore={PermissionSearchFields.DATE_VALIDITY_TO}
          labelFrom={
            PermissionSearchLabels[PermissionSearchFields.DATE_VALIDITY_FROM]
          }
        />
      </Grid>
      <Grid item xs={6}>
        <DateRange
          nameFrom={PermissionSearchFields.DATE_ANNULMENT_FROM}
          nameBefore={PermissionSearchFields.DATE_ANNULMENT_TO}
          labelFrom={
            PermissionSearchLabels[PermissionSearchFields.DATE_ANNULMENT_FROM]
          }
        />
      </Grid>
      <Grid item xs={6}>
        <AutoCompleteInput
          name={PermissionSearchFields.CODE}
          label={PermissionSearchLabels[PermissionSearchFields.CODE]}
          options={groupedTypes}
        />
      </Grid>
      <Grid item xs={6}>
        <AutoCompleteInput
          name={PermissionSearchFields.STATECODE}
          label={PermissionSearchLabels[PermissionSearchFields.STATECODE]}
          options={states}
          multiple
        />
      </Grid>
      <Grid item xs={6}>
        <AutoCompleteInput
          name={PermissionSearchFields.BASEDOCUMENTCODES}
          label={
            PermissionSearchLabels[PermissionSearchFields.BASEDOCUMENTCODES]
          }
          options={groupedTypesBase}
        />
      </Grid>
      <Grid item xs={6}>
        <AutoCompleteInput
          name={PermissionSearchFields.REGION}
          label={PermissionSearchLabels[PermissionSearchFields.REGION]}
          options={regionsOption}
          multiple
        />
      </Grid>
      <Grid item xs={6}>
        <AutoCompleteInput
          name={PermissionSearchFields.FEDERALDISTRICTCODE}
          label={
            PermissionSearchLabels[PermissionSearchFields.FEDERALDISTRICTCODE]
          }
          options={federalDistricts}
          multiple
        />
      </Grid>
    </Grid>
  );
};

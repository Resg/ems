import React, { useMemo } from 'react';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { Grid } from '@mui/material';

import { AutoCompleteInput } from '../../../../components/AutoCompleteInput';
import { DateRange } from '../../../../components/DateRange';
import Input from '../../../../components/Input';
import {
  ApplicationSearchFields,
  ApplicationSearchLabels,
} from '../../../../constants/permissionSearch';
import {
  useGetServiceMRFCQuery,
  useGetTechnologiesQuery,
} from '../../../../services/api/dictionaries';

interface ApplicationProps {}

export const Application: React.FC<ApplicationProps> = () => {
  const {
    handleChange,
    values = {} as any,
    setFieldValue,
  } = useFormikContext();

  const { data: serviceMRFC } = useGetServiceMRFCQuery({});
  const { data: technologies } = useGetTechnologiesQuery({});

  const serviceMRFCName = useMemo(() => {
    return (
      serviceMRFC?.map((service) => ({
        label: service.name,
        value: service.id,
      })) || []
    );
  }, [serviceMRFC]);

  const technologiesName = useMemo(() => {
    return (
      technologies?.data?.map((technology: { id: any; name: any }) => {
        return {
          value: technology.id,
          label: technology.name,
        };
      }) || []
    );
  }, [technologies]);

  return (
    <Grid container rowSpacing={2} columnSpacing={12}>
      <Grid item xs={6}>
        <Input
          name={ApplicationSearchFields.REQUEST_NUMBER}
          label={
            ApplicationSearchLabels[ApplicationSearchFields.REQUEST_NUMBER]
          }
          onChange={handleChange}
          value={get(values, ApplicationSearchFields.REQUEST_NUMBER) || ''}
        />
      </Grid>
      <Grid item xs={6}>
        <DateRange
          nameFrom={ApplicationSearchFields.REQUEST_DATE_FROM}
          nameBefore={ApplicationSearchFields.REQUEST_DATE_TO}
          labelFrom={
            ApplicationSearchLabels[ApplicationSearchFields.REQUEST_DATE_FROM]
          }
        />
      </Grid>

      <Grid item xs={12}>
        <AutoCompleteInput
          name={ApplicationSearchFields.SERVICE_MRFC}
          label={ApplicationSearchLabels[ApplicationSearchFields.SERVICE_MRFC]}
          options={serviceMRFCName}
          multiple
        />
      </Grid>
      <Grid item xs={12}>
        <AutoCompleteInput
          name={ApplicationSearchFields.COMMUNICATION_TECNOLOGY}
          label={
            ApplicationSearchLabels[
              ApplicationSearchFields.COMMUNICATION_TECNOLOGY
            ]
          }
          options={technologiesName}
          multiple
        />
      </Grid>
    </Grid>
  );
};

import React, { useCallback, useEffect, useMemo } from 'react';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { Checkbox, FormControlLabel, Grid } from '@mui/material';

import { AutoCompleteInput } from '../../../../components/AutoCompleteInput';
import { ClerkInput } from '../../../../components/ClerkInput';
import { DateRange } from '../../../../components/DateRange';
import { DebounceAutoComplete } from '../../../../components/DebounceAutoComplete';
import { FieldWrapper } from '../../../../components/FieldWrapper';
import Input from '../../../../components/Input';
import {
  DocumentSearchRequestFields,
  DocumentSearchRequestLabels,
} from '../../../../constants/documentSearch';
import {
  useGetAsyncContractorsListMutation,
  useGetCounterpartiesAsyncMutation,
  useGetRadioServiceQuery,
  useGetRegionsQuery,
  useGetTechnologiesQuery,
} from '../../../../services/api/dictionaries';
import { Counterparty } from '../../../../types/dictionaries';

import styles from './styles.module.scss';

interface DocumentRequestProps {}

export const Request: React.FC<DocumentRequestProps> = () => {
  const {
    handleChange,
    values = {} as any,
    setFieldValue,
  } = useFormikContext();

  const typeId = values.filters.typeId;

  const { data: radioServices } = useGetRadioServiceQuery({});
  const { data: regions } = useGetRegionsQuery();
  const { data: technologies } = useGetTechnologiesQuery({});
  const [getCounterparties] = useGetCounterpartiesAsyncMutation();
  const [getContractorsList] = useGetAsyncContractorsListMutation();

  const isDate = useMemo(
    () => get(values, DocumentSearchRequestFields.DATE),
    [values]
  );

  const isOuterDate = useMemo(
    () => get(values, DocumentSearchRequestFields.OUTER_DATE),
    [values]
  );
  const requestManager = useMemo(
    () => get(values, DocumentSearchRequestFields.REQUESTMANAGER),
    [values]
  );
  const isLinkedRequests = useMemo(
    () => get(values, DocumentSearchRequestFields.IS_LINKED_REQUESTS),
    [values]
  );

  const getOptions = useCallback(
    async (value: string) => {
      if (value) {
        const response = await getContractorsList({ name: value });
        if ('data' in response) {
          return response.data.data.map((option: Record<string, any>) => {
            return {
              label: option.name,
              value: option.id,
            };
          });
        }
        return [];
      }
      return [];
    },
    [getContractorsList]
  );

  const getValueOptions = useCallback(
    async (ids: number[]) => {
      if (ids && ids.length) {
        const counterparties = await getCounterparties({ ids });
        if ('data' in counterparties) {
          return (
            counterparties?.data.data.map((option: Counterparty) => {
              return {
                label: option.name,
                value: option.id,
              };
            }) || []
          );
        } else {
          return [];
        }
      }
      return [];
    },
    [getCounterparties]
  );

  const getOptions2 = useCallback(
    async (value: string) => {
      if (value) {
        const response = await getContractorsList({ name: value });
        if ('data' in response) {
          return response.data.data.map((option: Record<string, any>) => {
            return {
              label: option.name,
              value: option.id,
            };
          });
        }
        return [];
      }
      return [];
    },
    [getContractorsList]
  );

  const getValueOptions2 = useCallback(
    async (ids: number[]) => {
      if (ids && ids.length) {
        const counterparties = await getCounterparties({ ids });
        if ('data' in counterparties) {
          return (
            counterparties?.data.data.map((option: Counterparty) => {
              return {
                label: option.name,
                value: option.id,
              };
            }) || []
          );
        } else {
          return [];
        }
      }
      return [];
    },
    [getCounterparties]
  );

  useEffect(() => {
    if (!isDate) {
      setFieldValue(DocumentSearchRequestFields.DATESTART, '');
      setFieldValue(DocumentSearchRequestFields.DATEEND, '');
    }
    if (!isOuterDate) {
      setFieldValue(DocumentSearchRequestFields.OUTER_DATE_FROM, '');
      setFieldValue(DocumentSearchRequestFields.OUTER_DATE_TO, '');
    }
  }, [isOuterDate, isDate]);

  return (
    <Grid container rowSpacing={2} columnSpacing={12}>
      <Grid item xs={12}>
        <Input
          name={DocumentSearchRequestFields.NUMBER}
          label={
            DocumentSearchRequestLabels[DocumentSearchRequestFields.NUMBER]
          }
          onChange={handleChange}
          value={get(values, DocumentSearchRequestFields.NUMBER) || ''}
        />
      </Grid>
      <Grid item xs={6} className={styles.dateInput}>
        <FieldWrapper
          name={DocumentSearchRequestFields.DATE}
          defaultValue={false}
          className={styles.checkbox}
        >
          <FormControlLabel
            label={
              DocumentSearchRequestLabels[DocumentSearchRequestFields.DATE]
            }
            control={
              <Checkbox
                name={DocumentSearchRequestFields.DATE}
                checked={get(values, DocumentSearchRequestFields.DATE) || false}
                onChange={() =>
                  setFieldValue(DocumentSearchRequestFields.DATE, !isDate)
                }
              />
            }
          />
        </FieldWrapper>
        <DateRange
          nameFrom={DocumentSearchRequestFields.DATESTART}
          nameBefore={DocumentSearchRequestFields.DATEEND}
          labelFrom={
            DocumentSearchRequestLabels[DocumentSearchRequestFields.DATESTART]
          }
          disabled={!isDate}
          className={styles.dateRange}
          clearField
        />
      </Grid>
      <Grid item xs={6}>
        <FieldWrapper
          name={DocumentSearchRequestFields.REQUESTMANAGER}
          defaultValue={[]}
        >
          <ClerkInput
            name={DocumentSearchRequestFields.REQUESTMANAGER}
            label={
              DocumentSearchRequestLabels[
                DocumentSearchRequestFields.REQUESTMANAGER
              ]
            }
          />
        </FieldWrapper>
      </Grid>
      <Grid item xs={6}>
        <DebounceAutoComplete
          name={DocumentSearchRequestFields.CONTRACTOR}
          label={
            DocumentSearchRequestLabels[DocumentSearchRequestFields.CONTRACTOR]
          }
          getOptions={getOptions}
          getValueOptions={getValueOptions}
        />
      </Grid>
      <Grid item xs={6}>
        <DebounceAutoComplete
          name={DocumentSearchRequestFields.CONCLUSION_CONTRACTOR}
          label={
            DocumentSearchRequestLabels[
              DocumentSearchRequestFields.CONCLUSION_CONTRACTOR
            ]
          }
          disabled={![1, 21].includes(typeId)}
          getOptions={getOptions2}
          getValueOptions={getValueOptions2}
        />
      </Grid>
      <Grid item xs={6} className={styles.dateInput}>
        <FieldWrapper
          name={DocumentSearchRequestFields.OUTER_DATE}
          defaultValue={false}
          className={styles.checkbox}
        >
          <FormControlLabel
            label={
              DocumentSearchRequestLabels[
                DocumentSearchRequestFields.OUTER_DATE
              ]
            }
            control={
              <Checkbox
                name={DocumentSearchRequestFields.OUTER_DATE}
                checked={
                  get(values, DocumentSearchRequestFields.OUTER_DATE) || false
                }
                onChange={() =>
                  setFieldValue(
                    DocumentSearchRequestFields.OUTER_DATE,
                    !isOuterDate
                  )
                }
              />
            }
          />
        </FieldWrapper>
        <DateRange
          nameFrom={DocumentSearchRequestFields.OUTER_DATE_FROM}
          nameBefore={DocumentSearchRequestFields.OUTER_DATE_TO}
          labelFrom={
            DocumentSearchRequestLabels[
              DocumentSearchRequestFields.OUTER_DATE_FROM
            ]
          }
          disabled={!isOuterDate}
          className={styles.dateRange}
          clearField
        />
      </Grid>
      <Grid item xs={6}>
        <AutoCompleteInput
          name={DocumentSearchRequestFields.RADIO_SERVICE}
          label={
            DocumentSearchRequestLabels[
              DocumentSearchRequestFields.RADIO_SERVICE
            ]
          }
          options={
            radioServices?.map((service) => {
              return { value: service.id, label: service.name };
            }) || []
          }
        />
      </Grid>
      <Grid item xs={12}>
        <AutoCompleteInput
          name={DocumentSearchRequestFields.TECHNOLOGY}
          label={
            DocumentSearchRequestLabels[DocumentSearchRequestFields.TECHNOLOGY]
          }
          options={
            technologies?.data?.map((technology: { id: any; name: any }) => {
              return {
                value: technology.id,
                label: technology.name,
              };
            }) || []
          }
          multiple
        />
      </Grid>
      <Grid item xs={12}>
        <AutoCompleteInput
          name={DocumentSearchRequestFields.REGION}
          label={
            DocumentSearchRequestLabels[DocumentSearchRequestFields.REGION]
          }
          options={
            regions?.map((region) => {
              return {
                value: region.aoguid,
                label: region.name,
              };
            }) || []
          }
          multiple
          disabled
        />
      </Grid>
      <Grid item xs={12}>
        <FormControlLabel
          label={
            DocumentSearchRequestLabels[
              DocumentSearchRequestFields.IS_LINKED_REQUESTS
            ]
          }
          control={
            <Checkbox
              name={DocumentSearchRequestFields.IS_LINKED_REQUESTS}
              checked={
                get(values, DocumentSearchRequestFields.IS_LINKED_REQUESTS) ||
                false
              }
              onChange={() =>
                setFieldValue(
                  DocumentSearchRequestFields.IS_LINKED_REQUESTS,
                  !isLinkedRequests
                )
              }
            />
          }
        />
      </Grid>
      <Grid item xs={12}>
        <Input
          name={DocumentSearchRequestFields.PERMISSION_NUMBER}
          label={
            DocumentSearchRequestLabels[
              DocumentSearchRequestFields.PERMISSION_NUMBER
            ]
          }
          onChange={handleChange}
          value={
            get(values, DocumentSearchRequestFields.PERMISSION_NUMBER) || ''
          }
        />
      </Grid>
    </Grid>
  );
};

import React, { useEffect, useMemo } from 'react';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { FormControlLabel, Grid, Radio, RadioGroup } from '@mui/material';

import { AutoCompleteInput } from '../../../../components/AutoCompleteInput';
import { CheckboxInput } from '../../../../components/CheckboxInput';
import { ClerkInput } from '../../../../components/ClerkInput';
import { DateRange } from '../../../../components/DateRange';
import Input from '../../../../components/Input';
import {
  DocumentSearchFields,
  DocumentSearchRegistryFields,
  DocumentSearchRegistryLabels,
} from '../../../../constants/documentSearch';
import {
  useGetDivisionsQuery,
  useGetInternalRegistryStatesQuery,
  useGetRegistryPacketNamesQuery,
} from '../../../../services/api/dictionaries';

import styles from './styles.module.scss';

export const Registry: React.FC = () => {
  const {
    handleChange,
    setFieldValue,
    values = {} as any,
  } = useFormikContext();
  const { data: states = [] } = useGetInternalRegistryStatesQuery({});
  const { data: packetRegistryNames = [] } = useGetRegistryPacketNamesQuery({
    type: 'INNER',
  });
  const { data: divisions = [] } = useGetDivisionsQuery({});

  const type = useMemo(() => {
    return get(values, DocumentSearchRegistryFields.REGISTRY_TYPE) || null;
  }, [values]);

  const isArchive = useMemo(() => {
    return get(values, DocumentSearchRegistryFields.IS_ARCHIVE) || null;
  }, [values]);

  const isOuter = useMemo(() => {
    return (
      get(values, DocumentSearchRegistryFields.REGISTRY_TYPE) === 'OUTER' ||
      null
    );
  }, [values]);

  const isDocOuter = useMemo(() => {
    return get(values, DocumentSearchFields.TYPE) === 21 || null;
  }, [values]);

  useEffect(() => {
    if (isArchive)
      setFieldValue(DocumentSearchRegistryFields.REGISTRY_STATE_CODES, []);
  }, [isArchive]);

  useEffect(() => {
    if (isOuter) {
      [
        DocumentSearchRegistryFields.REGISTRY_STATE_CODES,
        DocumentSearchRegistryFields.REGISTRY_NAME,
        DocumentSearchRegistryFields.REGISTRY_SENDER_DIVISIONS,
        DocumentSearchRegistryFields.REGISTRY_SENDER_CLERKS,
        DocumentSearchRegistryFields.REGISTRY_RECIPIENT_DIVISIONS,
        DocumentSearchRegistryFields.REGISTRY_RECIPIENT_CLERKS,
      ].forEach((i) => setFieldValue(i, []));

      [
        DocumentSearchRegistryFields.REGISTRY_DATE_CREATE_FROM,
        DocumentSearchRegistryFields.REGISTRY_DATE_CREATE_TO,
        DocumentSearchRegistryFields.REGISTRY_DATE_RECEIVE_FROM,
        DocumentSearchRegistryFields.REGISTRY_DATE_RECEIVE_TO,
      ].forEach((i) => setFieldValue(i, ''));
    }
  }, [isOuter]);

  return (
    <Grid container rowSpacing={2} columnSpacing={12}>
      <Grid item xs={12}>
        <Input
          name={DocumentSearchRegistryFields.REGISTRY_NUMBER}
          label={
            DocumentSearchRegistryLabels[
              DocumentSearchRegistryFields.REGISTRY_NUMBER
            ]
          }
          onChange={handleChange}
          value={
            get(values, DocumentSearchRegistryFields.REGISTRY_NUMBER) || ''
          }
        />
      </Grid>
      <Grid item xs={12}>
        Тип:
        <RadioGroup
          name={DocumentSearchRegistryFields.REGISTRY_TYPE}
          value={type || 'ALL'}
          onChange={handleChange}
          row
        >
          <Grid item xs={4}>
            <FormControlLabel label="Все" control={<Radio />} value="ALL" />
          </Grid>
          <Grid item xs={4}>
            <FormControlLabel
              label="Внутренний"
              control={<Radio />}
              value="INNER"
            />
          </Grid>
          <Grid item xs={4}>
            <FormControlLabel
              label="Внешний"
              control={<Radio />}
              value="OUTER"
              disabled={!isDocOuter}
            />
          </Grid>
        </RadioGroup>
      </Grid>
      <Grid item xs={12}>
        <CheckboxInput
          name={DocumentSearchRegistryFields.IS_ARCHIVE}
          label={
            DocumentSearchRegistryLabels[
              DocumentSearchRegistryFields.IS_ARCHIVE
            ]
          }
          className={styles.checkbox}
        />
      </Grid>
      {!isOuter && (
        <>
          <Grid item xs={12}>
            <AutoCompleteInput
              name={DocumentSearchRegistryFields.REGISTRY_STATE_CODES}
              label={
                DocumentSearchRegistryLabels[
                  DocumentSearchRegistryFields.REGISTRY_STATE_CODES
                ]
              }
              disabled={isArchive}
              options={states}
              multiple
            />
          </Grid>
          <Grid item xs={12}>
            <AutoCompleteInput
              name={DocumentSearchRegistryFields.REGISTRY_NAME}
              label={
                DocumentSearchRegistryLabels[
                  DocumentSearchRegistryFields.REGISTRY_NAME
                ]
              }
              options={packetRegistryNames}
              multiple
            />
          </Grid>
          <Grid item xs={4}>
            <DateRange
              nameFrom={DocumentSearchRegistryFields.REGISTRY_DATE_CREATE_FROM}
              nameBefore={DocumentSearchRegistryFields.REGISTRY_DATE_CREATE_TO}
              labelFrom={
                DocumentSearchRegistryLabels[
                  DocumentSearchRegistryFields.REGISTRY_DATE_CREATE_FROM
                ]
              }
            />
          </Grid>
        </>
      )}
      <Grid item xs={4}>
        <DateRange
          nameFrom={DocumentSearchRegistryFields.REGISTRY_DATE_SENDING_FROM}
          nameBefore={DocumentSearchRegistryFields.REGISTRY_DATE_SENDING_TO}
          labelFrom={
            DocumentSearchRegistryLabels[
              DocumentSearchRegistryFields.REGISTRY_DATE_SENDING_FROM
            ]
          }
        />
      </Grid>
      {!isOuter && (
        <>
          <Grid item xs={4}>
            <DateRange
              nameFrom={DocumentSearchRegistryFields.REGISTRY_DATE_RECEIVE_FROM}
              nameBefore={DocumentSearchRegistryFields.REGISTRY_DATE_RECEIVE_TO}
              labelFrom={
                DocumentSearchRegistryLabels[
                  DocumentSearchRegistryFields.REGISTRY_DATE_RECEIVE_FROM
                ]
              }
            />
          </Grid>
          <Grid item xs={12}>
            <AutoCompleteInput
              name={DocumentSearchRegistryFields.REGISTRY_SENDER_DIVISIONS}
              label={
                DocumentSearchRegistryLabels[
                  DocumentSearchRegistryFields.REGISTRY_SENDER_DIVISIONS
                ]
              }
              options={divisions}
              multiple
            />
          </Grid>
          <Grid item xs={12}>
            <ClerkInput
              name={DocumentSearchRegistryFields.REGISTRY_SENDER_CLERKS}
              label={
                DocumentSearchRegistryLabels[
                  DocumentSearchRegistryFields.REGISTRY_SENDER_CLERKS
                ]
              }
              multiple
            />
          </Grid>
          <Grid item xs={12}>
            <AutoCompleteInput
              name={DocumentSearchRegistryFields.REGISTRY_RECIPIENT_DIVISIONS}
              label={
                DocumentSearchRegistryLabels[
                  DocumentSearchRegistryFields.REGISTRY_RECIPIENT_DIVISIONS
                ]
              }
              options={divisions}
              multiple
            />
          </Grid>
          <Grid item xs={12}>
            <ClerkInput
              name={DocumentSearchRegistryFields.REGISTRY_RECIPIENT_CLERKS}
              label={
                DocumentSearchRegistryLabels[
                  DocumentSearchRegistryFields.REGISTRY_RECIPIENT_CLERKS
                ]
              }
              multiple
            />
          </Grid>
        </>
      )}
    </Grid>
  );
};

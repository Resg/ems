import React, { useCallback, useEffect, useMemo } from 'react';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { Checkbox, FormControlLabel, Grid } from '@mui/material';

import { ClerkInput } from '../../../../components/ClerkInput';
import { DateRange } from '../../../../components/DateRange';
import { DebounceAutoComplete } from '../../../../components/DebounceAutoComplete';
import { FieldWrapper } from '../../../../components/FieldWrapper';
import Input from '../../../../components/Input';
import {
  DocumentSearchResolutionFields,
  DocumentSearchResolutionLabels,
} from '../../../../constants/documentSearch';
import {
  useGetAsyncContractorsListMutation,
  useGetCounterpartiesAsyncMutation,
  useGetCounterpartyPersonsListMutation,
  useGetCounterpartyPersonsMutation,
} from '../../../../services/api/dictionaries';
import { Counterparty } from '../../../../types/dictionaries';

import styles from './styles.module.scss';

interface DocumentResolutionProps {}

export const Resolution: React.FC<DocumentResolutionProps> = () => {
  const {
    handleChange,
    values = {} as any,
    setFieldValue,
  } = useFormikContext();

  const [getCounterparties] = useGetCounterpartiesAsyncMutation();
  const [getContractorsList] = useGetAsyncContractorsListMutation();
  const [getPersons] = useGetCounterpartyPersonsMutation();
  const [getPersonsList] = useGetCounterpartyPersonsListMutation();

  const isDate = useMemo(
    () => get(values, DocumentSearchResolutionFields.EXTERNAL_DOCUMENT_DATE),
    [values]
  );

  const isResolutionDate = useMemo(
    () => get(values, DocumentSearchResolutionFields.RESOLUTION_DATE),
    [values]
  );

  const isResolutionPlanDate = useMemo(
    () => get(values, DocumentSearchResolutionFields.RESOLUTION_PLAN_DATE),
    [values]
  );
  const resolutionAuthor = useMemo(
    () => get(values, DocumentSearchResolutionFields.RESOLUTION_AUTHOR),
    [values]
  );

  const isHaveResolution = useMemo(
    () => get(values, DocumentSearchResolutionFields.IS_HAVE_RESOLUTION),
    [values]
  );

  const getOptionsPerson = useCallback(
    async (value: string) => {
      if (value) {
        const response = await getPersons({ fullName: value });
        if ('data' in response) {
          return response.data;
        }
        return [];
      }
      return [];
    },
    [getPersons]
  );

  const getValueOptionsPerson = useCallback(
    async (ids: number[]) => {
      if (ids && ids.length) {
        const counterparties = await getPersonsList({ ids });
        if ('data' in counterparties) {
          return counterparties.data || [];
        }
        return [];
      }
      return [];
    },
    [getPersonsList]
  );

  const getOptionsPerformer = useCallback(
    async (value: string) => {
      if (value) {
        const response = await getPersons({ fullName: value });
        if ('data' in response) {
          return response.data;
        }
        return [];
      }
      return [];
    },
    [getPersons]
  );

  const getValueOptionsPerformer = useCallback(
    async (ids: number[]) => {
      if (ids && ids.length) {
        const counterparties = await getPersonsList({ ids });
        if ('data' in counterparties) {
          return counterparties.data || [];
        }
        return [];
      }
      return [];
    },
    [getPersonsList]
  );

  const getOptionsOrgPerformer = useCallback(
    async (value: string) => {
      if (value) {
        const response = await getContractorsList({ name: value });
        if ('data' in response) {
          return response.data.data.map((option: Record<string, any>) => {
            return {
              label: option.name,
              value: option.id,
            };
          });
        }
        return [];
      }
      return [];
    },
    [getContractorsList]
  );

  const getValueOptionsOrgPerformer = useCallback(
    async (ids: number[]) => {
      if (ids && ids.length) {
        const counterparties = await getCounterparties({ ids });
        if ('data' in counterparties) {
          return (
            counterparties?.data.data.map((option: Counterparty) => {
              return {
                label: option.name,
                value: option.id,
              };
            }) || []
          );
        } else {
          return [];
        }
      }
      return [];
    },
    [getCounterparties]
  );

  useEffect(() => {
    if (!isDate) {
      setFieldValue(
        DocumentSearchResolutionFields.EXTERNAL_DOCUMENT_DATE_FROM,
        ''
      );
      setFieldValue(
        DocumentSearchResolutionFields.EXTERNAL_DOCUMENT_DATE_TO,
        ''
      );
    }
  }, [isDate]);

  useEffect(() => {
    if (!isResolutionPlanDate) {
      setFieldValue(
        DocumentSearchResolutionFields.RESOLUTION_PLAN_DATE_FROM,
        ''
      );
      setFieldValue(DocumentSearchResolutionFields.RESOLUTION_PLAN_DATE_TO, '');
    }
  }, [isResolutionPlanDate]);

  useEffect(() => {
    if (!isResolutionDate) {
      setFieldValue(DocumentSearchResolutionFields.RESOLUTION_DATE_FROM, '');
      setFieldValue(DocumentSearchResolutionFields.RESOLUTION_DATE_TO, '');
    }
  }, [isResolutionDate]);

  return (
    <Grid container rowSpacing={2} columnSpacing={12}>
      <Grid item xs={12}>
        <FormControlLabel
          label={
            DocumentSearchResolutionLabels[
              DocumentSearchResolutionFields.IS_HAVE_RESOLUTION
            ]
          }
          control={
            <Checkbox
              name={DocumentSearchResolutionFields.IS_HAVE_RESOLUTION}
              checked={
                get(
                  values,
                  DocumentSearchResolutionFields.IS_HAVE_RESOLUTION
                ) || false
              }
              onChange={() =>
                setFieldValue(
                  DocumentSearchResolutionFields.IS_HAVE_RESOLUTION,
                  !isHaveResolution
                )
              }
            />
          }
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          name={DocumentSearchResolutionFields.EXTERNAL_DOCUMENT_NUMBER}
          label={
            DocumentSearchResolutionLabels[
              DocumentSearchResolutionFields.EXTERNAL_DOCUMENT_NUMBER
            ]
          }
          onChange={handleChange}
          value={
            get(
              values,
              DocumentSearchResolutionFields.EXTERNAL_DOCUMENT_NUMBER
            ) || ''
          }
        />
      </Grid>
      <Grid item xs={6} className={styles.dateInput}>
        <FieldWrapper
          name={DocumentSearchResolutionFields.EXTERNAL_DOCUMENT_DATE}
          defaultValue={false}
          className={styles.checkbox}
        >
          <FormControlLabel
            label={
              DocumentSearchResolutionLabels[
                DocumentSearchResolutionFields.EXTERNAL_DOCUMENT_DATE
              ]
            }
            control={
              <Checkbox
                name={DocumentSearchResolutionFields.EXTERNAL_DOCUMENT_DATE}
                checked={
                  get(
                    values,
                    DocumentSearchResolutionFields.EXTERNAL_DOCUMENT_DATE
                  ) || false
                }
                onChange={() =>
                  setFieldValue(
                    DocumentSearchResolutionFields.EXTERNAL_DOCUMENT_DATE,
                    !isDate
                  )
                }
              />
            }
          />
        </FieldWrapper>
        <DateRange
          nameFrom={DocumentSearchResolutionFields.EXTERNAL_DOCUMENT_DATE_FROM}
          nameBefore={DocumentSearchResolutionFields.EXTERNAL_DOCUMENT_DATE_TO}
          labelFrom={
            DocumentSearchResolutionLabels[
              DocumentSearchResolutionFields.EXTERNAL_DOCUMENT_DATE_FROM
            ]
          }
          disabled={!isDate}
          className={styles.dateRange}
          clearField
        />
      </Grid>
      <Grid item xs={12}>
        <FieldWrapper
          name={DocumentSearchResolutionFields.RESOLUTION_AUTHORS_NAME}
          defaultValue={[]}
        >
          <ClerkInput
            name={DocumentSearchResolutionFields.RESOLUTION_AUTHORS_NAME}
            label={
              DocumentSearchResolutionLabels[
                DocumentSearchResolutionFields.RESOLUTION_AUTHORS_NAME
              ]
            }
            multiple
          />
        </FieldWrapper>
      </Grid>
      <Grid item xs={12}>
        <DebounceAutoComplete
          name={DocumentSearchResolutionFields.RESOLUTION_AUTHOR}
          label={
            DocumentSearchResolutionLabels[
              DocumentSearchResolutionFields.RESOLUTION_AUTHOR
            ]
          }
          getOptions={getOptionsPerson}
          getValueOptions={getValueOptionsPerson}
        />
      </Grid>
      <Grid item xs={6} className={styles.dateInput}>
        <FieldWrapper
          name={DocumentSearchResolutionFields.RESOLUTION_DATE}
          defaultValue={false}
          className={styles.checkbox}
        >
          <FormControlLabel
            label={
              DocumentSearchResolutionLabels[
                DocumentSearchResolutionFields.RESOLUTION_DATE
              ]
            }
            control={
              <Checkbox
                name={DocumentSearchResolutionFields.RESOLUTION_DATE}
                checked={
                  get(values, DocumentSearchResolutionFields.RESOLUTION_DATE) ||
                  false
                }
                onChange={() =>
                  setFieldValue(
                    DocumentSearchResolutionFields.RESOLUTION_DATE,
                    !isResolutionDate
                  )
                }
              />
            }
          />
        </FieldWrapper>
        <DateRange
          nameFrom={DocumentSearchResolutionFields.RESOLUTION_DATE_FROM}
          nameBefore={DocumentSearchResolutionFields.RESOLUTION_DATE_TO}
          labelFrom={
            DocumentSearchResolutionLabels[
              DocumentSearchResolutionFields.RESOLUTION_DATE_FROM
            ]
          }
          disabled={!isResolutionDate}
          className={styles.dateRange}
          clearField
        />
      </Grid>

      <Grid item xs={6} className={styles.dateInput}>
        <FieldWrapper
          name={DocumentSearchResolutionFields.RESOLUTION_PLAN_DATE}
          defaultValue={false}
          className={styles.checkbox}
        >
          <FormControlLabel
            label={
              DocumentSearchResolutionLabels[
                DocumentSearchResolutionFields.RESOLUTION_PLAN_DATE
              ]
            }
            control={
              <Checkbox
                name={DocumentSearchResolutionFields.RESOLUTION_PLAN_DATE}
                checked={
                  get(
                    values,
                    DocumentSearchResolutionFields.RESOLUTION_PLAN_DATE
                  ) || false
                }
                onChange={() =>
                  setFieldValue(
                    DocumentSearchResolutionFields.RESOLUTION_PLAN_DATE,
                    !isResolutionPlanDate
                  )
                }
              />
            }
          />
        </FieldWrapper>
        <DateRange
          nameFrom={DocumentSearchResolutionFields.RESOLUTION_PLAN_DATE_FROM}
          nameBefore={DocumentSearchResolutionFields.RESOLUTION_PLAN_DATE_TO}
          labelFrom={
            DocumentSearchResolutionLabels[
              DocumentSearchResolutionFields.RESOLUTION_PLAN_DATE_FROM
            ]
          }
          disabled={!isResolutionPlanDate}
          className={styles.dateRange}
          clearField
        />
      </Grid>
      <Grid item xs={12}>
        <Input
          name={DocumentSearchResolutionFields.RESOLUTION_DESCRIPTION}
          label={
            DocumentSearchResolutionLabels[
              DocumentSearchResolutionFields.RESOLUTION_DESCRIPTION
            ]
          }
          onChange={handleChange}
          value={
            get(
              values,
              DocumentSearchResolutionFields.RESOLUTION_DESCRIPTION
            ) || ''
          }
          multiline
        />
      </Grid>
      <Grid item xs={12}>
        <DebounceAutoComplete
          name={
            DocumentSearchResolutionFields.RESOLUTION_ORGANIZATION_PERFORMER
          }
          label={
            DocumentSearchResolutionLabels[
              DocumentSearchResolutionFields.RESOLUTION_ORGANIZATION_PERFORMER
            ]
          }
          getOptions={getOptionsOrgPerformer}
          getValueOptions={getValueOptionsOrgPerformer}
          multiple
        />
      </Grid>
      <Grid item xs={12}>
        <DebounceAutoComplete
          name={DocumentSearchResolutionFields.RESOLUTION_PERFORMER}
          label={
            DocumentSearchResolutionLabels[
              DocumentSearchResolutionFields.RESOLUTION_PERFORMER
            ]
          }
          getOptions={getOptionsPerformer}
          getValueOptions={getValueOptionsPerformer}
        />
      </Grid>
    </Grid>
  );
};

import React, { useCallback, useEffect, useMemo } from 'react';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import {
  Checkbox,
  FormControlLabel,
  Grid,
  Radio,
  RadioGroup,
} from '@mui/material';

import { AutoCompleteInput } from '../../../../components/AutoCompleteInput';
import { ClerkInput } from '../../../../components/ClerkInput';
import { DateRange } from '../../../../components/DateRange';
import { DebounceAutoComplete } from '../../../../components/DebounceAutoComplete';
import { FieldWrapper } from '../../../../components/FieldWrapper';
import Input from '../../../../components/Input';
import {
  DocumentSearchFields,
  DocumentSearchLabels,
} from '../../../../constants/documentSearch';
import {
  useGetAsyncContractorsListMutation,
  useGetCounterpartiesAsyncMutation,
  useGetDivisionsQuery,
  useGetDocumentRubricsOptionsQuery,
  useGetDocumentTypesQuery,
  useGetInnerAddressesQuery,
  useGetRequestTypesQuery,
  useGetRubricsTypesQuery,
  useGetStatusesQuery,
} from '../../../../services/api/dictionaries';
import { useGetDictionaries } from '../../../../services/api/dictionaries/useGetDictionaries';
import { Counterparty } from '../../../../types/dictionaries';

import styles from './styles.module.scss';

interface DocumentProps {}

export const Document: React.FC<DocumentProps> = () => {
  const getNumId = useCallback((value: string) => {
    return Number(value.split('_')[0]);
  }, []);

  const {
    handleChange,
    values = {} as any,
    setFieldValue,
  } = useFormikContext();
  const typeId = values.filters.typeId;

  const { data: RubricsOptions = [], refetch } =
    useGetDocumentRubricsOptionsQuery({
      classifierFullCode: values.filters.classifierName,
      documentTypeId: values.filters.typeId,
    });

  const { data: types } = useGetDocumentTypesQuery({});
  const { data: statuses } = useGetStatusesQuery({});
  const { data: divisions } = useGetDivisionsQuery({});
  const { data: rubricTypes } = useGetRubricsTypesQuery({});
  const { dictionariesAccesses = [] } = useGetDictionaries({});
  const [getCounterparties] = useGetCounterpartiesAsyncMutation();
  const [getContractorsList] = useGetAsyncContractorsListMutation();
  const { data: innerAddresses } = useGetInnerAddressesQuery({});

  const getOptions = useCallback(async (value: string) => {
    if (value) {
      const response = await getContractorsList({ name: value });
      if ('data' in response) {
        return response.data.data.map((option: Record<string, any>) => {
          return {
            label: option.name,
            value: option.id,
          };
        });
      }
      return [];
    }
    return [];
  }, []);

  const getValueOptions = useCallback(
    async (ids: number[]) => {
      if (ids && ids.length) {
        const counterparties = await getCounterparties({ ids });
        if ('data' in counterparties) {
          return (
            counterparties?.data.data.map((option: Counterparty) => {
              return {
                label: option.name,
                value: option.id,
              };
            }) || []
          );
        } else {
          return [];
        }
      }
      return [];
    },
    [getCounterparties]
  );

  const getOptions2 = useCallback(async (value: string) => {
    if (value) {
      const response = await getContractorsList({ name: value });
      if ('data' in response) {
        return response.data.data.map((option: Record<string, any>) => {
          return {
            label: option.name,
            value: option.id,
          };
        });
      }
      return [];
    }
    return [];
  }, []);

  const getValueOptions2 = useCallback(
    async (ids: number[]) => {
      if (ids && ids.length) {
        const counterparties = await getCounterparties({ ids });
        if ('data' in counterparties) {
          return (
            counterparties?.data.data.map((option: Counterparty) => {
              return {
                label: option.name,
                value: option.id,
              };
            }) || []
          );
        } else {
          return [];
        }
      }
      return [];
    },
    [getCounterparties]
  );

  const innerAddressesOptions = useMemo(() => {
    return (
      innerAddresses?.data?.map((option) => ({
        label: option.name,
        value: getNumId(option.id),
      })) || []
    );
  }, [innerAddresses]);

  const { data: requestTypes } = useGetRequestTypesQuery();
  const isMyDocuments = useMemo(
    () => get(values, DocumentSearchFields.ISMYDOCUMENTS),
    [values]
  );
  const contractorIds = useMemo(
    () => get(values, DocumentSearchFields.CONTRACTOR),
    [values]
  );
  const isEDSAttached = useMemo(
    () => get(values, DocumentSearchFields.ISEDSATTACHED),
    [values]
  );
  const isManagerSet = useMemo(
    () => get(values, DocumentSearchFields.ISMANAGERSET),
    [values]
  );
  const isDate = useMemo(
    () => get(values, DocumentSearchFields.DATE),
    [values]
  );
  const isDateCreation = useMemo(
    () => get(values, DocumentSearchFields.DATECREATION),
    [values]
  );
  const isDateSign = useMemo(
    () => get(values, DocumentSearchFields.DATESIGN),
    [values]
  );
  const isLegacyClient = useMemo(
    () => get(values, DocumentSearchFields.ISLEGACYCLIENT),
    [values]
  );
  const isPhysicalClient = useMemo(
    () => get(values, DocumentSearchFields.ISPHYSICALCLIENT),
    [values]
  );
  const isDateContractor = useMemo(
    () => get(values, DocumentSearchFields.DATECONTRACTOR),
    [values]
  );
  const haveNoFiles = useMemo(
    () => get(values, DocumentSearchFields.HAVENOFILES),
    [values]
  );
  const rubricType = useMemo(
    () =>
      get(values, DocumentSearchFields.RUBRIC_TYPE) ||
      rubricTypes?.filter((type) => type.label === 'Все'),
    [values, rubricTypes]
  );

  useEffect(() => {
    if (!isDate) {
      setFieldValue(DocumentSearchFields.DATESTART, '');
      setFieldValue(DocumentSearchFields.DATEEND, '');
    }
    if (!isDateCreation) {
      setFieldValue(DocumentSearchFields.DATECREATIONSTART, '');
      setFieldValue(DocumentSearchFields.DATECREATIONEND, '');
    }
    if (!isDateSign) {
      setFieldValue(DocumentSearchFields.DATESIGNSTART, '');
      setFieldValue(DocumentSearchFields.DATESIGNEND, '');
    }
    if (!isDateContractor) {
      setFieldValue(DocumentSearchFields.DATECONTRACTORSTART, '');
      setFieldValue(DocumentSearchFields.DATECONTRACTOREND, '');
    }
  }, [isDate, isDateCreation, isDateSign, isDateContractor]);
  return (
    <Grid container rowSpacing={2} columnSpacing={12}>
      <Grid item xs={12}>
        <FormControlLabel
          label={DocumentSearchLabels[DocumentSearchFields.ISMYDOCUMENTS]}
          control={
            <Checkbox
              name={DocumentSearchFields.ISMYDOCUMENTS}
              checked={get(values, DocumentSearchFields.ISMYDOCUMENTS) || false}
              onChange={() =>
                setFieldValue(
                  DocumentSearchFields.ISMYDOCUMENTS,
                  !isMyDocuments
                )
              }
            />
          }
        />
      </Grid>
      <Grid item xs={12}>
        <AutoCompleteInput
          name={DocumentSearchFields.TYPE}
          label={DocumentSearchLabels[DocumentSearchFields.TYPE]}
          options={
            types?.map((type) => {
              return { value: type.value, label: type.label };
            }) || []
          }
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          name={DocumentSearchFields.NUMBER}
          label={DocumentSearchLabels[DocumentSearchFields.NUMBER]}
          onChange={handleChange}
          value={get(values, DocumentSearchFields.NUMBER) || ''}
        />
      </Grid>
      <Grid item xs={6}>
        <FieldWrapper
          name={DocumentSearchFields.LOCALNUMBER}
          disabled={typeId === 1}
          defaultValue=""
        >
          <Input
            name={DocumentSearchFields.LOCALNUMBER}
            label={DocumentSearchLabels[DocumentSearchFields.LOCALNUMBER]}
            disabled={typeId === 1}
            onChange={handleChange}
            value={get(values, DocumentSearchFields.LOCALNUMBER) || ''}
          />
        </FieldWrapper>
      </Grid>
      <Grid item xs={12}>
        <FormControlLabel
          label={DocumentSearchLabels[DocumentSearchFields.ISMANAGERSET]}
          control={
            <Checkbox
              name={DocumentSearchFields.ISMANAGERSET}
              checked={get(values, DocumentSearchFields.ISMANAGERSET) || false}
              onChange={() =>
                setFieldValue(DocumentSearchFields.ISMANAGERSET, !isManagerSet)
              }
            />
          }
        />
      </Grid>
      <Grid item xs={4} className={styles.dateInput}>
        <FieldWrapper
          name={DocumentSearchFields.DATE}
          disabled={![1, 21, 22, 23].includes(typeId)}
          defaultValue={false}
          className={styles.checkbox}
        >
          <FormControlLabel
            label={DocumentSearchLabels[DocumentSearchFields.DATE]}
            control={
              <Checkbox
                name={DocumentSearchFields.DATE}
                checked={get(values, DocumentSearchFields.DATE) || false}
                onChange={() =>
                  setFieldValue(DocumentSearchFields.DATE, !isDate)
                }
                disabled={![1, 21, 22, 23].includes(typeId)}
              />
            }
          />
        </FieldWrapper>
        <DateRange
          nameFrom={DocumentSearchFields.DATESTART}
          nameBefore={DocumentSearchFields.DATEEND}
          labelFrom={DocumentSearchLabels[DocumentSearchFields.DATESTART]}
          disabled={!isDate}
          className={styles.dateRange}
          clearField
        />
      </Grid>
      <Grid item xs={4} className={styles.dateInput}>
        <FieldWrapper
          name={DocumentSearchFields.DATECREATION}
          disabled={![21, 22, 23, 100].includes(typeId)}
          defaultValue=""
          className={styles.checkbox}
        >
          <FormControlLabel
            label={DocumentSearchLabels[DocumentSearchFields.DATECREATION]}
            control={
              <Checkbox
                name={DocumentSearchFields.DATECREATION}
                checked={
                  get(values, DocumentSearchFields.DATECREATION) || false
                }
                onChange={() =>
                  setFieldValue(
                    DocumentSearchFields.DATECREATION,
                    !isDateCreation
                  )
                }
                disabled={![21, 22, 23, 100].includes(typeId)}
              />
            }
          />
        </FieldWrapper>
        <DateRange
          nameFrom={DocumentSearchFields.DATECREATIONSTART}
          nameBefore={DocumentSearchFields.DATECREATIONEND}
          labelFrom={
            DocumentSearchLabels[DocumentSearchFields.DATECREATIONSTART]
          }
          disabled={!isDateCreation}
          className={styles.dateRange}
          clearField
        />
      </Grid>
      <Grid item xs={4} className={styles.dateInput}>
        <FieldWrapper
          name={DocumentSearchFields.DATESIGN}
          disabled={![21, 22, 23].includes(typeId)}
          defaultValue=""
          className={styles.checkbox}
        >
          <FormControlLabel
            label={DocumentSearchLabels[DocumentSearchFields.DATESIGN]}
            control={
              <Checkbox
                name={DocumentSearchFields.DATESIGN}
                checked={get(values, DocumentSearchFields.DATESIGN) || false}
                onChange={() =>
                  setFieldValue(DocumentSearchFields.DATESIGN, !isDateSign)
                }
                disabled={![21, 22, 23].includes(typeId)}
              />
            }
          />
        </FieldWrapper>
        <DateRange
          nameFrom={DocumentSearchFields.DATESIGNSTART}
          nameBefore={DocumentSearchFields.DATESIGNEND}
          labelFrom={DocumentSearchLabels[DocumentSearchFields.DATESIGNSTART]}
          disabled={!isDateSign}
          className={styles.dateRange}
          clearField
        />
      </Grid>
      <Grid item xs={4}>
        <FieldWrapper
          name={DocumentSearchFields.RUBRIC_TYPE}
          disabled={![1, 21].includes(typeId)}
          defaultValue={![1, 21].includes(typeId) ? '' : rubricType}
        >
          <AutoCompleteInput
            name={DocumentSearchFields.RUBRIC_TYPE}
            label={DocumentSearchLabels[DocumentSearchFields.RUBRIC_TYPE]}
            options={
              rubricTypes?.map((type) => {
                return { value: type.value, label: type.label };
              }) || []
            }
            disabled={![1, 21].includes(typeId)}
            initialValue={![1, 21].includes(typeId) ? '' : rubricType}
          />
        </FieldWrapper>
      </Grid>
      <Grid item xs={8}>
        <AutoCompleteInput
          name={DocumentSearchFields.RUBRIC}
          label={DocumentSearchLabels[DocumentSearchFields.RUBRIC]}
          options={RubricsOptions.map((rubric) => {
            return { value: rubric.value, label: rubric.label };
          })}
          multiple
        />
      </Grid>
      <Grid item xs={12}>
        <AutoCompleteInput
          name={DocumentSearchFields.STATUS}
          label={DocumentSearchLabels[DocumentSearchFields.STATUS]}
          options={
            statuses?.map((status) => {
              return { value: status.value, label: status.label };
            }) || []
          }
          multiple
        />
      </Grid>
      <Grid item xs={12}>
        <AutoCompleteInput
          name={DocumentSearchFields.ACCESS}
          label={DocumentSearchLabels[DocumentSearchFields.ACCESS]}
          options={dictionariesAccesses.map((access) => {
            return { value: access.code, label: access.name };
          })}
        />
      </Grid>
      <Grid item xs={12}>
        <FieldWrapper
          name={DocumentSearchFields.CONTRACTOR}
          disabled={typeId !== 1}
          defaultValue={[]}
        >
          <DebounceAutoComplete
            name={typeId !== 21 ? DocumentSearchFields.CONTRACTOR : ''}
            label="Корреспондент"
            getOptions={getOptions}
            getValueOptions={getValueOptions}
            multiple
            disabled={typeId !== 1}
          />
        </FieldWrapper>
      </Grid>
      <Grid item xs={12}>
        <FieldWrapper
          name={DocumentSearchFields.ADDRESSEE}
          disabled={typeId !== 21}
          defaultValue={[]}
        >
          <DebounceAutoComplete
            name={typeId !== 1 ? DocumentSearchFields.ADDRESSEE : ''}
            label={DocumentSearchLabels[DocumentSearchFields.ADDRESSEE]}
            getOptions={getOptions2}
            getValueOptions={getValueOptions2}
            multiple
            disabled={typeId !== 21}
          />
        </FieldWrapper>
      </Grid>
      <Grid item xs={6}>
        <FieldWrapper
          name={DocumentSearchFields.ISLEGACYCLIENT}
          disabled={![1, 21].includes(typeId)}
          defaultValue={false}
        >
          <FormControlLabel
            label={DocumentSearchLabels[DocumentSearchFields.ISLEGACYCLIENT]}
            control={
              <Checkbox
                name={DocumentSearchFields.ISLEGACYCLIENT}
                checked={
                  get(values, DocumentSearchFields.ISLEGACYCLIENT) || false
                }
                onChange={() =>
                  setFieldValue(
                    DocumentSearchFields.ISLEGACYCLIENT,
                    !isLegacyClient
                  )
                }
                disabled={![1, 21].includes(typeId)}
              />
            }
          />
        </FieldWrapper>
      </Grid>
      <Grid item xs={6}>
        <FieldWrapper
          name={DocumentSearchFields.ISPHYSICALCLIENT}
          disabled={![1, 21].includes(typeId)}
          defaultValue={false}
        >
          <FormControlLabel
            label={DocumentSearchLabels[DocumentSearchFields.ISPHYSICALCLIENT]}
            control={
              <Checkbox
                name={DocumentSearchFields.ISPHYSICALCLIENT}
                checked={
                  get(values, DocumentSearchFields.ISPHYSICALCLIENT) || false
                }
                onChange={() =>
                  setFieldValue(
                    DocumentSearchFields.ISPHYSICALCLIENT,
                    !isPhysicalClient
                  )
                }
                disabled={![1, 21].includes(typeId)}
              />
            }
          />
        </FieldWrapper>
      </Grid>
      <Grid item xs={12}>
        <FieldWrapper
          name={DocumentSearchFields.DIVISION}
          disabled={![21, 22, 23, 100].includes(typeId)}
          defaultValue={[]}
        >
          <AutoCompleteInput
            name={DocumentSearchFields.DIVISION}
            label={DocumentSearchLabels[DocumentSearchFields.DIVISION]}
            options={
              divisions?.map((division) => {
                return { value: division.value, label: division.label };
              }) || []
            }
            multiple
            disabled={![21, 22, 23, 100].includes(typeId)}
          />
        </FieldWrapper>
      </Grid>
      <Grid item xs={12}>
        <FieldWrapper
          name={DocumentSearchFields.CLERK}
          disabled={![21, 22, 23, 100].includes(typeId)}
          defaultValue={[]}
        >
          <ClerkInput
            name={DocumentSearchFields.CLERK}
            label={DocumentSearchLabels[DocumentSearchFields.CLERK]}
            multiple
            disabled={![21, 22, 23, 100].includes(typeId)}
          />
        </FieldWrapper>
      </Grid>
      <Grid item xs={12}>
        <FieldWrapper
          name={DocumentSearchFields.INNERADDRESSEE}
          disabled={![1, 22, 23].includes(typeId)}
          defaultValue={[]}
        >
          <ClerkInput
            name={DocumentSearchFields.INNERADDRESSEE}
            label={DocumentSearchLabels[DocumentSearchFields.INNERADDRESSEE]}
            options={innerAddressesOptions}
            multiple
            disabled={![1, 22, 23].includes(typeId)}
          />
        </FieldWrapper>
      </Grid>
      <Grid item xs={12}>
        <FieldWrapper
          name={DocumentSearchFields.RECIPIENT}
          disabled={typeId !== 1}
          defaultValue={[]}
        >
          <ClerkInput
            name={DocumentSearchFields.RECIPIENT}
            label={DocumentSearchLabels[DocumentSearchFields.RECIPIENT]}
            options={innerAddressesOptions}
            multiple
            disabled={typeId !== 1}
          />
        </FieldWrapper>
      </Grid>
      <Grid item xs={12}>
        <FieldWrapper
          name={DocumentSearchFields.LOCATIONDIVSION}
          disabled={![1, 21].includes(typeId)}
          defaultValue={[]}
        >
          <AutoCompleteInput
            name={DocumentSearchFields.LOCATIONDIVSION}
            label={DocumentSearchLabels[DocumentSearchFields.LOCATIONDIVSION]}
            options={
              divisions?.map((division) => {
                return { value: division.value, label: division.label };
              }) || []
            }
            multiple
            disabled={![1, 21].includes(typeId)}
          />
        </FieldWrapper>
      </Grid>
      <Grid item xs={12}>
        <FieldWrapper
          name={DocumentSearchFields.LOCATIONCLERK}
          disabled={![1, 21].includes(typeId)}
          defaultValue={[]}
        >
          <ClerkInput
            name={DocumentSearchFields.LOCATIONCLERK}
            label={DocumentSearchLabels[DocumentSearchFields.LOCATIONCLERK]}
            multiple
            disabled={![1, 21].includes(typeId)}
          />
        </FieldWrapper>
      </Grid>
      <Grid item xs={12}>
        <FieldWrapper
          name={DocumentSearchFields.CREATOR}
          disabled={typeId !== 1}
          defaultValue={[]}
        >
          <ClerkInput
            name={DocumentSearchFields.CREATOR}
            label={DocumentSearchLabels[DocumentSearchFields.CREATOR]}
            multiple
            disabled={typeId !== 1}
          />
        </FieldWrapper>
      </Grid>
      <Grid item xs={12}>
        <FieldWrapper
          name={DocumentSearchFields.EXTERNALSIGNER}
          disabled={typeId !== 1}
          defaultValue={''}
        >
          <Input
            name={DocumentSearchFields.EXTERNALSIGNER}
            label={DocumentSearchLabels[DocumentSearchFields.EXTERNALSIGNER]}
            disabled={typeId !== 1}
            onChange={handleChange}
            value={get(values, DocumentSearchFields.EXTERNALSIGNER) || ''}
          />
        </FieldWrapper>
      </Grid>
      <Grid item xs={12}>
        <FieldWrapper
          name={DocumentSearchFields.INTERNALSIGNER}
          disabled={![21, 22, 23].includes(typeId)}
          defaultValue={[]}
        >
          <ClerkInput
            name={DocumentSearchFields.INTERNALSIGNER}
            label={DocumentSearchLabels[DocumentSearchFields.INTERNALSIGNER]}
            multiple
            disabled={![21, 22, 23].includes(typeId)}
          />
        </FieldWrapper>
      </Grid>
      <Grid item xs={6}>
        <FieldWrapper
          name={DocumentSearchFields.CONTRACTORDOCUMENTNUMBER}
          disabled={typeId !== 1}
          defaultValue={''}
        >
          <Input
            name={DocumentSearchFields.CONTRACTORDOCUMENTNUMBER}
            label={
              DocumentSearchLabels[
                DocumentSearchFields.CONTRACTORDOCUMENTNUMBER
              ]
            }
            disabled={typeId !== 1}
            onChange={handleChange}
            value={
              get(values, DocumentSearchFields.CONTRACTORDOCUMENTNUMBER) || ''
            }
          />
        </FieldWrapper>
      </Grid>
      <Grid item xs={6} className={styles.dateInput}>
        <FieldWrapper
          name={DocumentSearchFields.DATECONTRACTOR}
          disabled={typeId !== 1}
          defaultValue={false}
          className={styles.checkbox}
        >
          <FormControlLabel
            label={DocumentSearchLabels[DocumentSearchFields.DATECONTRACTOR]}
            control={
              <Checkbox
                name={DocumentSearchFields.DATECONTRACTOR}
                checked={
                  get(values, DocumentSearchFields.DATECONTRACTOR) || false
                }
                onChange={() =>
                  setFieldValue(
                    DocumentSearchFields.DATECONTRACTOR,
                    !isDateContractor
                  )
                }
                disabled={typeId !== 1}
              />
            }
          />
        </FieldWrapper>
        <DateRange
          nameFrom={DocumentSearchFields.DATECONTRACTORSTART}
          nameBefore={DocumentSearchFields.DATECONTRACTOREND}
          labelFrom={
            DocumentSearchLabels[DocumentSearchFields.DATECONTRACTORSTART]
          }
          disabled={!isDateContractor}
          className={styles.dateRange}
          clearField
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          rows={2}
          multiline
          name={DocumentSearchFields.DESCRIPTION}
          label={DocumentSearchLabels[DocumentSearchFields.DESCRIPTION]}
          onChange={handleChange}
          value={get(values, DocumentSearchFields.DESCRIPTION) || ''}
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          rows={2}
          multiline
          name={DocumentSearchFields.COMMENT}
          label={DocumentSearchLabels[DocumentSearchFields.COMMENT]}
          onChange={handleChange}
          value={get(values, DocumentSearchFields.COMMENT) || ''}
        />
      </Grid>
      <Grid item xs={12}>
        <p>Отсутствуют прикрепленные файлы:</p>
        <RadioGroup
          name={DocumentSearchFields.HAVENOFILES}
          value={haveNoFiles}
          row
        >
          <Grid item xs={6}>
            <FieldWrapper
              name={DocumentSearchFields.HAVENOFILES}
              disabled={typeId === 22}
              defaultValue={null}
            >
              <FormControlLabel
                label="Все"
                control={<Radio />}
                disabled={typeId === 22}
                onChange={() =>
                  setFieldValue(DocumentSearchFields.HAVENOFILES, true)
                }
                onClick={() =>
                  haveNoFiles === true
                    ? setFieldValue(DocumentSearchFields.HAVENOFILES, null)
                    : null
                }
                checked={haveNoFiles === true}
              />
            </FieldWrapper>
          </Grid>
          <Grid item xs={6}>
            <FieldWrapper
              name={DocumentSearchFields.HAVENOFILES}
              disabled={typeId === 22}
              defaultValue={null}
            >
              <FormControlLabel
                label="ABBY Scan Station"
                control={<Radio />}
                disabled={typeId === 22}
                onChange={() =>
                  setFieldValue(DocumentSearchFields.HAVENOFILES, false)
                }
                onClick={() =>
                  haveNoFiles === false
                    ? setFieldValue(DocumentSearchFields.HAVENOFILES, null)
                    : null
                }
                checked={haveNoFiles === false}
              />
            </FieldWrapper>
          </Grid>
        </RadioGroup>
      </Grid>
      <Grid item xs={12}>
        <FieldWrapper
          name={DocumentSearchFields.REGISTRANT}
          disabled={typeId !== 21}
          defaultValue={[]}
        >
          <ClerkInput
            name={DocumentSearchFields.REGISTRANT}
            label={DocumentSearchLabels[DocumentSearchFields.REGISTRANT]}
            multiple
            disabled={typeId !== 21}
          />
        </FieldWrapper>
      </Grid>
      <Grid item xs={12}>
        <FieldWrapper
          name={DocumentSearchFields.ISEDSATTACHED}
          disabled={typeId !== 21}
        >
          <FormControlLabel
            label={DocumentSearchLabels[DocumentSearchFields.ISEDSATTACHED]}
            control={
              <Checkbox
                name={DocumentSearchFields.ISEDSATTACHED}
                checked={
                  get(values, DocumentSearchFields.ISEDSATTACHED) || false
                }
                onChange={() =>
                  setFieldValue(
                    DocumentSearchFields.ISEDSATTACHED,
                    !isEDSAttached
                  )
                }
              />
            }
            disabled={typeId !== 21}
          />
        </FieldWrapper>
      </Grid>
    </Grid>
  );
};

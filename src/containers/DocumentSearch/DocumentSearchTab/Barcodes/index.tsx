import React from 'react';

import { Grid } from '@mui/material';

import { ListInput } from '../../../../components/ListInput';
import { DocumentSearchFields } from '../../../../constants/documentSearch';

export const Barcodes: React.FC = () => {
  return (
    <Grid container direction="column" spacing={1} sx={{ mb: 2 }}>
      <Grid item>Список штрих-кодов</Grid>
      <Grid item>
        <ListInput
          name={DocumentSearchFields.DOCUMENT_IDS}
          listElementType="integer"
          multiline
          rows={15}
        />
      </Grid>
    </Grid>
  );
};

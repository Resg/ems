import React, { ChangeEvent, useMemo } from 'react';
import { useFormikContext } from 'formik';

import { AutoCompleteInput } from '../../../../../components/AutoCompleteInput';
import {
  DocumentSearchBandLabels,
  DocumentSearchBandsFields,
} from '../../../../../constants/documentSearch';

export interface InputAbbreviationProps {
  index: number;
  position: 'from' | 'to';
  options: { label: string; value: any }[];
}

export const InputAbbreviation: React.FC<InputAbbreviationProps> = ({
  index,
  position,
  options,
}) => {
  const { setFieldValue, values = {} as any } = useFormikContext();
  const vals = values?.filters?.frequencyBands[index];
  const direct = vals[DocumentSearchBandsFields.BAND_DIRECT];
  const label = useMemo(
    () =>
      position === 'from'
        ? DocumentSearchBandLabels[
            DocumentSearchBandsFields.BAND_ABBREVIATION_LOW
          ]
        : DocumentSearchBandLabels[
            DocumentSearchBandsFields.BAND_ABBREVIATION_HIGH
          ],
    [position]
  );
  const names = useMemo(() => {
    if (position === 'from') {
      return {
        main:
          direct === 'T'
            ? DocumentSearchBandsFields.KOEFFICIENT_TO_STANDART_TRANSMIT_FROM
            : DocumentSearchBandsFields.KOEFFICIENT_TO_STANDART_RECEIVE_FROM,
        all: [
          DocumentSearchBandsFields.KOEFFICIENT_TO_STANDART_TRANSMIT_FROM,
          DocumentSearchBandsFields.KOEFFICIENT_TO_STANDART_RECEIVE_FROM,
        ],
      };
    } else {
      return {
        main:
          direct === 'T'
            ? DocumentSearchBandsFields.KOEFFICIENT_TO_STANDART_TRANSMIT_TO
            : DocumentSearchBandsFields.KOEFFICIENT_TO_STANDART_RECEIVE_TO,
        all: [
          DocumentSearchBandsFields.KOEFFICIENT_TO_STANDART_TRANSMIT_TO,
          DocumentSearchBandsFields.KOEFFICIENT_TO_STANDART_RECEIVE_TO,
        ],
      };
    }
  }, [direct, position]);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (direct === 'TR') {
      names.all.forEach((n) =>
        setFieldValue(
          `${DocumentSearchBandsFields.FREQUENCY_BANDS}[${index}].${n}`,
          e.target.value
        )
      );
    } else {
      setFieldValue(
        `${DocumentSearchBandsFields.FREQUENCY_BANDS}[${index}].${names.main}`,
        e.target.value
      );
    }
  };

  return (
    <AutoCompleteInput
      name={`${DocumentSearchBandsFields.FREQUENCY_BANDS}[${index}].${names.main}`}
      label={label}
      options={options}
      onInputChange={handleChange}
    />
  );
};

import React, { ChangeEvent, useMemo } from 'react';
import { useFormikContext } from 'formik';

import Input from '../../../../../components/Input';
import {
  DocumentSearchBandLabels,
  DocumentSearchBandsFields,
} from '../../../../../constants/documentSearch';

export interface InputBandProps {
  index: number;
  position: 'from' | 'to';
}

export const InputBand: React.FC<InputBandProps> = ({ index, position }) => {
  const { setFieldValue, values = {} as any } = useFormikContext();
  const vals = values?.filters?.frequencyBands[index];
  const direct = vals[DocumentSearchBandsFields.BAND_DIRECT];
  const label = useMemo(
    () =>
      position === 'from'
        ? DocumentSearchBandLabels[DocumentSearchBandsFields.BAND_LOW]
        : DocumentSearchBandLabels[DocumentSearchBandsFields.BAND_HIGH],
    [position]
  );
  const names = useMemo(() => {
    if (position === 'from') {
      return {
        main:
          direct === 'T'
            ? DocumentSearchBandsFields.FREQUENCY_TRANSMIT_FROM
            : DocumentSearchBandsFields.FREQUENCY_RECEIVE_FROM,
        all: [
          DocumentSearchBandsFields.FREQUENCY_TRANSMIT_FROM,
          DocumentSearchBandsFields.FREQUENCY_RECEIVE_FROM,
        ],
      };
    } else {
      return {
        main:
          direct === 'T'
            ? DocumentSearchBandsFields.FREQUENCY_TRANSMIT_TO
            : DocumentSearchBandsFields.FREQUENCY_RECEIVE_TO,
        all: [
          DocumentSearchBandsFields.FREQUENCY_TRANSMIT_TO,
          DocumentSearchBandsFields.FREQUENCY_RECEIVE_TO,
        ],
      };
    }
  }, [direct, position]);

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (direct === 'TR') {
      names.all.forEach((n) =>
        setFieldValue(
          `${DocumentSearchBandsFields.FREQUENCY_BANDS}[${index}].${n}`,
          e.target.value
        )
      );
    } else {
      setFieldValue(
        `${DocumentSearchBandsFields.FREQUENCY_BANDS}[${index}].${names.main}`,
        e.target.value
      );
    }
  };

  return (
    <Input
      label={label}
      name={DocumentSearchBandsFields.BAND_LOW}
      onChange={onChange}
      value={vals[names.main] || ''}
    />
  );
};

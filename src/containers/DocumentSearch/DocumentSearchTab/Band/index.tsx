import React, { useEffect } from 'react';
import { FieldArray, useFormikContext } from 'formik';

import { AddBox, Delete } from '@mui/icons-material';
import { Button, Grid } from '@mui/material';

import { AutoCompleteInput } from '../../../../components/AutoCompleteInput';
import {
  DocumentSearchBandLabels,
  DocumentSearchBandsFields,
} from '../../../../constants/documentSearch';
import { useGetFrequencyMeasuresQuery } from '../../../../services/api/dictionaries';

import { InputAbbreviation } from './InputAbbreviation';
import { InputBand } from './InputBand';

import styles from './styles.module.scss';

const directOptions = [
  { label: 'Прием', value: 'R' },
  { label: 'Передача', value: 'T' },
  { label: 'Прием/Передача', value: 'TR' },
];

export const Band: React.FC = () => {
  const { setFieldValue, values = {} as any } = useFormikContext();
  const { data: measures = [] } = useGetFrequencyMeasuresQuery();
  const vals = values?.filters?.frequencyBands;

  useEffect(() => {
    if (!vals) {
      setFieldValue(DocumentSearchBandsFields.FREQUENCY_BANDS, [{}]);
    }
  }, [values]);

  return (
    <Grid container rowSpacing={2} columnSpacing={12}>
      <FieldArray
        name={DocumentSearchBandsFields.FREQUENCY_BANDS}
        render={(arrayHelpers) => (
          <>
            {vals?.map((band: any, index: number) => {
              const path = `${DocumentSearchBandsFields.FREQUENCY_BANDS}[${index}].`;
              const transmitFrom =
                DocumentSearchBandsFields.FREQUENCY_TRANSMIT_FROM;
              const kTramsmitFrom =
                DocumentSearchBandsFields.KOEFFICIENT_TO_STANDART_TRANSMIT_FROM;
              const transmitTo =
                DocumentSearchBandsFields.FREQUENCY_TRANSMIT_TO;
              const receiveFrom =
                DocumentSearchBandsFields.FREQUENCY_RECEIVE_FROM;
              const receiveTo = DocumentSearchBandsFields.FREQUENCY_RECEIVE_TO;
              const kReceiveFrom =
                DocumentSearchBandsFields.KOEFFICIENT_TO_STANDART_RECEIVE_FROM;
              const kTramsmitTo =
                DocumentSearchBandsFields.KOEFFICIENT_TO_STANDART_TRANSMIT_TO;
              const kReceiveTo =
                DocumentSearchBandsFields.KOEFFICIENT_TO_STANDART_RECEIVE_TO;

              const onDirectChange = (value: { value: string }) => {
                switch (value.value) {
                  case 'T': {
                    setFieldValue(`${path}${transmitFrom}`, band[receiveFrom]);
                    setFieldValue(
                      `${path}${kTramsmitFrom}`,
                      band[kReceiveFrom]
                    );
                    setFieldValue(`${path}${transmitTo}`, band[receiveTo]);
                    setFieldValue(`${path}${kTramsmitTo}`, band[kReceiveTo]);

                    [receiveFrom, kReceiveFrom, receiveTo, kReceiveTo].forEach(
                      (i) => setFieldValue(`${path}${i}`, '')
                    );
                    return;
                  }
                  case 'R': {
                    setFieldValue(`${path}${receiveFrom}`, band[transmitFrom]);
                    setFieldValue(
                      `${path}${kReceiveFrom}`,
                      band[kTramsmitFrom]
                    );
                    setFieldValue(`${path}${receiveTo}`, band[transmitTo]);
                    setFieldValue(`${path}${kReceiveTo}`, band[kTramsmitTo]);

                    [
                      transmitFrom,
                      kTramsmitFrom,
                      transmitTo,
                      kTramsmitTo,
                    ].forEach((i) => setFieldValue(`${path}${i}`, ''));
                    return;
                  }
                  case 'TR': {
                    const from = band[transmitFrom]
                      ? band[transmitFrom]
                      : band[receiveFrom];
                    const to = band[transmitTo]
                      ? band[transmitTo]
                      : band[receiveTo];
                    const kFrom = band[kTramsmitFrom]
                      ? band[kTramsmitFrom]
                      : band[kReceiveFrom];
                    const kTo = band[kTramsmitTo]
                      ? band[kTramsmitTo]
                      : band[kReceiveTo];
                    setFieldValue(`${path}${transmitFrom}`, from);
                    setFieldValue(`${path}${kTramsmitFrom}`, kFrom);
                    setFieldValue(`${path}${receiveFrom}`, from);
                    setFieldValue(`${path}${kReceiveFrom}`, kFrom);
                    setFieldValue(`${path}${transmitTo}`, to);
                    setFieldValue(`${path}${kTramsmitTo}`, kTo);
                    setFieldValue(`${path}${receiveTo}`, to);
                    setFieldValue(`${path}${kReceiveTo}`, kTo);
                    return;
                  }
                }
              };

              return (
                <>
                  <Grid item xs={12}>
                    <AutoCompleteInput
                      name={`${path}${DocumentSearchBandsFields.BAND_DIRECT}`}
                      label={
                        DocumentSearchBandLabels[
                          DocumentSearchBandsFields.BAND_DIRECT
                        ]
                      }
                      options={directOptions}
                      onInputChange={onDirectChange}
                    />
                  </Grid>
                  <Grid item xs={3}>
                    <InputBand position="from" index={index} />
                  </Grid>
                  <Grid item xs={3}>
                    <InputAbbreviation
                      position="from"
                      index={index}
                      options={measures}
                    />
                  </Grid>
                  <Grid item xs={3}>
                    <InputBand position="to" index={index} />
                  </Grid>
                  <Grid item xs={3}>
                    <InputAbbreviation
                      position="to"
                      index={index}
                      options={measures}
                    />
                  </Grid>
                  <Grid item xs={12} display="flex" justifyContent="flex-end">
                    <Button
                      startIcon={<Delete />}
                      onClick={() => arrayHelpers.remove(index)}
                      disabled={vals && vals.length < 2}
                    >
                      Удалить
                    </Button>
                  </Grid>
                  <Grid item xs={12} className={styles.horContainer}>
                    <div className={styles.hor} />
                  </Grid>
                </>
              );
            })}
            <Grid item xs={12} display="flex" justifyContent="flex-end">
              <Button
                startIcon={<AddBox />}
                onClick={() => arrayHelpers.push({})}
                disabled={values?.filters?.frequencyBands?.length > 4}
              >
                Добавить
              </Button>
            </Grid>
          </>
        )}
      />
    </Grid>
  );
};

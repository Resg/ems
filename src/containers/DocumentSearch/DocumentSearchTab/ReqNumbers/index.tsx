import React from 'react';

import { Grid } from '@mui/material';

import { ListInput } from '../../../../components/ListInput';
import { DocumentSearchFields } from '../../../../constants/documentSearch';

export const ReqNumbers: React.FC = () => {
  return (
    <Grid container direction="column" spacing={1} sx={{ mb: 2 }}>
      <Grid item>Список номеров заявок</Grid>
      <Grid item>
        <ListInput
          name={DocumentSearchFields.REQUEST_NUMBERS}
          caseInResults="upper"
          multiline
          rows={15}
        />
      </Grid>
    </Grid>
  );
};

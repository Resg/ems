import React, { useCallback, useEffect, useMemo } from 'react';
import { format } from 'date-fns';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import {
  Checkbox,
  FormControlLabel,
  Grid,
  Radio,
  RadioGroup,
} from '@mui/material';

import { CheckboxInput } from '../../../../components/CheckboxInput';
import { ClerkInput } from '../../../../components/ClerkInput';
import { DateRange } from '../../../../components/DateRange';
import { FieldWrapper } from '../../../../components/FieldWrapper';
import Input from '../../../../components/Input';
import {
  DocumentSearchFields,
  DocumentSearchLabels,
} from '../../../../constants/documentSearch';
import { useGetInnerAddressesQuery } from '../../../../services/api/dictionaries';
import { useGetCalculateDateMutation } from '../../../../services/api/documentSearch';

import styles from './styles.module.scss';

interface InternalResolutionProps {}

export const InternalResolution: React.FC<InternalResolutionProps> = () => {
  const {
    handleChange,
    values = {} as any,
    setFieldValue,
  } = useFormikContext();

  const [calculateDate] = useGetCalculateDateMutation();
  const { data: innerAddresses } = useGetInnerAddressesQuery({});

  const isDate = useMemo(
    () => get(values, DocumentSearchFields.HAVEDATERESOLUTION),
    [values]
  );

  const isDatePlan = useMemo(
    () => get(values, DocumentSearchFields.HAVEDATEPLANRESOLUTION),
    [values]
  );

  const isDateInterim = useMemo(
    () => get(values, DocumentSearchFields.HAVEDATEINTERIMRESOLUTION),
    [values]
  );

  const isDateRemoved = useMemo(
    () => get(values, DocumentSearchFields.HAVEDATERMOVEDRESOLUTION),
    [values]
  );

  const isControlled = useMemo(() => {
    return get(values, DocumentSearchFields.RESOLUTIONCONTROL) || null;
  }, [values]);

  const dateExpires = useMemo(
    () => get(values, DocumentSearchFields.DATEEXPIRES),
    [values]
  );

  const dateResolutionFrom = useMemo(
    () => get(values, DocumentSearchFields.RESOLUTIONPLANDATEFROM),
    [values]
  );

  const innerAddressesOptions = useMemo(() => {
    return (
      innerAddresses?.data?.map((option) => ({
        label: option.name,
        value: option.id,
      })) || []
    );
  }, [innerAddresses]);

  const handleExpireDate = useCallback(async () => {
    if (dateExpires) {
      const response = await calculateDate({
        startDate: dateResolutionFrom || format(new Date(), 'yyyy-MM-dd'),
        numberOfDays: dateExpires,
      });
      if ('data' in response) {
        setFieldValue(
          DocumentSearchFields.RESOLUTIONPLANDATETO,
          response?.data?.date
        );
      }
    }
  }, [dateExpires]);

  const handleChangeNoControl = useCallback(() => {
    isControlled === 'NOT'
      ? setFieldValue(DocumentSearchFields.RESOLUTIONCONTROL, null)
      : setFieldValue(DocumentSearchFields.RESOLUTIONCONTROL, 'NOT');
  }, [isControlled]);

  const handleChangeControl = useCallback(() => {
    isControlled && isControlled !== 'NOT'
      ? setFieldValue(DocumentSearchFields.RESOLUTIONCONTROL, null)
      : setFieldValue(DocumentSearchFields.RESOLUTIONCONTROL, 'ALL');
  }, [isControlled]);

  useEffect(() => {
    if (!isDate) {
      setFieldValue(DocumentSearchFields.RESOLUTIONDATEFROM, '');
      setFieldValue(DocumentSearchFields.RESOLUTIONDATETO, '');
    }
    if (!isDatePlan) {
      setFieldValue(DocumentSearchFields.RESOLUTIONPLANDATEFROM, '');
      setFieldValue(DocumentSearchFields.RESOLUTIONPLANDATETO, '');
      setFieldValue(DocumentSearchFields.DATEEXPIRES, '');
    }
    if (!isDateInterim) {
      setFieldValue(DocumentSearchFields.RESOLUTIONINTERIMDATEFROM, '');
      setFieldValue(DocumentSearchFields.RESOLUTIONINTERIMDATETO, '');
    }
    if (!isDateRemoved) {
      setFieldValue(DocumentSearchFields.REMOVEDCONTROLDATEFROM, '');
      setFieldValue(DocumentSearchFields.REMOVEDCONTROLDATETO, '');
    }
  }, [isDate, isDatePlan, isDateInterim]);

  useEffect(() => {
    handleExpireDate();
  }, [handleExpireDate]);

  return (
    <Grid container rowSpacing={2} columnSpacing={12}>
      <Grid item xs={6}>
        <CheckboxInput
          name={DocumentSearchFields.HAVENORESLUTION}
          label={DocumentSearchLabels[DocumentSearchFields.HAVENORESLUTION]}
        />
      </Grid>
      <Grid item xs={6}>
        <CheckboxInput
          name={DocumentSearchFields.HAVERESOLUTION}
          label={DocumentSearchLabels[DocumentSearchFields.HAVERESOLUTION]}
        />
      </Grid>
      <Grid item xs={12}>
        <ClerkInput
          name={DocumentSearchFields.RESOLUTIONAUTHORS}
          label={DocumentSearchLabels[DocumentSearchFields.RESOLUTIONAUTHORS]}
          multiple
        />
      </Grid>
      <Grid item xs={6} className={styles.dateInput}>
        <FieldWrapper
          name={DocumentSearchFields.HAVEDATERESOLUTION}
          defaultValue={false}
          className={styles.checkbox}
        >
          <FormControlLabel
            label={
              DocumentSearchLabels[DocumentSearchFields.HAVEDATERESOLUTION]
            }
            control={
              <Checkbox
                name={DocumentSearchFields.HAVEDATERESOLUTION}
                checked={
                  get(values, DocumentSearchFields.HAVEDATERESOLUTION) || false
                }
                onChange={() =>
                  setFieldValue(
                    DocumentSearchFields.HAVEDATERESOLUTION,
                    !isDate
                  )
                }
              />
            }
          />
        </FieldWrapper>
        <DateRange
          nameFrom={DocumentSearchFields.RESOLUTIONDATEFROM}
          nameBefore={DocumentSearchFields.RESOLUTIONDATETO}
          labelFrom={
            DocumentSearchLabels[DocumentSearchFields.RESOLUTIONDATEFROM]
          }
          disabled={!isDate}
          className={styles.dateRange}
          clearField
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          name={DocumentSearchFields.RESOLUTIONDESCRPTION}
          label={
            DocumentSearchLabels[DocumentSearchFields.RESOLUTIONDESCRPTION]
          }
          onChange={handleChange}
          value={get(values, DocumentSearchFields.RESOLUTIONDESCRPTION) || ''}
          rows={3}
          multiline
        />
      </Grid>
      <Grid item xs={6}>
        <FormControlLabel
          label={'Неконтрольные'}
          control={
            <Checkbox
              name={DocumentSearchFields.RESOLUTIONCONTROL}
              checked={isControlled === 'NOT'}
              onChange={handleChangeNoControl}
            />
          }
        />
      </Grid>
      <Grid item xs={6}>
        <FormControlLabel
          label={'Контрольные'}
          control={
            <Checkbox
              name={DocumentSearchFields.RESOLUTIONCONTROL}
              checked={isControlled && isControlled !== 'NOT'}
              onChange={handleChangeControl}
            />
          }
        />
      </Grid>
      <Grid item xs={12}>
        Тип контроля:
      </Grid>
      <Grid item xs={12}>
        <RadioGroup
          name={DocumentSearchFields.RESOLUTIONCONTROL}
          value={isControlled}
          defaultValue={'ALL'}
          row
        >
          <Grid item xs={2}>
            <FormControlLabel
              label="Все"
              control={<Radio />}
              disabled={!isControlled || isControlled === 'NOT'}
              onChange={() =>
                setFieldValue(DocumentSearchFields.RESOLUTIONCONTROL, 'ALL')
              }
              onClick={() =>
                isControlled === 'ALL'
                  ? setFieldValue(DocumentSearchFields.RESOLUTIONCONTROL, null)
                  : null
              }
              checked={isControlled === 'ALL'}
            />
          </Grid>
          <Grid item xs={3}>
            <FormControlLabel
              label="Централизованный"
              control={<Radio />}
              disabled={!isControlled || isControlled === 'NOT'}
              onChange={() =>
                setFieldValue(DocumentSearchFields.RESOLUTIONCONTROL, 'F')
              }
              onClick={() =>
                isControlled === 'F'
                  ? setFieldValue(DocumentSearchFields.RESOLUTIONCONTROL, null)
                  : null
              }
              checked={isControlled === 'F'}
            />
          </Grid>
          <Grid item xs={3}>
            <FormControlLabel
              label="Авторский"
              control={<Radio />}
              disabled={!isControlled || isControlled === 'NOT'}
              onChange={() =>
                setFieldValue(DocumentSearchFields.RESOLUTIONCONTROL, 'A')
              }
              onClick={() =>
                isControlled === 'A'
                  ? setFieldValue(DocumentSearchFields.RESOLUTIONCONTROL, null)
                  : null
              }
              checked={isControlled === 'A'}
            />
          </Grid>
        </RadioGroup>
      </Grid>
      <Grid item xs={4} className={styles.dateInput}>
        <FieldWrapper
          name={DocumentSearchFields.HAVEDATEPLANRESOLUTION}
          defaultValue={false}
          className={styles.checkbox}
        >
          <FormControlLabel
            label={
              DocumentSearchLabels[DocumentSearchFields.HAVEDATEPLANRESOLUTION]
            }
            control={
              <Checkbox
                name={DocumentSearchFields.HAVEDATEPLANRESOLUTION}
                checked={
                  get(values, DocumentSearchFields.HAVEDATEPLANRESOLUTION) ||
                  false
                }
                onChange={() =>
                  setFieldValue(
                    DocumentSearchFields.HAVEDATEPLANRESOLUTION,
                    !isDate
                  )
                }
              />
            }
          />
        </FieldWrapper>
        <DateRange
          nameFrom={DocumentSearchFields.RESOLUTIONPLANDATEFROM}
          nameBefore={DocumentSearchFields.RESOLUTIONPLANDATETO}
          labelFrom={
            DocumentSearchLabels[DocumentSearchFields.RESOLUTIONPLANDATEFROM]
          }
          disabled={!isDatePlan}
          className={styles.dateRange}
          clearField
        />
      </Grid>
      <Grid item xs={4}>
        <Input
          name={DocumentSearchFields.DATEEXPIRES}
          label={DocumentSearchLabels[DocumentSearchFields.DATEEXPIRES]}
          onChange={handleChange}
          value={dateExpires || ''}
          type="number"
        />
      </Grid>
      <Grid item xs={4} className={styles.dateInput}>
        <FieldWrapper
          name={DocumentSearchFields.HAVEDATEINTERIMRESOLUTION}
          defaultValue={false}
          className={styles.checkbox}
        >
          <FormControlLabel
            label={
              DocumentSearchLabels[
                DocumentSearchFields.HAVEDATEINTERIMRESOLUTION
              ]
            }
            control={
              <Checkbox
                name={DocumentSearchFields.HAVEDATEINTERIMRESOLUTION}
                checked={
                  get(values, DocumentSearchFields.HAVEDATEINTERIMRESOLUTION) ||
                  false
                }
                onChange={() =>
                  setFieldValue(
                    DocumentSearchFields.HAVEDATEINTERIMRESOLUTION,
                    !isDate
                  )
                }
              />
            }
          />
        </FieldWrapper>
        <DateRange
          nameFrom={DocumentSearchFields.RESOLUTIONINTERIMDATEFROM}
          nameBefore={DocumentSearchFields.RESOLUTIONINTERIMDATETO}
          labelFrom={
            DocumentSearchLabels[DocumentSearchFields.RESOLUTIONINTERIMDATEFROM]
          }
          disabled={!isDateInterim}
          className={styles.dateRange}
          clearField
        />
      </Grid>
      <Grid item xs={12}>
        <ClerkInput
          name={DocumentSearchFields.RESOLUTIONPERFORMERCLERKS}
          label={
            DocumentSearchLabels[DocumentSearchFields.RESOLUTIONPERFORMERCLERKS]
          }
          options={innerAddressesOptions}
          multiple
        />
      </Grid>
      <Grid item xs={6}>
        <CheckboxInput
          name={DocumentSearchFields.ISCHILDRENINCLUDED}
          label={DocumentSearchLabels[DocumentSearchFields.ISCHILDRENINCLUDED]}
        />
      </Grid>
      <Grid item xs={6}>
        <CheckboxInput
          name={DocumentSearchFields.ISRESPONSIBLE}
          label={DocumentSearchLabels[DocumentSearchFields.ISRESPONSIBLE]}
        />
      </Grid>
      <Grid item xs={6}>
        <CheckboxInput
          name={DocumentSearchFields.ISREMOVEDCONTROL}
          label={DocumentSearchLabels[DocumentSearchFields.ISREMOVEDCONTROL]}
        />
      </Grid>
      <Grid item xs={4} className={styles.dateInput}>
        <FieldWrapper
          name={DocumentSearchFields.HAVEDATERMOVEDRESOLUTION}
          defaultValue={false}
          className={styles.checkbox}
        >
          <FormControlLabel
            label={
              DocumentSearchLabels[
                DocumentSearchFields.HAVEDATERMOVEDRESOLUTION
              ]
            }
            control={
              <Checkbox
                name={DocumentSearchFields.HAVEDATERMOVEDRESOLUTION}
                checked={
                  get(values, DocumentSearchFields.HAVEDATERMOVEDRESOLUTION) ||
                  false
                }
                onChange={() =>
                  setFieldValue(
                    DocumentSearchFields.HAVEDATERMOVEDRESOLUTION,
                    !isDate
                  )
                }
              />
            }
          />
        </FieldWrapper>
        <DateRange
          nameFrom={DocumentSearchFields.REMOVEDCONTROLDATEFROM}
          nameBefore={DocumentSearchFields.REMOVEDCONTROLDATETO}
          labelFrom={
            DocumentSearchLabels[DocumentSearchFields.REMOVEDCONTROLDATEFROM]
          }
          disabled={!isDateRemoved}
          className={styles.dateRange}
          clearField
        />
      </Grid>
      <Grid item xs={12}>
        <p>Срок исполнения нарушен:</p>
      </Grid>
      <Grid item xs={6}>
        <Input
          name={DocumentSearchFields.RESOLUTIONDELAYFROM}
          label={DocumentSearchLabels[DocumentSearchFields.RESOLUTIONDELAYFROM]}
          onChange={handleChange}
          value={get(values, DocumentSearchFields.RESOLUTIONDELAYFROM) || ''}
          type="number"
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          name={DocumentSearchFields.RESOLUTIONDELAYTO}
          label={DocumentSearchLabels[DocumentSearchFields.RESOLUTIONDELAYTO]}
          onChange={handleChange}
          value={get(values, DocumentSearchFields.RESOLUTIONDELAYTO) || ''}
          type="number"
        />
      </Grid>
    </Grid>
  );
};

import React, { useCallback, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import {
  matchPath,
  useLocation,
  useNavigate,
  useSearchParams,
} from 'react-router-dom';

import {
  AddCircleOutline,
  ChevronRight,
  DeleteOutline,
  Refresh,
} from '@mui/icons-material';
import { Box } from '@mui/material';

import { DataTable } from '../../../components/CustomDataGrid';
import { RoundButton } from '../../../components/RoundButton';
import {
  DeleteToolButton,
  ToolButton,
  ToolsPanel,
} from '../../../components/ToolsPanel';
import { Routes } from '../../../constants/routes';
import {
  useDeleteAppendiceMutation,
  useGetConclusionAppendicesQuery,
} from '../../../services/api/conclusions';
import { RootState } from '../../../store';
import { ConclusionAppendice } from '../../../types/conclusions';
import { appendicesToTableAdapter } from '../../../utils/conclusions';

import styles from './styles.module.scss';

export const ConclusionsAppendices = () => {
  const navigate = useNavigate();
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const [searchParams] = useSearchParams();
  const location = useLocation();
  const id = Number(
    matchPath(Routes.CONCLUSIONS.CONCLUSION_TABS, location.pathname)?.params
      ?.id || ''
  );
  const { data, refetch, isLoading } = useGetConclusionAppendicesQuery(
    { id, page, size: perPage },
    { skip: !id }
  );
  const { cols, rows } = useMemo(
    () => appendicesToTableAdapter(data?.data),
    [data]
  );
  const [deleteAppendice] = useDeleteAppendiceMutation();
  const selectedArr = useMemo(
    () => Object.keys(selected).filter((key) => selected[Number(key)]),
    [selected]
  );

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const handleDetails = useCallback(() => {
    window
      .open(
        `/conclusions/${id}/appendices/${selectedArr[0]}`,
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  }, [selectedArr, openFormInNewTab]);

  const handleDelete = useCallback(async () => {
    await deleteAppendice({ id: Number(selectedArr[0]) });
    setSelected({});
    refetch();
  }, [selectedArr]);

  const handleDoubleClick = useCallback(
    (attachment: Partial<ConclusionAppendice>) => {
      if (attachment.id && id) {
        window
          .open(
            `/conclusions/${id}/appendices/${attachment.id}`,
            openFormInNewTab ? '_blank' : '_self'
          )
          ?.focus();
      }
    },
    [id, openFormInNewTab]
  );

  return (
    <div className={styles.content}>
      <Box my={3}></Box>
      <DataTable
        formKey={'conslutions_appendices'}
        rows={rows}
        cols={cols}
        onRowSelect={setSelected}
        onRowDoubleClick={handleDoubleClick}
        loading={isLoading}
        height="fullHeight"
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: data?.totalCount || 0,
        }}
      >
        <ToolsPanel
          leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
        >
          <ToolButton
            label={'Подробно'}
            startIcon={<ChevronRight />}
            onClick={handleDetails}
            disabled={selectedArr.length !== 1}
            fast
          />
          <ToolButton
            label={'Добавить'}
            startIcon={<AddCircleOutline />}
            onClick={() =>
              window.open(
                `/conclusions/${id}/appendices/`,
                openFormInNewTab ? '_blank' : '_self'
              )
            }
            fast
          />
          <DeleteToolButton
            label={'Удалить'}
            startIcon={<DeleteOutline />}
            disabled={selectedArr.length !== 1}
            fast
            description={`Вы действительно хотите удалить приложение?`}
            title={'Удаление приложения'}
            onConfirm={handleDelete}
          />
        </ToolsPanel>
      </DataTable>
      ;
    </div>
  );
};

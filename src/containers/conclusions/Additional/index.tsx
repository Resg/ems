import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import {
  matchPath,
  useLocation,
  useNavigate,
  useSearchParams,
} from 'react-router-dom';

import {
  AddCircleOutline,
  Apps,
  ArrowDownward,
  ArrowUpward,
  ChevronRight,
  DeleteOutline,
  Pin,
} from '@mui/icons-material';
import { Box } from '@mui/material';

import { AdditionalConditionAddForm } from '../../../components/AdditionalConditionAddForm';
import { AdditionalConditionCreateForm } from '../../../components/AdditionalConditionCreateForm';
import { ConclusionAdditionsButton } from '../../../components/ConclusionAdditionals/ConclusionAdditionsButton';
import { DataTable } from '../../../components/CustomDataGrid';
import { RoundButton } from '../../../components/RoundButton';
import {
  DeleteToolButton,
  ToolButton,
  ToolsPanel,
} from '../../../components/ToolsPanel';
import { Routes } from '../../../constants/routes';
import {
  useDeleteAdditionalsMutation,
  useGetConclusionAdditionalsQuery,
  useRecountAdditionalsMutation,
} from '../../../services/api/conclusions';
import { RootState } from '../../../store';
import { ConclusionAdditional } from '../../../types/conclusions';
import { additionalsToTableAdapter } from '../../../utils/conclusions';

import styles from './styles.module.scss';

export const ConclusionsAdditional = () => {
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();
  const location = useLocation();
  const id = Number(
    matchPath(Routes.CONCLUSIONS.CONCLUSION_TABS, location.pathname)?.params
      ?.id || ''
  );
  const { data, refetch, isLoading } = useGetConclusionAdditionalsQuery(
    { id },
    { skip: !id }
  );
  const { cols, rows } = additionalsToTableAdapter(data?.data);
  const [move, setMove] = useState(rows);
  const [deleteAdditionals] = useDeleteAdditionalsMutation();
  const [recountAdditionals] = useRecountAdditionalsMutation();
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const selectedArr = useMemo(
    () => Object.keys(selected).filter((key) => selected[Number(key)]),
    [selected]
  );

  const selectedArrNum = useMemo(() => selectedArr.map(Number), [selectedArr]);

  const [arrUpDisable, setArrUpDisable] = useState(false);
  const [arrDownDisable, setArrDownDisable] = useState(false);
  const [recount, setRecount] = useState(false);
  const [additionalForm, setAdditionalForm] = useState(false);
  const [additionalSaveForm, setAdditionalSaveForm] = useState(false);
  const [tableFilters, setTableFilters] = useState(false);
  useEffect(() => {
    setMove(rows);
  }, [data]);

  useEffect(() => {
    const indexes = selectedArr.map((o) =>
      move.findIndex((i) => i.id === Number(o))
    );

    if (!indexes.length) {
      setArrUpDisable(true);
      setArrDownDisable(true);
    } else {
      setArrUpDisable(false);
      setArrDownDisable(false);
    }

    if (indexes.includes(0)) setArrUpDisable(true);
    if (indexes.includes(move.length - 1)) setArrDownDisable(true);
  }, [selectedArr, move]);

  const handleArrUp = useCallback(() => {
    const arr = [...move];

    arr.forEach((i, idx) => {
      if (selectedArr.includes(`${i.id}`)) {
        [arr[idx], arr[idx - 1]] = [arr[idx - 1], arr[idx]];
      }
    });

    setMove(arr);
    setRecount(true);
  }, [selectedArr, move]);

  const handleArrDown = useCallback(() => {
    const arr = [...move].reverse();

    arr.forEach((i, idx) => {
      if (selectedArr.includes(`${i.id}`)) {
        [arr[idx], arr[idx - 1]] = [arr[idx - 1], arr[idx]];
      }
    });
    arr.reverse();

    setMove(arr);
    setRecount(true);
  }, [selectedArr, move]);

  const handleRecount = async () => {
    await recountAdditionals({ id: id, ids: move.map((i) => i.id) });
    refetch();
    setRecount(false);
  };
  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );
  const handleDetails = useCallback(() => {
    window
      .open(
        `/conclusions/additionals/${selectedArr[0]}`,
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  }, [selectedArr, openFormInNewTab]);

  const handleCloseAdditionalTemplateModal = useCallback(() => {
    setAdditionalForm(false);
    refetch();
  }, [refetch]);

  const handleCloseAdditionalSaveTemplateModal = useCallback(() => {
    setAdditionalSaveForm(false);
    refetch();
  }, [refetch]);

  const handleDelete = useCallback(async () => {
    await deleteAdditionals({ ids: selectedArr });
    setSelected({});
    refetch();
  }, [deleteAdditionals, refetch, selectedArr]);

  const handleDoubleClick = useCallback(
    (condition: Partial<ConclusionAdditional>) => {
      if (condition.id) {
        window
          .open(
            `/conclusions/additionals/${condition.id}`,
            openFormInNewTab ? '_blank' : '_self'
          )
          ?.focus();
      }
    },
    [openFormInNewTab]
  );

  return (
    <div className={styles.content}>
      <Box my={3}></Box>
      <DataTable
        formKey={'conslutions_additional'}
        rows={move}
        cols={cols}
        onRowSelect={setSelected}
        height="fullHeight"
        onRowDoubleClick={handleDoubleClick}
        initialSelected={selected}
        loading={isLoading}
        showFilters={tableFilters}
      >
        <ToolsPanel
          setFilters={setTableFilters}
          leftActions={
            <>
              <RoundButton
                icon={<ArrowUpward />}
                onClick={handleArrUp}
                disabled={arrUpDisable}
              />
              <RoundButton
                icon={<ArrowDownward />}
                onClick={handleArrDown}
                disabled={arrDownDisable}
              />
              <RoundButton
                icon={<Pin />}
                onClick={handleRecount}
                disabled={!recount}
              />
            </>
          }
        >
          <ToolButton
            label={'Подробно'}
            startIcon={<ChevronRight />}
            disabled={selectedArr.length !== 1}
            onClick={handleDetails}
            fast
          />
          <ConclusionAdditionsButton
            label={'Добавить'}
            fast
            id={Number(id)}
            startIcon={<AddCircleOutline />}
            onConfirm={refetch}
          />
          <ToolButton
            label={'По набору'}
            startIcon={<Apps />}
            onClick={() => setAdditionalForm(true)}
            fast
          />
          <DeleteToolButton
            label={'Удалить'}
            startIcon={<DeleteOutline />}
            disabled={selectedArr.length === 0}
            fast
            description={`Вы действительно хотите удалить ${
              selectedArr.length > 1
                ? 'дополнительные условия'
                : 'дополнительное условие'
            }?`}
            title={'Удаление дополнительных условий'}
            onConfirm={handleDelete}
          />
          <ToolButton
            label={'Сохранить в набор'}
            startIcon={<AddCircleOutline />}
            onClick={() => setAdditionalSaveForm(true)}
            disabled={!selectedArr.length}
          />
        </ToolsPanel>
      </DataTable>
      <AdditionalConditionAddForm
        open={additionalForm}
        onClose={handleCloseAdditionalTemplateModal}
      />
      <AdditionalConditionCreateForm
        open={additionalSaveForm}
        onClose={handleCloseAdditionalSaveTemplateModal}
        selectedArrNum={selectedArrNum}
      />
    </div>
  );
};

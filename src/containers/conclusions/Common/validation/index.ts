import { array, number, object, string } from 'yup';

import { ConclusionFormFields } from '../../../../constants/conclusions';

export const validationSchema = object({
  [ConclusionFormFields.NUMBER]: string().required(),
  [ConclusionFormFields.CLIENT]: array(number()).required(),
  [ConclusionFormFields.DATE]: string().nullable(),
  [ConclusionFormFields.END_DATE]: string().nullable(),
  [ConclusionFormFields.PERIOD]: string().nullable(),
  [ConclusionFormFields.STATUS]: string().nullable(),
  [ConclusionFormFields.TYPE]: string().required(),
  [ConclusionFormFields.SORT]: number().required(),
  [ConclusionFormFields.SIGN]: number().required(),
  [ConclusionFormFields.STATE]: string().nullable(),
  [ConclusionFormFields.DATE_ANNULMENT]: string().nullable(),
  [ConclusionFormFields.REASON]: string().nullable(),
  [ConclusionFormFields.COMMENT]: string().nullable(),
});

import React, { useCallback, useMemo, useState } from 'react';
import { Form, Formik } from 'formik';
import {
  matchPath,
  useLocation,
  useNavigate,
  useSearchParams,
} from 'react-router-dom';

import {
  Edit,
  EditOff,
  InsertDriveFile,
  PersonPin,
  Preview,
  Save,
} from '@mui/icons-material';
import { Box, Grid } from '@mui/material';

import { AutoCompleteInput } from '../../../components/AutoCompleteInput';
import { ClerkInput } from '../../../components/ClerkInput';
import { ContractorSearchForm } from '../../../components/ContractorSearchForm';
import { CreateDocumentToolButton } from '../../../components/CreateDocumentByTemplateForm/CreateDocumentToolButton';
import { DatePickerInput } from '../../../components/DatePickerInput';
import { DebounceAutoComplete } from '../../../components/DebounceAutoComplete';
import Input from '../../../components/Input';
import { RoundButton } from '../../../components/RoundButton';
import { ToolsPanel } from '../../../components/ToolsPanel';
import {
  ConclusionFormDefaults,
  ConclusionFormFields,
  ConclusionFormLabels,
} from '../../../constants/conclusions';
import { Routes } from '../../../constants/routes';
import {
  useCreateConclusionMutation,
  useGetConclusionContractorsQuery,
  useGetConclusionDataQuery,
  useUpdateConclusionMutation,
} from '../../../services/api/conclusions';
import {
  useGetAsyncContractorsListMutation,
  useGetConclusionAnnulmentsQuery,
  useGetConclusionPeriodsQuery,
  useGetConclusionSortsQuery,
  useGetConclusionStatesQuery,
  useGetConclusionStatusesQuery,
  useGetConclusionTypesQuery,
  useGetCounterpartiesAsyncMutation,
} from '../../../services/api/dictionaries';
import {
  useGetConclusionNumberQuery,
  useGetConclusionSignersQuery,
  useGetRequestContractorsQuery,
} from '../../../services/api/request';
import { ConclusionFormType } from '../../../types/conclusions';
import { Counterparty } from '../../../types/dictionaries';
import { ConclusionClient } from '../../../types/requests';

import { validationSchema } from './validation';

import styles from './styles.module.scss';

export const ConclusionsCommon = () => {
  const [searchParams] = useSearchParams();
  const location = useLocation();
  const id = Number(
    matchPath(Routes.CONCLUSIONS.CONCLUSION_TABS, location.pathname)?.params
      ?.id || ''
  );
  const reqId = Number(searchParams.get('reqId'));
  const [editMode, setEditMode] = useState(!id);
  const [openModal, setOpenModal] = useState(false);
  const { data, refetch } = useGetConclusionDataQuery({ id }, { skip: !id });
  const { data: periods = [] } = useGetConclusionPeriodsQuery({});
  const { data: statuses = [] } = useGetConclusionStatusesQuery({});
  const { data: types = [] } = useGetConclusionTypesQuery({});
  const { data: sorts = [] } = useGetConclusionSortsQuery({});
  const { data: states = [] } = useGetConclusionStatesQuery({});
  const { data: reasons = [] } = useGetConclusionAnnulmentsQuery({});
  const [update] = useUpdateConclusionMutation();
  const [create] = useCreateConclusionMutation();
  const [getCounterparties] = useGetCounterpartiesAsyncMutation();
  const [getContractorsList] = useGetAsyncContractorsListMutation();
  const navigate = useNavigate();

  const { data: conclusionContractor } = useGetConclusionContractorsQuery(
    { conclusionId: id },
    { skip: !id }
  );

  const contractors = useMemo(
    () =>
      conclusionContractor && conclusionContractor.length
        ? conclusionContractor.map((el) => el.id)
        : [],
    [conclusionContractor]
  );

  const { data: signer } = useGetConclusionSignersQuery(
    { id: reqId },
    { skip: !reqId }
  );
  const { data: num } = useGetConclusionNumberQuery(
    { id: reqId },
    { skip: !reqId }
  );

  const { data: defaultContractor = '' } = useGetRequestContractorsQuery(
    { requestId: reqId },
    { skip: !reqId }
  );

  const initialValues = useMemo(
    () => ({
      ...(data || ConclusionFormDefaults),
      signerId: (reqId ? signer?.id : data?.signerId) || null,
      number: reqId ? num?.number || '' : data?.number || '',
      clientId: undefined,
      contractorIds: reqId
        ? defaultContractor?.data?.map((el: ConclusionClient) => el.id)
        : contractors,
      typeCode: data?.typeCode || 'Y',
      sortId: data?.sortId || 1,
    }),
    [reqId, defaultContractor, data, signer, num, contractors]
  );

  const handleSubmit = async (values: ConclusionFormType) => {
    if (!id) {
      const createData = {
        ...values,
        requestId: reqId,
        isMarine: null,
        text: 'NULL',
      };

      const response = await create({ data: createData });
      if ('data' in response) {
        navigate(`/conclusions?id=${response.data.id}`);
        window.location.reload();
      }
    } else {
      const updateData = {
        ...values,
        id: id,
        text: 'NULL',
      };

      await update({ id: id, data: updateData });
    }
    setEditMode(false);
    window.location.reload();
  };

  const getOptions = useCallback(async (value: string) => {
    if (value) {
      const response = await getContractorsList({
        name: value,
        isOnlyActual: true,
      });
      if ('data' in response) {
        return response.data.data.map((option: Record<string, any>) => {
          return {
            label: option.name,
            value: option.id,
          };
        });
      }
      return [];
    }
    return [];
  }, []);

  const getValueOptions = useCallback(
    async (ids: number[]) => {
      if (ids && ids.length) {
        const counterparties = await getCounterparties({ ids });
        if ('data' in counterparties) {
          return (
            counterparties?.data.data.map((option: Counterparty) => {
              return {
                label: option.name,
                value: option.id,
              };
            }) || []
          );
        } else {
          return [];
        }
      }
      return [];
    },
    [getCounterparties]
  );

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      enableReinitialize
      onSubmit={handleSubmit}
    >
      {({
        values,
        handleChange,
        handleSubmit,
        errors,
        setFieldValue,
        handleReset,
      }) => {
        const handleAdd = (value: number[]) => {
          setFieldValue(ConclusionFormFields.CLIENT, value[0]);
        };

        return (
          <Form>
            <Box my={3}>
              <ToolsPanel
                leftActions={
                  !editMode ? (
                    <RoundButton
                      icon={<Edit />}
                      onClick={() => setEditMode(true)}
                    />
                  ) : (
                    <>
                      <RoundButton
                        icon={<Save />}
                        onClick={() => handleSubmit()}
                      />
                      <RoundButton
                        icon={<EditOff />}
                        onClick={() => {
                          handleReset();
                          setEditMode(false);
                        }}
                      />
                    </>
                  )
                }
              >
                <CreateDocumentToolButton
                  label={'Сформировать документ'}
                  startIcon={<InsertDriveFile />}
                  disabled={editMode}
                  fast
                  objectTypeCode={'CONCLUSION'}
                  objectIds={String(id)}
                  onDocumentCreated={refetch}
                  id={'1'}
                />
                <CreateDocumentToolButton
                  label={'Предпросмотр документа'}
                  startIcon={<Preview />}
                  disabled={editMode}
                  fast
                  objectTypeCode={'CONCLUSION'}
                  objectIds={String(id)}
                  onDocumentCreated={refetch}
                  isPreview
                />
              </ToolsPanel>
            </Box>
            <Grid container columns={6} columnSpacing={9} rowSpacing={3}>
              <Grid item xs={2}>
                <Input
                  name={ConclusionFormFields.NUMBER}
                  label={ConclusionFormLabels[ConclusionFormFields.NUMBER]}
                  disabled={!editMode}
                  value={values[ConclusionFormFields.NUMBER]}
                  onChange={handleChange}
                  required={!id}
                />
              </Grid>
              <Grid item xs={2}>
                <DatePickerInput
                  label={ConclusionFormLabels[ConclusionFormFields.DATE]}
                  name={ConclusionFormFields.DATE}
                  disabled={!editMode}
                />
              </Grid>
              <Grid item xs={2}>
                <DatePickerInput
                  label={ConclusionFormLabels[ConclusionFormFields.END_DATE]}
                  name={ConclusionFormFields.END_DATE}
                  disabled={!editMode}
                />
              </Grid>
              <Grid item xs={6} className={styles.container}>
                <DebounceAutoComplete
                  name={ConclusionFormFields.CLIENT}
                  label={ConclusionFormLabels[ConclusionFormFields.CLIENT]}
                  getOptions={getOptions}
                  getValueOptions={getValueOptions}
                  disabled={!editMode}
                  required={!id}
                  multiple
                />

                <RoundButton
                  icon={<PersonPin />}
                  onClick={() => setOpenModal(true)}
                  disabled={!editMode}
                />

                <ContractorSearchForm
                  openClerkModal={openModal}
                  onClose={() => setOpenModal(false)}
                  addDoc={handleAdd}
                />
              </Grid>
              <Grid item xs={3}>
                <AutoCompleteInput
                  name={ConclusionFormFields.PERIOD}
                  label={ConclusionFormLabels[ConclusionFormFields.PERIOD]}
                  options={periods}
                  disabled={!editMode}
                />
              </Grid>
              <Grid item xs={3}>
                <AutoCompleteInput
                  name={ConclusionFormFields.STATUS}
                  label={ConclusionFormLabels[ConclusionFormFields.STATUS]}
                  options={statuses}
                  disabled={!editMode}
                />
              </Grid>
              <Grid item xs={3}>
                <AutoCompleteInput
                  name={ConclusionFormFields.TYPE}
                  label={ConclusionFormLabels[ConclusionFormFields.TYPE]}
                  options={types}
                  disabled={!editMode}
                  required={!id}
                />
              </Grid>
              <Grid item xs={3}>
                <AutoCompleteInput
                  name={ConclusionFormFields.SORT}
                  label={ConclusionFormLabels[ConclusionFormFields.SORT]}
                  options={sorts}
                  disabled={!editMode}
                  required={!id}
                />
              </Grid>
              <Grid item xs={3}>
                <ClerkInput
                  name={ConclusionFormFields.SIGN}
                  label={ConclusionFormLabels[ConclusionFormFields.SIGN]}
                  disabled={!editMode}
                  required={!id}
                />
              </Grid>
              <Grid item xs={3}>
                <AutoCompleteInput
                  name={ConclusionFormFields.STATE}
                  label={ConclusionFormLabels[ConclusionFormFields.STATE]}
                  options={states}
                  disabled={!editMode}
                />
              </Grid>
              <Grid item xs={3}>
                <DatePickerInput
                  label={
                    ConclusionFormLabels[ConclusionFormFields.DATE_ANNULMENT]
                  }
                  name={ConclusionFormFields.DATE_ANNULMENT}
                  disabled={!editMode}
                />
              </Grid>
              <Grid item xs={3}>
                <AutoCompleteInput
                  name={ConclusionFormFields.REASON}
                  label={ConclusionFormLabels[ConclusionFormFields.REASON]}
                  options={reasons}
                  disabled={!editMode}
                />
              </Grid>
              <Grid item xs={6}>
                <Input
                  name={ConclusionFormFields.COMMENT}
                  label={ConclusionFormLabels[ConclusionFormFields.COMMENT]}
                  multiline
                  value={values[ConclusionFormFields.COMMENT]}
                  onChange={handleChange}
                  disabled={!editMode}
                />
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

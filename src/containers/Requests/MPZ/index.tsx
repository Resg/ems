import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Form, Formik } from 'formik';

import {
  AddCircleOutline,
  ChevronRight,
  Edit,
  EditOff,
  Refresh,
  Save,
} from '@mui/icons-material';
import { Grid, Stack } from '@mui/material';

import { AutoCompleteInput } from '../../../components/AutoCompleteInput';
import { DataTable } from '../../../components/CustomDataGrid/DataTable';
import { DatePickerInput } from '../../../components/DatePickerInput';
import Input from '../../../components/Input';
import { RoundButton } from '../../../components/RoundButton';
import { ToolButton, ToolsPanel } from '../../../components/ToolsPanel';
import { DeleteToolButton } from '../../../components/ToolsPanel/DeleteToolButton';
import {
  CountriesTableCols,
  DefaultMPZ,
  MPZFields,
  MPZLabels,
} from '../../../constants/MPZ';
import {
  useGetMPZStatesQuery,
  useGetRegistrationStatesQuery,
} from '../../../services/api/dictionaries';
import {
  useCreateMPZDataMutation,
  useDeleteCoordinationCountryMutation,
  useGetCoordinationCountriesQuery,
  useGetMPZDataQuery,
  usePutMPZDataMutation,
} from '../../../services/api/request';
import { CoordinationCoutry } from '../../../types/requests';

import ContrySelectionPopup from './Popup';

import styles from './styles.module.scss';

interface MPZProps {
  requestId: number;
  ilpScreenFormCode?: string;
}

export const MPZ: React.FC<MPZProps> = ({ requestId, ilpScreenFormCode }) => {
  const [editMode, setEditMode] = useState<boolean>(false);
  const [page, setPage] = useState(1);
  const [size, setSize] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const [popupOpen, setPopupOpen] = useState<'create' | 'edit' | null>(null);
  const [coordinationId, setCoordinationId] = useState<number>(0);

  const { data: regStates } = useGetRegistrationStatesQuery();
  const { data: MPZStates } = useGetMPZStatesQuery();
  const { data: formData, refetch: refetchFormData } = useGetMPZDataQuery({
    requestId,
  });
  const {
    data: tableData,
    refetch: refetchTableData,
    isLoading,
  } = useGetCoordinationCountriesQuery(
    { ilpId: formData && formData.id, page, size },
    { skip: !formData }
  );
  const [saveMPZ] = usePutMPZDataMutation();
  const [createMPZ] = useCreateMPZDataMutation();
  const [deleteCountry] = useDeleteCoordinationCountryMutation();

  useEffect(() => {
    setSelected({});
  }, [tableData]);

  const rows = useMemo(() => {
    return (
      tableData?.data.map(
        ({ id, orderNumber, countryName, sendDate, receiveDate, status }) => {
          return {
            id,
            orderNumber,
            countryName,
            sendDate: sendDate && new Date(sendDate).toLocaleDateString(),
            receiveDate:
              receiveDate && new Date(receiveDate).toLocaleDateString(),
            status,
          };
        }
      ) || []
    );
  }, [tableData?.data]);

  const selectedCountries = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  useEffect(() => {
    if (selectedCountries[0]) {
      setCoordinationId(selectedCountries[0]);
    }
  }, [selectedCountries]);

  const initialValues = useMemo(() => {
    if (formData) {
      return {
        sendDate: formData.sendDate,
        receiveDate: formData.receiveDate,
        completionDate: formData.completionDate,
        registrationStateCode: formData.registrationStateCode,
        comments: formData.comments,
        stateCode: formData.stateCode,
        administrationId: formData.administrationId,
      };
    }
    return DefaultMPZ;
  }, [formData]);

  const handleSubmit = useCallback(
    async (values = {} as any) => {
      if (formData) {
        saveMPZ({
          data: values,
          requestId: formData.requestId,
          id: formData.id,
        });
      } else {
        const response = await createMPZ({ data: values, requestId });
        if ('data' in response) {
          refetchFormData();
        }
      }
      setEditMode(false);
    },
    [formData]
  );

  const handleDelete = useCallback(() => {
    deleteCountry({ coordinationId: selectedCountries[0] });
  }, [selectedCountries]);

  const sendDateLabel = useMemo(() => {
    if (ilpScreenFormCode === 'COORDINATION_REQUEST')
      return MPZLabels[MPZFields.SEND_DATE];
    if (ilpScreenFormCode === 'BROADCASTING_SERVICE')
      return MPZLabels[MPZFields.PUBLISH_SENT];
    return MPZLabels[MPZFields.REQUEST_SENT];
  }, [ilpScreenFormCode]);

  const receiveDateLabel = useMemo(() => {
    if (ilpScreenFormCode === 'COORDINATION_REQUEST')
      return MPZLabels[MPZFields.RECEIVE_DATE];
    if (ilpScreenFormCode === 'BROADCASTING_SERVICE')
      return MPZLabels[MPZFields.PUBLISH_DATE];
    return MPZLabels[MPZFields.REQUEST_RECEIVED];
  }, [ilpScreenFormCode]);

  const handleDoubleClick = useCallback(
    (country: Partial<CoordinationCoutry>) => {
      if (country.id) {
        setCoordinationId(country.id);
        setPopupOpen('edit');
      }
    },
    []
  );

  return (
    <>
      <Formik
        initialValues={initialValues}
        enableReinitialize
        onSubmit={handleSubmit}
      >
        {({ values, handleSubmit, handleReset, handleChange }) => {
          return (
            <>
              <Stack className={styles.stack}>
                {editMode ? (
                  <>
                    <RoundButton
                      onClick={() => handleSubmit()}
                      icon={<Save />}
                    />
                    <RoundButton
                      onClick={() => {
                        handleReset();
                        setEditMode(false);
                      }}
                      icon={<EditOff />}
                    />
                  </>
                ) : (
                  <RoundButton
                    onClick={() => setEditMode(true)}
                    icon={<Edit />}
                  />
                )}
              </Stack>
              <Form>
                <Grid container spacing={2}>
                  <Grid item xs={4}>
                    <AutoCompleteInput
                      name={MPZFields.STATE_CODE}
                      label={
                        ilpScreenFormCode === 'COORDINATION_REQUEST'
                          ? MPZLabels[MPZFields.STATE_CODE]
                          : MPZLabels[MPZFields.MPZ]
                      }
                      options={MPZStates || []}
                      disabled={!editMode}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <DatePickerInput
                      name={MPZFields.COMPLETION_DATE}
                      label={
                        ilpScreenFormCode === 'COORDINATION_REQUEST'
                          ? MPZLabels[MPZFields.COMPLETION_DATE]
                          : MPZLabels[MPZFields.DATE]
                      }
                      disabled={!editMode}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <DatePickerInput
                      name={MPZFields.SEND_DATE}
                      label={sendDateLabel}
                      disabled={!editMode}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <Input
                      name={MPZFields.ADMINISTRATION_ID}
                      label={MPZLabels[MPZFields.ADMINISTRATION_ID]}
                      value={values[MPZFields.ADMINISTRATION_ID]}
                      onChange={handleChange}
                      disabled={!editMode}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <DatePickerInput
                      name={MPZFields.RECEIVE_DATE}
                      label={receiveDateLabel}
                      disabled={!editMode}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <AutoCompleteInput
                      name={MPZFields.REGISTRATION_STATE_CODE}
                      label={MPZLabels[MPZFields.REGISTRATION_STATE_CODE]}
                      options={regStates || []}
                      disabled={!editMode}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Input
                      name={MPZFields.COMMENTS}
                      label={MPZLabels[MPZFields.COMMENTS]}
                      value={values[MPZFields.COMMENTS]}
                      disabled={!editMode}
                      onChange={handleChange}
                      multiline
                    />
                  </Grid>
                </Grid>
              </Form>
            </>
          );
        }}
      </Formik>
      <div className={styles.hor} />
      <div className={styles.label}>Страны</div>

      <DataTable
        formKey={'countries_table'}
        cols={CountriesTableCols}
        rows={rows}
        onRowSelect={setSelected}
        onRowDoubleClick={handleDoubleClick}
        loading={isLoading}
        height="216px"
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: size,
          setPerPage: setSize,
          total: tableData?.totalCount || 0,
        }}
        className={styles.table}
      >
        <ToolsPanel
          className={styles.toolBar}
          leftActions={
            <RoundButton icon={<Refresh />} onClick={refetchTableData} />
          }
        >
          <ToolButton
            label="Подробно"
            endIcon={<ChevronRight />}
            disabled={selectedCountries.length !== 1}
            onClick={() => setPopupOpen('edit')}
            fast
          />
          <ToolButton
            label="Добавить"
            startIcon={<AddCircleOutline />}
            onClick={async () => {
              if (!formData) {
                handleSubmit(initialValues);
              }
              setPopupOpen('create');
            }}
            fast
          />
          <DeleteToolButton
            label="Удалить"
            title="Удаление координации"
            description="Вы действительно хотите удалить координацию?"
            disabled={selectedCountries.length !== 1}
            onConfirm={handleDelete}
            fast
          />
        </ToolsPanel>
      </DataTable>
      <ContrySelectionPopup
        open={popupOpen}
        setOpen={setPopupOpen}
        coordinationId={coordinationId}
        ilpId={formData?.id}
      />
    </>
  );
};

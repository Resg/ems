import React, { useCallback, useMemo, useRef } from 'react';
import { Form, Formik, FormikProps, FormikValues } from 'formik';

import { Close, Save } from '@mui/icons-material';
import { Button, Grid, Stack } from '@mui/material';

import { AutoCompleteInput } from '../../../../components/AutoCompleteInput';
import { DatePickerInput } from '../../../../components/DatePickerInput';
import Input from '../../../../components/Input';
import { Popup } from '../../../../components/Popup';
import {
  CountrySelectionFields,
  CountrySelectionLabels,
  DefaultCountrySelection,
} from '../../../../constants/MPZ';
import {
  useGetCoordinationStatusesQuery,
  useGetCountriesQuery,
} from '../../../../services/api/dictionaries';
import {
  useAddCoordinationCountryMutation,
  useGetCoordinationCountryQuery,
  useSaveCoordinationCountryMutation,
} from '../../../../services/api/request';

interface ContrySelectionPopupProps {
  open: 'create' | 'edit' | null;
  setOpen: (open: 'create' | 'edit' | null) => void;
  coordinationId: number;
  ilpId?: number;
}

const ContrySelectionPopup: React.FC<ContrySelectionPopupProps> = ({
  open,
  setOpen,
  coordinationId,
  ilpId,
}) => {
  const { data: countries = [] } = useGetCountriesQuery();
  const { data: statuses = [] } = useGetCoordinationStatusesQuery();
  const [addCoordination] = useAddCoordinationCountryMutation();
  const [saveCoordination] = useSaveCoordinationCountryMutation();
  const { data } = useGetCoordinationCountryQuery(
    { coordinationId },
    { skip: open !== 'edit' || !coordinationId }
  );

  const formRef = useRef<FormikProps<FormikValues>>(null);

  const handleSubmit = useCallback(() => {
    const formValues = formRef?.current?.values;
    if (formValues) {
      const { countryId, sendDate, receiveDate, comments, statusCode } =
        formValues;
      const sendData = formValues && {
        countryId,
        sendDate,
        receiveDate,
        comments,
        statusCode,
      };
      if (open == 'edit' && coordinationId && ilpId && sendData) {
        saveCoordination({ data: sendData, ilpId, coordinationId });
      }
      if (open !== 'edit' && sendData && ilpId) {
        addCoordination({ data: sendData, ilpId });
      }
      setOpen(null);
    }
  }, [formRef?.current?.values, open, data, coordinationId, ilpId]);

  const bar = useMemo(() => {
    return (
      <Stack
        direction="row"
        justifyContent="flex-end"
        spacing={1.5}
        style={{ width: '100%' }}
      >
        <Button
          variant="contained"
          startIcon={<Save />}
          onClick={() => handleSubmit()}
        >
          Сохранить
        </Button>
        <Button
          variant="outlined"
          startIcon={<Close />}
          onClick={() => setOpen(null)}
        >
          Отмена
        </Button>
      </Stack>
    );
  }, [handleSubmit]);

  return (
    <Popup
      open={Boolean(open)}
      onClose={() => setOpen(null)}
      title="Страна-координатор"
      width={750}
      height={600}
      bar={bar}
    >
      <Formik
        initialValues={
          open === 'edit'
            ? data || DefaultCountrySelection
            : DefaultCountrySelection
        }
        enableReinitialize
        innerRef={formRef}
        onSubmit={handleSubmit}
      >
        {({ values, handleChange }) => {
          return (
            <Form>
              <Grid container spacing={2}>
                <Grid item xs={12} sx={{ mt: 3 }}>
                  <AutoCompleteInput
                    name={CountrySelectionFields.COUNTRY_ID}
                    label={
                      CountrySelectionLabels[CountrySelectionFields.COUNTRY_ID]
                    }
                    options={countries}
                  />
                </Grid>
                <Grid item xs={6}>
                  <DatePickerInput
                    name={CountrySelectionFields.SEND_DATE}
                    label={
                      CountrySelectionLabels[CountrySelectionFields.SEND_DATE]
                    }
                  />
                </Grid>
                <Grid item xs={6}>
                  <DatePickerInput
                    name={CountrySelectionFields.RECEIVE_DATE}
                    label={
                      CountrySelectionLabels[
                        CountrySelectionFields.RECEIVE_DATE
                      ]
                    }
                  />
                </Grid>
                <Grid item xs={12}>
                  <AutoCompleteInput
                    name={CountrySelectionFields.STATUS_CODE}
                    label={
                      CountrySelectionLabels[CountrySelectionFields.STATUS_CODE]
                    }
                    options={statuses}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Input
                    name={CountrySelectionFields.COMMENTS}
                    label={
                      CountrySelectionLabels[CountrySelectionFields.COMMENTS]
                    }
                    value={values[CountrySelectionFields.COMMENTS] || ''}
                    onChange={handleChange}
                    multiline
                  />
                </Grid>
                <Grid item xs={12}>
                  <Input
                    name={CountrySelectionFields.UPDATED_TIME_NAME}
                    label={
                      CountrySelectionLabels[
                        CountrySelectionFields.UPDATED_TIME_NAME
                      ]
                    }
                    value={
                      values[CountrySelectionFields.UPDATED_TIME_NAME] || ''
                    }
                    onChange={handleChange}
                    disabled
                  />
                </Grid>
              </Grid>
            </Form>
          );
        }}
      </Formik>
    </Popup>
  );
};

export default ContrySelectionPopup;

import { useCallback, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';

import {
  AddCircleOutline,
  ChevronRight,
  DeleteOutline,
  Refresh,
} from '@mui/icons-material';

import { DataTable } from '../../../components/CustomDataGrid';
import { RoundButton } from '../../../components/RoundButton';
import {
  DeleteToolButton,
  ToolButton,
  ToolsPanel,
} from '../../../components/ToolsPanel';
import {
  useCloseStageMutation,
  useDeleteStageMutation,
  useGetRequestStagesQuery,
} from '../../../services/api/request';
import { RootState } from '../../../store';
import { stagesToRowAdapter } from '../../../utils/requests';

import styles from './styles.module.scss';

export interface StagesProps {
  id: number;
}

export const Stages = ({ id }: StagesProps) => {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const { data, isLoading, refetch } = useGetRequestStagesQuery({
    id,
    page,
    size: perPage,
  });
  const stages = data?.data;
  const { cols, rows } = useMemo(() => stagesToRowAdapter(stages), [stages]);
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const selectedArr = useMemo(
    () => Object.keys(selected).filter((key) => selected[Number(key)]),
    [selected]
  );
  const selectedStage = useMemo(
    () => stages?.find((i) => i.operationId === Number(selectedArr[0])),
    [selected]
  );

  const [popup, setPopup] = useState(false);
  const [deleteStage] = useDeleteStageMutation();
  const [closeStage] = useCloseStageMutation();

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const handleDetails = useCallback(() => {
    window
      .open(
        `/stages?id=${selectedArr[0]}&reqId=${id}`,
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  }, [selected, id, openFormInNewTab]);

  const createStage = useCallback(() => {
    window
      .open(`/stages?reqId=${id}`, openFormInNewTab ? '_blank' : '_self')
      ?.focus();
  }, [id, openFormInNewTab]);

  const handleDelete = useCallback(async () => {
    await deleteStage({ id: Number(selectedArr[0]) });
    setSelected({});
    setPopup(false);
    refetch();
  }, [deleteStage, selected, refetch]);

  const handleRowClick = (row: any) => {
    window
      .open(
        `/stages?id=${row.id}&reqId=${id}`,
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  };

  const handleClose = useCallback(async () => {
    await closeStage({ id: Number(selectedArr[0]) });
    refetch();
  }, [selected, refetch, closeStage]);

  return (
    <div className={styles.content}>
      <DataTable
        formKey={'requests_stages'}
        cols={cols}
        rows={rows}
        onRowSelect={setSelected}
        onRowDoubleClick={handleRowClick}
        height="fullHeight"
        loading={isLoading}
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: data?.totalCount || 0,
        }}
      >
        <ToolsPanel
          className={styles.toolBar}
          leftActions={
            <>
              <RoundButton icon={<Refresh />} onClick={refetch} />
            </>
          }
        >
          <ToolButton
            label="Подробно"
            endIcon={<ChevronRight />}
            onClick={handleDetails}
            disabled={selectedArr.length !== 1}
            fast
          />
          <ToolButton
            label="Создать"
            startIcon={<AddCircleOutline />}
            onClick={createStage}
            fast
          />

          <DeleteToolButton
            label={'Удалить'}
            startIcon={<DeleteOutline />}
            disabled={
              selectedArr.length !== 1 ||
              !!selectedStage?.endDate ||
              selectedStage?.isAutomatic
            }
            fast
            description={`Вы действительно хотите удалить этап № ${selectedArr[0]}?`}
            title={'Удаление этапа'}
            onConfirm={handleDelete}
          />

          <ToolButton
            label="Закрыть этап"
            onClick={handleClose}
            disabled={
              selectedArr.length !== 1 ||
              !!selectedStage?.endDate ||
              selectedStage?.isMainFlow
            }
          />
        </ToolsPanel>
      </DataTable>
    </div>
  );
};

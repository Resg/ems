import { useEffect, useMemo, useState } from 'react';

import { Refresh } from '@mui/icons-material';

import { DataTable } from '../../../components/CustomDataGrid/DataTable';
import { RoundButton } from '../../../components/RoundButton';
import { ToolsPanel } from '../../../components/ToolsPanel';
import { RequestConclusionPCTRCols } from '../../../constants/requests';
import { useGetRequestPCTRQuery } from '../../../services/api/request';
import { PCTRToRowAdapter } from '../../../utils/requests';

import styles from './styles.module.scss';

interface PCTRProps {
  id: number;
}

export const SPS_PCTR: React.FC<PCTRProps> = ({ id }) => {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});

  const {
    data: PCTRData,
    refetch,
    isLoading,
  } = useGetRequestPCTRQuery({
    id,
    page,
    size: perPage,
  });

  const rows = useMemo(() => {
    return PCTRToRowAdapter(PCTRData?.data);
  }, [PCTRData?.data]);

  useEffect(() => {
    refetch();
  }, [page, perPage]);

  return (
    <div className={styles.content}>
      <DataTable
        formKey={'request_consolusions_PCTR'}
        cols={RequestConclusionPCTRCols}
        rows={rows}
        onRowSelect={setSelected}
        height="fullHeight"
        loading={isLoading}
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: PCTRData?.totalCount || 0,
        }}
        className={styles.table}
      >
        <ToolsPanel
          className={styles.toolBar}
          leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
        ></ToolsPanel>
      </DataTable>
    </div>
  );
};

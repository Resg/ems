import { useCallback, useEffect, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';

import { AddCircleOutline, Close } from '@mui/icons-material';
import { Button, Grid, Stack } from '@mui/material';

import { DataTable } from '../../../../components/CustomDataGrid';
import { DatePickerInput } from '../../../../components/DatePickerInput';
import { FilterBar } from '../../../../components/FilterBarNew';
import { Popup } from '../../../../components/Popup';
import {
  SolutionModalFields,
  SolutionModalLabels,
} from '../../../../constants/solution';
import { SolutionModalCols } from '../../../../constants/solution';
import { useGetSolutionModalDataQuery } from '../../../../services/api/dictionaries';
import { useAddSolutionMutation } from '../../../../services/api/request';
import { RootState } from '../../../../store';
import { solutionModalToRowAdapter } from '../../../../utils/solution';

import styles from './styles.module.scss';

type SolutionModalProps = {
  open: boolean;
  onClose: () => void;
  id: number;
  requestId: number;
  refethTable: () => void;
};

export const SolutionModal: React.FC<SolutionModalProps> = ({
  open,
  onClose,
  id,
  requestId,
  refethTable,
}) => {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});

  const filter = useSelector(
    (state: RootState) => state.filters.data['solution-modal'] || {}
  );

  const { data, refetch, isLoading } = useGetSolutionModalDataQuery(
    {
      id,
      page,
      size: perPage,
      number: filter.elasticSearch || undefined,
      datePeriodStart: filter.datePeriodStart || undefined,
      datePeriodEnd: filter.datePeriodEnd || undefined,
    },
    {
      skip:
        !filter.elasticSearch &&
        !filter.datePeriodStart &&
        !filter.datePeriodEnd,
    }
  );

  const solutionsModal = useMemo(() => data?.data, [data]);

  const [addSolution] = useAddSolutionMutation();

  const selectedSolution = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const handleAdd = useCallback(async () => {
    if (selectedSolution) {
      await addSolution({ requestId, solutionId: selectedSolution[0] });
      onClose();
      refethTable();
    }
  }, [selectedSolution, requestId]);

  const rows = useMemo(() => {
    return solutionModalToRowAdapter(solutionsModal);
  }, [solutionsModal]);

  useEffect(() => {
    refetch();
  }, [page, perPage]);

  const bar = (
    <Stack
      direction="row"
      justifyContent="flex-end"
      spacing={1.5}
      style={{ width: '100%' }}
    >
      <Button
        variant="contained"
        startIcon={<AddCircleOutline />}
        onClick={handleAdd}
      >
        Добавить
      </Button>
      <Button variant="outlined" startIcon={<Close />} onClick={onClose}>
        Отмена
      </Button>
    </Stack>
  );

  return (
    <Popup
      open={Boolean(open)}
      onClose={onClose}
      title={'Добавление решения ГКРЧ'}
      bar={bar}
      height={820}
      centred
    >
      <div className={styles.popupContent}>
        <FilterBar
          formKey={'solution-modal'}
          placeholder="Номер решения"
          refetchSearch={() => refetch()}
        >
          <Grid sx={{ mb: 5, mt: 2 }} container columnSpacing={10}>
            <Grid item xs={6}>
              <DatePickerInput
                name={SolutionModalFields.DATE_PERIOD_START}
                label={
                  SolutionModalLabels[SolutionModalFields.DATE_PERIOD_START]
                }
              />
            </Grid>
            <Grid item xs={6}>
              <DatePickerInput
                name={SolutionModalFields.DATE_PERIOD_END}
                label={SolutionModalLabels[SolutionModalFields.DATE_PERIOD_END]}
              />
            </Grid>
          </Grid>
        </FilterBar>
        <div className={styles.searchResultText}>РЕЗУЛЬТАТ ПОИСКА</div>
        <DataTable
          formKey={'solutions_solution_modal'}
          cols={SolutionModalCols}
          rows={rows}
          onRowSelect={setSelected}
          loading={isLoading}
          pagerProps={{
            page: page,
            setPage: setPage,
            perPage: perPage,
            setPerPage: setPerPage,
            total: data?.totalCount || 0,
          }}
          className={styles.table}
        ></DataTable>
      </div>
    </Popup>
  );
};

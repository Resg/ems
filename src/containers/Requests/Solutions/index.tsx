import { useCallback, useEffect, useMemo, useState } from 'react';
import { matchPath, useLocation } from 'react-router-dom';

import {
  AddCircleOutline,
  Cancel,
  DeleteOutline,
  Refresh,
} from '@mui/icons-material';
import { Button, Card, Dialog, Stack } from '@mui/material';

import { DataTable } from '../../../components/CustomDataGrid';
import { RoundButton } from '../../../components/RoundButton';
import { ToolButton, ToolsPanel } from '../../../components/ToolsPanel';
import { Routes } from '../../../constants/routes';
import { SolutionsCols } from '../../../constants/solution';
import {
  useDeleteSolutionMutation,
  useGetSolutionsQuery,
} from '../../../services/api/request';
import { solutionsToRowAdapter } from '../../../utils/solution';

import { SolutionModal } from './SolutionModal';

import styles from './styles.module.scss';

export interface SolutionsProps {
  id: number;
}

export const Solutions = ({ id }: SolutionsProps) => {
  const location = useLocation();
  const requestId = Number(
    matchPath(Routes.REQUESTS.REQUEST_TABS, location.pathname)?.params?.id || ''
  );

  const [popup, setPopup] = useState(false);
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);

  const [selected, setSelected] = useState<Record<number, boolean>>({});

  const [openModal, setOpenModal] = useState(false);
  const refethTable = () => refetch();
  const selectedSolution = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const { data, refetch, isLoading } = useGetSolutionsQuery(
    { id, page, size: perPage },
    { skip: !id }
  );
  const solutions = data?.data;

  const [deleteSolution] = useDeleteSolutionMutation();

  const rows = useMemo(() => {
    return solutionsToRowAdapter(solutions);
  }, [solutions]);

  const handleDelete = useCallback(async () => {
    if (selectedSolution) {
      await deleteSolution({ requestId, solutionId: selectedSolution[0] });
      setPopup(false);
      refetch();
    }
  }, [selectedSolution]);

  const handleClose = useCallback((e: React.MouseEvent) => {
    e.stopPropagation();
    setPopup(false);
  }, []);

  useEffect(() => {
    refetch();
  }, [page, perPage]);

  return (
    <div className={styles.content}>
      <DataTable
        formKey={'requests_solutions'}
        cols={SolutionsCols}
        loading={isLoading}
        rows={rows}
        onRowSelect={setSelected}
        height="fullHeight"
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: data?.totalCount || 0,
        }}
        className={styles.table}
      >
        <ToolsPanel
          className={styles.toolBar}
          leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
        >
          <ToolButton
            label="Добавить"
            startIcon={<AddCircleOutline />}
            onClick={() => setOpenModal(true)}
            fast
          />
          <ToolButton
            label="Удалить"
            startIcon={<DeleteOutline />}
            disabled={selectedSolution.length !== 1}
            onClick={(e: React.MouseEvent) => {
              e.stopPropagation();
              setPopup(true);
            }}
            fast
          >
            <Dialog open={popup}>
              <Card className={styles.card}>
                <div className={styles.dialogText}>
                  <h3>Удаление решения</h3>
                  <p>
                    Вы действительно хотите удалить решение №
                    {
                      data?.data?.find(
                        (solution) => solution.id === selectedSolution[0]
                      )?.number
                    }{' '}
                    ?
                  </p>
                </div>
                <Stack
                  direction="row"
                  justifyContent="flex-end"
                  spacing={1.5}
                  width="100%"
                >
                  <Button
                    variant="contained"
                    endIcon={<Cancel />}
                    onClick={handleClose}
                  >
                    Отменить
                  </Button>
                  <Button
                    variant="outlined"
                    endIcon={<DeleteOutline />}
                    onClick={handleDelete}
                  >
                    Удалить
                  </Button>
                </Stack>
              </Card>
            </Dialog>
          </ToolButton>
        </ToolsPanel>
      </DataTable>
      <SolutionModal
        id={id}
        open={openModal}
        onClose={() => setOpenModal(false)}
        requestId={requestId}
        refethTable={refethTable}
      />
    </div>
  );
};

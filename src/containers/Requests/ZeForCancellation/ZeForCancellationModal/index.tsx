import { useCallback, useEffect, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';

import { AddCircleOutline, Close } from '@mui/icons-material';
import { Button, Grid, Stack } from '@mui/material';

import { DataTable } from '../../../../components/CustomDataGrid';
import { DatePickerInput } from '../../../../components/DatePickerInput';
import { FilterBar } from '../../../../components/FilterBarNew';
import { Popup } from '../../../../components/Popup';
import { ConclusionModalCols } from '../../../../constants/conclusions';
import {
  SolutionModalFields,
  SolutionModalLabels,
} from '../../../../constants/solution';
import { useGetConclusionsQuery } from '../../../../services/api/conclusions';
import { useAddAnnulledConclusionMutation } from '../../../../services/api/request';
import { RootState } from '../../../../store';
import { zeForCancellationModalToRowAdapter } from '../../../../utils/zeForCancellation';

import styles from './styles.module.scss';

type ZeForCancellationModalProps = {
  open: boolean;
  onClose: () => void;
  id: number;
  refethTable: () => void;
};

export const ZeForCancellationModal: React.FC<ZeForCancellationModalProps> = ({
  open,
  onClose,
  id,
  refethTable,
}) => {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});

  const filters = useSelector(
    (state: RootState) => state.filters.data['conclusion-search'] || {}
  );

  const {
    elasticSearch,
    dateStart,
    dateEnd,
  }: {
    elasticSearch?: string;
    dateStart?: string;
    dateEnd?: string;
  } = filters;

  const { data, refetch, isLoading } = useGetConclusionsQuery(
    {
      searchParameters: {
        elasticSearch: elasticSearch || undefined,
        dateStart: dateStart || undefined,
        dateEnd: dateEnd || undefined,
      },
      page,
      size: perPage,
    },
    {
      skip: !elasticSearch && !dateStart && !dateEnd,
    }
  );

  const conclusionsModel = useMemo(() => data?.data, [data]);

  const [addSolution] = useAddAnnulledConclusionMutation();

  const selectedConclusions = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const handleAdd = useCallback(async () => {
    if (selectedConclusions) {
      await addSolution({ requestId: id, selectedConclusions });
      onClose();
      refethTable();
    }
  }, [selectedConclusions]);

  const rows = useMemo(() => {
    return zeForCancellationModalToRowAdapter(conclusionsModel);
  }, [conclusionsModel]);

  useEffect(() => {
    refetch();
  }, [page, perPage]);

  const bar = (
    <Stack
      direction="row"
      justifyContent="flex-end"
      spacing={1.5}
      style={{ width: '100%' }}
    >
      <Button
        variant="contained"
        startIcon={<AddCircleOutline />}
        onClick={handleAdd}
      >
        Добавить
      </Button>
      <Button variant="outlined" startIcon={<Close />} onClick={onClose}>
        Отмена
      </Button>
    </Stack>
  );

  return (
    <Popup
      open={Boolean(open)}
      onClose={onClose}
      height={820}
      centred
      title={'Добавление заключения'}
      bar={bar}
    >
      <div className={styles.popupContent}>
        <Stack
          direction="row"
          spacing={1.5}
          justifyContent="space-between"
        ></Stack>
        <FilterBar
          formKey={'conclusion-search'}
          placeholder="Номер заключения"
          refetchSearch={() => refetch()}
        >
          <Grid sx={{ mb: 5, mt: 2 }} container columnSpacing={10}>
            <Grid item xs={6}>
              <DatePickerInput
                name={SolutionModalFields.DATE_PERIOD_START}
                label={
                  SolutionModalLabels[SolutionModalFields.DATE_PERIOD_START]
                }
              />
            </Grid>
            <Grid item xs={6}>
              <DatePickerInput
                name={SolutionModalFields.DATE_PERIOD_END}
                label={SolutionModalLabels[SolutionModalFields.DATE_PERIOD_END]}
              />
            </Grid>
          </Grid>
        </FilterBar>
        <div className={styles.searchResultText}>РЕЗУЛЬТАТ ПОИСКА</div>
        <DataTable
          formKey={'requests_ze_for_cacellation_modal'}
          cols={ConclusionModalCols}
          rows={rows}
          onRowSelect={setSelected}
          loading={isLoading}
          pagerProps={{
            page: page,
            setPage: setPage,
            perPage: perPage,
            setPerPage: setPerPage,
            total: data?.totalCount || 0,
          }}
          className={styles.table}
        ></DataTable>
      </div>
    </Popup>
  );
};

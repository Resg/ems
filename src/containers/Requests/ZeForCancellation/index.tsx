import { useCallback, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';

import {
  AddCircleOutline,
  ChevronRight,
  Delete,
  Refresh,
} from '@mui/icons-material';

import { DataTable } from '../../../components/CustomDataGrid';
import { RoundButton } from '../../../components/RoundButton';
import { ToolButton, ToolsPanel } from '../../../components/ToolsPanel';
import { DeleteToolButton } from '../../../components/ToolsPanel/DeleteToolButton';
import { RequestConclusionsCols } from '../../../constants/requests';
import {
  useDeleteAnnulledConclusionMutation,
  useGetAnnulledConclusionQuery,
} from '../../../services/api/request';
import { RootState } from '../../../store';
import { AnnulledConclusionData } from '../../../types/requests';
import { AnnulledConclusionsToRowAdapter } from '../../../utils/requests';

import { ZeForCancellationModal } from './ZeForCancellationModal';

import styles from './styles.module.scss';

interface ZeForCancellationProps {
  id: number;
}

export const ZeForCancellation: React.FC<ZeForCancellationProps> = ({ id }) => {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});

  const [openModal, setOpenModal] = useState(false);
  const refethTable = () => refetch();

  const { data, refetch, isLoading } = useGetAnnulledConclusionQuery({
    id,
    page,
    size: perPage,
  });

  const [deleteConclusion] = useDeleteAnnulledConclusionMutation();

  const rows = useMemo(() => {
    return AnnulledConclusionsToRowAdapter(data?.data);
  }, [data?.data]);

  const selectedConclusions = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const openPage = useCallback(
    (path: string = '') => {
      window
        .open(`/conclusions${path}`, openFormInNewTab ? '_blank' : '_self')
        ?.focus();
    },
    [selectedConclusions, openFormInNewTab]
  );

  const handleDelete = useCallback(async () => {
    await deleteConclusion({ id, conclusinId: selectedConclusions[0] });
    refetch();
  }, [selectedConclusions, deleteConclusion]);

  const handleDoubleClick = useCallback(
    (conclusion: Partial<AnnulledConclusionData>) => {
      if (conclusion.id) {
        openPage(`?id=${conclusion.id}`);
      }
    },
    [openPage]
  );

  return (
    <div className={styles.content}>
      <DataTable
        formKey={'requests_ze_for_cacellation'}
        cols={RequestConclusionsCols}
        rows={rows}
        onRowSelect={setSelected}
        onRowDoubleClick={handleDoubleClick}
        loading={isLoading}
        height="fullHeight"
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: data?.totalCount || 0,
        }}
        className={styles.table}
      >
        <ToolsPanel
          className={styles.toolBar}
          leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
        >
          <ToolButton
            label="Подробно"
            endIcon={<ChevronRight />}
            disabled={selectedConclusions.length !== 1}
            onClick={() =>
              selectedConclusions && openPage(`?id=${selectedConclusions[0]}`)
            }
            fast
          />
          <ToolButton
            label="Добавить"
            startIcon={<AddCircleOutline />}
            onClick={() => setOpenModal(true)}
            fast
          />
          <DeleteToolButton
            label={'Удалить'}
            startIcon={<Delete />}
            disabled={selectedConclusions.length !== 1}
            fast
            description={`Вы действительно хотите удалить заключение ${
              data?.data?.find(
                (conclusion) => conclusion.id === selectedConclusions[0]
              )?.number
            }`}
            title={'Удаление заключения'}
            onConfirm={handleDelete}
          />
        </ToolsPanel>
      </DataTable>
      <ZeForCancellationModal
        id={id}
        open={openModal}
        onClose={() => setOpenModal(false)}
        refethTable={refethTable}
      />
    </div>
  );
};

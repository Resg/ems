import React, { useEffect } from 'react';
import { useFormikContext } from 'formik';

import { useAppDispatch } from '../../../../../store';
import { setError } from '../../../../../store/utils';

interface CyclingErrorMessageProps {}

export const CyclingErrorMessage: React.FC<CyclingErrorMessageProps> = ({}) => {
  const { values, errors, setErrors } = useFormikContext<any>();
  const dispatch = useAppDispatch();

  useEffect(() => {
    const { data } = values;
    let [_, ...array] = data;
    array = array.map((el: any, index: number) => {
      return { ...el, index: index + 2 };
    });
    const visited: Record<number, number> = array.reduce(
      (acc: any, curr: { index: number }) => {
        return { ...acc, [curr.index]: 0 };
      },
      {} as Record<number, number>
    );

    const i = 0;
    const startElements = array.filter(
      (el: { previousId: string | number }) => !el.previousId
    );

    const recursion = (element: { index: number }) => {
      visited[element.index] += 1;
      const currElements = array.filter(
        (el: { previousId: string | number }) => {
          return el.previousId === element.index;
        }
      );
      for (let i = 0; i < currElements.length; i++) {
        recursion(currElements[i]);
      }
    };

    for (let i = 0; i < startElements.length; i++) {
      recursion(startElements[i]);
    }
    const result = Object.values(visited).every((element) => element === 1);
    if (!result) {
      dispatch(
        setError({
          message:
            'Запрещено закольцовывать последовательность выполнения, поскольку в таком случае ни одна подзадача не может быть отправлена на исполнение первой',
        })
      );
    }
  }, [values, dispatch]);

  return null;
};

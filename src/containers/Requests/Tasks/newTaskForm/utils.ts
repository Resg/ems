import { FormikValues } from 'formik';

import { TaskType } from '../../../../types/dictionaries';
import { TaskRouteData } from '../../../../types/tasks';

export const prepareData = (
  data: FormikValues,
  taskTypes: TaskType[] = [],
  objectTypeCode: string,
  objectId: number,
  authorId?: number,
  parentId?: number,
  open?: 'task' | 'subtask' | null,
  subtaskTypes: TaskType[] = []
) => {
  return data.data.map((task: TaskRouteData, index: number) => {
    return {
      temporaryId: index + 1,
      typeId: task.typeId,
      title:
        index === 0
          ? taskTypes.find((type) => type.id === task.typeId)?.name
          : subtaskTypes.find((type) => type.id === task.typeId)?.name,
      comments: task.comments,
      objectTypeCode,
      objectId,
      term: task.term && +task.term,
      performerId: task.performerId || authorId,
      authorId,
      isControl: task.isControl,
      sendForExecution: index === 0,

      temporaryParentId: index !== 0 ? 1 : null, // для подзадачи с задачей
      temporaryPreviousId: task.previousId, // для подзадачи с задачей

      parentId: open === 'subtask' ? parentId : null, // для подзадачи
      previousId: open === 'subtask' ? task.previousId : null, // для подзадачи
    };
  });
};

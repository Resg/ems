import { useEffect, useMemo } from 'react';
import { FormikValues } from 'formik';

import { Grid } from '@mui/material';

import { AutoCompleteInput } from '../../../../components/AutoCompleteInput';
import { CheckboxInput } from '../../../../components/CheckboxInput';
import { ClerkInput } from '../../../../components/ClerkInput';
import Input from '../../../../components/Input';
import {
  TaskFormFields,
  TaskFormLabels,
} from '../../../../constants/newTaskForm';
import { useGetTaskTypesQuery } from '../../../../services/api/dictionaries';
import { ListResponse } from '../../../../types/api';
import {
  DictionariesClerkType,
  TaskType,
} from '../../../../types/dictionaries';
import { SiblingType } from '../../../../types/tasks';

import styles from './styles.module.scss';

interface FormSliceType {
  index: number;
  values: FormikValues;
  parentTypeId?: number;
  parentName?: string;
  handleChange: (e: React.ChangeEvent<any>) => void;
  dicts: {
    taskTypes?: ListResponse<TaskType[]>;
    clerks?: ListResponse<DictionariesClerkType[]>;
    taskChildren?: ListResponse<SiblingType[]>;
  };
  setSubtaskTypes: (subtask: any) => void;
}

export const FormSlice: React.FC<FormSliceType> = ({
  index,
  values,
  parentTypeId,
  parentName,
  handleChange,
  dicts: { taskTypes, clerks, taskChildren },
  setSubtaskTypes,
}) => {
  const task = values.data[index].type === 'task';
  const subtask = values.data[index].type === 'subtask';

  const { data: subtaskTypes } = useGetTaskTypesQuery(
    { parentTypeId },
    { skip: !parentTypeId }
  );

  useEffect(() => {
    if (values.data.length > 1) {
      values.data[0].performerId = null;
    }
  }, [values.data]);

  useEffect(() => {
    setSubtaskTypes(subtaskTypes?.data);
  }, [subtaskTypes?.data]);

  const previousIdOptions = useMemo(() => {
    if (values.data[0].type === 'subtask') {
      return (
        taskChildren?.data?.map((sibling) => {
          return {
            value: sibling.id,
            label: sibling.title,
          };
        }) || []
      );
    }
    const options: Array<{ value: number; label: string }> = [];

    values.data.forEach((task: FormikValues, ind: number) => {
      if (task.type === 'subtask') {
        const subtaskName = subtaskTypes?.data?.find(
          (type) => type.id === task.typeId
        )?.name;
        const subtaskPerformer = clerks?.data?.find(
          (clerk) => clerk.id === task.performerId
        )?.personFio;
        if (subtaskName && subtaskPerformer && index !== ind) {
          options.push({
            value: ind + 1,
            label: `${subtaskName} - ${subtaskPerformer}`,
          });
        }
      }
    });

    return options;
  }, [values.data, taskChildren]);

  return (
    <Grid container spacing={2} className={styles.formSlice}>
      <Grid item xs={12}>
        <h1 hidden={task || index === 0} className={styles.header}>
          Подзадача {index}
        </h1>
      </Grid>

      <Grid item xs={12} hidden={task || index !== 0}>
        <Input
          name={`data[${index}].parentName`}
          label="Основная задача"
          value={parentName || ''}
          disabled
        />
      </Grid>

      <Grid item xs={12} hidden={subtask}>
        <AutoCompleteInput
          name={`data[${index}].${TaskFormFields.TYPE_ID}`}
          label={TaskFormLabels[TaskFormFields.TYPE_ID]}
          required
          options={
            taskTypes?.data?.map((type) => {
              return {
                value: type.id,
                label: type.name,
              };
            }) || []
          }
          clearOnBlur={false}
        />
      </Grid>

      <Grid item xs={12} hidden={task}>
        <AutoCompleteInput
          name={`data[${index}].${TaskFormFields.TYPE_ID}`}
          label="Тип подзадачи"
          required
          options={
            subtaskTypes?.data?.map((type) => {
              return {
                value: type.id,
                label: type.name,
              };
            }) || []
          }
          clearOnBlur={false}
        />
      </Grid>

      <Grid
        item
        xs={task ? 12 : 6}
        hidden={index === 0 && values.data.length > 1}
      >
        <ClerkInput
          name={`data[${index}].${TaskFormFields.PERFORMER_ID}`}
          label={TaskFormLabels[TaskFormFields.PERFORMER_ID]}
          required
          clearOnBlur={index === 0 && values.data.length > 1}
        />
      </Grid>

      <Grid item xs={6} hidden={task}>
        <AutoCompleteInput
          name={`data[${index}].${TaskFormFields.PREVIOUS_ID}`}
          label={TaskFormLabels[TaskFormFields.PREVIOUS_ID]}
          options={previousIdOptions}
          clearOnBlur={false}
        />
      </Grid>

      <Grid item xs={6}>
        <Input
          name={`data[${index}].${TaskFormFields.TERM}`}
          label={TaskFormLabels[TaskFormFields.TERM]}
          value={values.data[index][TaskFormFields.TERM]}
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={12}>
        <Input
          name={`data[${index}].${TaskFormFields.COMMENTS}`}
          label={TaskFormLabels[TaskFormFields.COMMENTS]}
          value={values.data[index][TaskFormFields.COMMENTS]}
          onChange={handleChange}
          multiline
        />
      </Grid>
      <Grid item xs={12}>
        <CheckboxInput
          name={`data[${index}].${TaskFormFields.IS_CONTROL}`}
          label={TaskFormLabels[TaskFormFields.IS_CONTROL]}
        />
      </Grid>
      <Grid item xs={12}>
        <div className={styles.hor} />
      </Grid>
    </Grid>
  );
};

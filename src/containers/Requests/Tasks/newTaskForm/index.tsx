import { useEffect, useMemo, useRef, useState } from 'react';
import {
  FieldArray,
  Form,
  Formik,
  FormikProps,
  FormikValues,
  useFormikContext,
} from 'formik';
import { array, number, object } from 'yup';

import { AddBox, Close, Save, Send } from '@mui/icons-material';
import { Button, Stack } from '@mui/material';

import { Popup } from '../../../../components/Popup';
import { DefaultTaskFormFields } from '../../../../constants/newTaskForm';
import {
  useGetDocumentClerksQuery,
  useGetTaskTypesQuery,
} from '../../../../services/api/dictionaries';
import {
  useCreateTaskRouteMutation,
  useGetTaskChildrenQuery,
} from '../../../../services/api/task';
import { useGetUserQuery } from '../../../../services/api/user';
import { ObjectTask, TaskRouteData } from '../../../../types/tasks';

import { CyclingErrorMessage } from './CyclingErrorMessage';
import { FormSlice } from './formSlice';
import { prepareData } from './utils';

import styles from './styles.module.scss';

type NewTaskModalProps = {
  parent?: ObjectTask;
  open: 'task' | 'subtask' | null;
  objectTypeCode: string;
  objectId: number;
  closeModal: () => void;
};

export const NewTaskModal: React.FC<NewTaskModalProps> = ({
  parent = { id: 0, typeId: 0, title: '' },
  open,
  objectTypeCode,
  objectId,
  closeModal,
}) => {
  const { id: parentId, typeId: parentTypeId, title: parentName } = parent;
  const { data: taskTypes } = useGetTaskTypesQuery({ objectTypeCode });
  const { data: clerks } = useGetDocumentClerksQuery({});
  const { data: userInfo } = useGetUserQuery({});
  const { data: taskChildren } = useGetTaskChildrenQuery(
    { parentId },
    { skip: open !== 'subtask', refetchOnMountOrArgChange: true }
  );
  const [createTask] = useCreateTaskRouteMutation();

  const formRef = useRef<FormikProps<FormikValues>>(null);
  const [formLength, setFormLength] = useState<number>(1);
  const [subtaskTypes, setSubtaskTypes] = useState();

  const validationSchema = useMemo(() => {
    return object({
      data: array()
        .of(
          object({
            performerId: number().nullable().required('Обязательное поле'),
            typeId: number().required('Обязательное поле').nullable(),
            term: number(),
          })
        )
        .transform((cur) => {
          if (formLength > 1) {
            return [
              {
                ...cur[0],
                performerId: 1,
              },
              ...cur.slice(1),
            ];
          }
          return cur;
        }),
    });
  }, [formLength]);

  const FormUpdater = () => {
    const { values = {} as any } = useFormikContext();

    useEffect(() => {
      setFormLength(values.data.length);
    }, [values.data.length]);
    return null;
  };

  const sendForm = async (data: TaskRouteData[]) => {
    const response = await createTask({ taskData: data });
    if ('data' in response) {
      closeModal();
    }
  };

  const handleSubmit = (saveMode: boolean = false) => {
    let data = prepareData(
      formRef?.current?.values || {},
      taskTypes?.data,
      objectTypeCode,
      objectId,
      userInfo?.clerkId,
      parentId,
      open,
      subtaskTypes
    );
    if (saveMode) {
      data = data.map((task: TaskRouteData) => {
        return { ...task, sendForExecution: false };
      });
    }

    formRef?.current?.validateForm().then((result) => {
      const errors = result?.data;

      if (errors) {
        const valuesToTouch = { data: [] as any };

        if (Array.isArray(errors)) {
          const arr = [...errors];

          arr.forEach((i) => {
            const errSlice = {} as any;

            if (i) {
              Object.keys(i).forEach((key) => {
                errSlice[key] = true;
              });

              valuesToTouch.data.push(errSlice);
            } else {
              valuesToTouch.data.push({});
            }
          });

          formRef?.current?.setTouched(valuesToTouch);
        }
      }

      if (!errors) {
        sendForm(data);
      }
    });
  };

  const bar = (
    <Stack
      direction="row"
      justifyContent="flex-end"
      spacing={1.5}
      style={{ width: '100%' }}
    >
      <Button
        variant="contained"
        startIcon={<Send />}
        onClick={() => handleSubmit()}
      >
        Отправить на исполнение
      </Button>
      <Button
        variant="outlined"
        startIcon={<Save />}
        onClick={() => handleSubmit(true)}
      >
        Сохранить
      </Button>
      <Button variant="outlined" startIcon={<Close />} onClick={closeModal}>
        Отмена
      </Button>
    </Stack>
  );

  return (
    <Popup
      open={Boolean(open)}
      onClose={closeModal}
      title={open === 'task' ? 'Создание задачи' : 'Создание подзадачи'}
      bar={bar}
    >
      <Formik
        initialValues={{ data: [{ ...DefaultTaskFormFields, type: open }] }}
        validationSchema={validationSchema}
        enableReinitialize
        onSubmit={() => handleSubmit()}
        innerRef={formRef}
      >
        {({ handleChange, values }) => {
          return (
            <Form autoComplete="off">
              <FieldArray
                name="data"
                render={(arrayHelpers) => {
                  return [
                    values.data.map((_value: TaskRouteData, index: number) => {
                      return (
                        <FormSlice
                          key={index}
                          index={index}
                          handleChange={handleChange}
                          parentName={parentName}
                          parentTypeId={
                            values.data[0].type === 'task'
                              ? values.data[0].typeId
                              : parentTypeId
                          }
                          values={values}
                          dicts={{
                            taskTypes,
                            clerks,
                            taskChildren,
                          }}
                          setSubtaskTypes={setSubtaskTypes}
                        />
                      );
                    }),
                    open === 'task' && (
                      <Stack direction="row" justifyContent="flex-end">
                        <Button
                          startIcon={<AddBox />}
                          className={styles.button}
                          onClick={() =>
                            arrayHelpers.push({
                              ...DefaultTaskFormFields,
                              type: 'subtask',
                            })
                          }
                        >
                          Создать подзадачу
                        </Button>
                      </Stack>
                    ),
                  ];
                }}
              />
              <FormUpdater />
              <CyclingErrorMessage />
            </Form>
          );
        }}
      </Formik>
    </Popup>
  );
};

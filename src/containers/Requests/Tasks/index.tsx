import React, { useCallback, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';

import {
  AddCircleOutline,
  ChevronRight,
  DeleteOutline,
  ExpandMore,
  Refresh,
  Restore,
  Send,
} from '@mui/icons-material';

import { TaskTemplateForm } from '../../../components/CreateTaskTemplateForm';
import { TreeDataGrid } from '../../../components/CustomDataGrid';
import { CreateRightGroupColumn } from '../../../components/CustomDataGrid/TreeDataGrid/Columns/GroupColumn';
import { DefaultRow } from '../../../components/CustomDataGrid/types';
import { createTreeByRows } from '../../../components/CustomDataGrid/utils';
import { ExecuteButton } from '../../../components/Execution';
import {
  ExecutionButtonIcons,
  ExecutionStatusIds,
} from '../../../components/Execution/ExecutionButton/constants';
import { MenuButtonItem } from '../../../components/MenuButton/MenuButtonItem';
import { RoundButton } from '../../../components/RoundButton';
import { SaveToTemplateModal } from '../../../components/SaveToTemplateModal';
import { DeleteToolButton } from '../../../components/ToolsPanel';
import { ToolButton, ToolsPanel } from '../../../components/ToolsPanel';
import { MenuToolButton } from '../../../components/ToolsPanel/MenuToolButton';
import { RequestTasksCols } from '../../../constants/requests';
import { RequestTasksExecutionButtons } from '../../../constants/tasks';
import {
  useDeleteTaskMutation,
  useGetObjectTasksQuery,
  useRevokeTaskMutation,
  useSendToExecutionMutation,
  useSendToReexecutionMutation,
} from '../../../services/api/incomingDocumentTasks';
import { RootState } from '../../../store';
import { RequestData } from '../../../types/requests';

import { NewTaskModal } from './newTaskForm';

import styles from './styles.module.scss';
export interface TasksProps {
  request: RequestData;
}

const groups = ['parentId', 'id'];

export const Tasks: React.FC<TasksProps> = ({ request }) => {
  const [taskModal, setTaskModal] = useState<'task' | 'subtask' | null>(null);
  const [tableFilters, setTableFilters] = useState(false);
  const [templateModal, setTemplateModal] = useState<'task' | 'route' | null>(
    null
  );

  const { data, refetch, isLoading } = useGetObjectTasksQuery(
    {
      id: request?.id || 0,
      type: 'REQUEST',
    },
    { skip: !document }
  );

  const [deleteTask] = useDeleteTaskMutation();
  const [sendToExecution] = useSendToExecutionMutation();
  const [revokeTask] = useRevokeTaskMutation();
  const [reexecution] = useSendToReexecutionMutation();

  const [taskTemplate, setTaskTemplate] = useState(false);
  const [cols, setCols] = useState(RequestTasksCols);
  const [selectedRows, setSelectedRows] = useState<Record<string, boolean>>({});
  const [taskTemplateName, setTaskTemplateName] = useState('');

  const visibleColumns = useMemo(
    () => cols.filter((col) => !col.hidden),
    [cols]
  );
  const idsOfSelected = Object.keys(selectedRows).filter(
    (selected) => selectedRows[selected] && Number(selected)
  );

  const selectedRowsFullData = useMemo(
    () => data?.data.filter((row) => row.id === Number(idsOfSelected[0])) || [],
    [selectedRows]
  );

  const revokeDisabled = useMemo(() => {
    return !(
      idsOfSelected.length === 1 &&
      selectedRowsFullData[0]?.canRevokeFromExecution
    );
  }, [idsOfSelected, selectedRowsFullData]);

  const rows = useMemo(() => {
    return data?.data.map((rows) => {
      return {
        ...rows,
        title: rows.title,
        author: rows.author,
        performer: rows.performer,
        objectLabel: rows.objectLabel,
        termLabel: rows.termLabel,
        comments: rows.comments,
        result: rows.result,
        performerComment: rows.performerComment,
        status: rows.status,
        completeDate:
          rows.completeDate &&
          new Date(rows.completeDate).toLocaleString().replace(/,/g, ''),
        createDate:
          rows.createDate &&
          new Date(rows.createDate).toLocaleString().replace(/,/g, ''),
        id: rows.id,
        editor: rows.editor,
        externalTaskId: rows.externalTaskId,
      };
    });
  }, [data?.data]);

  const handleCloseTaskTemplateModal = useCallback(() => {
    setTaskTemplate(false);
    refetch();
  }, [refetch]);

  const handleChooseTaskTemplateName = useCallback(
    (curName: string) => {
      setTaskTemplate(true);
      setTaskTemplateName(curName);
      refetch();
    },
    [refetch]
  );

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const handleDetails = useCallback(() => {
    window
      .open(
        `/tasks/details/${idsOfSelected[0]}`,
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  }, [idsOfSelected, openFormInNewTab]);

  const handleRevoke = useCallback(() => {
    revokeTask(Number(idsOfSelected[0]));
  }, [revokeTask, idsOfSelected]);

  const singleSelected = useMemo(
    () => idsOfSelected.length === 1,
    [idsOfSelected]
  );

  const deleteDisabled = useMemo(() => {
    return !(singleSelected && selectedRowsFullData[0]?.canDelete);
  }, [singleSelected, selectedRowsFullData]);

  const handleDelete = useCallback(() => {
    deleteTask(Number(idsOfSelected[0]));
  }, [deleteTask, idsOfSelected]);
  const sendExeDisabled = useMemo(() => {
    return !(singleSelected && selectedRowsFullData[0]?.canSendToExecution);
  }, [singleSelected, selectedRows]);

  const sendReexecutionDisabled = useMemo(() => {
    return !(singleSelected && selectedRowsFullData[0]?.canReturnToExecution);
  }, [singleSelected, selectedRows]);

  const handleSendExe = useCallback(() => {
    sendToExecution(Number(idsOfSelected[0]));
  }, [sendToExecution, idsOfSelected]);

  const handleReexecute = useCallback(() => {
    reexecution(Number(idsOfSelected[0]));
  }, [reexecution, idsOfSelected]);

  const handleRowDoubleClick = useCallback(
    (row: any) => {
      if (row.id) {
        window
          .open(
            `/tasks/details/${row.id}`,
            openFormInNewTab ? '_blank' : '_self'
          )
          ?.focus();
      }
    },
    [openFormInNewTab]
  );

  return (
    <div className={styles.container}>
      <TreeDataGrid
        formKey={'tasks'}
        cols={cols}
        rows={rows || []}
        groups={groups}
        className={styles.table}
        onRowSelect={setSelectedRows}
        onRowDoubleClick={handleRowDoubleClick}
        loading={isLoading}
        expanded
        childrenNoSelect
        createTree={(rows: DefaultRow[]) => createTreeByRows(rows, true)}
        createColumn={(handleRowExpand: (rowId: string) => void) =>
          CreateRightGroupColumn(groups, handleRowExpand)
        }
        showFilters={tableFilters}
      >
        <ToolsPanel
          className={styles.tools}
          setFilters={setTableFilters}
          leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
        >
          {[
            <ToolButton
              label={'Подробно'}
              disabled={idsOfSelected.length !== 1}
              onClick={handleDetails}
              fast
              endIcon={<ChevronRight />}
            />,
            <MenuToolButton
              label={'Создать'}
              startIcon={<AddCircleOutline />}
              endIcon={<ExpandMore />}
            >
              <MenuButtonItem onClick={() => setTaskModal('task')}>
                Задачу
              </MenuButtonItem>
              ,
              <MenuButtonItem
                onClick={() => setTaskModal('subtask')}
                disabled={
                  !(
                    idsOfSelected.length === 1 &&
                    data?.data.find(
                      (task) => task.id === Number(idsOfSelected[0])
                    )?.canCreateSubtask
                  )
                }
              >
                Подзадачу
              </MenuButtonItem>
              ,
              <MenuButtonItem
                onClick={() =>
                  handleChooseTaskTemplateName('Создание задач по шаблону')
                }
              >
                Задачу по шаблону
              </MenuButtonItem>
              ,
            </MenuToolButton>,
            <ToolButton
              label="Сохранить в шаблон"
              startIcon={<AddCircleOutline />}
              onClick={() => setTemplateModal('task')}
              disabled={idsOfSelected.length !== 1}
              fast
            />,
            ...RequestTasksExecutionButtons.map((action) => {
              const ButtonIcon = ExecutionButtonIcons[action];
              return (
                <ExecuteButton
                  startIcon={<ButtonIcon />}
                  statusId={ExecutionStatusIds[action]}
                  rows={selectedRowsFullData}
                  ids={idsOfSelected.map((id) => Number(id))}
                  label={action}
                  key={action}
                  setSelected={setSelectedRows}
                />
              );
            }),
            <ToolButton
              label={'Отправить на исполнение'}
              startIcon={<Send />}
              onClick={handleSendExe}
              disabled={sendExeDisabled}
            />,
            <ToolButton
              label="Отозвать на доработку"
              startIcon={<Restore />}
              onClick={handleRevoke}
              disabled={revokeDisabled}
            />,
            <DeleteToolButton
              label={'Удалить'}
              startIcon={<DeleteOutline />}
              title={'Удаление задач'}
              description={`Вы действительно хотите удалить задачу?`}
              onConfirm={handleDelete}
              disabled={deleteDisabled}
            />,
          ]}
        </ToolsPanel>
      </TreeDataGrid>
      <NewTaskModal
        parent={data?.data.find((task) => task.id === Number(idsOfSelected[0]))}
        objectTypeCode={'REQUEST'}
        objectId={request.id}
        open={taskModal}
        closeModal={() => {
          setTaskModal(null);
          refetch();
        }}
      />
      <SaveToTemplateModal
        open={templateModal}
        setOpen={setTemplateModal}
        objectTypeCode={'REQUEST'}
        selectedId={Number(idsOfSelected[0])}
        rows={data?.data || []}
      />
      <TaskTemplateForm
        objectTypeCode={'REQUEST'}
        open={taskTemplate}
        onClose={handleCloseTaskTemplateModal}
        formName={taskTemplateName}
        id={request?.id}
      />
    </div>
  );
};

// onClick={() => handleChooseTaskTemplateName('Создание задач по шаблону')
// () => handleChooseTaskTemplateName('Создание подзадач по шаблону')

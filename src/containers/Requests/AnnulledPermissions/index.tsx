import { useCallback, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import { matchPath, useLocation } from 'react-router-dom';

import {
  AddCircleOutline,
  ChevronRight,
  DeleteOutline,
  Refresh,
} from '@mui/icons-material';

import { DataTable } from '../../../components/CustomDataGrid/DataTable';
import { RoundButton } from '../../../components/RoundButton';
import { ToolButton, ToolsPanel } from '../../../components/ToolsPanel';
import { DeleteToolButton } from '../../../components/ToolsPanel/DeleteToolButton';
import { RequestAnnulledPermissionsCols } from '../../../constants/requests';
import { Routes } from '../../../constants/routes';
import { ConclusionSearch } from '../../../pages/CommonRequest/ConclusionSearch';
import { PermissionSearch } from '../../../pages/CommonRequest/PermissionSearch';
import {
  useAddAnnulledPermissionMutation,
  useDeleteAnnulledPermissionMutation,
  useGetAnnulledPermissionsQuery,
} from '../../../services/api/request';
import { RootState } from '../../../store';
import { AnnulledPermissions as AnnulledPermission } from '../../../types/requests';
import { AnnulledPermissionsToRowAdapter } from '../../../utils/requests';

import styles from './styles.module.scss';

export interface AnnulledPermissionsProps {
  id: number;
}

export const AnnulledPermissions = ({ id }: AnnulledPermissionsProps) => {
  const location = useLocation();
  const requestId = Number(
    matchPath(Routes.REQUESTS.REQUEST_TABS, location.pathname)?.params?.id || ''
  );
  const [addPermission] = useAddAnnulledPermissionMutation();

  const [popup, setPopup] = useState(false);
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);

  const [selected, setSelected] = useState<Record<number, boolean>>({});

  const selectedPermission = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const openPage = useCallback(
    (path: string = '') => {
      window
        .open(`/permissions${path}`, openFormInNewTab ? '_blank' : '_self')
        ?.focus();
    },
    [selectedPermission, openFormInNewTab]
  );

  const [searchPopup, setSearchPopup] = useState<
    'conclusion' | 'permission' | null
  >(null);

  const { data, refetch, isLoading } = useGetAnnulledPermissionsQuery({
    requestId,
    page,
    size: perPage,
  });

  const permissions = useMemo(() => {
    return data?.data;
  }, [data]);

  const [permission, setPermission] = useState<{ id: string; number: string }>({
    id: '',
    number: '',
  });

  const setDocumentField = ({ id, number }: { id: string; number: string }) => {
    setPermission({ id, number });
  };

  const [deletePermission] = useDeleteAnnulledPermissionMutation();

  const rows = useMemo(() => {
    return AnnulledPermissionsToRowAdapter(permissions);
  }, [permissions]);

  const handleDelete = useCallback(async () => {
    if (selectedPermission) {
      await deletePermission({
        requestId,
        permissionId: permissions?.find(
          (permission) => permission.id === selectedPermission[0]
        )?.permissionId!,
      });
      setSelected({});
      setPopup(false);
      refetch();
    }
  }, [selectedPermission]);

  const handleClose = useCallback((e: React.MouseEvent) => {
    e.stopPropagation();
    setPopup(false);
  }, []);

  const handleAdd = useCallback(
    async (data: any, selectedRows: string[]) => {
      if (selectedRows) {
        await addPermission({ requestId, permissionIds: selectedRows });
      }
    },
    [requestId]
  );

  const handleDoubleClick = useCallback((permission: AnnulledPermission) => {
    if (permission.id) {
      openPage(`?id=${permission.permissionId}`);
    }
  }, []);

  return (
    <div className={styles.content}>
      <DataTable
        formKey={'request_annulled_permissions'}
        cols={RequestAnnulledPermissionsCols}
        rows={rows}
        onRowSelect={setSelected}
        onRowDoubleClick={handleDoubleClick}
        height="fullHeight"
        loading={isLoading}
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: data?.totalCount || 0,
        }}
        className={styles.table}
      >
        <ToolsPanel
          className={styles.toolBar}
          leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
        >
          <ToolButton
            label="Добавить"
            startIcon={<AddCircleOutline />}
            onClick={() => setSearchPopup('permission')}
            fast
          />
          <ToolButton
            label="Подробно"
            endIcon={<ChevronRight />}
            disabled={selectedPermission.length !== 1}
            onClick={() =>
              selectedPermission &&
              openPage(
                `?id=${permissions?.find(
                  (permission) => permission.id === selectedPermission[0]
                )?.permissionId!}`
              )
            }
            fast
          />
          <DeleteToolButton
            label="Удалить"
            startIcon={<DeleteOutline />}
            disabled={selectedPermission.length !== 1}
            fast
            description={` Вы действительно хотите удалить разрешение № ${permissions?.find(
              (permission) => permission.id === selectedPermission[0]
            )?.number!} `}
            title={'Удаление разрешение'}
            onConfirm={handleDelete}
          />
        </ToolsPanel>
      </DataTable>
      <PermissionSearch
        open={searchPopup === 'permission'}
        setSearchPopup={setSearchPopup}
        onSubmit={handleAdd}
      />
      <ConclusionSearch
        open={searchPopup === 'conclusion'}
        setSearchPopup={setSearchPopup}
        onSubmit={handleAdd}
      />
    </div>
  );
};

import { useCallback, useEffect, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';

import {
  AddCircleOutline,
  ChevronRight,
  DeleteOutline,
  Refresh,
} from '@mui/icons-material';

import { DataTable } from '../../../components/CustomDataGrid/DataTable';
import { RoundButton } from '../../../components/RoundButton';
import {
  DeleteToolButton,
  ToolButton,
  ToolsPanel,
} from '../../../components/ToolsPanel';
import { RequestConclusionsCols } from '../../../constants/requests';
import {
  useDeleteConclusionMutation,
  useGetConclusionQuery,
} from '../../../services/api/request';
import { RootState } from '../../../store';
import { ConclusionData } from '../../../types/conclusions';
import { ConclusionsToRowAdapter } from '../../../utils/requests';

import styles from './styles.module.scss';

interface ConclusionsProps {
  id: number;
  num: any;
}

export const Conclusions: React.FC<ConclusionsProps> = ({ id, num }) => {
  const [popup, setPopup] = useState(false);
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});

  const { data, refetch, isLoading } = useGetConclusionQuery({
    id,
    page,
    size: perPage,
  });
  const [deleteConclusion] = useDeleteConclusionMutation();

  const rows = useMemo(() => {
    return ConclusionsToRowAdapter(data?.data);
  }, [data?.data]);

  const selectedConclusions = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const openPage = useCallback(
    (path: string = '') => {
      window
        .open(`/conclusions${path}`, openFormInNewTab ? '_blank' : '_self')
        ?.focus();
    },
    [selectedConclusions]
  );

  const handleDelete = useCallback(async () => {
    if (selectedConclusions) {
      await deleteConclusion({ id: selectedConclusions[0] });
      setPopup(false);
      refetch();
    }
  }, [selectedConclusions]);

  const handleClose = useCallback((e: React.MouseEvent) => {
    e.stopPropagation();
    setPopup(false);
  }, []);

  useEffect(() => {
    refetch();
  }, [page, perPage]);

  const handleDoubleClick = useCallback(
    (conclusion: Partial<ConclusionData>) => {
      if (conclusion.id) {
        openPage(`?id=${conclusion.id}`);
      }
    },
    [openPage]
  );

  return (
    <div className={styles.content}>
      <DataTable
        formKey={'request_conslusions'}
        cols={RequestConclusionsCols}
        rows={rows}
        onRowSelect={setSelected}
        onRowDoubleClick={handleDoubleClick}
        height="fullHeight"
        loading={isLoading}
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: data?.totalCount || 0,
        }}
        className={styles.table}
      >
        <ToolsPanel
          className={styles.toolBar}
          leftActions={
            <>
              <RoundButton icon={<Refresh />} onClick={refetch} />
            </>
          }
        >
          <ToolButton
            label="Подробно"
            endIcon={<ChevronRight />}
            disabled={selectedConclusions.length !== 1}
            onClick={() =>
              selectedConclusions && openPage(`?id=${selectedConclusions[0]}`)
            }
            fast
          />
          <ToolButton
            label="Создать"
            startIcon={<AddCircleOutline />}
            onClick={() => openPage(`?reqId=${id}`)}
            fast
          />
          <DeleteToolButton
            label={'Удалить'}
            startIcon={<DeleteOutline />}
            disabled={selectedConclusions.length !== 1}
            fast
            description={`Вы действительно хотите удалить заключение № ${selectedConclusions[0]}?`}
            title={'Удаление заключения'}
            onConfirm={handleDelete}
          />
        </ToolsPanel>
      </DataTable>
    </div>
  );
};

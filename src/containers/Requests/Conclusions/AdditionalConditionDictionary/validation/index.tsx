import { boolean, object, string } from 'yup';

import { AdditionalConditionsDictionariesFields } from '../../../../../constants/conclusions';

export const validationSchema = object({
  [AdditionalConditionsDictionariesFields.NAME]:
    string().required('Обязательное поле'),
  [AdditionalConditionsDictionariesFields.CONCLUSION]: string().nullable(),
  [AdditionalConditionsDictionariesFields.PERMISSION]: string().nullable(),
  [AdditionalConditionsDictionariesFields.ISACTUAL]: boolean(),
});

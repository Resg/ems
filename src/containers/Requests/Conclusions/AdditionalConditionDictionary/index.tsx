import React, { useMemo, useState } from 'react';
import { Form, Formik } from 'formik';
import { useNavigate } from 'react-router-dom';

import { Edit, EditOff, Save } from '@mui/icons-material';
import { Grid, Stack } from '@mui/material';

import { CheckboxInput } from '../../../../components/CheckboxInput';
import Input from '../../../../components/Input';
import { RoundButton } from '../../../../components/RoundButton';
import {
  AdditionalConditionDictionariesDefaults,
  AdditionalConditionsDictionariesFields,
  AdditionalConditionsDictionariesLabels,
} from '../../../../constants/conclusions';
import {
  useCreateAdditionalConclusionTemplateMutation,
  useGetAdditionalConclusionDataTemplateQuery,
  useUpdateAdditionalConclusionTemplateMutation,
} from '../../../../services/api/dictionaries';

import { validationSchema } from './validation';

interface AdditionalConclusionsProps {
  id: number;
}

export const AdditionalConditionDictionary: React.FC<
  AdditionalConclusionsProps
> = ({ id }) => {
  const navigate = useNavigate();
  const [editMode, setEditMode] = useState(!id);
  const { data, refetch } = useGetAdditionalConclusionDataTemplateQuery(
    { id },
    { skip: !id }
  );

  const [updateAdditionalConclusion] =
    useUpdateAdditionalConclusionTemplateMutation();
  const [createAdditionalConclusion] =
    useCreateAdditionalConclusionTemplateMutation();

  const initialValues = useMemo(
    () => ({
      ...(data || AdditionalConditionDictionariesDefaults),
    }),
    [data]
  );

  const handleSubmit = async (values: any) => {
    const updateData = {
      name: values.name,
      conclusionText: values.conclusionText,
      permissionText: values.permissionText,
      isActual: Boolean(values.isActual),
    };
    let response;
    if (id) {
      response = await updateAdditionalConclusion({
        id: id,
        data: updateData,
      });
    } else {
      response = await createAdditionalConclusion({
        data: updateData,
      });
    }
    setEditMode(false);
    refetch();
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      enableReinitialize
      onSubmit={handleSubmit}
    >
      {({
        handleSubmit,
        handleReset,
        handleChange,
        values,
        errors,
        touched,
        handleBlur,
      }) => {
        const handleEditOff = () => {
          handleReset();
          setEditMode(false);
        };
        return (
          <Form>
            <Stack my={3} spacing={2} direction="row">
              {!editMode && (
                <RoundButton
                  icon={<Edit />}
                  onClick={() => setEditMode(true)}
                />
              )}
              {editMode && (
                <>
                  <RoundButton icon={<Save />} onClick={() => handleSubmit()} />
                  <RoundButton icon={<EditOff />} onClick={handleEditOff} />
                </>
              )}
            </Stack>
            <Grid container rowSpacing={2} columnSpacing={12}>
              <Grid item xs={12}>
                <Input
                  name={AdditionalConditionsDictionariesFields.NAME}
                  label={
                    AdditionalConditionsDictionariesLabels[
                      AdditionalConditionsDictionariesFields.NAME
                    ]
                  }
                  value={
                    values[AdditionalConditionsDictionariesFields.NAME] || ''
                  }
                  onChange={handleChange}
                  multiline
                  disabled={!editMode}
                  required
                  error={Boolean(
                    errors[AdditionalConditionsDictionariesFields.NAME] &&
                      touched[AdditionalConditionsDictionariesFields.NAME]
                  )}
                  helperText={
                    errors[AdditionalConditionsDictionariesFields.NAME]
                  }
                  onBlur={handleBlur}
                />
              </Grid>
            </Grid>
            <div>
              <Grid sx={{ mb: 5, mt: 2 }} container columnSpacing={10}>
                <Grid item xs={6}>
                  <Input
                    name={AdditionalConditionsDictionariesFields.CONCLUSION}
                    label={
                      AdditionalConditionsDictionariesLabels[
                        AdditionalConditionsDictionariesFields.CONCLUSION
                      ]
                    }
                    value={
                      values[
                        AdditionalConditionsDictionariesFields.CONCLUSION
                      ] || ''
                    }
                    onChange={handleChange}
                    multiline
                    rows={16}
                    disabled={!editMode}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Input
                    name={AdditionalConditionsDictionariesFields.PERMISSION}
                    label={
                      AdditionalConditionsDictionariesLabels[
                        AdditionalConditionsDictionariesFields.PERMISSION
                      ]
                    }
                    value={
                      values[
                        AdditionalConditionsDictionariesFields.PERMISSION
                      ] || ''
                    }
                    onChange={handleChange}
                    multiline
                    rows={16}
                    disabled={!editMode}
                  />
                </Grid>
                <Grid item xs={1}>
                  <CheckboxInput
                    name={AdditionalConditionsDictionariesFields.ISACTUAL}
                    label={
                      AdditionalConditionsDictionariesLabels[
                        AdditionalConditionsDictionariesFields.ISACTUAL
                      ]
                    }
                    disabled={!editMode}
                  />
                </Grid>
              </Grid>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

import { useCallback, useEffect, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';

import {
  Add,
  Cancel,
  ChevronRight,
  DeleteOutline,
  Refresh,
} from '@mui/icons-material';
import { Button, Card, Dialog, Stack } from '@mui/material';

import { CreateDocumentToolButton } from '../../../../components/CreateDocumentByTemplateForm/CreateDocumentToolButton';
import { DataTable } from '../../../../components/CustomDataGrid/DataTable';
import { RoundButton } from '../../../../components/RoundButton';
import { ToolButton, ToolsPanel } from '../../../../components/ToolsPanel';
import { RequestConclusionDocumentsCols } from '../../../../constants/requests';
import { Routes } from '../../../../constants/routes';
import {
  useDeleteConclusionDocumentsMutation,
  useGetConclusionDocumentsQuery,
} from '../../../../services/api/request';
import { RootState } from '../../../../store';
import { ConclusionDocumentsData } from '../../../../types/requests';
import { ConclusionDocumentsToRowAdapter } from '../../../../utils/requests';

import styles from './styles.module.scss';

interface ConclusionsProps {
  id: number;
}

export const ConclusionDoc: React.FC<ConclusionsProps> = ({ id }) => {
  const [popup, setPopup] = useState(false);
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});

  const { data, refetch, isLoading } = useGetConclusionDocumentsQuery({
    id,
    page,
    size: perPage,
  });
  const [deleteConclusionDoc] = useDeleteConclusionDocumentsMutation();

  const rows = useMemo(() => {
    return ConclusionDocumentsToRowAdapter(data?.data);
  }, [data?.data]);

  const selectedConclusionsDocuments = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const handleDetails = useCallback(() => {
    window
      .open(
        Routes.DOCUMENTS.DOCUMENT.replace(
          ':id',
          selectedConclusionsDocuments[0]
        ),
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  }, [selectedConclusionsDocuments, openFormInNewTab]);

  const handleDelete = useCallback(async () => {
    await deleteConclusionDoc({ id: selectedConclusionsDocuments[0] });
    setPopup(false);
    refetch();
  }, [selectedConclusionsDocuments]);

  const handleClose = useCallback((e: React.MouseEvent) => {
    e.stopPropagation();
    setPopup(false);
  }, []);

  useEffect(() => {
    refetch();
  }, [page, perPage]);

  const handleDoubleClick = useCallback(
    (document: Partial<ConclusionDocumentsData>) => {
      if (document.id) {
        window
          .open(
            Routes.DOCUMENTS.DOCUMENT.replace(':id', document.id),
            openFormInNewTab ? '_blank' : '_self'
          )
          ?.focus();
      }
    },
    [openFormInNewTab]
  );

  return (
    <div className={styles.content}>
      <DataTable
        formKey={'request_consolusions_documents'}
        cols={RequestConclusionDocumentsCols}
        rows={rows}
        onRowSelect={setSelected}
        onRowDoubleClick={handleDoubleClick}
        height="fullHeight"
        loading={isLoading}
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: data?.totalCount || 0,
        }}
        className={styles.table}
      >
        <ToolsPanel
          className={styles.toolBar}
          leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
        >
          <ToolButton
            label="Подробно"
            startIcon={<ChevronRight />}
            disabled={selectedConclusionsDocuments.length !== 1}
            onClick={() => handleDetails()}
            fast
          />
          <CreateDocumentToolButton
            label={'Добавить'}
            startIcon={<Add />}
            fast
            objectTypeCode={'CONCLUSION'}
            objectIds={String(id)}
            onDocumentCreated={refetch}
          />

          <ToolButton
            label="Удалить"
            startIcon={<DeleteOutline />}
            disabled={selectedConclusionsDocuments.length !== 1}
            onClick={(e: React.MouseEvent) => {
              e.stopPropagation();
              setPopup(true);
            }}
            fast
          >
            <Dialog open={popup}>
              <Card className={styles.card}>
                <div className={styles.dialogText}>
                  Вы действительно хотите удалить заключение №
                  {
                    data?.data?.find(
                      (conclusionDoc) =>
                        conclusionDoc.id === selectedConclusionsDocuments[0]
                    )?.number
                  }
                  ?
                </div>
                <Stack
                  direction="row"
                  justifyContent="flex-end"
                  spacing={1.5}
                  width="100%"
                >
                  <Button
                    variant="contained"
                    endIcon={<Cancel />}
                    onClick={handleClose}
                  >
                    Отменить
                  </Button>
                  <Button
                    variant="outlined"
                    endIcon={<DeleteOutline />}
                    onClick={handleDelete}
                  >
                    Удалить
                  </Button>
                </Stack>
              </Card>
            </Dialog>
          </ToolButton>
        </ToolsPanel>
      </DataTable>
    </div>
  );
};

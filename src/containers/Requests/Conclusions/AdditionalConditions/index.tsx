import React, { useMemo, useState } from 'react';
import { Form, Formik } from 'formik';

import { Edit, EditOff, Save } from '@mui/icons-material';
import { Grid, Stack } from '@mui/material';

import Input from '../../../../components/Input';
import { RoundButton } from '../../../../components/RoundButton';
import {
  AdditionalConditionDocumentDefaults,
  AdditionalConditionsFields,
  AdditionalConditionsLabels,
} from '../../../../constants/conclusions';
import {
  useGetAdditionalConclusionDataQuery,
  useUpdateAdditionalConclusionMutation,
} from '../../../../services/api/conclusions';

import { validationSchema } from './validation';

interface AdditionalConclusionsProps {
  id: number;
}

export const AdditionalCondition: React.FC<AdditionalConclusionsProps> = ({
  id,
}) => {
  const [editMode, setEditMode] = useState(!id);

  const [updateAdditionalConclusion] = useUpdateAdditionalConclusionMutation();
  const { data, refetch } = useGetAdditionalConclusionDataQuery(
    { id },
    { skip: !id }
  );

  const initialValues = useMemo(
    () => ({
      ...(data || AdditionalConditionDocumentDefaults),
    }),
    [data]
  );

  const isViewPage = useMemo(() => {
    return window.location.href.includes('view');
  }, [window.location.href]);

  const handleSubmit = async (values: any) => {
    const updateData = {
      ordinalNumber: values.ordinalNumber,
      name: values.name,
      conclusionTextEdit: values.conclusionText,
      permissionTextEdit: values.permissionText,
    };
    await updateAdditionalConclusion({
      id: id,
      data: updateData,
    });
    setEditMode(false);
    refetch();
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      enableReinitialize
      onSubmit={handleSubmit}
    >
      {({
        handleSubmit,
        handleReset,
        handleChange,
        values,
        errors,
        touched,
        setTouched,
      }) => {
        const handleEditOff = () => {
          handleReset();
          setEditMode(false);
        };

        return (
          <Form>
            {isViewPage ? null : (
              <Stack my={3} spacing={2} direction="row">
                {!editMode && (
                  <RoundButton
                    icon={<Edit />}
                    onClick={() => setEditMode(true)}
                  />
                )}
                {editMode && (
                  <>
                    <RoundButton
                      icon={<Save />}
                      onClick={() => handleSubmit()}
                    />
                    <RoundButton icon={<EditOff />} onClick={handleEditOff} />
                  </>
                )}
              </Stack>
            )}

            <Grid sx={{ mb: 5, mt: 2 }} container columnSpacing={10}>
              <Grid item xs={6}>
                <Input
                  name={AdditionalConditionsFields.NUMBER}
                  label={
                    AdditionalConditionsLabels[
                      AdditionalConditionsFields.NUMBER
                    ]
                  }
                  value={values[AdditionalConditionsFields.NUMBER] || ''}
                  onChange={handleChange}
                  multiline
                  disabled={!editMode}
                  error={Boolean(
                    errors[AdditionalConditionsFields.NUMBER] &&
                      touched[AdditionalConditionsFields.NUMBER]
                  )}
                  helperText={errors[AdditionalConditionsFields.NUMBER]}
                  onBlur={() =>
                    setTouched({ [AdditionalConditionsFields.NUMBER]: true })
                  }
                  required
                />
              </Grid>
              <Grid item xs={6}>
                <Input
                  name={AdditionalConditionsFields.NAME}
                  label={
                    AdditionalConditionsLabels[AdditionalConditionsFields.NAME]
                  }
                  value={values[AdditionalConditionsFields.NAME] || ''}
                  onChange={handleChange}
                  multiline
                  disabled={!editMode}
                />
              </Grid>
            </Grid>
            <div>
              <Grid sx={{ mb: 5, mt: 2 }} container columnSpacing={10}>
                <Grid item xs={6}>
                  <Input
                    name={AdditionalConditionsFields.CONCLUSION}
                    label={
                      AdditionalConditionsLabels[
                        AdditionalConditionsFields.CONCLUSION
                      ]
                    }
                    value={values[AdditionalConditionsFields.CONCLUSION] || ''}
                    onChange={handleChange}
                    multiline
                    rows={24}
                    disabled={!editMode}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Input
                    name={AdditionalConditionsFields.PERMISSION}
                    label={
                      AdditionalConditionsLabels[
                        AdditionalConditionsFields.PERMISSION
                      ]
                    }
                    value={values[AdditionalConditionsFields.PERMISSION] || ''}
                    onChange={handleChange}
                    multiline
                    rows={24}
                    disabled={!editMode}
                  />
                </Grid>
              </Grid>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

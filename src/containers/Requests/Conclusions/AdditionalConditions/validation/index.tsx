import { number, object, string } from 'yup';

import { AdditionalConditionsFields } from '../../../../../constants/conclusions';

export const validationSchema = object({
  [AdditionalConditionsFields.NUMBER]: number(),
  [AdditionalConditionsFields.NAME]: string().nullable(),
  [AdditionalConditionsFields.CONCLUSION]: string().nullable(),
  [AdditionalConditionsFields.PERMISSION]: string().nullable(),
});

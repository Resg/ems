import React, { useCallback, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';

import { Add, ChevronRight, Delete, NoteAdd } from '@mui/icons-material';
import { Refresh } from '@mui/icons-material/';

import { CreateDocumentToolButton } from '../../../components/CreateDocumentByTemplateForm/CreateDocumentToolButton';
import { DataTable } from '../../../components/CustomDataGrid';
import { RoundButton } from '../../../components/RoundButton';
import { SearchDocumentModal } from '../../../components/SearchDocumentModal';
import { ToolButton, ToolsPanel } from '../../../components/ToolsPanel';
import { DeleteToolButton } from '../../../components/ToolsPanel/DeleteToolButton';
import {
  InitialDocumentsCols,
  RequestDocumentsCols,
} from '../../../constants/requests';
import { Routes } from '../../../constants/routes';
import { useAddDocumentsToFavoritesMutation } from '../../../services/api/document';
import {
  useAddDocumentsToRequestMutation,
  useConfirmRequestMutation,
  useDeleteDocumentMutation,
  useDeleteInitialDocumentMutation,
  useGetRequestDocumentsQuery,
  useGetRequestInitiatingDocumentsQuery,
  useMakeDocumentMaterialMutation,
  useMakeInitialMutation,
} from '../../../services/api/request';
import { RootState } from '../../../store';
import { RequestDocument } from '../../../types/requests';
import { massServerQuery } from '../../../utils/incomingDocumentTasks';
import {
  RequestDocumentsToRowAdapter,
  RequestInitialDocumentsToRowAdapter,
} from '../../../utils/requests';

import styles from './styles.module.scss';

export interface DocumentsProps {
  id: number;
}

export const Documents: React.FC<DocumentsProps> = ({ id }) => {
  const {
    data: initialDocuments,
    refetch: refetchInitial,
    isLoading: isLoadingInitial,
  } = useGetRequestInitiatingDocumentsQuery({
    id,
  });
  const {
    data: documents,
    refetch: refetchDocuments,
    isLoading: isLoadingDocuments,
  } = useGetRequestDocumentsQuery({ id });
  // const { data } = useGetRequestQuery({ id }, { skip: !id });
  const [deleteInitial] = useDeleteInitialDocumentMutation();
  const [deleteDocument] = useDeleteDocumentMutation();
  const [makeMaterial] = useMakeDocumentMaterialMutation();
  const [addDocuments] = useAddDocumentsToRequestMutation();
  const [addToFavorite] = useAddDocumentsToFavoritesMutation();
  const [makeInitial] = useMakeInitialMutation();
  const [confirmRequest] = useConfirmRequestMutation();
  const [initialSelected, setInitialSelected] = useState<
    Record<number, boolean>
  >({});
  const [filters, setFilters] = useState(false);
  const [tableFilters, setTableFilters] = useState(false);
  const [documentsSelected, setDocumentsSelected] = useState<
    Record<number, boolean>
  >({});
  const [openModal, setOpenModal] = useState(false);
  const [openAddDocModal, setOpenAddDocModal] = useState(false);

  const mappedInitial = useMemo(
    () =>
      (initialDocuments?.data || []).reduce(
        (acc, doc) => ({ ...acc, [doc.id]: doc }),
        {} as Record<number, RequestDocument>
      ),
    [initialDocuments?.data]
  );
  const initialIds = useMemo(
    () =>
      Object.keys(initialSelected)
        .map(Number)
        .filter((key) => initialSelected[key]),
    [initialSelected]
  );
  const documentIds = useMemo(
    () =>
      Object.keys(documentsSelected)
        .map(Number)
        .filter((key) => documentsSelected[key]),
    [documentsSelected]
  );

  const initialRows = useMemo(() => {
    return RequestInitialDocumentsToRowAdapter(initialDocuments?.data);
  }, [initialDocuments?.data]);

  const DocumentRows = useMemo(() => {
    return RequestDocumentsToRowAdapter(documents?.data);
  }, [documents?.data, initialDocuments?.data]);

  const selectedDocumentRows = useMemo(
    () => DocumentRows.filter((el) => documentIds.includes(el.id)),
    [DocumentRows, documentIds]
  );

  const checkDocumentType = useMemo(() => {
    const arrType = selectedDocumentRows.map((el: any) =>
      el.type === 'Входящий' ? true : false
    );

    if (arrType.includes(false)) {
      return false;
    }
    return true;
  }, [selectedDocumentRows]);

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const handleOpenDocument = useCallback(
    (id: number) => () => {
      window
        .open(
          Routes.DOCUMENTS.DOCUMENT.replace(':id', id),
          openFormInNewTab ? '_blank' : '_self'
        )
        ?.focus();
    },
    [openFormInNewTab]
  );

  const handleDeleteInitial = useCallback(() => {
    deleteInitial({ id, initialIds });
  }, [deleteInitial, id, initialIds]);

  const handleDeleteDocuments = useCallback(() => {
    deleteDocument({ id, documentIds });
  }, [deleteDocument, documentIds, id]);

  const handleMakeMaterial = useCallback(() => {
    massServerQuery(initialIds, (documentId) =>
      makeMaterial({ id, documentId })
    );
  }, [id, initialIds, makeMaterial]);

  const handleMakeInitial = useCallback(() => {
    makeInitial({ documentIds, id });
    setDocumentsSelected({});
  }, [documentIds, id, makeInitial]);

  const addInitial = useCallback(
    (ids: number[]) => {
      makeInitial({ id, documentIds: ids });
    },
    [id]
  );

  const disabledMakeMaterial = useMemo(
    () =>
      !initialIds.length ||
      initialIds.some(
        (id) =>
          mappedInitial[id]?.isMaterial || !mappedInitial[id]?.canBeMaterial
      ),
    [initialIds, mappedInitial]
  );

  const handleAddDocument = useCallback(
    (ids: number[]) => {
      addDocuments({ id, documentIds: ids });
    },
    [id]
  );

  const handleAddDocumentToFavorite = useCallback(() => {
    addToFavorite({ documentIds });
  }, [addToFavorite, documentIds]);

  const handleConfirmRequest = useCallback(() => {
    confirmRequest({ id });
  }, [confirmRequest, id]);

  const handleRowDoubleClick = useCallback(
    (e: RequestDocument) => {
      if (e.id) {
        window
          .open(
            Routes.DOCUMENTS.DOCUMENT.replace(':id', e.id),
            openFormInNewTab ? '_blank' : '_self'
          )
          ?.focus();
      }
    },
    [openFormInNewTab]
  );

  return (
    <div className={styles.container}>
      <div className={styles.title}>Инициирующие документы</div>

      <DataTable
        cols={InitialDocumentsCols}
        rows={initialRows}
        className={styles.table}
        height="122px"
        loading={isLoadingInitial}
        onRowSelect={setInitialSelected}
        onRowDoubleClick={handleRowDoubleClick}
        formKey={'initiating_documents'}
        showFilters={tableFilters}
      >
        <ToolsPanel
          setFilters={setTableFilters}
          className={styles.tools}
          leftActions={
            <RoundButton icon={<Refresh />} onClick={() => refetchInitial()} />
          }
          formKey={'initiating_documents'}
        >
          <ToolButton
            label={'Подробно'}
            endIcon={<ChevronRight />}
            fast
            disabled={initialIds.length !== 1}
            onClick={handleOpenDocument(initialIds[0])}
          />
          <ToolButton
            label={'Материалы обращения'}
            disabled={disabledMakeMaterial}
            onClick={handleMakeMaterial}
          />
          <DeleteToolButton
            label={'Удалить'}
            startIcon={<Delete />}
            disabled={!initialIds.length}
            fast
            description={`Удалить ${
              initialIds.length > 1
                ? 'выбранные инициирующие документы'
                : 'выбранный инициирующий документ'
            } из заявки?`}
            title={'Подтверждение действия'}
            onConfirm={handleDeleteInitial}
          />
          <ToolButton
            label={'Добавить документ'}
            startIcon={<NoteAdd />}
            onClick={() => setOpenModal(true)}
            fast
          />
        </ToolsPanel>
      </DataTable>
      <div className={styles.title}>Документы по заявке</div>

      <DataTable
        cols={RequestDocumentsCols}
        rows={DocumentRows}
        className={styles.table}
        height="calc(100% - 384px)"
        onRowSelect={setDocumentsSelected}
        onRowDoubleClick={handleRowDoubleClick}
        loading={isLoadingDocuments}
        initialSelected={documentsSelected}
        formKey={'application_documents'}
        showFilters={filters}
      >
        <ToolsPanel
          setFilters={setFilters}
          className={styles.tools}
          leftActions={
            <RoundButton
              icon={<Refresh />}
              onClick={() => refetchDocuments()}
            />
          }
          formKey={'application_documents'}
        >
          <ToolButton
            label={'Подробно'}
            endIcon={<ChevronRight />}
            fast
            disabled={documentIds.length !== 1}
            onClick={handleOpenDocument(documentIds[0])}
          />
          <CreateDocumentToolButton
            label={'Добавить'}
            startIcon={<Add />}
            fast
            objectTypeCode={'REQUEST'}
            objectIds={String(id)}
            onDocumentCreated={handleAddDocument}
          />
          <DeleteToolButton
            label={'Удалить'}
            startIcon={<Delete />}
            disabled={!documentIds.length}
            fast
            description={`Удалить ${
              documentIds.length > 1
                ? 'выбранные документы'
                : 'выбранный документ'
            }?`}
            title={'Подтверждение действия'}
            onConfirm={handleDeleteDocuments}
          />
          <ToolButton label={'Закрыть заявку'} onClick={handleConfirmRequest} />
          <ToolButton
            label={'Добавить документ'}
            onClick={() => setOpenAddDocModal(true)}
          />
          <ToolButton
            label={'Инициирующий'}
            disabled={!documentIds.length || !checkDocumentType}
            onClick={handleMakeInitial}
          />
          <ToolButton
            disabled={!documentIds.length}
            label={'Добавить в избранное'}
            onClick={handleAddDocumentToFavorite}
          />
        </ToolsPanel>
      </DataTable>

      <SearchDocumentModal
        open={openModal}
        onChoice={addInitial}
        onClose={() => setOpenModal(false)}
        refethTable={refetchInitial}
      />
      <SearchDocumentModal
        open={openAddDocModal}
        onChoice={handleAddDocument}
        onClose={() => setOpenAddDocModal(false)}
        refethTable={refetchDocuments}
      />
    </div>
  );
};

import { useCallback, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import { matchPath, useLocation } from 'react-router-dom';

import {
  AddCircleOutline,
  ChevronRight,
  DeleteOutline,
  Refresh,
} from '@mui/icons-material';

import { DataTable } from '../../../components/CustomDataGrid';
import { RequestSearchModal } from '../../../components/RequestSearchModal';
import { RoundButton } from '../../../components/RoundButton';
import {
  DeleteToolButton,
  ToolButton,
  ToolsPanel,
} from '../../../components/ToolsPanel';
import { LinkRequestsCols } from '../../../constants/linkRequests';
import { Routes } from '../../../constants/routes';
import {
  useDeleteLinkRequestMutation,
  useGetLinkRequestQuery,
} from '../../../services/api/request';
import { RootState } from '../../../store';
import { linkRequestsToRowAdapter } from '../../../utils/linkRequests';

import styles from './styles.module.scss';

export interface LinkRequestsProps {
  id: number;
}

export const LinkRequests = ({ id }: LinkRequestsProps) => {
  const location = useLocation();
  const requestId = Number(
    matchPath(Routes.REQUESTS.REQUEST_TABS, location.pathname)?.params?.id || ''
  );

  const [popup, setPopup] = useState(false);
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);

  const [selected, setSelected] = useState<Record<number, boolean>>({});

  const [openModal, setOpenModal] = useState(false);
  const { data, refetch, isLoading } = useGetLinkRequestQuery(
    { requestId, page, size: perPage },
    { skip: !id }
  );

  const linkRequests = useMemo(() => {
    return data?.data;
  }, [data]);

  const selectedSolution = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected, linkRequests]);

  const refethTable = () => refetch();

  const [deleteSolution] = useDeleteLinkRequestMutation();

  const rows = useMemo(() => {
    return linkRequestsToRowAdapter(linkRequests);
  }, [linkRequests]);

  const handleDelete = useCallback(async () => {
    if (selectedSolution) {
      await deleteSolution({
        id: requestId,
        bondedRequestId: selectedSolution[0],
      });
      setSelected({});
      setPopup(false);
      refetch();
    }
  }, [selectedSolution, requestId]);
  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );
  const handleDetails = useCallback(() => {
    window
      .open(
        Routes.REQUESTS.REQUEST.replace(':id', selectedSolution[0]),
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  }, [selectedSolution, openFormInNewTab]);

  const handleRowDoubleClick = useCallback(
    (e: { id: number }) => {
      if (e.id) {
        window
          .open(
            Routes.REQUESTS.REQUEST.replace(':id', e.id),
            openFormInNewTab ? '_blank' : '_self'
          )
          ?.focus();
      }
    },
    [openFormInNewTab]
  );

  return (
    <div className={styles.content}>
      <DataTable
        formKey={'requests_link_requests'}
        cols={LinkRequestsCols}
        rows={rows}
        onRowSelect={setSelected}
        onRowDoubleClick={handleRowDoubleClick}
        loading={isLoading}
        height="fullHeight"
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: data?.totalCount || 0,
        }}
        className={styles.table}
      >
        <ToolsPanel
          className={styles.toolBar}
          leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
        >
          <ToolButton
            label={'Подробно'}
            startIcon={<ChevronRight />}
            onClick={handleDetails}
            disabled={selectedSolution.length !== 1}
            fast
          />
          <DeleteToolButton
            label={'Удалить'}
            startIcon={<DeleteOutline />}
            disabled={selectedSolution.length !== 1}
            fast
            description={`Вы действительно хотите удалить заявку?`}
            title={'Удаление заявки'}
            onConfirm={handleDelete}
          />
          <ToolButton
            label="Добавить"
            startIcon={<AddCircleOutline />}
            onClick={() => setOpenModal(true)}
            fast
          ></ToolButton>
        </ToolsPanel>
      </DataTable>
      <RequestSearchModal
        id={id}
        open={openModal}
        onClose={() => setOpenModal(false)}
        requestId={requestId}
        refetchTable={refethTable}
      />
    </div>
  );
};

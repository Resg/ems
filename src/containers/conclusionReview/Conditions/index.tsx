import React, { useCallback, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router';

import { ChevronRight, Refresh } from '@mui/icons-material';
import { Box } from '@mui/material';

import { DataTable } from '../../../components/CustomDataGrid';
import { RoundButton } from '../../../components/RoundButton';
import { ToolButton, ToolsPanel } from '../../../components/ToolsPanel';
import { useGetConclusionConditionsQuery } from '../../../services/api/conclusions';
import { RootState } from '../../../store';
import { ConclusionAdditional } from '../../../types/conclusions';
import { additionalsToTableAdapter } from '../../../utils/conclusions';

interface ConditionsProps {}

export const Conditions: React.FC<ConditionsProps> = () => {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const { id } = useParams();

  const { data, refetch, isLoading } = useGetConclusionConditionsQuery(
    { conclusionId: Number(id), page, size: perPage },
    { skip: !id }
  );

  const selectedArr = useMemo(
    () => Object.keys(selected).filter((key) => selected[Number(key)]),
    [selected]
  );

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const handleDetails = useCallback(() => {
    window
      .open(
        `/conclusions/additionals/${selectedArr[0]}/view`,
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  }, [selectedArr, openFormInNewTab]);

  const handleDoubleClick = useCallback(
    (condition: Partial<ConclusionAdditional>) => {
      if (condition.id) {
        window
          .open(
            `/conclusions/additionals/${condition.id}/view`,
            openFormInNewTab ? '_blank' : '_self'
          )
          ?.focus();
      }
    },
    [openFormInNewTab]
  );

  const { cols, rows } = useMemo(
    () => additionalsToTableAdapter(data?.data),
    [data]
  );

  return (
    <>
      <Box my={3}></Box>
      <DataTable
        formKey={'conslution_review_conditions'}
        rows={rows}
        cols={cols}
        onRowSelect={setSelected}
        onRowDoubleClick={handleDoubleClick}
        loading={isLoading}
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: data?.totalCount || 0,
        }}
      >
        <ToolsPanel
          leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
        >
          <ToolButton
            label={'Подробно'}
            startIcon={<ChevronRight />}
            onClick={handleDetails}
            disabled={selectedArr.length !== 1}
            fast
          />
        </ToolsPanel>
      </DataTable>
    </>
  );
};

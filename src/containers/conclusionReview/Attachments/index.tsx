import React, { useCallback, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router';

import { ChevronRight, Refresh } from '@mui/icons-material';
import { Box } from '@mui/material';

import { DataTable } from '../../../components/CustomDataGrid';
import { RoundButton } from '../../../components/RoundButton';
import { ToolButton, ToolsPanel } from '../../../components/ToolsPanel';
import { useGetConclusionAppendicesQuery } from '../../../services/api/conclusions';
import { RootState } from '../../../store';
import { ConclusionAppendice } from '../../../types/conclusions';
import { appendicesToTableAdapter } from '../../../utils/conclusions';

interface AttachmentsProps {}

export const Attachments: React.FC<AttachmentsProps> = () => {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const { id } = useParams();
  const { data, refetch, isLoading } = useGetConclusionAppendicesQuery(
    { id: Number(id), page, size: perPage },
    { skip: !id }
  );

  const selectedArr = useMemo(
    () => Object.keys(selected).filter((key) => selected[Number(key)]),
    [selected]
  );
  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );
  const handleDetails = useCallback(() => {
    window
      .open(
        `/conclusions/${id}/appendices/view/${selectedArr[0]}`,
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  }, [selectedArr, openFormInNewTab]);

  const handleDoubleClick = useCallback(
    (attachment: Partial<ConclusionAppendice>) => {
      if (attachment.id && id) {
        window
          .open(
            `/conclusions/${id}/appendices/view/${attachment.id}`,
            openFormInNewTab ? '_blank' : '_self'
          )
          ?.focus();
      }
    },
    [id, openFormInNewTab]
  );

  const { cols, rows } = useMemo(
    () => appendicesToTableAdapter(data?.data),
    [data]
  );

  return (
    <>
      <Box my={3}></Box>
      <DataTable
        formKey={'attachments'}
        rows={rows}
        cols={cols}
        onRowSelect={setSelected}
        onRowDoubleClick={handleDoubleClick}
        loading={isLoading}
        pagerProps={{
          page: page,
          setPage: setPage,
          perPage: perPage,
          setPerPage: setPerPage,
          total: data?.totalCount || 0,
        }}
      >
        <ToolsPanel
          leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
        >
          <ToolButton
            label={'Подробно'}
            startIcon={<ChevronRight />}
            onClick={handleDetails}
            disabled={selectedArr.length !== 1}
            fast
          />
        </ToolsPanel>
      </DataTable>
    </>
  );
};

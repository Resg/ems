import React from 'react';
import { Form, Formik } from 'formik';
import { useParams } from 'react-router';

import { Grid } from '@mui/material';

import Input from '../../../components/Input';
import {
  ConclusionReviewFields,
  ConclusionReviewLabels,
} from '../../../constants/conclusionsReview';
import {
  useGetConclusionDataQuery,
  useGetConclusionFilesQuery,
} from '../../../services/api/conclusions';
import { filesToTableAdapter } from '../../../utils/conclusions';
import { FilesTable } from '../../documents/FilesTable';

import styles from './styles.module.scss';

interface CommonProps {}

export const Common: React.FC<CommonProps> = () => {
  const { id } = useParams();

  const { data } = useGetConclusionDataQuery({ id: Number(id) }, { skip: !id });

  const { data: files, refetch } = useGetConclusionFilesQuery(
    { id: id || '' },
    { skip: !id }
  );

  const refetchTable = () => refetch();

  const { cols, rows } = filesToTableAdapter(files);

  return (
    <Formik
      initialValues={{
        number: data?.number || '',
        date: data?.date || '',
        dateValidity: data?.dateValidity || '',
        status: data?.status || '',
        type: data?.type || '',
        sort: data?.sort || '',
      }}
      enableReinitialize
      onSubmit={() => {}}
    >
      {({ values }) => {
        return (
          <Form>
            <Grid container spacing={2} className={styles.form}>
              <Grid item xs={12}>
                <Input
                  name={ConclusionReviewFields.NUMBER}
                  label={ConclusionReviewLabels[ConclusionReviewFields.NUMBER]}
                  value={values.number}
                  disabled
                />
              </Grid>
              <div className={styles.date}>
                <Grid item xs={6} className={styles.dateInput}>
                  <Input
                    name={ConclusionReviewFields.DATE}
                    label={ConclusionReviewLabels[ConclusionReviewFields.DATE]}
                    value={values.date}
                    disabled
                  />
                </Grid>
                <Grid item xs={6}>
                  <Input
                    name={ConclusionReviewFields.END_DATE}
                    label={
                      ConclusionReviewLabels[ConclusionReviewFields.END_DATE]
                    }
                    value={values.dateValidity}
                    disabled
                  />
                </Grid>
              </div>
              <Grid item xs={12}>
                <Input
                  name={ConclusionReviewFields.STATUS}
                  label={ConclusionReviewLabels[ConclusionReviewFields.STATUS]}
                  value={values.status}
                  disabled
                />
              </Grid>
              <Grid item xs={12}>
                <Input
                  name={ConclusionReviewFields.TYPE}
                  label={ConclusionReviewLabels[ConclusionReviewFields.TYPE]}
                  value={values.type}
                  disabled
                />
              </Grid>
              <Grid item xs={12}>
                <Input
                  name={ConclusionReviewFields.SORT}
                  label={ConclusionReviewLabels[ConclusionReviewFields.SORT]}
                  value={values.sort}
                  disabled
                />
              </Grid>
              <Grid item xs={12}>
                <FilesTable
                  label="Файлы"
                  columns={cols}
                  rows={rows}
                  refetchTable={refetchTable}
                  isHidden={true}
                />
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

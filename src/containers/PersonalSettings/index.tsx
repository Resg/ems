import React, { useMemo, useState } from 'react';
import { Form, Formik } from 'formik';

import { Edit, EditOff, Save } from '@mui/icons-material';
import { Grid, Stack } from '@mui/material';

import { CheckboxSettingsInput } from '../../components/CheckboxSettingsInput';
import { RoundButton } from '../../components/RoundButton';
import {
  PersonalSettingsDefaults,
  PersonalSettingsFormFields,
  PersonalSettingsFormLabels,
} from '../../constants/personalSettings';
import {
  useGetPersonalSettingsQuery,
  useUpdatePersonalSettingsMutation,
} from '../../services/api/user';

export const Settings: React.FC = () => {
  const [editMode, setEditMode] = useState(false);

  const [updatePersonalSettings] = useUpdatePersonalSettingsMutation();
  const { data: personalData, refetch } = useGetPersonalSettingsQuery({});

  const initialValues = useMemo(() => {
    return (
      {
        openFormInNewTab: personalData?.openFormInNewTab,
        autoCountersUpdate: personalData?.autoCountersUpdate,
      } || PersonalSettingsDefaults
    );
  }, [personalData]);

  const handleSubmit = async (values: any) => {
    const updateData = {
      openFormInNewTab: values.openFormInNewTab,
      autoCountersUpdate: values.autoCountersUpdate,
    };
    await updatePersonalSettings({
      data: updateData,
    });
    setEditMode(false);
    refetch();
  };

  return (
    <Formik
      initialValues={initialValues}
      enableReinitialize
      onSubmit={handleSubmit}
    >
      {({ values, handleReset, handleSubmit }) => {
        const handleEditOff = () => {
          handleReset();
          setEditMode(false);
        };
        return (
          <Form>
            <Stack my={3} spacing={2} direction="row">
              {!editMode && (
                <RoundButton
                  icon={<Edit />}
                  onClick={() => setEditMode(true)}
                />
              )}
              {editMode && (
                <>
                  <RoundButton icon={<Save />} onClick={() => handleSubmit()} />
                  <RoundButton icon={<EditOff />} onClick={handleEditOff} />
                </>
              )}
            </Stack>
            <Grid container rowSpacing={2} columnSpacing={12}>
              <Grid item xs={12}>
                <CheckboxSettingsInput
                  name={PersonalSettingsFormFields.IS_OPEN_FORM_IN_NEW_TAB}
                  label={
                    PersonalSettingsFormLabels[
                      PersonalSettingsFormFields.IS_OPEN_FORM_IN_NEW_TAB
                    ]
                  }
                  disabled={!editMode}
                  title={'Открывать форму в новой вкладке браузера'}
                  text={
                    'Открытие формы записи в новой вкладке браузера: по двойному клику по строке, кнопке "Подробно" (и т.п.)'
                  }
                />
              </Grid>
              <Grid item xs={12}>
                <CheckboxSettingsInput
                  name={PersonalSettingsFormFields.IS_AUTO_COUNTERS_UPDATE}
                  label={
                    PersonalSettingsFormLabels[
                      PersonalSettingsFormFields.IS_AUTO_COUNTERS_UPDATE
                    ]
                  }
                  disabled={!editMode}
                  title={'Автоматическое обновление счетчиков в главном меню'}
                  text={
                    'Периодическое обновление счётчиков главного меню (поступившие документы, поступившие заявки и т.д.)'
                  }
                />
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

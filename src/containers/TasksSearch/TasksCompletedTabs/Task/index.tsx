import React, { useMemo } from 'react';
import { useFormikContext } from 'formik';

import { Grid } from '@mui/material';

import { AutoCompleteInput } from '../../../../components/AutoCompleteInput';
import { ClerkInput } from '../../../../components/ClerkInput';
import { DateRange } from '../../../../components/DateRange';
import { RadioButtonGroup } from '../../../../components/RadioButtonGroup';
import {
  TasksCompletedSearchFields,
  TasksCompletedSearchLabels,
} from '../../../../constants/tasksSearch';
import { useGetTaskTypesQuery } from '../../../../services/api/dictionaries';

import styles from '../../../Registry/AddToRegistryPopup/styles.module.scss';

interface TaskProps {}

export const Task: React.FC<TaskProps> = () => {
  const {
    handleChange,
    setFieldValue,
    values = {} as any,
  } = useFormikContext();

  const { data: types } = useGetTaskTypesQuery({
    rootObjectTypeCode: 'DOCS',
  });

  const taskTypes = useMemo(
    () =>
      types?.data.map((type) => {
        return { value: type.id, label: type.name };
      }) || [],
    [types]
  );

  return (
    <Grid container rowSpacing={2} columnSpacing={12}>
      <Grid item xs={12}>
        <RadioButtonGroup
          name={TasksCompletedSearchFields.ISONLYMAIN}
          options={[
            { value: false, label: 'Все' },
            { value: true, label: 'Только по ЭРК' },
          ]}
          className={styles.radioGroup}
          defaultValue={false}
          row
        />
      </Grid>
      <Grid item xs={6}>
        <AutoCompleteInput
          name={TasksCompletedSearchFields.TYPE}
          label={TasksCompletedSearchLabels[TasksCompletedSearchFields.TYPE]}
          options={taskTypes}
        />
      </Grid>
      <Grid item xs={6}>
        <ClerkInput
          name={TasksCompletedSearchFields.AUTHOR}
          label={TasksCompletedSearchLabels[TasksCompletedSearchFields.AUTHOR]}
          multiple
        />
      </Grid>
      <Grid item xs={3}>
        <DateRange
          nameFrom={TasksCompletedSearchFields.DATECREATIONFROM}
          nameBefore={TasksCompletedSearchFields.DATECREATIONTO}
          labelFrom={'Дата создания'}
        />
      </Grid>
      <Grid item xs={3}>
        <DateRange
          nameFrom={TasksCompletedSearchFields.DATEPREPAREFROM}
          nameBefore={TasksCompletedSearchFields.DATEPREPARETO}
          labelFrom={'Дата поступления'}
        />
      </Grid>
      <Grid item xs={3}>
        <DateRange
          nameFrom={TasksCompletedSearchFields.DATEPLANFROM}
          nameBefore={TasksCompletedSearchFields.DATEPLANTO}
          labelFrom={'Плановая дата выполнения'}
        />
      </Grid>
      <Grid item xs={3}>
        <DateRange
          nameFrom={TasksCompletedSearchFields.DATECOMPLETEFROM}
          nameBefore={TasksCompletedSearchFields.DATECOMPLETETO}
          labelFrom={'Дата выполнения'}
        />
      </Grid>
    </Grid>
  );
};

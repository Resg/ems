import React from 'react';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { Grid } from '@mui/material';

import Input from '../../../../components/Input';
import {
  TasksCompletedSearchFields,
  TasksCompletedSearchLabels,
} from '../../../../constants/tasksSearch';

interface ResolutionProps {}

export const Resolution: React.FC<ResolutionProps> = () => {
  const { handleChange, values = {} as any } = useFormikContext();

  return (
    <Grid container rowSpacing={2} columnSpacing={12}>
      <Grid item xs={12}>
        <Input
          rows={3}
          multiline
          name={TasksCompletedSearchFields.RESOLUTION_CONTENT}
          label={
            TasksCompletedSearchLabels[
              TasksCompletedSearchFields.RESOLUTION_CONTENT
            ]
          }
          onChange={handleChange}
          value={
            get(values, TasksCompletedSearchFields.RESOLUTION_CONTENT) || ''
          }
        />
      </Grid>
    </Grid>
  );
};

import React from 'react';

import { Grid } from '@mui/material';

import { ListInput } from '../../../../components/ListInput';
import { TasksCompletedSearchFields } from '../../../../constants/tasksSearch';

interface BarcodeProps {}

export const Barcode: React.FC<BarcodeProps> = () => {
  return (
    <Grid container direction="column" spacing={1} sx={{ mb: 2 }}>
      <Grid item>Список штрих-кодов</Grid>
      <Grid item>
        <ListInput
          name={TasksCompletedSearchFields.DOCUMENT_IDS}
          listElementType="integer"
          multiline
          rows={15}
        />
      </Grid>
    </Grid>
  );
};

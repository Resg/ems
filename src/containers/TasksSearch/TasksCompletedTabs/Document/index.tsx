import React, { useCallback, useMemo, useState } from 'react';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { PersonPin } from '@mui/icons-material';
import { Grid } from '@mui/material';

import { AutoCompleteInput } from '../../../../components/AutoCompleteInput';
import { ClerkInput } from '../../../../components/ClerkInput';
import { ContractorSearchForm } from '../../../../components/ContractorSearchForm';
import { DateRange } from '../../../../components/DateRange';
import { DebounceAutoComplete } from '../../../../components/DebounceAutoComplete';
import Input from '../../../../components/Input';
import { RoundButton } from '../../../../components/RoundButton';
import {
  TasksCompletedSearchFields,
  TasksCompletedSearchLabels,
} from '../../../../constants/tasksSearch';
import {
  useGetAsyncContractorsListMutation,
  useGetCounterpartiesAsyncMutation,
  useGetDocumentRubricsOptionsQuery,
  useGetDocumentTypesQuery,
} from '../../../../services/api/dictionaries';
import { Counterparty } from '../../../../types/dictionaries';

import styles from '../../../../pages/CommonRequest/styles.module.scss';

interface DocumentProps {}

export const Document: React.FC<DocumentProps> = () => {
  const {
    handleChange,
    values = {} as any,
    setFieldValue,
  } = useFormikContext();

  const documentTypeId = get(values, TasksCompletedSearchFields.DOCUMENT_TYPE);

  const { data: rubrics = [] } = useGetDocumentRubricsOptionsQuery({
    documentTypeId,
  });

  const { data: types } = useGetDocumentTypesQuery({});
  const [getCounterparties] = useGetCounterpartiesAsyncMutation();
  const [getContractorsList] = useGetAsyncContractorsListMutation();
  const [openModal, setOpenModal] = useState(false);

  const taskTypes = useMemo(
    () =>
      types?.map((type) => {
        return { value: type.value, label: type.label };
      }) || [],
    [types]
  );

  const getOptions = useCallback(async (value: string) => {
    if (value) {
      const response = await getContractorsList({ name: value });
      if ('data' in response) {
        return response.data.data.map((option: Record<string, any>) => {
          return {
            label: option.name,
            value: option.id,
          };
        });
      }
      return [];
    }
    return [];
  }, []);

  const getValueOptions = useCallback(
    async (ids: number[]) => {
      if (ids && ids.length) {
        const counterparties = await getCounterparties({ ids });
        if ('data' in counterparties) {
          return (
            counterparties?.data.data.map((option: Counterparty) => {
              return {
                label: option.name,
                value: option.id,
              };
            }) || []
          );
        } else {
          return [];
        }
      }
      return [];
    },
    [getCounterparties]
  );

  const handleAdd = (value: number[]) => {
    if (values[TasksCompletedSearchFields.CONTRACTORS]) {
      setFieldValue(TasksCompletedSearchFields.CONTRACTORS, [
        ...(values[TasksCompletedSearchFields.CONTRACTORS] as number[]),
        ...value,
      ]);
    } else {
      setFieldValue(TasksCompletedSearchFields.CONTRACTORS, value);
    }
  };

  return (
    <Grid container rowSpacing={2} columnSpacing={12}>
      <Grid item xs={12}>
        <AutoCompleteInput
          name={TasksCompletedSearchFields.DOCUMENT_TYPE}
          label={
            TasksCompletedSearchLabels[TasksCompletedSearchFields.DOCUMENT_TYPE]
          }
          options={taskTypes}
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          name={TasksCompletedSearchFields.NUMBER}
          label={TasksCompletedSearchLabels[TasksCompletedSearchFields.NUMBER]}
          onChange={handleChange}
          value={get(values, TasksCompletedSearchFields.NUMBER) || ''}
        />
      </Grid>
      <Grid item xs={6}>
        <Input
          name={TasksCompletedSearchFields.DOCUMENT_INTERNAL_NUMBER}
          label={
            TasksCompletedSearchLabels[
              TasksCompletedSearchFields.DOCUMENT_INTERNAL_NUMBER
            ]
          }
          onChange={handleChange}
          value={
            get(values, TasksCompletedSearchFields.DOCUMENT_INTERNAL_NUMBER) ||
            ''
          }
        />
      </Grid>
      <Grid item xs={6}>
        <DateRange
          nameFrom={TasksCompletedSearchFields.DOCUMENT_DATE_FROM}
          nameBefore={TasksCompletedSearchFields.DOCUMENT_DATE_TO}
          labelFrom={'Дата'}
        />
      </Grid>
      <Grid item xs={6}></Grid>
      <Grid item xs={12}>
        <AutoCompleteInput
          name={TasksCompletedSearchFields.RUBRICS}
          label={TasksCompletedSearchLabels[TasksCompletedSearchFields.RUBRICS]}
          multiple
          options={rubrics}
        />
      </Grid>
      <Grid item xs={12} className={styles.container}>
        <DebounceAutoComplete
          name={TasksCompletedSearchFields.CONTRACTORS}
          label={
            TasksCompletedSearchLabels[TasksCompletedSearchFields.CONTRACTORS]
          }
          getOptions={getOptions}
          getValueOptions={getValueOptions}
          multiple
        />
        <RoundButton icon={<PersonPin />} onClick={() => setOpenModal(true)} />

        <ContractorSearchForm
          openClerkModal={openModal}
          onClose={() => setOpenModal(false)}
          addDoc={handleAdd}
        />
      </Grid>
      <Grid item xs={12}>
        <ClerkInput
          name={TasksCompletedSearchFields.DOCUMENTS_AUTHORS}
          label={
            TasksCompletedSearchLabels[
              TasksCompletedSearchFields.DOCUMENTS_AUTHORS
            ]
          }
          multiple
        />
      </Grid>
      <Grid item xs={12}>
        <Input
          rows={2}
          multiline
          name={TasksCompletedSearchFields.DOCUMENTS_DESCRIPTION}
          label={
            TasksCompletedSearchLabels[
              TasksCompletedSearchFields.DOCUMENTS_DESCRIPTION
            ]
          }
          onChange={handleChange}
          value={
            get(values, TasksCompletedSearchFields.DOCUMENTS_DESCRIPTION) || ''
          }
        />
      </Grid>
    </Grid>
  );
};

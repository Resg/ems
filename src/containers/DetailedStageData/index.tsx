import React, { useCallback, useMemo, useState } from 'react';
import { Form, Formik } from 'formik';
import { object, string } from 'yup';

import { Edit, EditOff, Save } from '@mui/icons-material';
import { Grid, Stack } from '@mui/material';

import { AutoCompleteInput } from '../../components/AutoCompleteInput';
import Input from '../../components/Input';
import { RoundButton } from '../../components/RoundButton';
import {
  DefaultDetailedStageData,
  DetailedStageDataFields,
  DetailedStageDataLabels,
} from '../../constants/detailedStageData';
import {
  useGetDetailedDataOnTheTaskSubtaskQuery,
  useGetTaskDataQuery,
  useUpdateTaskSubtaskMutation,
} from '../../services/api/task';
import { taskSubtaskUpdateAdapter } from '../../utils/tasks';

import styles from './styles.module.scss';

export interface DetailedStageDataProps {
  id: number;
}

export const DetailedStageData: React.FC<DetailedStageDataProps> = ({ id }) => {
  const [editMode, setEditMode] = useState<boolean>(false);
  const [saveTask] = useUpdateTaskSubtaskMutation();

  const { data: formData } = useGetDetailedDataOnTheTaskSubtaskQuery({ id });
  const { data: taskChildren } = useGetTaskDataQuery(
    {
      parentId: formData?.parentId || 0,
      excludedTaskId: formData?.id,
    },
    {
      skip: !formData?.parentId,
    }
  );

  const initialValues = useMemo(() => {
    if (formData) {
      return {
        name: formData.title,
        executeAfter: formData.previousId,
        comments: formData.comments,
      };
    }
    return DefaultDetailedStageData;
  }, [formData]);

  const handleSubmit = useCallback(
    async (values = {} as any) => {
      if (!formData) {
        return;
      }
      const updateData = taskSubtaskUpdateAdapter(formData, values);
      await saveTask({ id, data: updateData });
      setEditMode(false);
    },
    [formData]
  );

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validation}
      enableReinitialize
      onSubmit={handleSubmit}
    >
      {({
        values,
        handleSubmit,
        handleReset,
        handleChange,
        setFieldValue,
        errors,
      }) => {
        return (
          <>
            <Stack className={styles.stack}>
              {editMode ? (
                <>
                  <RoundButton onClick={() => handleSubmit()} icon={<Save />} />
                  <RoundButton
                    onClick={() => {
                      handleReset();
                      setEditMode(false);
                    }}
                    icon={<EditOff />}
                  />
                </>
              ) : (
                <RoundButton
                  disabled={
                    !formData?.canEditComments ||
                    !formData?.canEditTaskRelations
                  }
                  onClick={() => setEditMode(true)}
                  icon={<Edit />}
                />
              )}
            </Stack>
            <Form>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <Input
                    name={DetailedStageDataFields.NAME}
                    label={
                      DetailedStageDataLabels[DetailedStageDataFields.NAME]
                    }
                    value={values[DetailedStageDataFields.NAME] || ''}
                    onChange={handleChange}
                    error={Boolean(errors[DetailedStageDataFields.NAME])}
                    helperText={errors[DetailedStageDataFields.NAME]}
                    disabled={!editMode}
                    notEditableField={!formData?.canEditComments}
                    required
                  />
                </Grid>
                <Grid item xs={12}>
                  <AutoCompleteInput
                    name={DetailedStageDataFields.EXECUTE_AFTER}
                    label={
                      DetailedStageDataLabels[
                        DetailedStageDataFields.EXECUTE_AFTER
                      ]
                    }
                    options={
                      taskChildren?.data?.map((stage) => {
                        if (editMode) {
                          return {
                            value: stage.id,
                            label: stage.title,
                          };
                        }
                        return {
                          value: stage.id,
                          label: stage.title.split(' :')[0],
                        };
                      }) || []
                    }
                    disabled={!editMode || !formData?.canEditTaskRelations}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Input
                    name={DetailedStageDataFields.COMMENTS}
                    label={
                      DetailedStageDataLabels[DetailedStageDataFields.COMMENTS]
                    }
                    value={values[DetailedStageDataFields.COMMENTS] || ''}
                    onChange={handleChange}
                    multiline
                    disabled={!editMode}
                    notEditableField={!formData?.canEditComments}
                  />
                </Grid>
              </Grid>
            </Form>
          </>
        );
      }}
    </Formik>
  );
};

const validation = object({
  [DetailedStageDataFields.NAME]: string().required('Обязательное поле'),
});

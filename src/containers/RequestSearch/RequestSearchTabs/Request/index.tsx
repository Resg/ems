import React, { useCallback, useMemo } from 'react';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { Grid } from '@mui/material';

import { AutoCompleteInput } from '../../../../components/AutoCompleteInput';
import { CheckboxInput } from '../../../../components/CheckboxInput';
import { ClerkInput } from '../../../../components/ClerkInput';
import { DateRange } from '../../../../components/DateRange';
import { DebounceAutoComplete } from '../../../../components/DebounceAutoComplete';
import { FieldWrapper } from '../../../../components/FieldWrapper';
import Input from '../../../../components/Input';
import {
  RequestSearchFields,
  RequestSearchLabels,
} from '../../../../constants/requestSearch';
import {
  useGetAsyncContractorsListMutation,
  useGetCounterpartiesAsyncMutation,
  useGetCountriesQuery,
  useGetRadioServiceQuery,
  useGetRegionsQuery,
  useGetRequestStagesQuery,
  useGetRequestTypesQuery,
  useGetServiceMRFCQuery,
  useGetTechnologiesQuery,
} from '../../../../services/api/dictionaries';
import { Counterparty } from '../../../../types/dictionaries';

interface RequestProps {}

export const Request: React.FC<RequestProps> = () => {
  const { handleChange, values = {} as any } = useFormikContext();

  const [getCounterparties] = useGetCounterpartiesAsyncMutation();
  const [getContractorsList] = useGetAsyncContractorsListMutation();
  const { data: stages } = useGetRequestStagesQuery();
  const { data: radioServices } = useGetRadioServiceQuery({});
  const { data: technologies } = useGetTechnologiesQuery({});
  const { data: regions } = useGetRegionsQuery();
  const { data: requestTypes } = useGetRequestTypesQuery();
  const { data: serviceMRFC } = useGetServiceMRFCQuery({});
  const { data: countries = [] } = useGetCountriesQuery();
  const serviceMRFCIds = useMemo(() => values.filters.serviceMRFCIds, [values]);

  const getOptions = useCallback(async (value: string) => {
    if (value) {
      const response = await getContractorsList({ name: value });
      if ('data' in response) {
        return response.data.data.map((option: Record<string, any>) => {
          return {
            label: option.name,
            value: option.id,
          };
        });
      }
      return [];
    }
    return [];
  }, []);

  const getValueOptions = useCallback(
    async (ids: number[]) => {
      if (ids && ids.length) {
        const counterparties = await getCounterparties({ ids });
        if ('data' in counterparties) {
          return (
            counterparties?.data.data.map((option: Counterparty) => {
              return {
                label: option.name,
                value: option.id,
              };
            }) || []
          );
        } else {
          return [];
        }
      }
      return [];
    },
    [getCounterparties]
  );

  const stagesName = useMemo(() => {
    return (
      stages?.data.map((stage) => ({
        label: stage.name,
        value: stage.id,
      })) || []
    );
  }, [stages]);

  const technologiesName = useMemo(() => {
    return (
      technologies?.data.map((technology) => ({
        label: technology.name,
        value: technology.id,
      })) || []
    );
  }, [technologies]);

  const radioServicesName = useMemo(() => {
    return (
      radioServices?.map((service) => ({
        label: service.name,
        value: service.id,
      })) || []
    );
  }, [radioServices]);

  const regionsName = useMemo(() => {
    return (
      regions?.map((region) => ({
        label: region.name,
        value: region.aoguid,
      })) || []
    );
  }, [regions]);

  const requestTypesName = useMemo(() => {
    return (
      requestTypes?.map((type) => ({
        label: type.name,
        value: type.id,
      })) || []
    );
  }, [requestTypes]);

  const serviceMRFCName = useMemo(() => {
    return (
      serviceMRFC?.map((service) => ({
        label: service.name,
        value: service.id,
      })) || []
    );
  }, [serviceMRFC]);

  return (
    <Grid container spacing={2}>
      <Grid item xs={6}>
        <Input
          name={RequestSearchFields.NUMBER}
          label={RequestSearchLabels[RequestSearchFields.NUMBER]}
          value={get(values, RequestSearchFields.NUMBER) || ''}
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={6}>
        <DateRange
          nameFrom={RequestSearchFields.DATE_FROM}
          nameBefore={RequestSearchFields.DATE_TO}
          labelFrom={'Дата'}
        />
      </Grid>
      <Grid item xs={12}>
        <DebounceAutoComplete
          name={RequestSearchFields.CONTRACTOR}
          label={RequestSearchLabels[RequestSearchFields.CONTRACTOR]}
          getOptions={getOptions}
          getValueOptions={getValueOptions}
          multiple
        />
      </Grid>
      <Grid item xs={3}>
        <DebounceAutoComplete
          name={RequestSearchFields.CONCLUSION_CONTRACTOR}
          label={RequestSearchLabels[RequestSearchFields.CONCLUSION_CONTRACTOR]}
          getOptions={getOptions}
          getValueOptions={getValueOptions}
        />
      </Grid>
      <Grid item xs={3}>
        <ClerkInput
          name={RequestSearchFields.MANAGER}
          label={RequestSearchLabels[RequestSearchFields.MANAGER]}
        />
      </Grid>
      <Grid item xs={6}>
        <AutoCompleteInput
          name={RequestSearchFields.STAGE_NAME}
          label={RequestSearchLabels[RequestSearchFields.STAGE_NAME]}
          options={stagesName}
          multiple
        />
      </Grid>
      <Grid item xs={3}>
        <DateRange
          nameFrom={RequestSearchFields.DATE_STAGE_START_FROM}
          nameBefore={RequestSearchFields.DATE_STAGE_START_TO}
          labelFrom={'Дата начала этапа'}
        />
      </Grid>
      <Grid item xs={3}>
        <DateRange
          nameFrom={RequestSearchFields.DATE_STAGE_END_FROM}
          nameBefore={RequestSearchFields.DATE_STAGE_END_TO}
          labelFrom={'Дата окончания этапа'}
        />
      </Grid>
      <Grid item xs={3}>
        <AutoCompleteInput
          name={RequestSearchFields.SERVICE}
          label={RequestSearchLabels[RequestSearchFields.SERVICE]}
          options={radioServicesName}
          multiple
        />
      </Grid>
      <Grid item xs={3}>
        <AutoCompleteInput
          name={RequestSearchFields.TECHNOLOGY}
          label={RequestSearchLabels[RequestSearchFields.TECHNOLOGY]}
          options={technologiesName}
          multiple
        />
      </Grid>
      <Grid item xs={3}>
        <AutoCompleteInput
          name={RequestSearchFields.REGION}
          label={RequestSearchLabels[RequestSearchFields.REGION]}
          options={regionsName}
          multiple
        />
      </Grid>
      <Grid item xs={3}>
        <AutoCompleteInput
          name={RequestSearchFields.REQUEST_TYPE}
          label={RequestSearchLabels[RequestSearchFields.REQUEST_TYPE]}
          options={requestTypesName}
          multiple
        />
      </Grid>
      <Grid item xs={3}>
        <AutoCompleteInput
          name={RequestSearchFields.SERVICE_MRFC}
          label={RequestSearchLabels[RequestSearchFields.SERVICE_MRFC]}
          options={serviceMRFCName}
          multiple
        />
      </Grid>
      <Grid item xs={3}>
        <AutoCompleteInput
          name={RequestSearchFields.COUNTRY}
          label={RequestSearchLabels[RequestSearchFields.COUNTRY]}
          options={countries}
          multiple
        />
      </Grid>
      <Grid item xs={3}>
        <Input
          name={RequestSearchFields.CALL_SIGN}
          label={RequestSearchLabels[RequestSearchFields.CALL_SIGN]}
          value={get(values, RequestSearchFields.CALL_SIGN) || ''}
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={3}>
        <CheckboxInput
          name={RequestSearchFields.IS_JOINT_USE}
          label={RequestSearchLabels[RequestSearchFields.IS_JOINT_USE]}
        />
      </Grid>
      <Grid item xs={6}>
        <FieldWrapper
          name={RequestSearchFields.PERMISSION_NUMBER}
          disabled={
            !serviceMRFCIds?.some((ai: number) => [15, 19, 26].includes(ai))
          }
        >
          <Input
            name={RequestSearchFields.PERMISSION_NUMBER}
            label={RequestSearchLabels[RequestSearchFields.PERMISSION_NUMBER]}
            value={get(values, RequestSearchFields.PERMISSION_NUMBER) || ''}
            onChange={handleChange}
            disabled={
              !serviceMRFCIds?.some((ai: number) => [15, 19, 26].includes(ai))
            }
          />
        </FieldWrapper>
      </Grid>
    </Grid>
  );
};

import { Grid } from '@mui/material';

import { ClerkInput } from '../../../../components/ClerkInput';
import { DateRange } from '../../../../components/DateRange';
import { ListInput } from '../../../../components/ListInput';
import {
  RequestSearchFields,
  RequestSearchLabels,
} from '../../../../constants/requestSearch';

export const Finances = () => {
  return (
    <Grid container spacing={2} sx={{ mb: 2 }}>
      <Grid item xs={3}>
        <ListInput
          name={RequestSearchFields.ORDER_NUMBERS}
          label={RequestSearchLabels[RequestSearchFields.ORDER_NUMBERS]}
        />
      </Grid>
      <Grid item xs={3}>
        <DateRange
          nameFrom={RequestSearchFields.ORDER_DATE_FROM}
          nameBefore={RequestSearchFields.ORDER_DATE_TO}
          labelFrom={RequestSearchLabels[RequestSearchFields.ORDER_DATE_FROM]}
          labelBefore={RequestSearchLabels[RequestSearchFields.ORDER_DATE_TO]}
        />
      </Grid>
      <Grid item xs={6}>
        <ClerkInput
          name={RequestSearchFields.ORDER_PERFORMER_ID}
          label={RequestSearchLabels[RequestSearchFields.ORDER_PERFORMER_ID]}
        />
      </Grid>
      <Grid item xs={3}>
        <ListInput
          name={RequestSearchFields.CONTRACT_NUMBERS}
          label={RequestSearchLabels[RequestSearchFields.CONTRACT_NUMBERS]}
        />
      </Grid>
      <Grid item xs={3}>
        <DateRange
          nameFrom={RequestSearchFields.CONTRACT_DATE_FROM}
          nameBefore={RequestSearchFields.CONTRACT_DATE_TO}
          labelFrom={
            RequestSearchLabels[RequestSearchFields.CONTRACT_DATE_FROM]
          }
          labelBefore={
            RequestSearchLabels[RequestSearchFields.CONTRACT_DATE_TO]
          }
        />
      </Grid>
      <Grid item xs={6}>
        <ClerkInput
          name={RequestSearchFields.CONTRACT_PERFORMER_ID}
          label={RequestSearchLabels[RequestSearchFields.CONTRACT_PERFORMER_ID]}
        />
      </Grid>
      <Grid item xs={3}>
        <ListInput
          name={RequestSearchFields.ACCOUNT_NUMBERS}
          label={RequestSearchLabels[RequestSearchFields.ACCOUNT_NUMBERS]}
        />
      </Grid>
      <Grid item xs={3}>
        <DateRange
          nameFrom={RequestSearchFields.ACCOUNT_DATE_FROM}
          nameBefore={RequestSearchFields.ACCOUNT_DATE_TO}
          labelFrom={RequestSearchLabels[RequestSearchFields.ACCOUNT_DATE_FROM]}
          labelBefore={RequestSearchLabels[RequestSearchFields.ACCOUNT_DATE_TO]}
        />
      </Grid>
      <Grid item xs={6}>
        <ClerkInput
          name={RequestSearchFields.ACCOUNT_PERFORMER_ID}
          label={RequestSearchLabels[RequestSearchFields.ACCOUNT_PERFORMER_ID]}
        />
      </Grid>
    </Grid>
  );
};

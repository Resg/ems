import React from 'react';
import { FieldArray, useFormikContext } from 'formik';
import { get } from 'lodash';

import { AddBox, Delete } from '@mui/icons-material';
import { Button, Grid } from '@mui/material';

import { AutoCompleteInput } from '../../../../components/AutoCompleteInput';
import Input from '../../../../components/Input';
import {
  DefaultFrequencyBand,
  RequestSearchFields,
  RequestSearchLabels,
} from '../../../../constants/requestSearch';
import {
  useGetConclusionStatesQuery,
  useGetFrequencyMeasuresQuery,
} from '../../../../services/api/dictionaries';

import styles from './styles.module.scss';

interface FrequencyBandProps {}

export const FrequencyBand: React.FC<FrequencyBandProps> = () => {
  const {
    handleChange,
    values = {} as any,
    setFieldValue,
  } = useFormikContext();
  const { data: frequencyStatesOptions = [] } = useGetConclusionStatesQuery({});
  const { data: measureOptions = [] } = useGetFrequencyMeasuresQuery();

  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <AutoCompleteInput
            name={RequestSearchFields.FREQUENCY_STATES}
            label={RequestSearchLabels[RequestSearchFields.FREQUENCY_STATES]}
            options={frequencyStatesOptions}
            multiple
          />
        </Grid>
        <FieldArray
          name={RequestSearchFields.FREQUENCY_BANDS}
          render={(arrayHelpers) => (
            <>
              {values?.filters?.frequencyBands?.map(
                (_band: any, index: number) => (
                  <>
                    <Grid item xs={12} className={styles.label}>
                      Передача
                    </Grid>
                    <Grid item xs={3}>
                      <Input
                        name={`${RequestSearchFields.FREQUENCY_BANDS}[${index}].${RequestSearchFields.FREQUENCY_TRANSMIT_FROM}`}
                        label={
                          RequestSearchLabels[
                            RequestSearchFields.FREQUENCY_TRANSMIT_FROM
                          ]
                        }
                        value={
                          get(
                            values,
                            `${RequestSearchFields.FREQUENCY_BANDS}[${index}].${RequestSearchFields.FREQUENCY_TRANSMIT_FROM}`
                          ) || ''
                        }
                        onChange={handleChange}
                      />
                    </Grid>
                    <Grid item xs={3}>
                      <AutoCompleteInput
                        name={`${RequestSearchFields.FREQUENCY_BANDS}[${index}].${RequestSearchFields.KOEFFICIENT_TO_STANDART_TRANSMIT_FROM}`}
                        label={
                          RequestSearchLabels[
                            RequestSearchFields
                              .KOEFFICIENT_TO_STANDART_TRANSMIT_FROM
                          ]
                        }
                        options={measureOptions}
                      />
                    </Grid>
                    <Grid item xs={3}>
                      <Input
                        name={`${RequestSearchFields.FREQUENCY_BANDS}[${index}].${RequestSearchFields.FREQUENCY_TRANSMIT_TO}`}
                        label={
                          RequestSearchLabels[
                            RequestSearchFields.FREQUENCY_TRANSMIT_TO
                          ]
                        }
                        value={
                          get(
                            values,
                            `${RequestSearchFields.FREQUENCY_BANDS}[${index}].${RequestSearchFields.FREQUENCY_TRANSMIT_TO}`
                          ) || ''
                        }
                        onChange={handleChange}
                      />
                    </Grid>
                    <Grid item xs={3}>
                      <AutoCompleteInput
                        name={`${RequestSearchFields.FREQUENCY_BANDS}[${index}].${RequestSearchFields.KOEFFICIENT_TO_STANDART_TRANSMIT_TO}`}
                        label={
                          RequestSearchLabels[
                            RequestSearchFields
                              .KOEFFICIENT_TO_STANDART_TRANSMIT_TO
                          ]
                        }
                        options={measureOptions}
                      />
                    </Grid>
                    <Grid item xs={12} className={styles.label}>
                      Прием
                    </Grid>
                    <Grid item xs={3}>
                      <Input
                        name={`${RequestSearchFields.FREQUENCY_BANDS}[${index}].${RequestSearchFields.FREQUENCY_RECEIVE_FROM}`}
                        label={
                          RequestSearchLabels[
                            RequestSearchFields.FREQUENCY_RECEIVE_FROM
                          ]
                        }
                        value={
                          get(
                            values,
                            `${RequestSearchFields.FREQUENCY_BANDS}[${index}].${RequestSearchFields.FREQUENCY_RECEIVE_FROM}`
                          ) || ''
                        }
                        onChange={handleChange}
                      />
                    </Grid>
                    <Grid item xs={3}>
                      <AutoCompleteInput
                        name={`${RequestSearchFields.FREQUENCY_BANDS}[${index}].${RequestSearchFields.KOEFFICIENT_TO_STANDART_RECEIVE_FROM}`}
                        label={
                          RequestSearchLabels[
                            RequestSearchFields
                              .KOEFFICIENT_TO_STANDART_RECEIVE_FROM
                          ]
                        }
                        options={measureOptions}
                      />
                    </Grid>
                    <Grid item xs={3}>
                      <Input
                        name={`${RequestSearchFields.FREQUENCY_BANDS}[${index}].${RequestSearchFields.FREQUENCY_RECEIVE_TO}`}
                        label={
                          RequestSearchLabels[
                            RequestSearchFields.FREQUENCY_RECEIVE_TO
                          ]
                        }
                        value={
                          get(
                            values,
                            `${RequestSearchFields.FREQUENCY_BANDS}[${index}].${RequestSearchFields.FREQUENCY_RECEIVE_TO}`
                          ) || ''
                        }
                        onChange={handleChange}
                      />
                    </Grid>
                    <Grid item xs={3}>
                      <AutoCompleteInput
                        name={`${RequestSearchFields.FREQUENCY_BANDS}[${index}].${RequestSearchFields.KOEFFICIENT_TO_STANDART_RECEIVE_TO}`}
                        label={
                          RequestSearchLabels[
                            RequestSearchFields
                              .KOEFFICIENT_TO_STANDART_RECEIVE_TO
                          ]
                        }
                        options={measureOptions}
                      />
                    </Grid>
                    <Grid item xs={12} display="flex" justifyContent="flex-end">
                      <Button
                        startIcon={<Delete />}
                        onClick={() => arrayHelpers.remove(index)}
                      >
                        Удалить
                      </Button>
                    </Grid>
                    <Grid item xs={12} className={styles.horContainer}>
                      <div className={styles.hor} />
                    </Grid>
                  </>
                )
              )}
              <Grid item xs={12} display="flex" justifyContent="flex-end">
                <Button
                  startIcon={<AddBox />}
                  onClick={() => arrayHelpers.push(DefaultFrequencyBand)}
                  disabled={values?.filters?.frequencyBands?.length > 4}
                >
                  Добавить
                </Button>
              </Grid>
            </>
          )}
        />
      </Grid>
    </>
  );
};

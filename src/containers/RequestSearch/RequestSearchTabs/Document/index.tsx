import React from 'react';

import { Grid } from '@mui/material';

import { AutoCompleteInput } from '../../../../components/AutoCompleteInput';
import { ClerkInput } from '../../../../components/ClerkInput';
import { DateRange } from '../../../../components/DateRange';
import { ListInput } from '../../../../components/ListInput';
import {
  RequestSearchFields,
  RequestSearchLabels,
} from '../../../../constants/requestSearch';
import {
  useGetDocumentRubricsOptionsQuery,
  useGetDocumentTypesQuery,
} from '../../../../services/api/dictionaries';

interface FrequencyBandProps {}

export const Document: React.FC<FrequencyBandProps> = () => {
  const { data: types = [] } = useGetDocumentTypesQuery({
    includeIrrelevant: true,
  });
  const { data: rubrics = [] } = useGetDocumentRubricsOptionsQuery({});

  return (
    <Grid container spacing={3} sx={{ mb: 2 }}>
      <Grid item xs={12}>
        <AutoCompleteInput
          name={RequestSearchFields.DOC_TYPES}
          label={RequestSearchLabels[RequestSearchFields.DOC_TYPES]}
          multiple
          options={types}
        />
      </Grid>
      <Grid item xs={6}>
        <ListInput
          name={RequestSearchFields.DOC_NUMBERS}
          label={RequestSearchLabels[RequestSearchFields.DOC_NUMBERS]}
          multiline
          rows={3}
          separator="line-break"
        />
      </Grid>
      <Grid item xs={6}>
        <DateRange
          nameFrom={RequestSearchFields.DOC_DATE_FROM}
          nameBefore={RequestSearchFields.DOC_DATE_TO}
          labelFrom={RequestSearchLabels[RequestSearchFields.DOC_DATE_FROM]}
          labelBefore={RequestSearchLabels[RequestSearchFields.DOC_DATE_TO]}
        />
      </Grid>
      <Grid item xs={6}>
        <ListInput
          name={RequestSearchFields.DOC_CONTRACTOR_NUMBERS}
          label={
            RequestSearchLabels[RequestSearchFields.DOC_CONTRACTOR_NUMBERS]
          }
        />
      </Grid>
      <Grid item xs={6}>
        <DateRange
          nameFrom={RequestSearchFields.DOC_CONTRACTOR_DATE_FROM}
          nameBefore={RequestSearchFields.DOC_CONTRACTOR_DATE_TO}
          labelFrom={
            RequestSearchLabels[RequestSearchFields.DOC_CONTRACTOR_DATE_FROM]
          }
          labelBefore={
            RequestSearchLabels[RequestSearchFields.DOC_CONTRACTOR_DATE_TO]
          }
        />
      </Grid>
      <Grid item xs={6}>
        <ListInput
          name={RequestSearchFields.LOCAL_NUMBERS}
          label={RequestSearchLabels[RequestSearchFields.LOCAL_NUMBERS]}
        />
      </Grid>
      <Grid item xs={6}>
        <DateRange
          nameFrom={RequestSearchFields.DOC_CREATE_DATE_FROM}
          nameBefore={RequestSearchFields.DOC_CREATE_DATE_TO}
          labelFrom={
            RequestSearchLabels[RequestSearchFields.DOC_CREATE_DATE_FROM]
          }
          labelBefore={
            RequestSearchLabels[RequestSearchFields.DOC_CREATE_DATE_TO]
          }
        />
      </Grid>
      <Grid item xs={12}>
        <AutoCompleteInput
          name={RequestSearchFields.RUBRICS}
          label={RequestSearchLabels[RequestSearchFields.RUBRICS]}
          multiple
          options={rubrics}
        />
      </Grid>
      <Grid item xs={6}>
        <ClerkInput
          name={RequestSearchFields.PERFORMER}
          label={RequestSearchLabels[RequestSearchFields.PERFORMER]}
        />
      </Grid>
    </Grid>
  );
};

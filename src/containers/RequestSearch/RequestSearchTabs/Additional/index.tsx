import React from 'react';
import { FieldArray, FormikValues, useFormikContext } from 'formik';

import { Grid } from '@mui/material';

import { AddBlock } from '../../../../components/AddBlock';
import { RequestSearchFields } from '../../../../constants/requestSearch';

import { FormRow } from './FormRow';

export const Additional: React.FC = () => {
  const { values } = useFormikContext<FormikValues>();
  const objects = values.filters.objects;
  const emptyObject = {
    typeCode: null,
    documentDate: null,
  };

  return (
    <Grid container spacing={2} sx={{ mb: 1 }}>
      <FieldArray
        name={RequestSearchFields.OBJECTS}
        render={(arrayHelpers) => {
          return [
            <Grid item xs={12}>
              {objects?.map((_obj: any, index: number) => (
                <FormRow key={index} index={index} helpers={arrayHelpers} />
              ))}
            </Grid>,
            <Grid item xs={12}>
              <AddBlock
                disabled={objects?.length > 4}
                onAdd={() => arrayHelpers.push(emptyObject)}
              />
            </Grid>,
          ];
        }}
      />
    </Grid>
  );
};

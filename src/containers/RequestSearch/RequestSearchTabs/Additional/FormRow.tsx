import React from 'react';
import { FieldArrayRenderProps, FormikValues, useFormikContext } from 'formik';

import { DeleteOutline } from '@mui/icons-material';
import { Button, Grid, Stack } from '@mui/material';

import { AutoCompleteInput } from '../../../../components/AutoCompleteInput';
import { DatePickerInput } from '../../../../components/DatePickerInput';
import Input from '../../../../components/Input';
import {
  docTypesOptions,
  RequestSearchFields,
  RequestSearchLabels,
} from '../../../../constants/requestSearch';

export interface FormRowProps {
  index: number;
  helpers: FieldArrayRenderProps;
}

export const FormRow: React.FC<FormRowProps> = ({ index, helpers }) => {
  const { handleChange, values } = useFormikContext<FormikValues>();

  return (
    <Grid container spacing={2} sx={{ mb: 2 }}>
      <Grid item xs={6}>
        <AutoCompleteInput
          name={`${RequestSearchFields.OBJECTS}[${index}].${RequestSearchFields.OBJECTS_DOC_TYPE}`}
          label={RequestSearchLabels[RequestSearchFields.OBJECTS_DOC_TYPE]}
          options={docTypesOptions}
        />
      </Grid>
      <Grid item xs={3}>
        <Input
          name={`${RequestSearchFields.OBJECTS}[${index}].${RequestSearchFields.OBJECTS_DOC_NUMBER}`}
          label={RequestSearchLabels[RequestSearchFields.OBJECTS_DOC_NUMBER]}
          value={
            values.filters.objects
              ? values.filters.objects[index].documentNumber
              : ''
          }
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={3}>
        <DatePickerInput
          name={`${RequestSearchFields.OBJECTS}[${index}].${RequestSearchFields.OBJECTS_DOC_DATE}`}
          label={RequestSearchLabels[RequestSearchFields.OBJECTS_DOC_DATE]}
        />
      </Grid>
      <Grid item xs={12}>
        <Stack flexDirection="row" justifyContent="flex-end">
          <Button
            endIcon={<DeleteOutline />}
            onClick={() => helpers.remove(index)}
          >
            Удалить
          </Button>
        </Stack>
      </Grid>
    </Grid>
  );
};

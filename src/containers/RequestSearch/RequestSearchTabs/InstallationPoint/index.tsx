import React from 'react';
import { useFormikContext } from 'formik';
import { get } from 'lodash';

import { Grid } from '@mui/material';

import { CoordinationInputGroup } from '../../../../components/Coordination/CoordinationInputGroup';
import Input from '../../../../components/Input';
import {
  RequestSearchFields,
  RequestSearchLabels,
} from '../../../../constants/requestSearch';

interface InstallationPointProps {}

export const InstallationPoint: React.FC<InstallationPointProps> = () => {
  const { handleChange, values = {} as any } = useFormikContext();
  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Input
          name={RequestSearchFields.STATION_NAME}
          label={RequestSearchLabels[RequestSearchFields.STATION_NAME]}
          value={get(values, RequestSearchFields.STATION_NAME) || ''}
          onChange={handleChange}
        />
      </Grid>
      <CoordinationInputGroup
        names={[
          RequestSearchFields.LATITUDE_FROM,
          RequestSearchFields.LATITUDE_TO,
        ]}
        charName={RequestSearchFields.LATITUDE_CHARACTER}
        labels={[
          RequestSearchLabels[RequestSearchFields.LATITUDE_FROM],
          RequestSearchLabels[RequestSearchFields.LATITUDE_TO],
        ]}
        type="latitude"
        xs={6}
      />
      <CoordinationInputGroup
        names={[
          RequestSearchFields.LONGTITUTE_FROM,
          RequestSearchFields.LONGTITUTE_TO,
        ]}
        charName={RequestSearchFields.LONGTITUTE_CHARACTER}
        labels={[
          RequestSearchLabels[RequestSearchFields.LONGTITUTE_FROM],
          RequestSearchLabels[RequestSearchFields.LONGTITUTE_TO],
        ]}
        type="longtitude"
        xs={6}
      />
      <Grid item xs={12}>
        <Input
          name={RequestSearchFields.STATION_ADDRESS}
          label={RequestSearchLabels[RequestSearchFields.STATION_ADDRESS]}
          value={get(values, RequestSearchFields.STATION_ADDRESS) || ''}
          onChange={handleChange}
        />
      </Grid>
    </Grid>
  );
};

import { ExecutionFields } from '../constants/execution';

export interface ExecutionFormState {
  [ExecutionFields.FILE_LIST]: any;
  [ExecutionFields.COMMENTS]: string | null;
  [ExecutionFields.TERM_WORK_DAYS]: number | null;
  [ExecutionFields.PERFORMER_ID]: number | null;
  [ExecutionFields.REQUEST_MANAGER]: number | null;
}

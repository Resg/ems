export type DictionariesAccessType = {
  code: string;
  name: string;
};

export type DictionariesRubricType = {
  id: number;
  documentClass: string;
  documentTypeId: number;
  name: string;
  controlPeriod: number;
  flags: number;
  controlPeriodType: string;
};

export type DictionariesTemplateType = {
  id: number;
  description: string;
};

export type DictionariesClerkType = {
  id: number;
  personId: number;
  personFio: string;
  userId: number;
  divisionId: number;
  division: string;
  postId: number;
  post: string;
  flags: number;
  isDefaultRegistrar: boolean;
  isSigningAuthority: boolean;
  isFired: boolean;
  isOld: boolean;
  startDate: string;
  viewStartDate: string;
  endDate: string;
  viewEndDate: string;
  actionId: number;
  searchPersonFio: string;
  login: string;
  divisionKey: string;
  dpCode: string;
  phoneNumber: string;
  internalPhoneNumber: string;
};

export type ClerkForFormType = {
  id: number;
  clerkId: number;
  clerkName: string;
  divisionId: number;
};

export interface Option {
  label: string;
  value: any;
  parent?: number;
  divisionId?: number;
  isDefaultOutgoingDocumentsSigner?: boolean;
}

export type RoutesType = {
  id: number;
  name: string;
  comment: string;
  editClass: string;
  executeClass: string;
  displayName: string;
  objectTypeCode: string;
  hasParentTask: boolean;
  termDefault: number;
  сonfirmDefault: boolean;
  recordTypeCode: string;
  parentId: number;
};

export interface RequestTypesType {
  id: number;
  name: string;
  needCountry: boolean;
  reissuance: boolean;
  releaseLicence: boolean;
}

export interface RequestStagesType {
  id: number;
  name: string;
}

export interface RadioServiceType {
  id: number;
  name: string;
  useTechnology: boolean;
  useNetCategory: boolean;
  useProgram: boolean;
  regionOptional: boolean;
}

export interface ServiceMRFCType {
  id: number;
  name: string;
  nameEnglish: string;
  useTechnologyStandardRKN: boolean;
  useMultiplex: boolean;
}

export interface TechnologyType {
  id: number;
  name: string;
  division: number;
  useBandTx: boolean;
  useBandRx: boolean;
  useChannelList: boolean;
  useFrequencyList: boolean;
  useBandChannel: boolean;
  bandTxDefault: string;
  bandRxDefault: string;
}

export interface FrequencyBandType {
  id: number;
  name: string;
  netType: number;
  txFrequencyBandLabel: string;
  txFrequencyBandBegin: number;
  txFrequencyBandEnd: number;
  rxFrequencyBandLabel: string;
  rxFrequencyBandBegin: number;
  rxFrequencyBandEnd: number;
}

export interface FrequencyBandSearchType {
  radioService?: number;
  serviceMrfc?: number;
  technology?: number;
  direction?: string;
}

export interface SSEType {
  code: string;
  value: string;
}

export interface SSEResponseType {
  data: SSEType[];
}

export interface ProgramType {
  id: number;
  name: string;
}

export interface NetTypesType {
  id: number;
  name: string;
  radioService: number;
  netCategory: number;
}

export interface NetTypesParamsType {
  serviceMrfc?: number;
}

export interface TechologyParamsType {
  radioService?: number;
  serviceMRFC?: number;
}

export interface RegionType {
  federalDistrict: number;
  aoguid: string;
  name: string;
  nameEnglish: string;
}

export interface ContractorType {
  id: number;
  name: string;
  isOld: boolean;
  isReorganized: boolean;
}

export interface ContractorParamsType {
  name?: string;
  selectionType?: number;
  documentTypeId?: number;
  isOnlyActual?: boolean;
}

export interface TaskType {
  id: number;
  name: string;
  comment: string;
  editClass: string;
  executeClass: string;
  displayName: string;
  objectTypeCode: string;
  hasParentTask: boolean;
  termDefault: number;
  сonfirmDefault: boolean;
  recordTypeCode: string;
}

export interface ConclusionData {
  domain: string;
  code: string;
  name: string;
  description: string;
  flags: number;
}

export interface DocumentTemplate {
  id: number;
  rubricId: number;
  itemId: number;
  parentId: number;
  templateId: number;
  description: string;
  executableFileName: string;
  executableTemplate: string;
  parametersCode: string;
  isFolder: boolean;
  level: number;
  documentTypeId: number;
  externalParameters: string;
}

export interface Counterparty {
  id: number;
  fullName: string;
  name: string;
  inn: string;
  ogrn: string;
  statusCode: string;
  status: string;
  typeCode: string;
  type: string;
  ownership: string;
  legalAddress: string;
  postalAddress: string;
  parentId: number;
  pfrentName: string;
  comments: string;
  legalAddressFias: string;
  postalAddressFias: string;
}

export interface CounterpartySearchParameters {
  elasticSearch?: string;
  ids?: number[];
  templateId?: number;
  objectId?: number;
  objectTypeCode?: string;
  requestIds?: number[];
}

export interface CounterpartySearchWithPagination {
  searchParameters?: {
    elasticSearch?: string;
    ids?: number[];
    templateId?: number;
    objectId?: number;
    objectTypeCode?: string;
    requestIds?: number[];
  };
  sort?: string;
  page?: number;
  size?: number;
  useCount?: boolean;
}

export interface DeliveryType {
  id: number;
  existedContractorId: number;
  existedContactPersonId: number;
  existedAddressId: number;
  existedDeliveryId: number;
  existedContactId: number;
  existedIsContactPersonContact: string;
  existedContactInfo: string;
  addressAndContactInfoView: string;
  deliveryType: string;
  isEnabled: boolean;
  isPostalDelivery: boolean;
  isElectronicDelivery: boolean;
  isDefaultDelivery: boolean;
  addressId: number;
  deliveryId: number;
  contractorId: number;
  contractorName: string;
  address: string;
  deliveryTypeCode: string;
  deliveryFlags: number;
  contactId: number;
  isContactPersonContact: string;
  contactInfoView: string;
  contactPersonLastName: string;
  contactPersonFirstName: string;
  contactPersonMiddleName: string;
  contactPersonId: number;
  priorityByDeliveryType: number;
  rowNumberByDeliveryType: number;
  isEds: boolean;
  rowNumberByContactType: number;
  isSelected: boolean;
  errorMessage: string;
  initialId?: number;
}

export interface CounterpartyPerson {
  contactFio: string;
  contactPersonId: number;
  documentTypeNumberDate: string | null;
  personId: number;
  postName: string;
}

export interface ConclusionPeriod {
  code: string;
  value: string;
}

export interface ConclusionState {
  code: string;
  name: string;
}

export interface ConclusionAnnulment {
  code: string;
  value: string;
}

export interface RequestModalSolution {
  id: number;
  page?: number;
  size?: number;
  useCount?: boolean;
  number: string;
  date: string;
  dateEnd: string | null;
}

export interface RequestModalZeForCancellation {
  id: number;
  number: string;
  date: string;
  dateValidity: string;
  dateAnnulment: string;
  state: string;
  status: string;
  type: string;
  author: string;
}

export interface RequestModalParams {
  id: number;
  page?: number;
  size?: number;
  useCount?: boolean;
  number: string;
  datePeriodStart: string | null;
  datePeriodEnd: string | null;
}

export interface CommunicationChannel {
  id: number;
  name: string;
  description: string;
  group: string;
  frequencyBandBegin: number;
  frequencyBandEnd: number;
}

export interface RegistrationState extends ConclusionState {}

export interface MPZState extends ConclusionState {}

export interface InnerAddress {
  id: string;
  name: string;
  typeCode: 'CLERK' | 'DIVISION';
}

export interface CoordinationStatus extends ConclusionState {}

export interface Country extends ProgramType {}

export interface RegistryName {
  id: number;
  name: string;
  code: number;
  registryTypeCode: string;
  printedFormTemplateId: number;
  comments: string;
}

export interface RegistryPacketName {
  id: number;
  name: string;
  type: string;
  templateId: number;
  templateTypeId: number;
}

export interface FrequencyMeasure {
  abbreviation: string;
  description: string;
  valueInHZ: number;
  koefficientToStandart: string;
  isStandart: boolean;
}

export interface OrganizationalLegalForm {
  id: number;
  name: string;
  nameDative: string;
  nameGenitive: string;
  nameInstrumental: string;
  nameNominative: string;
  abbreviation: string;
}
export interface AdditionalConclusionDictionaryData {
  id: number;
  name: string;
  isActual: boolean;
  conclusionText: string;
  permissionText: string;
}
export interface PermissionTypesType {
  code: string;
  description: string;
  orderNumber: number;
  permissionType: string;
  baseDocumentType: string;
}

export interface FederalDistrictsType {
  federalDistrictCode: string;
  federalDistrict: string;
}

export interface InternalRegistryStates {
  code: string;
  name: string;
}

export interface DefaultAddresseeDeliveryType {
  isOnHand: boolean;
  onHandFinishDate: string;
  deliveryId: number;
  contactPersonId: number;
  contactPersonPersonId: number;
  contractorFullName: string;
  addressId: number;
  address: string;
  defaultDeliveryId: number;
  defaultDeliveryTypeCode: string;
  defaultDeliveryType: string;
  contactInfo: string;
  contactId: number;
  contractorId: number;
}

export interface AdressesListType {
  id: number;
  index: string;
  address: string;
  addressTypeCode: string;
  addressType: string;
  contractorId: number;
  deliveryTypeCode: string;
  deliveryType: string;
}

export interface AdressesTransofrmListType {
  index: string;
  address: string;
}

export interface MailPackage {
  id: number;
  deliveryDate: string;
  localNumber: number;
  returnDate: string;
  sendingDateTime: string;
  comment: string;
  deliveryName: string;
  notificationNumber: number;
  postRegistryNumber: number;
  postRegistryDateTime: string;
  sendingsCount: number;
}

export interface MailPackageSearch {
  searchParameters?: {
    packetId?: string | number;
  };
  sort?: string;
  page?: number;
  size?: number;
  useCount?: boolean;
}

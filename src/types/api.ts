export interface ListResponse<T> {
  data: T;
  page: number;
  pageCount: number;
  queryGUID: string;
  size: number;
  sort: string;
  totalCount: number;
  useCount: boolean;
}

export interface SearchDataType {
  searchParameters?: {
    elasticSearch?: string;
    technologyId?: number;
    serviceId?: number;
    bandReceiveId?: number;
    bandTransmitId?: number;
    requestId?: number;
  };
  sort?: string;
  page?: number;
  size?: number;
  useCount?: boolean;
  abortController?: AbortController;
}

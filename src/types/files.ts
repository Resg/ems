export interface File {
  documentId: number; // 123,
  name: string; // "1.docx",
  description: string; // "Файл",
  content: any; // <BLOB>,
  extension: string; // 'docx',
  size: number; // 1157,
  typeCode: string; // "F",
  additionalAction: string; // "Добавление описания файла"
  config: Record<string, any>;
  canOpenInR7Office: boolean;
  url: string;
  title: string;
}

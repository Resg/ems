export interface LinkRequests {
  requestId: number;
  id: number;
  number: string;
  date: string;
  clientName: string;
  manager: string;
  lastStage: string;
  lastStageDate: string;
  hasActiveLicence: boolean;
}

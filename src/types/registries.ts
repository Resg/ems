export interface Registry {
  id: number;
  date: string;
  number: string;
  senderDivisionId: number;
  senderDivision: string;
  senderDivisionKey: string;
  senderDivisionFullName: string;
  senderId: number;
  senderName: string;
  senderPost: string;
  recipientDivisionId: number;
  recipientDivision: string;
  recipientDivisionKey: number;
  recipientId: number;
  recipientName: string;
  recipientPost: string;
  typeCode: string;
  type: string;
  templateCode: string;
  template: string;
  stateCode: string;
  state: string;
  actionId: number;
  doneDateTimeName: string;
  actionSentId: number;
  sentDateTimeName: string;
  actionReceivedId: number;
  receivedDateTimeName: string;
  comments: string;
  nameId: number;
  name: string;
  nameCode: number;
  sentTime: string;
}

export interface RegistryDocuments {
  id: number;
  orderNumberInRegistry: number;
  type: string;
  number: string;
  date: string;
  compound: string;
  rubricList: string;
  contractorList: string;
  orderNumberName: string;
  status: string;
  registryId: number;
  copyId: number;
}
export interface SaveRegistryParams {
  id: number;
  date: string;
  number: string;
  senderDivisionId?: number;
  senderId?: number;
  recipientDivisionId?: number;
  recipientId?: number;
  typeCode: string;
  templateCode: string;
  comments: string;
  nameId: number;
}

export interface RegistrySearchType {
  id: number;
  number: string;
  date: string;
  sentDateTimeName: string;
  sentTime: string;
  receivedDateTimeName: string;
  receivedTime: string;
  senderDivision: string;
  senderName: string;
  name: string;
  recipientDivision: string;
  recipientName: string;
  type?: string;
  state?: string;
  template?: string;
}

export type RegistriesSearchDataType = {
  page?: number;
  size?: number;
  useCount?: boolean;
  sort?: string;
  searchParameters?: {
    elasticSearch?: string;
    viewCode?: string;
    isDocumentRegistry?: boolean;
    isOutgoingPacketRegistry?: boolean;
    stateCode?: string[];
  };
};
export interface AddDocumentToRegistryParams {
  divisionId?: number;
  clerkId?: number;
  typeCode: string;
  createNew: boolean;
  registryNameId: number | null;
  comments: string | null;
  document: Array<{ id: number; copyId: number[] }>;
}

export interface AddPacketToRegistryParams {
  packetIds: number[];
  divisionId?: number;
  clerkId?: number;
  typeCode: string;
  createNew: boolean;
  templateCode: string;
  registryNameId: number;
  comments: string;
}

export interface RegistryPackets {
  id: number;
  number: string;
  dateSign: string;
  dateRegistration: string;
  contractorName: string;
  typeDelivery: string;
  rubricList: string;
  state: string;
  registryId: number;
  ids: number;
}

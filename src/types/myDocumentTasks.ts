export interface MyDocTasksColumnType {
  key: Exclude<keyof MyDocTasksRowType, 'id' | 'selected'>;
  name: string;
  hidden: boolean;
}

export interface MyDocTasksRowType {
  title: string;
  author: string;
  performer: string;
  comments: string | null;
  performerComment: string | null;
  status: string;
  termLabel: string | null;
  completeDate: string | null;
  result: string | null;
  createDate: string;
  editor: string;
  id: number;
  rootObjectId: string;
  externalTaskId: string | null;
  objectLabel: string;
  selected: boolean;
}

export interface MyTasksType {
  title: string;
  author: string;
  performer: string;
  objectLabel: string;
  termLabel: string | null;
  comments: string | null;
  result: string | null;
  performerComment: string | null;
  status: string;
  completeDate: string | null;
  createDate: string;
  objectTypeCode: string;
  objectTypeCodeCalculated: string;
  objectId: string;
  authorId: number;
  previousId: number | null;
  id: number;
  typeId: number;
  parentId: number | null;
  canCreateSubtask: boolean;
  canDelete: boolean;
  canSendDoAction: boolean;
  canExecute: boolean;
  performerId: number;
  editorId: number;
  editor: string;
  canSendToAuthor: boolean;
  canReturnToEditor: boolean;
  canReturnToExecution: boolean;
  canRevokeFromExecution: boolean;
  canReassignPerformer: boolean;
  canSendToExecution: boolean;
  rootObjectId: string;
  editClass: string;
  executeClass: string;
  isExternalForm: boolean;
  externalTaskId: string | null;
  selected: boolean;
}

export interface MyDocTasksType {
  data: Array<MyTasksType>;
  page: number;
  pageCount: number;
  queryGUID: string;
  size: number;
  sort: string;
  useCount: boolean;
  totalCount: number;
}

export interface EmployeeData {
  id: number;
  personId: number;
  personFio: string;
  userId: number;
  divisionId: number;
  division: string;
  postId: number;
  post: string;
  flags: number;
  isSigningAuthority: boolean;
  isFired: boolean;
  isOld: boolean;
  startDate: string;
  viewStartDate: string;
  endDate: string;
  viewEndDate: string;
  actionId: number;
  searchPersonFio: string;
  login: string;
  divisionKey: string;
  dpCode: string;
  phoneNumber: string;
  internalPhoneNumber: string;
  personFioShort: string;
  isDefaultOutgoingDocumentsSigner: true;
  isDefaultRegistrar: boolean;
}

export interface DivisionData {
  id: number;
  divisionParentId: number;
  addDate: string;
  endDate: string;
  actionId: number;
  divisionCode: string;
  shortName: string;
  fullName: string;
  managerId: number;
  managerFIO: string;
  type: string;
  searchName: string;
}

export interface EmployeeTreeNode {
  div: DivisionData;
  children: EmployeeTreeNode[];
  employees: EmployeeData[];
}

export interface AllTreesNode {
  [key: number]: EmployeeTreeNode;
}

export interface ObjectTask {
  temporaryId?: number;
  temporaryPreviousId?: number;
  temporaryParentId?: number;
  title: string;
  author: string;
  performer: string;
  objectLabel: string;
  termLabel: string | null;
  comments: string | null;
  result: string | null;
  performerComment: string | null;
  status: string;
  completeDate: string | null;
  createDate: string;
  objectTypeCode: string;
  objectTypeCodeCalculated: string;
  objectId: string;
  authorId: number;
  previousId: number | null;
  id: number;
  typeId: number;
  parentId: number | null;
  canCreateSubtask: boolean;
  canDelete: boolean;
  canSendDoAction: boolean;
  canExecute: boolean;
  performerId: number;
  editorId: number;
  editor: string;
  canSendToAuthor: boolean;
  canReturnToEditor: boolean;
  canReturnToExecution: boolean;
  canRevokeFromExecution: boolean;
  canReassignPerformer: boolean;
  rootObjectId: string;
  editClass: string;
  executeClass: string;
  isExternalForm: boolean;
  externalTaskId: string | null;
  canSendToExecution: boolean;
}

export interface TaskRouteData {
  authorId: number;
  comments?: string;
  isControl: boolean;
  objectId?: string | number;
  objectTypeCode?: string;
  parentId?: number;
  performerId?: number;
  planDate?: string | null;
  sendForExecution: boolean;
  temporaryId: number;
  temporaryОbjectId?: number;
  temporaryParentId?: number;
  temporaryPreviousId?: number;
  term?: number;
  title?: string;
  typeId?: number;
  previousId?: number;
}

export interface SubmitTemplateData {
  temporaryId?: number;
  typeId?: number;
  title?: string | null;
  comments?: string | null;
  parentId?: number;
  temporaryParentId?: number;
  previousId?: number;
  temporaryPreviousId?: number;
  objectTypeCode?: string;
  rootObjectId?: string;
  temporaryОbjectId?: number;
  term?: number | null;
  planDate?: string | null;
  performerId?: number;
  authorId?: number;
  isControl?: boolean;
}

export interface SiblingType {
  id: number;
  title: string;
  comment?: string;
}

export interface TaskDetails {
  id: number;
  editorId: number;
  editor: string;
  authorId: number;
  author: string;
  performerId: number;
  performer: string;
  performerShortName: string;
  typeId: number;
  objectTypeCode: string;
  objectId: number;
  isControl: boolean;
  parentId: number;
  previousId: number;
  previousTitle: string;
  title: string;
  term: number;
  comments: string;
  performerComment: string;
  statusId: number;
  status: string;
  continueStatusId: number;
  createDate: string;
  canEditPerformer: boolean;
  canEditAuthor: boolean;
  canEditDateTarget: boolean;
  canEditTaskRelations: boolean;
  canEditComments: boolean;
  canEditNotifyOnEnd: boolean;
  canSendToAuthor: boolean;
  isPerformerCommentRequired: boolean;
  rootObjectId: number;
  isEds: boolean;
  planDate: string;
  editClass: string;
  executeClass: string;
  isExternalForm: boolean;
  externalTaskId: string;
}

export interface TaskSubtaskUpdateData {
  id: number;
  title: string;
  comment: string;
  previousId: number;
  term: number;
  performerId: number;
  isControl: boolean;
  authorId: number;
  NULLS: boolean;
  dateTarget: string;
}

export interface TaskSubtaskValuesData {
  comments: string;
  executeAfter: number;
  executor: SiblingType[];
  name: string;
}

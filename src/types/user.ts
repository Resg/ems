export interface UserInfo {
  login: string;
  clerkId: number;
  personId: number;
  personFio: string;
  personFioShort: string;
  userId: number;
  divisionId: number;
  division: string;
  postId: number;
  post: string;
  divisionKey: string;
  availableRoles: string | null;
  departmentId: number;
}

export interface TaskTemplate {
  id: number;
  name: string;
  template: string;
  userLogin: string;
  objectTypeCode: string;
}

export interface AdditionalTemplate {
  id: number;
  name: string;
}

export interface SettingsTemplate {
  userLogin?: string;
  openFormInNewTab?: boolean;
  autoCountersUpdate?: boolean;
}

export interface UpdateSettingsTemplate {
  openFormInNewTab: boolean;
  autoCountersUpdate: boolean;
}

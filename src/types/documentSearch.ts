export interface DocumentSearch {
  type: string;
  id: number;
  number: string;
  date: string;
  dateSign: string;
  localNumber: string;
  dateCreate: string;
  rubricList: string;
  description: string;
  comments: string;
  status: string;
  contractorList: string;
  contractorNumber: string;
  contractorDate: string;
  contractorCountry: string;
  recipientList: string;
  incomingNumber: string;
  recieptDate: string;
  performer: string;
  divisionFullName: string;
  requestList: string;
  returnDate: string;
  contractorDateSign: string;
  recipientListDRKK: string;
  channelList: string;
  regionList: string;
  resAddressList: string;
  managerList: string;
  сoordinationСountryCodeList: string;
}

export interface searchValues {
  chipName: string;
  filters: Record<string, any>;
}

export interface CalculateDate {
  date?: string;
}

export interface CalculateDateParams {
  startDate: string;
  numberOfDays: string;
  dayType?: string;
}

export interface DocumentSearchType extends DocumentSearch {}

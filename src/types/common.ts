export interface RequestOptions {
  method: 'GET' | 'POST' | 'DELETE' | 'PUT' | 'OPTIONS';
  url: string;
  headers?: Record<string, any>;
  data?: any;
  params?: Record<string, any>;
  signal?: AbortSignal;
}

declare global {
  interface Window {
    DocsAPI: any;
  }
}

export interface RequestType {
  id: number;
  number: string;
  date: string;
  requestType: number;
  radioService: number;
  serviceMRFC: number;
  technology: number;
  technologyRKN: number;
  frequencyRangeTx: number;
  frequencyRangeRx: number;
  frequencyRange: number;
  frequency?: number[];
  channelRange: number[];
  program: number;
  multiplex: string;
  netCategory: number;
  netType: number;
  region: string[];
  address: string;
  commentaryRFG: string;
  commentary: string;
  sse: string;
  usi: boolean;
  dynamic: boolean;
  client: number[];
  manager: number;
  incomingDocument: number;
  conclusion?: number;
  decision: number;
  formClassName: string;
  outgoingDocument: string;
  channel: number[];
  document?: string;
  permission?: number;
  documentType?: string;

  lastStage?: number | null;
  editorDateName?: string; // 'последние изменения'

  creatorDateName?: string; // 'автор заявки'
  materialIncomingNumber?: string;
  materialIncomingDate?: string;
  materialOuterNumber?: string;
  materialOuterDate?: string;
  materialCreatorDateName?: string; // 'автор вх. письма'
}

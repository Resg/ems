export interface RequestStage {
  comments: string;
  controlPeriod: number;
  controlPeriodType: string;
  creatorDateName: string;
  divisionId: number;
  divisionName: string;
  editorDateName: string;
  endDate: string;
  flowingOperationName: string;
  isAutomatic: boolean;
  isControlPeriodCustomizable: boolean;
  isMainFlow: boolean;
  isnAct: number;
  operationId: number;
  parentDivisionId: number;
  parentDivisionName: string;
  performerClerkId: number;
  performerClerkName: string;
  performerExternal: string | null;
  planDate: string;
  previousOperationId: number;
  requestId: number;
  stageFromId: number;
  stageId: string;
  stageName: string;
  stageToId: number;
  startDate: string;
  stateCode: string | null;
  stateName: string | null;
  transitionId: number;
  viewEndDate: string;
}

export interface RequestData {
  addressStation: string | null;
  bandReceivingId: number;
  bandTransmitId: number;
  canAddStation: boolean;
  canDeleteStation: boolean;
  canEdit: boolean;
  channelId: number;
  channelList: string | null;
  channelName: string;
  clientId: number;
  clientIdList: number[];
  clientName: string | null;
  clientNameList: string[] | null;
  closeDate: string | null;
  comments: string;
  commentsForRfg: string;
  conclusionId: number | null;
  conclusionNumber: number | null;
  country: string | null;
  countryId: number | null;
  creatorDateName: string;
  creatorMaterialsDateName: string;
  date: string;
  editorDateName: string;
  endDate: string | null;
  frequency: string | null;
  frequencyReceivingText: string;
  frequencyTransmitText: string;
  hasBondedRequests: boolean;
  id: number;
  inProcess: boolean;
  isConclusionToAnnulList: boolean;
  isDynamic: boolean;
  isLicenseToAnnulList: boolean;
  isNeedCountry: boolean;
  isRegionOptional: boolean;
  isRemake: boolean;
  isUsi: boolean;
  isnAct: string;
  kladrLevelHigh: number;
  kladrLevelLow: number;
  lastStage: string | null;
  lastStageDate: string | null;
  manager: string;
  managerDivisionId: number;
  managerId: number;
  materialId: number;
  materialInnerDate: string;
  materialInnerNumber: string;
  materialOuterDate: string;
  materialOuterNumber: string;
  mpzForm: string;
  multiplexNumberCode: number | null;
  multiplexNumberValue: number | null;
  networkCategoryID: number;
  networkCategoryName: string;
  networkStateCode: string;
  networkStateValue: string;
  networkTypeCode: string | null;
  networkTypeName: string | null;
  number: string;
  pchtrFormType: string;
  permissionAnnulmentDate: string | null;
  permissionCreatDateName: string | null;
  permissionDate: string | null;
  permissionId: string | null;
  permissionNumber: number | null;
  permissionStatus: string | null;
  permissionValidity: string | null;
  reassignNumber: number | null;
  reassignType: string | null;
  regionIdList: string;
  regionList: string[] | null;
  resAddress: string | null;
  resCount: string | null;
  rknTechnologyCode: number | null;
  rknTechnologyName: string | null;
  rknflag: boolean;
  schemaFlag: boolean;
  service: string;
  serviceGrhc: string;
  serviceGrhcId: number;
  serviceId: number;
  shipId: number | null;
  showMultiplex: boolean;
  showStages: string | null;
  showStationInformation: boolean;
  startDate: string;
  stationCount: number;
  status: string;
  statusId: number;
  szm: string;
  szmCode: string;
  szmValue: string;
  technology: string | null;
  technologyId: number;
  type: string;
  typeId: number;
  useChannelList: boolean;
  useConclusionValidPeriod: boolean;
  useFrequencyList: boolean;
  useNetworkCategory: boolean;
  useReceivingBand: boolean;
  useTechnology: boolean;
  useTransmitBand: boolean;
  viewEndDate: string;
  viewStartDate: string;
  requestType: number;
}

export interface StageCreateData {
  name: string;
  stageId: number;
  stageName: string;
  controlPeriod: number;
  controlPeriodType: string;
  isControlPeriodCustomizable: boolean;
  defaultParentDivisionid: number;
  defaultParentDivisionName: string;
  countRequest: number;
}

export interface EditStageData {
  requestId: number;
  stageId: number;
  parentDivisionId: number;
  divisionId: number;
  performerClerkId: number;
  performerExternal: string | null;
  comments: string;
  stateCode: string | null;
  startDate: string;
  controlPeriod: number;
}

export interface UpdateStageData {
  //requestId: number;
  // stageId: number;
  parentDivisionId: number;
  divisionId: number;
  performerClerkId: number;
  //performerExternal: string | null;
  comments: string;
  //stateCode: string | null;
  startDate: string;
  controlPeriod: number;
  operationId: number;
  endDate: string;
}

export interface RequestDocument {
  id: number;
  title: string;
  typeId?: number;
  type: string;
  date: string;
  comments?: string;
  number: string;
  rubricId?: number;
  rubric?: string;
  objectTypeCode?: string;
  objectId?: string;
  description: string;
  contractorList?: string;
  recipientList?: string;
  rubricList: string;
  localNumber?: string;
  numberSort?: string;
  contractorNumber?: string;
  contractorDate: string;
  requestNumber?: string;
  canBeMaterial?: boolean;
  isMaterial?: boolean;
  dateSign?: string;
  performer?: string;
}

export interface RequestDocumentRows {
  id: number;
  title: string;
  type: string;
  date: string;
  number: string;
  description: string;
  rubricList: string;
  contractorList: string;
  contractorNumber: string;
  contractorDate: string;
}

export interface ConclusionData {
  id: number;
  requestId: number;
  number: string;
  date: string;
  dateValidity: string;
  dateAnnulment: string;
  stateCode: string;
  state: string;
  statusCode: 'Y' | 'N';
  status: string;
  typeCode: 'Y' | 'N';
  type: string;
  sortId: number;
  sort: string;
  createdDateTimeName: string;
}

export interface AnnulledConclusionData extends ConclusionData {
  conclusionRequestId: number;
}

export interface ConclusionDocumentsData {
  id: number;
  title: string;
  type: string;
  number: string;
  rubricList: string;
  performer: string;
  description: string;
  date: string;
  dateSign: string;
  recipientList: string;
  contractorDate: string;
  contractorNumber: string;
}
export interface ConclusionClient {
  id: number;
  requestId: number;
  name: string;
  orderNumber: number;
}

export interface AnnulledPermissions {
  id: number;
  requestId: number;
  permissionId: number;
  contractorName: string;
  number: string;
  date: string;
  dateValidity: string;
  dateAnnulment: string;
  state: string;
  stateCode: string;
  baseDocument: string;
  type: string;
  serviceMRFC: string;
  technology: string;
  regionList: string;
  federalDistrictList: string;
  stationCount: number;
}

export interface MPZData {
  sendDate: string;
  receiveDate: string;
  completionDate: string;
  registrationStateCode: string;
  comments: string;
  stateCode: string;
  administrationId: string;
}

export interface CoordinationCoutry {
  id: number;
  ilpId: number;
  orderNumber: number;
  countryId: number;
  countryCode: string;
  countryName: string;
  sendDate: string;
  receiveDate: string;
  comments: string;
  status: string;
  statusCode: string;
  updatedDateTimeName: string;
  createdFromAFrequencyCard: boolean;
}

export interface TabParameters {
  id: number;
  ilpScreenFormCode: string;
  showConclusionListToAnnulTab: boolean;
  showPermissionListToAnnulTab: boolean;
}

export interface CoordinationCoutryParams {
  countryId: number;
  sendDate: string;
  receiveDate: string;
  comments: string;
  statusCode: string;
}

export interface PCTRData {
  techStationId: number;
  stationId: number;
  stationPoint: any;
  notes: string;
  address: string;
  stationState: string;
  reqId: number;
  stationNumber: string;
  etsVidId: string;
  power: number;
  aoguid: string;
  additionAddress: string;
  stationDate: string;
  spsId: number;
  afsHeight: number;
  angleAsimuth: number;
  antennaGain: number;
  antennaLoss: number;
  channels: string;
  freqTransm: number;
  freqReceive: number;
  afsHeightSea: number;
  angleElevation: number;
  apertureAngleH: number;
  apertureAngleV: number;
  classEmi: string;
  polarization: string;
  cellRadius: number;
  stateTransm: string;
  stateReceive: string;
  netState: string;
  state: string;
  freqDate: string;
  powerTransm: number;
  spsNumber: number;
}

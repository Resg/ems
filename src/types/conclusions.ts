export interface ConclusionAdditional {
  id: number;
  conclusionId: number;
  ordinalNumber: number;
  name: string;
  conclusionText: string;
  conclusionTextSort: string;
  permissionText: string;
  permissionTextSort: string;
  templateId: number;
}

export interface ConclusionAppendice {
  id: number;
  name: string;
  text: string;
  createDate: string;
}

export interface ConclusionData {
  requestId: number;
  serviceId: number;
  id: number;
  number: string;
  date: string;
  dateValidity: string;
  statusCode: string;
  text: string;
  typeCode: string;
  signerId: number;
  signer: string;
  status: string;
  type: string;
  createdDateTimeName: string;
  sortId: number;
  sort: string | number;
  isMarine: boolean;
  stateCode: string;
  state: string;
  dateAnnulment: string;
  clientId: number;
  clientName: string;
  annulmentReasonCode: string;
  annulmentReason: string;
  comment: string;
  requestFormClass: string;
  validityPeriodCode: string;
  validityPeriod: string;
}

export interface ConclusionFormType {
  number: string;
  date: string | null;
  dateValidity: string | null;
  contractorIds: number[] | null;
  validityPeriodCode: string;
  statusCode: string;
  typeCode: string;
  sortId: number | null;
  signerId: number | null;
  stateCode: string;
  dateAnnulment: string;
  annulmentReasonCode: string;
  comment: string;
}

export interface ConclusionUpdateData extends ConclusionFormType {
  id: number;
  text: string;
}

export interface ConclusionCreateData extends ConclusionFormType {
  requestId: number;
  isMarine: number | null;
  text: string;
}

export interface ConclusionType {
  id: number;
  request: string;
  number: string;
  date: string;
  dateValidity: string;
  dateAnnulment: string;
  type: string;
  state: string;
  status: string;
  author: string;
}

export interface ConclusionSearchType {
  searchParameters: {
    elasticSearch?: string;
    dateStart?: string;
    dateEnd?: string;
  };
  page: number;
  size: number;
  sort?: string;
}

export interface AdditionalConclusionData {
  id: number;
  conclusionId: number;
  ordinalNumber: number;
  name: string;
  conclusionText: string;
  permissionText: string;
  conclusionTextEdit: string;
  permissionTextEdit: string;
  isActual: boolean;
}

export interface AdditionalConclusionDataTemplate {
  id: number;
  name: string;
  conclusionText: string;
  permissionText: string;
  isActual: boolean;
}

export interface UpdateAdditionalConclusionDataTemplate {
  name: string;
  conclusionText: string;
  permissionText: string;
  isActual: boolean;
}

export interface AdditionalConclusionRequestProps {
  elasticSearch?: string;
  isOnlyActual?: boolean;
}

export interface UpdateAdditionalConclusionData {
  ordinalNumber: number;
  name: string;
  conclusionTextEdit: string;
  permissionTextEdit: string;
}

export interface CreateAdditionalConclusionData {
  id: number;
}

export interface ConclusionFileType {
  id: number;
  version: number;
  title: string;
  name: string;
  size: number;
  contentId: number;
  mimeType: string;
  type: string;
  countPage: number;
  documentId: number;
  templateId: number;
  action: string;
}

export interface ConclusionReviewFormData {
  id: number;
  requestId: number;
  number: string;
  date: string;
  validUntil: string;
  stateCode: string;
  tex: string;
  typeCode: string;
  signerId: number;
  actionId: number;
  sort: number;
  sortName: string;
  requestServiceId: number;
  contractorName: string;
}

export interface ConclusionContractorType {
  id: number;
  conclusionId: number;
  name: string;
  orderNumber: number;
}

export interface ConclusionConditions {
  id: number;
  conclusionId: number;
  ordinalNumber: number;
  name: string;
  conclusionText: string;
  conclusionTextSort: string;
  permissionText: string;
  permissionTextSort: string;
  templateId: number;
}

export interface RequestSolution {
  frequencyRangeId: number;
  frequencyRangeDescription: string;
  parentSolutionId: number;
  parentSolutionNumber: string;
  id: number;
  number: string;
  date: string;
  dateEnd: string | null;
  logId: number;
  logDescription: string;
}

export interface PermissionType {
  id: number;
  number: number;
  date: string;
  dateValidity: string;
  state: string;
  dateAnnulment: string;
  contractorName: string;
  baseDocument: string;
  type: string;
  serviceMRFC: string;
  technology: string;
  stationsCount: number;
  regionList: string[];
  federalDistrictList: string[];
}

export interface PermissionSearchType {
  searchParameters: {
    number?: string;
    dateStart?: string;
    dateEnd?: string;
  };
  page: number;
  size: number;
}

export interface PermissionData {
  id: number;
  number: string;
  date: string;
  dateValidity: string;
  requestId: number;
  stateCode: string;
  stat: string;
  dateAnnulment: string;
  clientId: number;
  clientName: string;
  annulmentReasonCode: string;
  annulmentReason: string;
  comment: string;
  baseDocumentCode: string;
  baseDocument: string;
  type: string;
  baseObjectId: number;
  baseDocumentNumber: string;
  pchtrType: string;
  fsFlag: boolean;
  basePermissionDate: string;
  createdDateTimeName: string;
  editDateTimeName: string;
  isPCHTR: boolean;
  canExtractPermission: boolean;
}

export interface PermissionContractorType {
  id: string | number;
  permissionId: number;
  name: string;
  orderNumber: number;
}

export interface PermissionFormType {
  requestId: number;
  number: string;
  date: string;
  dateValidity: string;
  stateCode: string;
  dateAnnulment: string;
  annulmentReasonCode: string;
  comment: string;
  baseDocumentCode: string;
  baseObjectId: number;
  isAnnulmentBasePermission: boolean;
  contractorIds: number[];
}

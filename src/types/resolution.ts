export interface Resolution {
  ids: number[] | null;
  parentId: number | null;
  documentIds: number[] | null;
  date: string | null;
  authorId: number | null;
  status?: string | null;
  editorId?: number | null;
  executorIds: number[] | null;
  description: string | null;
  isControlled: boolean;
  controllerId: number | null;
  isItemControlled: boolean;
  controlItem: string | null;
  controlPeriod: number | null;
  controlDate: string | null;
  cancelControlDate: string | null;
}

export interface ResolutionPayload {
  ids: number[];
  parentId: number;
  documentIds: number[];
  date: string;
  authorId: number;
  status?: string;
  editorId?: number;
  executorIds: number[];
  description: string;
  isControlled: boolean;
  controllerId: number;
  isItemControlled: boolean;
  controlItem: string;
  controlPeriod: number;
  controlDate: string;
  cancelControlDate: string;
}

export interface DocumentResolution {
  id: number;
  type: number;
  title: string;
  documentId: number;
  createDate: string;
  content: string;
  executionDate: string;
  daysUntilExeutionDate: string;
  editorClerkId: number;
  authorClerkId: number;
  performerClerkId: number;
  statusId: number;
  statusName: string;
  controlFlag: boolean;
  date: string;
  executionTerm: number;
  controlTypeCode: string;
  controlTypeName: string;
  comment: string;
  controlParagraphFlag: boolean;
  controlParagraphNumber: number;
  canExecute: boolean;
  authorName: string;
  performerName: string;
  canEdit: boolean;
  inControl: number;
  canAddSubresolution: boolean;
  canRevokeFromExecution: boolean;
  hasSubresolution: boolean;
  controlCancelDate: string;
  executionControlFlag: boolean;
  parentResolutionId: number;
  parentResolutionTitle: string;
  subResolutionIdsString: number;
  canRevokeToEdit: boolean;
  canSendToAuthor: boolean;
}

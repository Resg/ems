export interface IIncomingRequestColumn {
  key: Exclude<keyof IIncomingRequestRow, 'id' | 'selected'>;
  name: string;
  hidden?: boolean;
}

export interface IIncomingRequestRow {
  id: number;
  type: string;
  author: string;
  status: string;
  planDate: string;
  parentType: string;
  requestNumber: string;
  requestDate: string;
  requestContractor: string;
  requestRegionList: string;
  requestTechnology: string;
  requestManager: string;
  requestStatus: string;
  requestParallelStages: string;
  comments: string;
  requestCommentsForRfg: string;
  selected: boolean;
}

export interface IRequest {
  id: number; // 51998336,
  requestId: number; // 3211285,
  typeId: number; //  100,
  type: string; //  Экспертиза РЧЗ,
  editorId: number; // 25882,
  editor: string; // Васильев Василий Васильевич,
  authorId: number; // 25882,
  author: string; // Васильев Василий Васильевич,
  performerId: number; // 25882,
  performer: string; // Васильев Василий Васильевич,
  confirmerId: number; // 25882,
  confirmer: string; // Васильев Василий Васильевич,
  createDate: string; // 2020-09-29T13:25:14,
  planDate: string; // 2020-09-29T13:25:14,
  prepareDate: string; // 2020-09-29T13:25:14,
  completeDate: string; // 2020-09-29T13:25:14,
  parentType: string; // ""
  statusId: number; // 2,
  statusGroupId: number; // 2,
  status: string; // Готова к выполнению,
  isControllerControl: boolean; // false,
  isResponsiblePerformerControl: boolean; // false,
  requestNumber: string; // 11-3-015361,
  requestDate: string; // 2020-09-29T13:25:14,
  requestContractor: string; // АЭРОНЕТ,
  requestMaterialNumber: string; // 2011тест1,
  requestMaterialDate: number; // 40695,
  requestManager: string; // Васильев Василий Васильевич,
  requestTechnology: string; // Фиксированная спутниковая,
  requestRegionList: string; // Город Москва,
  requestStatus: string; // Анализ документов для получения заключения,
  requestResAddress: string; // Строка,
  hasNotexecutedSubtasks: boolean; // false,
  canExecute: boolean; // true,
  canSendToAuthor: boolean; //// false,
  canReturnToEditor: boolean; // false,
  canReturnToAuthor: boolean; // true,
  canSendToExecution: boolean; // false,
  canReturnToExecution: boolean;
  canReassignPerformer: boolean; // true,
  canDelete: boolean; // false,
  comments: string; // Вх.: 2011тест1,
  requestCommentsForRfg: string; // Строка,
  editClass: string; // tasks.view.forms.matexpert.MatexpertCreation,
  executeClass: string; // tasks.view.forms.matexpert.MatexpertExecute,
  isExternalForm: boolean; // false,
  requestParallelStages: string; // Выставлен счет-1 (паралл.); На согласовании (паралл.)
  selected: boolean;
}

export interface IIncomingRequest {
  data: Array<IRequest>;
  sort: string;
  page: number;
  size: number;
  useCount: boolean;
  totalCount: number;
}

export interface IRequestQueryParameters {
  sort?: string;
  page?: number;
  size?: number;
  useCount?: boolean;
}

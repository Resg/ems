export interface IncomingDraftsData {
  id: number;
  documentId: number;
  typeId: number;
  type: string;
  editorId: number;
  authorId: number;
  author: string;
  performerId: number;
  confirmerId: number;
  confirmer: string;
  editor: string;
  createDate: string;
  planDate: string;
  statusId: number;
  statusGroupId: number;
  status: string;
  documentNumber: string;
  documentLocalNumber: string;
  documentDate: string;
  documentTypeId: number;
  documentType: string;
  resolutionId: number | null;
  rubricList: string;
  documentDescription: string;
  contractorList: string;
  documentCreatedNameDateTime: string;
  canSendToAuthor: boolean;
  canSendToExecution: boolean;
  canDelete: boolean;
  canMarkAsRead: boolean;
  canClose: boolean;
}

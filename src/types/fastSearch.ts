export interface SearchResultItem {
  objectType: string;
  objectId: number;
  objectDate: string;
  description: string;
}

export interface SearchResult {
  data: SearchResultItem[];
  page: number;
  size: number;
  useCount: boolean;
  totalCount: number;
  sort: string;
  pageCount: number;
}

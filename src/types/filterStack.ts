export type FilterChip = {
  id: number;
  name: string;
  formKey: string;
  filter: string;
};

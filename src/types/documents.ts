export type AccessCodeType = 'RES' | 'CON' | 'COM';
export type AccessType =
  | 'Для служебного пользования'
  | 'Конфиденциальный'
  | 'Общий';
export type ControlType = 0 | 1 | 2;

export type CommonDocumentType = {
  templateId: number | null;
  template: string | null;
  actionId: number;
  barcode: string;
  canAddFile: boolean;
  canDeleteFile: boolean;
  canEdit: boolean;
  canEditEds: boolean;
  canEditFile: boolean;
  canEditSignDate: boolean;
  classCode: string | null;
  comments: string | null;
  compound: string;
  contractorDate: string;
  contractorList: string;
  contractorNumber: string;
  copiesCount: number;
  contractorCountry: string;
  createdNameDateTime: string;
  clerkNameSigner: number[];
  clerkNameReceiver: number[];
  clerkNameExe: number;
  clerkNameRegister: number;
  date: string;
  dateChange: string;
  dateCreate: string;
  dateSign: string;
  deliveryType: string; // "Курьер"
  deliveryTypeCode: string; // "MAN"
  description: string;
  detailedTitle: string;
  divisionFullName: string | null;
  divisionId: number; // 7
  divisionKey: string; // "051"
  divisionName: string; // "Отдел документооборота"
  documentAccess: AccessType;
  documentAccessCode: AccessCodeType;
  externalDate: string | null;
  id: number;
  inRoute: boolean;
  internalDateList: any;
  internalDivisionNumber: string; // "26909"
  internalNumberList: any; //
  isActual: boolean;
  isConfidential: boolean;
  isControl: ControlType;
  isControl2: ControlType;
  isCreateRequestEnabled: boolean;
  isEditable: boolean;
  isEds: boolean;
  isInOpenRegistry: boolean;
  isMaketEds: boolean;
  isMoKzRegistration: boolean;
  isRepeated: boolean;
  isRequestManagerAssigned: boolean;
  isSigned: boolean;
  isUrgent: boolean;
  lastChangedNameDateTime: string;
  localNumber: any; //
  location: string;
  mainDocumentId: any; //
  // maket: number | null;
  // maketId: string | null;
  isManualSign: boolean | null;
  moKzId: string | null;
  number: string;
  objectId: string | null;
  objectName: string; // "DOCS_INCOMING"
  objectTypeCode: string; // "DOCS"
  objectType: string;
  outerNumber: string | null;
  performer: any; // PerformerType
  recipientExternal: any; //
  recipientInternalDivisionFullName: any; //
  recipientInternalDivisionId: any; //
  recipientInternalDivisionKey: any; //
  recipientInternalDivisionShortName: any; //
  recipientInternalId: string | null;
  recipientInternalName: any; //
  recipientInternalPhone: any; //
  recipientInternalPost: any; //
  recipientList: string;
  referencePoint: string | null;
  requestList: string;
  requestNumber: any; //
  rubric: string;
  rubricId: number;
  rubricList: string;
  signerClerkId: string | null;
  signerDivisionId: any; //
  signerDivisionKey: any; //
  signerDivisionShortName: any; //
  signerDivivisionFullName: any; //
  signerList: any; //
  signerName: any; //
  signerPhone: any; //
  signerPost: any; //
  status: string; // "Зарегистрирован"
  statusCode: string; // "RE"
  title: string;
  toBeControlled: boolean;
  type: string; // "Входящий"
  typeId: number; // typeId
  viewObjectId: number;
  viewObjectTypeCode: string;
};

export type DocumentOwnerType = {
  baseDocument: string | null;
  clerkId: number;
  clerkName: string;
  clerkPost: string;
  comments: string | null;
  contractorAddress: string | null;
  contractorAddressDeliveryType: string | null;
  contractorAddressDeliveryTypeCode: string | null;
  contractorAddressId: number | null;
  contractorAddressPostIndex: string | null;
  contractorContactId: number | null;
  contractorContactInfo: string | null;
  contractorContactPerson: string | null;
  contractorContactPersonId: number | null;
  contractorDeliveryId: number | null;
  contractorDocumentDate: string;
  contractorDocumentNumber: string;
  contractorFullName: string | null;
  contractorId: number | null;
  contractorName: string | null;
  contractorPersonPost: string | null;
  contractorTypeCode: string | null; // LEG
  copyId?: number;
  copyName: string | null;
  divisionFullName: string;
  divisionId: number;
  divisionKey: string;
  divisionShortName: string;
  documentId: number;
  id: number;
  packetOutgoingDeliveryDate: string | null;
  packetOutgoingLastReturnDate: string | null;
  packetOutgoingPagesCount: number | null;
  packetOutgoingSendDate: string | null;
  packetOutgoingStatus: string | null;
  packetPostNotificationNumber: string | null;
  packetPostReturnDate: string | null;
  personId: number;
  signDate: string;
  signOrder?: number; // 0
  signerName: string | null;
  usage: string; // "Подписывающий"
  usageCode: string; // "SIGNER"
};

export type DocumentFileType = {
  contentId: number;
  id: number;
  lastChangeBy: string;
  lastChangeDate: string;
  mimeType: string; // "pdf"
  name: string;
  size: number;
  type: string; // "R"
  version: number; // 1
};

export type DocumentIncludedFileType = {
  id: number;
  title: string;
  name: string;
  documentId: number;
  version: number; // 1,
  size: number;
  mimeType: string; // "pdf",
  contentId: number;
  countPage: number;
  type: string; // "R",
  isEds: boolean;
  isSigned: boolean;
  isEdsCorrect: boolean;
  toSend: boolean;
  maketId: number | null;
  actionCode: string | null;
  description: string | null;
  lastChangeBy: string;
  lastChangeDate: string;
};

export type DocumentSightingType = {
  taskId: number;
  performerClerkId: number;
  taskDateComplete: string;
  taskPerformerComment: string;
  taskStatusName: string;
  performerFio: string;
};

export type DocumentRubricType = {
  documentId: number;
  id: number;
  name: string;
  documentTypeId: number;
};

export type DocumentHolderType = {
  documentId: number;
  copyId: number;
  isOriginal: boolean;
  orderNumber: number;
  createdDateTimeName: string;
  orderNumberName: string;
};

export type DocumentBoundType = {
  id: number;
  fromDocumentId: number;
  fromDocumentNumber: string;
  fromDocumentDate: string;
  toDocumentId: number;
  fromDocumentInfo: string;
  toDocumentInfo: string;
  toDocumentNumber: string;
  toDocumentDate: number;
  fromType: string;
  fromTypeName: string;
  isnAction: number;
  addedDateTimeName: string;
  actionId: number;
  comments: string;
  toType: string;
  fromDocumentDateSign: string;
  toDocumentDateSign: string;
  toTypeName: string;
};

type InnerAddressType = {
  id?: number;
  divisionId?: number;
  clerkId?: number;
  signOrder?: number; // 0,
  copyId?: number;
};

type OuterAddressType = {
  id: number;
  contractorId: number;
  contractorContactPersonId: number;
  comments: string;
  contractorDeliveryId: number;
  contractorAddressDeliveryTypeCode: string; // "MAN",
  contractorContactId: number;
  packetOutgoingPagesCount: number;
};

type PerformerType = {
  id?: number;
  divisionId?: number;
  clerkId: number;
};

type RegistarType = PerformerType;

type FollowedDocumentType = {
  id: number;
  numberOfCopies: number;
};

type CommonOutputDataType = {
  id: number | string | null;
  title: string;
  typeId: number; // typeId
  classCode: string | null;
  comments: string;
  number: string;
  date: string | null;
  outerNumber: string | null;
  recipientInternalId: string | null; // number ?
  extendedParameters: string | null;
  objectTypeCode: string; // "DOCS"
  objectId: string | null; // number ?
  documentAccessCode: AccessCodeType;
  isControl: ControlType;
  isUrgent: boolean;
  deliveryTypeCode: string; // "Курьер"
  description: string;
  compound: string;
  nulls: string | null;
  divisionId: number;
  referencePoint: string | null;
  templateId: number | null;
  isActual: boolean;
  isManualSign: boolean | null;
  toBeControlled: boolean;
  isConfidential: boolean;
  moKzId: string | null; // number ?
  externalDate: string | null;
  isEds: boolean;
  signerIds: Array<number>;
  rubricIds: Array<number> | string;
};

export type UpdateOutputDataType = CommonOutputDataType & {
  rubricId: number | null;
  signerClerkId: string | null;
  innerAddressees?: Array<InnerAddressType>;
  outerAddressees?: Array<OuterAddressType>;
  performer?: PerformerType;
  registrar?: RegistarType;
};

export type CreateOutputDataType = CommonOutputDataType & {
  externalProcedure: boolean;
  recipientExternal: string | null;
  parameters: string | null;
  innerAddressees?: Array<Pick<InnerAddressType, 'clerkId'>>;
  outerAddressees?: Array<Omit<OuterAddressType, 'id'>>;
  performer: Omit<PerformerType, 'id' | 'divisionId'>;
  registrar: Omit<RegistarType, 'id' | 'divisionId'>;
  requestIds: Array<number>;
  formName: string;
  followedDocuments: Array<FollowedDocumentType>;
  methodOfCreation: string; // "BY_TEMPLATE"
};

//@todo разобраться с типами
export type CreateOutcomingData = {
  typeId: number;
  comments: string | null;
  documentAccessCode: string;
  description: string;
  compound: string;
  templateId: number;
  isConfidential: boolean;
  isEds: boolean;
  signerIds: number[];
  performer: {
    clerkId: number;
  };
  rubricIds: number[];
};

export type DocumentsSearchType = {
  id: number;
  title: string;
  typeId: number;
  type: string;
  date: string;
  comments: string;
  number: string;
  classCode: string;
  outerNumber: string;
  rubricId: number;
  rubric: string;
  statusCode: string;
  status: string;
  objectType: string;
  objectId: string;
  description: string;
  compound: string;
  contractorList: string;
  recipientList: string;
  recipientListDRKK: string;
  channelList: string;
  regionList: string;
  resAddress: string;
  rubricList: string;
  managerList: string;
  countryCodeCoordinationList: string;
  performer: string;
  dateCreate: string;
  dateSign: string;
  isInOpenRegistry: boolean;
  localNumber: string;
  isControl: 0 | 1 | 2;
  isPerform: boolean;
  numberSort: string;
  isRemovable: boolean;
  returnDate: string;
  outgoingDocumentNumber: string;
  contractorNumber: string;
  contractorDate: string;
  contractorCountry: string;
  isEds: boolean;
  divisionFullName: string;
  requestList: string;
};

export type DocumentLinkType = {
  fromDocumentId: string | number;
  toDocumentIds: (string | number)[];
  fromType?: string;
  comments?: string;
};

export type DocumentRequestsType = {
  id: number;
  number: string;
  clientName: string;
  service: string;
  technology: string;
  manager: string;
  status: string;
  channelList: string | null;
  regionList: string;
  resAddress: string | null;
  countryId: string | null;
  country: string | null;
};

export type DocumentFollowedType = {
  addedDateTimeName: string;
  date: string;
  description: string | null;
  comment: string | null;
  composition: string | null;
  сreateDate: string;
  followedDocumentId: number;
  id: number;
  inOpenRegistr: boolean;
  mainDocumentId: number;
  mainDocumentTitle: string;
  maketId: number | null;
  number: string;
  numberLocal: string;
  objectId: string;
  objectType: string;
  pageCount: number;
  performerNameList: string | null;
  resolutionPerformerList: string | null;
  rubricList: string;
  signDate: string | null;
  signerNameList: string | null;
  title: string;
  typeCode: number;
  typeName: string;
};

export type DocumentForTask = {
  mainDocumentId: number;
  id: number;
  number: string;
  title: string;
  typeId: number;
  type: string;
  typeCode: string;
  date: string;
  isMain: boolean;
};

export interface IncludedDocument {
  id: number;
  title: string;
  typeId: number;
  type: string;
  date: string;
  comments: string;
  number: string;
  classCode: string;
  outerNumber: string;
  rubricId: number;
  rubric: string;
  statusCode: string;
  status: string;
  objectTypeCode: string;
  objectId: number;
  description: string;
  compound: string;
  senderList: string;
  receiverList: string;
  rubricList: string;
  performerShortName: string;
  dateCreate: string;
  isControl: boolean;
  isPerformed: boolean;
  dateSign: string;
  isInOpenRegistry: boolean;
  localNumber: string;
  billList: string;
}

export interface ChildDocument {
  parentId: number;
  copyId: number;
  isOriginal: boolean;
  orderNumber: number;
  orderNumberName: string;
  orderNumberShortName: string;
  registryId: number;
  isBusy: boolean;
}

export interface ParentDocument {
  id: number;
  typeId: number;
  title: string;
  number: string;
  localNumber: string;
  date: string;
  statusCode: string;
  type: string;
  description: string;
  comments: string;
  isEds: boolean;
  canAddToRegistry: boolean;
  orderNumberInRegistry: number;
  children?: ChildDocument[];
}

export interface CommonDocumentFormData {
  id: number;
  typeId: number;
  number: string;
  date: string;
  dateCreate: string;
  contractorList: string;
  rubricId: number;
  description: string;
  deliveryType: string;
  documentAccess: AccessType;
  divisionName: string;
  isConfidential: boolean;
  comments: string | null;
  compound: string | null;
  canEdit: boolean;
  dateSign: string;
  documentAccessCode: AccessCodeType;
  isEds: boolean;
  isControl: ControlType;
  isActual: boolean;
  templateId: number | null;
  template: string | null;
  title: string;
  classCode: string | null;
  outerNumber: string | null;
  signerClerkId: string | null;
  recipientInternalId: string | null;
  objectTypeCode: string;
  objectId: string | null;
  objectName: string;
  isUrgent: boolean;
  deliveryTypeCode: string;
  divisionId: number;
  referencePoint: string | null;
  isManualSign: boolean | null;
  toBeControlled: boolean;
  moKzId: string | null;
  externalDate: string | null;
}

export interface ClerkReceiverType {
  clerkId: number;
  clerkName: string;
  comments: string;
  contractorAddress: string;
  contractorAddressDeliveryType: string;
  contractorAddressDeliveryTypeCode: string;
  contractorAddressPostIndex: string;
  contractorContactId: number;
  contractorContactInfo: string;
  contractorContactPerson: string;
  contractorContactPersonId: number;
  contractorDeliveryId: number;
  contractorId: number;
  contractorName: string;
  copyId: number;
  divisionId: number;
  id: number;
  packetOutgoingDeliveryDate: string;
  packetOutgoingPagesCount: number;
  packetOutgoingSendDate: string;
  packetOutgoingStatus: string;
  packetPostNotificationNumber: string;
  packetPostReturnDate: string;
  signOrder: number;
}

export interface HistoryData {
  id?: number;
  copyId: number;
  numberName: string;
  code: string;
  name: string;
  date: string;
  time: string;
  clerkId: number;
  clerkName: string;
  registryId: number;
  registryNum: string;
  registryType: string;
  registryTargetDivision: string;
  registryTargetClerkName: string;
  requestManagerName: string;
  documentHolder: string;
  registryName: string;
  outgoingPackageNumber: string;
  postPackageLocalNumber: string;
  orderNumber: number;
}

export interface DocumentProtocolType {
  id: number;
  sessionId: number;
  date: string;
  type: string;
  information: string;
  documentId: string;
  clerkShortName: string;
  shortReport: string;
}

export interface ChangeDataSignType {
  id: number | string;
  dateSign: string;
}
export type DocumentSightingsType = {
  taskId: number;
  performerClerkId?: number;
  taskDateComplete?: string;
  taskPerformerComment?: string;
  taskStatusName?: string;
  performerFio?: string;
};

export type DocumentOutgoingPacketsType = {
  id: number;
  orderNumber: number;
  countCopies: number;
  number: string;
  localNumber: string;
  componation: number;
  requestNumber: string;
  rubricList: string;
  isEds: boolean;
  toSendFile?: boolean;
  name?: string;
  fileName: string;
  fileId?: number;
  contentFileId?: number;
};

export type DocumentOutgoingPacketsTypeFiles = {
  documentId: number;
  id: number;
  size: number;
  type: string;
  name: string;
  contentId: number;
  toSend: boolean;
};
export type DocumentFavoriteType = {
  id: number;
  title: string;
  typeId: number;
  type: string;
  numbe: string;
  localNumber: string;
  date: string;
  contractorList: string;
  rubricList: string;
  description: string;
  comments: string;
  performer: string;
  statusCode: string;
  status: string;
};

export type DocumentSignedType = {
  hasSightOrSignOnExecution: boolean;
};

export interface DocumentDeliveryType {
  id: number;
  createDate: string;
  changeDate: string;
  closingDate: string;
  deliveryTypeCode: string;
  addressTypeCode: string;
  aoguid: string;
  index: string;
  address: string;
  email: string;
  fax: string;
  editAddress: string;
  comment: string;
  name: string;
  documentId: number;
  sendingDate: string;
  deliveryDate: string;
  localNumber: string;
  isnDowner: number;
  clientId: number;
  clientShortName: string;
  clientFullName: string;
  recipientPersonId: number;
  recipientPersonNameFull: string;
  statusCode: string;
  status: string;
  postPacketId: number;
  deliveryTypeName: string;
  recipientPersonName: string;
  documentTitleList: string;
  documentComposition: string;
  registrId: number;
  sendingTime: string;
  sendingDateTime: string;
  registrationNumber: string;
  countSendingPostPacket: number;
  trustedPersonId: number;
  trustedPersonName: string;
  ownType: string;
  returnPostPacketeDate: string;
  notificationNumber: string;
  commentPostPacket: string;
  documentDate: string;
  signDate: string;
  sortingNumber: string;
  isInOpenRegistr: boolean;
  returnOutgoingPacketLastDate: string;
  countSendingOutgoingPacket: number;
}
export type DocumentPacketsType = {
  id: number;
  createDateTime: string;
  changeDateTime: string;
  deliveryTypeCode: string;
  addressTypeCode: string;
  aoguid: string;
  index: string;
  address: string;
  email: string;
  fax: string;
  isEditedAddress: boolean;
  comments: string;
  name: string;
  documentId: number;
  deliveryDateTime: string;
  localNumber: string;
  documentOwnerId: number;
  contractorId: number;
  contractorShortName: string;
  contractorFullName: string;
  recipientPersonId: number;
  recipientPersonFullName: string;
  statusCode: string;
  status: string;
  postPacketId: number;
  deliveryType: string;
  recipientPersonName: string;
  documentTitleList: string;
  documentCompositionList: string;
  outerRegistryId: number;
  outerRegistryNumber: string;
  outerRegistryDateTime: string;
  sendingDateTime: string;
  countPostPacketSending: number;
  trustedPersonId: number;
  trustedPersonName: string;
  ownershipType: string;
  returnPostPacketDateTime: string;
  notificationNumber: string;
  commentsPostPacket: string;
  innerRegistryId: number;
  innerRegistryNumber: string;
  innerRegistryDateTime: string;
  documentDate: string;
  documentSignDate: string;
  isInOpenRegistry: boolean;
  canEdit: boolean;
  returnOutgoingPacketLastDateTime: string;
  countSending: number;
  countPages: string;
};

export type RegistryPacketsType = {
  id: number;
  number: string;
  type: string;
  dateTime: string;
};

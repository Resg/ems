export interface ColumnSetting {
  ordinalNumber: number;
  name: string;
  width: number | string;
  isVisible: boolean;
  key: string;
}

export interface Tool {
  name: string;
  checked: boolean;
  disabled?: boolean;
}

export interface Sort {
  columnKey: string;
  direction: 'ASC' | 'DESC'; // сортировка: asc - по возрастанию; desc - по убыванию
}

export interface UserTableSettings {
  columnSettings?: ColumnSetting[];
  group?: string[];
  showGroupedQuantity?: boolean;
  sort?: Sort[];
  tools?: Tool[];
}

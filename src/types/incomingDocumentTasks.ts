export type IncomingDocColumnType = {
  key: keyof IncomingDocRowType;
  name: string;
  hidden: boolean;
};
export type IncomingDocRowType = {
  id: number;
  performer: string; // taskActorName
  type: string; // taskTypeName
  documentId: number; // docId
  documentType: string; // docTypeAbbr
  rubricList: string | null;
  documentDescription: string; // docDescr
  resolutionContent: string | null; // resContent
  resolutionId: number | null;
  prepareDate: string; // taskDatePrepared
  selected: boolean;
};

export type IncomingTaskDocColumnType = {
  key: keyof IncomingTaskDocRowType;
  name: string;
  width: string;
  hidden: boolean;
};

export type IncomingTaskDocRowType = {
  id: number;
  type: string;
  status: string;
  performer: string;
  planDate: string;
  performerComment: string;
  author: string;
  documentType: string;
  rubricList: string | null;
  documentDescription: string;
  contractorList: string;
  documentNumber: string;
  documentDate: string;
  documentCorrespondentNumber: string;
  documentCorrespondentDate: string;
  documentStatus: string;
  documentInternalNumber: string;
  documentId: number;
  mainDocumentIdList: string;
  includedDocumentIdList: string;
  documentCreatedNameDateTime: string;
  resolutionId: number | null;
  resolutionContent: string | null;
  resolutionControlParagrafString?: string;
  requestList: string;
  requestServiceGrhcList: string;
  selected: boolean;
};

export interface Task {
  author: string; // "Абрамов Анатолий Юрьевич"
  authorId: number; // 43464
  canClose: boolean; // false
  canContinue: boolean; // false
  canDelete: boolean; // false
  canExecute: boolean; // true
  canMarkAsRead: boolean; // false
  canReassignPerformer: boolean; // false
  canReject: boolean; // false
  canReturnToAuthor: boolean; // true
  canReturnToEditor: boolean; // false
  canReturnToExecution: boolean; // false
  canRevokeFromExecution: boolean; // false
  canSendToAuthor: boolean; // false
  canSendToExecution: boolean; // false
  canSendToWorkFromRevision: boolean; // false
  canTakeInWork: boolean; // true
  completeDate: any; // null
  confirmer: any; // null
  contractorList: any; // null
  controllerId: any; // null
  createDate: string; // "2022-02-02T12:42:09"
  documentCorrespondentDate: any; // null
  documentCorrespondentNumber: any; // null
  documentCreatedNameDateTime: string; // "Матясов Д. С., 02.02.2022 12:42"
  documentDate: any; // null
  documentDescription: string; // "1"
  documentId: number; // 12169940
  documentInternalNumber: string; // "01-00-1205/12169940"
  documentNumber: any; // null
  documentStatus: string; // "На рассмотрении"
  documentType: string; // "Внутренний предприятия"
  documentTypeId: number; // 23
  editor: string; // "Матясов Дмитрий Станиславович"
  editorId: number; // 25882
  hasNotexecutedSubtasks: boolean; // false
  id: number; // 59180566
  includedDocumentIdList: any; // null
  isControllerControl: boolean; // false
  isDocumentUseEds: boolean; // false
  isResolutionExecutionControl: boolean | null; // false
  isResponsiblePerformerControl: boolean; // true
  mainDocumentIdList: string; // "12169940"
  performer: string; // "Матясов Дмитрий Станиславович"
  performerComment: any; // null
  performerId: number; // 25882
  planDate: any; // null
  prepareDate: string; // "2022-02-02T12:42:12"
  requestList: any; // null
  requestServiceGrhcList: any; // null
  resolutionContent: string | null; // "1"
  resolutionControlParagrafString: any; // null
  resolutionId: number | null; // 59180566
  rubricList: string | null; // "Акт о наделении имуществом"
  status: string; // "Готова к выполнению"
  statusGroupId: number; // 2
  statusId: number; // 2
  type: string; // "На исполнение (отв.)"
  typeId: number; // 132
  selected: boolean;
}
export type IncomingDocTasksType = {
  data: Array<Task>;
  page: number;
  pageCount: number;
  queryGUID: string;
  size: number;
  sort: string;
  useCount: boolean;
  totalCount: number;
};

export type IncomingDocSearchParametersType = {
  searchParameters?: {
    documentId?: number;
    document?: {
      isOnlyMain: boolean;
    };
    documentIds?: Array<number>;
  };
  sort?: string;
  page?: number;
  size?: number;
  useCount?: boolean;
};

export interface AdditionStatusRequestProps {
  typeId?: number;
  statusId?: number;
  id?: number;
}

export interface ExecuteRequestProps {
  id: number;
  statusId: number;
  comments: string | null;
  isEds: boolean;
  performerId: number | null;
  termWorkDays: number | null;
  taskParamsComments: string | null;
}

export interface TaskData {
  id: number;
  editorId: number;
  editor: string;
  authorId: number;
  author: string;
  performerId: number;
  performer: string;
  performerShortName: string;
  typeId: number;
  objectTypeCode: string;
  objectId: string;
  isControl: boolean;
  parentId: number;
  previousId: number;
  previousTitle: string;
  title: string;
  term: number;
  comments: string;
  performerComment: string;
  statusId: number;
  status: string;
  continueStatusId: number;
  createDate: string;
  canEditPerformer: boolean;
  canEditAuthor: boolean;
  canEditDateTarget: boolean;
  canEditTaskRelations: boolean;
  canEditComments: boolean;
  canEditNotifyOnEnd: boolean;
  canSendToAuthor: boolean;
  isPerformerCommentRequired: boolean;
  rootObjectId: string;
  isEds: boolean;
  planDate: string;
  editClass: string;
  executeClass: string;
  isExternalForm: boolean;
  externalTaskId: string;
}

export interface updateTaskData {
  id: number;
  title: string;
  comment: string;
  previousId: number;
  term: number;
  performerId: number;
  isControl: boolean;
  authorId: number;
  NULLS: boolean;
  dateTarget: string;
}

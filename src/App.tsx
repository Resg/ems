import { useEffect } from 'react';
import { useSnackbar } from 'notistack';
import { useDispatch, useSelector } from 'react-redux';
import { Route, Routes, useLocation } from 'react-router-dom';

import StyledEngine from '@mui/material/StyledEngineProvider';

import { Container } from './components/Container';
import { EmployeeSearchModal } from './components/EmployeeSearchModal';
import { ExecutionForm } from './components/Execution';
import { Header } from './components/Header';
import { Layout } from './components/Layout';
import { Settings } from './components/Settings';
import Sidebar from './components/Sidebar';
import { SnackbarCloseButton } from './components/SnackbarCloseButton';
import { Routes as PageRoutes } from './constants/routes';
import { ConclusionAppendicesPage } from './pages/ConclusionAppendices';
import { ConclusionPage } from './pages/Conclusions';
import { AdditionalConditionPage } from './pages/Conclusions/AdditionalCondition';
import { AdditionalConditionDictionaryPage } from './pages/Conclusions/AdditionalConditionDictionary';
import { ConclusionReviewPage } from './pages/ConclusionsReview';
import { ConclusionReviewPageById } from './pages/ConclusionsReview/Review';
import { CreateByTemplatePage } from './pages/CreateDocumentByTemplate';
import { DetailedStageDataPage } from './pages/DetailedStageData';
import { DocumentPage } from './pages/documents';
import { DocumentFavoritePage } from './pages/documents/Favorite';
import { DocumentSearchPage } from './pages/documentSearch';
import { DraftsPage } from './pages/Drafts';
import { FastSearchPage } from './pages/FastSearchPage';
import { FileEditor } from './pages/FileEitor';
import { IncomingRequestPage } from './pages/IncomingRequests';
import { IncomingTaskPage } from './pages/IncomingTasks';
import MainPage from './pages/MainPage';
import { MyTasksPage } from './pages/MyTasks';
import { OutgoingPacketPage } from './pages/OutgoingPackets';
import { PermissionPage } from './pages/Permissions';
import { PermissionsSearchPage } from './pages/PermissionsSearch';
import { PersonalSettingsPage } from './pages/PersonalSettings';
import { RegistryPage } from './pages/Registry';
import { RegistrySearchPage } from './pages/RegistrySearch';
import { RequestPage } from './pages/Requests';
import { RequestSearchPage } from './pages/requestSearch';
import { StagePage } from './pages/Stage';
import { SystemPage } from './pages/System';
import { TaskDetailsPage } from './pages/TaskDetails';
import { CompletedTaskPage } from './pages/tasksCompleted';
import { InworkTaskPage } from './pages/TasksInWork';
import { UserService } from './services/auth';
import { resetBreadCrumbs, setOpenFormInNewTab } from './store/utils';
import { addBreadCrumbs, getTitle } from './utils/breadcrumb';
import { RootState } from './store';

function App() {
  const error = useSelector((state: RootState) => state.utils.error);

  const userMessage = useSelector(
    (state: RootState) => state.utils.userMessage
  );

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  useEffect(() => {
    if (!UserService.isLoggedIn()) {
      UserService.doLogin();
    }
  }, []);

  const breadCrumbs = useSelector(
    (state: RootState) => state.utils.breadCrumbs
  );

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const location = useLocation();

  const locationData = useSelector(
    (state: RootState) => state.utils.locationData
  );
  const dispatch = useDispatch();

  useEffect(() => {
    if (error && error.data.showError !== false) {
      const key = enqueueSnackbar(error.data.message, {
        autoHideDuration: null,
        onClick: () => {},
        variant: 'error',
        anchorOrigin: { horizontal: 'right', vertical: 'top' },
        action: (key) => (
          <SnackbarCloseButton snackbarKey={key} errorData={error.data} />
        ),
      });
    }
  }, [enqueueSnackbar, error]);

  useEffect(() => {
    if (userMessage) {
      enqueueSnackbar(userMessage.data.message, {
        autoHideDuration: null,
        onClick: () => {},
        variant: userMessage.data.type,
        anchorOrigin: { horizontal: 'right', vertical: 'top' },
        action: (key) => (
          <SnackbarCloseButton snackbarKey={key} errorData={userMessage.data} />
        ),
      });
    }
  }, [enqueueSnackbar, userMessage]);

  useEffect(() => {
    document.title = getTitle(breadCrumbs);
  }, [breadCrumbs]);

  useEffect(() => {
    const handleCtrlDown = (event: any) => {
      if (event.key === 'Control') {
        dispatch(setOpenFormInNewTab(true));
      }
    };

    const handleCtrlUp = (event: any) => {
      if (event.key === 'Control') {
        dispatch(setOpenFormInNewTab(false));
      }
    };

    document.addEventListener('keydown', handleCtrlDown);
    document.addEventListener('keyup', handleCtrlUp);
  });

  useEffect(() => {
    if (openFormInNewTab) dispatch(resetBreadCrumbs());
    addBreadCrumbs(location, breadCrumbs, locationData, dispatch);
  }, [location, locationData, dispatch, openFormInNewTab]);

  return (
    <StyledEngine injectFirst>
      <Layout>
        <Sidebar />
        <Header />
        <Container>
          <Routes>
            <Route path={PageRoutes.MAIN} element={<MainPage />} />
            <Route
              path={PageRoutes.REQUESTS_TASKS.INCOMING}
              element={<IncomingRequestPage />}
            />
            <Route
              path={PageRoutes.DOCUMENTS_TASKS.MY_TASKS}
              element={<MyTasksPage />}
            />
            <Route
              path={PageRoutes.DOCUMENTS_TASKS.DRAFTS}
              element={<DraftsPage />}
            />
            <Route
              path={PageRoutes.DOCUMENTS_TASKS.INCOMING}
              element={<IncomingTaskPage />}
            />
            <Route
              path={PageRoutes.DOCUMENTS_TASKS.IN_WORK}
              element={<InworkTaskPage />}
            />
            <Route
              path={PageRoutes.DOCUMENTS_TASKS.COMPLETED}
              element={<CompletedTaskPage />}
            />
            <Route
              path={PageRoutes.DOCUMENTS.CREATE_BY_TEMPLATE}
              element={<CreateByTemplatePage />}
            />
            {[
              PageRoutes.DOCUMENTS.DOCUMENT,
              PageRoutes.DOCUMENTS.DOCUMENT_TABS,
              PageRoutes.DOCUMENTS.CREATE,
              PageRoutes.DOCUMENTS.REDIRECT,
            ].map((path: string, index: number) => (
              <Route path={path} element={<DocumentPage />} key={index} />
            ))}
            {[
              PageRoutes.REQUESTS.REQUEST,
              PageRoutes.REQUESTS.REQUEST_TABS,
              PageRoutes.REQUESTS.CREATE,
              PageRoutes.REQUESTS.REDIRECT,
            ].map((path: string, index: number) => (
              <Route path={path} element={<RequestPage />} key={index} />
            ))}
            {[
              PageRoutes.CONCLUSIONS.APPENDICES.APPENDIX,
              PageRoutes.CONCLUSIONS.APPENDICES.APPENDIX_ID,
              PageRoutes.CONCLUSIONS.APPENDICES.VIEW,
              PageRoutes.CONCLUSIONS.APPENDICES.VIEW_ID,
            ].map((path: string, index: number) => (
              <Route
                path={path}
                element={<ConclusionAppendicesPage />}
                key={index}
              />
            ))}
            {[
              PageRoutes.CONCLUSIONS.CONCLUSION,
              PageRoutes.CONCLUSIONS.CONCLUSION_TABS,
              PageRoutes.CONCLUSIONS.CREATE,
              PageRoutes.CONCLUSIONS.REDIRECT,
            ].map((path: string, index: number) => (
              <Route path={path} element={<ConclusionPage />} key={index} />
            ))}
            {[
              PageRoutes.PERMISSIONS.PERMISSION,
              PageRoutes.PERMISSIONS.PERMISSION_TABS,
              PageRoutes.PERMISSIONS.CREATE,
              PageRoutes.PERMISSIONS.REDIRECT,
            ].map((path: string, index: number) => (
              <Route path={path} element={<PermissionPage />} key={index} />
            ))}
            <Route
              path={PageRoutes.DOCUMENTS.SEARCH}
              element={<DocumentSearchPage />}
            />
            <Route path={PageRoutes.FILE_EDITOR} element={<FileEditor />} />
            <Route path={PageRoutes.STAGES} element={<StagePage />} />
            {[
              PageRoutes.REGISTRIES.REGISTRY,
              PageRoutes.REGISTRIES.REGISTRY_TABS,
              PageRoutes.REGISTRIES.CREATE,
              PageRoutes.REGISTRIES.REDIRECT,
            ].map((path: string, index: number) => (
              <Route path={path} element={<RegistryPage />} key={index} />
            ))}
            <Route
              path={PageRoutes.REGISTRIES.INCOMING_DOCUMENTS_EXPECTED}
              element={
                <RegistrySearchPage registryType="REGISTRIES_INCOMING_DOCUMENTS_EXPECTED" />
              }
            />
            <Route
              path={PageRoutes.REGISTRIES.INCOMING_DOCUMENTS_ACCEPTED}
              element={
                <RegistrySearchPage registryType="REGISTRIES_INCOMING_DOCUMENTS_ACCEPTED" />
              }
            />
            <Route
              path={PageRoutes.REGISTRIES.INCOMING_DOCUMENTS_RETURNED}
              element={
                <RegistrySearchPage registryType="REGISTRIES_INCOMING_DOCUMENTS_RETURNED" />
              }
            />
            <Route
              path={PageRoutes.REGISTRIES.INCOMING_PACKETS_EXPECTED}
              element={
                <RegistrySearchPage registryType="REGISTRIES_INCOMING_OUTGOINGPACKET_EXPECTED" />
              }
            />
            <Route
              path={PageRoutes.REGISTRIES.INCOMING_PACKETS_ACCEPTED}
              element={
                <RegistrySearchPage registryType="REGISTRIES_INCOMING_OUTGOINGPACKET_ACCEPTED" />
              }
            />
            <Route
              path={PageRoutes.REGISTRIES.INCOMING_PACKETS_RETURNED}
              element={
                <RegistrySearchPage registryType="REGISTRIES_INCOMING_OUTGOINGPACKET_RETURNED" />
              }
            />
            <Route
              path={PageRoutes.REGISTRIES.OUTCOMING_DOCUMENTS_OPENED}
              element={
                <RegistrySearchPage registryType="REGISTRIES_OUTGOING_DOCUMENTS_OPENED" />
              }
            />
            <Route
              path={PageRoutes.REGISTRIES.OUTCOMING_DOCUMENTS_CLOSED}
              element={
                <RegistrySearchPage registryType="REGISTRIES_OUTGOING_DOCUMENTS_CLOSED" />
              }
            />
            <Route
              path={PageRoutes.REGISTRIES.OUTCOMING_DOCUMENTS_SENT}
              element={
                <RegistrySearchPage registryType="REGISTRIES_OUTGOING_DOCUMENTS_SENT" />
              }
            />
            <Route
              path={PageRoutes.REGISTRIES.OUTCOMING_PACKETS_OPENED}
              element={
                <RegistrySearchPage registryType="REGISTRIES_OUTGOING_OUTGOINGPACKET_OPENED" />
              }
            />
            <Route
              path={PageRoutes.REGISTRIES.OUTCOMING_PACKETS_CLOSED}
              element={
                <RegistrySearchPage registryType="REGISTRIES_OUTGOING_OUTGOINGPACKET_CLOSED" />
              }
            />
            <Route
              path={PageRoutes.REGISTRIES.OUTCOMING_PACKETS_SENT}
              element={
                <RegistrySearchPage registryType="REGISTRIES_OUTGOING_OUTGOINGPACKET_SENT" />
              }
            />
            <Route
              path={PageRoutes.REGISTRIES.ARCHIVE}
              element={<RegistrySearchPage registryType="REGISTRIES_ARCHIVE" />}
            />
            <Route
              path={PageRoutes.CONCLUSIONS.ADDITIONAL.CONDITIONS}
              element={<AdditionalConditionPage />}
            />
            <Route
              path={PageRoutes.REQUESTS.SEARCH}
              element={<RequestSearchPage />}
            />
            <Route
              path={PageRoutes.DETAILED_STAGE_DATA}
              element={<DetailedStageDataPage />}
            />
            <Route
              path={PageRoutes.TASK_DETAILS}
              element={<TaskDetailsPage />}
            />
            {[
              PageRoutes.TASKS.TASK,
              PageRoutes.TASKS.TASK_TABS,
              PageRoutes.TASKS.CREATE,
              PageRoutes.TASKS.REDIRECT,
            ].map((path: string, index: number) => (
              <Route path={path} element={<TaskDetailsPage />} key={index} />
            ))}
            <Route
              path={PageRoutes.CONCLUSIONS.ADDITIONAL.CONDITIONS_DICTIONARIES}
              element={<AdditionalConditionDictionaryPage />}
            />
            <Route
              path={PageRoutes.REGISTRIES.OUTGOING_PACKETS}
              element={<OutgoingPacketPage />}
            />
            <Route
              path={PageRoutes.PERSONAL_SETTINGS}
              element={<PersonalSettingsPage />}
            />
            <Route
              path={PageRoutes.DOCUMENTS.FAVORITE}
              element={<DocumentFavoritePage />}
            />
            <Route
              path={PageRoutes.CONCLUSIONS.REVIEW.CONCLUSION}
              element={<ConclusionReviewPageById />}
            />
            <Route
              path={PageRoutes.CONCLUSIONS.REVIEW.SEARCH}
              element={<ConclusionReviewPage />}
            />
            <Route
              path={PageRoutes.PERMISSIONS.SEARCH}
              element={<PermissionsSearchPage />}
            />
            <Route path={PageRoutes.SYSTEM} element={<SystemPage />} />
            <Route
              path={PageRoutes.CONCLUSIONS.ADDITIONAL.CONDITIONS_VIEW}
              element={<AdditionalConditionPage />}
            />
            <Route
              path={PageRoutes.FAST_SEARCH.SEARCH_RESULT}
              element={<FastSearchPage />}
            />
          </Routes>
          <ExecutionForm />
          <EmployeeSearchModal />
          <Settings />
        </Container>
      </Layout>
    </StyledEngine>
  );
}

export default App;

import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import { Tab, Tabs } from '@mui/material';

import { Page } from '../../../components/Page';
import { TabPanel } from '../../../components/TabPanel';
import { AdditionalCondition } from '../../../containers/Requests/Conclusions/AdditionalConditions';
import { useGetAdditionalConclusionDataQuery } from '../../../services/api/conclusions';
import { useAppDispatch } from '../../../store';
import { setLocationData } from '../../../store/utils';

export const AdditionalConditionPage = () => {
  const { id } = useParams();
  const dispatch = useAppDispatch();

  const { data } = useGetAdditionalConclusionDataQuery(
    { id: Number(id) },
    { skip: !id }
  );

  const [tabValue, setTabValue] = useState(0);
  const handleTabChange = (_e: React.SyntheticEvent, newValue: number) => {
    setTabValue(newValue);
  };

  useEffect(() => {
    dispatch(
      setLocationData({
        name: data ? `Условие: ${data.ordinalNumber}` : 'Условие',
        data: data,
      })
    );
  }, [data]);

  return (
    <Page title={`Условие № ${data?.ordinalNumber}`}>
      <Tabs value={tabValue} onChange={handleTabChange}>
        <Tab label="Общие" value={0} />
      </Tabs>
      <TabPanel value={tabValue} index={0}>
        <AdditionalCondition id={Number(id)} />
      </TabPanel>
    </Page>
  );
};

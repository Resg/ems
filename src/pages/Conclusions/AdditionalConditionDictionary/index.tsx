import React, { useEffect, useState } from 'react';
import { useSearchParams } from 'react-router-dom';

import { Tab, Tabs } from '@mui/material';

import { Page } from '../../../components/Page';
import { TabPanel } from '../../../components/TabPanel';
import { AdditionalConditionDictionary } from '../../../containers/Requests/Conclusions/AdditionalConditionDictionary';
import { useGetConclusionAdditionsDictionaryTemplatesQuery } from '../../../services/api/dictionaries';
import { useAppDispatch } from '../../../store';
import { setLocationData } from '../../../store/utils';

export const AdditionalConditionDictionaryPage = () => {
  const dispatch = useAppDispatch();
  const [searchParams] = useSearchParams();
  const id = Number(searchParams.get('id'));

  const { data } = useGetConclusionAdditionsDictionaryTemplatesQuery(
    { id: id },
    { skip: !id }
  );

  const [tabValue, setTabValue] = useState(0);
  const handleTabChange = (_e: React.SyntheticEvent, newValue: number) => {
    setTabValue(newValue);
  };

  useEffect(() => {
    dispatch(
      setLocationData({
        name: 'Запись справочника дополнительных условий заключений',
        data: data,
      })
    );
  }, [data]);

  return (
    <Page title={`Запись справочника дополнительных условий заключений`}>
      <Tabs value={tabValue} onChange={handleTabChange}>
        <Tab label="Общие" value={0} />
      </Tabs>
      <TabPanel value={tabValue} index={0}>
        <AdditionalConditionDictionary id={id} />
      </TabPanel>
    </Page>
  );
};

import React, { useEffect, useState } from 'react';
import {
  matchPath,
  useLocation,
  useNavigate,
  useParams,
  useSearchParams,
} from 'react-router-dom';

import { Tab, Tabs } from '@mui/material';

import Card from '../../components/Card';
import { TabPanel } from '../../components/TabPanel';
import {
  ConclusionTabs,
  ConclusionTabsLabels,
} from '../../constants/conclusions';
import { Routes } from '../../constants/routes';
import { ConclusionsAdditional } from '../../containers/conclusions/Additional';
import { ConclusionsAppendices } from '../../containers/conclusions/Appendices';
import { ConclusionsCommon } from '../../containers/conclusions/Common';
import { ConclusionDoc } from '../../containers/Requests/Conclusions/Documents';
import { useGetConclusionDataQuery } from '../../services/api/conclusions';
import { useAppDispatch } from '../../store';
import { setLocationData } from '../../store/utils';

import styles from './styles.module.scss';

export const ConclusionPage = () => {
  const { tab } = useParams();
  const [searchParams] = useSearchParams();
  const [tabValue, setTabValue] = useState(tab || ConclusionTabs.COMMON);
  const dispatch = useAppDispatch();
  const location = useLocation();
  const navigate = useNavigate();

  const idForRedirect = Number(searchParams.get('id'));
  const id =
    Number(
      matchPath(Routes.CONCLUSIONS.CONCLUSION_TABS, location.pathname)?.params
        ?.id
    ) ||
    Number(
      matchPath(Routes.CONCLUSIONS.CONCLUSION, location.pathname)?.params?.id
    ) ||
    0;

  const { data } = useGetConclusionDataQuery({ id }, { skip: !id });

  const handleTabChange = (
    _e: React.SyntheticEvent,
    newValue: ConclusionTabs
  ) => {
    setTabValue(newValue);
  };

  useEffect(() => {
    if (id) {
      navigate(
        Routes.CONCLUSIONS.CONCLUSION_TABS.replace(':id', id).replace(
          ':tab',
          tabValue
        )
      );
    }
  }, [tabValue]);

  useEffect(() => {
    if (idForRedirect) {
      navigate(
        Routes.CONCLUSIONS.CONCLUSION_TABS.replace(
          ':id',
          idForRedirect
        ).replace(':tab', tabValue)
      );
    }
  }, [idForRedirect]);

  useEffect(() => {
    dispatch(
      setLocationData({
        name: data ? `Заключение: ${data.number}` : 'Новое заключение',
        data: data,
      })
    );
  }, [data]);

  return (
    <>
      <h1 className={styles.title}>
        {data ? `Заключение № ${data.number}` : 'Новое заключение'}
      </h1>
      <Card className={styles.card}>
        {data ? (
          <Tabs value={tabValue} onChange={handleTabChange}>
            {Object.values(ConclusionTabs).map((tab) => (
              <Tab key={tab} label={ConclusionTabsLabels[tab]} value={tab} />
            ))}
          </Tabs>
        ) : (
          <Tabs value={tabValue} onChange={handleTabChange}>
            <Tab
              key={ConclusionTabs.COMMON}
              label={ConclusionTabsLabels[ConclusionTabs.COMMON]}
              value={ConclusionTabs.COMMON}
            />
          </Tabs>
        )}

        <TabPanel
          value={tabValue}
          index={ConclusionTabs.COMMON}
          className={styles.tabContent}
        >
          <ConclusionsCommon />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={ConclusionTabs.CONDITIONS}
          className={styles.tabContent}
        >
          <ConclusionsAdditional />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={ConclusionTabs.DOCUMENTS}
          className={styles.tabContent}
        >
          <ConclusionDoc id={id} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={ConclusionTabs.PCTR}
          className={styles.tabContent}
        >
          ПЧТР
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={ConclusionTabs.ATTACHMENTS}
          className={styles.tabContent}
        >
          <ConclusionsAppendices />
        </TabPanel>
      </Card>
    </>
  );
};

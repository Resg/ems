import React, { useEffect } from 'react';
import { useSearchParams } from 'react-router-dom';

import { Tab, Tabs } from '@mui/material';

import Card from '../../components/Card';
import { DetailedStageData } from '../../containers/DetailedStageData';
import { useAppDispatch } from '../../store';
import { setLocationData } from '../../store/utils';

import styles from './styles.module.scss';

export const DetailedStageDataPage: React.FC = () => {
  const [searchParams] = useSearchParams();
  const id = Number(searchParams.get('taskId'));
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(setLocationData({ singleName: 'Детальные данные по этапу' }));
  }, []);

  return (
    <>
      <h1>Детальные данные по этапу</h1>
      <Card className={styles.card}>
        <Tabs value={0} className={styles.tabs}>
          <Tab value={0} label="Общие" className={styles.tab} />
        </Tabs>
        <DetailedStageData id={id} />
      </Card>
    </>
  );
};

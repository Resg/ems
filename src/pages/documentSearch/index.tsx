import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { FormikProps } from 'formik';
import { useSnackbar } from 'notistack';
import { useDispatch, useSelector } from 'react-redux';

import {
  ChevronRight,
  ClearAll,
  CorporateFare,
  DeleteSweep,
  Description,
  DoorSliding,
  ExpandMore,
  Logout,
  PlaylistAdd,
  Refresh,
  Search,
  StarOutline,
} from '@mui/icons-material';
import { Button, Tab, Tabs } from '@mui/material';

import Card from '../../components/Card';
import { DataTable } from '../../components/CustomDataGrid';
import { FilterBar, FormValues } from '../../components/FilterBarNew';
import { MenuButtonItem } from '../../components/MenuButton/MenuButtonItem';
import { RoundButton } from '../../components/RoundButton';
import { TabPanel } from '../../components/TabPanel';
import { ToolButton, ToolsPanel } from '../../components/ToolsPanel';
import { MenuToolButton } from '../../components/ToolsPanel/MenuToolButton';
import {
  DocumentClearMap,
  DocumentSearchCols,
  DocumentSearchLabel,
  DocumentSearchTabs,
} from '../../constants/documentSearch';
import { Routes } from '../../constants/routes';
import { Band } from '../../containers/DocumentSearch/DocumentSearchTab/Band';
import { Barcodes } from '../../containers/DocumentSearch/DocumentSearchTab/Barcodes';
import { DocNumbers } from '../../containers/DocumentSearch/DocumentSearchTab/DocNumbers';
import { Document } from '../../containers/DocumentSearch/DocumentSearchTab/Document';
import { InternalResolution } from '../../containers/DocumentSearch/DocumentSearchTab/InternalResolution';
import { Registry } from '../../containers/DocumentSearch/DocumentSearchTab/Registry';
import { ReqNumbers } from '../../containers/DocumentSearch/DocumentSearchTab/ReqNumbers';
import { Request } from '../../containers/DocumentSearch/DocumentSearchTab/Request';
import { Resolution } from '../../containers/DocumentSearch/DocumentSearchTab/Resolution';
import AddToRegistryPopup from '../../containers/Registry/AddToRegistryPopup';
import { useSetDocumentToFavoritesMutation } from '../../services/api/document';
import { useGetDocumentsQuery } from '../../services/api/documentSearch';
import { RootState } from '../../store';
import { setLocationData } from '../../store/utils';
import { documentSearchToRowAdapter } from '../../utils/documentSearch';
import { prepareData } from '../../utils/documentSearch';

import styles from './styles.module.scss';

export const DocumentSearchPage: React.FC = () => {
  const [popupMode, setPopupMode] = useState<'documents' | 'packets' | null>(
    null
  );
  const [setFavorite] = useSetDocumentToFavoritesMutation();
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const [tab, setTab] = useState(DocumentSearchTabs.DOCUMENT);
  const { enqueueSnackbar } = useSnackbar();
  const dispatch = useDispatch();

  const handleChange = useCallback(
    (event: React.SyntheticEvent, value: DocumentSearchTabs) => {
      setTab(value);
    },
    []
  );

  const selectedDocuments = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const filters = useSelector(
    (state: RootState) => state.filters.data['document-search'] || {}
  );

  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const { data, refetch, isLoading } = useGetDocumentsQuery({
    searchParameters: filters,
    page: page,
    size: perPage,
    useCount: true,
  });

  const rows = useMemo(() => {
    return documentSearchToRowAdapter(data?.data);
  }, [data?.data]);

  const clearTab = useCallback(
    (
      setFieldValue: (
        field: string,
        value: any,
        shouldValidate?: boolean
      ) => void
    ) => {
      const fields: { [key: string]: any } = DocumentClearMap[tab];

      for (const key in fields) {
        setFieldValue(key, fields[key]);
      }
    },
    [tab]
  );

  const createFilterButtons = useCallback(
    (
      onSearch: ({ values }: { values: FormValues }) => () => Promise<void>,
      props: FormikProps<FormValues>
    ) => {
      const { resetForm, values, setFieldValue } = props;

      return (
        <>
          <Button
            color="primary"
            variant="contained"
            startIcon={<Search />}
            onClick={onSearch({ values: prepareData(values as FormValues) })}
          >
            Поиск
          </Button>
          <Button
            variant="outlined"
            startIcon={<ClearAll />}
            onClick={() => clearTab(setFieldValue)}
          >
            Очистить вкладку
          </Button>
          <Button
            color="primary"
            variant="outlined"
            startIcon={<DeleteSweep />}
            onClick={() => resetForm()}
          >
            Очистить всё
          </Button>
        </>
      );
    },
    [clearTab]
  );

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const openPage = useCallback(
    (path: string = '') => {
      window.open(`${path}`, openFormInNewTab ? '_blank' : '_self')?.focus();
    },
    [selected, openFormInNewTab]
  );

  const rowDoubleHandler = useCallback(
    (e: any) => {
      if (e.id) {
        window
          .open(
            Routes.DOCUMENTS.DOCUMENT.replace(':id', e.id),
            openFormInNewTab ? '_blank' : '_self'
          )
          ?.focus();
      }
    },
    [openFormInNewTab]
  );

  useEffect(() => {
    dispatch(setLocationData({ menuName: ['Документы', 'Поиск документов'] }));
  }, []);

  const addToFavorite = () => {
    setFavorite({ ids: selectedDocuments }).then((response) => {
      if ('data' in response) {
        enqueueSnackbar('Документ добавлен в избранные документы', {
          autoHideDuration: 3000,
          onClick: () => {},
          variant: 'success',
          anchorOrigin: { horizontal: 'right', vertical: 'top' },
        });
      }
    });
  };

  return (
    <>
      <h1 className={styles.title}>Поиск документов</h1>

      <FilterBar
        formKey="document-search"
        createFilterButtons={createFilterButtons}
        refetchSearch={() => refetch()}
      >
        <Tabs
          value={tab}
          onChange={handleChange}
          scrollButtons
          variant="scrollable"
        >
          <Tab
            label={DocumentSearchLabel[DocumentSearchTabs.DOCUMENT]}
            value={DocumentSearchTabs.DOCUMENT}
          />
          <Tab
            label={DocumentSearchLabel[DocumentSearchTabs.INTERNAL_RESOLUTION]}
            value={DocumentSearchTabs.INTERNAL_RESOLUTION}
          />
          <Tab
            label={DocumentSearchLabel[DocumentSearchTabs.RESOLUTION_VO]}
            value={DocumentSearchTabs.RESOLUTION_VO}
          />
          <Tab
            label={DocumentSearchLabel[DocumentSearchTabs.REGISTRY]}
            value={DocumentSearchTabs.REGISTRY}
          />
          <Tab
            label={DocumentSearchLabel[DocumentSearchTabs.REQUEST]}
            value={DocumentSearchTabs.REQUEST}
          />
          <Tab
            label={
              DocumentSearchLabel[DocumentSearchTabs.LIST_OF_DOCUMENTS_NUMBERS]
            }
            value={DocumentSearchTabs.LIST_OF_DOCUMENTS_NUMBERS}
          />
          <Tab
            label={DocumentSearchLabel[DocumentSearchTabs.LIST_OF_BARCODES]}
            value={DocumentSearchTabs.LIST_OF_BARCODES}
          />
          <Tab
            label={
              DocumentSearchLabel[DocumentSearchTabs.LIST_OF_REQUESTS_NUMBERS]
            }
            value={DocumentSearchTabs.LIST_OF_REQUESTS_NUMBERS}
          />
          <Tab
            label={DocumentSearchLabel[DocumentSearchTabs.BAND]}
            value={DocumentSearchTabs.BAND}
          />
        </Tabs>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={DocumentSearchTabs.DOCUMENT}
        >
          <Document />
        </TabPanel>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={DocumentSearchTabs.INTERNAL_RESOLUTION}
        >
          <InternalResolution />
        </TabPanel>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={DocumentSearchTabs.REQUEST}
        >
          <Request />
        </TabPanel>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={DocumentSearchTabs.RESOLUTION_VO}
        >
          <Resolution />
        </TabPanel>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={DocumentSearchTabs.BAND}
        >
          <Band />
        </TabPanel>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={DocumentSearchTabs.REGISTRY}
        >
          <Registry />
        </TabPanel>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={DocumentSearchTabs.LIST_OF_DOCUMENTS_NUMBERS}
        >
          <DocNumbers />
        </TabPanel>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={DocumentSearchTabs.LIST_OF_BARCODES}
        >
          <Barcodes />
        </TabPanel>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={DocumentSearchTabs.LIST_OF_REQUESTS_NUMBERS}
        >
          <ReqNumbers />
        </TabPanel>
      </FilterBar>
      <Card className={styles.card}>
        <DataTable
          formKey={'document_search'}
          cols={DocumentSearchCols}
          rows={rows}
          onRowSelect={setSelected}
          loading={isLoading}
          onRowDoubleClick={rowDoubleHandler}
          className={styles.table}
          height="fullHeight"
          pagerProps={{
            page,
            setPage,
            perPage,
            setPerPage,
            total: data?.totalCount || 0,
          }}
        >
          <ToolsPanel
            className={styles.panel}
            leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
          >
            <ToolButton
              label={'подробно'}
              startIcon={<ChevronRight />}
              disabled={selectedDocuments.length !== 1}
              onClick={() =>
                selectedDocuments.length &&
                openPage(`/documents?id=${selectedDocuments[0]}`)
              }
              fast
            />
            <MenuToolButton label={'добавить'} endIcon={<ExpandMore />}>
              <MenuButtonItem
                icon={<DoorSliding />}
                linkProps={{
                  to: '/documents?documentType=DOCS_PODR',
                  newTab: true,
                }}
              >
                Внутренний подразделения
              </MenuButtonItem>
              <MenuButtonItem
                icon={<CorporateFare />}
                linkProps={{
                  to: '/documents?documentType=DOCS_PREDPR',
                  newTab: true,
                }}
              >
                Внутренний предприятия
              </MenuButtonItem>
              <MenuButtonItem
                icon={<Logout />}
                linkProps={{
                  to: '/documents?documentType=DOCS_OUTCOMING',
                  newTab: true,
                }}
              >
                Исходящий
              </MenuButtonItem>
              <MenuButtonItem
                icon={<Description />}
                linkProps={{
                  to: '/documents?documentType=DOCS_OUTCOMING_DRKK',
                  newTab: true,
                }}
              >
                ДЭРК
              </MenuButtonItem>
            </MenuToolButton>
            <ToolButton
              label={'Добавить в реестр'}
              startIcon={<PlaylistAdd />}
              onClick={() => setPopupMode('documents')}
              disabled={selectedDocuments.length !== 1}
              fast
            />
            <ToolButton
              label={'добавить в избранное'}
              startIcon={<StarOutline />}
              onClick={addToFavorite}
              disabled={selectedDocuments.length !== 1}
              fast
            />
          </ToolsPanel>
        </DataTable>
        <AddToRegistryPopup
          mode={popupMode}
          setMode={setPopupMode}
          documentIds={selectedDocuments[0]}
        />
      </Card>
    </>
  );
};

import React, { useMemo, useState } from 'react';
import { useSelector } from 'react-redux';

import { Check, Close, Refresh } from '@mui/icons-material';
import { Button, Grid, Stack } from '@mui/material';

import Card from '../../../components/Card';
import { DataTable } from '../../../components/CustomDataGrid';
import { DatePickerInput } from '../../../components/DatePickerInput';
import { FilterBar } from '../../../components/FilterBarNew';
import { Popup } from '../../../components/Popup';
import { RoundButton } from '../../../components/RoundButton';
import { ToolsPanel } from '../../../components/ToolsPanel';
import { ConclusionCols } from '../../../constants/conclusions';
import { useGetConclusionsQuery } from '../../../services/api/conclusions';
import { RootState } from '../../../store';

import styles from '../styles.module.scss';

interface ConclusionSearchProps {
  open: boolean;
  setSearchPopup: (open: 'conclusion' | 'permission' | null) => void;
  onSubmit: (selected: any, selectedRows: string[]) => void;
}

export const ConclusionSearch: React.FC<ConclusionSearchProps> = ({
  open,
  setSearchPopup,
  onSubmit,
}) => {
  const filters = useSelector(
    (state: RootState) => state.filters.data['conclusion-search'] || {}
  );
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);

  const {
    elasticSearch,
    dateStart,
    dateEnd,
  }: {
    elasticSearch?: string;
    dateStart?: string;
    dateEnd?: string;
  } = filters;

  const {
    data: conclusions,
    refetch: refetchConclusions,
    isLoading,
  } = useGetConclusionsQuery(
    {
      searchParameters: {
        elasticSearch: elasticSearch || undefined,
        dateStart: dateStart || undefined,
        dateEnd: dateEnd || undefined,
      },
      page,
      size: perPage,
    },
    {
      skip: !elasticSearch && !dateStart && !dateEnd,
    }
  );

  const [cols, setCols] = useState(ConclusionCols);

  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const selectedRows = useMemo(() => {
    return (
      Object.keys(selected).filter(
        (selectedKey) => selected[Number(selectedKey)]
      ) || []
    );
  }, [selected]);

  return (
    <Popup
      open={open}
      onClose={() => setSearchPopup(null)}
      bar={
        <Stack
          direction="row"
          justifyContent="flex-end"
          spacing={1.5}
          style={{ width: '100%' }}
        >
          <Button
            variant="contained"
            startIcon={<Check />}
            disabled={selectedRows.length !== 1}
            onClick={() => {
              onSubmit(
                conclusions?.data.find(
                  (permission) => permission.id === Number(selectedRows[0])
                ),
                selectedRows
              );
              setSearchPopup(null);
            }}
          >
            Выбрать
          </Button>
          <Button
            variant="outlined"
            startIcon={<Close />}
            onClick={() => setSearchPopup(null)}
          >
            Отмена
          </Button>
        </Stack>
      }
    >
      <>
        <h1 className={styles.header}>Поиск заключения</h1>
        <Card className={styles.card}>
          <FilterBar
            formKey="conclusion-search"
            placeholder="Номер заключения"
            refetchSearch={() => refetchConclusions()}
          >
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <DatePickerInput
                  name="filters.dateStart"
                  label="Начало периода"
                />
              </Grid>
              <Grid item xs={6}>
                <DatePickerInput
                  name="filters.dateEnd"
                  label="Окончание периода"
                />
              </Grid>
            </Grid>
          </FilterBar>
          <div className={styles.subheader}>Список номеров</div>

          <DataTable
            formKey={'common_request_conclusion_search'}
            rows={conclusions?.data || []}
            cols={cols}
            onRowSelect={setSelected}
            loading={isLoading}
            pagerProps={{
              page,
              setPage,
              perPage,
              setPerPage,
              total: conclusions?.totalCount || 0,
            }}
          >
            <ToolsPanel
              className={styles.tools}
              leftActions={
                <RoundButton
                  icon={<Refresh />}
                  onClick={() => refetchConclusions()}
                />
              }
            />
          </DataTable>
        </Card>
      </>
    </Popup>
  );
};

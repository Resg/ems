import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { format } from 'date-fns';
import {
  Form,
  Formik,
  FormikProps,
  FormikValues,
  useFormikContext,
} from 'formik';
import { isEmpty, isEqual } from 'lodash';
import { useSearchParams } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { array, number, object, string } from 'yup';

import { Edit, EditOff, PersonPin, Save } from '@mui/icons-material';
import { Grid, Stack } from '@mui/material';

import { AutoCompleteInput } from '../../components/AutoCompleteInput';
import { CheckboxInput } from '../../components/CheckboxInput';
import { ClerkInput } from '../../components/ClerkInput';
import { ContractorSearchForm } from '../../components/ContractorSearchForm';
import { DatePickerInput } from '../../components/DatePickerInput';
import { DebounceAutoComplete } from '../../components/DebounceAutoComplete';
import Input from '../../components/Input';
import { RoundButton } from '../../components/RoundButton';
import {
  RequestDefaultFields,
  RequestFields,
  RequestLabels,
} from '../../constants/commonRequestForm';
import { Routes } from '../../constants/routes';
import { CollapseStage } from '../../containers/documents/bookmarks/Tasks/CollapseStage';
import { useGetConclusionDataQuery } from '../../services/api/conclusions';
import {
  useGetAsyncContractorsListMutation,
  useGetCommunicationChannelsQuery,
  useGetCounterpartiesAsyncMutation,
  useGetFrequencyBandsQuery,
  useGetMultiplexesQuery,
  useGetNetCategoriesQuery,
  useGetNetTypesQuery,
  useGetProgramsQuery,
  useGetRadioServiceQuery,
  useGetRegionsQuery,
  useGetRequestTypesQuery,
  useGetServiceMRFCQuery,
  useGetSSEQuery,
  useGetTechnologiesQuery,
  useGetTechnologiesRKNQuery,
} from '../../services/api/dictionaries';
import { useGetDocumentByIdQuery } from '../../services/api/document';
import { useGetPermissionDataQuery } from '../../services/api/permissions';
import {
  useCreateRequestMutation,
  useGetCommonDataQuery,
  useGetDefultDataQuery,
  useSaveRequestMutation,
} from '../../services/api/request';
import {
  FrequencyBandSearchType,
  NetTypesParamsType,
  TechologyParamsType,
} from '../../types/dictionaries';
import { Counterparty } from '../../types/dictionaries';
import { prepareData } from '../../utils/commonRequestForm';

import { ConclusionSearch } from './ConclusionSearch';
import { PermissionSearch } from './PermissionSearch';
import { validationSchema } from './validation';

import styles from './styles.module.scss';

interface CommonRequestProps {
  typeId?: number;
  id: number;
}

export const CommonRequestForm: React.FC<CommonRequestProps> = ({
  typeId,
  id,
}) => {
  const [searchParams] = useSearchParams();
  const incomingDocumentId = Number(searchParams.get('incomingDocumentId'));
  const formRef = useRef<FormikProps<FormikValues>>(null);
  const selectedRadioService = formRef.current?.values.radioService;
  const selectedServiceMRFC = formRef.current?.values.serviceMRFC;

  const [editMode, setEditMode] = useState<boolean>(!id);
  const [stateValidationSchema, setStateValidationSchema] =
    useState<Record<string, any>>(validationSchema);
  const [freguencyParams, setFreguencyParams] =
    useState<FrequencyBandSearchType>({});
  const [netTypesParams, setNetTypesParams] = useState<NetTypesParamsType>({});
  const [technologyParams, setTechnologyParams] = useState<TechologyParamsType>(
    {}
  );
  const [channelParams, setChannelParams] = useState<{ bandTx?: number }>({});
  const [inputValue, setInputValue] = useState<string>('');
  const [openPopup, setOpenPopup] = useState<boolean>(false);
  const documentId = useRef<string>('');
  const [openModal, setOpenModal] = useState(false);
  const [getContractorsList] = useGetAsyncContractorsListMutation();

  const [searchPopup, setSearchPopup] = useState<
    'conclusion' | 'permission' | null
  >(null);

  const [openEmployeeSearchModal, setOpenEmployeeSearchModal] = useState(true);

  const handleOpen = useCallback(() => {
    if (inputValue.length > 2) {
      setOpenPopup(true);
    }
  }, [inputValue.length]);

  const handleInputChange = (
    _event: React.SyntheticEvent,
    newValue: string
  ) => {
    setInputValue(newValue);
    if (newValue.length > 2) {
      setOpenPopup(true);
    } else {
      setOpenPopup(false);
    }
  };

  const [createRequest] = useCreateRequestMutation();
  const [saveRequest] = useSaveRequestMutation();
  const { data } = useGetCommonDataQuery({ id });
  const [getCounterparties] = useGetCounterpartiesAsyncMutation();
  const { data: channels } = useGetCommunicationChannelsQuery(channelParams);
  const { data: requestTypes } = useGetRequestTypesQuery();
  const { data: radioServices } = useGetRadioServiceQuery({
    serviceMRFCId: selectedServiceMRFC,
  });
  const { data: serviceMRFC } = useGetServiceMRFCQuery({
    radioservice: selectedRadioService,
  });

  const { data: sse } = useGetSSEQuery();
  const { data: multiplexes } = useGetMultiplexesQuery();
  const { data: programs } = useGetProgramsQuery();
  const { data: netCategories } = useGetNetCategoriesQuery();
  const { data: techologiesRKN } = useGetTechnologiesRKNQuery();
  const { data: regions } = useGetRegionsQuery();
  const { data: defaultValues } = useGetDefultDataQuery({ incomingDocumentId });
  const { data: generalDateAboutIncomingDocument } = useGetDocumentByIdQuery({
    id: incomingDocumentId,
  });

  const { data: netTypes } = useGetNetTypesQuery(
    { ...netTypesParams },
    { skip: isEmpty(netTypesParams) }
  );

  const { data: frequencyBands } = useGetFrequencyBandsQuery(
    { ...freguencyParams },
    { skip: isEmpty(freguencyParams) }
  );

  const { data: frequencyBandsTx } = useGetFrequencyBandsQuery(
    { ...freguencyParams, direction: 'tx' },
    { skip: isEmpty(freguencyParams) }
  );

  const { data: frequencyBandsRx } = useGetFrequencyBandsQuery(
    { ...freguencyParams, direction: 'rx' },
    { skip: isEmpty(freguencyParams) }
  );

  const { data: technologies } = useGetTechnologiesQuery(technologyParams, {
    skip: isEmpty(technologyParams),
  });

  const { data: selectedPermission } = useGetPermissionDataQuery(
    { id: data?.permission },
    { skip: !data?.permission }
  );

  const { data: selectedConclusion } = useGetConclusionDataQuery(
    { id: data?.conclusion },
    { skip: !data?.conclusion }
  );

  const usePrevious = (value: string) => {
    const ref = useRef('');
    useEffect(() => {
      ref.current = value;
    });
    return ref.current;
  };

  const FormDataUpdater = () => {
    const { values = {} as any, setFieldValue } = useFormikContext();
    const radioService = values[RequestFields.RADIO_SERVICE];
    const serviceMrfc = values[RequestFields.SERVICE_MRFC];
    const technology = values[RequestFields.TECHNOLOGY];
    const docType = values[RequestFields.DOCUMENT_TYPE];
    const bandTx = values[RequestFields.CHANNEL_RANGE];
    const previousDocType = usePrevious(docType);

    const selectedTechnology = technologies?.data?.find(
      (technology) => technology.id === values[RequestFields.TECHNOLOGY]
    );

    const selectedRadioService = radioServices?.find(
      (service) => service.id === values[RequestFields.RADIO_SERVICE]
    );

    const selectedServiceMRFC = serviceMRFC?.find(
      (service) => service.id === values[RequestFields.SERVICE_MRFC]
    );

    useEffect(() => {
      const newSchema: Record<string, any> = { ...validationSchema };
      if (
        selectedTechnology?.useBandChannel === false &&
        selectedTechnology?.useBandTx === true &&
        selectedTechnology?.useBandRx === false
      ) {
        newSchema[RequestFields.FREQUENCY_RANGE] = number()
          .nullable()
          .required('Обязательное поле');
      }
      if (selectedTechnology?.useBandTx && selectedTechnology?.useBandRx) {
        newSchema[RequestFields.FREQUENCY_RANGE_RX] = number()
          .nullable()
          .required('Обязательное поле');
      }
      if (selectedTechnology?.useBandTx && selectedTechnology?.useBandRx) {
        newSchema[RequestFields.FREQUENCY_RANGE_TX] = number()
          .nullable()
          .required('Обязательное поле');
      }
      if (selectedServiceMRFC?.useTechnologyStandardRKN) {
        newSchema[RequestFields.TECHNOLOGY_RMN] = string()
          .nullable()
          .required('Обязательное поле');
      }
      if (selectedRadioService?.useNetCategory) {
        newSchema[RequestFields.NET_CATEGORY] = number()
          .nullable()
          .required('Обязательное поле');
      }
      if (selectedRadioService?.useTechnology) {
        newSchema[RequestFields.TECHNOLOGY] = number()
          .nullable()
          .required('Обязательное поле');
      }
      if (selectedRadioService?.regionOptional === false) {
        newSchema[RequestFields.REGION] = array()
          .of(string())
          .nullable()
          .min(1, 'Обязательное поле');
      }

      setStateValidationSchema((prevState) => {
        return isEqual(Object.keys(prevState), Object.keys(newSchema))
          ? prevState
          : newSchema;
      });
    }, [technology, radioService]);

    useEffect(() => {
      if (bandTx) {
        setChannelParams((prevState) => {
          const newState = {
            bandTx,
          };
          return isEqual(newState, prevState) ? prevState : newState;
        });
      }
    }, [bandTx]);

    useEffect(() => {
      setFreguencyParams((prevState) => {
        const newState = {
          radioService,
          serviceMRFC: serviceMrfc,
          technology,
        };
        return isEqual(newState, prevState) ? prevState : newState;
      });

      setTechnologyParams((prevState) => {
        const newState = {
          radioService,
          serviceMRFC: serviceMrfc,
        };
        return isEqual(newState, prevState) ? prevState : newState;
      });
    }, [radioService, serviceMrfc, technology]);

    useEffect(() => {
      setNetTypesParams((prevState) => {
        const newState = {
          serviceMrfc,
        };
        return isEqual(newState, prevState) ? prevState : newState;
      });
    }, [serviceMrfc]);

    useEffect(() => {
      if (
        docType === 'PERMISSION' &&
        previousDocType !== docType &&
        previousDocType !== ''
      ) {
        setSearchPopup('permission');
      }

      if (
        docType === 'CONCLUSION' &&
        previousDocType !== docType &&
        previousDocType !== ''
      ) {
        setSearchPopup('conclusion');
      }
    }, [docType]);

    useEffect(() => {
      if (defaultValues && incomingDocumentId && !values.client) {
        setFieldValue(RequestFields.CLIENT, defaultValues.client);
      }
    }, [defaultValues, incomingDocumentId]);

    return null;
  };

  const navigate = useNavigate();
  const handleSubmit = async (values = {} as any) => {
    const formData = prepareData(values, documentId.current);

    let response;
    if (data && data.id) {
      response = await saveRequest({ data: formData, id: data.id });
    } else {
      response = await createRequest({
        ...formData,
        incomingDocument: incomingDocumentId,
      });
    }
    if ('data' in response) {
      navigate(Routes.REQUESTS.REQUEST.replace(':id', response.data.id));
      window.location.reload();
    }
  };

  const initialValues = useMemo(() => {
    return {
      ...(data || RequestDefaultFields),
      document: selectedPermission?.number || selectedConclusion?.number || '',
      documentType: data?.conclusion
        ? 'CONCLUSION'
        : data?.permission
        ? 'PERMISSION'
        : '',
      date: data?.date || format(new Date(), 'yyyy-MM-dd'),
    };
  }, [data, selectedPermission, selectedConclusion]);

  const getOptions = useCallback(async (value: string) => {
    if (value) {
      const response = await getContractorsList({
        documentTypeId: typeId,
        name: value,
        isOnlyActual: true,
      });
      if ('data' in response) {
        return response.data.data.map((option: Record<string, any>) => {
          return {
            label: option.name,
            value: option.id,
          };
        });
      }
      return [];
    }
    return [];
  }, []);

  const getValueOptions = useCallback(
    async (ids: number[]) => {
      if (ids && ids.length) {
        const counterparties = await getCounterparties({ ids });
        if ('data' in counterparties) {
          return (
            counterparties?.data.data.map((option: Counterparty) => {
              return {
                label: option.name,
                value: option.id,
              };
            }) || []
          );
        } else {
          return [];
        }
      }
      return [];
    },
    [getCounterparties]
  );

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={object(stateValidationSchema)}
      enableReinitialize
      onSubmit={handleSubmit}
      innerRef={formRef}
    >
      {({
        handleChange,
        handleSubmit,
        values,
        setFieldValue,
        errors,
        handleReset,
      }) => {
        const showTypeDocument = requestTypes?.find(
          (type) =>
            type.id == values[RequestFields.REQUEST_TYPE] && type.reissuance
        );

        const selectedTechnology = technologies?.data?.find(
          (technology) => technology.id === values[RequestFields.TECHNOLOGY]
        );

        const selectedServiceMRFC = serviceMRFC?.find(
          (service) => service.id === values[RequestFields.SERVICE_MRFC]
        );

        const selectedRadioService = radioServices?.find(
          (service) => service.id === values[RequestFields.RADIO_SERVICE]
        );

        const setDocumentField = (selected: any, selectedRows: string[]) => {
          setFieldValue(RequestFields.DOCUMENT, selected.number);
          documentId.current = selected.id;
        };

        const handleAdd = (value: number[]) => {
          if (values[RequestFields.CLIENT]) {
            setFieldValue(RequestFields.CLIENT, [
              ...(values[RequestFields.CLIENT] as number[]),
              ...value,
            ]);
          } else {
            setFieldValue(RequestFields.CLIENT, value);
          }
        };

        const handleRequestTypeChange = () => {
          setFieldValue(RequestFields.DOCUMENT_TYPE, null);
          setFieldValue(RequestFields.DOCUMENT, null);
        };

        const handleChannelRangeChange = () => {
          setFieldValue(RequestFields.CHANNEL, []);
        };
        return (
          <>
            <Form>
              <Stack className={styles.tabBar}>
                {editMode ? (
                  <>
                    <RoundButton
                      onClick={() => handleSubmit()}
                      icon={<Save />}
                    />
                    <RoundButton
                      onClick={() => {
                        handleReset();
                        setEditMode(false);
                      }}
                      icon={<EditOff />}
                    />
                  </>
                ) : (
                  <RoundButton
                    onClick={() => setEditMode(true)}
                    icon={<Edit />}
                  />
                )}
              </Stack>
              <Grid container spacing={2}>
                <Grid item xs={4}>
                  <Input
                    name={RequestFields.NUMBER}
                    label={RequestLabels[RequestFields.NUMBER]}
                    value={values[RequestFields.NUMBER]}
                    onChange={handleChange}
                    disabled
                  />
                </Grid>
                <Grid item xs={4}>
                  <DatePickerInput
                    name={RequestFields.DATE}
                    label={RequestLabels[RequestFields.DATE]}
                    disabled={!editMode}
                    required
                  />
                </Grid>
                <Grid item xs={4}>
                  <AutoCompleteInput
                    name={RequestFields.REQUEST_TYPE}
                    label={RequestLabels[RequestFields.REQUEST_TYPE]}
                    options={
                      requestTypes?.map((type) => {
                        return { value: type.id, label: type.name };
                      }) || []
                    }
                    disabled={!editMode}
                    onInputChange={handleRequestTypeChange}
                    required
                  />
                </Grid>
                {showTypeDocument && (
                  <>
                    <Grid item xs={6}>
                      <AutoCompleteInput
                        name={RequestFields.DOCUMENT_TYPE}
                        label={RequestLabels[RequestFields.DOCUMENT_TYPE]}
                        options={[
                          { value: 'CONCLUSION', label: 'Заключение' },
                          { value: 'PERMISSION', label: 'Разрешение' },
                        ]}
                        disabled={!editMode}
                        required
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Input
                        name={RequestFields.DOCUMENT}
                        label={RequestLabels[RequestFields.DOCUMENT]}
                        value={values[RequestFields.DOCUMENT]}
                        onChange={handleChange}
                        disabled
                        required
                      />
                    </Grid>
                  </>
                )}
                <Grid item xs={12} className={styles.container}>
                  <DebounceAutoComplete
                    name={RequestFields.CLIENT}
                    label={RequestLabels[RequestFields.CLIENT]}
                    getOptions={getOptions}
                    getValueOptions={getValueOptions}
                    multiple
                    disabled={!editMode}
                    required
                  />
                  <RoundButton
                    icon={<PersonPin />}
                    onClick={() => setOpenModal(true)}
                    disabled={!editMode}
                  />

                  <ContractorSearchForm
                    openClerkModal={openModal}
                    onClose={() => setOpenModal(false)}
                    addDoc={handleAdd}
                  />
                </Grid>

                <Grid item xs={6}>
                  <AutoCompleteInput
                    name={RequestFields.RADIO_SERVICE}
                    label={RequestLabels[RequestFields.RADIO_SERVICE]}
                    options={
                      radioServices?.map((service) => {
                        return { value: service.id, label: service.name };
                      }) || []
                    }
                    disabled={!editMode}
                    required
                  />
                </Grid>
                <Grid item xs={6}>
                  <AutoCompleteInput
                    name={RequestFields.SERVICE_MRFC}
                    label={RequestLabels[RequestFields.SERVICE_MRFC]}
                    options={
                      serviceMRFC?.map((service) => {
                        return { value: service.id, label: service.name };
                      }) || []
                    }
                    disabled={!editMode}
                    required
                  />
                </Grid>
                <Grid
                  item
                  xs={12}
                  hidden={!selectedRadioService?.useTechnology}
                >
                  <AutoCompleteInput
                    name={RequestFields.TECHNOLOGY}
                    label={RequestLabels[RequestFields.TECHNOLOGY]}
                    options={
                      technologies?.data?.map((technology) => {
                        return {
                          value: technology.id,
                          label: technology.name,
                        };
                      }) || []
                    }
                    disabled={!editMode}
                    required
                  />
                </Grid>

                {
                  <Grid
                    item
                    xs={12}
                    hidden={!selectedServiceMRFC?.useTechnologyStandardRKN}
                  >
                    <AutoCompleteInput
                      name={RequestFields.TECHNOLOGY_RMN}
                      label={RequestLabels[RequestFields.TECHNOLOGY_RMN]}
                      options={
                        techologiesRKN?.map((technology) => {
                          return {
                            value: technology.id,
                            label: technology.name,
                          };
                        }) || []
                      }
                      disabled={!editMode}
                      required
                    />
                  </Grid>
                }
                <Grid
                  item
                  xs={12}
                  hidden={
                    !(
                      selectedTechnology?.useBandChannel === false &&
                      selectedTechnology?.useBandTx === true &&
                      selectedTechnology?.useBandRx === false
                    )
                  }
                >
                  <AutoCompleteInput
                    name={RequestFields.FREQUENCY_RANGE}
                    label={RequestLabels[RequestFields.FREQUENCY_RANGE]}
                    options={
                      frequencyBands?.map((band) => {
                        return {
                          value: band.id,
                          label: band.name,
                        };
                      }) || []
                    }
                    disabled={!editMode}
                    required
                  />
                </Grid>
                <Grid
                  item
                  xs={6}
                  hidden={
                    !(
                      selectedTechnology?.useBandTx &&
                      selectedTechnology?.useBandRx
                    )
                  }
                >
                  <AutoCompleteInput
                    name={RequestFields.FREQUENCY_RANGE_TX}
                    label={RequestLabels[RequestFields.FREQUENCY_RANGE_TX]}
                    options={
                      frequencyBandsTx?.map((band) => {
                        return {
                          value: band.id,
                          label: band.name,
                        };
                      }) || []
                    }
                    disabled={!editMode}
                    required
                  />
                </Grid>

                <Grid
                  item
                  xs={6}
                  hidden={
                    !(
                      selectedTechnology?.useBandTx &&
                      selectedTechnology?.useBandRx
                    )
                  }
                >
                  <AutoCompleteInput
                    name={RequestFields.FREQUENCY_RANGE_RX}
                    label={RequestLabels[RequestFields.FREQUENCY_RANGE_RX]}
                    options={
                      frequencyBandsRx?.map((band) => {
                        return {
                          value: band.id,
                          label: band.name,
                        };
                      }) || []
                    }
                    disabled={!editMode}
                    required
                  />
                </Grid>
                <Grid
                  item
                  xs={12}
                  hidden={!selectedTechnology?.useFrequencyList}
                >
                  <Input
                    name={RequestFields.FREQUENCY}
                    label={RequestLabels[RequestFields.FREQUENCY]}
                    value={values[RequestFields.FREQUENCY]}
                    onChange={handleChange}
                    disabled={!editMode}
                  />
                </Grid>
                <Grid
                  item
                  xs={12}
                  hidden={
                    selectedTechnology?.useBandChannel === undefined
                      ? true
                      : !selectedTechnology?.useBandChannel
                  }
                >
                  <AutoCompleteInput
                    name={RequestFields.CHANNEL_RANGE}
                    label={RequestLabels[RequestFields.CHANNEL_RANGE]}
                    options={
                      frequencyBands?.map((band) => {
                        return {
                          value: band.id,
                          label: band.name,
                        };
                      }) || []
                    }
                    disabled={!editMode}
                    onInputChange={handleChannelRangeChange}
                  />
                </Grid>
                <Grid item xs={12} hidden={!selectedTechnology?.useChannelList}>
                  <AutoCompleteInput
                    name={RequestFields.CHANNEL}
                    label={RequestLabels[RequestFields.CHANNEL]}
                    options={
                      channels?.map((channel) => {
                        return {
                          value: channel.id,
                          label: channel.description,
                        };
                      }) || []
                    }
                    multiple
                    disabled={!editMode}
                  />
                </Grid>
                <Grid
                  item
                  xs={12}
                  hidden={!selectedRadioService?.useNetCategory}
                >
                  <AutoCompleteInput
                    name={RequestFields.NET_CATEGORY}
                    label={RequestLabels[RequestFields.NET_CATEGORY]}
                    options={
                      netCategories?.map((type) => {
                        return {
                          value: type.id,
                          label: type.name,
                        };
                      }) || []
                    }
                    disabled={!editMode}
                    required
                  />
                </Grid>
                <Grid item xs={12}>
                  <AutoCompleteInput
                    name={RequestFields.NET_TYPE}
                    label={RequestLabels[RequestFields.NET_TYPE]}
                    options={
                      netTypes?.map((type) => {
                        return {
                          value: type.id,
                          label: type.name,
                        };
                      }) || []
                    }
                    disabled={!editMode}
                  />
                </Grid>
                <Grid item xs={12} hidden={!selectedRadioService?.useProgram}>
                  <AutoCompleteInput
                    name={RequestFields.PROGRAM}
                    label={RequestLabels[RequestFields.PROGRAM]}
                    options={
                      programs?.map((program) => {
                        return {
                          value: program.id,
                          label: program.name,
                        };
                      }) || []
                    }
                    disabled={!editMode}
                  />
                </Grid>
                <Grid item xs={12} hidden={!selectedServiceMRFC?.useMultiplex}>
                  <AutoCompleteInput
                    name={RequestFields.MULTIPLEX}
                    label={RequestLabels[RequestFields.MULTIPLEX]}
                    options={
                      multiplexes?.map((multiplex) => {
                        return {
                          value: multiplex.code,
                          label: multiplex.value,
                        };
                      }) || []
                    }
                    disabled={!editMode}
                  />
                </Grid>
                <Grid item xs={12}>
                  <AutoCompleteInput
                    name={RequestFields.REGION}
                    label={RequestLabels[RequestFields.REGION]}
                    options={
                      regions?.map((region) => {
                        return {
                          value: region.aoguid,
                          label: region.name,
                        };
                      }) || []
                    }
                    multiple
                    required={!selectedRadioService?.regionOptional}
                    disabled={!editMode}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Input
                    name={RequestFields.ADDRESS}
                    label={RequestLabels[RequestFields.ADDRESS]}
                    value={values[RequestFields.ADDRESS]}
                    onChange={handleChange}
                    disabled={!editMode}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Input
                    name={RequestFields.COMMENTARY_RFG}
                    label={RequestLabels[RequestFields.COMMENTARY_RFG]}
                    value={values[RequestFields.COMMENTARY_RFG]}
                    onChange={handleChange}
                    multiline
                    disabled={!editMode}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Input
                    name={RequestFields.COMMENTARY}
                    label={RequestLabels[RequestFields.COMMENTARY]}
                    value={values[RequestFields.COMMENTARY]}
                    onChange={handleChange}
                    multiline
                    disabled={!editMode}
                  />
                </Grid>
                <Grid item xs={12}>
                  <AutoCompleteInput
                    name={RequestFields.SSE}
                    label={RequestLabels[RequestFields.SSE]}
                    options={
                      sse?.data.map((event) => {
                        return { value: event.code, label: event.value };
                      }) || []
                    }
                    disabled={!editMode}
                  />
                </Grid>
                <Grid item xs={6} className={styles.lowerGrid}>
                  <ClerkInput
                    name={RequestFields.MANAGER}
                    label={RequestLabels[RequestFields.MANAGER]}
                    disabled={!editMode}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Input
                    name={RequestFields.LAST_STAGE}
                    label={RequestLabels[RequestFields.LAST_STAGE]}
                    value={values[RequestFields.LAST_STAGE] || ''}
                    disabled
                  />
                </Grid>
              </Grid>

              <CollapseStage open={false}>
                <Grid container spacing={2}>
                  <Grid item xs={6} className={styles.upperGrid}>
                    <Input
                      name={RequestFields.CREATOR_DATE_NAME}
                      label={RequestLabels[RequestFields.CREATOR_DATE_NAME]}
                      value={values[RequestFields.CREATOR_DATE_NAME]}
                      disabled
                    />
                  </Grid>
                  <Grid item xs={6} className={styles.upperGrid}>
                    <Input
                      name={RequestFields.EDITOR_DATE_NAME}
                      label={RequestLabels[RequestFields.EDITOR_DATE_NAME]}
                      value={values[RequestFields.EDITOR_DATE_NAME]}
                      disabled
                    />
                  </Grid>
                  <Grid item xs={1}>
                    <CheckboxInput
                      name={RequestFields.USI}
                      label={RequestLabels[RequestFields.USI]}
                      disabled={!editMode}
                    />
                  </Grid>
                  <Grid item xs={11}>
                    <CheckboxInput
                      name={RequestFields.DYNAMIC}
                      label={RequestLabels[RequestFields.DYNAMIC]}
                      disabled={!editMode}
                    />
                  </Grid>
                  <Grid item xs={12} className={styles.fieldsHeader}>
                    материалы обращения
                  </Grid>
                  <Grid item xs={6}>
                    <Input
                      name={RequestFields.INCOMING_DOCUMENT}
                      label={RequestLabels[RequestFields.INCOMING_DOCUMENT]}
                      value={
                        data
                          ? `№${data.materialIncomingNumber || ''}, ${
                              data.materialIncomingDate?.toLocaleString() || ''
                            }`
                          : generalDateAboutIncomingDocument
                          ? `№${
                              generalDateAboutIncomingDocument.number || ''
                            }, ${
                              generalDateAboutIncomingDocument.date?.toLocaleString() ||
                              ''
                            }`
                          : ''
                      }
                      disabled
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <Input
                      name={RequestFields.OUTGOING_DOCUMENT}
                      label={RequestLabels[RequestFields.OUTGOING_DOCUMENT]}
                      value={
                        data
                          ? `№${data.materialOuterNumber || ''}, ${
                              data.materialOuterDate?.toLocaleString() || ''
                            }`
                          : ''
                      }
                      disabled
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Input
                      name={RequestFields.MATERIAL_CREATOR_DATE_NAME}
                      label={
                        RequestLabels[RequestFields.MATERIAL_CREATOR_DATE_NAME]
                      }
                      value={(data && data.materialCreatorDateName) || ''}
                      disabled
                    />
                  </Grid>
                </Grid>
              </CollapseStage>
              <FormDataUpdater />
            </Form>
            <PermissionSearch
              open={searchPopup === 'permission'}
              setSearchPopup={setSearchPopup}
              onSubmit={setDocumentField}
            />
            <ConclusionSearch
              open={searchPopup === 'conclusion'}
              setSearchPopup={setSearchPopup}
              onSubmit={setDocumentField}
            />
          </>
        );
      }}
    </Formik>
  );
};

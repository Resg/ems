import React, { useCallback, useMemo, useState } from 'react';
import { FormikProps } from 'formik';
import { useSelector } from 'react-redux';

import {
  Check,
  ClearAll,
  Close,
  DeleteSweep,
  Refresh,
  Search,
} from '@mui/icons-material';
import { Button, Stack, Tab, Tabs } from '@mui/material';

import { DataTable } from '../../../components/CustomDataGrid';
import { FilterBar, FormValues } from '../../../components/FilterBarNew';
import { Popup } from '../../../components/Popup';
import { RoundButton } from '../../../components/RoundButton';
import { TabPanel } from '../../../components/TabPanel';
import { ToolsPanel } from '../../../components/ToolsPanel';
import { PermissionCols } from '../../../constants/permissions';
import {
  PermissionClearMap,
  PermissionSearchLabel,
  PermissionSearchTabs,
} from '../../../constants/permissionSearch';
import { Permission } from '../../../containers/PermissionSearch/PermissionSearchTabs/Permission';
import { useGetPermissionsQuery } from '../../../services/api/permissions';
import { RootState } from '../../../store';
import { prepareData } from '../../../utils/permissionSearch';

import styles from './styles.module.scss';

interface PermissionSearchProps {
  open: boolean;
  setSearchPopup: (open: 'conclusion' | 'permission' | null) => void;
  onSubmit: (selected: any, selectedRows: string[]) => void;
}

export const PermissionSearch: React.FC<PermissionSearchProps> = ({
  open,
  setSearchPopup,
  onSubmit,
}) => {
  const filters = useSelector(
    (state: RootState) => state.filters.data['permission-search'] || {}
  );
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);

  const [tab, setTab] = useState(PermissionSearchTabs.PERMISSION);

  const handleChange = useCallback(
    (event: React.SyntheticEvent, value: PermissionSearchTabs) => {
      setTab(value);
    },
    []
  );

  const {
    elasticSearch,
  }: {
    elasticSearch?: string;
  } = filters;

  const {
    data: permissions,
    refetch: refetchPermissions,
    isLoading,
  } = useGetPermissionsQuery(
    {
      searchParameters: filters,
      page,
      size: perPage,
    },
    {
      skip: !elasticSearch,
    }
  );

  const [cols, setCols] = useState(PermissionCols);

  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const selectedRows = useMemo(() => {
    return (
      Object.keys(selected).filter(
        (selectedKey) => selected[Number(selectedKey)]
      ) || []
    );
  }, [selected]);

  const clearTab = useCallback(
    (
      setFieldValue: (
        field: string,
        value: any,
        shouldValidate?: boolean
      ) => void
    ) => {
      const fields: { [key: string]: any } = PermissionClearMap[tab];

      for (const key in fields) {
        setFieldValue(key, fields[key]);
      }
    },
    [tab]
  );

  const createFilterButtons = useCallback(
    (
      onSearch: ({ values }: { values: FormValues }) => () => Promise<void>,
      props: FormikProps<FormValues>
    ) => {
      const { resetForm, values, setFieldValue } = props;

      return (
        <>
          <Button
            color="primary"
            variant="contained"
            startIcon={<Search />}
            onClick={onSearch({ values: prepareData(values as FormValues) })}
          >
            Поиск
          </Button>
          <Button
            variant="outlined"
            startIcon={<ClearAll />}
            onClick={() => clearTab(setFieldValue)}
          >
            Очистить вкладку
          </Button>
          <Button
            color="primary"
            variant="outlined"
            startIcon={<DeleteSweep />}
            onClick={() => resetForm()}
          >
            Очистить всё
          </Button>
        </>
      );
    },
    [clearTab]
  );

  return (
    <Popup
      open={open}
      onClose={() => setSearchPopup(null)}
      title={'Поиск разрешения'}
      height={820}
      centred
      bar={
        <Stack
          direction="row"
          justifyContent="flex-end"
          spacing={1.5}
          style={{ width: '100%' }}
        >
          <Button
            variant="contained"
            startIcon={<Check />}
            disabled={selectedRows.length !== 1}
            onClick={() => {
              onSubmit(
                permissions?.data.find(
                  (permission) => permission.id === Number(selectedRows[0])
                ),
                selectedRows
              );
              setSearchPopup(null);
            }}
          >
            Выбрать
          </Button>
          <Button
            variant="outlined"
            startIcon={<Close />}
            onClick={() => setSearchPopup(null)}
          >
            Отмена
          </Button>
        </Stack>
      }
    >
      <div className={styles.popupContent}>
        <FilterBar
          formKey="permission-search"
          placeholder="Номер разрешения"
          refetchSearch={() => refetchPermissions()}
          createFilterButtons={createFilterButtons}
        >
          <Tabs value={tab} onChange={handleChange}>
            <Tab
              label={PermissionSearchLabel[PermissionSearchTabs.PERMISSION]}
              value={PermissionSearchTabs.PERMISSION}
            />
            <Tab
              label={
                PermissionSearchLabel[PermissionSearchTabs.LIST_OF_NUMBERS]
              }
              value={PermissionSearchTabs.LIST_OF_NUMBERS}
            />
            <Tab
              label={PermissionSearchLabel[PermissionSearchTabs.REQUEST]}
              value={PermissionSearchTabs.REQUEST}
            />
          </Tabs>
          <TabPanel
            className={styles.tabPanel}
            value={tab}
            index={PermissionSearchTabs.PERMISSION}
          >
            <Permission />
          </TabPanel>
        </FilterBar>
        <DataTable
          formKey={'common_request_permission_search'}
          rows={permissions?.data || []}
          cols={cols}
          onRowSelect={setSelected}
          loading={isLoading}
          pagerProps={{
            page,
            setPage,
            perPage,
            setPerPage,
            total: permissions?.totalCount || 0,
          }}
        >
          <ToolsPanel
            className={styles.tools}
            leftActions={
              <RoundButton
                icon={<Refresh />}
                onClick={() => refetchPermissions()}
              />
            }
          />
        </DataTable>
      </div>
    </Popup>
  );
};

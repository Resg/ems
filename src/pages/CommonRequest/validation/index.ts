import { array, date, number } from 'yup';

export const validationSchema = {
  date: date().required('Обязательное поле'),
  requestType: number().nullable().required('Обязательное поле'),
  client: array().of(number()).nullable().min(1, 'Обязательное поле'),
  radioService: number().nullable().required('Обязательное поле'),
  serviceMRFC: number().nullable().required('Обязательное поле'),
};

import React, { useMemo, useState } from 'react';
import { useParams } from 'react-router-dom';

import { Tab, Tabs } from '@mui/material';

import { Page } from '../../components/Page';
import { TabPanel } from '../../components/TabPanel';
import { Common } from '../../containers/OutgoingPackets/Common';
import { MailPackages } from '../../containers/OutgoingPackets/MailPackages';
import { Registry } from '../../containers/OutgoingPackets/Registry';
import { useGetPacketsDocumentQuery } from '../../services/api/document';

import styles from './styles.module.scss';
export const OutgoingPacketPage: React.FC = ({}) => {
  const { id } = useParams();

  const numId = useMemo(() => {
    return Number(id);
  }, [id]);

  const { data: documentData } = useGetPacketsDocumentQuery(
    { id: numId },
    { skip: !numId }
  );

  const checkDeliveryArr = ['CLET', 'DHL', 'FELD', 'LET', 'POST'];

  const isVisibleMailPackage = useMemo(() => {
    if (documentData) {
      return checkDeliveryArr.includes(documentData?.deliveryTypeCode);
    }
  }, [documentData?.deliveryTypeCode]);

  const [tabValue, setTabValue] = useState(0);

  const handleTabChange = (_e: React.SyntheticEvent, newValue: number) => {
    setTabValue(newValue);
  };

  return (
    <Page title={`Исходящий пакет №${id}`}>
      <Tabs value={tabValue} onChange={handleTabChange}>
        <Tab label={'Общие'} value={0} />
        <Tab label={'Реестры'} value={1} />
        {isVisibleMailPackage && <Tab label={'Почтовые пакеты'} value={2} />}
      </Tabs>
      <TabPanel value={tabValue} index={0}>
        <Common id={numId} />
      </TabPanel>
      <TabPanel value={tabValue} index={1} className={styles.tab}>
        <div className={styles.tabContent}>
          <Registry />
        </div>
      </TabPanel>
      {isVisibleMailPackage && (
        <TabPanel value={tabValue} index={2} className={styles.tab}>
          <div className={styles.tabContent}>
            <MailPackages />
          </div>
        </TabPanel>
      )}
    </Page>
  );
};

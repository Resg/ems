import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Formik, FormikValues } from 'formik';
import { useParams } from 'react-router';
import { useNavigate } from 'react-router-dom';
import { mixed, object, string } from 'yup';

import { Edit, Save } from '@mui/icons-material';
import { Grid, Tab, Tabs } from '@mui/material';

import Card from '../../components/Card';
import { FileInput } from '../../components/FileInput';
import Input from '../../components/Input';
import { RoundButton } from '../../components/RoundButton';
import {
  ConclusionAppendicesFields,
  ConclusionAppendicesLabels,
  DefaultConclusionAppendicesFields,
} from '../../constants/conclusions';
import {
  useCreateConclusionAppendixMutation,
  useGetConclusionAppendixQuery,
  useUpdateConclusionAppendixMutation,
} from '../../services/api/conclusions';
import { useAppDispatch } from '../../store';
import { setLocationData } from '../../store/utils';

import styles from './styles.module.scss';

export const ConclusionAppendicesPage = () => {
  const { id, appendixId } = useParams();
  const [isEdit, setIsEdit] = useState(!appendixId);
  const [create] = useCreateConclusionAppendixMutation();
  const [update] = useUpdateConclusionAppendixMutation();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const { data } = useGetConclusionAppendixQuery(
    { id: appendixId as string },
    { skip: !appendixId }
  );

  const isViewPage = useMemo(() => {
    return window.location.href.includes('view');
  }, [window.location.href]);

  const handleSubmit = useCallback(
    (values: FormikValues) => {
      const data = new FormData();
      Object.keys(values).forEach((key) => {
        data.append(key, values[key]);
      });
      data.append('conclusionId', String(id));
      if (appendixId) {
        update({ id: Number(appendixId), data });
      } else {
        create({ data });
      }
      setIsEdit(false);
    },
    [appendixId, create, id, update]
  );

  useEffect(() => {
    dispatch(setLocationData({ name: 'Приложение', data: data }));
  }, [data]);

  const initialValues = useMemo(() => {
    return {
      ...(data || DefaultConclusionAppendicesFields),
    };
  }, [data]);

  return (
    <>
      <h2>Приложение</h2>
      <Formik
        initialValues={initialValues}
        validationSchema={validation}
        enableReinitialize
        onSubmit={handleSubmit}
      >
        {({ values, handleBlur, handleChange, handleSubmit, errors }) => (
          <Card className={styles.card}>
            <Tabs value={1} className={styles.tabs}>
              <Tab value={1} label={'Общие'} />
            </Tabs>
            <div className={styles.container}>
              {isViewPage ? null : (
                <div className={styles.round}>
                  {isEdit ? (
                    <RoundButton
                      onClick={() => handleSubmit()}
                      icon={<Save />}
                    />
                  ) : (
                    <RoundButton
                      icon={<Edit />}
                      onClick={() => setIsEdit(true)}
                    />
                  )}
                </div>
              )}

              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Input
                    name={ConclusionAppendicesFields.NAME}
                    label={
                      ConclusionAppendicesLabels[
                        ConclusionAppendicesFields.NAME
                      ]
                    }
                    disabled={!isEdit}
                    value={values[ConclusionAppendicesFields.NAME] || ''}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    required
                    error={Boolean(errors[ConclusionAppendicesFields.NAME])}
                    helperText={
                      errors[ConclusionAppendicesFields.NAME] as string
                    }
                  />
                </Grid>
                <Grid item xs={12}>
                  <Input
                    name={ConclusionAppendicesFields.TEXT}
                    label={
                      ConclusionAppendicesLabels[
                        ConclusionAppendicesFields.TEXT
                      ]
                    }
                    disabled={!isEdit}
                    value={values[ConclusionAppendicesFields.TEXT] || ''}
                    onChange={handleChange}
                    multiline
                    onBlur={handleBlur}
                    error={Boolean(errors[ConclusionAppendicesFields.TEXT])}
                    helperText={
                      errors[ConclusionAppendicesFields.TEXT] as string
                    }
                  />
                </Grid>
                <Grid item xs={12}>
                  <FileInput
                    disabled={!isEdit}
                    required
                    name={ConclusionAppendicesFields.FILE}
                    label={
                      ConclusionAppendicesLabels[
                        ConclusionAppendicesFields.FILE
                      ]
                    }
                  />
                </Grid>
              </Grid>
            </div>
          </Card>
        )}
      </Formik>
    </>
  );
};

const validation = object({
  [ConclusionAppendicesFields.NAME]: string().required(),
  [ConclusionAppendicesFields.TEXT]: string(),
  [ConclusionAppendicesFields.FILE]: mixed().required(),
});

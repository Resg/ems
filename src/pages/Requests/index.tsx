import React, { useCallback, useEffect, useRef, useState } from 'react';
import {
  matchPath,
  useLocation,
  useNavigate,
  useParams,
  useSearchParams,
} from 'react-router-dom';

import { MoreVert } from '@mui/icons-material';
import { Menu, Tab, Tabs } from '@mui/material';

import Card from '../../components/Card';
import { MenuButtonItem } from '../../components/MenuButton/MenuButtonItem';
import { RoundButton } from '../../components/RoundButton';
import { TabPanel } from '../../components/TabPanel';
import { RequestTabs, RequestTabsLabels } from '../../constants/requests';
import { Routes } from '../../constants/routes';
import { AnnulledPermissions } from '../../containers/Requests/AnnulledPermissions';
import { Conclusions } from '../../containers/Requests/Conclusions';
import { Documents } from '../../containers/Requests/Documents';
import { LinkRequests } from '../../containers/Requests/LinkRequests';
import { MPZ } from '../../containers/Requests/MPZ';
import { SPS_PCTR } from '../../containers/Requests/PCTR';
import { PermissionsList } from '../../containers/Requests/PermissionsList';
import { Solutions } from '../../containers/Requests/Solutions';
import { Stages } from '../../containers/Requests/Stages';
import { Tasks } from '../../containers/Requests/Tasks';
import { ZeForCancellation } from '../../containers/Requests/ZeForCancellation';
import {
  useGetPCTRCodeFormQuery,
  useGetRequestQuery,
  useGetRequestTabsQuery,
  useGetTabParametersQuery,
} from '../../services/api/request';
import { useAppDispatch } from '../../store';
import { setLocationData } from '../../store/utils';
import { CommonRequestForm } from '../CommonRequest';

import styles from './styles.module.scss';

export const RequestPage = () => {
  const { tab } = useParams();
  const dispatch = useAppDispatch();
  const location = useLocation();
  const [tabValue, setTabValue] = useState(tab || RequestTabs.COMMON);
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const idForRedirect = Number(searchParams.get('id'));
  const id =
    Number(
      matchPath(Routes.REQUESTS.REQUEST_TABS, location.pathname)?.params?.id
    ) ||
    Number(matchPath(Routes.REQUESTS.REQUEST, location.pathname)?.params?.id) ||
    0;
  const [isOpenMenu, setIsOpenMenu] = useState(false);
  const ref = useRef<HTMLButtonElement>(null);

  const { data } = useGetRequestQuery({ id: id as number }, { skip: !id });
  const { data: tabParameters } = useGetTabParametersQuery(
    { id: id as number },
    { skip: !id }
  );
  const { data: requestTabs } = useGetRequestTabsQuery(
    { id: id as number },
    { skip: !id }
  );

  const { data: PCTRCodeForm } = useGetPCTRCodeFormQuery(
    { requestId: id },
    { skip: !id }
  );

  const handleOpenMenu = useCallback(() => {
    setIsOpenMenu(true);
  }, []);

  const handleCloseMenu = useCallback(() => {
    setIsOpenMenu(false);
  }, []);

  const handleTabChange = (e: React.SyntheticEvent, newValue: RequestTabs) => {
    setTabValue(newValue);
  };

  const handleClickMenuTab = useCallback(
    (tab: RequestTabs) => () => {
      setTabValue(tab);
      setIsOpenMenu(false);
    },
    []
  );

  useEffect(() => {
    if (id) {
      navigate(
        Routes.REQUESTS.REQUEST_TABS.replace(':id', id).replace(
          ':tab',
          tabValue
        )
      );
    }
  }, [tabValue]);

  useEffect(() => {
    if (idForRedirect) {
      navigate(
        Routes.REQUESTS.REQUEST_TABS.replace(':id', idForRedirect).replace(
          ':tab',
          tabValue
        )
      );
    }
  }, [idForRedirect]);

  useEffect(() => {
    dispatch(
      setLocationData({
        name: data ? `Заявка № ${data.number}` : 'Новая заявка',
        data: data,
      })
    );
  }, [data]);

  return (
    <>
      <div className={styles.title}>Заявка №{data?.number}</div>
      <Card className={styles.card}>
        <div className={styles.tabBar}>
          {id ? (
            <Tabs
              value={tabValue}
              onChange={handleTabChange}
              scrollButtons={false}
              variant="scrollable"
            >
              {Object.values(RequestTabs).map((tab) => {
                if (requestTabs && requestTabs[tab] === false) return null;
                return (
                  <Tab key={tab} label={RequestTabsLabels[tab]} value={tab} />
                );
              })}
            </Tabs>
          ) : (
            <Tabs
              value={tabValue}
              onChange={handleTabChange}
              scrollButtons={false}
              variant="scrollable"
            >
              <Tab
                key={RequestTabs.COMMON}
                label={RequestTabsLabels[RequestTabs.COMMON]}
                value={RequestTabs.COMMON}
              />
            </Tabs>
          )}
          <RoundButton icon={<MoreVert />} ref={ref} onClick={handleOpenMenu} />
          <Menu
            open={isOpenMenu}
            anchorEl={ref.current}
            onClose={handleCloseMenu}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            className={styles.menu}
          >
            {Object.values(RequestTabs).map((tab) => {
              if (requestTabs && requestTabs[tab] === false) return null;
              return (
                <MenuButtonItem key={tab} onClick={handleClickMenuTab(tab)}>
                  {RequestTabsLabels[tab]}
                </MenuButtonItem>
              );
            })}
          </Menu>
        </div>
        <TabPanel value={tabValue} index={RequestTabs.COMMON}>
          <CommonRequestForm typeId={data?.requestType} id={id} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={RequestTabs.DOCUMENTS}
          className={styles.tab}
        >
          <Documents id={id} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={RequestTabs.PCTR}
          className={styles.tab}
        >
          {PCTRCodeForm?.code === 'SPS' ? <SPS_PCTR id={id} /> : 'ПЧТР ОВЧЧМ'}
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={RequestTabs.STAGES}
          className={styles.tab}
        >
          <div className={styles.tabContent}>
            <Stages id={id} />
          </div>
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={RequestTabs.CONCLUSIONS}
          className={styles.tab}
        >
          <Conclusions id={id} num={data?.number} />
        </TabPanel>
        {data && (
          <TabPanel
            index={RequestTabs.TASKS}
            value={tabValue}
            className={styles.tab}
          >
            <Tasks request={data} />
          </TabPanel>
        )}
        <TabPanel
          value={tabValue}
          index={RequestTabs.SOLUTIONS}
          className={styles.tab}
        >
          <Solutions id={id} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={RequestTabs.ZE_FOR_CANCELLATION}
          className={styles.tab}
        >
          <ZeForCancellation id={id} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={RequestTabs.MPZ}
          className={styles.tab}
        >
          <MPZ
            requestId={id}
            ilpScreenFormCode={tabParameters?.ilpScreenFormCode}
          />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={RequestTabs.ANNULLED_PERMISSIONS}
          className={styles.tab}
        >
          <AnnulledPermissions id={id} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={RequestTabs.LIST_OF_FREQUENCIES}
          className={styles.tab}
        >
          <PermissionsList id={id} />
        </TabPanel>
        <TabPanel
          value={tabValue}
          index={RequestTabs.LINKED_TASK}
          className={styles.tab}
        >
          <LinkRequests id={id} />
        </TabPanel>
      </Card>
    </>
  );
};

import React, { useEffect, useState } from 'react';

import { Tab, Tabs } from '@mui/material';

import { Page } from '../../components/Page';
import { TabPanel } from '../../components/TabPanel';
import { Settings } from '../../containers/PersonalSettings';
import { useAppDispatch } from '../../store';
import { setLocationData } from '../../store/utils';

export const PersonalSettingsPage = () => {
  const dispatch = useAppDispatch();
  const [tabValue, setTabValue] = useState(0);
  const handleTabChange = (_e: React.SyntheticEvent, newValue: number) => {
    setTabValue(newValue);
  };

  useEffect(() => {
    dispatch(setLocationData({ menuName: ['Профиль'] }));
  }, []);

  return (
    <Page title={`Профиль`}>
      <Tabs value={tabValue} onChange={handleTabChange}>
        <Tab label="Настройки" value={0} />
      </Tabs>
      <TabPanel value={tabValue} index={0}>
        <Settings />
      </TabPanel>
    </Page>
  );
};

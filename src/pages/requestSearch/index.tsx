import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { FormikProps } from 'formik';
import { useDispatch, useSelector } from 'react-redux';

import {
  AddCircleOutline,
  AddToPhotos,
  Article,
  ChevronRight,
  ClearAll,
  ContentCopy,
  CorporateFare,
  DeleteOutline,
  DeleteSweep,
  ExpandMore,
  NoteAdd,
  Pattern,
  Radio,
  Search,
  Splitscreen,
  Task,
  Water,
  WifiTethering,
} from '@mui/icons-material';
import { Button, Tab, Tabs } from '@mui/material';

import Card from '../../components/Card';
import { CreateDocumentToolButton } from '../../components/CreateDocumentByTemplateForm/CreateDocumentToolButton';
import { DataTable } from '../../components/CustomDataGrid';
import { FormValues } from '../../components/FilterBarNew';
import { FilterBar } from '../../components/FilterBarNew';
import { MenuButtonItem } from '../../components/MenuButton/MenuButtonItem';
import { TabPanel } from '../../components/TabPanel';
import { ToolButton, ToolsPanel } from '../../components/ToolsPanel';
import { DeleteToolButton } from '../../components/ToolsPanel/DeleteToolButton';
import { MenuToolButton } from '../../components/ToolsPanel/MenuToolButton';
import {
  RequestClearMap,
  RequestSearchCols,
  RequestSearchLabel,
  RequestSearchTabs,
} from '../../constants/requestSearch';
import { Routes } from '../../constants/routes';
import { Additional } from '../../containers/RequestSearch/RequestSearchTabs/Additional';
import { Document } from '../../containers/RequestSearch/RequestSearchTabs/Document';
import { Finances } from '../../containers/RequestSearch/RequestSearchTabs/Finances';
import { FrequencyBand } from '../../containers/RequestSearch/RequestSearchTabs/FrequencyBand';
import { InstallationPoint } from '../../containers/RequestSearch/RequestSearchTabs/InstallationPoint';
import { Numbers } from '../../containers/RequestSearch/RequestSearchTabs/Numbers';
import { Request } from '../../containers/RequestSearch/RequestSearchTabs/Request';
import {
  useDeleteRequestMutation,
  useGetRequestsQuery,
} from '../../services/api/request';
import { RootState } from '../../store';
import { setLocationData } from '../../store/utils';
import { RequestData } from '../../types/requests';
import { massServerQuery } from '../../utils/incomingDocumentTasks';
import { prepareData } from '../../utils/requestSearch';

import styles from './styles.module.scss';

export const RequestSearchPage: React.FC = () => {
  const filters = useSelector(
    (state: RootState) => state.filters.data['request-search'] || {}
  );
  const dispatch = useDispatch();
  const [cols, setCols] = useState(RequestSearchCols);
  const [page, setPage] = useState(1);
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const [perPage, setPerPage] = useState(10);
  const [tab, setTab] = useState(RequestSearchTabs.REQUEST);

  const handleChange = useCallback(
    (_event: React.SyntheticEvent, value: RequestSearchTabs) => {
      setTab(value);
    },
    []
  );

  const { data, isLoading, refetch } = useGetRequestsQuery(
    {
      searchParameters: filters,
      page: page,
      size: perPage,
      useCount: true,
    },
    { skip: !filters }
  );
  const [deleteRequest] = useDeleteRequestMutation();

  const visibleColumns = useMemo(
    () => cols.filter((col) => !col.hidden),
    [cols]
  );

  const rows = useMemo(() => {
    return data?.data || [];
  }, [data]);

  const selectedRequests = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const [orderedRequests, setOrderedRequests] = useState<number[]>([]);

  useEffect(() => {
    const orderedIdsSet = new Set(orderedRequests);
    const updatedOrderedIds = orderedRequests.filter((id: number) =>
      selectedRequests.includes(id)
    );

    selectedRequests.forEach((id) => {
      if (!orderedIdsSet.has(id)) {
        updatedOrderedIds.push(id);
      }
    });

    setOrderedRequests(updatedOrderedIds);
  }, [selectedRequests]);

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const openRequest = useCallback(() => {
    window.open(
      Routes.REQUESTS.REQUEST.replace(':id', selectedRequests[0]),
      openFormInNewTab ? '_blank' : '_self'
    );
  }, [selectedRequests, openFormInNewTab]);

  const openStage = useCallback(() => {
    window.open(
      `/stages?reqId=${selectedRequests[0]}`,
      openFormInNewTab ? '_blank' : '_self'
    );
  }, [selectedRequests, openFormInNewTab]);

  const handleDelete = useCallback(() => {
    massServerQuery(selectedRequests, deleteRequest);
  }, [selectedRequests]);

  const clearTab = useCallback(
    (
      setFieldValue: (
        field: string,
        value: any,
        shouldValidate?: boolean
      ) => void
    ) => {
      const fields: { [key: string]: any } = RequestClearMap[tab];

      for (const key in fields) {
        setFieldValue(key, fields[key]);
      }
    },
    [tab]
  );

  useEffect(() => {
    dispatch(setLocationData({ menuName: ['Заявки', 'Поиск заявок'] }));
  }, []);

  const createFilterButtons = useCallback(
    (
      onSearch: ({ values }: { values: FormValues }) => () => Promise<void>,
      props: FormikProps<FormValues>
    ) => {
      const { resetForm, values, setFieldValue } = props;

      return (
        <>
          <Button
            color="primary"
            variant="contained"
            startIcon={<Search />}
            onClick={onSearch({ values: prepareData(values as FormValues) })}
          >
            Поиск
          </Button>
          <Button
            variant="outlined"
            startIcon={<ClearAll />}
            onClick={() => clearTab(setFieldValue)}
          >
            Очистить вкладку
          </Button>
          <Button
            color="primary"
            variant="outlined"
            startIcon={<DeleteSweep />}
            onClick={() => resetForm()}
          >
            Очистить всё
          </Button>
        </>
      );
    },
    [clearTab]
  );

  const rowDoubleHandler = useCallback(
    (request: RequestData) => {
      window.open(
        Routes.REQUESTS.REQUEST.replace(':id', request.id),
        openFormInNewTab ? '_blank' : '_self'
      );
    },
    [openFormInNewTab]
  );

  return (
    <>
      <h1 className={styles.title}>Поиск заявок</h1>

      <FilterBar
        formKey="request-search"
        placeholder="Номер заявки"
        createFilterButtons={createFilterButtons}
        refetchSearch={() => refetch()}
      >
        <Tabs value={tab} onChange={handleChange}>
          <Tab
            label={RequestSearchLabel[RequestSearchTabs.REQUEST]}
            value={RequestSearchTabs.REQUEST}
          />
          <Tab
            label={RequestSearchLabel[RequestSearchTabs.NUMBER_LIST]}
            value={RequestSearchTabs.NUMBER_LIST}
          />
          <Tab
            label={RequestSearchLabel[RequestSearchTabs.DOCUMENT]}
            value={RequestSearchTabs.DOCUMENT}
          />
          <Tab
            label={RequestSearchLabel[RequestSearchTabs.FINANCES]}
            value={RequestSearchTabs.FINANCES}
          />
          <Tab
            label={RequestSearchLabel[RequestSearchTabs.FREQUENCY_BAND]}
            value={RequestSearchTabs.FREQUENCY_BAND}
          />
          <Tab
            label={RequestSearchLabel[RequestSearchTabs.INSTALLATION_POINT]}
            value={RequestSearchTabs.INSTALLATION_POINT}
          />
          <Tab
            label={RequestSearchLabel[RequestSearchTabs.ADDITIONAL]}
            value={RequestSearchTabs.ADDITIONAL}
          />
        </Tabs>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={RequestSearchTabs.REQUEST}
        >
          <Request />
        </TabPanel>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={RequestSearchTabs.INSTALLATION_POINT}
        >
          <InstallationPoint />
        </TabPanel>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={RequestSearchTabs.FREQUENCY_BAND}
        >
          <FrequencyBand />
        </TabPanel>

        {tab === RequestSearchTabs.NUMBER_LIST && <Numbers />}
        {tab === RequestSearchTabs.ADDITIONAL && <Additional />}
        {tab === RequestSearchTabs.DOCUMENT && <Document />}
        {tab === RequestSearchTabs.FINANCES && <Finances />}
      </FilterBar>
      <Card className={styles.card}>
        <DataTable
          formKey={'pages_request_search'}
          cols={cols}
          rows={rows}
          onRowSelect={setSelected}
          onRowDoubleClick={rowDoubleHandler}
          loading={isLoading}
          height="fullHeight"
          pagerProps={{
            page,
            setPage,
            perPage,
            setPerPage,
            total: data?.totalCount || 0,
          }}
        >
          <ToolsPanel className={styles.panel}>
            <ToolButton
              label="Подробно"
              startIcon={<ChevronRight />}
              disabled={selectedRequests.length !== 1}
              onClick={openRequest}
            />
            <MenuToolButton
              label="Cоздать заявку"
              startIcon={<AddCircleOutline />}
              endIcon={<ExpandMore />}
            >
              <MenuButtonItem
                icon={<Article />}
                linkProps={{
                  to: Routes.REQUESTS.CREATE,
                  newTab: true,
                }}
              >
                Общая форма
              </MenuButtonItem>
              <MenuButtonItem
                icon={<Radio />}
                linkProps={{
                  to: Routes.REQUESTS.CREATE,
                  newTab: true,
                }}
              >
                Заявка радиолюбителя
              </MenuButtonItem>
              <MenuButtonItem
                icon={<Water />}
                linkProps={{
                  to: Routes.REQUESTS.CREATE,
                  newTab: true,
                }}
              >
                Заявка "Морская служба"
              </MenuButtonItem>
            </MenuToolButton>
            <MenuToolButton
              label="Добавить документ"
              startIcon={<NoteAdd />}
              endIcon={<ExpandMore />}
              disabled={!selectedRequests.length}
            >
              <CreateDocumentToolButton
                label="Создать документ"
                startIcon={<NoteAdd />}
                variant="submenu"
                disabled={!selectedRequests.length}
                objectTypeCode={'REQUEST'}
                objectIds={selectedRequests.toString()}
                id={'1'}
                fast
              />
              <MenuButtonItem icon={<CorporateFare />} disabled>
                Выбрать из списка
              </MenuButtonItem>
            </MenuToolButton>
            <ToolButton
              label="Копировать"
              startIcon={<ContentCopy />}
              disabled={selectedRequests.length !== 1}
              fast
            />
            <DeleteToolButton
              label="Удалить"
              startIcon={<DeleteOutline />}
              disabled={!selectedRequests.length}
              description="Вы действительно хотите удалить выбранные заявки?"
              title="Удаление заявок"
              onConfirm={handleDelete}
              fast
            />
            <ToolButton
              label="Проверить на позывные"
              startIcon={<WifiTethering />}
              disabled={!selectedRequests.length}
            />
            <ToolButton
              label="Разделить"
              startIcon={<Splitscreen />}
              disabled={selectedRequests.length !== 1}
            />
            <ToolButton
              label="Создать задачи по шаблону"
              startIcon={<Pattern />}
              disabled={!selectedRequests.length}
            />
            <ToolButton
              label="Создать этап"
              startIcon={<AddToPhotos />}
              disabled={!selectedRequests.length}
              onClick={openStage}
            />
            <ToolButton
              label="Выдать заключения"
              startIcon={<Task />}
              disabled={!selectedRequests.length}
            />
            <CreateDocumentToolButton
              label="Сформировать заключение"
              startIcon={<Task />}
              disabled={!selectedRequests.length}
              objectTypeCode={'REQUEST'}
              precreationObjectTypeCode={'CONCLUSION'}
              objectIds={selectedRequests.toString()}
              fast
            />
          </ToolsPanel>
        </DataTable>
      </Card>
    </>
  );
};

import { Grid } from '@mui/material';

import { ListInput } from '../../../../components/ListInput';
import { RequestSearchFields } from '../../../../constants/requestSearch';

export const Numbers = () => {
  return (
    <Grid container direction="column" spacing={1} sx={{ mb: 2 }}>
      <Grid item>Список номеров</Grid>
      <Grid item>
        <ListInput
          name={RequestSearchFields.NUMBERS}
          separator="line-break"
          multiline
          rows={15}
        />
      </Grid>
    </Grid>
  );
};

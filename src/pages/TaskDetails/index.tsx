import React, { useEffect, useMemo, useState } from 'react';
import {
  matchPath,
  useLocation,
  useNavigate,
  useParams,
} from 'react-router-dom';

import { Tab, Tabs } from '@mui/material';

import { Page } from '../../components/Page';
import { TabPanel } from '../../components/TabPanel';
import { Routes } from '../../constants/routes';
import { TaskTabs, TaskTabsLabels } from '../../constants/tasks';
import { Common } from '../../containers/Tasks/Common';
import { useGetTaskQuery } from '../../services/api/incomingDocumentTasks';
import { useAppDispatch } from '../../store';
import { setLocationData } from '../../store/utils';

export const TaskDetailsPage: React.FC = ({}) => {
  const { id: idForRedirect } = useParams();
  const { tab } = useParams();
  const location = useLocation();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const id =
    Number(matchPath(Routes.TASKS.TASK_TABS, location.pathname)?.params?.id) ||
    Number(matchPath(Routes.TASKS.TASK, location.pathname)?.params?.id) ||
    0;

  const numId = useMemo(() => {
    return Number(id);
  }, [id]);

  const { data } = useGetTaskQuery(numId);

  const { data: parentData } = useGetTaskQuery(data?.parentId, {
    skip: !data?.parentId,
  });

  const taskName = useMemo(() => {
    if (data?.parentId !== null && parentData?.typeId !== 8) {
      return ' Детальные данные по подзадаче';
    }
    return ' Детальные данные по задаче';
  }, [data]);

  const [tabValue, setTabValue] = useState(tab || TaskTabs.COMMON);

  const handleTabChange = (_e: React.SyntheticEvent, newValue: TaskTabs) => {
    setTabValue(newValue);
  };

  useEffect(() => {
    if (id) {
      navigate(
        Routes.TASKS.TASK_TABS.replace(':id', id).replace(':tab', tabValue)
      );
    }
  }, [tabValue]);

  useEffect(() => {
    if (idForRedirect) {
      navigate(
        Routes.TASKS.TASK_TABS.replace(':id', idForRedirect).replace(
          ':tab',
          tabValue
        )
      );
    }
  }, [idForRedirect]);

  useEffect(() => {
    dispatch(
      setLocationData({ name: 'Детальные данные по подзадаче', data: data })
    );
  }, [data]);

  return (
    <Page title={`${taskName}`}>
      <Tabs value={tabValue} onChange={handleTabChange}>
        <Tab label={TaskTabsLabels[TaskTabs.COMMON]} value={TaskTabs.COMMON} />
        <Tab
          label={TaskTabsLabels[TaskTabs.DOCUMENTS]}
          value={TaskTabs.DOCUMENTS}
        />
      </Tabs>
      <TabPanel value={tabValue} index={TaskTabs.COMMON}>
        <Common id={numId} />
      </TabPanel>
      <TabPanel value={tabValue} index={TaskTabs.DOCUMENTS}>
        <div>СВЯЗАННЫЕ ЗАЯВКИ</div>
      </TabPanel>
    </Page>
  );
};

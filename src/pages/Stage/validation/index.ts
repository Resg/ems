import { date, number, object, string } from 'yup';

import { DetailFormFields } from '../../../constants/stages';

export const validationSchema = object({
  [DetailFormFields.NAME]: string().nullable().required('Обязательное поле'),
  [DetailFormFields.PERIOD]: number().required(),
  [DetailFormFields.PLAN_DATE]: string(),
  [DetailFormFields.PERIOD_TYPE]: string(),
  [DetailFormFields.DIVISION]: string().required('Обязательное поле'),
  [DetailFormFields.GROUP]: string(),
  [DetailFormFields.CLERK]: string(),
  [DetailFormFields.COMMENTS]: string(),
  [DetailFormFields.START_DATE]: date()
    .nullable()
    .required('Обязательное поле'),
  [DetailFormFields.END_DATE]: date().nullable(),
  [DetailFormFields.CREATOR]: string(),
  [DetailFormFields.EDITOR]: string(),
});

import React, { useEffect } from 'react';
import { format } from 'date-fns';
import { useFormikContext } from 'formik';

import { AutoCompleteInput } from '../../../components/AutoCompleteInput';
import { DetailFormFields, detailFormLabels } from '../../../constants/stages';
import { useGetCalculatedPlanDateQuery } from '../../../services/api/request';
import { StageCreateData } from '../../../types/requests';
import { getStageNames } from '../../../utils/requests';

export interface StageNameInputProps {
  data?: StageCreateData[];
  disabled?: boolean;
}

export const StageNameInput: React.FC<StageNameInputProps> = ({
  data,
  disabled,
}) => {
  const { values, setFieldValue } = useFormikContext<any>();
  const stageName = data?.find((i) => i.stageId === values.stageName);

  const { data: planDate } = useGetCalculatedPlanDateQuery({
    startDate: values[DetailFormFields.START_DATE],
    controlPeriod: stageName?.controlPeriod || 0,
    controlPeriodType: stageName?.controlPeriodType || '',
  });

  useEffect(() => {
    if (stageName) {
      setFieldValue(DetailFormFields.PERIOD, stageName.controlPeriod);
      setFieldValue(DetailFormFields.PERIOD_TYPE, stageName.controlPeriodType);
      setFieldValue(
        DetailFormFields.PLAN_DATE,
        planDate ? format(new Date(planDate.planDate), 'dd.MM.yyyy') : ''
      );
    } else {
      setFieldValue(DetailFormFields.PERIOD, '');
      setFieldValue(DetailFormFields.PERIOD_TYPE, '');
      setFieldValue(DetailFormFields.PLAN_DATE, '');
    }
  }, [stageName, planDate]);

  return (
    <AutoCompleteInput
      name={DetailFormFields.NAME}
      label={detailFormLabels[DetailFormFields.NAME]}
      options={getStageNames(data)}
      disabled={disabled}
      required
    />
  );
};

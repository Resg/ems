import React, { useEffect, useMemo, useState } from 'react';
import { Form, Formik } from 'formik';
import { useSearchParams } from 'react-router-dom';

import { Edit, EditOff, Save } from '@mui/icons-material';
import { Grid, Stack, Tab, Tabs } from '@mui/material';

import { AutoCompleteInput } from '../../components/AutoCompleteInput';
import { ClerkInput } from '../../components/ClerkInput';
import { DatePickerInput } from '../../components/DatePickerInput';
import Input from '../../components/Input';
import { Page } from '../../components/Page';
import { RoundButton } from '../../components/RoundButton';
import { TabPanel } from '../../components/TabPanel';
import {
  DetailFormFields,
  detailFormLabels,
  stageDetailDefaults,
} from '../../constants/stages';
import {
  useGetClerksQuery,
  useGetDivisionsQuery,
} from '../../services/api/dictionaries';
import {
  useCreateStageMutation,
  useGetStageCreateDataQuery,
  useGetStageDetailQuery,
  useUpdateStageMutation,
} from '../../services/api/request';
import { useAppDispatch } from '../../store';
import { setLocationData } from '../../store/utils';

import { StageNameInput } from './StageNameInput';
import { validationSchema } from './validation';

export const StagePage = () => {
  const [searchParams] = useSearchParams();
  const dispatch = useAppDispatch();
  const id = searchParams.get('id') || '';
  const reqId = searchParams.get('reqId') || '';
  const [tabValue, setTabValue] = useState(0);
  const [editMode, setEditMode] = useState(!id);
  const [updateStage] = useUpdateStageMutation();
  const [createStage] = useCreateStageMutation();
  const { data, refetch } = useGetStageDetailQuery(
    { id: Number(id) },
    { skip: !id }
  );
  const { data: divisions = [] } = useGetDivisionsQuery({});
  const { data: clerks = [] } = useGetClerksQuery({});
  const { data: createData } = useGetStageCreateDataQuery({ reqIds: [reqId] });
  const initialValues = useMemo(
    () => ({
      ...(data || stageDetailDefaults),
      stageName: data ? Number(data?.stageId) : null,
      parentDivisionName: data?.parentDivisionId,
      performerClerkName: data?.performerClerkId,
      divisionName: data?.divisionId,
    }),
    [data]
  );

  const handleTabChange = (_e: React.SyntheticEvent, newValue: number) => {
    setTabValue(newValue);
  };

  const handleSubmit = async (values: any) => {
    if (!id) {
      const createFormData = {
        requestId: Number(reqId),
        stageId: values.stageName,
        parentDivisionId: values.parentDivisionName,
        divisionId: values.divisionName,
        performerClerkId: values.performerClerkName,
        performerExternal: null,
        comments: values.comments,
        stateCode: null,
        startDate: values.startDate,
        controlPeriod: Number(values.controlPeriod),
      };

      await createStage({ stageData: createFormData });
      setEditMode(false);
    } else {
      const updateData = {
        parentDivisionId: values.parentDivisionName || 0,
        divisionId: values.divisionName || 0,
        performerClerkId: values.performerClerkName || 0,
        comments: values.comments,
        startDate: values.startDate,
        controlPeriod: Number(values.controlPeriod),
        operationId: Number(id),
        endDate: values.endDate,
      };

      await updateStage({ stageData: updateData });
      setEditMode(false);
      refetch();
    }
  };

  useEffect(() => {
    dispatch(
      setLocationData({ name: 'Детальные данные по этапу', data: data })
    );
  }, [data]);

  return (
    <Page title="Детальные данные по этапу">
      <Tabs value={tabValue} onChange={handleTabChange}>
        <Tab label="Общие" value={0} />
      </Tabs>

      <TabPanel value={tabValue} index={0}>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          enableReinitialize
          onSubmit={handleSubmit}
        >
          {({
            values,
            handleReset,
            handleChange,
            setFieldValue,
            errors,
            touched,
            handleSubmit,
          }) => {
            const divisionClerks = clerks.filter(
              (i) => i.divisionId === values.divisionName
            );

            const handleEditOff = () => {
              handleReset();
              setEditMode(false);
            };

            return (
              <Form>
                <Stack my={3} spacing={2} direction="row">
                  {!editMode && (
                    <RoundButton
                      icon={<Edit />}
                      onClick={() => setEditMode(true)}
                    />
                  )}
                  {editMode && (
                    <>
                      <RoundButton
                        icon={<Save />}
                        onClick={() => handleSubmit()}
                      />
                      <RoundButton icon={<EditOff />} onClick={handleEditOff} />
                    </>
                  )}
                </Stack>
                <Grid container columns={4} spacing={2}>
                  <Grid item xs={4}>
                    <StageNameInput data={createData?.data} />
                  </Grid>
                  <Grid item xs={1}>
                    <Input
                      name={DetailFormFields.PERIOD}
                      label={detailFormLabels[DetailFormFields.PERIOD]}
                      value={values[DetailFormFields.PERIOD]}
                      required
                      disabled
                    />
                  </Grid>
                  <Grid item xs={1}>
                    <Input
                      name={DetailFormFields.PERIOD_TYPE}
                      label={detailFormLabels[DetailFormFields.PERIOD_TYPE]}
                      value={values[DetailFormFields.PERIOD_TYPE]}
                      required
                      disabled
                    />
                  </Grid>
                  <Grid item xs={2}>
                    <Input
                      name={DetailFormFields.PLAN_DATE}
                      label={detailFormLabels[DetailFormFields.PLAN_DATE]}
                      value={values[DetailFormFields.PLAN_DATE]}
                      onChange={handleChange}
                      disabled
                    />
                  </Grid>
                  <Grid item xs={2}>
                    <AutoCompleteInput
                      name={DetailFormFields.DIVISION}
                      label={detailFormLabels[DetailFormFields.DIVISION]}
                      options={divisions}
                      required
                      disabled={Boolean(id)}
                    />
                  </Grid>
                  <Grid item xs={2}>
                    <AutoCompleteInput
                      name={DetailFormFields.GROUP}
                      label={detailFormLabels[DetailFormFields.GROUP]}
                      options={
                        divisions?.filter(
                          (i) => i.parent === values[DetailFormFields.DIVISION]
                        ) || []
                      }
                      disabled={Boolean(id)}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <ClerkInput
                      name={DetailFormFields.CLERK}
                      label={detailFormLabels[DetailFormFields.CLERK]}
                      disabled={!editMode}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <Input
                      name={DetailFormFields.COMMENTS}
                      label={detailFormLabels[DetailFormFields.COMMENTS]}
                      value={values[DetailFormFields.COMMENTS]}
                      onChange={handleChange}
                      multiline
                      disabled={!editMode}
                    />
                  </Grid>
                  <Grid item xs={1}>
                    <DatePickerInput
                      name={DetailFormFields.START_DATE}
                      label={detailFormLabels[DetailFormFields.START_DATE]}
                      required
                      disabled={!editMode}
                    />
                  </Grid>
                  <Grid item xs={1}>
                    <DatePickerInput
                      name={DetailFormFields.END_DATE}
                      label={detailFormLabels[DetailFormFields.END_DATE]}
                      disabled={!editMode || !id}
                    />
                  </Grid>
                  <Grid item xs={1}>
                    <Input
                      name={DetailFormFields.CREATOR}
                      label={detailFormLabels[DetailFormFields.CREATOR]}
                      value={values[DetailFormFields.CREATOR]}
                      disabled
                    />
                  </Grid>
                  <Grid item xs={1}>
                    <Input
                      name={DetailFormFields.EDITOR}
                      label={detailFormLabels[DetailFormFields.EDITOR]}
                      value={values[DetailFormFields.EDITOR]}
                      disabled
                    />
                  </Grid>
                </Grid>
              </Form>
            );
          }}
        </Formik>
      </TabPanel>
    </Page>
  );
};

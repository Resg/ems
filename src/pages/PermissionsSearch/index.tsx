import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { FormikProps } from 'formik';
import { useSelector } from 'react-redux';

import {
  ChevronRight,
  ClearAll,
  ControlPointDuplicate,
  DeleteOutline,
  DeleteSweep,
  ExpandMore,
  Refresh,
  Search,
  Task,
  Verified,
} from '@mui/icons-material';
import { Button, Tab, Tabs } from '@mui/material';

import Card from '../../components/Card';
import { DataTable } from '../../components/CustomDataGrid';
import { FilterBar, FormValues } from '../../components/FilterBarNew';
import { MenuButtonItem } from '../../components/MenuButton/MenuButtonItem';
import { RoundButton } from '../../components/RoundButton';
import { TabPanel } from '../../components/TabPanel';
import {
  DeleteToolButton,
  SettingsTypes,
  ToolButton,
  ToolsPanel,
} from '../../components/ToolsPanel';
import { MenuToolButton } from '../../components/ToolsPanel/MenuToolButton';
import { PermissionCols } from '../../constants/permissions';
import {
  PermissionClearMap,
  PermissionSearchLabel,
  PermissionSearchTabs,
} from '../../constants/permissionSearch';
import { Application } from '../../containers/PermissionSearch/PermissionSearchTabs/Application';
import { Numbers } from '../../containers/PermissionSearch/PermissionSearchTabs/Numbers';
import { Permission } from '../../containers/PermissionSearch/PermissionSearchTabs/Permission';
import {
  useDeletePermissionMutation,
  useGetPermissionsQuery,
} from '../../services/api/permissions';
import { RootState, useAppDispatch } from '../../store';
import { setLocationData } from '../../store/utils';
import { prepareData } from '../../utils/permissionSearch';

import styles from './styles.module.scss';

export const PermissionsSearchPage: React.FC = () => {
  const filters = useSelector(
    (state: RootState) => state.filters.data['permission-search'] || {}
  );
  const dispatch = useAppDispatch();
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [tab, setTab] = useState(PermissionSearchTabs.PERMISSION);
  const { elasticSearch }: { elasticSearch?: string } = filters;
  const {
    data: permissions,
    refetch: refetchPermissions,
    isLoading,
  } = useGetPermissionsQuery(
    {
      searchParameters: filters,
      page,
      size: perPage,
    },
    { skip: !elasticSearch && !Object.keys(filters).length }
  );
  const [deletePermission] = useDeletePermissionMutation();

  const tabChange = useCallback(
    (event: React.SyntheticEvent, value: PermissionSearchTabs) => setTab(value),
    []
  );

  const [cols, setCols] = useState(PermissionCols);
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const selectedRows = useMemo(() => {
    return (
      Object.keys(selected).filter(
        (selectedKey) => selected[Number(selectedKey)]
      ) || []
    );
  }, [selected]);

  const clearTab = useCallback(
    (
      setFieldValue: (
        field: string,
        value: any,
        shouldValidate?: boolean
      ) => void
    ) => {
      const fields: { [key: string]: any } = PermissionClearMap[tab];

      for (const key in fields) {
        setFieldValue(key, fields[key]);
      }
    },
    [tab]
  );

  const createFilterButtons = useCallback(
    (
      onSearch: ({ values }: { values: FormValues }) => () => Promise<void>,
      props: FormikProps<FormValues>
    ) => {
      const { resetForm, values, setFieldValue } = props;

      return (
        <>
          <Button
            color="primary"
            variant="contained"
            startIcon={<Search />}
            onClick={onSearch({ values: prepareData(values as FormValues) })}
          >
            Поиск
          </Button>
          <Button
            variant="outlined"
            startIcon={<ClearAll />}
            onClick={() => clearTab(setFieldValue)}
          >
            Очистить вкладку
          </Button>
          <Button
            color="primary"
            variant="outlined"
            startIcon={<DeleteSweep />}
            onClick={() => resetForm()}
          >
            Очистить всё
          </Button>
        </>
      );
    },
    [clearTab]
  );

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const openPermission = useCallback(() => {
    window
      .open(
        `/permissions?id=${selectedRows[0]}`,
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  }, [selectedRows, openFormInNewTab]);

  const handleDelete = useCallback(async () => {
    await deletePermission({ id: selectedRows[0] });
    setSelected({});
    refetchPermissions();
  }, [selectedRows]);

  useEffect(() => {
    dispatch(setLocationData({ menuName: ['Разрешения', 'Поиск разрешений'] }));
  }, []);

  return (
    <>
      <div className={styles.title}>Поиск разрешений</div>
      <FilterBar
        formKey="permission-search"
        placeholder="Номер разрешения"
        refetchSearch={() => refetchPermissions()}
        createFilterButtons={createFilterButtons}
      >
        <Tabs value={tab} onChange={tabChange}>
          <Tab
            label={PermissionSearchLabel[PermissionSearchTabs.PERMISSION]}
            value={PermissionSearchTabs.PERMISSION}
          />
          <Tab
            label={PermissionSearchLabel[PermissionSearchTabs.LIST_OF_NUMBERS]}
            value={PermissionSearchTabs.LIST_OF_NUMBERS}
          />
          <Tab
            label={PermissionSearchLabel[PermissionSearchTabs.REQUEST]}
            value={PermissionSearchTabs.REQUEST}
          />
        </Tabs>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={PermissionSearchTabs.PERMISSION}
        >
          <Permission />
        </TabPanel>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={PermissionSearchTabs.REQUEST}
        >
          <Application />
        </TabPanel>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={PermissionSearchTabs.LIST_OF_NUMBERS}
        >
          <Numbers />
        </TabPanel>
      </FilterBar>
      <Card className={styles.card}>
        <DataTable
          formKey={'pages_permissions_search'}
          rows={permissions?.data || []}
          cols={cols}
          onRowSelect={setSelected}
          loading={isLoading}
          height="fullHeight"
          pagerProps={{
            page,
            setPage,
            perPage,
            setPerPage,
            total: permissions?.totalCount || 0,
          }}
        >
          <ToolsPanel
            className={styles.panel}
            leftActions={
              <RoundButton
                icon={<Refresh />}
                onClick={() => refetchPermissions()}
              />
            }
            settings={[SettingsTypes.COLS]}
          >
            <ToolButton
              label={'подробно'}
              startIcon={<ChevronRight />}
              disabled={selectedRows.length !== 1}
              onClick={openPermission}
              fast
            />
            <MenuToolButton
              label={'добавить'}
              endIcon={<ExpandMore />}
              disabled
            >
              <MenuButtonItem icon={<Task />} disabled>
                На основаниии заключения
              </MenuButtonItem>
              <MenuButtonItem icon={<Verified />} disabled>
                На основаниии разрешения
              </MenuButtonItem>
              <MenuButtonItem icon={<ControlPointDuplicate />} disabled>
                Дополнение к разрешению
              </MenuButtonItem>
            </MenuToolButton>
            <DeleteToolButton
              label={'Удалить'}
              startIcon={<DeleteOutline />}
              disabled={selectedRows.length !== 1}
              fast
              description={`Вы действительно хотите удалить разрешение?`}
              title={'Удаление разрешения'}
              onConfirm={handleDelete}
            />
          </ToolsPanel>
        </DataTable>
      </Card>
    </>
  );
};

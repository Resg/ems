import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';

import {
  Assignment,
  Cancel,
  DeleteOutline,
  Done,
  Refresh,
  Send,
  SummarizeRounded,
  TaskAlt,
} from '@mui/icons-material';

import Card from '../../components/Card';
import { TreeDataGrid } from '../../components/CustomDataGrid';
import { ExecuteButton } from '../../components/Execution';
import {
  ExecutionButtonActions,
  ExecutionButtonIcons,
  ExecutionFastButtons,
  ExecutionStatusIds,
} from '../../components/Execution/ExecutionButton/constants';
import { FilterBar } from '../../components/FilterBarNew';
import { ExcelIcon } from '../../components/Icons';
import { ResolutionToolButton } from '../../components/Resolution';
import { RoundButton } from '../../components/RoundButton';
import {
  DeleteToolButton,
  ToolButton,
  ToolsPanel,
} from '../../components/ToolsPanel';
import { ModalToolButton } from '../../components/ToolsPanel/ModalToolButton';
import { IncomingIncomingDocumentExecutionButtons } from '../../constants/incomingDocumentForm';
import { Routes } from '../../constants/routes';
import {
  useDeleteTaskMutation,
  useGetIncomingDocumentTasksQuery,
  useGetTaskInWorkMutation,
  useSendToAcquaintedMutation,
  useSendToContinueMutation,
  useSendToExecutionMutation,
  useSendToReexecutionMutation,
} from '../../services/api/incomingDocumentTasks';
import { RootState, useAppDispatch } from '../../store';
import { setLocationData } from '../../store/utils';
import { Task } from '../../types/incomingDocumentTasks';
import {
  makeIncomingDocumentTasksColumns,
  massServerQuery,
  prepareTasksForGrid,
} from '../../utils/incomingDocumentTasks';

import styles from './styles.module.scss';

const groups = ['performer', 'type'];

interface idsObjectType {
  ids: number[];
  documentIds: number[];
  resolutionId: (number | null)[];
}

const cols = makeIncomingDocumentTasksColumns();
export const IncomingTaskPage = React.memo(() => {
  const filters = useSelector(
    (state: RootState) => state.filters.data['tasks-documents-chipses'] || {}
  );
  const dispatch = useAppDispatch();
  const { data, refetch, isLoading } = useGetIncomingDocumentTasksQuery({
    searchParameters: { ...filters, inWorkManual: false },
  });

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );
  const rowDoubleHandler = useCallback(
    (e: any) => {
      if (e.documentId) {
        window
          .open(
            Routes.DOCUMENTS.DOCUMENT.replace(':id', e.documentId),
            openFormInNewTab ? '_blank' : '_self'
          )
          ?.focus();
      }
    },
    [openFormInNewTab]
  );

  const dataRows = useMemo(() => prepareTasksForGrid(data?.data || []), [data]);

  const [tableFilters, setFilters] = useState(false);
  const [selectedRows, setSelectedRows] = useState<Record<string, boolean>>({});
  const [getTaskInWork] = useGetTaskInWorkMutation();
  const [deleteTask] = useDeleteTaskMutation();
  const [sendToExecution] = useSendToExecutionMutation();
  const [sendToContinue] = useSendToContinueMutation();
  const [sendToAcquainted] = useSendToAcquaintedMutation();
  const [reexecution] = useSendToReexecutionMutation();
  const handleWorkRequest = useCallback(async () => {
    massServerQuery(idsObject.ids, getTaskInWork).then(() => refetch());
  }, [selectedRows]);

  const rowsMap = useMemo(
    () =>
      (data?.data || []).reduce((acc, row) => {
        acc[row.id] = row;
        return acc;
      }, {} as Record<number, Task>),
    [data?.data]
  );

  const idsObject = useMemo(() => {
    const result: idsObjectType = {
      ids: [],
      documentIds: [],
      resolutionId: [],
    };

    Object.keys(selectedRows).forEach((key) => {
      const numberKey = Number(key);

      if (selectedRows[key] && !isNaN(numberKey) && rowsMap[numberKey]) {
        result.ids.push(rowsMap[numberKey].id);
        result.documentIds.push(rowsMap[numberKey].documentId);
        result.resolutionId.push(rowsMap[numberKey].resolutionId);
      }
    });

    return result;
  }, [rowsMap, selectedRows]);

  const openPage = useCallback(
    (path: string = '') => {
      window.open(`${path}`, openFormInNewTab ? '_blank' : '_self')?.focus();
    },
    [selectedRows, openFormInNewTab]
  );

  useEffect(() => {
    dispatch(
      setLocationData({
        menuName: ['Задачи по документам', 'Поступившие документы'],
      })
    );
  }, []);

  const handleDelete = useCallback(async () => {
    await deleteTask(idsObject.ids[0]);
    setSelectedRows({});
    refetch();
  }, [selectedRows, deleteTask, idsObject]);

  const handleExecute = useCallback(async () => {
    await sendToExecution(idsObject.ids[0]);
    setSelectedRows({});
    refetch();
  }, [selectedRows, sendToExecution, idsObject]);

  const handleClose = useCallback(async () => {
    await sendToContinue({ id: idsObject.ids[0] });
    setSelectedRows({});
    refetch();
  }, [selectedRows, sendToContinue, idsObject]);

  const handleReexecute = useCallback(async () => {
    await reexecution(idsObject.ids[0]);
    setSelectedRows({});
    refetch();
  }, [selectedRows, reexecution, idsObject]);

  const closeDisabled = useMemo(() => {
    return !(
      idsObject.ids.length === 1 && rowsMap[idsObject.ids[0]]['canClose']
    );
  }, [idsObject, rowsMap]);

  const handleAcquainted = useCallback(async () => {
    massServerQuery(idsObject.ids, sendToAcquainted).then(() => refetch());
  }, [selectedRows, sendToContinue]);

  const acquaintedDisabled = useMemo(() => {
    return !(
      idsObject.ids.length !== 0 && rowsMap[idsObject.ids[0]]['canMarkAsRead']
    );
  }, [idsObject, rowsMap]);

  return (
    <>
      <h1 className={styles.title}>Поступившие документы</h1>
      <FilterBar
        formKey="tasks-documents-chipses"
        refetchSearch={() => refetch()}
        placeholder="Штрих-код/Номер документа"
      />
      <Card className={styles.card}>
        <TreeDataGrid
          formKey={'received_documents'}
          cols={cols}
          showFilters={tableFilters}
          rows={dataRows}
          groups={groups}
          loading={isLoading}
          onRowDoubleClick={rowDoubleHandler}
          onRowSelect={setSelectedRows}
        >
          <ToolsPanel
            setFilters={setFilters}
            className={styles.panel}
            leftActions={
              <>
                <RoundButton icon={<Refresh />} onClick={refetch} />
                <RoundButton icon={<ExcelIcon />} />
              </>
            }
          >
            {[
              ...IncomingIncomingDocumentExecutionButtons.map((action) => {
                const ButtonIcon = ExecutionButtonIcons[action];
                return action === ExecutionButtonActions.INWORK ? (
                  <ToolButton
                    label={ExecutionButtonActions.INWORK}
                    onClick={handleWorkRequest}
                    disabled={!idsObject.ids.length}
                    startIcon={<ButtonIcon />}
                    key={ExecutionButtonActions.INWORK}
                  />
                ) : (
                  <ExecuteButton
                    startIcon={<ButtonIcon />}
                    statusId={ExecutionStatusIds[action]}
                    rows={idsObject.ids.map((id) => rowsMap[id] as Task)}
                    ids={idsObject.ids}
                    label={action}
                    key={action}
                    fast={ExecutionFastButtons.includes(action)}
                  />
                );
              }),
              <ToolButton
                label={ExecutionButtonActions.CLOSE}
                onClick={handleClose}
                disabled={closeDisabled}
                startIcon={<Cancel />}
                key={ExecutionButtonActions.CLOSE}
              />,
              <ToolButton
                label={ExecutionButtonActions.ACQUAINTED}
                onClick={handleAcquainted}
                disabled={acquaintedDisabled}
                startIcon={<TaskAlt />}
                key={ExecutionButtonActions.ACQUAINTED}
              />,
              <ModalToolButton
                label={'Отправить на исполнение'}
                labelButton={'Да'}
                startIcon={<Send />}
                endIcon={<Done />}
                disabled={
                  idsObject.ids.length !== 1 ||
                  !rowsMap[idsObject.ids[0]]['canSendToExecution']
                }
                description={`Отправить выбранную задачу на исполнение?`}
                title={'Подтверждение действия'}
                onConfirm={handleExecute}
              />,
              <DeleteToolButton
                label={'Удалить'}
                startIcon={<DeleteOutline />}
                disabled={
                  idsObject.ids.length !== 1 ||
                  !rowsMap[idsObject.ids[0]]['canDelete']
                }
                description={`Удалить выбранную задачу?`}
                title={'Подтверждение действия'}
                onConfirm={handleDelete}
              />,
              <ToolButton
                label="Открыть документ"
                startIcon={<SummarizeRounded />}
                disabled={idsObject.documentIds.length !== 1}
                onClick={() =>
                  idsObject.documentIds &&
                  openPage(`/documents?id=${idsObject.documentIds[0]}`)
                }
                fast
              />,
              <ToolButton
                label="Открыть задачу"
                startIcon={<Assignment />}
                disabled={idsObject.ids.length !== 1}
                onClick={() =>
                  idsObject.ids &&
                  openPage(`/tasks/details/${idsObject.ids[0]}`)
                }
                fast
              />,
              <ResolutionToolButton
                label={'Открыть резолюцию'}
                documentIds={idsObject.documentIds}
                refetch={refetch}
                disabled={
                  idsObject.documentIds.length !== 1 ||
                  !idsObject.resolutionId[0]
                }
                id={idsObject.resolutionId[0]}
              />,
              <ResolutionToolButton
                label={'Создать резолюцию'}
                documentIds={idsObject.documentIds}
                refetch={refetch}
              />,
            ]}
          </ToolsPanel>
        </TreeDataGrid>
      </Card>
    </>
  );
});

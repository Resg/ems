import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';

import {
  ArrowBack,
  Assignment,
  ChevronRight,
  DeleteOutline,
  Done,
  Refresh,
  Send,
  SummarizeRounded,
} from '@mui/icons-material';

import Card from '../../components/Card';
import { TreeDataGrid } from '../../components/CustomDataGrid';
import { ExecuteButton } from '../../components/Execution';
import {
  ExecutionButtonIcons,
  ExecutionFastButtons,
  ExecutionStatusIds,
} from '../../components/Execution/ExecutionButton/constants';
import { ExcelIcon } from '../../components/Icons';
import { MenuButtonItem } from '../../components/MenuButton/MenuButtonItem';
import { RoundButton } from '../../components/RoundButton';
import {
  DeleteToolButton,
  ToolButton,
  ToolsPanel,
} from '../../components/ToolsPanel';
import { MenuToolButton } from '../../components/ToolsPanel/MenuToolButton';
import { ModalToolButton } from '../../components/ToolsPanel/ModalToolButton';
import {
  IncomingMyTasksExecutionButtons,
  MyTasksColumns,
} from '../../constants/myTasks';
import { Routes } from '../../constants/routes';
import {
  useDeleteTaskMutation,
  useRevokeTaskMutation,
  useSendToExecutionMutation,
  useSendToReexecutionMutation,
} from '../../services/api/incomingDocumentTasks';
import { useGetMyDocumentTasksQuery } from '../../services/api/myDocumentTasks';
import { RootState, useAppDispatch } from '../../store';
import { setLocationData } from '../../store/utils';
import { MyTasksType } from '../../types/myDocumentTasks';
import { prepareMyTasksForGrid } from '../../utils/myDocumentTasks';

import styles from './styles.module.scss';

const groups = ['performer', 'title'];

interface idsObjectType {
  ids: number[];
  documentIds: number[];
}

export const MyTasksPage: React.FC = () => {
  const dispatch = useAppDispatch();
  const [deleteTask] = useDeleteTaskMutation();
  const [sendToReexecution] = useSendToExecutionMutation();
  const [revokeTask] = useRevokeTaskMutation();
  const [reexecution] = useSendToReexecutionMutation();
  const [selectedRows, setSelectedRows] = useState<Record<string, boolean>>({});
  const [tableFilters, setTableFilters] = useState(false);
  const { data, isLoading, refetch } = useGetMyDocumentTasksQuery({
    type: 'DOCUMENT',
  });

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const rowDoubleHandler = useCallback(
    (e: any) => {
      if (e.rootObjectId) {
        window
          .open(
            Routes.DOCUMENTS.DOCUMENT.replace(':id', e.rootObjectId),
            openFormInNewTab ? '_blank' : '_self'
          )
          ?.focus();
      }
    },
    [openFormInNewTab]
  );

  const rowsMap = useMemo(
    () =>
      (data?.data || []).reduce((acc, row) => {
        acc[row.id] = row;
        return acc;
      }, {} as Record<number, MyTasksType>),
    [data?.data]
  );

  const idsObject = useMemo(() => {
    const result: idsObjectType = {
      ids: [],
      documentIds: [],
    };

    Object.keys(selectedRows).forEach((key) => {
      const numberKey = Number(key);

      if (selectedRows[key] && !isNaN(numberKey) && rowsMap[numberKey]) {
        result.ids.push(rowsMap[numberKey].id);
        result.documentIds.push(Number(rowsMap[numberKey].rootObjectId));
      }
    });

    return result;
  }, [rowsMap, selectedRows]);

  const openPage = useCallback(
    (path: string = '') => {
      window.open(`${path}`, openFormInNewTab ? '_blank' : '_self')?.focus();
    },
    [selectedRows, openFormInNewTab]
  );

  const dataRows = useMemo(
    () => prepareMyTasksForGrid(data?.data || []),
    [data]
  );

  useEffect(() => {
    dispatch(
      setLocationData({
        menuName: ['Задачи по документам', 'Поставленные мной'],
      })
    );
  }, []);

  const handleDelete = useCallback(async () => {
    await deleteTask(idsObject.ids[0]);
    setSelectedRows({});
    refetch();
  }, [idsObject]);

  const handleExecute = useCallback(async () => {
    await sendToReexecution(idsObject.ids[0]);
    setSelectedRows({});
    refetch();
  }, [idsObject]);

  const handleRevoke = useCallback(async () => {
    await revokeTask(idsObject.ids[0]);
    setSelectedRows({});
    refetch();
  }, [idsObject]);

  const handleReexecute = useCallback(async () => {
    await reexecution(idsObject.ids[0]);
    setSelectedRows({});
    refetch();
  }, [idsObject]);

  return (
    <>
      <h1 className={styles.title}>Поставленные мной</h1>
      <Card className={styles.card}>
        <TreeDataGrid
          formKey={'delivered_by_me'}
          cols={MyTasksColumns}
          showFilters={tableFilters}
          rows={dataRows}
          groups={groups}
          loading={isLoading}
          onRowDoubleClick={rowDoubleHandler}
          onRowSelect={setSelectedRows}
        >
          <ToolsPanel
            className={styles.panel}
            setFilters={setTableFilters}
            leftActions={
              <>
                <RoundButton icon={<Refresh />} />
                <RoundButton icon={<ExcelIcon />} />
              </>
            }
          >
            {[
              ...IncomingMyTasksExecutionButtons.map((action) => {
                const ButtonIcon = ExecutionButtonIcons[action];
                return (
                  <ExecuteButton
                    startIcon={<ButtonIcon />}
                    statusId={ExecutionStatusIds[action]}
                    rows={idsObject.ids.map((id) => rowsMap[id] as MyTasksType)}
                    ids={idsObject.ids}
                    label={action}
                    key={action}
                    fast={ExecutionFastButtons.includes(action)}
                  />
                );
              }),
              <MenuToolButton
                label="Подробно"
                startIcon={<ChevronRight />}
                fast
              >
                <MenuButtonItem
                  icon={<SummarizeRounded />}
                  disabled={idsObject.documentIds.length !== 1}
                  onClick={() =>
                    idsObject.documentIds &&
                    openPage(`/documents?id=${idsObject.documentIds[0]}`)
                  }
                >
                  Документ
                </MenuButtonItem>
                <MenuButtonItem
                  icon={<Assignment />}
                  disabled={idsObject.ids.length !== 1}
                  onClick={() =>
                    idsObject.ids &&
                    openPage(`/tasks/details/${idsObject.ids[0]}`)
                  }
                >
                  Задача
                </MenuButtonItem>
              </MenuToolButton>,
              <ToolButton
                label="Отозвать на доработку"
                startIcon={<ArrowBack />}
                disabled={idsObject.documentIds.length !== 1}
                onClick={handleRevoke}
              />,
              <ModalToolButton
                label={'Отправить на исполнение'}
                labelButton={'Да'}
                startIcon={<Send />}
                endIcon={<Done />}
                disabled={
                  idsObject.ids.length !== 1 ||
                  !rowsMap[idsObject.ids[0]]['canSendToExecution']
                }
                description={`Отправить выбранную задачу на исполнение?`}
                title={'Подтверждение действия'}
                onConfirm={handleExecute}
              />,
              <DeleteToolButton
                label={'Удалить'}
                startIcon={<DeleteOutline />}
                disabled={
                  idsObject.ids.length !== 1 ||
                  !rowsMap[idsObject.ids[0]]['canDelete']
                }
                description={`Удалить выбранную задачу?`}
                title={'Подтверждение действия'}
                onConfirm={handleDelete}
              />,
            ]}
          </ToolsPanel>
        </TreeDataGrid>
      </Card>
    </>
  );
};

import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  ChevronRight,
  DeleteOutline,
  Done,
  Refresh,
  Send,
} from '@mui/icons-material';

import Card from '../../components/Card';
import { TreeDataGrid } from '../../components/CustomDataGrid';
import { ExecuteButton } from '../../components/Execution';
import {
  ExecutionButtonActions,
  ExecutionButtonIcons,
  ExecutionFastButtons,
  ExecutionStatusIds,
} from '../../components/Execution/ExecutionButton/constants';
import { FilterBar } from '../../components/FilterBarNew';
import { ExcelIcon } from '../../components/Icons';
import { ResolutionToolButton } from '../../components/Resolution';
import { RoundButton } from '../../components/RoundButton';
import {
  DeleteToolButton,
  ToolButton,
  ToolsPanel,
} from '../../components/ToolsPanel';
import { ModalToolButton } from '../../components/ToolsPanel/ModalToolButton';
import { Routes } from '../../constants/routes';
import {
  useDeleteTaskMutation,
  useGetIncomingDocumentTasksQuery,
  useRevokeTasksMutation,
  useSendToExecutionMutation,
  useSendToReexecutionMutation,
} from '../../services/api/incomingDocumentTasks';
import { RootState } from '../../store';
import { setLocationData } from '../../store/utils';
import { Task } from '../../types/incomingDocumentTasks';
import {
  makeIncomingDocumentTasksColumns,
  prepareTasksForGrid,
} from '../../utils/incomingDocumentTasks';

import styles from '../IncomingTasks/styles.module.scss';

const groups = ['performer', 'type'];
const cols = makeIncomingDocumentTasksColumns();
export const InworkTaskPage = React.memo(() => {
  const dispatch = useDispatch();
  const filters = useSelector(
    (state: RootState) => state.filters.data['tasks-inwork-chipses'] || {}
  );
  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );
  const { data, refetch, isLoading } = useGetIncomingDocumentTasksQuery({
    searchParameters: { ...filters, inWorkManual: true },
  });

  const rowDoubleHandler = useCallback(
    (e: any) => {
      if (e.documentId) {
        window
          .open(
            Routes.DOCUMENTS.DOCUMENT.replace(':id', e.documentId),
            openFormInNewTab ? '_blank' : '_self'
          )
          ?.focus();
      }
    },
    [openFormInNewTab]
  );

  const dataRows = useMemo(() => prepareTasksForGrid(data?.data || []), [data]);

  const [tableFilters, setTableFilters] = useState(false);
  const [cols, setCols] = useState(makeIncomingDocumentTasksColumns());

  const [selectedRows, setSelectedRows] = useState<Record<string, boolean>>({});
  const [revokeTasks] = useRevokeTasksMutation();
  const [deleteTask] = useDeleteTaskMutation();
  const [sendToExecution] = useSendToExecutionMutation();
  const [reexecution] = useSendToReexecutionMutation();

  const visibleColumns = useMemo(
    () => cols.filter((col) => !col.hidden),
    [cols]
  );

  const rowsMap = useMemo(
    () =>
      (data?.data || []).reduce((acc, row) => {
        acc[row.id] = row;
        return acc;
      }, {} as Record<number, Task>),
    [data?.data]
  );

  const idsObject = useMemo(() => {
    const result: Record<string, number[]> = {
      ids: [],
      documentIds: [],
      resolutionId: [],
    };
    Object.keys(selectedRows).forEach((key) => {
      const numberKey = Number(key);
      if (selectedRows[key] && !isNaN(numberKey) && rowsMap[numberKey]) {
        result.ids.push(rowsMap[numberKey].id);
        result.documentIds.push(rowsMap[numberKey].documentId);
      }
    });
    return result;
  }, [rowsMap, selectedRows]);

  const handleWorkRequest = useCallback(async () => {
    revokeTasks({ ids: idsObject.id }).then(() => refetch());
  }, [selectedRows, idsObject]);

  useEffect(() => {
    dispatch(
      setLocationData({ menuName: ['Задачи по документам', 'В работе'] })
    );
  }, []);

  const handleDelete = useCallback(async () => {
    await deleteTask(idsObject.ids[0]);
    setSelectedRows({});
    refetch();
  }, [idsObject]);

  const handleExecute = useCallback(async () => {
    await sendToExecution(idsObject.ids[0]);
    setSelectedRows({});
    refetch();
  }, [idsObject]);

  const handleReexecute = useCallback(async () => {
    await reexecution(idsObject.ids[0]);
    setSelectedRows({});
    refetch();
  }, [idsObject]);

  return (
    <>
      <h1 className={styles.title}>В работе</h1>
      <FilterBar
        formKey="tasks-inwork-chipses"
        refetchSearch={() => refetch()}
      />
      <Card className={styles.card}>
        <TreeDataGrid
          formKey={'tasks_in_work'}
          showFilters={tableFilters}
          cols={cols}
          rows={dataRows}
          groups={groups}
          loading={isLoading}
          onRowDoubleClick={rowDoubleHandler}
          onRowSelect={setSelectedRows}
        >
          <ToolsPanel
            setFilters={setTableFilters}
            className={styles.panel}
            leftActions={
              <>
                <RoundButton icon={<Refresh />} />
                <RoundButton icon={<ExcelIcon />} />
              </>
            }
          >
            {[
              <ToolButton
                label={'Подробно'}
                startIcon={<ChevronRight />}
                fast
              />,
              ...Object.values(ExecutionButtonActions).map((action) => {
                const ButtonIcon = ExecutionButtonIcons[action];
                return action === ExecutionButtonActions.INWORK ? (
                  <ToolButton
                    label={'Cнять с работы'}
                    onClick={handleWorkRequest}
                    disabled={!idsObject.ids.length}
                    startIcon={<ButtonIcon />}
                    key={ExecutionButtonActions.INWORK}
                  />
                ) : (
                  <ExecuteButton
                    startIcon={<ButtonIcon />}
                    statusId={ExecutionStatusIds[action]}
                    rows={idsObject.ids.map((id) => rowsMap[id] as Task)}
                    ids={idsObject.ids}
                    label={action}
                    key={action}
                    fast={ExecutionFastButtons.includes(action)}
                  />
                );
              }),
              <ModalToolButton
                label={'Отправить на исполнение'}
                labelButton={'Да'}
                startIcon={<Send />}
                endIcon={<Done />}
                disabled={
                  idsObject.ids.length !== 1 ||
                  !rowsMap[idsObject.ids[0]]['canSendToExecution']
                }
                description={`Отправить выбранную задачу на исполнение?`}
                title={'Подтверждение действия'}
                onConfirm={handleExecute}
              />,
              <DeleteToolButton
                label={'Удалить'}
                startIcon={<DeleteOutline />}
                disabled={
                  idsObject.ids.length !== 1 ||
                  !rowsMap[idsObject.ids[0]]['canDelete']
                }
                description={`Удалить выбранную задачу?`}
                title={'Подтверждение действия'}
                onConfirm={handleDelete}
              />,
              <ResolutionToolButton
                label={'Открыть резолюцию'}
                documentIds={idsObject.documentIds}
                refetch={refetch}
                disabled={
                  idsObject.documentIds.length !== 1 ||
                  !idsObject.resolutionId[0]
                }
                id={idsObject.resolutionId[0]}
              />,
              <ResolutionToolButton
                label={'Создать резолюцию'}
                documentIds={idsObject.documentIds}
              />,
            ]}
          </ToolsPanel>
        </TreeDataGrid>
      </Card>
    </>
  );
});

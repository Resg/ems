import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { Container } from '../../components/Container';
import { setLocationData } from '../../store/utils';

const MainPage = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(setLocationData({ mainPage: true }));
  }, []);
  return <Container>Главная страница</Container>;
};

export default MainPage;

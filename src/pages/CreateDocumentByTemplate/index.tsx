import React, { useEffect } from 'react';
import { useSearchParams } from 'react-router-dom';

import Card from '../../components/Card';
import { CreateDocumentByTemplateForm } from '../../components/CreateDocumentByTemplateForm';
import { useAppDispatch } from '../../store';
import { setLocationData } from '../../store/utils';

export const CreateByTemplatePage: React.FC = () => {
  const [searchParams] = useSearchParams();
  const objectTypeCode = searchParams.get('objectTypeCode');
  const objectIds = searchParams.get('objectId');
  const dispatch = useAppDispatch();
  const precreationObjectTypeCode = searchParams.get(
    'precreationObjectTypeCode'
  );

  useEffect(() => {
    dispatch(setLocationData({ singleName: 'Создание документа по шаблону' }));
  }, []);

  return (
    <Card>
      <CreateDocumentByTemplateForm
        objectTypeCode={objectTypeCode}
        objectIds={objectIds}
        precreationObjectTypeCode={precreationObjectTypeCode}
      />
    </Card>
  );
};

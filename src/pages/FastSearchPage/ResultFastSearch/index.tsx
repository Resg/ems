import { useCallback, useMemo, useState } from 'react';

import { Card, Grid, Typography } from '@mui/material';

import Pager from '../../../components/CustomDataGrid/Pager';
import { useAppSelector } from '../../../store';
import { SearchResultItem } from '../../../types/fastSearch';

import styles from './styles.module.scss';

interface ResultFastSearchProps {
  searchResult: SearchResultItem[];
}

export const ResultFastSearch: React.FC<ResultFastSearchProps> = ({
  searchResult,
}) => {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const searchValue = useAppSelector(
    (state) => state.fastSearch.searchValueData
  );
  const currentPage = useMemo(() => {
    const firstPageIndex = (page - 1) * perPage;
    const lastPageIndex = firstPageIndex + perPage;
    return searchResult.slice(firstPageIndex, lastPageIndex);
  }, [page, perPage, searchResult]);

  const openFormInNewTab = useAppSelector(
    (state) => state.utils.openFormInNewTab
  );

  const openPage = useCallback(
    (path: string = '') => {
      window.open(`${path}`, openFormInNewTab ? '_blank' : '_self')?.focus();
    },
    [openFormInNewTab]
  );

  const resultHandle = (type: string, id: number) => {
    const resultType = type === 'DOCUMENT' ? 'documents' : 'requests';
    openPage(`${resultType}/${id}`);
  };

  const changeBold = (text: string) => {
    const index = text.indexOf(searchValue);
    if (index !== -1) {
      return (
        <>
          {text.substring(0, index)}
          <Typography
            component="span"
            fontWeight="bold"
            className={styles.links}
          >
            {text.substring(index, index + searchValue.length)}
          </Typography>
          {text.substring(index + searchValue.length)}
        </>
      );
    }
    return text;
  };

  return (
    <div className={styles.container}>
      <Grid container>
        {currentPage.map((request) => (
          <Grid item xs={12} key={request.objectId}>
            <Card
              className={styles.card}
              onClick={() => resultHandle(request.objectType, request.objectId)}
            >
              <Typography className={styles.title}>
                {request.objectType === 'DOCUMENT' ? 'Документ' : 'Заявка'}
              </Typography>
              <Typography
                key={request.objectId}
                noWrap={true}
                gutterBottom
                className={styles.links}
              >
                {changeBold(request.description)}
              </Typography>
            </Card>
          </Grid>
        ))}
      </Grid>

      <Pager
        total={searchResult.length}
        page={page}
        perPage={perPage}
        setPerPage={setPerPage}
        setPage={setPage}
        loading={false}
        removeBorderStyles={true}
      ></Pager>
    </div>
  );
};

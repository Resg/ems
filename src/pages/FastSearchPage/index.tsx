import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { useAppSelector } from '../../store';
import { setLocationData } from '../../store/utils';
import { SearchResult } from '../../types/fastSearch';

import { ResultFastSearch } from './ResultFastSearch';

import styles from './styles.module.scss';

interface FastSearchPageProps {}

export const FastSearchPage: React.FC<FastSearchPageProps> = () => {
  const dispatch = useDispatch();
  const searchResult = useAppSelector(
    (state) => state.fastSearch.searchResults
  );

  const { data } = searchResult as SearchResult;
  useEffect(() => {
    dispatch(setLocationData({ menuName: ['Результаты поиска'] }));
  }, []);

  return (
    <>
      <h1 className={styles.header}>Результаты поиска</h1>
      <div className={styles.container}>
        {data && <ResultFastSearch searchResult={data} />}
      </div>
    </>
  );
};

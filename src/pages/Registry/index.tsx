import React, { useEffect, useState } from 'react';
import {
  matchPath,
  useLocation,
  useNavigate,
  useParams,
} from 'react-router-dom';

import { Tab, Tabs } from '@mui/material';

import { Page } from '../../components/Page';
import { TabPanel } from '../../components/TabPanel';
import { RegistryTabs, RegistryTabsLabels } from '../../constants/registry';
import { Routes } from '../../constants/routes';
import { Common } from '../../containers/Registry/Common';
import { RegistryDocuments } from '../../containers/Registry/Documents';
import { RegistryPackets } from '../../containers/Registry/Packets';
import { useGetRegistryQuery } from '../../services/api/registries';
import { useAppDispatch } from '../../store';
import { setLocationData } from '../../store/utils';

export const RegistryPage = () => {
  const { tab } = useParams();
  const location = useLocation();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const { id: idForRedirect } = useParams();
  const [tabValue, setTabValue] = useState(tab || RegistryTabs.COMMON);
  const handleTabChange = (
    _e: React.SyntheticEvent,
    newValue: RegistryTabs
  ) => {
    setTabValue(newValue);
  };

  const id =
    Number(
      matchPath(Routes.REGISTRIES.REGISTRY_TABS, location.pathname)?.params?.id
    ) ||
    Number(
      matchPath(Routes.REGISTRIES.REGISTRY, location.pathname)?.params?.id
    ) ||
    0;

  const { data } = useGetRegistryQuery({ id: Number(id) });

  useEffect(() => {
    if (id) {
      navigate(
        Routes.REGISTRIES.REGISTRY_TABS.replace(':id', id).replace(
          ':tab',
          tabValue
        )
      );
    }
  }, [tabValue]);

  useEffect(() => {
    if (idForRedirect) {
      navigate(
        Routes.REGISTRIES.REGISTRY_TABS.replace(':id', idForRedirect).replace(
          ':tab',
          tabValue
        )
      );
    }
  }, [idForRedirect]);

  useEffect(() => {
    dispatch(
      setLocationData({
        name: data ? `Реестр: ${data.number}` : ' Новый реестр',
        data: data,
      })
    );
  }, [data]);

  return (
    <Page title={`Реестр №${data?.number}`}>
      <Tabs value={tabValue} onChange={handleTabChange}>
        <Tab
          label={RegistryTabsLabels[RegistryTabs.COMMON]}
          value={RegistryTabs.COMMON}
        />
        {data && !['IP', 'PP'].includes(data.templateCode) && (
          <Tab
            label={RegistryTabsLabels[RegistryTabs.DOCUMENTS]}
            value={RegistryTabs.DOCUMENTS}
          />
        )}
        {data?.templateCode === 'IP' && (
          <Tab
            label={RegistryTabsLabels[RegistryTabs.OUTCOMING_PACKETS]}
            value={RegistryTabs.OUTCOMING_PACKETS}
          />
        )}
      </Tabs>
      <TabPanel value={tabValue} index={RegistryTabs.COMMON}>
        <Common data={data} />
      </TabPanel>
      <TabPanel value={tabValue} index={RegistryTabs.DOCUMENTS}>
        <RegistryDocuments id={Number(id)} dataRegistry={data} />
      </TabPanel>
      <TabPanel value={tabValue} index={RegistryTabs.OUTCOMING_PACKETS}>
        <RegistryPackets id={Number(id)} dataRegistry={data} />
      </TabPanel>
    </Page>
  );
};

import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

import { Tab, Tabs } from '@mui/material';

import { Page } from '../../components/Page';
import { TabPanel } from '../../components/TabPanel';
import { System } from '../../containers/System';
import { setLocationData } from '../../store/utils';

export const SystemPage = () => {
  const [tabValue, setTabValue] = useState(0);
  const handleTabChange = (_e: React.SyntheticEvent, newValue: number) => {
    setTabValue(newValue);
  };
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setLocationData({ menuName: ['О системе'] }));
  }, []);

  return (
    <Page title={`О системе`}>
      <Tabs value={tabValue} onChange={handleTabChange}>
        <Tab label="Система" value={0} />
      </Tabs>
      <TabPanel value={tabValue} index={0}>
        <System />
      </TabPanel>
    </Page>
  );
};

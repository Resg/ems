import React, { useCallback, useEffect, useState } from 'react';
import { useParams } from 'react-router';

import { Tab, Tabs } from '@mui/material';

import Card from '../../../components/Card';
import { TabPanel } from '../../../components/TabPanel';
import {
  ConclusionReviewTabs,
  ConclusionReviewTabsLabel,
} from '../../../constants/conclusionsReview';
import { Attachments } from '../../../containers/conclusionReview/Attachments';
import { Common } from '../../../containers/conclusionReview/Common';
import { Conditions } from '../../../containers/conclusionReview/Conditions';
import { useGetConclusionDataQuery } from '../../../services/api/conclusions';
import { useAppDispatch } from '../../../store';
import { setLocationData } from '../../../store/utils';

import styles from './styles.module.scss';

export const ConclusionReviewPageById: React.FC = () => {
  const [tab, setTab] = useState(ConclusionReviewTabs.COMMON);
  const { id } = useParams();
  const dispatch = useAppDispatch();

  const { data } = useGetConclusionDataQuery({ id: Number(id) }, { skip: !id });

  const handleChange = useCallback(
    (_event: React.SyntheticEvent, value: ConclusionReviewTabs) => {
      setTab(value);
    },
    []
  );

  useEffect(() => {
    dispatch(
      setLocationData({
        name: data ? `Заключение № ${data.number}` : 'Заключение',
        data: data,
      })
    );
  }, [data]);

  return (
    <>
      <h1>{data ? `Заключение № ${data.number}` : 'Заключение'}</h1>
      <Card className={styles.card}>
        <div className={styles.panel}>
          <Tabs value={tab} onChange={handleChange}>
            <Tab
              label={ConclusionReviewTabsLabel[ConclusionReviewTabs.COMMON]}
              value={ConclusionReviewTabs.COMMON}
            />
            <Tab
              label={ConclusionReviewTabsLabel[ConclusionReviewTabs.CONDITIONS]}
              value={ConclusionReviewTabs.CONDITIONS}
            />
            <Tab
              label={
                ConclusionReviewTabsLabel[ConclusionReviewTabs.ATTACHMENTS]
              }
              value={ConclusionReviewTabs.ATTACHMENTS}
            />
          </Tabs>
          <TabPanel
            className={styles.tabPanel}
            value={tab}
            index={ConclusionReviewTabs.COMMON}
          >
            <Common />
          </TabPanel>
          <TabPanel
            className={styles.tabPanel}
            value={tab}
            index={ConclusionReviewTabs.ATTACHMENTS}
          >
            <Attachments />
          </TabPanel>
          <TabPanel
            className={styles.tabPanel}
            value={tab}
            index={ConclusionReviewTabs.CONDITIONS}
          >
            <Conditions />
          </TabPanel>
        </div>
      </Card>
    </>
  );
};

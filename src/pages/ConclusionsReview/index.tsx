import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { FormikProps } from 'formik';
import { SortColumn } from 'react-data-grid';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';

import {
  ChevronRight,
  DeleteSweep,
  Refresh,
  Search,
} from '@mui/icons-material';
import { Button, Tab, Tabs } from '@mui/material';

import Card from '../../components/Card';
import { DataTable } from '../../components/CustomDataGrid';
import { FilterBar, FormValues } from '../../components/FilterBarNew';
import { RoundButton } from '../../components/RoundButton';
import { TabPanel } from '../../components/TabPanel';
import { ToolButton, ToolsPanel } from '../../components/ToolsPanel';
import {
  ConclusionReviewClearMap,
  ConclusionReviewSearchTabs,
  ConclusionReviewSearchTabsLabel,
  СonclusionReviewCols,
} from '../../constants/conclusionsReview';
import { Conclusion } from '../../containers/ConclusionReviewSearch/ConslusionReviewSearchTabs/Conclusion';
import { useGetConclusionsForFormQuery } from '../../services/api/conclusions';
import { RootState, useAppDispatch } from '../../store';
import { setLocationData } from '../../store/utils';
import { ConclusionToRowAdapter } from '../../utils/conclusionReview';
import { prepareData } from '../../utils/conclusionReview';

import styles from './styles.module.scss';

export const ConclusionReviewPage: React.FC = () => {
  const filters = useSelector(
    (state: RootState) => state.filters.data['conclusion-review-search'] || {}
  );
  const [cols, setCols] = useState(СonclusionReviewCols);
  const [page, setPage] = useState(1);
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const [perPage, setPerPage] = useState(10);
  const [sort, setSort] = useState<SortColumn[]>([]);
  const [tab, setTab] = useState(ConclusionReviewSearchTabs.CONCLUSION);

  const handleChange = useCallback(
    (_event: React.SyntheticEvent, value: ConclusionReviewSearchTabs) => {
      setTab(value);
    },
    []
  );

  const sortRow = useMemo(() => {
    return (
      sort
        ?.map((obj: any) => `${obj.columnKey} ${obj.direction.toLowerCase()}`)
        .join(', ') || ''
    );
  }, [sort]);

  const { data, refetch, isFetching } = useGetConclusionsForFormQuery({
    searchParameters: filters,
    page: page,
    size: perPage,
    sort: sortRow,
  });

  const rows = useMemo(() => {
    return ConclusionToRowAdapter(data?.data) || [];
  }, [data]);

  const selectedConclusions = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const clearTab = useCallback(
    (
      setFieldValue: (
        field: string,
        value: any,
        shouldValidate?: boolean
      ) => void
    ) => {
      const fields: { [key: string]: any } = ConclusionReviewClearMap[tab];

      for (const key in fields) {
        setFieldValue(key, fields[key]);
      }
    },
    [tab]
  );

  const dispatch = useAppDispatch();
  const location = useLocation();

  useEffect(() => {
    dispatch(
      setLocationData({ menuName: ['Заключения', 'Просмотр заключений'] })
    );
  }, []);

  const createFilterButtons = useCallback(
    (
      onSearch: ({ values }: { values: FormValues }) => () => Promise<void>,
      props: FormikProps<FormValues>
    ) => {
      const { resetForm, values, setFieldValue } = props;

      return (
        <>
          <Button
            color="primary"
            variant="contained"
            startIcon={<Search />}
            onClick={onSearch({ values: prepareData(values as FormValues) })}
          >
            Поиск
          </Button>
          <Button
            color="primary"
            variant="outlined"
            startIcon={<DeleteSweep />}
            onClick={() => resetForm()}
          >
            Очистить всё
          </Button>
        </>
      );
    },
    [clearTab]
  );

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const rowDoubleHandler = useCallback(
    (e: any) => {
      if (e.id) {
        window
          .open(
            `/conclusions-review/${e.id}`,
            openFormInNewTab ? '_blank' : '_self'
          )
          ?.focus();
      }
    },
    [openFormInNewTab]
  );

  return (
    <>
      <h1 className={styles.title}>Просмотр заключений</h1>
      <FilterBar
        formKey="conclusion-review-search"
        placeholder="Номер заключения"
        refetchSearch={() => refetch()}
        createFilterButtons={createFilterButtons}
      >
        <Tabs value={tab} onChange={handleChange}>
          <Tab
            label={
              ConclusionReviewSearchTabsLabel[
                ConclusionReviewSearchTabs.CONCLUSION
              ]
            }
            value={ConclusionReviewSearchTabs.CONCLUSION}
          />
        </Tabs>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={ConclusionReviewSearchTabs.CONCLUSION}
        >
          <Conclusion />
        </TabPanel>
      </FilterBar>
      <Card className={styles.card}>
        <DataTable
          formKey={'pages_conclussions_review'}
          cols={cols}
          rows={rows}
          onRowSelect={setSelected}
          onRowDoubleClick={rowDoubleHandler}
          loading={isFetching}
          setSort={setSort}
          height="fullHeight"
          pagerProps={{
            page,
            setPage,
            perPage,
            setPerPage,
            total: data?.totalCount || 0,
          }}
        >
          <ToolsPanel
            className={styles.panel}
            leftActions={
              <RoundButton icon={<Refresh />} onClick={() => refetch()} />
            }
          >
            <ToolButton
              label={'подробно'}
              startIcon={<ChevronRight />}
              disabled={selectedConclusions.length !== 1}
              onClick={() =>
                selectedConclusions.length &&
                window
                  .open(
                    `/conclusions-review/${selectedConclusions[0]}`,
                    openFormInNewTab ? '_blank' : '_self'
                  )
                  ?.focus()
              }
              fast
            />
          </ToolsPanel>
        </DataTable>
      </Card>
    </>
  );
};

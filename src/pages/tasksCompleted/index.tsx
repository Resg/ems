import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { FormikProps } from 'formik';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';

import {
  ChevronRight,
  ClearAll,
  DeleteSweep,
  Refresh,
  Search,
} from '@mui/icons-material';
import { Button, Tab, Tabs } from '@mui/material';

import Card from '../../components/Card';
import { DataTable } from '../../components/CustomDataGrid';
import { FilterBar, FormValues } from '../../components/FilterBarNew';
import { MenuButtonItem } from '../../components/MenuButton/MenuButtonItem';
import { RoundButton } from '../../components/RoundButton';
import { TabPanel } from '../../components/TabPanel';
import { ToolsPanel } from '../../components/ToolsPanel';
import { MenuToolButton } from '../../components/ToolsPanel/MenuToolButton';
import { completedTasksCols } from '../../constants/completedTasks';
import {
  TasksCompletedClearMap,
  TasksCompletedSearchLabel,
  TasksCompletedSearchTabs,
} from '../../constants/tasksSearch';
import { Barcode } from '../../containers/TasksSearch/TasksCompletedTabs/Barcode';
import { Document } from '../../containers/TasksSearch/TasksCompletedTabs/Document';
import { Resolution } from '../../containers/TasksSearch/TasksCompletedTabs/Resolution';
import { Task } from '../../containers/TasksSearch/TasksCompletedTabs/Task';
import { useGetCompletedTasksQuery } from '../../services/api/completedTasks';
import { RootState, useAppDispatch } from '../../store';
import { setLocationData } from '../../store/utils';
import { prepareTaskCompletedData } from '../../utils/tasks';

import styles from './styles.module.scss';

export const CompletedTaskPage: React.FC = () => {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const [tab, setTab] = useState(TasksCompletedSearchTabs.TASK);

  const handleChange = useCallback(
    (_event: React.SyntheticEvent, value: TasksCompletedSearchTabs) => {
      setTab(value);
    },
    []
  );

  const selectedArr = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);
  const [cols, setCols] = useState(completedTasksCols);
  const filters = useSelector(
    (state: RootState) => state.filters.data['tasks-completed'] || {}
  );
  const { data, isLoading, refetch } = useGetCompletedTasksQuery({
    searchParameters: filters,
    page,
    size: perPage,
    useCount: true,
  });

  const visibleColumns = useMemo(
    () => cols.filter((col) => !col.hidden),
    [cols]
  );

  const selectedDocumentId = useMemo(() => {
    return data?.data?.filter((obj) => selectedArr.includes(obj.id));
  }, [selected]);

  const selectedIdLink = useMemo(() => {
    if (selectedDocumentId) {
      return selectedDocumentId[0];
    }
  }, [selected, selectedDocumentId]);

  const preparedDates = useMemo(() => {
    return data?.data.map((task) => ({
      ...task,
      createDate:
        task.createDate && new Date(task.createDate).toLocaleDateString(),
      planDate: task.planDate && new Date(task.planDate).toLocaleDateString(),
      prepareDate:
        task.prepareDate && new Date(task.prepareDate).toLocaleDateString(),
      completeDate:
        task.completeDate && new Date(task.completeDate).toLocaleDateString(),
      documentDate:
        task.documentDate && new Date(task.documentDate).toLocaleDateString(),
    }));
  }, [data]);

  const clearTab = useCallback(
    (
      setFieldValue: (
        field: string,
        value: any,
        shouldValidate?: boolean
      ) => void
    ) => {
      const fields: { [key: string]: any } = TasksCompletedClearMap[tab];

      for (const key in fields) {
        setFieldValue(key, fields[key]);
      }
    },
    [tab]
  );

  const createFilterButtons = useCallback(
    (
      onSearch: ({ values }: { values: FormValues }) => () => Promise<void>,
      props: FormikProps<FormValues>
    ) => {
      const { resetForm, values, setFieldValue } = props;

      return (
        <>
          <Button
            color="primary"
            variant="contained"
            startIcon={<Search />}
            onClick={onSearch({
              values: prepareTaskCompletedData(values as FormValues),
            })}
          >
            Поиск
          </Button>
          <Button
            variant="outlined"
            startIcon={<ClearAll />}
            onClick={() => clearTab(setFieldValue)}
          >
            Очистить вкладку
          </Button>
          <Button
            color="primary"
            variant="outlined"
            startIcon={<DeleteSweep />}
            onClick={() => resetForm()}
          >
            Очистить всё
          </Button>
        </>
      );
    },
    [clearTab]
  );

  const dispatch = useAppDispatch();
  const location = useLocation();

  useEffect(() => {
    dispatch(
      setLocationData({ menuName: ['Задачи по документам', 'Выполненные'] })
    );
  }, [location]);

  return (
    <>
      <h1>Выполненные</h1>
      <FilterBar
        formKey="tasks-completed"
        placeholder="Штрих-код/Номер документа"
        createFilterButtons={createFilterButtons}
        refetchSearch={() => refetch()}
      >
        <Tabs value={tab} onChange={handleChange}>
          <Tab
            label={TasksCompletedSearchLabel[TasksCompletedSearchTabs.TASK]}
            value={TasksCompletedSearchTabs.TASK}
          />
          <Tab
            label={
              TasksCompletedSearchLabel[TasksCompletedSearchTabs.RESOLUTION]
            }
            value={TasksCompletedSearchTabs.RESOLUTION}
          />
          <Tab
            label={TasksCompletedSearchLabel[TasksCompletedSearchTabs.DOCUMENT]}
            value={TasksCompletedSearchTabs.DOCUMENT}
          />
          <Tab
            label={
              TasksCompletedSearchLabel[TasksCompletedSearchTabs.BARCODE_LIST]
            }
            value={TasksCompletedSearchTabs.BARCODE_LIST}
          />
        </Tabs>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={TasksCompletedSearchTabs.TASK}
        >
          <Task />
        </TabPanel>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={TasksCompletedSearchTabs.RESOLUTION}
        >
          <Resolution />
        </TabPanel>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={TasksCompletedSearchTabs.BARCODE_LIST}
        >
          <Barcode />
        </TabPanel>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={TasksCompletedSearchTabs.DOCUMENT}
        >
          <Document />
        </TabPanel>
      </FilterBar>
      <Card className={styles.card}>
        <DataTable
          formKey={'pages_tasks_completed'}
          cols={cols}
          rows={preparedDates || []}
          onRowSelect={setSelected}
          loading={isLoading}
          className={styles.table}
          height="fullHeight"
          pagerProps={{
            page: page,
            setPage: setPage,
            perPage: perPage,
            setPerPage: setPerPage,
            total: data?.totalCount || 0,
          }}
        >
          <ToolsPanel
            leftActions={
              <>
                <RoundButton icon={<Refresh />} />
              </>
            }
            className={styles.panel}
          >
            <MenuToolButton label={'подробнее'} endIcon={<ChevronRight />}>
              <MenuButtonItem
                disabled={selectedArr.length !== 1}
                linkProps={{
                  to: `/documents?id=${selectedIdLink?.documentId}`,
                  newTab: true,
                }}
              >
                Документ
              </MenuButtonItem>
              <MenuButtonItem
                disabled
                linkProps={{
                  to: '', // @TODO: после АПИ на карточку задачи
                  newTab: true,
                }}
              >
                Задача
              </MenuButtonItem>
            </MenuToolButton>
          </ToolsPanel>
        </DataTable>
      </Card>
    </>
  );
};

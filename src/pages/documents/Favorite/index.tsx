import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useSnackbar } from 'notistack';
import { useDispatch, useSelector } from 'react-redux';

import { ChevronRight, Delete, Refresh } from '@mui/icons-material';

import Card from '../../../components/Card';
import { DataTable } from '../../../components/CustomDataGrid';
import { RoundButton } from '../../../components/RoundButton';
import { ToolButton, ToolsPanel } from '../../../components/ToolsPanel';
import { documentsFavoriteCols } from '../../../constants/documentsFavorite';
import { Routes } from '../../../constants/routes';
import {
  useDeleteFavoriteDocumentMutation,
  useGetFavoriteDocumentsQuery,
} from '../../../services/api/document';
import { RootState } from '../../../store';
import { setLocationData } from '../../../store/utils';

import styles from './styles.module.scss';

export const DocumentFavoritePage: React.FC = () => {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [selected, setSelected] = useState<Record<number, boolean>>({});
  const [cols, setCols] = useState(documentsFavoriteCols);
  const [deleteDocument] = useDeleteFavoriteDocumentMutation();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const dispatch = useDispatch();

  const { data, isLoading, refetch } = useGetFavoriteDocumentsQuery(
    {
      page,
      size: perPage,
      useCount: true,
    },
    { refetchOnMountOrArgChange: true }
  );

  const visibleColumns = useMemo(
    () => cols.filter((col) => !col.hidden),
    [cols]
  );

  const rows = useMemo(() => {
    return data?.data || [];
  }, [data]);

  const selectedDocuments = useMemo(() => {
    return Object.keys(selected)
      .map((el) => Number(el))
      .filter((el) => selected[el]);
  }, [selected]);

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const handleDetails = useCallback(() => {
    window
      .open(
        Routes.DOCUMENTS.DOCUMENT.replace(':id', selectedDocuments[0]),
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  }, [selectedDocuments, openFormInNewTab]);

  const handleDeleteDocuments = useCallback(async () => {
    const response = await deleteDocument({ ids: selectedDocuments });
    refetch();
    if ('data' in response) {
      const key = enqueueSnackbar(
        'Выбранные документы исключены из избранного',
        {
          autoHideDuration: 10000,
          onClick: () => closeSnackbar(key),
          variant: 'success',
          anchorOrigin: { horizontal: 'right', vertical: 'top' },
        }
      );
    }
  }, [deleteDocument, selectedDocuments, refetch]);

  useEffect(() => {
    dispatch(setLocationData({ menuName: ['Документы', 'Избранные'] }));
  }, []);

  const rowDoubleHandler = useCallback(
    (e: any) => {
      if (e.id) {
        window
          .open(
            Routes.DOCUMENTS.DOCUMENT.replace(':id', e.id),
            openFormInNewTab ? '_blank' : '_self'
          )
          ?.focus();
      }
    },
    [openFormInNewTab]
  );

  return (
    <>
      <h1 className={styles.title}>Избранные</h1>
      <Card className={styles.card}>
        <DataTable
          formKey={'documents_favorite'}
          cols={cols}
          rows={rows || []}
          onRowSelect={setSelected}
          onRowDoubleClick={rowDoubleHandler}
          loading={isLoading}
          height="fullHeight"
          className={styles.table}
          pagerProps={{
            page: page,
            setPage: setPage,
            perPage: perPage,
            setPerPage: setPerPage,
            total: data?.totalCount || 0,
          }}
        >
          <ToolsPanel
            leftActions={
              <>
                <RoundButton icon={<Refresh />} onClick={refetch} />
              </>
            }
            className={styles.panel}
          >
            <ToolButton
              label="Подробно"
              startIcon={<ChevronRight />}
              disabled={selectedDocuments.length !== 1}
              onClick={() => handleDetails()}
              fast
            />
            <ToolButton
              label={'Удалить'}
              disabled={!selectedDocuments.length}
              startIcon={<Delete />}
              fast
              onClick={handleDeleteDocuments}
            />
          </ToolsPanel>
        </DataTable>
      </Card>
    </>
  );
};

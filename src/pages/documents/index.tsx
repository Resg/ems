import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import {
  matchPath,
  useLocation,
  useNavigate,
  useParams,
  useSearchParams,
} from 'react-router-dom';

import { Grid, Tab, Tabs } from '@mui/material';

import Card from '../../components/Card';
import { TabPanel } from '../../components/TabPanel';
import {
  DocumentTabs,
  DocumentTabsLabels,
  DocumentTypes,
} from '../../constants/documents';
import { Routes } from '../../constants/routes';
import {
  Bonds,
  Common,
  Followed,
  History,
  Protocol,
  Requests,
  Resolutions,
  Tasks,
} from '../../containers/documents/bookmarks';
import { Delivery } from '../../containers/documents/bookmarks/Delivery';
import { FilesTable } from '../../containers/documents/FilesTable';
import { OutcomingDocumentForm } from '../../containers/documents/outcoming';
import { OutcomingDRKK } from '../../containers/documents/outcoming_DRKK';
import PodrDocumentForm from '../../containers/documents/podr';
import PredprDocumentForm from '../../containers/documents/predpr';
import { useGetDocumentByIdQuery } from '../../services/api/document';
import { setLocationData } from '../../store/utils';
import { CommonDocumentType } from '../../types/documents';

import styles from './styles.module.scss';

export const DocumentPage = () => {
  const dispatch = useDispatch();
  const [searchParams] = useSearchParams();
  const location = useLocation();
  const navigate = useNavigate();
  const { tab } = useParams();
  const idForRedirect = Number(searchParams.get('id'));
  const docType = String(searchParams.get('documentType'));
  const id =
    Number(
      matchPath(Routes.DOCUMENTS.DOCUMENT_TABS, location.pathname)?.params?.id
    ) ||
    Number(
      matchPath(Routes.DOCUMENTS.DOCUMENT, location.pathname)?.params?.id
    ) ||
    0;
  const { data, isLoading } = useGetDocumentByIdQuery(
    {
      id: id || '',
    },
    { skip: !id }
  );

  const [isDocumentCreation, setDocumentCreation] = useState(false);

  let tabs = [
    'Общие',
    'Сопровождаемые документы',
    'Резолюции',
    'Связки',
    'Заявки',
    'История',
    'Протокол',
    'Задачи',
  ];

  const documentTypes: Record<string, string> = {
    DOCS_PODR: 'Внутренний подразделения',
    DOCS_PREDPR: 'Внутренний предприятия',
    DOCS_OUTCOMING: 'Исходящий',
    DOCS_OUTCOMING_DRKK: 'ДЭРК',
  };

  const createBookmarks = (data?: CommonDocumentType) => {
    if (!data) {
      return {};
    }

    return {
      Общие: <Common docData={data} />,
      'Сопровождаемые документы': <Followed />,
      Резолюции: <Resolutions />,
      Связки: <Bonds docId={data.id} />,
      Заявки: <Requests />,
      История: <History />,
      Протокол: <Protocol docId={data.id} />,
      Доставка: <Delivery docId={data.id} />,
      Задачи: <Tasks docData={data} />,
    };
  };

  const bookmarks = createBookmarks(data);

  if (data?.objectName === 'DOCS_OUTCOMING_DRKK') {
    tabs = tabs.filter(
      (tab) => !['Сопровождаемые документы', 'Резолюции'].includes(tab)
    );
  }

  const [tabValue, setTabValue] = useState(tab || DocumentTabs.COMMON);

  const handleTabChange = (e: React.SyntheticEvent, newValue: DocumentTabs) => {
    setTabValue(newValue);
  };

  useEffect(() => {
    if (id) {
      navigate(
        Routes.DOCUMENTS.DOCUMENT_TABS.replace(':id', id).replace(
          ':tab',
          tabValue
        )
      );
    }
  }, [tabValue]);

  useEffect(() => {
    if (idForRedirect) {
      navigate(
        Routes.DOCUMENTS.DOCUMENT_TABS.replace(':id', idForRedirect).replace(
          ':tab',
          tabValue
        )
      );
    }
  }, [idForRedirect]);

  useEffect(() => {
    if (searchParams.get('documentType')) {
      setDocumentCreation(true);
    }
  }, [dispatch, searchParams]);

  useEffect(() => {
    dispatch(
      setLocationData({
        name: data
          ? `Документ ШК: ${data.id}`
          : `Новый документ: ${documentTypes[docType]}`,
        data: data,
      })
    );
  }, [data]);

  if (isDocumentCreation) {
    return (
      <Card className={styles.card}>
        <Tabs
          value={tabValue}
          className={styles.tabBar}
          onChange={handleTabChange}
        >
          <Tab
            key={DocumentTabs.COMMON}
            label={DocumentTabsLabels[DocumentTabs.COMMON]}
            value={DocumentTabs.COMMON}
          />
        </Tabs>
        <TabPanel
          key={tabs[0]}
          value={tabValue}
          index={0}
          className={styles.CreationTab}
        >
          {
            bookmarks[
              DocumentTabsLabels[DocumentTabs.COMMON] as keyof typeof bookmarks
            ]
          }
        </TabPanel>
        <Grid container spacing={2}>
          <Grid item sx={{ width: '100%' }}>
            {
              docTypes[
                searchParams.get('documentType')! as keyof typeof docTypes
              ]
            }
          </Grid>
        </Grid>
        <FilesTable
          label="Файлы"
          columns={[]}
          rows={[]}
          refetchTable={() => {
            return;
          }}
        />
      </Card>
    );
  }

  return isLoading || !data ? (
    <div />
  ) : (
    <>
      <div className={styles.title}>{`${data.title}`}</div>
      <Card className={styles.card}>
        <Tabs
          value={tabValue}
          className={styles.tabBar}
          onChange={handleTabChange}
        >
          {Object.values(DocumentTabs).map((tab) => {
            if (data?.objectName === 'DOCS_OUTCOMING_DRKK') {
              if (
                ['Сопровождаемые документы', 'Резолюции'].includes(
                  DocumentTabsLabels[tab]
                )
              ) {
                return null;
              }
            }
            if (data?.objectName !== 'DOCS_OUTCOMING' && tab === 'delivery') {
              return null;
            }
            return (
              <Tab key={tab} label={DocumentTabsLabels[tab]} value={tab} />
            );
          })}
        </Tabs>
        {Object.values(DocumentTabs).map((tab) => (
          <TabPanel
            key={tab}
            value={tabValue}
            index={tab}
            className={styles.tab}
          >
            <div className={styles.tabContent}>
              {bookmarks[DocumentTabsLabels[tab] as keyof typeof bookmarks]}
            </div>
          </TabPanel>
        ))}
      </Card>
    </>
  );
};

const docTypes = {
  [DocumentTypes.DOCS_PREDPR]: <PredprDocumentForm editMode />,
  [DocumentTypes.DOCS_OUTCOMING_DRKK]: <OutcomingDRKK />,
  [DocumentTypes.DOCS_OUTCOMING]: <OutcomingDocumentForm />,
  DOCS_PODR: <PodrDocumentForm editMode />,
};

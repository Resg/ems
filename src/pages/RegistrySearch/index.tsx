import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { FormikProps } from 'formik';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';

import {
  Archive,
  Cancel,
  Check,
  ChevronRight,
  ClearAll,
  DeleteOutline,
  DeleteSweep,
  ExpandMore,
  OpenInNew,
  Print,
  Redo,
  Refresh,
  Replay,
  Search,
  Send,
} from '@mui/icons-material';
import { Button, Tab, Tabs } from '@mui/material';

import Card from '../../components/Card';
import { DataTable } from '../../components/CustomDataGrid';
import { FilterBar, FormValues } from '../../components/FilterBarNew';
import { MenuButtonItem } from '../../components/MenuButton/MenuButtonItem';
import { RoundButton } from '../../components/RoundButton';
import { TabPanel } from '../../components/TabPanel';
import { ToolButton, ToolsPanel } from '../../components/ToolsPanel';
import { MenuToolButton } from '../../components/ToolsPanel/MenuToolButton';
import { RegistrySearchCols } from '../../constants/registry';
import {
  RegistryClearMap,
  RegistrySearchLabel,
  RegistrySearchTabs,
} from '../../constants/registrySearch';
import { Document } from '../../containers/RegistrySearch/RegistrySearchTabs/Document';
import { Numbers } from '../../containers/RegistrySearch/RegistrySearchTabs/Numbers';
import { OutgoingPacket } from '../../containers/RegistrySearch/RegistrySearchTabs/OutgoingPacket';
import { Registry } from '../../containers/RegistrySearch/RegistrySearchTabs/Registry';
import { useGetInternalRegistryStatesQuery } from '../../services/api/dictionaries';
import {
  useArchiveRegistriesMutation,
  useCloseRegistriesMutation,
  useDeleteRegistryMutation,
  useGetAcceptedRegistryMutation,
  useGetRegistriesQuery,
  useOpenRegistriesMutation,
  useReportRegistriesMutation,
  useReturnRegistryMutation,
  useSentAgainRegistriesMutation,
  useSentRegistriesMutation,
} from '../../services/api/registries';
import { RootState, useAppDispatch } from '../../store';
import { setLocationData } from '../../store/utils';
import { registriesSearchToRowAdapter } from '../../utils/registriesSearch';
import { prepareData } from '../../utils/registrySearch';

import styles from './styles.module.scss';

type RegistrySearchPageProps = {
  registryType: string;
};

interface ParametersType {
  viewCode: string;
  isDocumentRegistry?: boolean;
  isOutgoingPacketRegistry?: boolean;
  stateCode: string[];
}

interface RegistryParametersType {
  ['Params']: ParametersType;
  ['Buttons']: string[];
  ['Tabs']: string[];
  ['Title']: string;
}

interface RegistryType {
  [key: string]: RegistryParametersType;
}

export const RegistrySearchPage: React.FC<RegistrySearchPageProps> = ({
  registryType,
}) => {
  const filters = useSelector(
    (state: RootState) =>
      state.filters.data[`registries-search-${registryType}`] || {}
  );

  const dispatch = useAppDispatch();
  const location = useLocation();

  const [cols, setCols] = useState(RegistrySearchCols);
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [tab, setTab] = useState(RegistrySearchTabs.REGISTRY);

  const handleChange = useCallback(
    (_event: React.SyntheticEvent, value: RegistrySearchTabs) => {
      setTab(value);
    },
    []
  );
  const { data: states = [] } = useGetInternalRegistryStatesQuery({});

  const Labels: Record<string, string> = {
    registries: 'Реестры',
    incoming: 'Входящие',
    outgoing: 'Исходящие',
    archive: 'Архив',
    documents: 'Документы',
    outgoingpacket: 'Исходящие пакеты',
    expected: 'Ожидаемые',
    accepted: 'Принятые',
    returned: 'Возвращенные',
    opened: 'Открытые',
    closed: 'Закрытые',
    sent: 'Отправленные',
  };

  const registryTypes: RegistryType = {
    REGISTRIES_INCOMING_DOCUMENTS_EXPECTED: {
      Params: {
        viewCode: 'INCOMING',
        isDocumentRegistry: true,
        stateCode: ['S'],
      },
      Buttons: ['Принять', 'Вернуть', 'Печать'],
      Tabs: ['REGISTRY', 'DOCUMENT', 'REGISTRY_NUMBERS_LIST'],
      Title: 'Ожидаемые реестры документов',
    },
    REGISTRIES_INCOMING_DOCUMENTS_ACCEPTED: {
      Params: {
        viewCode: 'INCOMING',
        isDocumentRegistry: true,
        stateCode: ['R'],
      },
      Buttons: ['Печать', 'В архив'],
      Tabs: ['REGISTRY', 'DOCUMENT', 'REGISTRY_NUMBERS_LIST'],
      Title: 'Принятые реестры документов',
    },
    REGISTRIES_INCOMING_DOCUMENTS_RETURNED: {
      Params: {
        viewCode: 'INCOMING',
        isDocumentRegistry: true,
        stateCode: ['V'],
      },
      Buttons: ['Отправить', 'Печать', 'В архив'],
      Tabs: ['REGISTRY', 'DOCUMENT', 'REGISTRY_NUMBERS_LIST'],
      Title: 'Возвращенные реестры документов',
    },
    REGISTRIES_INCOMING_OUTGOINGPACKET_EXPECTED: {
      Params: {
        viewCode: 'INCOMING',
        isOutgoingPacketRegistry: true,
        stateCode: ['S'],
      },
      Buttons: ['Принять', 'Вернуть'],
      Tabs: ['REGISTRY', 'OUTGOING_PACKET', 'REGISTRY_NUMBERS_LIST'],
      Title: 'Ожидаемые реестры исходящих пакетов',
    },
    REGISTRIES_INCOMING_OUTGOINGPACKET_ACCEPTED: {
      Params: {
        viewCode: 'INCOMING',
        isOutgoingPacketRegistry: true,
        stateCode: ['R'],
      },
      Buttons: ['Печать', 'В архив'],
      Tabs: ['REGISTRY', 'OUTGOING_PACKET', 'REGISTRY_NUMBERS_LIST'],
      Title: 'Принятые реестры исходящих пакетов',
    },
    REGISTRIES_INCOMING_OUTGOINGPACKET_RETURNED: {
      Params: {
        viewCode: 'INCOMING',
        isOutgoingPacketRegistry: true,
        stateCode: ['V'],
      },
      Buttons: ['Отправить', 'Печать', 'В архив'],
      Tabs: ['REGISTRY', 'OUTGOING_PACKET', 'REGISTRY_NUMBERS_LIST'],
      Title: 'Возвращенные реестры исходящих пакетов',
    },
    REGISTRIES_OUTGOING_DOCUMENTS_OPENED: {
      Params: {
        viewCode: 'OUTGOING',
        isDocumentRegistry: true,
        stateCode: ['N'],
      },
      Buttons: ['Удалить', 'Закрыть', 'Печать'],
      Tabs: ['REGISTRY', 'DOCUMENT', 'REGISTRY_NUMBERS_LIST'],
      Title: 'Открытые реестры документов',
    },
    REGISTRIES_OUTGOING_DOCUMENTS_CLOSED: {
      Params: {
        viewCode: 'OUTGOING',
        isDocumentRegistry: true,
        stateCode: ['C'],
      },
      Buttons: ['Открыть', 'Отправить', 'Печать'],
      Tabs: ['REGISTRY', 'DOCUMENT', 'REGISTRY_NUMBERS_LIST'],
      Title: 'Закрытые реестры документов',
    },
    REGISTRIES_OUTGOING_DOCUMENTS_SENT: {
      Params: {
        viewCode: 'OUTGOING',
        isDocumentRegistry: true,
        stateCode: ['S', 'R'],
      },
      Buttons: ['Повторная отправка', 'Печать', 'В архив'],
      Tabs: ['REGISTRY', 'DOCUMENT', 'REGISTRY_NUMBERS_LIST'],
      Title: 'Отправленные реестры документов',
    },
    REGISTRIES_OUTGOING_OUTGOINGPACKET_OPENED: {
      Params: {
        viewCode: 'OUTGOING',
        isOutgoingPacketRegistry: true,
        stateCode: ['N'],
      },
      Buttons: ['Удалить', 'Закрыть', 'Печать'],
      Tabs: ['REGISTRY', 'OUTGOING_PACKET', 'REGISTRY_NUMBERS_LIST'],
      Title: 'Открытые реестры исходящих пакетов',
    },
    REGISTRIES_OUTGOING_OUTGOINGPACKET_CLOSED: {
      Params: {
        viewCode: 'OUTGOING',
        isOutgoingPacketRegistry: true,
        stateCode: ['C'],
      },
      Buttons: ['Открыть', 'Отправить', 'Печать'],
      Tabs: ['REGISTRY', 'OUTGOING_PACKET', 'REGISTRY_NUMBERS_LIST'],
      Title: 'Закрытые реестры исходящих пакетов',
    },
    REGISTRIES_OUTGOING_OUTGOINGPACKET_SENT: {
      Params: {
        viewCode: 'OUTGOING',
        isOutgoingPacketRegistry: true,
        stateCode: ['S', 'R'],
      },
      Buttons: ['Повторная отправка', 'Печать', 'В архив'],
      Tabs: ['REGISTRY', 'OUTGOING_PACKET', 'REGISTRY_NUMBERS_LIST'],
      Title: 'Отправленные реестры исходящих пакетов',
    },
    REGISTRIES_ARCHIVE: {
      Params: {
        viewCode: 'ARCHIVE',
        isDocumentRegistry: true,
        isOutgoingPacketRegistry: true,
        stateCode: ['R', 'V', 'S'],
      },
      Buttons: ['Печать'],
      Tabs: [
        'REGISTRY',
        'DOCUMENT',
        'OUTGOING_PACKET',
        'REGISTRY_NUMBERS_LIST',
      ],
      Title: 'Архив реестров',
    },
  };

  useEffect(() => {
    dispatch(
      setLocationData({
        menuName: location.pathname
          .split('/')
          .filter((el: string) => el)
          .map((el: string) => Labels[el]),
      })
    );
  }, [location.pathname]);

  const statesCodes = useMemo(
    () =>
      states
        .filter((state) =>
          registryTypes[registryType]['Params'].stateCode.includes(state.value)
        )
        .map((value) => value.value),
    [states, registryType]
  );

  const searchParameters = useMemo(() => {
    return {
      ...registryTypes[registryType]['Params'],
      ...filters,
      documentTypeIds: filters.documentTypeIds
        ? [filters.documentTypeIds]
        : undefined,
    };
  }, [filters, registryType]);

  const filteredSearchParameters = useMemo(() => {
    if (
      registryTypes[registryType]['Params']['viewCode'] !== 'ARCHIVE' ||
      Object.keys(filters).length === 0
    )
      return searchParameters;
    const filteredArr = Object.entries(searchParameters);
    const filtered = filteredArr.filter(
      ([key, value]) =>
        !(key === 'isDocumentRegistry' && !filters.isDocumentRegistry) &&
        !(
          key === 'isOutgoingPacketRegistry' &&
          !filters.isOutgoingPacketRegistry
        )
    );
    return Object.fromEntries(filtered) || {};
  }, [searchParameters, filters]);

  const { data, refetch, isLoading } = useGetRegistriesQuery(
    {
      searchParameters: filteredSearchParameters,
      page: page,
      size: perPage,
      useCount: true,
    },
    {
      skip: !Object.entries(filters).length,
    }
  );

  const [deleteRegistry] = useDeleteRegistryMutation();
  const [getAcceptedRegistry] = useGetAcceptedRegistryMutation();
  const [closeRegistry] = useCloseRegistriesMutation();
  const [openRegistry] = useOpenRegistriesMutation();
  const [archiveRegistry] = useArchiveRegistriesMutation();
  const [reportRegistry] = useReportRegistriesMutation();
  const [returnRegistry] = useReturnRegistryMutation();
  const [sentRegistry] = useSentRegistriesMutation();
  const [sentAgainRegistry] = useSentAgainRegistriesMutation();

  const rows = useMemo(() => {
    if (Object.entries(filters).length) {
      return registriesSearchToRowAdapter(data?.data);
    }
    return [];
  }, [filters, data?.data]);

  const [selectedRows, setSelectedRows] = useState<Record<number, boolean>>({});

  const selectedRegistries = useMemo(() => {
    return Object.keys(selectedRows)
      .map((el) => Number(el))
      .filter((el) => selectedRows[el]);
  }, [selectedRows]);

  const visibleColumns = useMemo(() => {
    if (registryTypes[registryType]['Params'].viewCode !== 'ARCHIVE')
      return cols.filter((col) => !col.archive);
    return cols;
  }, [cols, registryType]);

  const handleDelete = useCallback(async () => {
    await deleteRegistry({ id: selectedRegistries[0] });
    setSelectedRows({});
    refetch();
  }, [selectedRegistries]);

  const handleGetAccepted = useCallback(async () => {
    await getAcceptedRegistry({ ids: selectedRegistries });
    setSelectedRows({});
    refetch();
  }, [selectedRegistries]);

  const handleOpen = useCallback(async () => {
    await openRegistry({ ids: selectedRegistries });
    setSelectedRows({});
    refetch();
  }, [selectedRegistries]);

  const handleClose = useCallback(async () => {
    await closeRegistry({ ids: selectedRegistries });
    setSelectedRows({});
    refetch();
  }, [selectedRegistries]);

  const handleArchive = useCallback(async () => {
    await archiveRegistry({ ids: selectedRegistries });
    setSelectedRows({});
    refetch();
  }, [selectedRegistries]);

  const handleReport = useCallback(async () => {
    await reportRegistry({ ids: selectedRegistries });
    setSelectedRows({});
    refetch();
  }, [selectedRegistries]);

  const handleReturn = useCallback(async () => {
    await returnRegistry({ id: selectedRegistries[0] });
    setSelectedRows({});
    refetch();
  }, [selectedRegistries]);

  const handleSent = useCallback(async () => {
    await sentRegistry({ ids: selectedRegistries });
    setSelectedRows({});
    refetch();
  }, [selectedRegistries]);

  const handleSentAgain = useCallback(async () => {
    await sentAgainRegistry({ ids: selectedRegistries });
    setSelectedRows({});
    refetch();
  }, [selectedRegistries]);

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const handleDetails = useCallback(() => {
    window
      .open(
        `/registries/${selectedRegistries[0]}`,
        openFormInNewTab ? '_blank' : '_self'
      )
      ?.focus();
  }, [selectedRegistries, openFormInNewTab]);

  const clearTab = useCallback(
    (
      setFieldValue: (
        field: string,
        value: any,
        shouldValidate?: boolean
      ) => void
    ) => {
      const fields: { [key: string]: any } = RegistryClearMap[tab];

      for (const key in fields) {
        setFieldValue(key, fields[key]);
      }
    },
    [tab]
  );

  const initialValues = useMemo(() => {
    return {
      isArchive:
        registryTypes[registryType]['Params']['viewCode'] === 'ARCHIVE',
      isDocumentRegistry:
        registryTypes[registryType]['Params']['isDocumentRegistry'],
      isOutgoingPacketRegistry:
        registryTypes[registryType]['Params']['isOutgoingPacketRegistry'],
      states: statesCodes,
    };
  }, [registryType, statesCodes]);

  const createFilterButtons = useCallback(
    (
      onSearch: ({ values }: { values: FormValues }) => () => Promise<void>,
      props: FormikProps<FormValues>
    ) => {
      const { resetForm, values, setFieldValue } = props;

      return (
        <>
          <Button
            color="primary"
            variant="contained"
            startIcon={<Search />}
            onClick={onSearch({ values: prepareData(values as FormValues) })}
          >
            Поиск
          </Button>
          <Button
            variant="outlined"
            startIcon={<ClearAll />}
            onClick={() => clearTab(setFieldValue)}
          >
            Очистить вкладку
          </Button>
          <Button
            color="primary"
            variant="outlined"
            startIcon={<DeleteSweep />}
            onClick={() => resetForm()}
          >
            Очистить всё
          </Button>
        </>
      );
    },
    [clearTab]
  );

  return (
    <>
      <h1 className={styles.title}>{registryTypes[registryType]['Title']}</h1>
      <FilterBar
        formKey={`registries-search-${registryType}`}
        key={registryType}
        createFilterButtons={createFilterButtons}
        refetchSearch={() => refetch()}
        initValues={initialValues}
      >
        <Tabs value={tab} onChange={handleChange}>
          <Tab
            label={RegistrySearchLabel[RegistrySearchTabs.REGISTRY]}
            value={RegistrySearchTabs.REGISTRY}
          />
          {registryTypes[registryType]['Tabs'].includes('DOCUMENT') && (
            <Tab
              label={RegistrySearchLabel[RegistrySearchTabs.DOCUMENT]}
              value={RegistrySearchTabs.DOCUMENT}
            />
          )}
          {registryTypes[registryType]['Tabs'].includes('OUTGOING_PACKET') && (
            <Tab
              label={RegistrySearchLabel[RegistrySearchTabs.OUTGOING_PACKET]}
              value={RegistrySearchTabs.OUTGOING_PACKET}
            />
          )}
          <Tab
            label={
              RegistrySearchLabel[RegistrySearchTabs.REGISTRY_NUMBERS_LIST]
            }
            value={RegistrySearchTabs.REGISTRY_NUMBERS_LIST}
          />
        </Tabs>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={RegistrySearchTabs.REGISTRY}
        >
          <Registry params={registryTypes[registryType]['Params']} />
        </TabPanel>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={RegistrySearchTabs.OUTGOING_PACKET}
        >
          <OutgoingPacket />
        </TabPanel>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={RegistrySearchTabs.REGISTRY_NUMBERS_LIST}
        >
          <Numbers />
        </TabPanel>
        <TabPanel
          className={styles.tabPanel}
          value={tab}
          index={RegistrySearchTabs.DOCUMENT}
        >
          <Document />
        </TabPanel>
      </FilterBar>
      <Card className={styles.card}>
        <DataTable
          formKey={'pages_registry_search'}
          cols={cols}
          rows={rows}
          onRowSelect={setSelectedRows}
          loading={isLoading}
          height="fullHeight"
          pagerProps={{
            page,
            setPage,
            perPage,
            setPerPage,
            total: data?.totalCount || 0,
          }}
        >
          <ToolsPanel
            className={styles.panel}
            leftActions={<RoundButton icon={<Refresh />} onClick={refetch} />}
          >
            <ToolButton
              label={'подробно'}
              startIcon={<ChevronRight />}
              fast
              onClick={handleDetails}
              disabled={!(selectedRegistries.length === 1)}
            />
            <MenuToolButton label={'действия'} endIcon={<ExpandMore />}>
              {registryTypes[registryType]['Buttons'].includes('Удалить') && (
                <MenuButtonItem
                  icon={<DeleteOutline />}
                  disabled={!(selectedRegistries.length === 1)}
                  onClick={handleDelete}
                >
                  Удалить
                </MenuButtonItem>
              )}
              {registryTypes[registryType]['Buttons'].includes('Принять') && (
                <MenuButtonItem
                  icon={<Check />}
                  onClick={handleGetAccepted}
                  disabled={!(selectedRegistries.length >= 1)}
                >
                  Принять
                </MenuButtonItem>
              )}
              {registryTypes[registryType]['Buttons'].includes('Вернуть') && (
                <MenuButtonItem
                  icon={<Replay />}
                  disabled={!(selectedRegistries.length === 1)}
                  onClick={handleReturn}
                >
                  Вернуть
                </MenuButtonItem>
              )}
              {registryTypes[registryType]['Buttons'].includes('Закрыть') && (
                <MenuButtonItem
                  icon={<Cancel />}
                  disabled={!(selectedRegistries.length >= 1)}
                  onClick={handleClose}
                >
                  Закрыть
                </MenuButtonItem>
              )}
              {registryTypes[registryType]['Buttons'].includes('Открыть') && (
                <MenuButtonItem
                  icon={<OpenInNew />}
                  disabled={!(selectedRegistries.length >= 1)}
                  onClick={handleOpen}
                >
                  Открыть
                </MenuButtonItem>
              )}
              {registryTypes[registryType]['Buttons'].includes('Отправить') && (
                <MenuButtonItem
                  icon={<Redo />}
                  disabled={!(selectedRegistries.length >= 1)}
                  onClick={handleSent}
                >
                  Отправить
                </MenuButtonItem>
              )}
              {registryTypes[registryType]['Buttons'].includes(
                'Повторная отправка'
              ) && (
                <MenuButtonItem
                  icon={<Send />}
                  disabled={!(selectedRegistries.length >= 1)}
                  onClick={handleSentAgain}
                >
                  Повторная отправка
                </MenuButtonItem>
              )}
              {registryTypes[registryType]['Buttons'].includes('Печать') && (
                <MenuButtonItem
                  icon={<Print />}
                  disabled={!(selectedRegistries.length >= 1)}
                  onClick={handleReport}
                >
                  Печать
                </MenuButtonItem>
              )}
              {registryTypes[registryType]['Buttons'].includes('В архив') && (
                <MenuButtonItem
                  icon={<Archive />}
                  disabled={!(selectedRegistries.length >= 1)}
                  onClick={handleArchive}
                >
                  В архив
                </MenuButtonItem>
              )}
            </MenuToolButton>
          </ToolsPanel>
        </DataTable>
      </Card>
    </>
  );
};

import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';

import {
  Assignment,
  Cancel,
  DeleteOutline,
  Done,
  Refresh,
  Send,
  SummarizeRounded,
  TaskAlt,
} from '@mui/icons-material';

import Card from '../../components/Card';
import { DataTable } from '../../components/CustomDataGrid';
import { ExecuteButton } from '../../components/Execution';
import {
  ExecutionButtonActions,
  ExecutionButtonIcons,
  ExecutionFastButtons,
  ExecutionStatusIds,
} from '../../components/Execution/ExecutionButton/constants';
import { ExcelIcon } from '../../components/Icons';
import { ResolutionToolButton } from '../../components/Resolution';
import { RoundButton } from '../../components/RoundButton';
import {
  DeleteToolButton,
  ToolButton,
  ToolsPanel,
} from '../../components/ToolsPanel';
import { ModalToolButton } from '../../components/ToolsPanel/ModalToolButton';
import {
  DraftsCols,
  IncomingDraftsExecutionButtons,
} from '../../constants/drafts';
import { useGetListDraftTasksQuery } from '../../services/api/drafts';
import {
  useDeleteTaskMutation,
  useGetTaskInWorkMutation,
  useSendToAcquaintedMutation,
  useSendToContinueMutation,
  useSendToExecutionMutation,
} from '../../services/api/incomingDocumentTasks';
import { RootState, useAppDispatch } from '../../store';
import { setLocationData } from '../../store/utils';
import { IncomingDraftsData } from '../../types/drafts';
import { DraftsToRowAdapter } from '../../utils/drafts';
import { massServerQuery } from '../../utils/incomingDocumentTasks';

import styles from './styles.module.scss';

interface idsObjectType {
  ids: number[];
  documentIds: number[];
  resolutionId: (number | null)[];
}

export const DraftsPage: React.FC = () => {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [selectedRows, setSelectedRows] = useState<Record<string, boolean>>({});

  const [deleteTask] = useDeleteTaskMutation();
  const [sendToExecution] = useSendToExecutionMutation();
  const [getTaskInWork] = useGetTaskInWorkMutation();
  const [sendToContinue] = useSendToContinueMutation();
  const [sendToAcquainted] = useSendToAcquaintedMutation();

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const { data, refetch, isLoading } = useGetListDraftTasksQuery({
    page,
    size: perPage,
  });

  const drafts = useMemo(() => {
    return data?.data;
  }, [data]);

  const rowsMap = useMemo(
    () =>
      (data?.data || []).reduce((acc, row) => {
        acc[row.id] = row;
        return acc;
      }, {} as Record<number, IncomingDraftsData>),
    [data?.data]
  );

  const idsObject = useMemo(() => {
    const result: idsObjectType = {
      ids: [],
      documentIds: [],
      resolutionId: [],
    };

    Object.keys(selectedRows).forEach((key) => {
      const numberKey = Number(key);

      if (selectedRows[key] && !isNaN(numberKey) && rowsMap[numberKey]) {
        result.ids.push(rowsMap[numberKey].id);
        result.documentIds.push(rowsMap[numberKey].documentId);
        result.resolutionId.push(rowsMap[numberKey].resolutionId);
      }
    });

    return result;
  }, [rowsMap, selectedRows]);

  const rows = useMemo(() => {
    return DraftsToRowAdapter(drafts);
  }, [drafts]);

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(
      setLocationData({ menuName: ['Задачи по документам', 'Черновики'] })
    );
  }, []);

  const openPage = useCallback(
    (path: string = '') => {
      window.open(`${path}`, openFormInNewTab ? '_blank' : '_self')?.focus();
    },
    [selectedRows, openFormInNewTab]
  );

  const handleWorkRequest = useCallback(async () => {
    massServerQuery(idsObject.ids, getTaskInWork).then(() => refetch());
  }, [selectedRows, idsObject]);

  const handleDelete = useCallback(async () => {
    await deleteTask(idsObject.ids[0]);
    setSelectedRows({});
    refetch();
  }, [selectedRows, deleteTask, idsObject]);

  const handleExecute = useCallback(async () => {
    await sendToExecution(idsObject.ids[0]);
    setSelectedRows({});
    refetch();
  }, [selectedRows, sendToExecution, idsObject]);

  const handleClose = useCallback(async () => {
    await sendToContinue({ id: idsObject.ids[0] });
    setSelectedRows({});
    refetch();
  }, [selectedRows, sendToContinue, idsObject]);

  const closeDisabled = useMemo(() => {
    return !(
      idsObject.ids.length === 1 && rowsMap[idsObject.ids[0]]['canClose']
    );
  }, [idsObject, rowsMap]);

  const handleAcquainted = useCallback(async () => {
    massServerQuery(idsObject.ids, sendToAcquainted).then(() => refetch());
  }, [selectedRows, sendToContinue, idsObject]);

  const acquaintedDisabled = useMemo(() => {
    return !(
      idsObject.ids.length !== 0 && rowsMap[idsObject.ids[0]]['canMarkAsRead']
    );
  }, [idsObject, rowsMap]);

  return (
    <>
      <h1 className={styles.title}>Черновики</h1>
      <Card className={styles.panel}>
        <DataTable
          formKey={'pages_drafts'}
          cols={DraftsCols}
          rows={rows}
          onRowSelect={setSelectedRows}
          loading={isLoading}
          height="fullHeight"
          pagerProps={{
            page: page,
            setPage: setPage,
            perPage: perPage,
            setPerPage: setPerPage,
            total: data?.totalCount || 0,
          }}
        >
          <ToolsPanel
            className={styles.panel}
            leftActions={
              <>
                <RoundButton icon={<Refresh />} onClick={refetch} />
                <RoundButton icon={<ExcelIcon />} />
              </>
            }
          >
            {[
              ...IncomingDraftsExecutionButtons.map((action) => {
                const ButtonIcon = ExecutionButtonIcons[action];
                return action === ExecutionButtonActions.INWORK ? (
                  <ToolButton
                    label={ExecutionButtonActions.INWORK}
                    onClick={handleWorkRequest}
                    disabled={!idsObject.ids.length}
                    startIcon={<ButtonIcon />}
                    key={ExecutionButtonActions.INWORK}
                  />
                ) : (
                  <ExecuteButton
                    startIcon={<ButtonIcon />}
                    statusId={ExecutionStatusIds[action]}
                    rows={idsObject.ids.map(
                      (id) => rowsMap[id] as IncomingDraftsData
                    )}
                    ids={idsObject.ids}
                    label={action}
                    key={action}
                    fast={ExecutionFastButtons.includes(action)}
                  />
                );
              }),
              <ToolButton
                label={ExecutionButtonActions.CLOSE}
                onClick={handleClose}
                disabled={closeDisabled}
                startIcon={<Cancel />}
                key={ExecutionButtonActions.CLOSE}
              />,
              <ToolButton
                label={ExecutionButtonActions.ACQUAINTED}
                onClick={handleAcquainted}
                disabled={acquaintedDisabled}
                startIcon={<TaskAlt />}
                key={ExecutionButtonActions.ACQUAINTED}
              />,
              <ModalToolButton
                label={'Отправить на исполнение'}
                labelButton={'Да'}
                startIcon={<Send />}
                endIcon={<Done />}
                disabled={
                  idsObject.ids.length !== 1 ||
                  !rowsMap[idsObject.ids[0]]['canSendToExecution']
                }
                description={`Отправить выбранную задачу на исполнение?`}
                title={'Подтверждение действия'}
                onConfirm={handleExecute}
              />,
              <DeleteToolButton
                label={'Удалить'}
                startIcon={<DeleteOutline />}
                disabled={
                  idsObject.ids.length !== 1 ||
                  !rowsMap[idsObject.ids[0]]['canDelete']
                }
                description={`Удалить выбранную задачу?`}
                title={'Подтверждение действия'}
                onConfirm={handleDelete}
              />,
              <ToolButton
                label="Открыть документ"
                startIcon={<SummarizeRounded />}
                disabled={idsObject.documentIds.length !== 1}
                onClick={() =>
                  idsObject.documentIds &&
                  openPage(`/documents?id=${idsObject.documentIds[0]}`)
                }
                fast
              />,
              <ToolButton
                label="Открыть задачу"
                startIcon={<Assignment />}
                disabled={idsObject.ids.length !== 1}
                onClick={() =>
                  idsObject.ids &&
                  openPage(`/tasks/details/${idsObject.ids[0]}`)
                }
                fast
              />,
              <ResolutionToolButton
                label={'Открыть резолюцию'}
                documentIds={idsObject.documentIds}
                refetch={refetch}
                disabled={
                  idsObject.documentIds.length !== 1 ||
                  !idsObject.resolutionId[0]
                }
                id={idsObject.resolutionId[0]}
              />,
              <ResolutionToolButton
                label={'Создать резолюцию'}
                documentIds={idsObject.documentIds}
                refetch={refetch}
              />,
            ]}
          </ToolsPanel>
        </DataTable>
      </Card>
    </>
  );
};

import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router';
import { useLocation } from 'react-router-dom';

import {
  AddCircleOutline,
  Approval,
  Close,
  DeleteOutline,
  InfoOutlined,
  MoreHoriz,
  Send,
  SignalCellularAlt,
} from '@mui/icons-material';
import {
  Dialog,
  Divider,
  IconButton,
  Menu,
  Tab,
  Tabs,
  Tooltip,
} from '@mui/material';

import Card from '../../components/Card';
import { ExecuteButton } from '../../components/Execution';
import {
  ExecutionButtonActions,
  ExecutionButtonIcons,
  ExecutionFastButtons,
  ExecutionStatusIds,
} from '../../components/Execution/ExecutionButton/constants';
import { MenuButtonItem } from '../../components/MenuButton/MenuButtonItem';
import OnlyOffice, {
  OnlyOfficeProps,
} from '../../components/OnlyOffice/OnlyOffice';
import { ResolutionForm } from '../../components/Resolution';
import { DeleteToolButton, ToolButton } from '../../components/ToolsPanel';
import { LocalStorageKeys } from '../../constants/localStorage';
import { TasksModal } from '../../containers/documents/bookmarks/Tasks/TasksModal';
import { useGetDocumentsByFileIdQuery } from '../../services/api/document';
import { useGetFileByIdQuery } from '../../services/api/files';
import {
  useDeleteTaskMutation,
  useGetIncomingDocumentTasksQuery,
  useSendToExecutionMutation,
} from '../../services/api/incomingDocumentTasks';
import { RootState, useAppDispatch } from '../../store';
import {
  addBreadCrumb,
  chooseBreadCrumbs,
  setCrumbsFileName,
  setSideBarIsOpen,
} from '../../store/utils';
import { useLocalStorage } from '../../utils/hooks/useLocalStorage';

import styles from './styles.module.scss';

const config: OnlyOfficeProps = {
  scriptUrl:
    'https://office.grfc.ru/ds-vpath/6.3.1-43/web-apps/apps/api/documents/api.js',
  height: '100%',
  onlyOfficeId: 'placeholder',
};

const ExecutionButtons = [
  ExecutionButtonActions.SIGN,
  ExecutionButtonActions.ENDORSE,
  ExecutionButtonActions.EXECUTE,
  ExecutionButtonActions.ACQUAINTED,
  ExecutionButtonActions.REASSIGN,
];

export const FileEditor = () => {
  const { id } = useParams();
  const [value, setValue] = useLocalStorage(LocalStorageKeys.BREAD_CRUMBS);

  const { data: document } = useGetDocumentsByFileIdQuery(
    { fileId: id || '' },
    { skip: !id }
  );

  const documentId = Number(document?.data?.[0]?.id);

  const { data: tasks, refetch } = useGetIncomingDocumentTasksQuery(
    {
      searchParameters: {
        documentIds: [documentId],
      },
    },
    { skip: !documentId }
  );

  const { data } = useGetFileByIdQuery({ id: Number(id) }, { skip: !id });
  const [deleteDocument] = useDeleteTaskMutation();
  const [sendToExecution] = useSendToExecutionMutation();

  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(setSideBarIsOpen(false));
  }, [dispatch]);

  const ids = tasks?.data.map((task) => task.documentId);
  const [resolutionOpen, setResolutionOpen] = useState(false);
  const [stageOpen, setStageOpen] = useState(false);
  const [anchorEl, setAnchorEl] = useState<Element | null>(null);
  const [tasksAnchorEl, setTasksAnchorEl] = useState<
    Record<string, Element | null>
  >({});

  useEffect(() => {
    if (data && value.length === 1) {
      dispatch(setCrumbsFileName(data.config.document.title));
    }
  }, [data, value]);

  const handleExpand = (e: React.MouseEvent) => {
    setAnchorEl(e.currentTarget);
  };

  const location = useLocation();
  const breadCrumbs = useSelector(
    (state: RootState) => state.utils.breadCrumbs
  );

  useEffect(() => {
    if (
      breadCrumbs.length &&
      ((breadCrumbs[breadCrumbs.length - 1]?.type &&
        breadCrumbs[breadCrumbs.length - 1].type === `object` &&
        breadCrumbs[breadCrumbs.length - 1].pathname === location.pathname) ||
        breadCrumbs[breadCrumbs.length - 1].type === `newObject`)
    ) {
      dispatch(chooseBreadCrumbs(-1));
    }

    if (
      !breadCrumbs[breadCrumbs.length - 1]?.pathname.includes('/documents?') &&
      document
    ) {
      dispatch(chooseBreadCrumbs(-1));
      dispatch(
        addBreadCrumb([
          {
            pathname: `/documents?id=${documentId}`,
            search: location.search,
            title: `Документ ШК: ${documentId}`,
            type: data ? `object` : 'newObject',
          },
        ])
      );
    }

    dispatch(
      addBreadCrumb([
        {
          pathname: location.pathname,
          search: location.search,
          title: data?.title ? data?.title : 'Файл',
          type: data ? `object` : 'newObject',
        },
      ])
    );
  }, [document, data]);

  return (
    <div className={styles.container}>
      <div id="placeholder" className={styles.width} />
      {data && (
        <OnlyOffice
          {...config}
          {...data.config}
          document={{ ...data.config.document }}
        />
      )}

      <div className={styles.buttons}>
        <>
          <Tooltip title={'Создать резолюцию'} placement={'left'}>
            <IconButton
              onClick={() => setResolutionOpen(true)}
              className={styles.button}
            >
              <Approval />
            </IconButton>
          </Tooltip>
          <Dialog open={resolutionOpen} maxWidth={'lg'} scroll="body">
            <Card className={styles.card}>
              <div className={styles.title}>
                <div>Резолюция</div>
                <IconButton
                  className={styles['close-button']}
                  onClick={() => setResolutionOpen(false)}
                >
                  <Close />
                </IconButton>
              </div>
              <Tabs value={0} className={styles.tabs}>
                <Tab value={0} label="Общие" />
              </Tabs>
              <ResolutionForm
                documentIds={[Number(documentId)]}
                edit={true}
                onClose={() => setResolutionOpen(false)}
                refetch={refetch}
              />
            </Card>
          </Dialog>
        </>
        <Divider sx={{ margin: '4px 0' }} />
        {tasks?.data.map((task) => {
          return (
            <>
              <Tooltip
                title={
                  <div>
                    {task.id} {task.type}
                    <br />
                    Автор: {task.author}
                  </div>
                }
                placement={'left'}
              >
                <IconButton className={styles.button}>
                  <InfoOutlined />
                </IconButton>
              </Tooltip>
              {ExecutionButtons.map((action) => {
                const ButtonIcon = ExecutionButtonIcons[action];
                return (
                  <ExecuteButton
                    startIcon={<ButtonIcon />}
                    statusId={ExecutionStatusIds[action]}
                    rows={[task]}
                    ids={[task.id]}
                    label={action}
                    key={action}
                    fast={ExecutionFastButtons.includes(action)}
                    button={'round'}
                  />
                );
              })}
              <Tooltip title={'Ещё'} placement={'left'}>
                <IconButton
                  onClick={(e) => {
                    setTasksAnchorEl({
                      ...tasksAnchorEl,
                      [task.id]: e.currentTarget,
                    });
                  }}
                  className={styles.button}
                >
                  <MoreHoriz />
                </IconButton>
              </Tooltip>
              <Menu
                open={Boolean(tasksAnchorEl[task.id])}
                anchorEl={tasksAnchorEl[task.id]}
                onClose={() =>
                  setTasksAnchorEl({
                    ...tasksAnchorEl,
                    [task.id]: null,
                  })
                }
                anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
                transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                className={styles.menu}
              >
                {[
                  ...Object.values(ExecutionButtonActions).map((action) => {
                    const ButtonIcon = ExecutionButtonIcons[action];
                    return (
                      <ExecuteButton
                        startIcon={<ButtonIcon />}
                        statusId={ExecutionStatusIds[action]}
                        rows={[task]}
                        ids={[task.id]}
                        label={action}
                        key={action}
                        fast={ExecutionFastButtons.includes(action)}
                        variant={'menu'}
                      />
                    );
                  }),
                ]}
                <ToolButton
                  label={'Отправить на исполнение'}
                  startIcon={<Send />}
                  onClick={() => sendToExecution(task.id)}
                  disabled={!task.canSendToExecution}
                  variant={'menu'}
                />
                <DeleteToolButton
                  label={'Удалить'}
                  startIcon={<DeleteOutline />}
                  disabled={!task.canDelete}
                  description={`Удалить выбранную задачу?`}
                  title={'Удаление задачи'}
                  onConfirm={() => deleteDocument(task.id)}
                  variant={'menu'}
                />
              </Menu>
              <Divider sx={{ margin: '4px 0' }} />
            </>
          );
        })}

        <>
          <Tooltip title={'Создать задачи'} placement={'left'}>
            <IconButton onClick={handleExpand} className={styles.button}>
              <AddCircleOutline />
            </IconButton>
          </Tooltip>
          <Menu
            open={Boolean(anchorEl)}
            anchorEl={anchorEl}
            onClose={() => setAnchorEl(null)}
            anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            className={styles.menu}
          >
            <MenuButtonItem onClick={() => setStageOpen(true)}>
              Маршрут
            </MenuButtonItem>
            <MenuButtonItem
              onClick={() => {}}
              icon={<SignalCellularAlt />}
              disabled
            >
              Этапы
            </MenuButtonItem>
            <MenuButtonItem onClick={() => {}} disabled>
              Маршрут по шаблону
            </MenuButtonItem>
          </Menu>
          {stageOpen && (
            <TasksModal
              mainId={documentId}
              open={stageOpen}
              onClose={() => setStageOpen(false)}
              stagesEditData={null}
            />
          )}
        </>
      </div>
    </div>
  );
};

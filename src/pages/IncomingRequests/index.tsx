import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useSnackbar } from 'notistack';
import { useSelector } from 'react-redux';

import {
  AddCircleOutline,
  Assignment,
  ChevronRight,
  DeleteOutline,
  Refresh,
  Send,
  SummarizeRounded,
  Undo,
} from '@mui/icons-material';

import Card from '../../components/Card';
import { TreeDataGrid } from '../../components/CustomDataGrid';
import { ExecuteButton } from '../../components/Execution';
import {
  ExecutionButtonIcons,
  ExecutionFastButtons,
  ExecutionStatusIds,
} from '../../components/Execution/ExecutionButton/constants';
import { ExcelIcon } from '../../components/Icons';
import { MenuButtonItem } from '../../components/MenuButton/MenuButtonItem';
import { RoundButton } from '../../components/RoundButton';
import { DeleteToolButton } from '../../components/ToolsPanel';
import { ToolButton, ToolsPanel } from '../../components/ToolsPanel';
import { MenuToolButton } from '../../components/ToolsPanel/MenuToolButton';
import { IncomingRequestExecutionButtons } from '../../constants/requests';
import { Routes } from '../../constants/routes';
import {
  useDeleteTaskMutation,
  useGetIncomingRequestsQuery,
  useSendToExecutionMutation,
  useSendToReexecutionMutation,
} from '../../services/api/incomingDocumentTasks';
import { useGetStageCreateDataQuery } from '../../services/api/request';
import { RootState, useAppDispatch } from '../../store';
import { setLocationData } from '../../store/utils';
import { IRequest } from '../../types/incomingRequests';
import {
  makeIncomingDocumentTasksColumns,
  tasksForUIAdapter,
} from '../../utils/incomingRequests';

import styles from '../IncomingTasks/styles.module.scss';

const groups = ['type'];
const cols = makeIncomingDocumentTasksColumns();
export const IncomingRequestPage = React.memo(() => {
  const dispatch = useAppDispatch();
  const { data, refetch, isLoading } = useGetIncomingRequestsQuery({});
  const [deleteRequest] = useDeleteTaskMutation();
  const [sendToExecution] = useSendToExecutionMutation();
  const [reexecution] = useSendToReexecutionMutation();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const openFormInNewTab = useSelector(
    (state: RootState) => state.utils.openFormInNewTab
  );

  const rowDoubleHandler = useCallback(
    (e: any) => {
      window
        .open(
          Routes.REQUESTS.REQUEST.replace(':id', e.requestId),
          openFormInNewTab ? '_blank' : '_self'
        )
        ?.focus();
    },
    [openFormInNewTab]
  );

  const dataRows = useMemo(() => tasksForUIAdapter(data?.data || []), [data]);

  const [selectedRows, setSelectedRows] = useState<Record<string, boolean>>({});
  const [tableFilters, setTableFilters] = useState(false);

  const rowsMap = useMemo(
    () =>
      (data?.data || []).reduce((acc, row) => {
        acc[row.id] = row;
        return acc;
      }, {} as Record<number, IRequest>),
    [data?.data]
  );

  const ids = useMemo(() => {
    const result: number[] = [];
    Object.keys(selectedRows).forEach((key) => {
      const numberKey = Number(key);
      if (selectedRows[key] && !isNaN(numberKey) && rowsMap[numberKey]) {
        result.push(rowsMap[numberKey].id);
      }
    });
    return result;
  }, [rowsMap, selectedRows]);

  const requestIds = useMemo(() => {
    const result: number[] = [];
    Object.keys(selectedRows).forEach((key) => {
      const numberKey = Number(key);
      if (selectedRows[key] && !isNaN(numberKey) && rowsMap[numberKey]) {
        result.push(rowsMap[numberKey].requestId);
      }
    });
    return result;
  }, [rowsMap, selectedRows]);

  const selectedRequests = useMemo(
    () => data?.data.filter((task) => ids.includes(task.id)) || [],
    [data?.data, ids]
  );

  const deleteDisabled = useMemo(() => {
    return !(ids.length === 1 && selectedRequests[0]?.canDelete);
  }, [ids, selectedRequests]);

  const handleDelete = useCallback(() => {
    deleteRequest(ids[0]);
  }, [deleteRequest, ids]);

  const sendExeDisabled = useMemo(() => {
    return !(ids.length === 1 && selectedRequests[0]?.canSendToExecution);
  }, [ids, selectedRequests]);

  const sendReexecutionDisabled = useMemo(() => {
    return !(ids.length === 1 && selectedRequests[0]?.canReturnToExecution);
  }, [ids, selectedRequests]);

  const handleSendExe = useCallback(() => {
    sendToExecution(ids[0]);
  }, [sendToExecution, ids]);

  const handleReexecute = useCallback(() => {
    reexecution(ids[0]);
  }, [reexecution, ids]);

  useEffect(() => {
    dispatch(
      setLocationData({ menuName: ['Задачи по заявкам', 'Поступившие заявки'] })
    );
  }, []);

  const { data: createData } = useGetStageCreateDataQuery(
    { reqIds: requestIds },
    { skip: !requestIds.length }
  );

  const openPage = useCallback(
    (path: string = '') => {
      window.open(`${path}`, openFormInNewTab ? '_blank' : '_self')?.focus();
    },
    [selectedRows, openFormInNewTab]
  );

  const createStageDisabled = useMemo(() => {
    return requestIds.length === 0;
  }, [ids]);

  const handleCreateStage = useCallback(() => {
    if (createData && createData.data.length) {
      openPage(`/stages`);
    } else {
      const key = enqueueSnackbar('Выбранные заявки не могут перейти на этап', {
        autoHideDuration: 10000,
        onClick: () => closeSnackbar(key),
        variant: 'error',
        anchorOrigin: { horizontal: 'right', vertical: 'top' },
      });
    }
  }, [sendToExecution, ids, createData]);

  return (
    <>
      <h1>Поступившие заявки</h1>
      <Card className={styles.card}>
        <TreeDataGrid
          rows={dataRows}
          cols={cols}
          groups={groups}
          loading={isLoading}
          onRowDoubleClick={rowDoubleHandler}
          onRowSelect={setSelectedRows}
          formKey={'received_applications'}
          showFilters={tableFilters}
        >
          <ToolsPanel
            setFilters={setTableFilters}
            leftActions={
              <>
                <RoundButton icon={<Refresh />} onClick={() => refetch()} />
                <RoundButton icon={<ExcelIcon />} />
              </>
            }
            className={styles.panel}
          >
            {[
              <MenuToolButton
                label="Подробно"
                startIcon={<ChevronRight />}
                fast
              >
                <MenuButtonItem
                  icon={<SummarizeRounded />}
                  disabled={requestIds.length !== 1}
                  onClick={() =>
                    requestIds && openPage(`/requests?id=${requestIds[0]}`)
                  }
                >
                  Заявка
                </MenuButtonItem>
                <MenuButtonItem
                  icon={<Assignment />}
                  disabled={ids.length !== 1}
                  onClick={() => ids && openPage(`/tasks/details/${ids[0]}`)}
                >
                  Задача
                </MenuButtonItem>
              </MenuToolButton>,
              ...IncomingRequestExecutionButtons.map((action) => {
                const ButtonIcon = ExecutionButtonIcons[action];
                return (
                  <ExecuteButton
                    startIcon={<ButtonIcon />}
                    statusId={ExecutionStatusIds[action]}
                    rows={ids.map((id) => rowsMap[id])}
                    ids={ids}
                    label={action}
                    key={action}
                    fast={ExecutionFastButtons.includes(action)}
                  />
                );
              }),
              <ToolButton
                label={'Отправить на исполнение'}
                startIcon={<Send />}
                onClick={handleSendExe}
                disabled={sendExeDisabled}
              />,
              <ToolButton
                label={'Вернуть на исполнение'}
                startIcon={<Undo />}
                onClick={handleReexecute}
                disabled={sendReexecutionDisabled}
              />,
              <DeleteToolButton
                label={'Удалить'}
                startIcon={<DeleteOutline />}
                title={'Удаление заявок'}
                description={`Вы действительно хотите удалить заявку?`}
                onConfirm={handleDelete}
                disabled={deleteDisabled}
              />,
              <ToolButton
                label={'Создать этап'}
                startIcon={<AddCircleOutline />}
                onClick={handleCreateStage}
                disabled={createStageDisabled}
              />,
            ]}
          </ToolsPanel>
        </TreeDataGrid>
      </Card>
    </>
  );
});

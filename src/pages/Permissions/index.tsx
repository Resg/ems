import React, { useEffect, useState } from 'react';
import {
  matchPath,
  useLocation,
  useNavigate,
  useParams,
  useSearchParams,
} from 'react-router-dom';

import { PermissionTabs, PermissionTabsLabels } from '@constants/permissions';
import { Routes } from '@constants/routes';
import { Tab, Tabs } from '@mui/material';

import Card from '../../components/Card';
import { TabPanel } from '../../components/TabPanel';
import { PermissionsCommon } from '../../containers/permissions/Common';
import { useGetPermissionDataQuery } from '../../services/api/permissions';
import { useAppDispatch } from '../../store';
import { setLocationData } from '../../store/utils';

import styles from './styles.module.scss';

export const PermissionPage = () => {
  const { tab } = useParams();
  const location = useLocation();
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const dispatch = useAppDispatch();
  const [tabValue, setTabValue] = useState(tab || PermissionTabs.COMMON);

  const idForRedirect = Number(searchParams.get('id'));
  const id =
    Number(
      matchPath(Routes.PERMISSIONS.PERMISSION_TABS, location.pathname)?.params
        ?.id
    ) ||
    Number(
      matchPath(Routes.PERMISSIONS.PERMISSION, location.pathname)?.params?.id
    ) ||
    0;

  const { data } = useGetPermissionDataQuery({ id }, { skip: !id });

  const handleTabChange = (
    _e: React.SyntheticEvent,
    newValue: PermissionTabs
  ) => {
    setTabValue(newValue);
  };

  useEffect(() => {
    dispatch(
      setLocationData({
        name: data ? `Разрешение: ${data.number}` : 'Новое разрешение',
        data: data,
      })
    );
  }, [data]);

  useEffect(() => {
    if (id) {
      navigate(
        Routes.PERMISSIONS.PERMISSION_TABS.replace(':id', id).replace(
          ':tab',
          tabValue
        )
      );
    }
  }, [tabValue]);

  useEffect(() => {
    if (idForRedirect) {
      navigate(
        Routes.PERMISSIONS.PERMISSION_TABS.replace(
          ':id',
          idForRedirect
        ).replace(':tab', tabValue)
      );
    }
  }, [idForRedirect]);

  return (
    <>
      <h1 className={styles.title}>
        {data ? `Разрешение № ${data.number}` : 'Разрешение'}
      </h1>
      <Card className={styles.card}>
        {data ? (
          <Tabs value={tabValue} onChange={handleTabChange}>
            {Object.values(PermissionTabs).map((tab) => (
              <Tab key={tab} label={PermissionTabsLabels[tab]} value={tab} />
            ))}
          </Tabs>
        ) : (
          <Tabs value={tabValue} onChange={handleTabChange}>
            <Tab
              key={PermissionTabs.COMMON}
              label={PermissionTabsLabels[PermissionTabs.COMMON]}
              value={PermissionTabs.COMMON}
            />
          </Tabs>
        )}

        <TabPanel value={tabValue} index={PermissionTabs.COMMON}>
          <PermissionsCommon />
        </TabPanel>
      </Card>
    </>
  );
};
